<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
        <script src="<c:url value="/resources/js/jquery-1.12.3.min.js" />"></script>
    </head>
    <body>
        <h2>Stable data upload progress</h2>
        <p>
            This page provides an overview of the stable data upload progress by data collection and data provider. <br/><br/>
            Each cell in the table below shows the number of stable data records that have been uploaded correctly in HD4DP against the total number of records that have been collected in HD4RES.<br/><br/>
            Detailed information is displayed when clicking on a cell, and hidden when clicking on the cell again. This detailed information contains the number of records for each status.
            In fact, each stable data record is associated with one of the following (technical) statuses:<br/>

            <ul>
                <li>new: the record has recently been uploaded in HD4RES and is waiting to be sent to HD4DP</li>
                <li>sent: the record has been sent to HD4DP</li>
                <li>uploaded: the record has been uploaded correctly in HD4DP</li>
                <li>error_sending: an error occurred while sending the record to HD4DP</li>
                <li>error_uploading: an error occurred while uploading the record in HD4DP</li>
            </ul>

            Finally, completed uploads are highlighted in green, in-progress uploads are highlighted in orange, and uploads for which a problem has been detected are highlighted in red.
        </p>

        <c:choose>
            <c:when test="${empty data.countsPerInstallation}">
                <span class="nodata">No data available.</span>
            </c:when>
            <c:otherwise>
                <table>
                    <%--header row--%>
                    <tr>
                        <th colspan="2">Data provider</th>
                        <c:forEach var="dataCollectionName" items="${dataCollectionNames}">
                            <th>${dataCollectionName}</th>
                        </c:forEach>
                    </tr>

                    <%--data rows--%>
                    <c:forEach var="entry" items="${data.countsPerInstallation}">

                        <c:set var="installationId" value="${entry.key}"/>
                        <c:set var="installationName" value="${installationNames[installationId]}"/>
                        <c:set var="countsPerInstallation" value="${entry.value}"/>
                        <tr>
                            <td class="alignleft">${installationName}</td>
                            <td>${installationId}</td>
                            <c:forEach var="dataCollectionName" items="${dataCollectionNames}">
                                <c:set var="countsPerDataCollection" value="${countsPerInstallation.countsPerDataCollection}"/>
                                <c:set var="counts" value="${countsPerDataCollection[dataCollectionName]}"/>
                                <td class="progress ${counts.globalStatus}">
                                    <c:if test="${not empty counts}" >
                                        <c:set var="uploaded" value="${counts.counts['uploaded']}"/>
                                        <c:set var="total" value="${counts.count}"/>
                                        <c:if test="${empty uploaded}">
                                            <c:set var="uploaded" value="0" />
                                        </c:if>
                                        <span class="progress">${uploaded}/${total}</span><br/>
                                        <span class="details" style="display: none">
                                        <c:forEach var="status" items="${stable_data_statuses}">
                                            <c:set var="count" value="${counts.counts[status]}" />
                                            <c:if test="${empty counts.counts[status]}">
                                                <c:set var="count" value="0" />
                                            </c:if>
                                            ${status}: ${count}<br/>
                                        </c:forEach>
                                        </span>
                                    </c:if>
                                </td>
                            </c:forEach>
                        </tr>
                    </c:forEach>
                    <tr>
                        <th colspan="100%" class="tablefooter">
                            <span id="showalldetails">show details</span>
                            <span id="hidealldetails">hide details</span>
                        </th>
                    </tr>
                </table>
            </c:otherwise>
        </c:choose>

        <script type="text/javascript">
            $(document).ready(function(){
                $(".progress").click(function() {
                    $(this).find(".details").toggle();
                });
                $("#showalldetails").click(function() {
                    $(document).find(".details").show();
                });
                $("#hidealldetails").click(function() {
                    $(document).find(".details").hide();
                });
            });
        </script>
    </body>
</html>
