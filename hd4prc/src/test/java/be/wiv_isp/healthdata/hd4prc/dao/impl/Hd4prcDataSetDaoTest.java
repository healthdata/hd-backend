/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4prc.dao.impl;

import be.wiv_isp.healthdata.hd4prc.dao.IHd4prcDataSetDao;
import be.wiv_isp.healthdata.hd4prc.domain.Hd4prcDataSet;
import be.wiv_isp.healthdata.hd4prc.domain.search.Hd4prcDataSetSearch;
import be.wiv_isp.healthdata.hd4prc.enumeration.DataSetFormat;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class Hd4prcDataSetDaoTest {

	@Autowired
	private IHd4prcDataSetDao dataSetDao;

	@Test
	public void testGetAll() {
		String workflowUuid1 = UUID.randomUUID().toString();
		Hd4prcDataSet dataSet1 = new Hd4prcDataSet();
		dataSet1.setWorkflowUuid(workflowUuid1);
		dataSet1.setFormat(DataSetFormat.EFORMS_KMEHR);
		dataSet1.setContent("content".getBytes(StandardCharsets.UTF_8));
		dataSetDao.create(dataSet1);

		Hd4prcDataSet dataSet2 = new Hd4prcDataSet();
		dataSet2.setWorkflowUuid(workflowUuid1);
		dataSet2.setFormat(DataSetFormat.EFORMS_KMEHR);
		dataSet2.setContent("other content".getBytes(StandardCharsets.UTF_8));
		dataSetDao.create(dataSet2);

		String workflowUuid2 = UUID.randomUUID().toString();
		Hd4prcDataSet dataSet3 = new Hd4prcDataSet();
		dataSet3.setWorkflowUuid(workflowUuid2);
		dataSet3.setFormat(DataSetFormat.EFORMS_KMEHR);
		dataSet3.setContent("content".getBytes(StandardCharsets.UTF_8));
		dataSetDao.create(dataSet3);


		Hd4prcDataSetSearch search1 = new Hd4prcDataSetSearch();
		search1.setWorkflowUuid(workflowUuid1);

		List<Hd4prcDataSet> actual1 = dataSetDao.getAll(search1);

		Assert.assertEquals(2, actual1.size());
		Assert.assertTrue(actual1.contains(dataSet1));
		Assert.assertTrue(actual1.contains(dataSet2));
	}
}
