/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4prc.mapper;

import be.wiv_isp.healthdata.hd4prc.dto.EFormsCreateRequest;
import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class EFormsCreateRequestMapperTest {

    @Test
    public void testMapper1() throws Exception {
        String request = IOUtils.toString(EFormsCreateRequestMapper.class.getResourceAsStream("/eFormsCreateRequest1.json"));
        JSONObject json = new JSONObject(request);

        EFormsCreateRequest expected = new EFormsCreateRequest();
        expected.setName("TEST");
        expected.setVersion("1.0.0");
        expected.setFormat("A4");
        expected.setIntegratorName("CareConnect");
        expected.setIntegratorVersion("2.6");

        EFormsCreateRequest result = EFormsCreateRequestMapper.map(json);
        Assert.assertEquals(expected, result);
    }
}
