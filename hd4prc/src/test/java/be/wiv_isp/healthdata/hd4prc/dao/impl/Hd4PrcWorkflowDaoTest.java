/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4prc.dao.impl;

import be.wiv_isp.healthdata.common.domain.enumeration.Language;
import be.wiv_isp.healthdata.hd4prc.dao.IHd4prcWorkflowDao;
import be.wiv_isp.healthdata.hd4prc.domain.Hd4prcIdentification;
import be.wiv_isp.healthdata.hd4prc.domain.Hd4prcWorkflow;
import be.wiv_isp.healthdata.hd4prc.domain.search.Hd4prcWorkflowSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class Hd4PrcWorkflowDaoTest {

	@Autowired
	private IHd4prcWorkflowDao workflowDao;

	@Test
	public void testCountUnique() {
		Hd4prcIdentification identification1 = new Hd4prcIdentification();
		identification1.setValue("111111111");

		Hd4prcIdentification identification2 = new Hd4prcIdentification();
		identification2.setValue("22222222");

		Hd4prcWorkflow workflow1 = new Hd4prcWorkflow();
		workflow1.setIdentification(identification1);
		workflow1.setUniqueID("uniqueId1");
		workflow1.setDataCollectionName("TEST");
		workflow1.setDataCollectionDefinitionId(1L);
		workflow1.setContent("{}".getBytes());
		workflow1.setFormat("A4");
		workflow1.setLanguage(Language.EN);
		workflow1.setReceivedOnHd4res(false);
		workflowDao.create(workflow1);

		Hd4prcWorkflow workflow2 = new Hd4prcWorkflow();
		workflow2.setIdentification(identification1);
		workflow2.setUniqueID("uniqueId2");
		workflow2.setDataCollectionName("TEST");
		workflow2.setDataCollectionDefinitionId(1L);
		workflow2.setContent("{}".getBytes());
		workflow2.setFormat("A4");
		workflow2.setLanguage(Language.EN);
		workflow2.setReceivedOnHd4res(false);
		workflowDao.create(workflow2);

		Hd4prcWorkflow workflow3 = new Hd4prcWorkflow();
		workflow3.setIdentification(identification1);
		workflow3.setUniqueID("uniqueId1");
		workflow3.setDataCollectionName("TEST");
		workflow3.setDataCollectionDefinitionId(2L);
		workflow3.setContent("{}".getBytes());
		workflow3.setFormat("A4");
		workflow3.setLanguage(Language.EN);
		workflow3.setReceivedOnHd4res(false);
		workflowDao.create(workflow3);

		Hd4prcWorkflow workflow4 = new Hd4prcWorkflow();
		workflow4.setIdentification(identification2);
		workflow4.setUniqueID("uniqueId1");
		workflow4.setDataCollectionName("TEST");
		workflow4.setDataCollectionDefinitionId(1L);
		workflow4.setContent("{}".getBytes());
		workflow4.setFormat("A4");
		workflow4.setLanguage(Language.EN);
		workflow4.setReceivedOnHd4res(false);
		workflowDao.create(workflow4);

		Hd4prcWorkflowSearch search = new Hd4prcWorkflowSearch();
		search.setDataCollectionDefinitionId(1L);
		search.setIdentification(identification1);
		search.setUniqueId("uniqueId1");

		Assert.assertEquals(1, workflowDao.count(search));

	}
}
