/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.hd4prc.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.search.OrganizationSearch;
import be.wiv_isp.healthdata.common.dto.SalesForceAccountDto;
import be.wiv_isp.healthdata.common.util.IdentificationType;
import be.wiv_isp.healthdata.hd4prc.service.IHd4prcUserService;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import be.wiv_isp.healthdata.orchestration.domain.validator.IUserValidator;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationSetupService;
import be.wiv_isp.healthdata.orchestration.service.ISalesForceOrganizationService;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractUserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
public class Hd4prcUserService extends AbstractUserService implements IHd4prcUserService {

    private static final Logger LOG = LoggerFactory.getLogger(Hd4prcUserService.class);

    @Autowired
    protected IOrganizationService organizationService;
    @Autowired
    protected ISalesForceOrganizationService salesForceOrganizationService;
    @Autowired
    protected IOrganizationSetupService organizationSetupService;
    @Autowired
    protected IUserValidator hd4prcUserValidator;

    public static String getIdentification(String identification) {
        return StringUtils.substring(identification, 0, 8);
    }

    @Override
    @Transactional
    public User getOrCreate(String identification) {
        identification = getIdentification(identification);
        final User user = new User();
        user.setLdapUser(false);
        user.setEnabled(true);
        user.setUsername(identification);
        user.setPassword(identification); // can be removed?
        Set<Authority> authorities = new HashSet<>();
        Authority hd4prcAuthority = new Authority();
        hd4prcAuthority.setAuthority(Authority.HD4PRC);
        authorities.add(hd4prcAuthority);
        Authority userAuthority = new Authority();
        userAuthority.setAuthority(Authority.USER);
        authorities.add(userAuthority);
        user.setAuthorities(authorities);

        return getOrCreate(user);
    }

    @Override
    @Transactional
    public User getOrCreate(final User user) {
        final String username = user.getUsername();

        final UserSearch search = new UserSearch();
        search.setUsername(username);
        final User existingUser = getUnique(search);
        if (existingUser != null) {
            return existingUser;
        }
        user.setOrganization(createOrganization(username));
        LOG.info("Create user[{}]", username);
        return create(user);
    }

    private Organization createOrganization(String identification) {
        LOG.info("Create organization[{}]", identification);
        Organization main = organizationService.getMain();
        SalesForceAccountDto organization = new SalesForceAccountDto();
        organization.setName(identification);
        organization.setHealthData_ID_Type__c(IdentificationType.RIZIV);
        organization.setRIZIV_Number__c(identification);
        organization.setHD4PrC_Installation__c(true);

        salesForceOrganizationService.createSalesForceOrganization(main.getHealthDataIDType(), main.getHealthDataIDValue(), organization);

        organizationSetupService.updateOrganizations();

        OrganizationSearch search = new OrganizationSearch();
        search.setHealthDataIDType(IdentificationType.RIZIV);
        search.setHealthDataIDValue(identification);
        return organizationService.getUnique(search);
    }

    @Override
    public IUserValidator getUserValidator() {
        return hd4prcUserValidator;
    }
}