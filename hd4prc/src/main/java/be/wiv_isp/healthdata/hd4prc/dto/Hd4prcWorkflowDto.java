/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4prc.dto;

import be.wiv_isp.healthdata.common.domain.enumeration.Language;
import be.wiv_isp.healthdata.gathering.dto.RegistrationWorkflowDto;
import be.wiv_isp.healthdata.hd4prc.domain.Hd4prcWorkflow;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import be.wiv_isp.healthdata.orchestration.dto.DocumentDto;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Hd4prcWorkflowDto extends RegistrationWorkflowDto {

    private String uuid;
    private Language language;
    private Hd4prcIdentificationDto identification;

    public Hd4prcWorkflowDto(Hd4prcWorkflow workflow) {
        id = workflow.getUuid();
        uuid = workflow.getUuid();
        dataCollectionName = workflow.getDataCollectionName();
        dataCollectionDefinitionId = workflow.getDataCollectionDefinitionId();
        identification = new Hd4prcIdentificationDto(workflow.getIdentification());
        document = new DocumentDto();
        document.setDocumentData(new DocumentData());
        document.getDocumentData().setContent(workflow.getContent());
        document.getDocumentData().setPatientID(workflow.getPatientId());
        document.getDocumentData().setPrivate(workflow.getPrivateContent());
        document.setCreatedOn(workflow.getCreatedOn());
        document.setUpdatedOn(workflow.getUpdatedOn());
        language = workflow.getLanguage();
        createdOn = workflow.getCreatedOn();
        updatedOn = workflow.getUpdatedOn();
    }

    @Override
    @JsonIgnore(value = false)
    public DocumentDto getDocument() {
        return super.getDocument();
    }

    public Hd4prcIdentificationDto getIdentification() {
        return identification;
    }

    public void setIdentification(Hd4prcIdentificationDto identification) {
        this.identification = identification;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
