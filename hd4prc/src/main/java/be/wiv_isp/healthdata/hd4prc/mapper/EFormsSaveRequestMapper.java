/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4prc.mapper;

import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.hd4prc.dto.EFormsSaveRequest;
import be.wiv_isp.healthdata.orchestration.jaxb.json.JsonUnmarshaller;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.*;

public class EFormsSaveRequestMapper {

    public static EFormsSaveRequest map(JSONObject json) throws JSONException {
        EFormsSaveRequest request = new EFormsSaveRequest();
        for (Iterator<String> iterator = json.keys(); iterator.hasNext();) {
            String key = iterator.next();
            if (!"null".equalsIgnoreCase(json.getString(key))) {
                if ("workflowId".equalsIgnoreCase(key)) {
                    request.setUuid(StringUtils.substring(json.getString(key), 0, 36));
                } else if ("documentData".equalsIgnoreCase(key)) { // new format >= v1.8.0
                    final JSONObject docDataObject = JsonUtils.getJSONObject(json, key);
                    final Iterator<String> docDataIt = docDataObject.keys();
                    while (docDataIt.hasNext()) {
                        final String docDataKey = docDataIt.next();
                        if ("content".equalsIgnoreCase(docDataKey)) {
                            request.setContent(JsonUtils.getString(docDataObject, docDataKey).getBytes(StandardCharsets.UTF_8));
                        } else if ("patientID".equalsIgnoreCase(docDataKey) && !docDataObject.isNull(docDataKey)) {
                            final Map<String, String> patientId = (Map<String, String>) JsonUnmarshaller.unmarshal(JsonUtils.getString(docDataObject, docDataKey), HashMap.class);
                            final Set<String> keysToRemove = new HashSet<>();
                            for (Map.Entry<String, String> entry : patientId.entrySet()) {
                                if (StringUtils.isBlank(entry.getValue()))
                                    keysToRemove.add(entry.getKey());
                            }
                            for (String keyToRemove : keysToRemove) {
                                patientId.remove(keyToRemove);
                            }
                            request.setPatientId(patientId);
                        } else if ("private".equalsIgnoreCase(docDataKey) && !docDataObject.isNull(docDataKey)) {
                            request.setPrivateContent(JsonUtils.getString(docDataObject, docDataKey).getBytes(StandardCharsets.UTF_8));
                        }
                    }
                }
            }
        }
        return request;
    }

}
