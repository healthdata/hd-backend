/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4prc.service.impl;

import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IWebServiceClientService;
import be.wiv_isp.healthdata.common.api.ClientVersion;
import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV6;
import be.wiv_isp.healthdata.common.dto.VersionDto;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.hd4prc.api.uri.Hd4prcDataCollectionDefinitionUri;
import be.wiv_isp.healthdata.hd4prc.service.IHd4prcDataCollectionDefinitionForwardService;
import com.sun.jersey.api.client.GenericType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;

@Service("hd4prcDataCollectionDefinitionForwardService")
public class Hd4prcDataCollectionDefinitionForwardService implements IHd4prcDataCollectionDefinitionForwardService {

    @Autowired
    private IWebServiceClientService webServiceClientService;

    @Autowired
    private IConfigurationService configurationService;

    private static final Logger LOG = LoggerFactory.getLogger(Hd4prcDataCollectionDefinitionForwardService.class);

    @Override
    public DataCollectionDefinitionDtoV6 get(String dataCollectionName, VersionDto version) {
        LOG.info("Getting data collection definition with name [{}] and version [{}] ", dataCollectionName, version);
        Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

        Hd4prcDataCollectionDefinitionUri dataCollectionDefinitionUri = new Hd4prcDataCollectionDefinitionUri();
        dataCollectionDefinitionUri.setHost(catalogueHost.getValue());

        WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.setUrl(dataCollectionDefinitionUri.toString());
        wsb.addHeader(HttpHeaders.CLIENT_VERSION, ClientVersion.Hd4prc.getDefault());
        wsb.setGet(true);
        wsb.addParameter(RestUtils.ParameterName.DATA_COLLECTION_NAME, dataCollectionName);
        wsb.addParameter("majorVersion", String.valueOf(version.getMajor()));
        wsb.addParameter("minorVersion", String.valueOf(version.getMinor()));
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.setReturnType(new GenericType<DataCollectionDefinitionDtoV6>() {});
        wsb.setLogResponseBody(true);

        return (DataCollectionDefinitionDtoV6) webServiceClientService.callWebService(wsb);    }
}
