/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.hd4prc.security.domain;

public enum ShibbolethAttribute {

    /* Person attributes */
    PERSON_LASTNAME("Shib-Person-LastName"), //urn:be:fgov:person:lastName
    PERSON_FIRSTNAME("Shib-Person-FirstName"), //urn:be:fgov:person:firstName
    PERSON_SSIN("Shib-Person-SSIN"), //urn:be:fgov:person:ssin
    ROLE("Shib-Role"), //urn:be:fgov:ehealth:1.0:role

    /* Organisation attributes */
    ORG_TYPE_CODE("Shib-Organization-Type-Code"), //urn:be:fgov:organization:type-code
    ORG_NAME("Shib-Organization-Name"), //urn:be:fgov:organization:name
    ORG_NAME_LOCALIZED("Shib-Organization-Name-Localised"), //urn:be:fgov:organization:name-localised
    ORG_ID("Shib-Organization-Id"), //urn:be:fgov:organization:id
    ORG_ID_CODE("Shib-Organization-Id-Code"), //urn:be:fgov:organization:id-code
    ORG_ID_TYPE("Shib-Organization-Id-Type"), //urn:be:fgov:organization:id-type

    /* Hospital attributes */
    HOSPITAL_NIHII("Shib-Hospital-Nihii-Number"), //urn:be:fgov:ehealth:1.0:hospital:nihii-number
    HOSPITAL_ISADMINISTRATIVE_PROFILE("Shib-Hospital-Administrative"), //urn:be:fgov:ehealth:1.0:hospital:nihii-number:person:ssin:remaph:administrative:boolean

    /* Doctor attributes */
    DOCTOR_NIHII("Shib-Doctor-Nihii"), //urn:be:fgov:person:ssin:ehealth:1.0:doctor:nihii11

    /* Lab */
    LAB_NIHII("Shib-Lab-Nihii"), //urn:be:fgov:ehealth:1.0:labo:nihii-number
    LAB_ISADMINISTRATIVE_PROFILE("Shib-Lab-Administrative"), //urn:be:fgov:ehealth:1.0:labo:nihii-number:person:ssin:remaph:administrative:boolean

    /* General eHealth/Shib attributes */
    ENTITY_QUALITY("Shib-Entity-Quality"), //urn:be:giami:delta:entity:quality
    USER_EXTERNAL_ID("Shib-User-External-Id"), //urn:be:giami:delta:user:external-id
    PERSISTENT_REF("Shib-Persistent-Ref"), //urn:be:fgov:ehealth:1.0:persistent-ref
    CHOSEN_LANGUAGE("Shib-Chosen-Language"), //urn:be:fgov:ehealth:1.0:chosenlanguage
    AUTHENTICATION_METHOD("Shib-Authentication-Method"), //urn:be:fgov:ehealth:1.0:authentication-method
    AUTHENTICATION_LEVEL("Shib-Authentication-Level"), //urn:be:fgov:ehealth:1.0:authentication-level
    AUTHENTICATION_AUTHORITY("Shib-Authentication-Authority"), //urn:be:fgov:ehealth:1.0:authentication-authority
    GENERAL_EHEALTH_REF("Shib-eHealth-Ref"), //urn:be:fgov:ehealth:1.0:ehealth-ref
    GENERAL_SERVICE_NAME("Shib-Service-Name"), //urn:be:fgov:ehealth:1.0:service-name
    GENERAL_AUTH_DECISION("Shib-Authz-Decision"), //urn:be:fgov:ehealth:1.0:authz-decision
    ACCESS_NETWORK("Shib-Access-Network"), //urn:be:fgov:ehealth:1.0:access-network
    LOGIN_TYPE("Shib-Login-Type"), //urn:be:fgov:ehealth:1.0:login-type
    TYPE_CODE("Shib-Type-Code"), //urn:be:fgov:person:professional:type-code

    PROFILE_OPTION_TYPE("Shib-Profile-Option-type"); //urn:be:fgov:ehealth:1.0:profileOptionType

    private String shibId;

    ShibbolethAttribute(String shibId) {
        this.shibId = shibId;
    }

    public String getShibId() {
        return shibId;
    }

    @Override
    public String toString() {
        return shibId;
    }

    public static ShibbolethAttribute getRoleAttribute() {
        return ROLE;
    }
}

