/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4prc.dto;

import java.util.Map;

public class EFormsSaveRequest {

    private String uuid;
    private byte[] content;
    private byte[] privateContent;
    private Map<String, String> patientId;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public byte[] getPrivateContent() {
        return privateContent;
    }

    public void setPrivateContent(byte[] privateContent) {
        this.privateContent = privateContent;
    }

    public Map<String, String> getPatientId() {
        return patientId;
    }

    public void setPatientId(Map<String, String> patientId) {
        this.patientId = patientId;
    }
}
