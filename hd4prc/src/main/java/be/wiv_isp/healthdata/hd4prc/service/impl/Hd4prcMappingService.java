/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4prc.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IWebServiceClientService;
import be.wiv_isp.healthdata.common.domain.enumeration.Language;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.common.json.SerializableJsonObject;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.gathering.service.impl.MappingService;
import be.wiv_isp.healthdata.hd4prc.dto.PdfMappingRequest;
import be.wiv_isp.healthdata.hd4prc.mapper.EFormsCreateRequestMapper;
import be.wiv_isp.healthdata.hd4prc.service.IHd4prcMappingService;
import be.wiv_isp.healthdata.orchestration.api.mapper.MappingResponseMapper;
import be.wiv_isp.healthdata.orchestration.mapping.MappingRequest;
import be.wiv_isp.healthdata.orchestration.mapping.MappingResponse;
import be.wiv_isp.healthdata.orchestration.service.IMappingService;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Service("hd4prcMappingService")
public class Hd4prcMappingService implements IHd4prcMappingService {

    @Autowired
    protected IWebServiceClientService webServiceClientService;
    @Autowired
    protected IConfigurationService configurationService;
    @Resource(name = "mappingService")
    private IMappingService mappingService;

    @Override
    public MappingResponse mapFromBdsToJson(byte[] dcdContent, byte[] content, Language language) {
        final MappingRequest request = new MappingRequest();
        request.getInput().put(MappingRequest.Input.BASIC_DATASET, new String(content, StandardCharsets.UTF_8));
        request.getInput().put(MappingRequest.Input.DATA_COLLECTION_DEFINITION, JsonUtils.createSerializableJsonObject(dcdContent));
        request.getInput().put(MappingRequest.Input.LANGUAGE, language.getMapping());

        request.getTargets().add(MappingRequest.Target.DOCUMENT);
        request.getTargets().add(MappingRequest.Target.TO_BE_CODED);
        request.getTargets().add(MappingRequest.Target.UNIQUE_ID);

        JSONObject response = mappingService.get(request, MappingService.Endpoint.IMPORT_BASIC_DATASET);
        return MappingResponseMapper.convert(response);
    }

    @Override
    public JSONObject mapToElasticSearchDocument(byte[] dcdContent, byte[] documentContent, byte[] privateContent) {
        final MappingRequest request = new MappingRequest();
        request.getInput().put(MappingRequest.Input.DATA_COLLECTION_DEFINITION, JsonUtils.createSerializableJsonObject(dcdContent));
        request.getTargets().add(MappingRequest.Target.ES);
        final JSONObject documentObject = new SerializableJsonObject();
        JsonUtils.put(documentObject, MappingRequest.Fields.CONTENT, JsonUtils.createJsonObject(documentContent));
        if(privateContent != null) {
            JsonUtils.put(documentObject, MappingRequest.Fields.PRIVATE, JsonUtils.createJsonObject(privateContent));
        }
        request.getInput().put(MappingRequest.Input.DOCUMENT, documentObject);

        final JSONObject response = mappingService.get(request);
        return MappingResponseMapper.convert(response).getEsDocument();

    }

    @Override
    public String getUniqueID(byte[] dcdContent, byte[] documentContent, byte[] privateContent) {
        final MappingRequest request = new MappingRequest();
        final JSONObject documentObject = new SerializableJsonObject();
        JsonUtils.put(documentObject, MappingRequest.Fields.CONTENT, JsonUtils.createJsonObject(documentContent));
        if(privateContent != null) {
            JsonUtils.put(documentObject, MappingRequest.Fields.PRIVATE, JsonUtils.createJsonObject(privateContent));
        }
        request.getInput().put(MappingRequest.Input.DOCUMENT, documentObject);
        request.getInput().put(MappingRequest.Input.DATA_COLLECTION_DEFINITION, JsonUtils.createSerializableJsonObject(dcdContent));
        request.getTargets().add(MappingRequest.Target.UNIQUE_ID);

        final JSONObject response = mappingService.get(request, MappingService.Endpoint.EMPTY);
        return MappingResponseMapper.convert(response).getUniqueID();
    }

    @Override
    public byte[] getPdf(byte[] templateContent, byte[] esDocument) {
        Configuration configuration = configurationService.get(ConfigurationKey.PDF_HOST);

        PdfMappingRequest request = new PdfMappingRequest();
        request.setTemplate(templateContent);
        request.setValues(esDocument);
        WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.setUrl(configuration.getValue());
        wsb.setPost(true);
        wsb.setType(MediaType.APPLICATION_JSON_TYPE);
        wsb.setAccept(MediaType.APPLICATION_OCTET_STREAM_TYPE);
        wsb.setJson(request);
        wsb.setReturnType(new GenericType<ClientResponse>() {});

        ClientResponse clientResponse = (ClientResponse) webServiceClientService.callWebService(wsb);
        try {
            return IOUtils.toByteArray(clientResponse.getEntityInputStream());
        } catch (IOException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION);
            throw exception;
        }
    }

    private JSONObject getMockResponse() {
        try {
            String request = IOUtils.toString(EFormsCreateRequestMapper.class.getResourceAsStream("/mappingMock1.json"));
            return new JSONObject(request);
        } catch (IOException  | JSONException e) {
            throw new RuntimeException(e);
        }
    }
}
