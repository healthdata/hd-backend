/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4prc.service.impl;

import be.healthconnect.adr._1_0.ADR;
import be.healthconnect.adr._1_0.Addressee;
import be.healthconnect.adr._1_0.Attachment;
import be.healthconnect.adr._1_0.Patient;
import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV6;
import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.domain.enumeration.Language;
import be.wiv_isp.healthdata.common.domain.enumeration.TemplateKey;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.factory.impl.MessageFactory;
import be.wiv_isp.healthdata.gathering.service.IRegistrationWorkflowService;
import be.wiv_isp.healthdata.hd4prc.action.standalone.Hd4prcCreateAction;
import be.wiv_isp.healthdata.hd4prc.api.uri.Hd4prcDocumentUri;
import be.wiv_isp.healthdata.hd4prc.dao.IHd4prcDataSetDao;
import be.wiv_isp.healthdata.hd4prc.dao.IHd4prcWorkflowDao;
import be.wiv_isp.healthdata.hd4prc.dao.IHd4prcWorkflowHistoryDao;
import be.wiv_isp.healthdata.hd4prc.domain.*;
import be.wiv_isp.healthdata.hd4prc.domain.search.Hd4prcWorkflowSearch;
import be.wiv_isp.healthdata.hd4prc.dto.*;
import be.wiv_isp.healthdata.hd4prc.enumeration.DataSetFormat;
import be.wiv_isp.healthdata.hd4prc.enumeration.ExportType;
import be.wiv_isp.healthdata.hd4prc.enumeration.Hd4prcAction;
import be.wiv_isp.healthdata.hd4prc.mapper.EFormsCreateRequestMapper;
import be.wiv_isp.healthdata.hd4prc.mapper.EFormsMergeRequestMapper;
import be.wiv_isp.healthdata.hd4prc.mapper.EFormsSaveRequestMapper;
import be.wiv_isp.healthdata.hd4prc.mapper.Hd4prcWorkflowMapper;
import be.wiv_isp.healthdata.hd4prc.service.IEFormsService;
import be.wiv_isp.healthdata.hd4prc.service.IHd4prcDataCollectionDefinitionForwardService;
import be.wiv_isp.healthdata.hd4prc.service.IHd4prcMappingService;
import be.wiv_isp.healthdata.orchestration.api.uri.DocumentUrl;
import be.wiv_isp.healthdata.orchestration.domain.CSVContent;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.TypedValue;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.mapper.WorkflowStatusMapper;
import be.wiv_isp.healthdata.orchestration.dto.MailDto;
import be.wiv_isp.healthdata.orchestration.dto.ValidationResponse;
import be.wiv_isp.healthdata.orchestration.jaxb.json.JsonMarshaller;
import be.wiv_isp.healthdata.orchestration.jaxb.json.JsonUnmarshaller;
import be.wiv_isp.healthdata.orchestration.domain.mail.RegistrationUpdatesContext;
import be.wiv_isp.healthdata.orchestration.mapping.MappingRequest;
import be.wiv_isp.healthdata.orchestration.mapping.MappingResponse;
import be.wiv_isp.healthdata.orchestration.service.*;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;

import javax.annotation.Resource;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Service("eFormsService")
public class EFormsService implements IEFormsService {

    @Autowired
    private IDataCollectionDefinitionForwardService dataCollectionDefinitionForwardService;
    @Autowired
    private IPdfTemplateForwardService pdfTemplateForwardService;
    @Resource(name="hd4prcDataCollectionDefinitionForwardService")
    private IHd4prcDataCollectionDefinitionForwardService catalogueServiceHd4prc;
    @Autowired
    private IDataCollectionDefinitionForwardService catalogueService;
    @Resource(name="hd4prcMappingService")
    private IHd4prcMappingService mappingServiceHd4prc;
    @Resource
    private IMappingService mappingService;
    @Autowired
    private IHd4prcWorkflowDao workflowDao;
    @Autowired
    private IHd4prcDataSetDao dataSetDao;
    @Autowired
    private IHd4prcWorkflowHistoryDao historyDao;
    @Autowired
    private IConfigurationService configurationService;
    @Autowired
    private ITemplateForwardService templateForwardService;
    @Autowired
    private IMailService mailService;
    @Autowired
    private IRegistrationWorkflowService workflowService;

    @Override
    @Transactional
    public Response create(String languageHeader, JSONObject json) {
        Language language = Language.getFromHeader(languageHeader);
        EFormsCreateRequest eFormsCreateRequest;
        try {
            eFormsCreateRequest = EFormsCreateRequestMapper.map(json);
        } catch (JSONException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.JSON_PARSE_EXCEPTION);
            throw exception;
        }

        DataCollectionDefinitionDtoV6 dataCollectionDefinition = catalogueServiceHd4prc.get(eFormsCreateRequest.getName(), eFormsCreateRequest.getVersion());
        if (!dataCollectionDefinition.isValidForCreation(new Date())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_DATE_FOR_DATA_COLLECTION_DEFINITION, dataCollectionDefinition.getDataCollectionName(), "creation");
            throw exception;
        }

        Hd4prcWorkflow workflow = new Hd4prcWorkflow();
        workflow.setDataCollectionName(dataCollectionDefinition.getDataCollectionName());
        workflow.setDataCollectionDefinitionId(dataCollectionDefinition.getId());
        workflow.setLanguage(language);
        workflow.setReceivedOnHd4res(false);
        workflow.setFormat(eFormsCreateRequest.getFormat());
        Integrator integrator = new Integrator();
        integrator.setName(eFormsCreateRequest.getIntegratorName());
        integrator.setVersion(eFormsCreateRequest.getIntegratorVersion());
        workflow.setIntegrator(integrator);
        workflow.setContent("{}".getBytes(StandardCharsets.UTF_8));
        workflow = workflowDao.create(workflow);
        createHistory(workflow.getUuid(), Hd4prcAction.CREATE);

        Hd4prcDocumentUri hd4prcDocumentUri = new Hd4prcDocumentUri();
        hd4prcDocumentUri.setHost(configurationService.get(ConfigurationKey.GUI_HOST).getValue());
        hd4prcDocumentUri.setUuid(workflow.getUuid());
        EFormsCreateResponse response = new EFormsCreateResponse();
        response.setFormId(workflow.getUuid());
        response.setClientUrl(hd4prcDocumentUri.toString());
        return Response.ok(response).build();
    }

    @Override
    @Transactional
    public Response upload(UriInfo info, String body, DataSetFormat format) {
        String uuid = RestUtils.getPathString(info, RestUtils.ParameterName.UUID);
        Hd4prcWorkflow workflow = getWorkflow(uuid);
        if(StringUtils.isBlank(body)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_PARAMETER, "body");
            throw exception;
        }
        byte[] content = body.getBytes(StandardCharsets.UTF_8);

        Hd4prcDataSet dataSet = new Hd4prcDataSet();
        dataSet.setWorkflowUuid(workflow.getUuid());
        dataSet.setFormat(format);
        dataSet.setContent(content);
        dataSetDao.create(dataSet);
        createHistory(workflow.getUuid(), Hd4prcAction.UPLOAD, dataSet.getUuid());

        return Response.ok(dataSet.getUuid())
                .type(MediaType.TEXT_PLAIN_TYPE)
                .build();
    }


    @Transactional
    public Response merge(UriInfo info, JSONObject json) {
        String uuid = RestUtils.getPathString(info, RestUtils.ParameterName.UUID);
        Hd4prcWorkflow workflow = getWorkflow(uuid);
        EFormsMergeRequest eFormsMergeRequest;
        try {
            eFormsMergeRequest = EFormsMergeRequestMapper.map(json);
        } catch (JSONException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.JSON_PARSE_EXCEPTION);
            throw exception;
        }
        Hd4prcDataSet dataSet = dataSetDao.get(eFormsMergeRequest.getDataSetId());
        if(dataSet == null) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "dataSet", eFormsMergeRequest.getDataSetId());
            throw exception;
        }
        switch (dataSet.getFormat()) {
            case EFORMS_KMEHR:
                mergeKmehr(workflow, dataSet);
                break;
            case HEALTHDATA_JSON:
                mergeJson(workflow, dataSet);
                break;
        }
        workflowDao.update(workflow);
        createHistory(workflow.getUuid(), Hd4prcAction.MERGE, dataSet.getUuid());

        return Response.noContent().build();
    }

    private void mergeKmehr(Hd4prcWorkflow workflow, Hd4prcDataSet dataSet) {
        DataCollectionDefinitionDtoV7 dataCollectionDefinition = catalogueService.get( workflow.getDataCollectionDefinitionId());
        MappingResponse mappingResponse = mappingServiceHd4prc.mapFromBdsToJson(dataCollectionDefinition.getContent(), dataSet.getContent(), workflow.getLanguage());
        String uniqueID = mappingResponse.getUniqueID();
        Hd4prcIdentification identification = new Hd4prcIdentification();
        identification.setValue(getIdentificationValue(dataSet));
        if (StringUtils.isNoneBlank(uniqueID) && !isUnique(dataCollectionDefinition.getId(), uniqueID, identification)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.CREATE_DUPLICATE_KEY, uniqueID);
            throw exception;
        }
        workflow.setIdentification(identification);
        workflow.setContent(mappingResponse.getDocumentContent().toString().getBytes(StandardCharsets.UTF_8));
        workflow.setUniqueID(uniqueID);
        if (mappingResponse.getPrivateData() != null) {
            workflow.setPrivateContent(mappingResponse.getPrivateData().toString().getBytes(StandardCharsets.UTF_8));
        }
        if (mappingResponse.getPatientID() != null) {
            workflow.setPatientId(mappingResponse.getPatientID());
        }
        workflow.setCodedContent(mappingResponse.getCodedData());
    }

    private String getIdentificationValue(Hd4prcDataSet dataSet) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new ByteArrayInputStream(dataSet.getContent()));
            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();
            XPathExpression expr = xpath.compile(MappingRequest.XPATH_VALUE.IDENTIFICATION_VALUE);

            return Hd4prcUserService.getIdentification(expr.evaluate(doc, XPathConstants.STRING).toString());
        } catch (Exception e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.XML_PARSE_EXCEPTION);
            throw exception;
        }
    }

    private void mergeJson(Hd4prcWorkflow workflow, Hd4prcDataSet dataSet) {
        EFormsWorkflowDataSetDto workflowDto = JsonUnmarshaller.unmarshal(new String (dataSet.getContent(), StandardCharsets.UTF_8), EFormsWorkflowDataSetDto.class);
        String uniqueID = workflowDto.getUniqueID();
        if (StringUtils.isNoneBlank(uniqueID) && !isUnique(workflowDto.getDataCollectionDefinitionId(), uniqueID, workflow.getIdentification(), workflow.getUuid())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.CREATE_DUPLICATE_KEY, uniqueID);
            throw exception;
        }
        Hd4prcWorkflowMapper.map(workflow, workflowDto);
    }

    @Override
    public Response formats() {
        List<String> exportFormats = new ArrayList<>();
        exportFormats.add(ExportType.ADR.getApi());
        exportFormats.add(ExportType.JSON.getApi());
        exportFormats.add(ExportType.PDF.getApi());
        return Response.ok(exportFormats).build();
    }

    @Override
    @Transactional
    public Response exportJson(UriInfo info) {
        String uuid = RestUtils.getPathString(info, RestUtils.ParameterName.UUID);
        Hd4prcWorkflow workflow = getWorkflow(uuid);
        createHistory(workflow.getUuid(), Hd4prcAction.EXPORT_JSON);
        DataCollectionDefinitionDtoV7 dataCollectionDefinition = catalogueService.get(workflow.getDataCollectionDefinitionId());
        String csv = mappingService.mapFromJsonToCsv(workflow.getContent(), dataCollectionDefinition.getContent(), false);

        return Response.ok(new EFormsWorkflowDataSetDto(workflow, csv))
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }

    @Override
    @Transactional
    public Response exportAdr(UriInfo info) {
        String uuid = RestUtils.getPathString(info, RestUtils.ParameterName.UUID);
        Hd4prcWorkflow workflow = getWorkflow(uuid);
        DataCollectionDefinitionDtoV7 dataCollectionDefinition = catalogueService.get( workflow.getDataCollectionDefinitionId());
        ValidationResponse validationResponse = mappingService.getValidationResponse(workflow.getContent(), dataCollectionDefinition.getContent());
        if(!validationResponse.isSuccess()) {
            return ExceptionService.createValidationException(validationResponse, workflow, dataCollectionDefinition);
        }
        String adrXml;
        try {
            adrXml = IOUtils.toString(EFormsCreateRequestMapper.class.getResourceAsStream("/adr.xml"));
        } catch (IOException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, e.getMessage());
            throw exception;
        }
        CSVContent csvContent = createCsv(workflow);
        byte[] encodeBase64 = Base64.encodeBase64(csvContent.toString().getBytes(StandardCharsets.UTF_8));

        String response = StringUtils.replace(adrXml, "{content as BASE64}", new String(encodeBase64));
        createHistory(workflow.getUuid(), Hd4prcAction.EXPORT_ADR);
        return Response.ok(response)
                .type(MediaType.APPLICATION_XML_TYPE)
                .build();
    }

    private CSVContent createCsv(Hd4prcWorkflow workflow) {
        Hd4prcCreateAction action = new Hd4prcCreateAction();
        action.setHd4prcCreateDto(Hd4prcWorkflowMapper.convert(workflow));

        CSVContent csvContent = new CSVContent();
        csvContent.putWorkflowId(workflow.getUuid());
        Map<String, TypedValue> codedContent = workflow.getCodedContent();
        if(codedContent != null) {
            for (Map.Entry<String, TypedValue> codedEntry : codedContent.entrySet()) {
                csvContent.putField(codedEntry.getKey(), codedEntry.getValue().getValue(), codedEntry.getValue().getType());
            }
        }
        if (codedContent.isEmpty()) {
            csvContent.putPatientId(MessageFactory.NO_PATIENT_ID_DEFINED);
        }
        csvContent.setLastField(JsonMarshaller.marshalToByteArray(action));
        return csvContent;
    }


    @Override
    @Transactional
    public Response exportPdf(UriInfo info) {
        String uuid = RestUtils.getPathString(info, RestUtils.ParameterName.UUID);
        Hd4prcWorkflow workflow = getWorkflow(uuid);
        DataCollectionDefinitionDtoV7 dataCollectionDefinition = catalogueService.get(workflow.getDataCollectionDefinitionId());
        byte[] pdfTemplate = pdfTemplateForwardService.get(workflow.getDataCollectionDefinitionId(), workflow.getLanguage());
        JSONObject jsonObject = mappingServiceHd4prc.mapToElasticSearchDocument(dataCollectionDefinition.getContent(), workflow.getContent(), workflow.getPrivateContent());
        PdfSourceDocumentDto pdfSourceDocument = new PdfSourceDocumentDto(jsonObject);
        byte[] pdf = mappingServiceHd4prc.getPdf(pdfTemplate, JsonMarshaller.marshalToBytes(pdfSourceDocument));
        createHistory(workflow.getUuid(), Hd4prcAction.EXPORT_PDF);
        return Response.ok(pdf)
                .type(new MediaType("application", "pdf"))
                .build();
    }

    @Override
    public Response getWorkflow(UriInfo info) {
        String uuid = RestUtils.getPathString(info, RestUtils.ParameterName.UUID);
        Hd4prcWorkflow workflow = getWorkflow(getUuidOnly(uuid));
        return Response.ok(new Hd4prcWorkflowDto(workflow)).build();
    }

    private static String getUuidOnly(String uuid) {
        return StringUtils.substring(uuid, 0, 36);
    }

    private Hd4prcWorkflow getWorkflow(String uuid) {
        Hd4prcWorkflow workflow = workflowDao.get(uuid);
        if(workflow == null) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.EXPIRED);
            throw exception;
        }
        if(workflow.getReceivedOnHd4res()) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.EXPIRED);
            throw exception;
        }
        if(isExpired(workflow)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.EXPIRED);
            throw exception;
        }
        return workflow;
    }

    public boolean isExpired(Hd4prcWorkflow workflow) {
        Configuration configuration = configurationService.get(ConfigurationKey.HD4PRC_WORKFLOW_EXPIRATION_TIME);
        int timeout = Integer.valueOf(configuration.getValue());
        return isExpired(workflow.getUpdatedOn(), new Date(), timeout);
    }

    @Override
    @Transactional
    public Response save(JSONObject json) {
        EFormsSaveRequest eFormsSaveRequest;
        try {
            eFormsSaveRequest = EFormsSaveRequestMapper.map(json);
        } catch (JSONException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.JSON_PARSE_EXCEPTION);
            throw exception;
        }
        Hd4prcWorkflow workflow = workflowDao.get(eFormsSaveRequest.getUuid());
        if(workflow == null) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.EXPIRED);
            throw exception;
        }
        if(workflow.getReceivedOnHd4res()) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.EXPIRED);
            throw exception;
        }
        if(isExpired(workflow)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.EXPIRED);
            throw exception;
        }
        DataCollectionDefinitionDtoV7 dataCollectionDefinitionDtoV6 = dataCollectionDefinitionForwardService.get(workflow.getDataCollectionDefinitionId());
        String uniqueID = mappingServiceHd4prc.getUniqueID(dataCollectionDefinitionDtoV6.getContent(), eFormsSaveRequest.getContent(), eFormsSaveRequest.getPrivateContent());
        if (StringUtils.isNoneBlank(uniqueID) && !isUnique(workflow.getDataCollectionDefinitionId(), uniqueID, workflow.getIdentification(), workflow.getUuid())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.CREATE_DUPLICATE_KEY, uniqueID);
            throw exception;
        }
        workflow.setContent(eFormsSaveRequest.getContent());
        workflow.setUniqueID(uniqueID != null ? uniqueID : RegistrationWorkflow.NO_UNIQUE_ID);
        workflow.setPrivateContent(eFormsSaveRequest.getPrivateContent());
        workflow.setPatientId(eFormsSaveRequest.getPatientId());
        workflow = workflowDao.update(workflow);
        createHistory(workflow.getUuid(), Hd4prcAction.SAVE);
        return Response.ok(new Hd4prcWorkflowDto(workflow)).build();
    }

    private boolean isExpired(Date lastUpdated, Date now, int expireTime) {
        Calendar expireCalendar = Calendar.getInstance();
        expireCalendar.setTime(lastUpdated);
        expireCalendar.add(Calendar.MINUTE, expireTime);

        Calendar nowCalendar = Calendar.getInstance();
        nowCalendar.setTime(now);
        return expireCalendar.before(nowCalendar);
    }

    private boolean isUnique(long dataCollectionDefinitionId, String uniqueId, Hd4prcIdentification identification) {
        Hd4prcWorkflow unique = getUnique(dataCollectionDefinitionId, uniqueId, identification);
        return unique == null;
    }


    private boolean isUnique(long dataCollectionDefinitionId, String uniqueId, Hd4prcIdentification identification, String uuid) {
        Hd4prcWorkflow unique = getUnique(dataCollectionDefinitionId, uniqueId, identification);
        if(unique == null) {
            return true;
        } else {
            return uuid.equals(unique.getUuid());
        }
    }

    private Hd4prcWorkflow getUnique(long dataCollectionDefinitionId, String uniqueId, Hd4prcIdentification identification) {
        Hd4prcWorkflowSearch search = new Hd4prcWorkflowSearch();
        search.setDataCollectionDefinitionId(dataCollectionDefinitionId);
        search.setUniqueId(uniqueId);
        search.setIdentification(identification);
        List<Hd4prcWorkflow> workflows = workflowDao.getAll(search);
        if(workflows.isEmpty()) {
            return null;
        } else {
            for (Hd4prcWorkflow workflow : workflows) {
                if(!isExpired(workflow)) {
                    return workflow;
                }
            }
        }
        return null;
    }

    private void createHistory(String workflowUuid, Hd4prcAction action) {
        createHistory(workflowUuid, action, null);
    }

    private void createHistory(String workflowUuid, Hd4prcAction action, String message) {
        Hd4prcWorkflowHistory history = new Hd4prcWorkflowHistory();
        history.setWorkflowUuid(workflowUuid);
        history.setAction(action);
        history.setMessage(message);
        historyDao.create(history);
    }

    @Override
    public ADR getAdr(User user, Long workflowId) {
        RegistrationWorkflow workflow = workflowService.get(workflowId);

        MailDto mail = mailService.createMail(TemplateKey.REGISTRATION_UPDATES_DMA_REP, getContext(workflow));

        ADR adr = new  ADR();
        List<Addressee> adressees = new ArrayList<>();
        Addressee adressee = new Addressee();
        adressee.setQuality("DOCTOR");
        adressee.setIdentifier(user.getUsername());
        adressee.setEhboxIdType("NIHII");
        adressees.add(adressee);
        adr.setAddressee(adressees);

        adr.setSubject(mail.getSubject());

        Patient patient = new Patient();
        Map<String, String> patientId = workflow.getMetaData();
        patient.setINSS(patientId.get("PATIENT_IDENTIFIER"));
        adr.setPatient(patient);

        List<Attachment> attachments = new ArrayList<>();
        Attachment attachment = new Attachment();
        attachment.setName("name");
        attachment.setContent(Base64.decodeBase64(mail.getText()));
        attachment.setMimeType("plain/text");
        attachment.setFunctionalType("dma-rep");
        attachments.add(attachment);
        adr.setAttachment(attachments);

        return adr;
    }

    private RegistrationUpdatesContext getContext(RegistrationWorkflow workflow) {
        final RegistrationUpdatesContext context = new RegistrationUpdatesContext();
        context.setInstallationUrl(configurationService.get(ConfigurationKey.GUI_HOST).getValue());

        final RegistrationUpdatesContext.RegistrationDetails details = new RegistrationUpdatesContext.RegistrationDetails();
        details.setId(workflow.getId());
        details.setLink(DocumentUrl.createUrl(configurationService.get(ConfigurationKey.GUI_HOST).getValue(), workflow.getId()));
        details.setStatuses(WorkflowStatusMapper.map(workflow.getStatus(), workflow.getFlags()));
        details.setOpenSince(DateFormat.DDMMYYYY.format(workflow.getUpdatedOn()));
        context.add(workflow.getDataCollectionName(), details);

        return context;
    }


}
