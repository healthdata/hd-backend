/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4prc.mapper;

import be.wiv_isp.healthdata.orchestration.domain.TypedValue;
import be.wiv_isp.healthdata.hd4prc.domain.Hd4prcWorkflow;
import be.wiv_isp.healthdata.hd4prc.domain.Integrator;
import be.wiv_isp.healthdata.hd4prc.dto.EFormsWorkflowDataSetDto;
import be.wiv_isp.healthdata.hd4prc.dto.TypedValueDto;
import be.wiv_isp.healthdata.orchestration.action.dto.Hd4prcCreateDto;
import be.wiv_isp.healthdata.orchestration.dto.IntegratorDto;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Hd4prcWorkflowMapper {

    static public void map(Hd4prcWorkflow workflow, EFormsWorkflowDataSetDto workflowDto) {
        workflow.setContent(workflowDto.getContent());
        workflow.setUniqueID(workflowDto.getUniqueID());
        workflow.setPrivateContent(workflowDto.getPrivateContent());
        Map<String, TypedValueDto> codedContentDto = workflowDto.getCodedContent();
        if(codedContentDto != null) {
            Map<String, TypedValue> codedContent = new HashMap<>();
            for (Map.Entry<String, TypedValueDto> entry : codedContentDto.entrySet()) {
                TypedValue typedValue = new TypedValue();
                typedValue.setType(entry.getValue().getType());
                typedValue.setValue(entry.getValue().getValue());
                codedContent.put(entry.getKey(), typedValue);
            }
            workflow.setCodedContent(codedContent);
        }
        workflow.setPatientId(workflowDto.getPatientId());
    }

    static public Hd4prcCreateDto convert(Hd4prcWorkflow workflow) {
        Hd4prcCreateDto dto = new Hd4prcCreateDto();
        dto.setDataCollectionName(workflow.getDataCollectionName());
        dto.setDataCollectionDefinitionId(workflow.getDataCollectionDefinitionId());
        dto.setIdentification(workflow.getIdentification().getValue());
        dto.setIntegrator(convert(workflow.getIntegrator()));
        dto.setContent(workflow.getContent());
        dto.setCreatedOn(workflow.getCreatedOn());
        dto.setSubmittedOn(new Timestamp(new Date().getTime()));
        return dto;
    }

    static private IntegratorDto convert(Integrator integrator) {
        IntegratorDto converted = new IntegratorDto();
        converted.setName(integrator.getName());
        converted.setVersion(integrator.getVersion());
        return converted;
    }
}
