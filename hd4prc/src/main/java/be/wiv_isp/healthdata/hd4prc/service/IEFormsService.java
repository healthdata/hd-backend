/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4prc.service;

import be.healthconnect.adr._1_0.ADR;
import be.wiv_isp.healthdata.hd4prc.enumeration.DataSetFormat;
import be.wiv_isp.healthdata.orchestration.domain.User;
import org.codehaus.jettison.json.JSONObject;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

public interface IEFormsService {

    Response create(String languageHeader, JSONObject json);

    Response upload(UriInfo info, String dataSet, DataSetFormat format);

    Response merge(UriInfo info, JSONObject jsonObject);

    Response save(JSONObject json);

    Response formats();

    Response exportJson(UriInfo info);

    Response exportPdf(UriInfo info);

    Response exportAdr(UriInfo info);

    Response getWorkflow(UriInfo info);

    ADR getAdr(User user, Long workflowId);

}
