/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4prc.service.impl;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.hd4prc.domain.Hd4prcWorkflow;
import be.wiv_isp.healthdata.orchestration.dto.ValidationResponse;
import org.apache.http.HttpStatus;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.util.UUID;

public class ExceptionService {

    private static final Logger LOG = LoggerFactory.getLogger(ExceptionService.class);

    class Code {
        final private static String INPUT_VALIDATION_ERROR = "input_validation_error";
    }

    class Message {
        final private static String INPUT_VALIDATION_ERROR = "The input contains validation errors.";
    }

    class Description {
        final private static String INPUT_VALIDATION_ERROR = "Check the error messages contained in this response to correct your input and try again. This error is entirely related to your input. Do not retry without changing the input.";
    }

    static Response createValidationException(ValidationResponse validationResponse, Hd4prcWorkflow workflow, DataCollectionDefinitionDtoV7 dataCollectionDefinition) {
            String uuid = UUID.randomUUID().toString();
        try {
            LOG.warn("UniqueExceptionId[{}]: Validation Exception", uuid);
            LOG.warn("UniqueExceptionId[{}]: ValidationResult [{}]", uuid, validationResponse);
            LOG.warn("UniqueExceptionId[{}]: Workflow [{}]", uuid, workflow);
            LOG.warn("UniqueExceptionId[{}]: DCD [{}]", uuid, dataCollectionDefinition);

            JSONObject response = new JSONObject();
            response.put("code", Code.INPUT_VALIDATION_ERROR);
            response.put("message", Message.INPUT_VALIDATION_ERROR);
            response.put("description", Description.INPUT_VALIDATION_ERROR);
            response.put("uniqueId", uuid);
            response.put("validationErrors", validationResponse.getResult());

            return Response.status(HttpStatus.SC_UNPROCESSABLE_ENTITY)
                    .entity(response.toString())
                    .build();
        } catch (JSONException e) {
            LOG.error("UniqueExceptionId[{}]: JSON exception", uuid);
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.JSON_EXCEPTION, Code.INPUT_VALIDATION_ERROR);
            throw exception;
        }
    }
}
