/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4prc.domain.search;

import be.wiv_isp.healthdata.common.domain.search.EntitySearch;

import java.util.Objects;

public class Hd4prcWorkflowHistorySearch extends EntitySearch{

    private String workflowUuid;

    public String getWorkflowUuid() {
        return workflowUuid;
    }

    public void setWorkflowUuid(String workflowUuid) {
        this.workflowUuid = workflowUuid;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof Hd4prcWorkflowHistorySearch) {
            Hd4prcWorkflowHistorySearch other = (Hd4prcWorkflowHistorySearch) o;

            return Objects.equals(workflowUuid, other.workflowUuid);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                this.workflowUuid);
    }
}
