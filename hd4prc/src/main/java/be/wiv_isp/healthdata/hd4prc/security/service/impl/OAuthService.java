/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.hd4prc.security.service.impl;

import be.wiv_isp.healthdata.common.api.exception.UnauthorizedException;
import be.wiv_isp.healthdata.hd4prc.security.domain.ShibbolethAttribute;
import be.wiv_isp.healthdata.hd4prc.security.mapper.ShibbolethAttributeMapper;
import be.wiv_isp.healthdata.hd4prc.security.service.IOAuthService;
import be.wiv_isp.healthdata.hd4prc.security.service.ITokenService;
import be.wiv_isp.healthdata.hd4prc.security.service.IUserDetailsFactory;
import be.wiv_isp.healthdata.orchestration.domain.User;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;
import java.util.Map;

@Service
public class OAuthService implements IOAuthService {

    private static final Logger LOG = LoggerFactory.getLogger(OAuthService.class);

    @Autowired
    private ITokenService tokenService;

    @Autowired
    private IUserDetailsFactory userDetailsFactory;

    @Override
    public OAuth2AccessToken login(final HttpServletRequest request) {
        LOG.debug("Processing login request");
        final Map<ShibbolethAttribute, String> attributes = ShibbolethAttributeMapper.extractShibbolethAttributes(request);
        final User user = userDetailsFactory.createUser(attributes);
        return tokenService.generateAccessToken(user);
    }

    @Override
    public OAuth2AccessToken refresh(final String grantType, String clientId, final String refreshTokenValue) {
        return tokenService.refreshToken(grantType, clientId, refreshTokenValue);
    }

    @Override
    public void logout(final HttpServletRequest request, HttpServletResponse response) {
        LOG.debug("Processing logout request");

        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            final String warning = "Could not retrieve authentication from security context";
            LOG.warn(warning);
            throw new UnauthorizedException(warning);
        }

        final String authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        if(authorizationHeader == null) {
            final String warning = "No bearer token provided";
            LOG.warn(warning);
            throw new UnauthorizedException(warning);
        }

        // invalidate session
        new SecurityContextLogoutHandler().logout(request, response, authentication);

        // revoke access token
        final String accessToken = authorizationHeader.replace("Bearer ", StringUtils.EMPTY);
        tokenService.revokeToken(accessToken);
    }
}
