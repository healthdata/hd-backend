/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4prc.dto;

import be.wiv_isp.healthdata.common.util.StringUtils;

import java.util.Objects;

public class EFormsMergeRequest {

    private String dataSetId;

    private String mergeStrategy;

    public String getDataSetId() {
        return dataSetId;
    }

    public void setDataSetId(String dataSetId) {
        this.dataSetId = dataSetId;
    }

    public String getMergeStrategy() {
        return mergeStrategy;
    }

    public void setMergeStrategy(String mergeStrategy) {
        this.mergeStrategy = mergeStrategy;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof EFormsMergeRequest) {
            EFormsMergeRequest other = (EFormsMergeRequest) o;

            return Objects.equals(dataSetId, other.dataSetId)
                    && Objects.equals(mergeStrategy, other.mergeStrategy);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                this.dataSetId,
                this.mergeStrategy);
    }

    @Override
    public String toString() {
        return StringUtils.toString(this);
    }
}
