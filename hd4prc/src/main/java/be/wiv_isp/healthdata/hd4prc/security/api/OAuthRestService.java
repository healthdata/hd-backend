/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.hd4prc.security.api;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.hd4prc.security.service.IOAuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.text.MessageFormat;

@Component
@Path("/uam/oauth")
public class OAuthRestService {

    private static final Logger LOG = LoggerFactory.getLogger(OAuthRestService.class);

    @Context
    private HttpServletRequest httpRequest;

    @Context
    private HttpServletResponse httpResponse;

    @Autowired
    private IOAuthService oAuthService;

    @Autowired
    private IConfigurationService configurationService;

    @GET
    @Path("/token")
    public Response login() {
        OAuth2AccessToken accessToken;
        try {
            accessToken = oAuthService.login(httpRequest);
        } catch(Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
        }
        final String guiHost = configurationService.get(ConfigurationKey.GUI_HOST).getValue();
        final String redirectUrl = MessageFormat.format("{0}/#/eid?accessToken={1}&refreshToken={2}", guiHost, accessToken.getValue(), accessToken.getRefreshToken().getValue());
        return Response.status(302).location(URI.create(redirectUrl)).build();
    }

    @POST
    @Path("/token")
    @Produces(MediaType.APPLICATION_JSON)
    public Response refresh(@Context UriInfo uriInfo) {
        LOG.info("Asking a new access token using an existing refresh token");

        final String grantType = RestUtils.getParameterString(uriInfo, "grant_type");
        final String clientId = RestUtils.getParameterString(uriInfo, "client_id");
        final String refreshToken = RestUtils.getParameterString(uriInfo, "refresh_token");

        final OAuth2AccessToken oAuth2AccessToken = oAuthService.refresh(grantType, clientId, refreshToken);
        return Response.ok(oAuth2AccessToken).build();
    }

    @GET
    @Path("/logout")
    public Response logout() {
        oAuthService.logout(httpRequest, httpResponse);
        final String redirectUrl = configurationService.get(ConfigurationKey.SHIBBOLETH_LOGOUT_ENDPOINT).getValue();
        return Response.status(302).location(URI.create(redirectUrl)).build();
    }
}
