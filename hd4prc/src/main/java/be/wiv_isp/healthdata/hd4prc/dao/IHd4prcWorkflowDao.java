package be.wiv_isp.healthdata.hd4prc.dao;

import be.wiv_isp.healthdata.common.dao.ICrudDaoV2;
import be.wiv_isp.healthdata.hd4prc.domain.Hd4prcWorkflow;
import be.wiv_isp.healthdata.hd4prc.domain.search.Hd4prcWorkflowSearch;

public interface IHd4prcWorkflowDao extends ICrudDaoV2<Hd4prcWorkflow, String, Hd4prcWorkflowSearch> {

    long count(Hd4prcWorkflowSearch search);

}
