/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4prc.domain;

import be.wiv_isp.healthdata.common.util.StringUtils;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Objects;

@Embeddable
public class Hd4prcIdentification {

	@Column(name = "HD4PRC_IDENTIFICATION_VALUE")
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof Hd4prcIdentification) {
			Hd4prcIdentification other = (Hd4prcIdentification) o;

			return Objects.equals(value, other.value);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				this.value);
	}

	@Override
	public String toString() {
		return StringUtils.toString(this);
	}
}
