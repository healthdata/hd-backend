/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4prc.service;

import be.wiv_isp.healthdata.common.domain.enumeration.Language;
import be.wiv_isp.healthdata.orchestration.mapping.MappingResponse;
import org.codehaus.jettison.json.JSONObject;

public interface IHd4prcMappingService {

    MappingResponse mapFromBdsToJson(byte[] dcdContent, byte[] content, Language language);

    JSONObject mapToElasticSearchDocument(byte[] dcdContent, byte[] documentContent, byte[] getPrivateContent);

    String getUniqueID(byte[] dcdContent, byte[] documentContent, byte[] getPrivateContent);

    byte[] getPdf(byte[] templateContent, byte[] esDocument);

}
