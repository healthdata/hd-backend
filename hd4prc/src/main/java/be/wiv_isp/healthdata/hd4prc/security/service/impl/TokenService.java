/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.hd4prc.security.service.impl;

import be.wiv_isp.healthdata.common.api.exception.BadRequestException;
import be.wiv_isp.healthdata.hd4prc.security.service.ITokenService;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.security.oauth2.OrganizationUsernamePasswordAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Service
public class TokenService implements ITokenService {

    private static final Logger LOG = LoggerFactory.getLogger(TokenService.class);

    @Autowired
    private DefaultTokenServices tokenServices;

    @Autowired
    private ClientDetailsService clientDetailsService;

    @Override
    public OAuth2AccessToken generateAccessToken(User user) {
        LOG.debug("Creating access token for user [{}]", user.getUsername());
        final OAuth2Request clientAuthentication = createClientAuthentication();
        final Authentication userAuthentication = new OrganizationUsernamePasswordAuthenticationToken(user.getOrganization().getId(), user, null, user.getAuthorities());
        final OAuth2AccessToken accessToken = tokenServices.createAccessToken(new OAuth2Authentication(clientAuthentication, userAuthentication));
        LOG.debug("Access token created");
        return accessToken;
    }

    @Override
    public OAuth2AccessToken refreshToken(final String grantType, final String clientId, final String refreshTokenValue) {
        LOG.debug("Refreshing access token");

        final ClientDetails clientDetails = getClientDetails();
        if (!clientDetails.getClientId().equals(clientId)) {
            throw new BadRequestException("Invalid client id");
        }

        final Map<String, String> requestParameters = new HashMap<>();
        requestParameters.put("grant_type" , grantType);
        requestParameters.put("refresh_token", refreshTokenValue);
        requestParameters.put("client_id", clientId);
        final TokenRequest tokenRequest = new TokenRequest(
                requestParameters,
                clientDetails.getClientId(),
                clientDetails.getScope(),
                grantType);

        return tokenServices.refreshAccessToken(refreshTokenValue, tokenRequest);
    }

    @Override
    public void revokeToken(String accessToken) {
        LOG.debug("Revoking access token");
        tokenServices.revokeToken(accessToken);
    }

    private OAuth2Request createClientAuthentication() {
        final ClientDetails clientDetails = getClientDetails();
        final String clientId = clientDetails.getClientId();
        final Collection<GrantedAuthority> authorities = clientDetails.getAuthorities();
        final Set<String> scope = clientDetails.getScope();

        return new OAuth2Request(null, clientId, authorities, true, scope, null, null, null, null);
    }

    private ClientDetails getClientDetails() {
        return clientDetailsService.loadClientByClientId("healthdata-client");
    }

}
