/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4prc.api;

import be.wiv_isp.healthdata.common.api.ClientVersion;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.hd4prc.enumeration.DataSetFormat;
import be.wiv_isp.healthdata.hd4prc.service.IEFormsService;
import be.wiv_isp.healthdata.orchestration.api.uri.DataCollectionDefinitionUri;
import be.wiv_isp.healthdata.orchestration.api.uri.DataReferenceUri;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IDataReferenceForwardService;
import be.wiv_isp.healthdata.orchestration.service.IWebServiceClientService;
import com.sun.jersey.api.client.GenericType;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Component
@Path("/cloud")
public class EFormsRestService {

    @Resource(name = "eFormsService")
    private IEFormsService eFormsService;


    @Autowired
    private IWebServiceClientService webServiceClientService;
    @Autowired
    private IConfigurationService configurationService;
    @Autowired
    private IDataReferenceForwardService dataReferenceService;

    @GET
    @Path("/workflows/{uuid}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getWorkflow(@Context UriInfo info) {
        return  eFormsService.getWorkflow(info);
    }

    @POST
    @Path("/workflows/actions")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response executeActions(final String json) throws JSONException {
        return  eFormsService.save(new JSONObject(json));
    }

    //TODO should be removed once the frontend is up to date
    @GET
    @Path("/datacollectiondefinitions/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@HeaderParam("ClientVersion") String clientVersion, @Context UriInfo info) {
        Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

        final DataCollectionDefinitionUri dataCollectionDefinitionUri = new DataCollectionDefinitionUri();
        dataCollectionDefinitionUri.setHost(catalogueHost.getValue());
        dataCollectionDefinitionUri.setId(getId(info));

        WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.addHeader(HttpHeaders.CLIENT_VERSION, ClientVersion.DataCollectionDefinition.getDefault());
        wsb.setUrl(dataCollectionDefinitionUri.toString());
        wsb.setGet(true);
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.setReturnType(new GenericType<JSONObject>() {});
        wsb.setLogResponseBody(false);

        JSONObject json = (JSONObject) webServiceClientService.callWebService(wsb);
        return Response.ok(json.toString()).build();
    }

    private Long getId(UriInfo info) {
        return RestUtils.getPathLong(info, RestUtils.ParameterName.ID);
    }

    @GET
    @Path("/datareference")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@Context UriInfo info) {
        Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

        final DataReferenceUri dataReferenceUri = new DataReferenceUri();
        dataReferenceUri.setHost(catalogueHost.getValue());

        WebServiceBuilder wsb = new WebServiceBuilder();
        MultivaluedMap<String, String> queryParameters = info.getQueryParameters();
        for (String queryParameterkey : queryParameters.keySet()) {
            wsb.addParameter(queryParameterkey, queryParameters.getFirst(queryParameterkey));
        }

        wsb.setUrl(dataReferenceUri.toString());
        wsb.setGet(true);
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.setReturnType(new GenericType<String>() {
        });

        String json = dataReferenceService.getDataReference(wsb);
        return Response.ok(json).build();
    }

    @POST
    @Path("/api/v1/forms")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response create(@HeaderParam("Accept-Language") String languageHeader, String json) throws JSONException {
        return  eFormsService.create(languageHeader, new JSONObject(json));
    }

    @POST
    @Path("/api/v1/forms/{uuid}/data-set")
    @Consumes("application/vnd.healthconnect.eforms.kmehr.integrator.v1+xml;charset=UTF-8")
    @Produces(MediaType.TEXT_PLAIN)
    public Response uploadKmehr(@Context UriInfo info, String kmehr) {
        return  eFormsService.upload(info, kmehr, DataSetFormat.EFORMS_KMEHR);
    }

    @POST
    @Path("/api/v1/forms/{uuid}/data-set")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response uploadJson(@Context UriInfo info, String json) {
        return  eFormsService.upload(info, json, DataSetFormat.HEALTHDATA_JSON);
    }

    @POST
    @Path("/api/v1/forms/{uuid}/merge")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response merge(@Context UriInfo info, String json) throws JSONException {
        return  eFormsService.merge(info, new JSONObject(json));
    }

    @GET
    @Path("/api/v1/forms/{uuid}/formats")
    @Produces(MediaType.APPLICATION_JSON)
    public Response formats() {
        return  eFormsService.formats();
    }

    @GET
    @Path("/api/v1/forms/{uuid}/data-set")
    @Produces(MediaType.APPLICATION_JSON)
    public Response exportDataSet(@Context UriInfo info) {
        return  eFormsService.exportJson(info);
    }

    @GET
    @Path("/api/v1/forms/{uuid}/form-data")
    @Produces(MediaType.APPLICATION_JSON)
    public Response exportFormData(@Context UriInfo info) {
        return  eFormsService.exportJson(info);
    }

    @GET
    @Path("/api/v1/forms/{uuid}/pdf")
    @Produces("application/pdf")
    public Response exportPdf(@Context UriInfo info) {
        return  eFormsService.exportPdf(info);
    }

    @GET
    @Path("/api/v1/forms/{uuid}/adr")
    @Produces(MediaType.APPLICATION_XML)
    public Response exportAdr(@Context UriInfo info) {
        return  eFormsService.exportAdr(info);
    }

    @GET
    @Path("/api/v1/forms/{uuid}/json")
    @Produces(MediaType.APPLICATION_JSON)
    public Response exportJson(@Context UriInfo info) {
        return  eFormsService.exportJson(info);
    }
}
