/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.hd4prc.security.service.impl;

import be.wiv_isp.healthdata.common.api.exception.UnauthorizedException;
import be.wiv_isp.healthdata.hd4prc.security.domain.ShibbolethAttribute;
import be.wiv_isp.healthdata.hd4prc.security.service.IUserDetailsFactory;
import be.wiv_isp.healthdata.hd4prc.service.IHd4prcUserService;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;

@Service
public class UserDetailsFactory implements IUserDetailsFactory {

    private static final Logger LOG = LoggerFactory.getLogger(UserDetailsFactory.class);

    @Autowired
    private IHd4prcUserService hd4prcUserService;

    @Override
    public User createUser(final Map<ShibbolethAttribute, String> attributes) {
        LOG.debug("Creating user from Shibboleth attributes");
        performAuthorizationChecks(attributes);
        final User user = create(attributes);
        final User created = hd4prcUserService.getOrCreate(user);
        LOG.debug("Created user: [{}]", created);
        return created;
    }

    protected void performAuthorizationChecks(Map<ShibbolethAttribute, String> attributes) {
        LOG.debug("Performing authorization checks");
        verifyAttribute(attributes, ShibbolethAttribute.GENERAL_AUTH_DECISION, "Permit");
    }

    protected void verifyAttribute(Map<ShibbolethAttribute, String> attributes, ShibbolethAttribute attribute, String expectedValue) {
        LOG.debug("Verifying value for attribute [{}] is equal to [{}]", attribute, expectedValue);
        final String attributeValue = attributes.get(attribute);
        if(!expectedValue.equals(attributeValue)) {
            LOG.warn("Expected value for attribute [{}] is [{}]. Actual value: [{}]", attribute, expectedValue, attributeValue);
            throw new UnauthorizedException();
        }
    }

    private User create(final Map<ShibbolethAttribute, String> attributes) {
        final User user = new User();
        user.setUsername(attributes.get(ShibbolethAttribute.DOCTOR_NIHII));
        user.setLastName(attributes.get(ShibbolethAttribute.PERSON_LASTNAME));
        user.setFirstName(attributes.get(ShibbolethAttribute.PERSON_FIRSTNAME));
        user.setAuthorities(new HashSet<>(Collections.singletonList(new Authority(Authority.USER))));
        user.setEnabled(true);
        user.setLdapUser(false);
        return user;
    }
}
