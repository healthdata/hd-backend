/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4prc.dto;

import be.wiv_isp.healthdata.common.dto.VersionDto;
import be.wiv_isp.healthdata.common.util.StringUtils;

import java.util.Objects;

public class EFormsCreateRequest {

    private String name;

    private VersionDto version;

    private String format;

    private String integratorName;

    private String integratorVersion;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VersionDto getVersion() {
        return version;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getIntegratorName() {
        return integratorName;
    }

    public void setIntegratorName(String integratorName) {
        this.integratorName = integratorName;
    }

    public String getIntegratorVersion() {
        return integratorVersion;
    }

    public void setIntegratorVersion(String integratorVersion) {
        this.integratorVersion = integratorVersion;
    }

    public void setVersion(String version) {
        VersionDto versionDto = new VersionDto();
        String[] splitVersion = version.split("\\.");
        versionDto.setMajor(Integer.valueOf(splitVersion[0]));
        versionDto.setMinor(Integer.valueOf(splitVersion[1]));
        this.version = versionDto;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof EFormsCreateRequest) {
            EFormsCreateRequest other = (EFormsCreateRequest) o;

            return Objects.equals(name, other.name)
                    && Objects.equals(version, other.version)
                    && Objects.equals(format, other.format)
                    && Objects.equals(integratorName, other.integratorName)
                    && Objects.equals(integratorVersion, other.integratorVersion);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                this.name,
                this.version,
                this.format,
                this.integratorName,
                this.integratorVersion);
    }

    @Override
    public String toString() {
        return StringUtils.toString(this);
    }
}
