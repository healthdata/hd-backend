/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4prc.domain;

import be.wiv_isp.healthdata.orchestration.domain.TypedValue;
import be.wiv_isp.healthdata.common.domain.enumeration.Language;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.*;

@Entity
@Table(name = "HD4PRC_WORKFLOWS")
public class Hd4prcWorkflow {

    @Id
    @Column(name = "UUID", nullable = false)
    private String uuid;
    @Column(name = "DATA_COLLECTION_NAME", nullable = false)
    private String dataCollectionName;
    @Column(name = "DATA_COLLECTION_DEFINITION_ID", nullable = false)
    private Long dataCollectionDefinitionId;
    @Embedded
    private Hd4prcIdentification identification;
    @Embedded
    private Integrator integrator;
    @Column(name = "FORMAT", nullable = false)
    private String format;
    @Lob
    @Column(name = "CONTENT")
    private byte[] content;
    @Fetch(FetchMode.JOIN)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "HD4PRC_CODED_DOCUMENT_CONTENT", joinColumns = @JoinColumn(name = "WF_UUID"))
    @MapKeyColumn(name = "LOOKUP_KEY")
    private Map<String, TypedValue> codedContent;
    @Fetch(FetchMode.JOIN)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "HD4PRC_PATIENT_ID", joinColumns = @JoinColumn(name = "WF_UUID"))
    @MapKeyColumn(name = "LOOKUP_KEY")
    @Column(name = "VALUE")
    private Map<String, String> patientId;
    @Lob
    @Column(name = "PRIVATE_CONTENT")
    private byte[] privateContent;
    @Column(name = "LANGUAGE", nullable = false)
    @Enumerated(EnumType.STRING)
    private Language language;
    @Column(name = "UNIQUE_ID")
    private String uniqueID;
    @Column(name = "RECEIVED", nullable = false)
    private Boolean receivedOnHd4res;
    @Column(name = "CREATED_ON", nullable = false)
    private Timestamp createdOn;
    @Column(name = "UPDATED_ON", nullable = false)
    private Timestamp updatedOn;
    @Column(name = "SUBMITTED_ON")
    private Timestamp submittedOn;

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public Map<String, TypedValue> getCodedContent() {
        return codedContent;
    }

    public void setCodedContent(Map<String, TypedValue> codedContent) {
        this.codedContent = codedContent;
    }

    public Map<String, String> getPatientId() {
        return patientId;
    }

    public void setPatientId(Map<String, String> patientId) {
        this.patientId = patientId;
    }

    public byte[] getPrivateContent() {
        return privateContent;
    }

    public void setPrivateContent(byte[] privateContent) {
        this.privateContent = privateContent;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Long getDataCollectionDefinitionId() {
        return dataCollectionDefinitionId;
    }

    public void setDataCollectionDefinitionId(Long dataCollectionDefinitionId) {
        this.dataCollectionDefinitionId = dataCollectionDefinitionId;
    }

    public String getDataCollectionName() {
        return dataCollectionName;
    }

    public void setDataCollectionName(String dataCollectionName) {
        this.dataCollectionName = dataCollectionName;
    }

    public Hd4prcIdentification getIdentification() {
        return identification;
    }

    public void setIdentification(Hd4prcIdentification identification) {
        this.identification = identification;
    }

    public Integrator getIntegrator() {
        return integrator;
    }

    public void setIntegrator(Integrator integrator) {
        this.integrator = integrator;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Boolean getReceivedOnHd4res() {
        return receivedOnHd4res;
    }

    public void setReceivedOnHd4res(Boolean receivedOnHd4res) {
        this.receivedOnHd4res = receivedOnHd4res;
    }

    public Timestamp getSubmittedOn() {
        return submittedOn;
    }

    public void setSubmittedOn(Timestamp submittedOn) {
        this.submittedOn = submittedOn;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(String uniqueID) {
        this.uniqueID = uniqueID;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @PrePersist
    public void onCreate() {
        if (StringUtils.isBlank(uuid)) {
            uuid = UUID.randomUUID().toString();
        }
        Timestamp createdOnTimestamp = new Timestamp(new Date().getTime());
        createdOn = createdOnTimestamp;
        updatedOn = createdOnTimestamp;
    }

    @PreUpdate
    public void onUpdate() {
        updatedOn = new Timestamp(new Date().getTime());
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof Hd4prcWorkflow) {
            Hd4prcWorkflow other = (Hd4prcWorkflow) o;

            return Objects.equals(uuid, other.uuid)
                    && Objects.equals(dataCollectionName, other.dataCollectionName)
                    && Objects.equals(dataCollectionDefinitionId, other.dataCollectionDefinitionId)
                    && Objects.equals(identification, other.identification)
                    && Objects.equals(language, other.language)
                    && Objects.equals(uniqueID, other.uniqueID)
                    && Objects.equals(receivedOnHd4res, other.receivedOnHd4res)
                    && Objects.equals(createdOn, other.createdOn)
                    && Objects.equals(updatedOn, other.updatedOn)
                    && Objects.equals(submittedOn, other.submittedOn);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                this.uuid,
                this.dataCollectionName,
                this.dataCollectionDefinitionId,
                this.identification,
                this.language,
                this.uniqueID,
                this.receivedOnHd4res,
                this.createdOn,
                this.updatedOn,
                this.submittedOn);
    }

    @Override
    public String toString() {
        return be.wiv_isp.healthdata.common.util.StringUtils.toString(this);
    }

}
