/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4prc.domain;

import be.wiv_isp.healthdata.hd4prc.enumeration.DataSetFormat;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "HD4PRC_DATA_SET")
public class Hd4prcDataSet {

    @Id
    @Column(name = "UUID", nullable = false)
    private String uuid;
    @Column(name = "WF_UUID", nullable = false)
    private String workflowUuid;
    @Column(name = "FORMAT", nullable = false)
    @Enumerated(EnumType.STRING)
    private DataSetFormat format;
    @Lob
    @Column(name = "CONTENT", nullable = false)
    private byte[] content;
    @Column(name = "CREATED_ON", nullable = false)
    private Timestamp createdOn;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getWorkflowUuid() {
        return workflowUuid;
    }

    public void setWorkflowUuid(String workflowUuid) {
        this.workflowUuid = workflowUuid;
    }

    public DataSetFormat getFormat() {
        return format;
    }

    public void setFormat(DataSetFormat format) {
        this.format = format;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    @PrePersist
    public void onCreate() {
        if (StringUtils.isBlank(uuid)) {
            uuid = UUID.randomUUID().toString();
        }
        createdOn = new Timestamp(new Date().getTime());
    }
}
