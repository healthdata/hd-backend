/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4prc.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.hd4prc.dao.IHd4prcWorkflowDao;
import be.wiv_isp.healthdata.hd4prc.domain.Hd4prcIdentification;
import be.wiv_isp.healthdata.hd4prc.domain.Hd4prcWorkflow;
import be.wiv_isp.healthdata.hd4prc.domain.search.Hd4prcWorkflowSearch;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class Hd4prcWorkflowDao extends CrudDaoV2<Hd4prcWorkflow, String, Hd4prcWorkflowSearch> implements IHd4prcWorkflowDao {

    public Hd4prcWorkflowDao() {
        super(Hd4prcWorkflow.class);
    }

    @Override
    public long count(Hd4prcWorkflowSearch search) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Hd4prcWorkflow> rootEntry = cq.from(entityClass);

        List<Predicate> predicates = getPredicates(search, cb, rootEntry);

        if (!predicates.isEmpty()) {
            cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
        }
        CriteriaQuery<Long> count = cq.select(cb.countDistinct(rootEntry));
        TypedQuery<Long> countQuery = em.createQuery(count);
        return countQuery.getSingleResult();
    }


    @Override
    public List<Predicate> getPredicates(Hd4prcWorkflowSearch search, CriteriaBuilder cb, Root<Hd4prcWorkflow> rootEntry) {
        List<Predicate> predicates = new ArrayList<>();
        if (search.getDataCollectionDefinitionId() != null) {
            predicates.add(cb.equal(rootEntry.get("dataCollectionDefinitionId"), search.getDataCollectionDefinitionId()));
        }
        if(StringUtils.isNoneBlank(search.getUniqueId())) {
            predicates.add(cb.equal(rootEntry.get("uniqueID"), search.getUniqueId()));
        }
        Hd4prcIdentification identification = search.getIdentification();
        if (identification != null) {
            if(StringUtils.isNoneBlank(identification.getValue())) {
                predicates.add(cb.equal(rootEntry.<Hd4prcIdentification> get("identification").<Integer> get("value"), identification.getValue()));
            }
        }
        return predicates;
    }

}
