/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.hd4prc.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.hd4prc.security.domain.ShibbolethAttribute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ShibbolethAuthorityMapper {

    private static final Logger LOG = LoggerFactory.getLogger(ShibbolethAuthorityMapper.class);

    public static final Map<String, String> AUTHORITY_MAP = new HashMap<>();

    static {
        AUTHORITY_MAP.put("user", Authority.USER);
        AUTHORITY_MAP.put("admin", Authority.ADMIN);
    }

    public static Set<Authority> getAuthorities(final Map<ShibbolethAttribute, String> attributes) {
        final String authority = attributes.get(ShibbolethAttribute.getRoleAttribute());
        if(authority == null) {
            LOG.warn("No value found for shibboleth attribute [{}]. Could not determine authority", ShibbolethAttribute.getRoleAttribute());
            return null;
        }

        final String mappedAuthority = AUTHORITY_MAP.get(authority);

        if(mappedAuthority == null) {
            LOG.warn("Could not map authority [{}]", authority);
            return null;
        }

        final Set<Authority> authorities = new HashSet<>();
        authorities.add(new Authority(mappedAuthority));
        return authorities;
    }
}
