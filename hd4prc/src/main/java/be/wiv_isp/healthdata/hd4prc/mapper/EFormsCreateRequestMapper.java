/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4prc.mapper;

import be.wiv_isp.healthdata.hd4prc.dto.EFormsCreateRequest;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.util.Iterator;

public class EFormsCreateRequestMapper {

    public static EFormsCreateRequest map(JSONObject json) throws JSONException{
        EFormsCreateRequest request = new EFormsCreateRequest();
        for (Iterator<String> iterator = json.keys(); iterator.hasNext();) {
            String key = iterator.next();
            if (!"null".equalsIgnoreCase(json.getString(key))) {
                if ("id".equalsIgnoreCase(key)) {
                    JSONObject id = json.getJSONObject(key);
                    for (Iterator<String> iteratorId = id.keys(); iteratorId.hasNext(); ) {
                        String keyId = iteratorId.next();
                        if (!"null".equalsIgnoreCase(id.getString(keyId))) {
                            if ("name".equalsIgnoreCase(keyId)) {
                                request.setName(id.getString(keyId));
                            } else if ("version".equalsIgnoreCase(keyId)) {
                                request.setVersion(id.getString(keyId));
                            }
                        }
                    }
                } else if ("format".equalsIgnoreCase(key)) {
                    request.setFormat(json.getString(key));
                } else if ("integrator".equalsIgnoreCase(key)) {
                    JSONObject integrator = json.getJSONObject(key);
                    for (Iterator<String> iteratorIntegrator = integrator.keys(); iteratorIntegrator.hasNext(); ) {
                        String keyIntegrator = iteratorIntegrator.next();
                        if (!"null".equalsIgnoreCase(integrator.getString(keyIntegrator))) {
                            if ("name".equalsIgnoreCase(keyIntegrator)) {
                                request.setIntegratorName(integrator.getString(keyIntegrator));
                            } else if ("version".equalsIgnoreCase(keyIntegrator)) {
                                request.setIntegratorVersion(integrator.getString(keyIntegrator));
                            }
                        }
                    }
                }
            }
        }
        return request;
    }
}
