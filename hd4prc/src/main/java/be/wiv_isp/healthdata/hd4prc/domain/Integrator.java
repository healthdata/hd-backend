/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4prc.domain;

import be.wiv_isp.healthdata.common.util.StringUtils;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Objects;

@Embeddable
public class Integrator {

	@Column(name = "INTERGATOR_NAME")
	private String name;
	@Column(name = "INTEGRATOR_VERSION")
	private String version;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof Integrator) {
			Integrator other = (Integrator) o;

			return Objects.equals(name, other.name)
					&& Objects.equals(version, other.version);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				this.name,
				this.version);
	}

	@Override
	public String toString() {
		return StringUtils.toString(this);
	}
}
