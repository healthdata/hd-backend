/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4prc.domain.search;

import be.wiv_isp.healthdata.common.domain.search.EntitySearch;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.hd4prc.domain.Hd4prcIdentification;

import java.util.Objects;

public class Hd4prcWorkflowSearch extends EntitySearch {

    private Long dataCollectionDefinitionId;
    private Hd4prcIdentification identification;
    private String uniqueId;

    public Long getDataCollectionDefinitionId() {
        return dataCollectionDefinitionId;
    }

    public void setDataCollectionDefinitionId(Long dataCollectionDefinitionId) {
        this.dataCollectionDefinitionId = dataCollectionDefinitionId;
    }

    public Hd4prcIdentification getIdentification() {
        return identification;
    }

    public void setIdentification(Hd4prcIdentification identification) {
        this.identification = identification;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof RegistrationWorkflowSearch) {
            Hd4prcWorkflowSearch other = (Hd4prcWorkflowSearch) o;

            return Objects.equals(dataCollectionDefinitionId, other.dataCollectionDefinitionId)
                    && Objects.equals(identification, other.identification)
                    && Objects.equals(uniqueId, other.uniqueId);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                this.dataCollectionDefinitionId,
                this.identification,
                this.uniqueId);
    }
}
