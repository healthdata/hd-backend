/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4prc.domain;

import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import be.wiv_isp.healthdata.common.util.StringUtils;
import be.wiv_isp.healthdata.hd4prc.enumeration.Hd4prcAction;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "HD4PRC_WORKFLOW_HISTORY")
public class Hd4prcWorkflowHistory implements Comparable<Hd4prcWorkflowHistory> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @Column(name = "WF_UUID", nullable = false)
    private String workflowUuid;
    @Enumerated(EnumType.STRING)
    @Column(name = "ACTION", nullable = false)
    private Hd4prcAction action;
    @Column(name = "MESSAGE")
    private String message;
    @Column(name = "EXECUTED_ON", nullable = false)
    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp executedOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWorkflowUuid() {
        return workflowUuid;
    }

    public void setWorkflowUuid(String workflowUuid) {
        this.workflowUuid = workflowUuid;
    }

    public Hd4prcAction getAction() {
        return action;
    }

    public void setAction(Hd4prcAction action) {
        this.action = action;
    }

    public Timestamp getExecutedOn() {
        return executedOn;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setExecutedOn(Timestamp executedOn) {
        this.executedOn = executedOn;
    }

    @PrePersist
    public void onCreate() {
        setExecutedOn(new Timestamp(new Date().getTime()));
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof Hd4prcWorkflowHistory) {
            Hd4prcWorkflowHistory other = (Hd4prcWorkflowHistory) o;

            return Objects.equals(id, other.id)
                    && Objects.equals(workflowUuid, other.workflowUuid)
                    && Objects.equals(action, other.action)
                    && Objects.equals(message, other.message)
                    && Objects.equals(executedOn, other.executedOn);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                this.id,
                this.workflowUuid,
                this.action,
                this.message,
                this.executedOn);
    }

    @Override
    public String toString() {
        return StringUtils.toString(this);
    }

    @Override
    public int compareTo(Hd4prcWorkflowHistory other) {
        if (this.executedOn == null && other.executedOn == null) {
            return 0;
        }
        if (this.executedOn == null) {
            return -1;
        }
        if (other.executedOn == null) {
            return 1;
        }
        return this.executedOn.compareTo(other.executedOn);
    }
}
