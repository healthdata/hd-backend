/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4prc.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.hd4prc.dao.IHd4prcDataSetDao;
import be.wiv_isp.healthdata.hd4prc.domain.Hd4prcDataSet;
import be.wiv_isp.healthdata.hd4prc.domain.search.Hd4prcDataSetSearch;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class Hd4prcDataSetDao extends CrudDaoV2<Hd4prcDataSet, String, Hd4prcDataSetSearch> implements IHd4prcDataSetDao {

    public Hd4prcDataSetDao() {
        super(Hd4prcDataSet.class);
    }

    @Override
    protected List<Predicate> getPredicates(Hd4prcDataSetSearch search, CriteriaBuilder cb, Root<Hd4prcDataSet> rootEntry) {
        final List<Predicate> predicates = new ArrayList<>();
        if(StringUtils.isNoneBlank(search.getWorkflowUuid())) {
            predicates.add(cb.equal(rootEntry.get("workflowUuid"), search.getWorkflowUuid()));
        }
        return predicates;
    }
}
