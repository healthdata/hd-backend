/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4prc.dto;

import be.wiv_isp.healthdata.orchestration.domain.TypedValue;
import be.wiv_isp.healthdata.common.domain.enumeration.Language;
import be.wiv_isp.healthdata.common.json.deserializer.JsonToByteArrayDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.ByteArrayToJsonSerializer;
import be.wiv_isp.healthdata.common.util.StringUtils;
import be.wiv_isp.healthdata.hd4prc.domain.Hd4prcWorkflow;
import be.wiv_isp.healthdata.orchestration.dto.IntegratorDto;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class EFormsWorkflowDataSetDto {

    private String dataCollectionName;
    private Long dataCollectionDefinitionId;
    private Hd4prcIdentificationDto identification;
    private IntegratorDto integrator;
    private String format;
    @JsonSerialize(using = ByteArrayToJsonSerializer.class)
    @JsonDeserialize(using = JsonToByteArrayDeserializer.class)
    private byte[] content;
    private String csv;
    private Map<String, TypedValueDto> codedContent;
    private Map<String, String> patientId;
    @JsonSerialize(using = ByteArrayToJsonSerializer.class)
    @JsonDeserialize(using = JsonToByteArrayDeserializer.class)
    private byte[] privateContent;
    private Language language;
    private String uniqueID;

    public EFormsWorkflowDataSetDto() {
    }

    public EFormsWorkflowDataSetDto(Hd4prcWorkflow workflow, String csv) {
        dataCollectionName = workflow.getDataCollectionName();
        dataCollectionDefinitionId = workflow.getDataCollectionDefinitionId();
        identification = new Hd4prcIdentificationDto(workflow.getIdentification());
        integrator = new IntegratorDto();
        integrator.setVersion(workflow.getIntegrator().getVersion());
        integrator.setName(workflow.getIntegrator().getName());
        format = workflow.getFormat();
        content = workflow.getContent();
        this.csv = csv;
        codedContent = new HashMap<>();
        for (Map.Entry<String,TypedValue> entry : workflow.getCodedContent().entrySet()) {
            codedContent.put(entry.getKey(), new TypedValueDto(entry.getValue()));
        }
        privateContent = workflow.getPrivateContent();
        patientId = workflow.getPatientId();
        language = workflow.getLanguage();
        uniqueID = workflow.getUniqueID();
    }

    public String getDataCollectionName() {
        return dataCollectionName;
    }

    public void setDataCollectionName(String dataCollectionName) {
        this.dataCollectionName = dataCollectionName;
    }

    public Long getDataCollectionDefinitionId() {
        return dataCollectionDefinitionId;
    }

    public void setDataCollectionDefinitionId(Long dataCollectionDefinitionId) {
        this.dataCollectionDefinitionId = dataCollectionDefinitionId;
    }

    public Hd4prcIdentificationDto getIdentification() {
        return identification;
    }

    public void setIdentification(Hd4prcIdentificationDto identification) {
        this.identification = identification;
    }

    public IntegratorDto getIntegrator() {
        return integrator;
    }

    public void setIntegrator(IntegratorDto integrator) {
        this.integrator = integrator;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getCsv() {
        return csv;
    }

    public void setCsv(String csv) {
        this.csv = csv;
    }

    public Map<String, TypedValueDto> getCodedContent() {
        return codedContent;
    }

    public void setCodedContent(Map<String, TypedValueDto> codedContent) {
        this.codedContent = codedContent;
    }

    public Map<String, String> getPatientId() {
        return patientId;
    }

    public void setPatientId(Map<String, String> patientId) {
        this.patientId = patientId;
    }

    public byte[] getPrivateContent() {
        return privateContent;
    }

    public void setPrivateContent(byte[] privateContent) {
        this.privateContent = privateContent;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(String uniqueID) {
        this.uniqueID = uniqueID;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof EFormsWorkflowDataSetDto) {
            EFormsWorkflowDataSetDto other = (EFormsWorkflowDataSetDto) o;

            return Objects.equals(dataCollectionName, other.dataCollectionName)
                    && Objects.equals(dataCollectionDefinitionId, other.dataCollectionDefinitionId)
                    && Objects.equals(identification, other.identification)
                    && Objects.equals(integrator, other.integrator)
                    && Objects.equals(privateContent, other.privateContent)
                    && Objects.equals(format, other.format)
                    && Objects.equals(language, other.language)
                    && Objects.equals(uniqueID, other.uniqueID);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                this.dataCollectionName,
                this.dataCollectionDefinitionId,
                this.identification,
                this.integrator,
                this.privateContent,
                this.format,
                this.language,
                this.uniqueID);
    }

    @Override
    public String toString() {
        return StringUtils.toString(this);
    }
}
