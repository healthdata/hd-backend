/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.hd4prc.security.mapper;

import be.wiv_isp.healthdata.hd4prc.security.domain.ShibbolethAttribute;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class ShibbolethAttributeMapper {

    private static final Logger LOG = LoggerFactory.getLogger(ShibbolethAttributeMapper.class);

    public static Map<ShibbolethAttribute, String> extractShibbolethAttributes(final HttpServletRequest request) {
        LOG.debug("Extracting Shibboleth attributes from http headers");
        final Map<ShibbolethAttribute, String> attributes = new HashMap<>();
        for (final ShibbolethAttribute shibbolethAttribute : ShibbolethAttribute.values()) {
            final String shibbolethId = shibbolethAttribute.getShibId();
            if (StringUtils.isNotBlank(request.getHeader(shibbolethId))) {
                attributes.put(shibbolethAttribute, request.getHeader(shibbolethId));
            }
        }
        LOG.debug("Shibboleth attributes: " + attributes);
        return attributes;
    }
}
