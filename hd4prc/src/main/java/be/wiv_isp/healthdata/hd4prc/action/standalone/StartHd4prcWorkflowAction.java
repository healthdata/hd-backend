/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4prc.action.standalone;

import be.wiv_isp.healthdata.orchestration.domain.CSVContent;
import be.wiv_isp.healthdata.orchestration.domain.TypedValue;
import be.wiv_isp.healthdata.gathering.action.workflow.SaveRegistrationAction;
import be.wiv_isp.healthdata.gathering.action.workflow.SubmitRegistrationAction;
import be.wiv_isp.healthdata.hd4prc.dao.IHd4prcWorkflowDao;
import be.wiv_isp.healthdata.hd4prc.domain.Hd4prcWorkflow;
import be.wiv_isp.healthdata.hd4prc.service.IHd4prcUserService;
import be.wiv_isp.healthdata.orchestration.action.standalone.AbstractStartHd4prcWorkflowAction;
import be.wiv_isp.healthdata.orchestration.action.workflow.executor.IRegistrationWorkflowActionExecutor;
import be.wiv_isp.healthdata.orchestration.domain.AbstractWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import be.wiv_isp.healthdata.orchestration.factory.IActionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

public class StartHd4prcWorkflowAction extends AbstractStartHd4prcWorkflowAction {

    private String hd4prcWorkflowUuid;

    private Map<String, TypedValue> codedData;

    @Autowired
    private IHd4prcUserService hd4prcUserService;
    @Autowired
    private IHd4prcWorkflowDao hd4prcWorkflowDao;
    @Autowired
    private IActionFactory actionFactory;
    @Autowired
    private IRegistrationWorkflowActionExecutor workflowActionExecutor;

    @Override
    public void execute() {
        User user = hd4prcUserService.getOrCreate(getHd4prcCreateDto().getIdentification());

        Hd4prcWorkflow hd4prcWorkflow = hd4prcWorkflowDao.get(hd4prcWorkflowUuid);

        SaveRegistrationAction saveAction = (SaveRegistrationAction) actionFactory.getActionByActionType(WorkflowActionType.SAVE, WorkflowType.REGISTRATION);
        saveAction.setDataCollectionDefinitionId(getHd4prcCreateDto().getDataCollectionDefinitionId());
        DocumentData documentData = new DocumentData();
        documentData.setContent(getHd4prcCreateDto().getContent());
        documentData.setCoded(codedData);
        documentData.setPatientID(hd4prcWorkflow.getPatientId());
        documentData.setPrivate(hd4prcWorkflow.getPrivateContent());
        saveAction.setDocumentData(documentData);
        saveAction.setUniqueID(hd4prcWorkflow.getUniqueID());
        if(getHd4prcCreateDto().getIntegrator() != null) {
            saveAction.setSource(getHd4prcCreateDto().getIntegrator().toString());
        } else {
            saveAction.setSource("HD4PRC-unknown EMD");
        }
        AbstractWorkflow workflow = workflowActionExecutor.execute(saveAction, user);

        SubmitRegistrationAction submitAction = (SubmitRegistrationAction) actionFactory.getActionByActionType(WorkflowActionType.SUBMIT, WorkflowType.REGISTRATION);
        submitAction.setWorkflowId(workflow.getId());
        workflowActionExecutor.execute(submitAction, user);
    }

    @Override
    public void extractInfo(Message message) {
        super.extractInfo(message);
        final CSVContent csvContent = new CSVContent(message.getContent());
        this.hd4prcWorkflowUuid = csvContent.getWorkflowId();
        this.codedData = new HashMap<>();
        for (int i = 0; i < csvContent.getSize() - 1; i++) {
            final String fieldName = csvContent.getFieldName(i);
            if (csvContent.isTechnicalField(i) || csvContent.isDummyField(i)) {
                continue;
            }
            this.codedData.put(fieldName, new TypedValue(csvContent.getField(i), csvContent.getFieldType(i)));
        }
    }
}
