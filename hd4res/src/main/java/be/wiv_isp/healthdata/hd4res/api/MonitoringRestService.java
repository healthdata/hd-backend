/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.api;

import be.wiv_isp.healthdata.hd4res.action.standalone.ResendMessagesAction;
import be.wiv_isp.healthdata.hd4res.factory.IMessageFactory;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.StandAloneActionType;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.MessageFormat;

@Service
@Path("/monitoring")
public class MonitoringRestService {

    private static final Logger LOG = LoggerFactory.getLogger(MonitoringRestService.class);

    @Autowired
    private IMessageService messageService;
    @Autowired
    private IMessageFactory messageFactory;

    @POST
    @Path("/messages/resend/{identificationValue}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendResendMessagesAction(@PathParam("identificationValue") String identificationValue) {
        LOG.info(MessageFormat.format("Send {0} request received with identification value {1}", StandAloneActionType.RESEND_MESSAGES_ACTION, identificationValue));

        final Message replyMessage = messageService.getLast(identificationValue);
        final Message outboxMessage = messageFactory.createOutgoingMessage(new ResendMessagesAction(), replyMessage);
        messageService.create(outboxMessage);

        return Response.ok().build();
    }
}
