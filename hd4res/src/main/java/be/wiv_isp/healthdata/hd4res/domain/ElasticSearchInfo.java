/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain;

import be.wiv_isp.healthdata.orchestration.domain.AbstractElasticSearchInfo;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ELASTICSEARCH_INFO")
@GenericGenerator(name = "workflow-primary-key", strategy = "foreign", parameters = { @Parameter(name = "property", value = "workflow") })
public class ElasticSearchInfo extends AbstractElasticSearchInfo<RegistrationWorkflow> {
}
