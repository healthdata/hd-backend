/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain.dcd;

import be.wiv_isp.healthdata.common.util.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "DATA_COLLECTION_DEFINITION_HISTORIES")
public class DataCollectionDefinitionHistory implements Serializable, Comparable<DataCollectionDefinitionHistory> {

    private static final long serialVersionUID = 7740195697319851456L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "DATA_COLLECTION_DEFINITION_HISTORY_ID")
    private Long id;

    @Column(name = "DATA_COLLECTION_DEFINITIONS_ID")
    private Long dataCollectionDefinitionId;

    @Column(name = "CREATED_ON", nullable = false)
    private Timestamp createdOn;

    @Lob
    @Column(name = "CONTENT", nullable = false)
    private byte[] content;

    @Column(name = "USER_NAME")
    private String userName;

    @Column(name = "DESCRIPTION")
    private String description;

    public DataCollectionDefinitionHistory() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDataCollectionDefinitionId() {
        return dataCollectionDefinitionId;
    }

    public void setDataCollectionDefinitionId(Long dataCollectionDefinitionId) {
        this.dataCollectionDefinitionId = dataCollectionDefinitionId;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @PrePersist
    public void onCreate() {
        long currentTimeWithoutMilliseconds = 1000 * (new Date().getTime() / 1000);
        setCreatedOn(new Timestamp(currentTimeWithoutMilliseconds));
    }

    @Override
    public int compareTo(DataCollectionDefinitionHistory other) {
        if (this.createdOn == null && other.createdOn == null) {
            return 0;
        }
        if (this.createdOn == null) {
            return -1;
        }
        if (other.createdOn == null) {
            return 1;
        }
        return this.createdOn.compareTo(other.createdOn);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataCollectionDefinitionHistory that = (DataCollectionDefinitionHistory) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(content, that.content) &&
                Objects.equals(userName, that.userName) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, createdOn, content, userName, description);
    }

    @Override
    public String toString() {
        return StringUtils.toString(this);
    }
}
