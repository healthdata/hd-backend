/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dto;

import be.wiv_isp.healthdata.common.dto.FollowUpDefinitionDto;
import be.wiv_isp.healthdata.common.json.deserializer.JsonToByteArrayDeserializer;
import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.ByteArrayToJsonSerializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import be.wiv_isp.healthdata.common.util.StringUtils;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

public class DataCollectionDefinitionHistoryDto implements Serializable{

    private static final long serialVersionUID = 1L;

    @JsonSerialize(using = ByteArrayToJsonSerializer.class)
    @JsonDeserialize(using = JsonToByteArrayDeserializer.class)
    private byte[] content;

    private List<FollowUpDefinitionDto> followUpDefinitions;

    private Long historyId;

    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp createdOn;

    private String userName;

    private String description;

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public List<FollowUpDefinitionDto> getFollowUpDefinitions() {
        return followUpDefinitions;
    }

    public void setFollowUpDefinitions(List<FollowUpDefinitionDto> followUpDefinitions) {
        this.followUpDefinitions = followUpDefinitions;
    }

    public Long getHistoryId() {
        return historyId;
    }

    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataCollectionDefinitionHistoryDto that = (DataCollectionDefinitionHistoryDto) o;
        return Objects.equals(content, that.content) &&
                Objects.equals(historyId, that.historyId) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(userName, that.userName) &&
                Objects.equals(description, that.description) &&
                Objects.equals(followUpDefinitions, that.followUpDefinitions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(content, historyId, createdOn, userName, description, followUpDefinitions);
    }


    @Override
    public String toString() {
        return StringUtils.toString(this);
    }
}
