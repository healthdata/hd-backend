/*
 *  Copyright 2014-2017 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.hd4res.dao.IDataCollectionDefinitionHistoryDao;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionDefinitionHistory;
import be.wiv_isp.healthdata.hd4res.domain.search.DataCollectionDefinitionHistorySearch;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DataCollectionDefinitionHistoryDao extends CrudDaoV2<DataCollectionDefinitionHistory, Long, DataCollectionDefinitionHistorySearch> implements IDataCollectionDefinitionHistoryDao {

	public DataCollectionDefinitionHistoryDao() {
		super(DataCollectionDefinitionHistory.class);
	}

	@Override
	protected List<Predicate> getPredicates(DataCollectionDefinitionHistorySearch search, CriteriaBuilder cb, Root<DataCollectionDefinitionHistory> rootEntry) {
		final List<Predicate> predicates = new ArrayList<>();
		if (search.getDataCollectionDefinitionId() != null) {
			predicates.add(cb.equal(rootEntry.get("dataCollectionDefinitionId"), search.getDataCollectionDefinitionId()));
		}
		return predicates;
	}
}
