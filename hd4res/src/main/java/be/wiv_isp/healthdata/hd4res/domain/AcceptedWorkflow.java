/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "ACCEPTED_WORKFLOWS")
public class AcceptedWorkflow {

	@Id
	@Column(name = "WORKFLOW_ID")
	private Long id;

	@Column(name = "WORKFLOW_TYPE")
	@Enumerated(EnumType.STRING)
	private WorkflowType workflowType;

	@Column(name = "ERROR_PROCESSING", nullable = false)
	private boolean errorProcessing = false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public WorkflowType getWorkflowType() {
		return workflowType;
	}

	public void setWorkflowType(WorkflowType workflowType) {
		this.workflowType = workflowType;
	}

	public boolean isErrorProcessing() {
		return errorProcessing;
	}

	public void setErrorProcessing(boolean errorProcessing) {
		this.errorProcessing = errorProcessing;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof AcceptedWorkflow) {
			AcceptedWorkflow other = (AcceptedWorkflow) o;

			return Objects.equals(id, other.id);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}
}
