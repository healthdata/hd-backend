/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.api;

import be.wiv_isp.healthdata.common.dto.DataCollectionGroupDto;
import be.wiv_isp.healthdata.common.dto.SalesForceOrganizationDto;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.hd4res.domain.ParticipationDocument;
import be.wiv_isp.healthdata.hd4res.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.search.ParticipationWorkflowSearch;
import be.wiv_isp.healthdata.hd4res.dto.FullSearchParticipationWorkflowDto;
import be.wiv_isp.healthdata.hd4res.dto.ParticipationWorkflowDto;
import be.wiv_isp.healthdata.hd4res.service.IDataCollectionService;
import be.wiv_isp.healthdata.orchestration.action.workflow.executor.IParticipationWorkflowActionExecutor;
import be.wiv_isp.healthdata.orchestration.action.workflow.executor.IWorkflowActionExecutor;
import be.wiv_isp.healthdata.orchestration.api.AbstractParticipationWorkflowRestService;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.service.IAbstractParticipationWorkflowService;
import be.wiv_isp.healthdata.orchestration.service.IAbstractWorkflowService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionGroupForwardService;
import be.wiv_isp.healthdata.orchestration.service.IUserDataCollectionService;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
@Path("/participations")
public class ParticipationWorkflowRestService extends AbstractParticipationWorkflowRestService<ParticipationWorkflow, ParticipationWorkflowSearch, FullSearchParticipationWorkflowDto, ParticipationWorkflowDto, ParticipationDocument> {

	@Autowired
	private IAbstractParticipationWorkflowService workflowService;
	@Autowired
	private IParticipationWorkflowActionExecutor workflowActionExecutor;
	@Autowired
	protected IUserDataCollectionService userDataCollectionService;
	@Autowired
	private IDataCollectionGroupForwardService dataCollectionGroupForwardService;
	@Autowired
	private IDataCollectionService dataCollectionService;

	@Override
	protected IAbstractWorkflowService<ParticipationWorkflow, ParticipationWorkflowSearch> getWorkflowService() {
		return workflowService;
	}

	@Override
	public FullSearchParticipationWorkflowDto getFullDtoInstance(ParticipationWorkflowDto dto) {
		return new FullSearchParticipationWorkflowDto(dto);
	}

	@Override
	protected IWorkflowActionExecutor<ParticipationWorkflow> getWorkflowActionExecutor() {
		return workflowActionExecutor;
	}

	@GET
	@PreAuthorize("hasRole('ROLE_USER')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(@Context UriInfo info) {
		final Long dataCollectionDefinitionId = RestUtils.getParameterLong(info, RestUtils.ParameterName.DATA_COLLECTION_DEFINITION_ID);
		final List<ParticipationWorkflowDto> result = getParticipationWorkflowDtos(dataCollectionDefinitionId);

		return Response.ok(result).build();
	}

	@GET
	@Path("/nonparticipating")
	@PreAuthorize("hasRole('ROLE_USER')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getNonParticipating(@Context UriInfo info) {
		final HDUserDetails principal = AuthenticationService.getUserDetails();

		final Long dataCollectionDefinitionId = RestUtils.getParameterLong(info, RestUtils.ParameterName.DATA_COLLECTION_DEFINITION_ID);

		DataCollectionGroupDto dcg = dataCollectionGroupForwardService.get(dataCollectionDefinitionId);
		final String groupName = dcg.getName();

		if(!userDataCollectionService.isUserAuthorized(principal, groupName)) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		final Set<SalesForceOrganizationDto> participatingOrganizations = convert(getParticipationWorkflowDtos(dataCollectionDefinitionId));
		final Set<SalesForceOrganizationDto> authorizedOrganizations = new HashSet<>(dataCollectionService.getAuthorizedOrganizations(groupName));
		authorizedOrganizations.removeAll(participatingOrganizations);

		return Response.ok(authorizedOrganizations).build();
	}

	private List<ParticipationWorkflowDto> getParticipationWorkflowDtos(Long dataCollectionDefinitionId) {
		final HDUserDetails principal = AuthenticationService.getUserDetails();

		ParticipationWorkflowSearch search = new ParticipationWorkflowSearch();
		search.setWithDocument(null);
		search.setOrganization(principal.getOrganization());
		if (dataCollectionDefinitionId != null) {
			search.setDataCollectionDefinitionId(dataCollectionDefinitionId);
		}
		List<ParticipationWorkflow> participations = workflowService.getAll(search);
		List<ParticipationWorkflowDto> result = new ArrayList<>();
		for (ParticipationWorkflow participation : participations) {
			if(userDataCollectionService.isUserAuthorized(principal, participation.getDataCollectionName())) {
				result.add(new ParticipationWorkflowDto(participation));
			}
		}
		return result;
	}

	private Set<SalesForceOrganizationDto> convert(List<ParticipationWorkflowDto> dtos) {
		final Set<SalesForceOrganizationDto> set = new HashSet<>();
		for (final ParticipationWorkflowDto dto : dtos) {
			final SalesForceOrganizationDto salesForceOrganizationDto = new SalesForceOrganizationDto();
			salesForceOrganizationDto.setName(dto.getIdentificationName());
			salesForceOrganizationDto.setIdentificationType(dto.getIdentificationType());
			salesForceOrganizationDto.setIdentificationValue(dto.getIdentificationValue());
			set.add(salesForceOrganizationDto);
		}
		return set;
	}

	@Override
	protected boolean ignoreDeletedWorkflows() {
		return false;
	}
}
