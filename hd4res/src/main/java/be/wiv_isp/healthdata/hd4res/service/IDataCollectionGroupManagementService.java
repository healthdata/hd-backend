/*
 *  Copyright 2014-2017 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4res.service;


import be.wiv_isp.healthdata.hd4res.dto.DataCollectionGroupManagementDto;
import org.codehaus.jettison.json.JSONObject;

import java.util.List;

public interface IDataCollectionGroupManagementService {

    DataCollectionGroupManagementDto get(Long id);

    List<DataCollectionGroupManagementDto> getAll();

    DataCollectionGroupManagementDto create(JSONObject json, String userName) throws Exception;

    DataCollectionGroupManagementDto update(Long id, JSONObject json, String userName) throws Exception;

    void delete(Long id);

    DataCollectionGroupManagementDto publish(Long id, String username);

    DataCollectionGroupManagementDto republish(Long id, String username);

}
