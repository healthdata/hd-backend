/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4res.action.standalone;

import be.wiv_isp.healthdata.orchestration.domain.CSVContent;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.hd4res.factory.IMessageFactory;
import be.wiv_isp.healthdata.orchestration.action.standalone.AbstractHd4prcCreateAction;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageType;
import be.wiv_isp.healthdata.orchestration.jaxb.json.JsonMarshaller;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import org.springframework.beans.factory.annotation.Autowired;

public class Hd4prcCreateAction extends AbstractHd4prcCreateAction {

    @Autowired
    private IConfigurationService configurationService;
    @Autowired
    private IMessageService messageService;
    @Autowired
    private IMessageFactory messageFactory;

    private CSVContent csvContent;

    @Override
    public void execute() {
        String hd4prcIdValue = configurationService.get(ConfigurationKey.HEALTHDATA_HD4PRC_ID_VALUE).getValue();
        Message replyMessage = messageService.getLast(hd4prcIdValue);

        StartHd4prcWorkflowAction action = new StartHd4prcWorkflowAction();
        action.setHd4prcCreateDto(getHd4prcCreateDto());
        csvContent.setLastField(JsonMarshaller.marshalToByteArray(action));

        final Message message = messageFactory.createMessage(csvContent, replyMessage, MessageType.ACTION, null);
        messageService.create(message);
    }

    @Override
    public void extractInfo(Message message) {
        super.extractInfo(message);
        csvContent = new CSVContent(message.getContent());
    }
}
