/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.common.api.ClientVersion;
import be.wiv_isp.healthdata.common.exception.HealthDataAssert;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.hd4res.api.uri.RegistrationStatisticUri;
import be.wiv_isp.healthdata.hd4res.service.IRegistrationStatisticForwardService;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.ICatalogueClient;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Service
public class RegistrationStatisticForwardService implements IRegistrationStatisticForwardService {

    @Autowired
    private IConfigurationService configurationService;

    @Autowired
    private ICatalogueClient catalogueClient;

    @Override
    // TODO WDC-2948 (caching?)
    public ClientResponse get() {
        final Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

        final RegistrationStatisticUri uri = new RegistrationStatisticUri();
        uri.setHost(catalogueHost.getValue());

        final WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.setUrl(uri.toString());
        wsb.addHeader(HttpHeaders.CLIENT_VERSION, ClientVersion.CentralPlatform.getDefault());
        wsb.setGet(true);
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.setReturnType(new GenericType<ClientResponse>() {});

        final ClientResponse response = catalogueClient.addAuthorizationHeaderAndCallWebService(wsb);
        HealthDataAssert.assertTrue(response.getStatus() >= 200 && response.getStatus() < 300, Response.Status.INTERNAL_SERVER_ERROR, "Central platform returned status code {0}", response.getStatus());
        return response;
    }

}
