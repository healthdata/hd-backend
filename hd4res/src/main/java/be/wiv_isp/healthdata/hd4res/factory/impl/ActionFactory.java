/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.factory.impl;

import be.wiv_isp.healthdata.hd4res.action.standalone.*;
import be.wiv_isp.healthdata.hd4res.action.workflow.*;
import be.wiv_isp.healthdata.orchestration.action.Action;
import be.wiv_isp.healthdata.orchestration.domain.FollowUp;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.factory.impl.AbstractActionFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ActionFactory extends AbstractActionFactory {

	@Override
	protected List<Action> getActionList(WorkflowType workflowType) {
		final List<Action> actions = new ArrayList<>();

		switch (workflowType) {
			case REGISTRATION:
				actions.add(new CreateForReviewRegistrationAction());
				actions.add(new SubmitAnnotationsRegistrationAction());
				actions.add(new ResubmitRegistrationAction());
				actions.add(new SendRegistrationAction());
				actions.add(new UpdateStatusRegistrationAction());
				actions.add(new UpdateFollowUpAction());
				actions.add(new DeleteAction());

				break;
			case PARTICIPATION:
				actions.add(new StartParticipationAction());
				actions.add(new StopParticipationAction());
				actions.add(new CreateForReviewParticipationAction());
				actions.add(new SubmitAnnotationsParticipationAction());
				actions.add(new ResubmitParticipationAction());
				actions.add(new SendParticipationAction());
				actions.add(new UpdateStatusParticipationAction());
				break;
		}
		actions.add(new StatusMessageAction());
		actions.add(new UserRequestAction());
		actions.add(new StableDataUploadAction());
		actions.add(new UserUpdateAction());
		actions.add(new Hd4prcCreateAction());

		return actions;
	}

	@Override
	protected Class<? extends FollowUp> getFollowUpClass() {
		return FollowUp.class;
	}
}
