/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain;


import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import be.wiv_isp.healthdata.common.util.StringUtils;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "HD4DP_USERS")
public class Hd4dpUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "USER_ID")
    private Long id;

    @Column(name = "HD4DP_ID")
    private Long hd4dpId;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "FIRST_NAME")
    protected String firstName;

    @Column(name = "LAST_NAME")
    protected String lastName;

    @Column(name = "EMAIL")
    protected String email;

    @Column(name = "ENABLED")
    private boolean enabled;

    @Column(name = "LDAP_USER")
    private boolean ldapUser;

    @Embedded
    private HealthDataIdentification healthDataIdentification;

    @Fetch(FetchMode.JOIN)
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "USER_ID", nullable = false)
    private Set<Hd4dpAuthority> authorities;

    @Fetch(FetchMode.JOIN)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "HD4DP_USER_DATA_COLLECTIONS", joinColumns = @JoinColumn(name = "USER_ID"))
    @Column(name = "DATA_COLLECTION_NAME")
    private Set<String> dataCollectionNames;

    @JsonSerialize(using = TimestampSerializer.class) // TODO: should be removed as we use DTO object when exchanging object with other components
    @JsonDeserialize(using = TimestampDeserializer.class) // TODO: should be removed as we use DTO object when exchanging object with other components
    @Column(name = "CREATED_ON", nullable = false)
    protected Timestamp createdOn;

    @JsonSerialize(using = TimestampSerializer.class) // TODO: should be removed as we use DTO object when exchanging object with other components
    @JsonDeserialize(using = TimestampDeserializer.class) // TODO: should be removed as we use DTO object when exchanging object with other components
    @Column(name = "UPDATED_ON", nullable = false)
    protected Timestamp updatedOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHd4dpId() {
        return hd4dpId;
    }

    public void setHd4dpId(Long hd4dpId) {
        this.hd4dpId = hd4dpId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isLdapUser() {
        return ldapUser;
    }

    public void setLdapUser(boolean ldapUser) {
        this.ldapUser = ldapUser;
    }

    public HealthDataIdentification getHealthDataIdentification() {
        return healthDataIdentification;
    }

    public void setHealthDataIdentification(HealthDataIdentification healthDataIdentification) {
        this.healthDataIdentification = healthDataIdentification;
    }

    public Set<Hd4dpAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<Hd4dpAuthority> authorities) {
        if(this.authorities == null) {
            this.authorities = authorities;
        } else {
            this.authorities.clear();
            this.authorities.addAll(authorities);
        }
    }

    public Set<String> getDataCollectionNames() {
        return dataCollectionNames;
    }

    public void setDataCollectionNames(Set<String> dataCollectionNames) {
        if(this.dataCollectionNames == null) {
            this.dataCollectionNames = dataCollectionNames;
        } else {
            this.dataCollectionNames.clear();
            this.dataCollectionNames.addAll(dataCollectionNames);
        }
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @PrePersist
    public void onCreate() {
        Timestamp createdOnTimestamp = new Timestamp(new Date().getTime());
        setCreatedOn(createdOnTimestamp);
        setUpdatedOn(createdOnTimestamp);
    }

    @PreUpdate
    public void onUpdate() {
        setUpdatedOn(new Timestamp(new Date().getTime()));
    }

    @Override
    public String toString() {
        return StringUtils.toString(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Hd4dpUser))
            return false;

        Hd4dpUser hd4dpUser = (Hd4dpUser) o;

        if (enabled != hd4dpUser.enabled)
            return false;
        if (ldapUser != hd4dpUser.ldapUser)
            return false;
        if (id != null ? !id.equals(hd4dpUser.id) : hd4dpUser.id != null)
            return false;
        if (!hd4dpId.equals(hd4dpUser.hd4dpId))
            return false;
        if (!username.equals(hd4dpUser.username))
            return false;
        if (firstName != null ? !firstName.equals(hd4dpUser.firstName) : hd4dpUser.firstName != null)
            return false;
        if (lastName != null ? !lastName.equals(hd4dpUser.lastName) : hd4dpUser.lastName != null)
            return false;
        if (email != null ? !email.equals(hd4dpUser.email) : hd4dpUser.email != null)
            return false;
        if (!healthDataIdentification.equals(hd4dpUser.healthDataIdentification))
            return false;
        if (authorities != null ? !authorities.equals(hd4dpUser.authorities) : hd4dpUser.authorities != null)
            return false;
        if (dataCollectionNames != null ? !dataCollectionNames.equals(hd4dpUser.dataCollectionNames) : hd4dpUser.dataCollectionNames != null)
            return false;
        if (createdOn != null ? !createdOn.equals(hd4dpUser.createdOn) : hd4dpUser.createdOn != null)
            return false;
        return !(updatedOn != null ? !updatedOn.equals(hd4dpUser.updatedOn) : hd4dpUser.updatedOn != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + hd4dpId.hashCode();
        result = 31 * result + username.hashCode();
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (enabled ? 1 : 0);
        result = 31 * result + (ldapUser ? 1 : 0);
        result = 31 * result + healthDataIdentification.hashCode();
        result = 31 * result + (authorities != null ? authorities.hashCode() : 0);
        result = 31 * result + (dataCollectionNames != null ? dataCollectionNames.hashCode() : 0);
        result = 31 * result + (createdOn != null ? createdOn.hashCode() : 0);
        result = 31 * result + (updatedOn != null ? updatedOn.hashCode() : 0);
        return result;
    }
}
