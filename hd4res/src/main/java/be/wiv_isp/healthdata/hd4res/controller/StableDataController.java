/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.controller;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.hd4res.domain.StableData;
import be.wiv_isp.healthdata.hd4res.domain.StableDataCountResult;
import be.wiv_isp.healthdata.hd4res.domain.StableDataCountsResult;
import be.wiv_isp.healthdata.hd4res.domain.StatusMessage;
import be.wiv_isp.healthdata.hd4res.domain.search.StableDataSearch;
import be.wiv_isp.healthdata.hd4res.service.IStableDataService;
import be.wiv_isp.healthdata.orchestration.domain.search.StatusMessageSearch;
import be.wiv_isp.healthdata.orchestration.service.IStatusMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

@Controller
public class StableDataController {

    @Autowired
    private IStableDataService stableDataService;

    @Autowired
    private IStatusMessageService<StatusMessage> statusMessageService;

    @RequestMapping("/stable-data-report")
    public ModelAndView home() {

        final List<StableDataCountResult> result = stableDataService.getCounts(new StableDataSearch());
        final StableDataCountsResult data = stableDataService.convert(result);
        final Set<String> dataCollectionNames = new HashSet<>();
        final Map<String, String> installationNames = new HashMap<>();

        if(data.getCountsPerInstallation() != null) {
            for (Map.Entry<String, StableDataCountsResult.CountsPerInstallation> entry : data.getCountsPerInstallation().entrySet()) {
                final StableDataCountsResult.CountsPerInstallation countsPerInstallation = entry.getValue();
                for (Map.Entry<String, StableDataCountsResult.CountsPerDataCollection> entry1 : countsPerInstallation.getCountsPerDataCollection().entrySet()) {
                    final String dataCollectionName = entry1.getKey();
                    dataCollectionNames.add(dataCollectionName);
                }

                final String installationId = entry.getKey();
                final String installationName = getInstallationName(installationId);
                installationNames.put(installationId, installationName);
            }
        }

        final ModelAndView modelAndView = new ModelAndView("stable_data_report");
        modelAndView.addObject("data", data);
        modelAndView.addObject("installationNames", installationNames);
        modelAndView.addObject("dataCollectionNames", dataCollectionNames);
        modelAndView.addObject("stable_data_statuses", toLowerCaseString(StableData.Status.NEW, StableData.Status.SENT, StableData.Status.UPLOADED, StableData.Status.ERROR_SENDING, StableData.Status.ERROR_UPLOADING));

        return modelAndView;
    }

    private String getInstallationName(String identificationValue) {
        final StatusMessageSearch search = new StatusMessageSearch();
        final HealthDataIdentification identification = new HealthDataIdentification();
        identification.setValue(identificationValue);
        search.setHealthDataIdentification(identification);
        search.setLastOnly(true);

        try {
            final StatusMessage statusMessage = statusMessageService.getUnique(search);
            return statusMessage.getHealthDataIdentification().getName();
        } catch (Exception e) {
            return null;
        }
    }

    private List<String> toLowerCaseString(StableData.Status... values) {
        final List<String> result = new ArrayList<>();
        for (StableData.Status value : values) {
            result.add(value.toString().toLowerCase());
        }
        return result;
    }
}
