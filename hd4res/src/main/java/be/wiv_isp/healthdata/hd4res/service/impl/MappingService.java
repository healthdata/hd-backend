/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.common.domain.enumeration.Language;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.hd4res.service.IMappingService;
import be.wiv_isp.healthdata.orchestration.api.mapper.MappingResponseMapper;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.TypedValue;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.mapping.MappingRequest;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractMappingService;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Service
public class MappingService extends AbstractMappingService<RegistrationWorkflow> implements IMappingService{

    private static final Logger LOG = LoggerFactory.getLogger(MappingService.class);

    @Override
    public byte[] getPdfTemplate(byte[] dcdContent, Language language) {
        final MappingRequest request = new MappingRequest();
        request.getInput().put(MappingRequest.Input.DATA_COLLECTION_DEFINITION, JsonUtils.createSerializableJsonObject(dcdContent));
        request.getInput().put(MappingRequest.Input.LANGUAGE, language.getMapping());
        request.getTargets().add(MappingRequest.Target.PDF_TEMPLATE);


        byte[] handlebarsHeader = getHandlebars(ConfigurationKey.HANDLEBARS_HEADER);
        JSONObject response = get(request);
        byte[] pdfTemplate = MappingResponseMapper.convert(response).getPdfTemplate();
        byte[] handlebarsFooter = getHandlebars(ConfigurationKey.HANDLEBARS_FOOTER);

        return ArrayUtils.addAll(ArrayUtils.addAll(handlebarsHeader, pdfTemplate), handlebarsFooter);
    }

    private byte[] getHandlebars(ConfigurationKey configurationKey) {
        Configuration configuration = configurationService.get(configurationKey);

        WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.setUrl(configurationService.get(ConfigurationKey.GUI_HOST).getValue() + configuration.getValue());
        wsb.setGet(true);
        wsb.setAccept(MediaType.APPLICATION_OCTET_STREAM_TYPE);
        wsb.setReturnType(new GenericType<ClientResponse>() {});


        ClientResponse response = (ClientResponse)webServiceClientService.callWebService(wsb);
        try {
            if (response.getStatus() >= 200 && response.getStatus() < 300) {
                return IOUtils.toByteArray(response.getEntityInputStream());
            } else {
                String message;
                try {
                    message = IOUtils.toString(response.getEntityInputStream(), StandardCharsets.UTF_8);
                } catch (IOException e) {
                    message = "Couldn't get the response entity";
                }
                HealthDataException exception = new HealthDataException();
                exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, message);
                throw exception;
            }
        } catch (IOException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, "Couldn't read handlebars content.");
            throw exception;
        }
    }

    @Override
    protected void putAllDocumentContent(JSONObject documentObject, RegistrationWorkflow workflow) {
        JsonUtils.put(documentObject, MappingRequest.Fields.CONTENT, JsonUtils.createJsonObject(workflow.getDocument().getDocumentContent()));
        if (!workflow.getDocument().getCodedContent().isEmpty()) {
            JSONObject jsonCoded = new JSONObject();
            try {
                for (Map.Entry<String, TypedValue> codedEntry : workflow.getDocument().getCodedContent().entrySet()) {
                    JSONObject value = new JSONObject();
                    value.put("value", codedEntry.getValue().getValue());
                    value.put("type", codedEntry.getValue().getType());
                    jsonCoded.put(codedEntry.getKey(), value);
                }
            }
            catch (JSONException e) {
                jsonCoded = null;
            }
            JsonUtils.put(documentObject, MappingRequest.Fields.CODED, jsonCoded);
        }
        else
            JsonUtils.put(documentObject, MappingRequest.Fields.PATIENT_ID, new JSONObject(workflow.getMetaData()));
    }

}
