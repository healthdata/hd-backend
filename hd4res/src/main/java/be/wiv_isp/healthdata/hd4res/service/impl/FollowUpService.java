/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.hd4res.service.IFollowUpService;
import be.wiv_isp.healthdata.orchestration.dao.IFollowUpDao;
import be.wiv_isp.healthdata.orchestration.domain.FollowUp;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractFollowUpService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FollowUpService extends AbstractFollowUpService implements IFollowUpService {

    private static final Logger LOG = LoggerFactory.getLogger(FollowUpService.class);

    @Autowired
    protected IFollowUpDao dao;

    public FollowUpService() {
        super(FollowUp.class);
    }

    @Override
    public void prepareFollowUpsBeforeSaving(RegistrationWorkflow workflow, List<FollowUp> followUps) {
        LOG.debug("Updating workflow and follow-up ids before saving follow-up items");
        final List<FollowUp> fups = new ArrayList<>();
        for(FollowUp followUp : followUps) {
            LOG.debug("Retrieving follow-up item with name [{}]", followUp.getName());
            final FollowUp existingFollowUp = find(workflow.getFollowUps(), followUp.getName());
            if(existingFollowUp == null) {
                LOG.debug("No follow up found with name [{}]. Creating new follow up", followUp.getName());
                fups.add(followUp);
            } else {
                LOG.debug("A follow up was found with name [{}]. Updating existing follow up.", followUp.getName());
                existingFollowUp.setConditions(followUp.getConditions());
                existingFollowUp.setActivationDate(followUp.getActivationDate());
                existingFollowUp.setTiming(followUp.getTiming());
                existingFollowUp.setActive(followUp.isActive());
                existingFollowUp.setSubmittedOn(followUp.getSubmittedOn());
                existingFollowUp.setDescription(followUp.getDescription());
                fups.add(existingFollowUp);
            }
        }
        workflow.setFollowUps(fups);
    }

    private FollowUp find(List<FollowUp> followUps, String name) {
        for (FollowUp followUp : followUps) {
            if(name.equals(followUp.getName())) {
                return followUp;
            }
        }
        return null;
    }

    @Override
    protected IFollowUpDao<FollowUp> getDao() {
        return dao;
    }
}
