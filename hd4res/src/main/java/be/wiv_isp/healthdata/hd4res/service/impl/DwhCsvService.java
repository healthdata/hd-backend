/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.orchestration.service.impl.CsvService;
import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.hd4res.domain.ParticipationDocument;
import be.wiv_isp.healthdata.hd4res.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationDocument;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.search.ParticipationDocumentSearch;
import be.wiv_isp.healthdata.hd4res.domain.search.RegistrationDocumentSearch;
import be.wiv_isp.healthdata.hd4res.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.hd4res.service.IDwhCsvService;
import be.wiv_isp.healthdata.orchestration.dao.IParticipationDocumentDao;
import be.wiv_isp.healthdata.orchestration.dao.IRegistrationDocumentDao;
import be.wiv_isp.healthdata.orchestration.dao.IRegistrationWorkflowDao;
import be.wiv_isp.healthdata.orchestration.mapping.IMappingContextFactory;
import be.wiv_isp.healthdata.orchestration.service.IAbstractParticipationDocumentService;
import be.wiv_isp.healthdata.orchestration.service.IAbstractRegistrationDocumentService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionDefinitionForwardService;
import be.wiv_isp.healthdata.orchestration.service.IMappingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Service
public class DwhCsvService implements IDwhCsvService {

	private static final Logger LOG = LoggerFactory.getLogger(DwhCsvService.class);

	@Autowired
	private IParticipationDocumentDao participationDocumentDao;
	@Autowired
	private IRegistrationDocumentDao registrationDocumentDao;
	@Autowired
	private IRegistrationWorkflowDao<RegistrationWorkflow, RegistrationWorkflowSearch> workflowDao;
	@Autowired
	private IMappingService mappingService;
	@Autowired
	private IAbstractParticipationDocumentService participationDocumentService;
	@Autowired
	private IAbstractRegistrationDocumentService registrationDocumentService;
	@Autowired
	private IMappingContextFactory mappingContextFactory;
	@Autowired
	private IDataCollectionDefinitionForwardService catalogueService;

	@Override
	@Transactional
	public void populate() {
		populateRegistrations();
		populateParticipations();
	}

	private void populateRegistrations() {
		RegistrationDocumentSearch search = new RegistrationDocumentSearch();
		search.setWithCsv(false);
		List<RegistrationDocument> documents = registrationDocumentDao.getAll(search);
		for (RegistrationDocument document : documents) {
			try {
				RegistrationWorkflow workflow = document.getWorkflow();
				final DataCollectionDefinitionDtoV7 dcd = catalogueService.get(workflow.getDataCollectionDefinitionId());
				String csv = mappingService.mapFromJsonToCsv(workflow, mappingContextFactory.create(workflow), dcd.getContent(), false);
				document.setCsv(csv.getBytes(StandardCharsets.UTF_8));
				registrationDocumentService.update(document);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}
	}

	private void populateParticipations() {
		ParticipationDocumentSearch search = new ParticipationDocumentSearch();
		search.setWithCsv(false);
		List<ParticipationDocument> documents = participationDocumentDao.getAll(search);
		for (ParticipationDocument document : documents) {
			try {
				ParticipationWorkflow workflow = document.getWorkflow();
				final DataCollectionDefinitionDtoV7 dcd = catalogueService.get(workflow.getDataCollectionDefinitionId());
				String csv = mappingService.mapFromJsonToCsv(workflow.getDocument().getDocumentContent(), dcd.getContent(), false);
				document.setCsv(csv.getBytes(StandardCharsets.UTF_8));
				participationDocumentService.update(document);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}
	}

	@Override
	@Transactional
	public void validateDates() {
		List<Long> equals = new ArrayList<>();
		List<Long> notEquals = new ArrayList<>();
		List<Long> errors = new ArrayList<>();

		List<RegistrationWorkflow> workflows = workflowDao.getAll(new RegistrationWorkflowSearch());
		for (RegistrationWorkflow workflow : workflows) {
			validateDates(equals, notEquals, errors, workflow);
		}

		LOG.info("CSV validation: equal Workflows [{}]", equals);
		LOG.info("CSV validation: notEquals Workflows [{}]", notEquals);
		LOG.info("CSV validation: errors Workflows [{}]", errors);
	}


	@Override
	@Transactional
	public void validateDates(String dataCollectionName) {
		List<Long> equals = new ArrayList<>();
		List<Long> notEquals = new ArrayList<>();
		List<Long> errors = new ArrayList<>();

		RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		search.setDataCollectionName(dataCollectionName);
		List<RegistrationWorkflow> workflows = workflowDao.getAll(search);
		for (RegistrationWorkflow workflow : workflows) {
			validateDates(equals, notEquals, errors, workflow);
		}

		LOG.info("CSV validation: equal Workflows [{}]", equals);
		LOG.info("CSV validation: notEquals Workflows [{}]", notEquals);
		LOG.info("CSV validation: errors Workflows [{}]", errors);
	}

	@Override
	@Transactional
	public void validateDates(Long workflowId) {
		List<Long> equals = new ArrayList<>();
		List<Long> notEquals = new ArrayList<>();
		List<Long> errors = new ArrayList<>();

		RegistrationWorkflow workflow = workflowDao.get(workflowId);
		validateDates(equals, notEquals, errors, workflow);

		LOG.info("CSV validation: equal Workflows [{}]", equals);
		LOG.info("CSV validation: notEquals Workflows [{}]", notEquals);
		LOG.info("CSV validation: errors Workflows [{}]", errors);
	}

	private void validateDates(List<Long> equals, List<Long> notEquals, List<Long> errors, RegistrationWorkflow workflow) {
		RegistrationDocument document = workflow.getDocument();
		try {
			final DataCollectionDefinitionDtoV7 dcd = catalogueService.get(workflow.getDataCollectionDefinitionId());
			String csv = mappingService.mapFromJsonToCsv(workflow, mappingContextFactory.create(workflow), dcd.getContent(), false);
            StringReader readerOld = new StringReader(new String(document.getDocumentContent(), StandardCharsets.UTF_8));
            StringReader readerNew = new StringReader(csv);

            boolean equal = CsvService.compare(readerOld, readerNew);
            if(!equal) {
//				document.setCsv(mapFromSkryvToCsv.getResult().getBytes(StandardCharsets.UTF_8));
//				document = documentService.update(document);
                LOG.warn("[{}] : New csv is NOT the same as old csv.", workflow.getId());
                LOG.warn("RegistrationWorkflow ID;Old CSV;NewCsv");
                LOG.warn("[{}];[{}];[{}]", workflow.getId(), new String(document.getDocumentContent(), StandardCharsets.UTF_8), csv);
				notEquals.add(workflow.getId());
            } else {
                LOG.info("[{}] : New csv is the same as old csv.", workflow.getId());
				equals.add(workflow.getId());
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
			errors.add(workflow.getId());
        }
	}
}
