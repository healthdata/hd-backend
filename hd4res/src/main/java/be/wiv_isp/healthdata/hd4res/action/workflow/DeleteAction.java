/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.action.workflow;

import be.wiv_isp.healthdata.hd4res.domain.RegistrationDocument;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.domain.CSVContent;
import be.wiv_isp.healthdata.orchestration.domain.FollowUp;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.service.IAbstractRegistrationDocumentService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.MessageFormat;
import java.util.List;

public class DeleteAction extends AbstractRegistrationWorkflowAction {

	private static final Logger LOG = LoggerFactory.getLogger(DeleteAction.class);

	@Autowired
	private IAbstractRegistrationDocumentService documentService;

	@Override
	public WorkflowActionType getAction() {
		return WorkflowActionType.DELETE;
	}

	@Override
	public WorkflowType getWorkflowType() {
		return WorkflowType.REGISTRATION;
	}

	@Override
	public WorkflowStatus getEndStatus() {
		return WorkflowStatus.DELETED;
	}

	@Override
	public RegistrationWorkflow doExecute(RegistrationWorkflow workflow, HDUserDetails userDetails) {
		final List<FollowUp> followUps = workflow.getFollowUps();
		if(CollectionUtils.isNotEmpty(followUps)) {
			LOG.debug("unschedule and deactivate follow-ups");
			for (FollowUp followUp : followUps) {
				followUp.setActive(false);
				followUp.setActivationDate(null);
			}
		}
		RegistrationDocument document = workflow.getDocument();
		document.setDwhStatus(null);
		documentService.update(document);

		return workflow;
	}

	@Override
	public RegistrationWorkflow retrieveWorkflow() {
		LOG.debug(MessageFormat.format("Searching for workflow with hd4dp_workflow_id [{0}] and identification_value [{1}]", hd4dpWorkflowId, identificationWorkflow.getValue()));
		final RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		search.setHd4dpWorkflowId(hd4dpWorkflowId);
		search.setIdentificationValue(identificationWorkflow.getValue());

		final RegistrationWorkflow workflow = getWorkflowService().getUnique(search);

		if (workflow == null) {
			LOG.info(MessageFormat.format("No workflow found with hd4dp_workflow_id [{0}] and identification_value [{1}]", hd4dpWorkflowId, identificationWorkflow.getValue()));
		}

		return workflow;
	}

	@Override
	public void extractInfo(Message message) {
		final CSVContent csvContent = new CSVContent(message.getContent());
		this.hd4dpWorkflowId = csvContent.getWorkflowId();
	}
}
