/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.orchestration.domain.AbstractUserRequest;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "USER_REQUESTS")
public class UserRequest extends AbstractUserRequest {

    @Column(name = "REQUESTED_ON", nullable = false)
    private Timestamp requestedOn;

    @Column(name = "HD4DP_ID", nullable = false)
    private Long hd4dpId;

    @Column(name = "HD4DP_URL", nullable = false)
    private String hd4dpUrl;

    @Fetch(FetchMode.JOIN)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "USER_REQUEST_ARCHIVED_DATA_COLLECTIONS", joinColumns = @JoinColumn(name = "USER_REQUEST_ID"))
    @Column(name = "DATA_COLLECTION_NAME")
    private Set<String> archivedDataCollectionNames;

    @Embedded
    private HealthDataIdentification healthDataIdentification;

    @Fetch(FetchMode.JOIN)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "USER_REQUEST_EMAIL_ADMINS", joinColumns = @JoinColumn(name = "USER_REQUEST_ID"))
    @Column(name = "EMAIL_ADMIN")
    private Set<String> emailAdmins;

    //we don't save the defaultPassword in the database.
    @Transient
    private String password;

    /**
     * can't be in subclass because of bug in hibernate
     * https://hibernate.atlassian.net/browse/HHH-6562
     */
    @Fetch(FetchMode.JOIN)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "USER_REQUEST_DATA_COLLECTIONS", joinColumns = @JoinColumn(name = "USER_REQUEST_ID"))
    @Column(name = "DATA_COLLECTION_NAME")
    private Set<String> dataCollectionNames;

    public Timestamp getRequestedOn() {
        return requestedOn;
    }

    public void setRequestedOn(Timestamp requestedOn) {
        this.requestedOn = requestedOn;
    }

    public Long getHd4dpId() {
        return hd4dpId;
    }

    public void setHd4dpId(Long hd4dpId) {
        this.hd4dpId = hd4dpId;
    }

    public String getHd4dpUrl() {
        return hd4dpUrl;
    }

    public void setHd4dpUrl(String hd4dpUrl) {
        this.hd4dpUrl = hd4dpUrl;
    }

    public Set<String> getArchivedDataCollectionNames() {
        return archivedDataCollectionNames;
    }

    public void setArchivedDataCollectionNames(Set<String> archivedDataCollectionNames) {
        this.archivedDataCollectionNames = archivedDataCollectionNames;
    }

    public HealthDataIdentification getHealthDataIdentification() {
        return healthDataIdentification;
    }

    public void setHealthDataIdentification(HealthDataIdentification healthDataIdentification) {
        this.healthDataIdentification = healthDataIdentification;
    }

    public Set<String> getEmailAdmins() {
        return emailAdmins;
    }

    public void setEmailAdmins(Set<String> emailAdmins) {
        this.emailAdmins = emailAdmins;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<String> getDataCollectionNames() {
        if (dataCollectionNames == null) {
            dataCollectionNames = new HashSet<>();
        }
        return dataCollectionNames;
    }

    public void setDataCollectionNames(Set<String> dataCollectionNames) {
        this.dataCollectionNames = dataCollectionNames;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof UserRequest) {
            UserRequest other = (UserRequest) o;

            return Objects.equals(id, other.id)
                    && Objects.equals(firstName, other.firstName)
                    && Objects.equals(lastName, other.lastName)
                    && Objects.equals(email, other.email)
                    && Objects.equals(dataCollectionNames, other.dataCollectionNames)
                    && Objects.equals(approved, other.approved)
                    && Objects.equals(requestedOn, other.requestedOn)
                    && Objects.equals(healthDataIdentification, other.healthDataIdentification)
                    && Objects.equals(hd4dpId, other.hd4dpId)
                    && Objects.equals(hd4dpUrl, other.hd4dpUrl)
                    && Objects.equals(createdOn, other.createdOn)
                    && Objects.equals(updatedOn, other.updatedOn)
                    && Objects.equals(archivedDataCollectionNames, other.archivedDataCollectionNames)
                    && Objects.equals(emailAdmins, other.emailAdmins);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                this.id,
                this.firstName,
                this.lastName,
                this.email,
                this.dataCollectionNames,
                this.approved,
                this.requestedOn,
                this.healthDataIdentification,
                this.hd4dpId,
                this.hd4dpUrl,
                this.createdOn,
                this.updatedOn,
                this.archivedDataCollectionNames,
                this.emailAdmins);
    }

    @Override
    public String toString() {
        return "UserRequest {" +
                "id = " + Objects.toString(this.id) + ", " +
                "firstName = " + Objects.toString(this.firstName) + ", " +
                "lastName = " + Objects.toString(this.lastName) + ", " +
                "email = " + Objects.toString(this.email) + ", " +
                "dataCollectionNames = " + Objects.toString(this.dataCollectionNames) + ", " +
                "approved = " + Objects.toString(this.approved) + ", " +
                "requestedOn = " + Objects.toString(this.requestedOn) + ", " +
                "healthDataIdentification = " + Objects.toString(this.healthDataIdentification) + ", " +
                "hd4dpId = " + Objects.toString(this.hd4dpId) + ", " +
                "hd4dpUrl = " + Objects.toString(this.hd4dpUrl) + ", " +
                "createdOn = " + Objects.toString(this.createdOn) + ", " +
                "updatedOn = " + Objects.toString(this.updatedOn) + ", " +
                "archivedDataCollectionNames = " + Objects.toString(this.archivedDataCollectionNames) + ", " +
                "emailAdmins = " + Objects.toString(this.emailAdmins) + "}";
    }
}
