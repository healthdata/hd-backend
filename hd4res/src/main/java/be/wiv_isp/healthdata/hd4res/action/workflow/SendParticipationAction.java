/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.action.workflow;

import be.wiv_isp.healthdata.common.dto.DataCollectionGroupDto;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.hd4res.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.Note;
import be.wiv_isp.healthdata.orchestration.domain.SendStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.domain.mapper.NoteMessageDtoMapper;
import be.wiv_isp.healthdata.orchestration.factory.IAbstractMessageFactory;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionGroupForwardService;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import be.wiv_isp.healthdata.orchestration.service.INoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

public class SendParticipationAction extends AbstractParticipationWorkflowAction {

	private static final Logger LOG = LoggerFactory.getLogger(SendParticipationAction.class);

	@Autowired
	private IDataCollectionGroupForwardService catalogueService;
	@Autowired
	private IMessageService messageService;
	@Autowired
	private IAbstractMessageFactory<RegistrationWorkflow, ParticipationWorkflow> messageFactory;
	@Autowired
	protected INoteService noteService;

	@Override
	public WorkflowActionType getAction() {
		return WorkflowActionType.SEND;
	}

	@Override
	public String getSendStatus() {
		return SendStatus.PENDING;
	}

	@Override
	public ParticipationWorkflow doExecute(ParticipationWorkflow workflow, HDUserDetails userDetails) {
		try {
			Long dataCollectionDefinitionId = workflow.getDataCollectionDefinitionId();
			DataCollectionGroupDto group = catalogueService.get(dataCollectionDefinitionId);
			if (!group.isValidForComments(new Date())) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.INVALID_DATE_FOR_DATA_COLLECTION_DEFINITION, dataCollectionDefinitionId, "comments");
				throw exception;
			}
			final UpdateForCorrectionParticipationAction action = new UpdateForCorrectionParticipationAction();
			action.setDocumentVersion(workflow.getDocument().getHd4dpVersion());
			action.setNoteMaxOrder(workflow.getDocument().getNoteMaxOrder());
			List<Note> byDocumentId = noteService.getByDocumentId(WorkflowType.REGISTRATION, workflow.getDocument().getId());
			action.setNotesMessage(NoteMessageDtoMapper.mapToNoteDtos(byDocumentId));

			final Message message = messageFactory.createOutgoingMessage(action, workflow);
			messageService.create(message);

			setEndStatus(WorkflowStatus.CORRECTIONS_REQUESTED);
		} catch (Exception e) {
			LOG.error(MessageFormat.format("error while submitting worklfow: {0}", e.getMessage()), e);
		}
		return workflow;
	}

	@Override
	public WorkflowType getWorkflowType() {
		return WorkflowType.PARTICIPATION;
	}
}
