/*
 *  Copyright 2014-2017 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package be.wiv_isp.healthdata.hd4res.mapper;

import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionGroupHistory;
import org.codehaus.jettison.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.Iterator;

public class DataCollectionGroupHistoryRequestMapper {

	public static DataCollectionGroupHistory convert(JSONObject json) throws Exception {
		DataCollectionGroupHistory dataCollectionGroupHistory = new DataCollectionGroupHistory();
		for (Iterator<String> iterator = json.keys(); iterator.hasNext();) {
			String key = iterator.next();
			if (!"null".equalsIgnoreCase(json.getString(key))) {
				if ("description".equalsIgnoreCase(key)) {
					dataCollectionGroupHistory.setDescription(json.getString(key));
				} else if ("participationContent".equalsIgnoreCase(key)) {
					final JSONObject contentJSON = json.getJSONObject(key);
                    dataCollectionGroupHistory.setParticipationContent(contentJSON.toString().getBytes(StandardCharsets.UTF_8));
				}
			}
		} if(dataCollectionGroupHistory.getParticipationContent() == null) {
			dataCollectionGroupHistory.setParticipationContent(json.toString().getBytes(StandardCharsets.UTF_8));
		}
		return dataCollectionGroupHistory;
	}
}
