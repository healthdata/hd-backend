/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dto.converters;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDto;
import be.wiv_isp.healthdata.common.dto.TranslatableStringDto;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionDefinition;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionDefinitionHistory;
import be.wiv_isp.healthdata.hd4res.domain.dcd.FollowUpDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class DataCollectionDefinitionDtoConverter {

	private static final Logger LOG = LoggerFactory.getLogger(DataCollectionDefinitionDtoConverter.class);

	public static DataCollectionDefinitionDto convert(DataCollectionDefinition dataCollectionDefinition, DataCollectionDefinitionHistory history, List<FollowUpDefinition> followUpDefinitions) {
		DataCollectionDefinitionDto dataCollectionDefinitionDto = null;

		if (dataCollectionDefinition != null) {
			dataCollectionDefinitionDto = new DataCollectionDefinitionDto();

			dataCollectionDefinitionDto.setId(dataCollectionDefinition.getId());
			dataCollectionDefinitionDto.setDataCollectionName(dataCollectionDefinition.getDataCollectionName());
			dataCollectionDefinitionDto.setDataCollectionGroupId(dataCollectionDefinition.getDataCollectionGroupId());
			TranslatableStringDto label = new TranslatableStringDto();
			label.setEn(dataCollectionDefinition.getLabel().getEn());
			label.setFr(dataCollectionDefinition.getLabel().getFr());
			label.setNl(dataCollectionDefinition.getLabel().getNl());
			dataCollectionDefinitionDto.setLabel(label);
			TranslatableStringDto description = new TranslatableStringDto();
			description.setEn(dataCollectionDefinition.getDescription().getEn());
			description.setFr(dataCollectionDefinition.getDescription().getFr());
			description.setNl(dataCollectionDefinition.getDescription().getNl());
			dataCollectionDefinitionDto.setDescription(description);
			dataCollectionDefinitionDto.setContent(history.getContent());
			dataCollectionDefinitionDto.setMinorVersion(dataCollectionDefinition.getMinorVersion());
			dataCollectionDefinitionDto.setPublished(dataCollectionDefinition.isPublished());
			dataCollectionDefinitionDto.setCreatedOn(dataCollectionDefinition.getCreatedOn());
			dataCollectionDefinitionDto.setUpdatedOn(dataCollectionDefinition.getUpdatedOn());
			dataCollectionDefinitionDto.setFollowUpDefinitions(FollowUpDefinitionDtoConverter.convert(followUpDefinitions));
		}

		LOG.info("Converted dataCollectionDefinitionDto : " + dataCollectionDefinitionDto);

		return dataCollectionDefinitionDto;
	}
}
