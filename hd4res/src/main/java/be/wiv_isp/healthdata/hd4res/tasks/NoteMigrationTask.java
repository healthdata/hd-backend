/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4res.tasks;


import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.tasks.AbstractNoteMigrationTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.List;

@Component
public class NoteMigrationTask extends AbstractNoteMigrationTask {

    private static final Logger LOG = LoggerFactory.getLogger(NoteMigrationTask.class);

    @Override
    protected void execute() {
        LOG.info("Migrating all comments to notes.");
        List<AbstractRegistrationWorkflow> workflows = workflowService.getAll();
        Exception firstException = null;
        Long firstFaildWorkflowId = 0L;
        int failCounter = 0, successCounter = 0;
        for (AbstractRegistrationWorkflow workflow : workflows) {
            try {
                noteMigrationService.migrateCommentsToNotes(workflow);
                successCounter++;
            }
            catch(Exception e) {
                if (failCounter == 0) {
                    firstException = e;
                    firstFaildWorkflowId = workflow.getId();
                }
                failCounter++;
            }
        }
        if (failCounter > 0) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, MessageFormat.format("Note migration task failed for {0} workflows, was successful for {1} workflows. The first failure occurred for workflow [{2}] with exception : {3}.", failCounter, successCounter, firstFaildWorkflowId, firstException.getMessage()));
            throw exception;
        }
    }

}
