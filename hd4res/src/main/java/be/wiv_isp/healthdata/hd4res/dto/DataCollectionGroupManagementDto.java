/*
 *  Copyright 2014-2017 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package be.wiv_isp.healthdata.hd4res.dto;

import be.wiv_isp.healthdata.common.dto.PeriodDto;
import be.wiv_isp.healthdata.common.dto.TranslatableStringDto;
import be.wiv_isp.healthdata.common.json.deserializer.JsonToByteArrayDeserializer;
import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.ByteArrayToJsonSerializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import be.wiv_isp.healthdata.common.util.StringUtils;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class DataCollectionGroupManagementDto implements Serializable{

    private static final long serialVersionUID = 1L;

    private Long dataCollectionGroupId;

    private String dataCollectionGroupName;

    private TranslatableStringDto label;

    private TranslatableStringDto description;

    @JsonSerialize(using = ByteArrayToJsonSerializer.class)
    @JsonDeserialize(using = JsonToByteArrayDeserializer.class)
    private byte[] participationContent;

    private String userDescription;

    private Integer majorVersion;

    private boolean published;

    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp createdOn;

    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp updatedOn;

    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp startDate;

    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp endDateCreation;

    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp endDateSubmission;

    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp endDateComments;

    private PeriodDto period;

    private List<DataCollectionDefinitionManagementDto> dataCollectionDefinitions;

    public Long getDataCollectionGroupId() {
        return dataCollectionGroupId;
    }

    public void setDataCollectionGroupId(Long dataCollectionGroupId) {
        this.dataCollectionGroupId = dataCollectionGroupId;
    }

    public String getDataCollectionGroupName() {
        return dataCollectionGroupName;
    }

    public void setDataCollectionGroupName(String dataCollectionGroupName) {
        this.dataCollectionGroupName = dataCollectionGroupName;
    }

    public TranslatableStringDto getLabel() {
        return label;
    }

    public void setLabel(TranslatableStringDto label) {
        this.label = label;
    }

    public TranslatableStringDto getDescription() {
        return description;
    }

    public void setDescription(TranslatableStringDto description) {
        this.description = description;
    }

    public Integer getMajorVersion() {
        return majorVersion;
    }

    public void setMajorVersion(Integer majorVersion) {
        this.majorVersion = majorVersion;
    }


    public byte[] getParticipationContent() {
        return participationContent;
    }

    public void setParticipationContent(byte[] participationContent) {
        this.participationContent = participationContent;
    }

    public String getUserDescription() {
        return userDescription;
    }

    public void setUserDescription(String userDescription) {
        this.userDescription = userDescription;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDateCreation() {
        return endDateCreation;
    }

    public void setEndDateCreation(Timestamp endDateCreation) {
        this.endDateCreation = endDateCreation;
    }

    public PeriodDto getPeriod() {
        return period;
    }

    public void setPeriod(PeriodDto period) {
        this.period = period;
    }

    public Timestamp getEndDateSubmission() {
        return endDateSubmission;
    }

    public void setEndDateSubmission(Timestamp endDateSubmission) {
        this.endDateSubmission = endDateSubmission;
    }

    public Timestamp getEndDateComments() {
        return endDateComments;
    }

    public void setEndDateComments(Timestamp endDateComments) {
        this.endDateComments = endDateComments;
    }

    public boolean isValidForCreation(Date date) {
        return isBetween(startDate, date, endDateCreation);
    }

    public boolean isValidForSubmission(Date date) {
        return isBetween(startDate, date, endDateSubmission);
    }

    public boolean isValidForComments(Date date) {
        return isBetween(startDate, date, endDateComments);
    }

    public List<DataCollectionDefinitionManagementDto> getDataCollectionDefinitions() {
        return dataCollectionDefinitions;
    }

    public void setDataCollectionDefinitions(List<DataCollectionDefinitionManagementDto> dataCollectionDefinitions) {
        this.dataCollectionDefinitions = dataCollectionDefinitions;
    }

    private boolean isBetween(Date start, Date toCheck, Date end) {
        if (toCheck == null) {
            return false;
        }
        if (start == null) {
            return false;
        }
        if (end == null) {
            return toCheck.after(start);
        } else {
            return toCheck.after(start) && toCheck.before(end);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataCollectionGroupManagementDto that = (DataCollectionGroupManagementDto) o;
        return Objects.equals(published, that.published) &&
                Objects.equals(dataCollectionGroupId, that.dataCollectionGroupId) &&
                Objects.equals(dataCollectionGroupName, that.dataCollectionGroupName) &&
                Objects.equals(description, that.description) &&
                Objects.equals(participationContent, that.participationContent) &&
                Objects.equals(majorVersion, that.majorVersion) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(updatedOn, that.updatedOn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dataCollectionGroupId, dataCollectionGroupName, description, participationContent, majorVersion, published, createdOn, updatedOn);
    }


    @Override
    public String toString() {
        return StringUtils.toString(this);
    }
}
