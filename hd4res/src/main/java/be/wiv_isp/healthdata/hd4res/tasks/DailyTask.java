/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.tasks;

import be.wiv_isp.healthdata.common.exception.ExceptionUtils;
import be.wiv_isp.healthdata.orchestration.tasks.AbstractDailyTask;
import be.wiv_isp.healthdata.orchestration.tasks.HealthDataTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DailyTask extends AbstractDailyTask {

    @Autowired
    private FollowUpTask followUpTask;
    @Autowired
    private PendingRegistrationsNotificationTask pendingRegistrationsNotificationTask;

    private List<HealthDataTask> tasks;

    @Override
    protected List<HealthDataTask> getTasks() {
        if(tasks == null) {
            tasks = new ArrayList<>();
            tasks.add(statusMessageTask);
            tasks.add(dataCollectionGroupReplicationTask);
            tasks.add(followUpTask);
            tasks.add(pendingRegistrationsNotificationTask);
            tasks.add(dataCollectionGroupNotificationTask);
            tasks.add(passwordResetTokenCleanUpTask);
        }
        return tasks;
    }

    @Override
    protected void handleException(Exception e) {
        ExceptionUtils.log(e);
    }
}
