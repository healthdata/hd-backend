/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dto.converters;

import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionDefinitionHistory;
import be.wiv_isp.healthdata.hd4res.domain.dcd.FollowUpDefinition;
import be.wiv_isp.healthdata.hd4res.dto.DataCollectionDefinitionHistoryDto;

import java.util.List;

public class DataCollectionDefinitionHistoryDtoConverter {

    public static DataCollectionDefinitionHistoryDto convert(DataCollectionDefinitionHistory dataCollectionDefinitionHistory, List<FollowUpDefinition> followUpDefinitions) {
        final DataCollectionDefinitionHistoryDto dataCollectionDefinitionHistoryDto = new DataCollectionDefinitionHistoryDto();
        dataCollectionDefinitionHistoryDto.setHistoryId(dataCollectionDefinitionHistory.getId());
        dataCollectionDefinitionHistoryDto.setCreatedOn(dataCollectionDefinitionHistory.getCreatedOn());
        dataCollectionDefinitionHistoryDto.setContent(dataCollectionDefinitionHistory.getContent() == null ? null : dataCollectionDefinitionHistory.getContent());
        dataCollectionDefinitionHistoryDto.setUserName(dataCollectionDefinitionHistory.getUserName());
        dataCollectionDefinitionHistoryDto.setDescription(dataCollectionDefinitionHistory.getDescription());
        dataCollectionDefinitionHistoryDto.setFollowUpDefinitions(FollowUpDefinitionDtoConverter.convert(followUpDefinitions));
        return dataCollectionDefinitionHistoryDto;
    }
}
