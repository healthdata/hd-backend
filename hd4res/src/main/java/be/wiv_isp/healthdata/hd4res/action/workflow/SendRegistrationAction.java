/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.action.workflow;

import be.wiv_isp.healthdata.hd4res.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.hd4res.service.IFollowUpService;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.domain.mapper.NoteMessageDtoMapper;
import be.wiv_isp.healthdata.orchestration.factory.IAbstractMessageFactory;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import be.wiv_isp.healthdata.orchestration.service.INoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class SendRegistrationAction extends AbstractRegistrationWorkflowAction {

	private static final Logger LOG = LoggerFactory.getLogger(SendRegistrationAction.class);

	@Autowired
	private IMessageService messageService;
	@Autowired
	private IAbstractMessageFactory<RegistrationWorkflow, ParticipationWorkflow> messageFactory;
	@Autowired
	private IFollowUpService followUpService;
	@Autowired
	private INoteService noteService;

	@Override
	public WorkflowActionType getAction() {
		return WorkflowActionType.SEND;
	}

	@Override
	public String getSendStatus() {
		return SendStatus.PENDING;
	}

	@Override
	public RegistrationWorkflow doExecute(RegistrationWorkflow workflow, HDUserDetails userDetails) {

		final UpdateForCorrectionRegistrationAction action = new UpdateForCorrectionRegistrationAction();
		action.setDocumentVersion(workflow.getDocument().getHd4dpVersion());
		action.setNoteMaxOrder(workflow.getDocument().getNoteMaxOrder());
		List<Note> byDocumentId = noteService.getByDocumentId(WorkflowType.REGISTRATION, workflow.getDocument().getId());
		action.setNotesMessage(NoteMessageDtoMapper.mapToNoteDtos(byDocumentId));

		final Message message = messageFactory.createOutgoingMessage(action, workflow);
		messageService.create(message);

		for (FollowUp followUp : workflow.getFollowUps()) {
			// while waiting for the corrections, no follow-up-related monitoring is possible, because we do not know what follow-ups will be required in the corrected submission.
			if(!followUp.isActive()) {
				followUpService.unschedule(followUp);
			}
		}

		setEndStatus(WorkflowStatus.CORRECTIONS_REQUESTED);
		return workflow;
	}

	@Override
	public WorkflowType getWorkflowType() {
		return WorkflowType.REGISTRATION;
	}
}
