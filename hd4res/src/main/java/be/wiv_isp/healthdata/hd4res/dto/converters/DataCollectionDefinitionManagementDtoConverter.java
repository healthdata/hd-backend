/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dto.converters;

import be.wiv_isp.healthdata.common.dto.TranslatableStringDto;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionDefinition;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionDefinitionHistory;
import be.wiv_isp.healthdata.hd4res.domain.dcd.FollowUpDefinition;
import be.wiv_isp.healthdata.hd4res.dto.DataCollectionDefinitionManagementDto;

import java.util.List;


public class DataCollectionDefinitionManagementDtoConverter {

    public static DataCollectionDefinitionManagementDto convert(DataCollectionDefinition dataCollectionDefinition, DataCollectionDefinitionHistory history, List<FollowUpDefinition> followUpDefinitions) {
        DataCollectionDefinitionManagementDto dto = null;

        if (dataCollectionDefinition != null) {
            dto = new DataCollectionDefinitionManagementDto();
            dto.setDataCollectionDefinitionId(dataCollectionDefinition.getId());
            dto.setDataCollectionDefinitionName(dataCollectionDefinition.getDataCollectionName());
            TranslatableStringDto label = new TranslatableStringDto();
            label.setEn(dataCollectionDefinition.getLabel().getEn());
            label.setFr(dataCollectionDefinition.getLabel().getFr());
            label.setNl(dataCollectionDefinition.getLabel().getNl());
            dto.setLabel(label);
            TranslatableStringDto description = new TranslatableStringDto();
            description.setEn(dataCollectionDefinition.getDescription().getEn());
            description.setFr(dataCollectionDefinition.getDescription().getFr());
            description.setNl(dataCollectionDefinition.getDescription().getNl());
            dto.setDescription(description);
            if (history != null) {
                dto.setContent(history.getContent());
                dto.setUserDescription(history.getDescription());

            }
            dto.setFollowUpDefinitions(FollowUpDefinitionDtoConverter.convert(followUpDefinitions));
            dto.setMinorVersion(dataCollectionDefinition.getMinorVersion());
            dto.setPublished(dataCollectionDefinition.isPublished());
            dto.setCreatedOn(dataCollectionDefinition.getCreatedOn());
            dto.setUpdatedOn(dataCollectionDefinition.getUpdatedOn());
            dto.setDataCollectionGroupId(dataCollectionDefinition.getDataCollectionGroupId());
        }
        return dto;
    }
}
