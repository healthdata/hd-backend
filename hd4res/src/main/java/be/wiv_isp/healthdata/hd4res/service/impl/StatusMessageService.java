/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.PeriodExpression;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.util.DateUtils;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.hd4res.action.standalone.StatusMessageAction;
import be.wiv_isp.healthdata.hd4res.domain.StatusMessage;
import be.wiv_isp.healthdata.hd4res.factory.IMessageFactory;
import be.wiv_isp.healthdata.orchestration.action.StatusMessageContent;
import be.wiv_isp.healthdata.orchestration.dao.IStatusMessageDao;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.search.StatusMessageSearch;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import be.wiv_isp.healthdata.orchestration.service.IStatusMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

@Service
public class StatusMessageService extends AbstractService<StatusMessage, Long, StatusMessageSearch, IStatusMessageDao<StatusMessage>> implements IStatusMessageService<StatusMessage> {

    private static final Logger LOG = LoggerFactory.getLogger(StatusMessageService.class);

    @Autowired
    protected IStatusMessageDao<StatusMessage> dao;

    @Autowired
    private IMessageService messageService;

    @Autowired
    private IMessageFactory messageFactory;

    @Autowired
    private IConfigurationService configurationService;

    public StatusMessageService() {
        super(StatusMessage.class);
    }

    @Override
    public IStatusMessageDao<StatusMessage> getDao() {
        return dao;
    }

    @Override
    public void send() {
        LOG.info("Sending status messages");

        final Configuration configuration = configurationService.get(ConfigurationKey.STATUS_MESSAGE_VALIDITY_PERIOD);
        final PeriodExpression statusMessageValidityPeriod = new PeriodExpression(configuration.getValue());
        final Date minCreatedOn = DateUtils.remove(new Date(), statusMessageValidityPeriod);

        final StatusMessageSearch search = new StatusMessageSearch();
        search.setLastOnly(true);
        search.setCreatedOnAfter(minCreatedOn);
        final List<StatusMessage> statusMessages = getAll(search);

        for(final StatusMessage statusMessage : statusMessages) {
            LOG.debug(MessageFormat.format("Replying on status message with sender [{0}]", statusMessage.getHealthDataIdentification()));

            final StatusMessageAction action = new StatusMessageAction();
            action.setContent(new StatusMessageContent());
            action.setHealthDataIdentification(statusMessage.getHealthDataIdentification());

            final Message outboxMessage = messageFactory.createOutgoingMessage(action, statusMessage.getMessage());
            messageService.create(outboxMessage);
        }
    }
}
