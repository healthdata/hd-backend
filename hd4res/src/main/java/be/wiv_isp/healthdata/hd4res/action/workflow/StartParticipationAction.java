/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.action.workflow;

import be.wiv_isp.healthdata.common.dto.DataCollectionGroupDto;
import be.wiv_isp.healthdata.hd4res.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.search.ParticipationWorkflowSearch;
import be.wiv_isp.healthdata.hd4res.service.IDataCollectionService;
import be.wiv_isp.healthdata.orchestration.domain.CSVContent;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.factory.IAbstractMessageFactory;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionGroupForwardService;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.MessageFormat;

public class StartParticipationAction extends AbstractParticipationWorkflowAction {

	private static final Logger LOG = LoggerFactory.getLogger(StartParticipationAction.class);

	@Autowired
	private IDataCollectionGroupForwardService catalogueService;
	@Autowired
	private IOrganizationService organizationService;
	@Autowired
	private IDataCollectionService dataCollectionService;
	@Autowired
	private IMessageService messageService;
	@Autowired
	private IAbstractMessageFactory<RegistrationWorkflow, ParticipationWorkflow> messageFactory;

	@Override
	public WorkflowActionType getAction() {
		return WorkflowActionType.START;
	}

	@Override
	public ParticipationWorkflow doExecute(ParticipationWorkflow workflow, HDUserDetails userDetails) {
		final DataCollectionGroupDto dcg = catalogueService.get(dataCollectionDefinitionId);

		if (workflow == null) {
			workflow = createWorkflow(dcg);
			setEndStatus(WorkflowStatus.NEW);
		} else {
			setEndStatus(workflow.getStatus());
		}
		if (!isAuthorized(dcg, identificationWorkflow)) {
			processAuthorizationFailed(workflow);
			return workflow;
		}
		workflow.setCompleted(false);
		workflow.setStartedOn(startedOn);
		workflow.setCompletedOn(null);
		workflow.setIdentificationName(identificationWorkflow.getName());

		return workflow;
	}


	private ParticipationWorkflow createWorkflow(DataCollectionGroupDto dcg) {
		LOG.debug("ParticipationWorkflow does not exist, it must be created");
		final ParticipationWorkflow workflow = getWorkflowService().create(organizationService.getMain());

		workflow.setDataCollectionDefinitionId(dataCollectionDefinitionId);
		workflow.setDataCollectionName(dcg.getName());
		workflow.setHd4dpWorkflowId(hd4dpWorkflowId);
		workflow.setIdentificationType(identificationWorkflow.getType());
		workflow.setIdentificationValue(identificationWorkflow.getValue());

		return workflow;
	}

	private boolean isAuthorized(DataCollectionGroupDto dcg, HealthDataIdentification identification) {
		return dataCollectionService.accept(dcg.getName(), identification.getType(), identification.getValue());
	}

	private void processAuthorizationFailed(ParticipationWorkflow workflow) {
		LOG.error(MessageFormat.format("Authorization failed for workflow {0}", workflow.getId().toString()));
		setEndStatus(WorkflowStatus.AUTHORIZATION_FAILED);
		final UpdateStatusParticipationAction action = new UpdateStatusParticipationAction();
		action.setEndStatus(WorkflowStatus.UNAUTHORIZED);
		final Message message = messageFactory.createOutgoingMessage(action, workflow);
		messageService.create(message);
	}


	@Override
	public ParticipationWorkflow retrieveWorkflow() {
		ParticipationWorkflowSearch search = new ParticipationWorkflowSearch();
		search.setDataCollectionDefinitionId(dataCollectionDefinitionId);
		search.setIdentificationType(getIdentificationWorkflow().getType());
		search.setIdentificationValue(getIdentificationWorkflow().getValue());
		search.setWithDocument(null);

		return getWorkflowService().getUnique(search);
	}

	@Override
	public void extractInfo(Message message) {
		final CSVContent csvContent = new CSVContent(message.getContent());
		this.hd4dpWorkflowId = csvContent.getWorkflowId();
	}
}
