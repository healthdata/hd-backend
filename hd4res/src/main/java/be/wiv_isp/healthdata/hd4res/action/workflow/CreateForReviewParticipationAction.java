/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.action.workflow;

import be.wiv_isp.healthdata.common.dto.DataCollectionGroupDto;
import be.wiv_isp.healthdata.hd4res.domain.ParticipationDocument;
import be.wiv_isp.healthdata.hd4res.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.search.ParticipationWorkflowSearch;
import be.wiv_isp.healthdata.hd4res.service.IDataCollectionService;
import be.wiv_isp.healthdata.orchestration.action.workflow.ReportableAction;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.domain.mapper.BooleanConfigurationMapper;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import be.wiv_isp.healthdata.orchestration.factory.IAbstractMessageFactory;
import be.wiv_isp.healthdata.orchestration.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;

public class CreateForReviewParticipationAction extends AbstractParticipationWorkflowAction implements ReportableAction {

	private static final Logger LOG = LoggerFactory.getLogger(CreateForReviewParticipationAction.class);

	@Autowired
	private IDataCollectionService dataCollectionService;
	@Autowired
	private IDataCollectionGroupForwardService catalogueService;
	@Autowired
	private IConfigurationService configurationService;
	@Autowired
	private IMappingService mappingService;
	@Autowired
	private IMessageService messageService;
	@Autowired
	private IAbstractMessageFactory<RegistrationWorkflow, ParticipationWorkflow> messageFactory;
	@Autowired
	protected INoteService noteService;

	@Value("${host}")
	private String host;

	@Override
	public WorkflowActionType getAction() {
		return WorkflowActionType.CREATE_FOR_REVIEW;
	}

	@Override
	public ParticipationWorkflow retrieveWorkflow() {
		LOG.debug(MessageFormat.format("Searching for workflow with hd4dp_workflow_id [{0}] and identification_value [{1}]", hd4dpWorkflowId, identificationWorkflow.getValue()));
		final ParticipationWorkflowSearch search = new ParticipationWorkflowSearch();
		search.setHd4dpWorkflowId(hd4dpWorkflowId);
		search.setIdentificationValue(identificationWorkflow.getValue());
		search.setWithDocument(null);
		final ParticipationWorkflow workflow = getWorkflowService().getUnique(search);

		if (workflow == null) {
			LOG.info(MessageFormat.format("No workflow found with hd4dp_workflow_id [{0}] and identification_value [{1}]", hd4dpWorkflowId, identificationWorkflow.getValue()));
		}

		return workflow;
	}

	@Override
	public ParticipationWorkflow doExecute(ParticipationWorkflow workflow, HDUserDetails userDetails) {
		final DataCollectionGroupDto group = catalogueService.get(dataCollectionDefinitionId);

		setEndStatus(WorkflowStatus.NEW);
		if (!isAuthorized(group, identificationWorkflow)) {
			processAuthorizationFailed(workflow);
			return workflow;
		}

		createDocument(workflow, group);

		if (!isValid(group, documentContent)) {
			processValidationFailed(workflow);
			return workflow;
		}
		return workflow;
	}

	private void createDocument(ParticipationWorkflow workflow, DataCollectionGroupDto group) {
		markNonLoadedDocumentObsolete(workflow);
		DocumentData documentData = new DocumentData();
		documentData.setContent(documentContent);
		workflowService.setDocumentData(workflow, documentData, true);
		final String csv = mappingService.mapFromJsonToCsv(workflow.getDocument().getDocumentContent(), group.getParticipationContent(), false);
		workflow.getDocument().setCsv(csv.getBytes(StandardCharsets.UTF_8));
		workflow.getDocument().setHd4dpVersion(documentVersion);
		workflow.getDocument().setNoteMaxOrder(noteMaxOrder);
		noteService.updateNotesFromMessage(WorkflowType.REGISTRATION, workflow.getDocument().getId(), notesMessage);
	}

	private void markNonLoadedDocumentObsolete(ParticipationWorkflow workflow) {
		if(workflow.getDocumentHistory() == null) {
			return;
		}

		for(ParticipationDocument document : workflow.getDocumentHistory()) {
			if(document.getDwhStatus() == null) {
				document.setDwhStatus(AbstractDocument.DwhStatus.O);
			}
		}
	}

	private void processValidationFailed(ParticipationWorkflow workflow) {
		LOG.error(MessageFormat.format("Validation failed for workflow {0}", workflow.getId().toString()));
		setEndStatus(WorkflowStatus.VALIDATION_FAILED);
	}

	private void processAuthorizationFailed(ParticipationWorkflow workflow) {
		LOG.error(MessageFormat.format("Authorization failed for workflow {0}", workflow.getId().toString()));
		setEndStatus(WorkflowStatus.AUTHORIZATION_FAILED);
		final UpdateStatusParticipationAction action = new UpdateStatusParticipationAction();
		action.setEndStatus(WorkflowStatus.UNAUTHORIZED);
		final Message message = messageFactory.createOutgoingMessage(action, workflow);
		messageService.create(message);
	}

	private boolean isValid(DataCollectionGroupDto group, byte[] documentContent) {
		boolean useValidation = BooleanConfigurationMapper.map(configurationService.get(ConfigurationKey.USE_VALIDATION));

		if (useValidation) {
			return mappingService.isValid(documentContent, group.getParticipationContent());
		}

		return true;
	}

	private boolean isAuthorized(DataCollectionGroupDto group, HealthDataIdentification identification) {
		return dataCollectionService.accept(group.getName(), identification.getType(), identification.getValue());
	}

	@Override
	public void extractInfo(Message message) {
		final CSVContent csvContent = new CSVContent(message.getContent());
		this.hd4dpWorkflowId = csvContent.getWorkflowId();
	}

	@Override
	public WorkflowType getWorkflowType() {
		return WorkflowType.PARTICIPATION;
	}

	@Override
	public String getSource() {
		return source;
	}

	@Override
	public String getFile() {
		return file;
	}

	@Override
	public String getMessageType() {
		return messageType;
	}

	@Override
	public Progress getProgress() {
		return null;
	}

}
