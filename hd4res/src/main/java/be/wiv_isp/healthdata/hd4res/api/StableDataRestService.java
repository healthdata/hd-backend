/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.api;

import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.hd4res.domain.StableData;
import be.wiv_isp.healthdata.hd4res.domain.StableDataCountResult;
import be.wiv_isp.healthdata.hd4res.domain.StableDataUploadRequest;
import be.wiv_isp.healthdata.hd4res.domain.search.StableDataSearch;
import be.wiv_isp.healthdata.hd4res.mapper.StableDataUploadRequestMapper;
import be.wiv_isp.healthdata.hd4res.service.IStableDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Component
@Path("/stabledata")
public class StableDataRestService {

	private static final Logger LOG = LoggerFactory.getLogger(StableDataRestService.class);

	@Autowired
	private IStableDataService stableDataService;

	@POST
	@Path("/upload")
	@Produces(MediaType.APPLICATION_JSON)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Response upload(String json) {
		LOG.info("Stable data upload request received");
		final StableDataUploadRequest request = StableDataUploadRequestMapper.map(JsonUtils.createJsonObject(json));
		final List<StableData> result = stableDataService.execute(request);
		return Response.ok(result).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Path("/{id}")
	public Response get(@PathParam("id") Long id) {
		final StableData stableData = stableDataService.get(id);

		if(stableData == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}

		return Response.ok(stableData).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Response getAll(@Context UriInfo info) {
		final StableDataSearch search = new StableDataSearch();
		search.setDataCollectionName(RestUtils.getParameterString(info, "dataCollectionName"));
		search.setIdentificationValue(RestUtils.getParameterString(info, "identificationValue"));
		final List<StableDataCountResult> counts = stableDataService.getCounts(search);
		return Response.ok(counts).build();
	}
}
