/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.hd4res.api;

import be.wiv_isp.healthdata.orchestration.dto.HealthDataIdentificationDto;
import be.wiv_isp.healthdata.common.dto.SalesForceOrganizationDto;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.hd4res.service.IDataCollectionService;
import be.wiv_isp.healthdata.orchestration.api.AbstractDataCollectionRestService;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
@Path("/datacollections")
public class DataCollectionRestService extends AbstractDataCollectionRestService {

    @Autowired
    protected IDataCollectionService dataCollectionService;

    @GET
    @Path("/{dataCollectionName}/dataproviders")
    @PreAuthorize("hasRole('ROLE_USER')")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDataProviders(@Context UriInfo info) {
        final HDUserDetails principal = AuthenticationService.getUserDetails();
        final String dataCollectionName = RestUtils.getPathString(info, RestUtils.ParameterName.DATA_COLLECTION_NAME);

        if(!principal.getAuthorities().contains(new Authority(Authority.ADMIN))) {
            if(dataCollectionName == null || !userDataCollectionService.isUserAuthorized(principal, dataCollectionName)) {
                return Response.status(Response.Status.FORBIDDEN).build();
            }
        }

        final List<SalesForceOrganizationDto> dataProviders = dataCollectionService.getAuthorizedOrganizations(dataCollectionName);
        return Response.ok(convert(dataProviders)).build();
    }

    private List<HealthDataIdentificationDto> convert(Collection<SalesForceOrganizationDto> dtos) {
        final List<HealthDataIdentificationDto> res = new ArrayList<>();
        for (SalesForceOrganizationDto dto : dtos) {
            res.add(convert(dto));
        }
        return res;
    }

    private HealthDataIdentificationDto convert(SalesForceOrganizationDto organization) {
        final HealthDataIdentificationDto res = new HealthDataIdentificationDto();
        res.setType(organization.getIdentificationType());
        res.setValue(organization.getIdentificationValue());
        res.setName(organization.getName());
        res.setHd4prc(organization.isHd4prc());
        return res;
    }
}
