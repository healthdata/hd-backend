/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.hd4res.service.IDwhLinkNissService;
import be.wiv_isp.healthdata.hd4res.service.IReadableIdentifierService;
import be.wiv_isp.healthdata.orchestration.dao.IRegistrationWorkflowDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
public class DwhLinkNissService implements IDwhLinkNissService {

	private static final Logger LOG = LoggerFactory.getLogger(DwhLinkNissService.class);

	@Autowired
	private IRegistrationWorkflowDao workflowDao;

	@Autowired
	private IReadableIdentifierService readableIdentifierService;

	@Override
	@Transactional
	public void populateOldPatientIds() {
		Set<String> recodeDataCollectionNames = readableIdentifierService.getRecodeDataCollectionNames();
		for (String recodeDataCollectionName : recodeDataCollectionNames) {
			LOG.info("searching for all workflows of DCD [{}]", recodeDataCollectionName);
			RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
			search.setDataCollectionName(recodeDataCollectionName);
			List<RegistrationWorkflow> workflows = workflowDao.getAll(search);
			for (RegistrationWorkflow workflow : workflows) {
				try {
					readableIdentifierService.registerOldPatiendId(workflow);
				} catch (Exception e) {
					LOG.error("Couldn't register old patientId for workflow [{}]", workflow.getId());
				}
			}
		}
	}
}
