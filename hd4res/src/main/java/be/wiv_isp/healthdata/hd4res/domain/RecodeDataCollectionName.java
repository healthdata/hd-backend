/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.hd4res.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "RECODE_DCD_NAMES")
public class RecodeDataCollectionName {

	@Id
	@Column(name = "NAME")
	private String name;

	@Column(name = "OLD_ID_PROPERTY")
	private String oldIdProperty;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOldIdProperty() {
		return oldIdProperty;
	}

	public void setOldIdProperty(String oldIdProperty) {
		this.oldIdProperty = oldIdProperty;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof RecodeDataCollectionName) {
			RecodeDataCollectionName other = (RecodeDataCollectionName) o;

			return Objects.equals(name, other.name) &&
					Objects.equals(oldIdProperty, other.oldIdProperty);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, oldIdProperty);
	}
}
