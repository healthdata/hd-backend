/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.hd4res.dao.IDataCollectionDefinitionDao;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionDefinition;
import be.wiv_isp.healthdata.hd4res.domain.search.DataCollectionDefinitionSearch;
import be.wiv_isp.healthdata.hd4res.service.IDataCollectionDefinitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class DataCollectionDefinitionService extends AbstractService<DataCollectionDefinition, Long, DataCollectionDefinitionSearch, IDataCollectionDefinitionDao> implements IDataCollectionDefinitionService {

	@Autowired
	private IDataCollectionDefinitionDao dao;

	public DataCollectionDefinitionService() {
		super(DataCollectionDefinition.class);
	}

	@Override
	public List<DataCollectionDefinition> get(String name) {
		return dao.get(name);
	}

	@Override
	public List<DataCollectionDefinition> get(String name, Long groupId) {
		if (groupId == null)
			return get(name);
		DataCollectionDefinitionSearch search = new DataCollectionDefinitionSearch();
		search.setDataCollectionDefinitionName(name);
		search.setDataCollectionGroupId(groupId);
		return dao.getAll(search);
	}

	@Override
	public List<DataCollectionDefinition> getAll(Long groupId) {
		if (groupId == null)
			return getAll();
		DataCollectionDefinitionSearch search = new DataCollectionDefinitionSearch();
		search.setDataCollectionGroupId(groupId);
		return dao.getAll(search);
	}

	@Override
	public List<DataCollectionDefinition> getLatestMinors(Long dataCollectionGroupId) {
		return dao.latestMinors(dataCollectionGroupId);
	}

	@Override
	public DataCollectionDefinition getNextMinor(DataCollectionDefinition dataCollectionDefinition) {
		return getNextMinor(dataCollectionDefinition.getDataCollectionName(), dataCollectionDefinition.getDataCollectionGroupId(), dataCollectionDefinition.getMinorVersion());
	}

	private DataCollectionDefinition getNextMinor(String name, Long dataCollectionGroupId, int minor) {
		DataCollectionDefinitionSearch search = new DataCollectionDefinitionSearch();
		search.setDataCollectionDefinitionName(name);
		search.setDataCollectionGroupId(dataCollectionGroupId);
		search.setMinorVersion(minor + 1);
		return  dao.getUnique(search);
	}

	@Override
	public List<String> getExistingDataCollections() {
		return dao.getExistingDataCollections();
	}

	@Override
	protected IDataCollectionDefinitionDao getDao() {
		return dao;
	}
}
