/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.api;

import be.wiv_isp.healthdata.orchestration.annotation.Auditable;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ApiType;
import be.wiv_isp.healthdata.hd4res.service.IDwhCsvService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Component
@Path("/dwh/csv")
public class DwhCsvRestService {

	private static final Logger LOG = LoggerFactory.getLogger(DwhCsvRestService.class);

	@Autowired
	private IDwhCsvService dwhCsvService;

	@POST
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DWH)
	public Response getAll() {
		LOG.info("Populate CSV content in DOCUMENTS.");
		dwhCsvService.populate();
		return Response.noContent().build();
	}

	@POST
	@Path("/dates/all")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DWH)
	public Response validateDates() {
		LOG.info("Validate CSV dates and the json dates in DOCUMENTS.");
		dwhCsvService.validateDates();
		return Response.noContent().build();
	}

	@POST
	@Path("/dates/workflowid/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DWH)
	public Response validateDates(@PathParam("id") Long id) {
		LOG.info("Validate CSV dates and the json dates in DOCUMENTS.");
		dwhCsvService.validateDates(id);
		return Response.noContent().build();
	}

	@POST
	@Path("/dates/datacollectionname/{datacollectionname}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DWH)
	public Response validateDates(@PathParam("datacollectionname") String dataCollectionName) {
		LOG.info("Validate CSV dates and the json dates in DOCUMENTS.");
		dwhCsvService.validateDates(dataCollectionName);
		return Response.noContent().build();
	}
}
