/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.encrypt.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDao;
import be.wiv_isp.healthdata.hd4res.domain.search.Hd4ResSsinSearch;
import be.wiv_isp.healthdata.hd4res.encrypt.dao.IHd4ResSsinDao;
import be.wiv_isp.healthdata.hd4res.encrypt.domain.Hd4ResSsin;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class Hd4ResSsinDao extends CrudDao<Hd4ResSsin, String> implements IHd4ResSsinDao {

	public Hd4ResSsinDao() {
		super(Hd4ResSsin.class);
	}

	@Override
	public String getBiggest(String registerName) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Hd4ResSsin> cq = cb.createQuery(Hd4ResSsin.class);
		Root<Hd4ResSsin> rootEntry = cq.from(Hd4ResSsin.class);
		CriteriaQuery<Hd4ResSsin> all = cq.select(rootEntry);

		List<Predicate> predicates = new ArrayList<>();
		if (!StringUtils.isBlank(registerName)) {
			predicates.add(cb.equal(rootEntry.get("registerName"), registerName));
			predicates.add(cb.like(rootEntry.<String> get("landingZoneCodedNiss"), registerName + "\\_PATIENTID\\_____-____", '\\'));
		}
		cq.orderBy(cb.desc(rootEntry.get("landingZoneCodedNiss")));

		if (!predicates.isEmpty()) {
			cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		}

		TypedQuery<Hd4ResSsin> query = em.createQuery(all);
		query.setMaxResults(1);
		List<Hd4ResSsin> resultList = query.getResultList();
		if (resultList.isEmpty()) {
			return null;
		} else {
			return resultList.get(0).getLandingZoneCodedNiss();
		}
	}

	public List<Hd4ResSsin> getAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Hd4ResSsin> cq = cb.createQuery(Hd4ResSsin.class);
		Root<Hd4ResSsin> rootEntry = cq.from(Hd4ResSsin.class);
		CriteriaQuery<Hd4ResSsin> all = cq.select(rootEntry);
		TypedQuery<Hd4ResSsin> allQuery = em.createQuery(all);
		return allQuery.getResultList();
	}

	@Override
	public List<Hd4ResSsin> getAll(Hd4ResSsinSearch search) {
		if (search == null) {
			return getAll();
		}
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Hd4ResSsin> cq = cb.createQuery(Hd4ResSsin.class);
		Root<Hd4ResSsin> rootEntry = cq.from(Hd4ResSsin.class);

		List<Predicate> predicates = new ArrayList<>();
		if (search.getRegisterName() != null) {
			predicates.add(cb.equal(rootEntry.get("registerName"), search.getRegisterName()));
		}
		if (search.geteHealthCodedNissBatch() != null) {
			predicates.add(cb.equal(rootEntry.get("eHealthCodedNissBatch"), search.geteHealthCodedNissBatch()));
		}
		if (search.getLandingZoneCodedNiss() != null) {
			predicates.add(cb.equal(rootEntry.get("landingZoneCodedNiss"), search.getLandingZoneCodedNiss()));
		}
		if (!predicates.isEmpty()) {
			cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		}

		TypedQuery<Hd4ResSsin> allQuery = em.createQuery(cq);

		return allQuery.getResultList();
	}

	@Override
	public List<Hd4ResSsin> getPossibleOldIdMatches(String dataCollectionName, String oldPatientId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Hd4ResSsin> cq = cb.createQuery(Hd4ResSsin.class);
		Root<Hd4ResSsin> rootEntry = cq.from(Hd4ResSsin.class);

		List<Predicate> predicates = new ArrayList<>();
		predicates.add(cb.equal(rootEntry.get("registerName"), dataCollectionName));
		predicates.add(cb.like(rootEntry.<String> get("oldPatientIds"), String.format("%%%s%%", oldPatientId)));
		cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<Hd4ResSsin> allQuery = em.createQuery(cq);

		return allQuery.getResultList();
	}
}
