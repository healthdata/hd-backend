/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.hd4res.dao.IEmVersionDao;
import be.wiv_isp.healthdata.hd4res.domain.EmVersion;
import be.wiv_isp.healthdata.hd4res.domain.search.EmVersionSearch;
import be.wiv_isp.healthdata.orchestration.domain.MessageCorrespondent;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractEmVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EmVersionService extends AbstractEmVersionService {

    @Autowired
    private IEmVersionDao emVersionDao;

    @Override
    @Transactional
    public Boolean supportsMultiLine(MessageCorrespondent messageCorrespondent) {
        EmVersionSearch search = new EmVersionSearch();
        search.setMessageCorrespondent(messageCorrespondent);

        List<EmVersion> result = emVersionDao.getAll(search);
        if (result.isEmpty()) {
            return Boolean.FALSE;
        }
        else {
            return result.get(0).isMultiLine();
        }
    }

    @Override
    @Transactional
    public void updateCorrespondent(MessageCorrespondent messageCorrespondent, Boolean multiline) {
        EmVersionSearch search = new EmVersionSearch();
        search.setMessageCorrespondent(messageCorrespondent);

        List<EmVersion> result = emVersionDao.getAll(search);
        if (result.isEmpty()) {
            EmVersion emVersion = new EmVersion();
            emVersion.setMessageCorrespondent(messageCorrespondent);
            emVersion.setMultiLine(multiline);
            emVersionDao.create(emVersion);
        }
        else {
            EmVersion emVersion = result.get(0);
            if (multiline!=null && !multiline.equals(emVersion.isMultiLine())) {
                emVersion.setMultiLine(multiline);
                emVersionDao.update(emVersion);
            }
        }
    }

}
