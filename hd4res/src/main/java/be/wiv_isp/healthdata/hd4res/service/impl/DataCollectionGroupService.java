/*
 *  Copyright 2014-2017 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.hd4res.dao.IDataCollectionGroupDao;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionGroup;
import be.wiv_isp.healthdata.hd4res.domain.search.DataCollectionGroupSearch;
import be.wiv_isp.healthdata.hd4res.service.IDataCollectionGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DataCollectionGroupService extends AbstractService<DataCollectionGroup, Long, DataCollectionGroupSearch, IDataCollectionGroupDao> implements IDataCollectionGroupService {

	public DataCollectionGroupService() {
		super(DataCollectionGroup.class);
	}

	@Autowired
	private IDataCollectionGroupDao dataCollectionGroupDao;



	@Override
	public List<DataCollectionGroup> get(String name) {
		return dataCollectionGroupDao.get(name);
	}

	@Override
	public List<String> getExistingGroups() {
		return dataCollectionGroupDao.getExistingGroups();
	}

	@Override
	public void delete(DataCollectionGroup dataCollectionGroup) {
		dataCollectionGroupDao.delete(dataCollectionGroup);
	}


	@Override
	protected IDataCollectionGroupDao getDao() {
		return dataCollectionGroupDao;
	}
}
