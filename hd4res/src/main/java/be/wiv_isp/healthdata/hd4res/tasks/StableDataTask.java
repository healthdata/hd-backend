/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.tasks;

import be.wiv_isp.healthdata.orchestration.domain.mapper.IntegerConfigurationMapper;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.common.domain.search.Field;
import be.wiv_isp.healthdata.common.domain.search.Ordering;
import be.wiv_isp.healthdata.hd4res.domain.StableData;
import be.wiv_isp.healthdata.hd4res.domain.search.StableDataSearch;
import be.wiv_isp.healthdata.hd4res.service.IStableDataService;
import be.wiv_isp.healthdata.orchestration.tasks.SchedulableHealthDataTask;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.List;

@Component
public class StableDataTask extends SchedulableHealthDataTask {

    private static final Logger LOG = LoggerFactory.getLogger(StableDataTask.class);

    @Autowired
    private IStableDataService stableDataService;

    @Override
    public String getName() {
        return "STABLE_DATA_UPLOAD";
    }

    @Override
    protected String getTimingExpression() {
        return configurationService.get(ConfigurationKey.STABLE_DATA_TASK_TRIGGER).getValue();
    }

    @Override
    protected void execute() {
        int maxSent = IntegerConfigurationMapper.map(configurationService.get(ConfigurationKey.STABLE_DATA_RECORDS_PER_RUN));

        int totalSent = 0;
        boolean remainingStableData = true;
        while(remainingStableData && totalSent < maxSent) {
            int maxResult = maxSent - totalSent;

            final StableDataSearch search = new StableDataSearch();
            search.setStatus(StableData.Status.NEW);
            search.setOrdering(new Ordering(Field.CREATED_ON, Ordering.Type.ASC));
            search.setMaxResults(maxResult);

            final List<StableData> stableDataList = stableDataService.getAll(search);

            if(CollectionUtils.isNotEmpty(stableDataList)) {
                int nbSent = 0;
                for (StableData stableData : stableDataList) {
                    try {
                        stableDataService.send(stableData);
                        nbSent++;
                    } catch (Exception e) {
                        LOG.error(MessageFormat.format("Error while sending stable data {0}. Updating status to {1}. Error message: {2}", stableData.toString(), StableData.Status.ERROR_SENDING, e.getMessage()), e);
                        stableData.setStatus(StableData.Status.ERROR_SENDING);
                        stableDataService.update(stableData);
                    }
                }
                totalSent += nbSent;
                LOG.info("{} stable data records sent", nbSent);
            } else {
                remainingStableData = false;
            }
        }
    }
}
