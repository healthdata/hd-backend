/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.action.standalone;

import be.wiv_isp.healthdata.orchestration.domain.mapper.BooleanConfigurationMapper;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.common.domain.enumeration.TemplateKey;
import be.wiv_isp.healthdata.hd4res.domain.StableData;
import be.wiv_isp.healthdata.hd4res.domain.StableDataCountResult;
import be.wiv_isp.healthdata.hd4res.domain.StableDataCountsResult;
import be.wiv_isp.healthdata.hd4res.domain.search.StableDataSearch;
import be.wiv_isp.healthdata.hd4res.service.IStableDataService;
import be.wiv_isp.healthdata.orchestration.action.standalone.AbstractStableDataUploadAction;
import be.wiv_isp.healthdata.orchestration.domain.mail.DataCollectionDefinitionContext;
import be.wiv_isp.healthdata.orchestration.service.IMailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class StableDataUploadAction extends AbstractStableDataUploadAction {

    private static final Logger LOG = LoggerFactory.getLogger(StableDataUploadAction.class);

    @Autowired
    private IStableDataService stableDataService;

    @Autowired
    private IMailService mailService;

    @Autowired
    private IConfigurationService configurationService;

    @Override
    @Transactional
    public void execute() {
        for (Long id : successes) {
            StableData stableData = stableDataService.get(id);
            stableData.setStatus(StableData.Status.UPLOADED);
            stableDataService.update(stableData);
        }

        for (Long id : failures) {
            StableData stableData = stableDataService.get(id);
            stableData.setStatus(StableData.Status.ERROR_UPLOADING);
            stableDataService.update(stableData);
        }

        boolean notificationsEnabled = BooleanConfigurationMapper.map(configurationService.get(ConfigurationKey.UPLOADED_STABLE_DATA_NOTIFICATION_ENABLED));

        if(notificationsEnabled) {
            final StableDataSearch search = new StableDataSearch();
            search.setDataCollectionName(dataCollectionName);
            search.setIdentificationValue(healthDataIdentification.getValue());
            final List<StableDataCountResult> countResult = stableDataService.getCounts(search);
            final StableDataCountsResult counts = stableDataService.convert(countResult);

            final long total = counts.getCount(healthDataIdentification.getValue(), dataCollectionName, null);
            final long uploadedCount = counts.getCount(healthDataIdentification.getValue(), dataCollectionName, StableData.Status.UPLOADED);

            if (uploadedCount == total) {
                LOG.info("Notifying users that stable data have been uploaded for installation with id {} and data collection {}", healthDataIdentification.getValue(), dataCollectionName);

                final DataCollectionDefinitionContext context = new DataCollectionDefinitionContext();
                context.setDataCollectionName(dataCollectionName);

                mailService.createAndSendMail(TemplateKey.UPLOADED_STABLE_DATA, context, emails);
            }
        }
    }
}
