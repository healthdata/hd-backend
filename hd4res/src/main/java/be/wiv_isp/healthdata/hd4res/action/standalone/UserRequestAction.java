/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.action.standalone;

import be.wiv_isp.healthdata.hd4res.domain.UserRequest;
import be.wiv_isp.healthdata.hd4res.service.IUserRequestService;
import be.wiv_isp.healthdata.orchestration.action.standalone.AbstractUserRequestAction;
import be.wiv_isp.healthdata.orchestration.service.IUserRequestMailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;

public class UserRequestAction extends AbstractUserRequestAction {

    private static final Logger LOG = LoggerFactory.getLogger(UserRequestAction.class);

    @Autowired
    private IUserRequestService userRequestService;
    @Autowired
    private IUserRequestMailService userRequestMailService;

    @Override
    public void execute() {
        UserRequest userRequest = userRequestService.get(userRequestDto.getHd4dpId(), getHealthDataIdentification());
        if(userRequest == null) {
            userRequest = new UserRequest();
            mapCommonParameters(userRequest);
            userRequest.setHd4dpId(userRequestDto.getHd4dpId());
            userRequestService.create(userRequest);
        } else {
            if (userRequest.getRequestedOn() == null)
                userRequest.setRequestedOn(new Timestamp(new Date().getTime()));
            mapCommonParameters(userRequest);
            userRequest.setArchivedDataCollectionNames(null);
            userRequestService.update(userRequest);
        }
        if(Boolean.TRUE.equals(userRequestDto.getEmailSent())) {
            LOG.info("Mail notifications were sent by HD4DP. Not sending mail notifications again.");
        } else {
            LOG.warn("Mail notifications were not sent by HD4DP. Sending mail notifications using HD4RES mail server.");
            userRequestMailService.sendMails(userRequestDto);
        }
    }

    private void mapCommonParameters(UserRequest userRequest) {
        userRequest.setUsername(userRequestDto.getUsername());
        userRequest.setPassword(userRequestDto.getDefaultPassword());
        userRequest.setFirstName(userRequestDto.getFirstName());
        userRequest.setLastName(userRequestDto.getLastName());
        userRequest.setEmail(userRequestDto.getEmail());
        userRequest.setEmailRequester(userRequestDto.getEmailRequester());
        userRequest.setApproved(userRequestDto.getApproved());
        userRequest.setRequestedOn(userRequestDto.getRequestedOn());
        if (userRequest.getRequestedOn() == null)
            userRequest.setRequestedOn(new Timestamp(new Date().getTime()));
        userRequest.setHd4dpUrl(userRequestDto.getHd4dpUrl());
        if(userRequestDto.getDataCollectionNames() != null) {
            userRequest.setDataCollectionNames(new HashSet<>(userRequestDto.getDataCollectionNames()));
        }
        userRequest.setHealthDataIdentification(getHealthDataIdentification());
        if(userRequestDto.getEmailAdmins() != null) {
            userRequest.setEmailAdmins(new HashSet<>(userRequestDto.getEmailAdmins()));
        }
    }
}
