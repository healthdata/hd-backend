/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain.dcd;


import be.wiv_isp.healthdata.common.util.StringUtils;
import be.wiv_isp.healthdata.hd4res.domain.Description;
import be.wiv_isp.healthdata.hd4res.domain.Label;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "DATA_COLLECTION_DEFINITIONS")
public class DataCollectionDefinition implements Serializable {

	private static final long serialVersionUID = -3770857148167696369L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "DATA_COLLECTION_DEFINITIONS_ID")
	private Long id;

	@Column(name = "SAC_ID")
	private Long standAloneCatalogueId;

	@Column(name = "HISTORY_ID")
	private Long historyId;

	@Column(name = "DATA_COLLECTION_NAME", nullable = false)
	private String dataCollectionName;

	@Embedded
	private Label label;

	@Embedded
	private Description description;

	@Column(name = "MINOR_VERSION", nullable = false)
	private Integer minorVersion;

	@Column(name = "PUBLISHED", nullable = false)
	private boolean published;

	@Column(name = "CREATED_ON", nullable = false)
	private Timestamp createdOn;

	@Column(name = "UPDATED_ON", nullable = false)
	private Timestamp updatedOn;

	@Column(name = "DATA_COLLECTION_GROUPS_ID", nullable = false)
	private Long dataCollectionGroupId;


//	public DataCollectionDefinition() {
//	}
//
//	public DataCollectionDefinition(Long id,
//			Long standAloneCatalogueId,
//			String dataCollectionName,
//			Long dataCollectionGroupId,
//			String labelEn,
//			String labelFr,
//			String labelNl,
//			String descriptionEn,
//			String descriptionFr,
//			String descriptionNl,
//			Integer minorVersion,
//			Date createdOn,
//			Date updatedOn,
//			List<DataCollectionDefinitionHistory> history) {
//		this.id = id;
//		this.standAloneCatalogueId = standAloneCatalogueId;
//		this.dataCollectionName = dataCollectionName;
//		this.dataCollectionGroupId = dataCollectionGroupId;
//		this.label = new Label(labelEn, labelFr, labelNl);
//		this.description = new Description(descriptionEn, descriptionFr, descriptionNl);
//		this.minorVersion = minorVersion;
//		if (createdOn != null) {
//			this.createdOn = new Timestamp(createdOn.getTime());
//		}
//		if (updatedOn != null) {
//			this.updatedOn = new Timestamp(updatedOn.getTime());
//		}
//		this.history = history;
//	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStandAloneCatalogueId() {
		return standAloneCatalogueId;
	}

	public void setStandAloneCatalogueId(Long standAloneCatalogueId) {
		this.standAloneCatalogueId = standAloneCatalogueId;
	}

	public Long getHistoryId() {
		return historyId;
	}

	public void setHistoryId(Long historyId) {
		this.historyId = historyId;
	}

	public String getDataCollectionName() {
		return dataCollectionName;
	}

	public void setDataCollectionName(String dataCollectionName) {
		this.dataCollectionName = dataCollectionName;
	}

	public Integer getMinorVersion() {
		return minorVersion;
	}

	public void setMinorVersion(Integer minorVersion) {
		this.minorVersion = minorVersion;
	}

	public Label getLabel() {
		return label;
	}

	public void setLabel(Label label) {
		this.label = label;
	}

	public Description getDescription() {
		return description;
	}

	public void setDescription(Description description) {
		this.description = description;
	}

	public boolean isPublished() {
		return published;
	}

	public void setPublished(boolean published) {
		this.published = published;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Long getDataCollectionGroupId() {
		return dataCollectionGroupId;
	}

	public void setDataCollectionGroupId(Long dataCollectionGroupId) {
		this.dataCollectionGroupId = dataCollectionGroupId;
	}

	@PrePersist
	public void onCreate() {
		long currentTimeWithoutMilliseconds = new Date().getTime();
		setCreatedOn(new Timestamp(currentTimeWithoutMilliseconds));
		setUpdatedOn(new Timestamp(currentTimeWithoutMilliseconds));
	}

	@PreUpdate
	public void onUpdate() {
		long currentTimeWithoutMilliseconds = new Date().getTime();
		setUpdatedOn(new Timestamp(currentTimeWithoutMilliseconds));
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof DataCollectionDefinition) {
			DataCollectionDefinition other = (DataCollectionDefinition) o;

			return Objects.equals(id, other.id)
					&& Objects.equals(standAloneCatalogueId, other.standAloneCatalogueId)
					&& Objects.equals(dataCollectionName, other.dataCollectionName)
					&& Objects.equals(label, other.label)
					&& Objects.equals(description, other.description)
					&& Objects.equals(minorVersion, minorVersion)
					&& Objects.equals(createdOn, other.createdOn)
					&& Objects.equals(updatedOn, other.updatedOn)
					&& Objects.equals(dataCollectionGroupId, other.dataCollectionGroupId);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				this.id,
				this.standAloneCatalogueId,
				this.dataCollectionName,
				this.label,
				this.description,
				this.minorVersion,
				this.createdOn,
				this.updatedOn,
				this.dataCollectionGroupId);
	}

	@Override
	public String toString() {
		return StringUtils.toStringExclude(this);
	}
}
