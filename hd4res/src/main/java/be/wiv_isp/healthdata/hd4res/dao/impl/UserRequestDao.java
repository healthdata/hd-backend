/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.hd4res.domain.UserRequest;
import be.wiv_isp.healthdata.hd4res.domain.search.UserRequestSearch;
import be.wiv_isp.healthdata.orchestration.dao.impl.AbstractUserRequestDao;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Repository
public class UserRequestDao extends AbstractUserRequestDao<UserRequest, UserRequestSearch> {

    public UserRequestDao() {
        super(UserRequest.class);
    }

    @Override
    protected List<Predicate> getPredicates(UserRequestSearch search, CriteriaBuilder cb, Root<UserRequest> rootEntry) {
        final List<Predicate> predicates = new ArrayList<>();

        predicates.addAll(getSharedPredicates(search, cb, rootEntry));
        if (StringUtils.isNoneBlank(search.getDataCollectionName())) {
            predicates.add(cb.isMember(search.getDataCollectionName(), rootEntry.<Collection<String>>get("dataCollectionNames")));
            if(!search.isDisplayArchived()) {
                predicates.add(cb.isNotMember(search.getDataCollectionName(), rootEntry.<Collection<String>>get("archivedDataCollectionNames")));
            }
        }
        if (search.getHd4dpId() != null) {
            predicates.add(cb.equal(rootEntry.get("hd4dpId"), search.getHd4dpId()));
        }
        if(search.getHealthDataIdentification() != null) {
            if(StringUtils.isNoneBlank(search.getHealthDataIdentification().getType())) {
                predicates.add(cb.equal(rootEntry.<HealthDataIdentification>get("healthDataIdentification").get("type"), search.getHealthDataIdentification().getType()));
            }
            if(StringUtils.isNoneBlank(search.getHealthDataIdentification().getValue())) {
                predicates.add(cb.equal(rootEntry.<HealthDataIdentification>get("healthDataIdentification").get("value"), search.getHealthDataIdentification().getValue()));
            }
            if(StringUtils.isNoneBlank(search.getHealthDataIdentification().getName())) {
                predicates.add(cb.equal(rootEntry.<HealthDataIdentification>get("healthDataIdentification").get("name"), search.getHealthDataIdentification().getName()));
            }
        }
        if (search.getApproved() != null) {
            if(search.getApproved()) {
                predicates.add(cb.isTrue(rootEntry.<Boolean>get("approved")));
            } else {
                predicates.add(cb.isFalse(rootEntry.<Boolean>get("approved")));
            }
        }
        return predicates;
    }

}
