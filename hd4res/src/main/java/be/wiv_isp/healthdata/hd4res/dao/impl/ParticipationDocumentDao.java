/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.hd4res.domain.ParticipationDocument;
import be.wiv_isp.healthdata.hd4res.domain.search.ParticipationDocumentSearch;
import be.wiv_isp.healthdata.orchestration.dao.impl.AbstractParticipationDocumentDao;
import be.wiv_isp.healthdata.orchestration.domain.Attachment;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.util.List;

@Repository
public class ParticipationDocumentDao extends AbstractParticipationDocumentDao<ParticipationDocument, ParticipationDocumentSearch> {

	public ParticipationDocumentDao() {
		super(ParticipationDocument.class);
	}

	protected List<Predicate> getPredicates(ParticipationDocumentSearch search, CriteriaBuilder cb, Root<ParticipationDocument> rootEntry) {
		List<Predicate> predicates = super.getPredicates(search, cb, rootEntry);
		if (search.getDwhStatus() != null) {
			predicates.add(cb.equal(rootEntry.get("dwhStatus"), search.getDwhStatus().toUpperCase()));
		}
		if (search.getWithCsv() != null) {
			if (search.getWithCsv()) {
				predicates.add(cb.isNotNull(rootEntry.get("csv")));
			} else {
				predicates.add(cb.isNull(rootEntry.get("csv")));
			}
		}
		if (search.getAttachmentId() != null) {
			final ListJoin<ParticipationDocument, Attachment> join = rootEntry.joinList("attachments");
			predicates.add(cb.equal(join.get("id"), search.getAttachmentId()));
		}
		return predicates;
	}

	@Override
	protected boolean ignoreDeletedWorkflows() {
		return false;
	}
}
