/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.hd4res.dao.IHd4dpUserDao;
import be.wiv_isp.healthdata.hd4res.domain.Hd4dpUser;
import be.wiv_isp.healthdata.hd4res.domain.search.Hd4dpUserSearch;
import be.wiv_isp.healthdata.hd4res.service.IHd4dpUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Hd4dpUserService extends AbstractService<Hd4dpUser, Long, Hd4dpUserSearch, IHd4dpUserDao> implements IHd4dpUserService {

    @Autowired
    private IHd4dpUserDao dao;

    public Hd4dpUserService() {
        super(Hd4dpUser.class);
    }

    @Override
    protected IHd4dpUserDao getDao() {
        return dao;
    }
}
