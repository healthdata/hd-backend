/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.api;

import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.hd4res.domain.Hd4dpAuthority;
import be.wiv_isp.healthdata.hd4res.domain.Hd4dpUser;
import be.wiv_isp.healthdata.hd4res.domain.search.Hd4dpUserSearch;
import be.wiv_isp.healthdata.hd4res.dto.Hd4dpUserDto;
import be.wiv_isp.healthdata.hd4res.service.IHd4dpUserService;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.service.IUserDataCollectionService;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;

@Component
@Path("/hd4dpusers")
public class Hd4dpUserRestService {

	@Autowired
	private IHd4dpUserService hd4dpUserService;

	@Autowired
	private IUserDataCollectionService userDataCollectionService;

	@GET
	@PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(@Context UriInfo info) {
		final HDUserDetails principal = AuthenticationService.getUserDetails();
		final String dataCollectionName = RestUtils.getParameterString(info, RestUtils.ParameterName.DATA_COLLECTION_NAME);

		if(!principal.getAuthorities().contains(new Authority(Authority.ADMIN))) {
			if(dataCollectionName == null || !userDataCollectionService.isUserAuthorized(principal, dataCollectionName)) {
				return Response.status(Response.Status.FORBIDDEN).build();
			}
		}

		final Hd4dpUserSearch search = new Hd4dpUserSearch();

		search.setDataCollectionName(dataCollectionName);
		search.setHealthDataIdentificationType(RestUtils.getParameterString(info, RestUtils.ParameterName.IDENTIFICATION_TYPE));
		search.setHealthDataIdentificationValue(RestUtils.getParameterString(info, RestUtils.ParameterName.IDENTIFICATION_VALUE));
		final String authorityString = RestUtils.getParameterString(info, RestUtils.ParameterName.AUTHORITY);
		if(authorityString != null) {
			search.setAuthority(new Hd4dpAuthority(authorityString));
		}
		final List<Hd4dpUser> users = hd4dpUserService.getAll(search);

		return Response.ok(convert(users)).build();
	}

	private List<Hd4dpUserDto> convert(List<Hd4dpUser> users) {
		final List<Hd4dpUserDto> dtos = new ArrayList<>();
		for (Hd4dpUser user : users) {
			dtos.add(convert(user));
		}
		return dtos;
	}

	private Hd4dpUserDto convert(Hd4dpUser user) {
		final Hd4dpUserDto dto = new Hd4dpUserDto();
		dto.setId(user.getId());
		dto.setHd4dpId(user.getHd4dpId());
		dto.setUsername(user.getUsername());
		dto.setFirstName(user.getFirstName());
		dto.setLastName(user.getLastName());
		dto.setEmail(user.getEmail());
		dto.setAuthorities(user.getAuthorities());
		dto.setDataCollectionNames(user.getDataCollectionNames());
		dto.setEnabled(user.isEnabled());
		dto.setLdapUser(user.isLdapUser());
		dto.setHealthDataIdentification(user.getHealthDataIdentification());
		dto.setCreatedOn(user.getCreatedOn());
		dto.setUpdatedOn(user.getUpdatedOn());
		return dto;
	}

}
