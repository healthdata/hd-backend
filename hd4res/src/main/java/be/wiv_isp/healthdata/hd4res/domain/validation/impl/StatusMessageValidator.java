/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain.validation.impl;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.StatusResult;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.hd4res.domain.StatusMessage;
import be.wiv_isp.healthdata.orchestration.action.StatusMessageContent;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageStatus;
import be.wiv_isp.healthdata.orchestration.domain.validator.impl.AbstractStatusMessageValidator;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.Date;
import java.util.Iterator;

@Service
public class StatusMessageValidator extends AbstractStatusMessageValidator<StatusMessage> {

    private static final Logger LOG = LoggerFactory.getLogger(StatusMessageValidator.class);

    @Override
    public void validate(StatusMessage statusMessage) {
        LOG.debug("Validating status message");

        validateStatuses(statusMessage);
        validateMessages(statusMessage);
    }

    private void validateMessages(StatusMessage statusMessage) {
        final StatusMessageContent content = statusMessage.getContent();

        try {
            content.setValidation(StatusMessageContent.MESSAGES, StatusMessageContent.OK);

            if(content.isNull(StatusMessageContent.MESSAGES)) {
                final String message = MessageFormat.format("No {0} attribute found", StatusMessageContent.MESSAGES);
                LOG.debug(message);
                content.addValidationError(StatusMessageContent.MESSAGES, message);
                return;
            }

            final JSONObject messagesJsonObject = content.getJSONObject(StatusMessageContent.MESSAGES);

            final Iterator it = messagesJsonObject.keys();
            while(it.hasNext()) {
                final String key = (String) it.next();
                final MessageStatus status = MessageStatus.valueOf(key);
                final Long count = messagesJsonObject.getLong(key);

                if(count > 0 && countMustBeZero(status)) {
                    final String error = MessageFormat.format("{0} message(s) have status {1}", count, status);
                    content.addValidationError(StatusMessageContent.MESSAGES, error);
                }
            }
        } catch (JSONException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.ERROR_WHILE_PROCESSING_STATUS_MESSAGE_ACTION, e.getMessage());
            throw exception;
        }
    }

    private boolean countMustBeZero(MessageStatus status) {
        switch (status) {
            case ERROR_SENDING:
            case UNACKNOWLEDGED:
            case ERROR_PROCESSING:
                return true;
            default:
                return false;
        }
    }

    private void validateStatuses(StatusMessage statusMessage) {
        final StatusMessageContent content = statusMessage.getContent();

        try {
            content.setValidation(StatusMessageContent.STATUSES, StatusMessageContent.OK);

            // check response of the statuses API
            if(!content.isNull(StatusMessageContent.STATUSES)) {
                final JSONObject statusesJsonObject = content.getJSONObject(StatusMessageContent.STATUSES);

                final Iterator it = statusesJsonObject.keys();
                while (it.hasNext()) {
                    final String key = (String) it.next();
                    final String value = statusesJsonObject.getString(key);

                    StatusResult valueAsStatusResult = null;
                    try {
                        valueAsStatusResult = StatusResult.valueOf(value);
                    } catch (IllegalArgumentException e) {
                        // nothing to do, test below will fail and status will not be considered as UP
                    }

                    if (!StatusResult.UP.equals(valueAsStatusResult)) {
                        final String error = MessageFormat.format("Status of {0} is {1}", key, value);
                        content.addValidationError(StatusMessageContent.STATUSES, error);
                    }
                }
            } else {
                boolean isMainOrganization = content.isNull(StatusMessageContent.MAIN_ORGANIZATION);
                if(isMainOrganization) {
                    LOG.debug("No {} attribute found", StatusMessageContent.STATUSES);
                    content.addValidationError(StatusMessageContent.STATUSES, "Statuses could not be retrieved");
                }
            }

            // check reception of last HD4RES status message
            final Date sentOn = new Date(statusMessage.getSentOn().getTime());
            Date lastHd4resStatusMessageReceived;
            if(content.isNull(StatusMessageContent.LAST_HD4RES_STATUS_MESSAGE_RECEIVED)) {
                content.addValidationError(StatusMessageContent.STATUSES, "No status message received from HD4RES yet");
            } else {
                final String lastHd4resStatusMessageReceivedAsString = content.getString(StatusMessageContent.LAST_HD4RES_STATUS_MESSAGE_RECEIVED);
                lastHd4resStatusMessageReceived = DateFormat.stringToDate(lastHd4resStatusMessageReceivedAsString, DateFormat.DATE_AND_TIME);
                final Long intervalMillis = sentOn.getTime() - lastHd4resStatusMessageReceived.getTime();
                if(intervalMillis > _25_HOURS_AS_MILLIS) {
                    content.addValidationError(StatusMessageContent.STATUSES, MessageFormat.format("Last status message from HD4RES was received on {0} (more than 25 hours before the sending time of this message)", lastHd4resStatusMessageReceivedAsString));
                }
            }
        } catch (JSONException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.ERROR_WHILE_PROCESSING_STATUS_MESSAGE_ACTION, e.getMessage());
            throw exception;
        }
    }
}
