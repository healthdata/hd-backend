/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.action.workflow;

import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.action.workflow.AbstractWorkflowAction;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.RegistrationWorkflowHistory;
import be.wiv_isp.healthdata.orchestration.domain.WorkflowFlags;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.service.IAbstractRegistrationWorkflowService;
import be.wiv_isp.healthdata.orchestration.service.IElasticSearchService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractRegistrationWorkflowAction extends AbstractWorkflowAction<RegistrationWorkflow, RegistrationWorkflowHistory> {

	@Autowired
	protected IAbstractRegistrationWorkflowService<RegistrationWorkflow, RegistrationWorkflowSearch> workflowService;

	@Autowired
	protected IElasticSearchService elasticSearchService;

	@Override
	protected IAbstractRegistrationWorkflowService<RegistrationWorkflow, RegistrationWorkflowSearch> getWorkflowService() {
		return workflowService;
	}

	@Override
	public WorkflowType getWorkflowType() {
		return WorkflowType.REGISTRATION;
	}

	@Override
	public RegistrationWorkflow postExecute(RegistrationWorkflow workflow, HDUserDetails userDetails) {
		elasticSearchService.index(workflow.getId());
		return workflow;
	}

	@Override
	protected void setWorkflowFlags(RegistrationWorkflowHistory workflowHistory, RegistrationWorkflow workflow) {
		workflowHistory.setFlags(new WorkflowFlags(workflow.getFlags()));
	}

	@Override
	protected RegistrationWorkflowHistory getWorkflowHistoryInstance() {
		return new RegistrationWorkflowHistory();
	}
}
