/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class Label implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "LABEL_EN", nullable = false)
    private String en;
    @Column(name = "LABEL_FR", nullable = false)
    private String fr;
    @Column(name = "LABEL_NL", nullable = false)
    private String nl;


    public Label() {
    }

    public Label(String en, String fr, String nl) {
        this.en = en;
        this.fr = fr;
        this.nl = nl;
    }

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }

    public String getFr() {
        return fr;
    }

    public void setFr(String fr) {
        this.fr = fr;
    }

    public String getNl() {
        return nl;
    }

    public void setNl(String nl) {
        this.nl = nl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Label version = (Label) o;
        return Objects.equals(en, version.en) &&
                Objects.equals(fr, version.fr) &&
                Objects.equals(nl, version.nl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(en, fr, nl);
    }

    @Override
    public String toString() {
        return "Label {" +
                "en = " + Objects.toString(this.en) + ", " +
                "fr = " + Objects.toString(this.fr) + ", " +
                "nl = " + Objects.toString(this.nl) + "}";
    }
}
