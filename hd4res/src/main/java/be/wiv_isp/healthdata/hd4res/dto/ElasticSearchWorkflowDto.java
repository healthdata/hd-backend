/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dto;

import be.wiv_isp.healthdata.orchestration.domain.AbstractWorkflowHistory;
import be.wiv_isp.healthdata.orchestration.dto.DocumentDto;
import be.wiv_isp.healthdata.orchestration.dto.NoteDto;
import org.apache.commons.collections.CollectionUtils;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.Collections;
import java.util.List;

public class ElasticSearchWorkflowDto extends RegistrationWorkflowDto {

	private String identificationName;
	private AbstractWorkflowHistory firstSaveAction;
	private List<NoteDto> notes;
	private Long openNotes = 0L;
	private Long resolvedNotes = 0L;
	private Long totalNotes = 0L;

	public ElasticSearchWorkflowDto() {

	}

	public ElasticSearchWorkflowDto(RegistrationWorkflowDto dto, List<NoteDto> notes) {
		super(dto);
		if(CollectionUtils.isNotEmpty(dto.getHistory())) {
			setFirstSaveAction(Collections.min(dto.getHistory(), new WorkflowHistoryComparator()));
		}
		setNotes(notes);
		Long resolvedCounter = 0L;
		for (NoteDto note : notes)
			if (note.isResolved())
				resolvedCounter += 1L;
		setResolvedNotes(resolvedCounter);
		setTotalNotes((long) notes.size());
		setOpenNotes(getTotalNotes() - getResolvedNotes());
	}

	@Override
	@JsonIgnore(value = false)
	public DocumentDto getDocument() {
		return super.getDocument();
	}
	
	public AbstractWorkflowHistory getFirstSaveAction() {
		return firstSaveAction;
	}

	public void setFirstSaveAction(AbstractWorkflowHistory firstSaveAction) {
		this.firstSaveAction = firstSaveAction;
	}

	public String getIdentificationName() {
		return identificationName;
	}

	public void setIdentificationName(String identificationName) {
		this.identificationName = identificationName;
	}

	public List<NoteDto> getNotes() {
		return notes;
	}

	public void setNotes(List<NoteDto> notes) {
		this.notes = notes;
	}

	public Long getOpenNotes() {
		return openNotes;
	}

	public void setOpenNotes(Long openNotes) {
		this.openNotes = openNotes;
	}

	public Long getResolvedNotes() {
		return resolvedNotes;
	}

	public void setResolvedNotes(Long resolvedNotes) {
		this.resolvedNotes = resolvedNotes;
	}

	public Long getTotalNotes() {
		return totalNotes;
	}

	public void setTotalNotes(Long totalNotes) {
		this.totalNotes = totalNotes;
	}
}
