/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.api;

import be.wiv_isp.healthdata.common.api.ClientVersion;
import be.wiv_isp.healthdata.common.caching.CacheManagementService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.common.util.TokenUtils;
import be.wiv_isp.healthdata.orchestration.annotation.Auditable;
import be.wiv_isp.healthdata.orchestration.api.uri.TranslationUri;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ApiType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IUserService;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IWebServiceClientService;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Component
@Path("/translations/manager")
public class TranslationManagementRestService {

	private static final Logger LOG = LoggerFactory.getLogger(TranslationManagementRestService.class);

	@Autowired
	private IWebServiceClientService webServiceClientService;
	@Autowired
	private IUserService userService;
	@Autowired
	private IConfigurationService configurationService;
	@Autowired
	private CacheManager cacheManager;

	@PUT
	@PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
	@Auditable(apiType = ApiType.TRANSLATION)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@Context UriInfo info, String json) {
		final HDUserDetails principal = AuthenticationService.getUserDetails();
		if(userService.isGuestAccount(principal.getUsername())) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GENERAL_UNAUTHORIZED);
			throw exception;
		}

		Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

		String accessToken = getAccessToken(catalogueHost);

		final TranslationUri translationUri = new TranslationUri();
		translationUri.setHost(catalogueHost.getValue());

		WebServiceBuilder wsb = new WebServiceBuilder();

		wsb.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
		wsb.addHeader(HttpHeaders.CLIENT_VERSION, ClientVersion.Translation.getDefault());
		wsb.setUrl(translationUri.toString());
		wsb.setPut(true);
		wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb.setJson(json);
		wsb.setReturnType(new GenericType<ClientResponse>() {
		});

		ClientResponse response = (ClientResponse) webServiceClientService.callWebService(wsb);

		if (response.getStatus() == Response.Status.NO_CONTENT.getStatusCode()) {
			cacheManager.getCache(CacheManagementService.translationCache).clear();
			return Response.noContent().build();
		} else {
			LOG.error(response.getEntity(Object.class).toString());
			return Response.serverError().build();
		}
	}

	@DELETE
	@PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
	@Auditable(apiType = ApiType.TRANSLATION)
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@Context UriInfo info, String json) {
		final HDUserDetails principal = AuthenticationService.getUserDetails();
		if(userService.isGuestAccount(principal.getUsername())) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GENERAL_UNAUTHORIZED);
			throw exception;
		}
		Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

		String accessToken = getAccessToken(catalogueHost);

		final TranslationUri translationUri = new TranslationUri();
		translationUri.setHost(catalogueHost.getValue());

		WebServiceBuilder wsb = new WebServiceBuilder();

		wsb.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
		wsb.addHeader(HttpHeaders.CLIENT_VERSION, ClientVersion.Translation.getDefault());
		wsb.setUrl(translationUri.toString());
		wsb.setDelete(true);
		wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb.setJson(json);
		wsb.setReturnType(new GenericType<ClientResponse>() {
		});

		ClientResponse response = (ClientResponse) webServiceClientService.callWebService(wsb);

		if (response.getStatus() == Response.Status.NO_CONTENT.getStatusCode()) {
			cacheManager.getCache(CacheManagementService.translationCache).clear();
			return Response.noContent().build();
		} else {
			LOG.error(response.getEntity(Object.class).toString());
			return Response.serverError().build();
		}
	}

	private String getAccessToken(Configuration catalogueHost) {
		Configuration catalogueUserName = configurationService.get(ConfigurationKey.CATALOGUE_USERNAME);
		Configuration cataloguePassword = configurationService.get(ConfigurationKey.CATALOGUE_PASSWORD);

		return TokenUtils.getAccessTokenCatalogue(catalogueHost.getValue(), catalogueUserName.getValue(), cataloguePassword.getValue());
	}
}
