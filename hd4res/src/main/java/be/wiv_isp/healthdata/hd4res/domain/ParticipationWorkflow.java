/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain;

import be.wiv_isp.healthdata.orchestration.domain.AbstractParticipationWorkflow;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "PARTICIPATION_WORKFLOWS")
public class ParticipationWorkflow extends AbstractParticipationWorkflow<ParticipationDocument> {

	@Column(name = "STARTED_ON")
	protected Timestamp startedOn;

	@Column(name = "HD4DP_WORKFLOW_ID")
	private String hd4dpWorkflowId;

	@Column(name = "IDENTIFICATION_TYPE")
	private String identificationType;

	@Column(name = "IDENTIFICATION_VALUE")
	private String identificationValue;

	@Column(name = "IDENTIFICATION_NAME")
	private String identificationName;

	public Timestamp getStartedOn() {
		return startedOn;
	}

	public void setStartedOn(Timestamp startedOn) {
		this.startedOn = startedOn;
	}

	public String getHd4dpWorkflowId() {
		return hd4dpWorkflowId;
	}

	public void setHd4dpWorkflowId(String hd4dpWorkflowId) {
		this.hd4dpWorkflowId = hd4dpWorkflowId;
	}

	public String getIdentificationType() {
		return identificationType;
	}

	public void setIdentificationType(String identificationType) {
		this.identificationType = identificationType;
	}

	public String getIdentificationValue() {
		return identificationValue;
	}

	public void setIdentificationValue(String identificationValue) {
		this.identificationValue = identificationValue;
	}

	public String getIdentificationName() {
		return identificationName;
	}

	public void setIdentificationName(String identificationName) {
		this.identificationName = identificationName;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof ParticipationWorkflow) {
			ParticipationWorkflow other = (ParticipationWorkflow) o;

			return Objects.equals(id, other.id)
					&& Objects.equals(status, other.status)
					&& Objects.equals(documentHistory, other.documentHistory)
					&& Objects.equals(dataCollectionName, other.dataCollectionName)
					&& Objects.equals(dataCollectionDefinitionId, other.dataCollectionDefinitionId)
					&& Objects.equals(createdOn, other.createdOn)
					&& Objects.equals(updatedOn, other.updatedOn)
					&& Objects.equals(history, other.history)
					&& Objects.equals(hd4dpWorkflowId, other.hd4dpWorkflowId)
					&& Objects.equals(identificationType, other.identificationType)
					&& Objects.equals(identificationValue, other.identificationValue)
					&& Objects.equals(identificationName, other.identificationName)
					&& Objects.equals(organization, other.organization);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				this.id,
				this.status,
				this.documentHistory,
				this.dataCollectionName,
				this.dataCollectionDefinitionId,
				this.createdOn,
				this.updatedOn,
				this.history,
				this.hd4dpWorkflowId,
				this.identificationType,
				this.identificationValue,
				this.identificationName,
				this.organization);
	}

	@Override
	public String toString() {
		return "ParticipationWorkflow {" +
				"id = " + Objects.toString(this.id) + ", " +
				"status = " + Objects.toString(this.status) + ", " +
				"document = " + Objects.toString(this.documentHistory) + ", " +
				"dataCollectionName = " + Objects.toString(this.dataCollectionName) + ", " +
				"dataCollectionDefinitionId = " + Objects.toString(this.dataCollectionDefinitionId) + ", " +
				"createdOn = " + Objects.toString(this.createdOn) + ", " +
				"updatedOn = " + Objects.toString(this.updatedOn) + ", " +
				"history = " + Objects.toString(this.history) + ", " +
				"hd4dpWorkflowId = " + Objects.toString(this.hd4dpWorkflowId) + ", " +
				"identificationType = " + Objects.toString(this.identificationType) + ", " +
				"identificationValue = " + Objects.toString(this.identificationValue) + ", " +
				"identificationName = " + Objects.toString(this.identificationName) + ", " +
				"organization = " + Objects.toString(this.organization) + "}";
	}
}