/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.action.workflow;

import be.wiv_isp.healthdata.hd4res.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.search.ParticipationWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.action.workflow.AbstractWorkflowAction;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.ParticipationWorkflowHistory;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.service.IAbstractParticipationWorkflowService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractParticipationWorkflowAction extends AbstractWorkflowAction<ParticipationWorkflow, ParticipationWorkflowHistory> {

	@Autowired
	protected IAbstractParticipationWorkflowService<ParticipationWorkflow, ParticipationWorkflowSearch> workflowService;

	@Override
	protected IAbstractParticipationWorkflowService<ParticipationWorkflow, ParticipationWorkflowSearch> getWorkflowService() {
		return workflowService;
	}

	@Override
	public WorkflowType getWorkflowType() {
		return WorkflowType.PARTICIPATION;
	}

	@Override
	public ParticipationWorkflow postExecute(ParticipationWorkflow workflow, HDUserDetails userDetails) {
		return workflow;
	}

	@Override
	protected void setWorkflowFlags(ParticipationWorkflowHistory workflowHistory, ParticipationWorkflow workflow) {
		// nothing to do
	}

	@Override
	protected ParticipationWorkflowHistory getWorkflowHistoryInstance() {
		return new ParticipationWorkflowHistory();
	}
}
