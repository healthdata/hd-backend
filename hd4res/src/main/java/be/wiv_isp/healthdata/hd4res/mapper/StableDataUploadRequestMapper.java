/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.mapper;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.hd4res.domain.StableDataUploadRequest;
import org.codehaus.jettison.json.JSONObject;

import java.util.Iterator;

public class StableDataUploadRequestMapper {

    public static StableDataUploadRequest map(JSONObject json) {
        final StableDataUploadRequest request = new StableDataUploadRequest();

        final Iterator keys = json.keys();
        while(keys.hasNext()) {
            final String key = (String) keys.next();

            switch (key) {
                case "dataCollectionName":
                    String dataCollectionName = JsonUtils.getString(json, key);
                    request.setDataCollectionName(dataCollectionName);
                    break;
                case "receiver":
                    final JSONObject identificationJson = JsonUtils.getJSONObject(json, key);
                    final HealthDataIdentification healthDataIdentification = new HealthDataIdentification();
                    healthDataIdentification.setType(JsonUtils.getString(identificationJson, "type"));
                    healthDataIdentification.setValue(JsonUtils.getString(identificationJson, "value"));
                    request.setHealthDataIdentification(healthDataIdentification);
                    break;
                case "base64Csv":
                    String base64Csv = JsonUtils.getString(json, key);
                    request.setBase64Csv(base64Csv);
                    break;
                case "patientIdFormat":
                    final String patientIdFormatAsString = JsonUtils.getString(json, key);
                    final StableDataUploadRequest.PatientIdFormat patientIdFormat = StableDataUploadRequest.PatientIdFormat.valueOf(patientIdFormatAsString.toUpperCase());
                    request.setPatientIdFormat(patientIdFormat);
                    break;
            }

        }

        return request;
    }
}
