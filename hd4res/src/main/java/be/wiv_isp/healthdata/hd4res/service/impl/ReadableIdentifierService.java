/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.hd4res.dao.IOptionalPatientIdDataCollectionNameDao;
import be.wiv_isp.healthdata.hd4res.dao.IRecodeDataCollectionNameDao;
import be.wiv_isp.healthdata.hd4res.dao.IWorkflowNameDao;
import be.wiv_isp.healthdata.hd4res.domain.*;
import be.wiv_isp.healthdata.hd4res.domain.search.Hd4ResSsinSearch;
import be.wiv_isp.healthdata.hd4res.encrypt.dao.ICombiRegisterDao;
import be.wiv_isp.healthdata.hd4res.encrypt.dao.IHd4ResSsinDao;
import be.wiv_isp.healthdata.hd4res.encrypt.domain.CombiRegister;
import be.wiv_isp.healthdata.hd4res.encrypt.domain.Hd4ResSsin;
import be.wiv_isp.healthdata.hd4res.service.IReadableIdentifierService;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;

@Component
public class ReadableIdentifierService implements IReadableIdentifierService {

	private static final Logger LOG = LoggerFactory.getLogger(ReadableIdentifierService.class);

	@Autowired
	private IHd4ResSsinDao hd4ResSsinDao;
	@Autowired
	private ICombiRegisterDao combiRegisterDao;
	@Autowired
	private IWorkflowNameDao workflowNameDao;
	@Autowired
	private IRecodeDataCollectionNameDao recodeDataCollectionNameDao;
	@Autowired
	private IOptionalPatientIdDataCollectionNameDao optionalPatientIdDataCollectionNameDao;

	@Transactional
	@Override
	public String getReadableSsin(String registerName, String encodedSsin) {
		CombiRegister combiRegister = combiRegisterDao.get(registerName);
		if (combiRegister != null) {
			registerName = combiRegister.getCombiName();
		}
		Hd4ResSsin hd4ResSSin = null;
		Set<String> optionalPatientIdDataCollectionNames = getOptionalPatientIdDataCollectionNames();
		if(!optionalPatientIdDataCollectionNames.contains(registerName) || !Hd4ResSsin.CODED_NO_PATIENT_ID.equals(encodedSsin)) {
			Hd4ResSsinSearch search = new Hd4ResSsinSearch();
			search.setRegisterName(registerName);
			search.seteHealthCodedNissBatch(encodedSsin);
			List<Hd4ResSsin> hd4ResSsins = hd4ResSsinDao.getAll(search);
			if(!hd4ResSsins.isEmpty()) {
				hd4ResSSin = hd4ResSsins.get(0);
			}
		}

		if (hd4ResSSin == null) {
			long number;
			String biggest = hd4ResSsinDao.getBiggest(registerName);
			if (StringUtils.isNoneBlank(biggest)) {
				try {
					String[] split = biggest.split("_");
					String numberAsString = split[split.length - 1].replace("-", "");
					number = Long.valueOf(numberAsString) + 1;
				} catch (Exception e) {
					LOG.error("", e);
					throw e;
				}
			} else {
				number = 1L;
			}

			String landingZoneCodedNiss = format(registerName, number);

			hd4ResSSin = new Hd4ResSsin();
			hd4ResSSin.setRegisterName(registerName);
			hd4ResSSin.seteHealthCodedNissBatch(encodedSsin);
			hd4ResSSin.seteHealthCodedNissManual(convertBatchToManual(encodedSsin));
			hd4ResSSin.setLandingZoneCodedNiss(landingZoneCodedNiss);
			hd4ResSSin = hd4ResSsinDao.create(hd4ResSSin);
		}
		return hd4ResSSin.getLandingZoneCodedNiss();
	}

	@Override
	public String convertBatchToManual(String bacthEncodedSsin) {
		return new String(Base64.encodeBase64((";F;" + bacthEncodedSsin).getBytes()), StandardCharsets.UTF_8);
	}

	@Override
	public String getEncodedSsin(String readableSsin) {
		Hd4ResSsinSearch search = new Hd4ResSsinSearch();
		search.setLandingZoneCodedNiss(readableSsin);
		List<Hd4ResSsin> hd4ResSsins = hd4ResSsinDao.getAll(search);
		if (hd4ResSsins.isEmpty()) {
			return null;
		} else if (hd4ResSsins.size() == 1) {
			return hd4ResSsins.get(0).geteHealthCodedNissBatch();
		} else {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.NON_UNIQUE_SEARCH_RESULT, hd4ResSsins.size(), "encodedSsin", readableSsin);
			throw exception;
		}
	}

	private String format(String registerName, long id) {
		DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
		symbols.setGroupingSeparator('-');

		DecimalFormat formatter = new DecimalFormat("00,000,000", symbols);
		formatter.setGroupingSize(4);
		return registerName + "_PATIENTID_" + formatter.format(id);
	}

	@Transactional
	@Override
	public String createWorkflowName(String definitionName) {
		Long biggest = workflowNameDao.getBiggest(definitionName);
		long number;
		if (biggest == null) {
			number = 1L;
		} else {
			number = biggest + 1;
		}

		final WorkflowName workflowName = new WorkflowName();
		workflowName.setDefinitionName(definitionName);
		workflowName.setNumber(number);

		workflowNameDao.create(workflowName);
		return definitionName + "_" + formatWorkflowNumber(number);
	}

	private String formatWorkflowNumber(long id) {
		DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
		symbols.setGroupingSeparator('-');

		DecimalFormat formatter = new DecimalFormat("00,000,000", symbols);
		formatter.setGroupingSize(4);
		return formatter.format(id);
	}

	@Override
	public void registerOldPatiendId(RegistrationWorkflow workflow) {
		LOG.info("Get the old patient id from workflow with id [{}]", workflow.getId());
		RecodeDataCollectionName recodeDataCollectionName = geRecodeDataCollectionName(workflow.getDataCollectionName());
		if (recodeDataCollectionName == null) {
			return;
		}
		String oldPatientIdFromDocument = getOldPatientIdFromDocument(workflow.getDocument(), recodeDataCollectionName);
		String landingZoneCodedNiss = workflow.getMetaData().get(RegistrationWorkflow.MetaData.CODED_PATIENT_IDENTIFIER);
		Hd4ResSsin hd4ResSsin = hd4ResSsinDao.get(landingZoneCodedNiss);
		boolean otherRecordsWithOldPatientId = markOtherRecordsWithOldPatientId(landingZoneCodedNiss, workflow.getDataCollectionName(), oldPatientIdFromDocument);
		markIfOldPatientIdIsPresent(oldPatientIdFromDocument, hd4ResSsin, otherRecordsWithOldPatientId);
	}

	private void markIfOldPatientIdIsPresent(String oldPatientIdFromDocument, Hd4ResSsin hd4ResSsin, boolean otherRecordsWithOldPatientId) {
		String oldPatientIds = hd4ResSsin.getOldPatientIds();
		List<String> oldPatientIdList;
		boolean otherOldPatientIdPresent;
		if (StringUtils.isNotBlank(oldPatientIds)) {
			otherOldPatientIdPresent = true;
			oldPatientIdList = new ArrayList<>(Arrays.asList(oldPatientIds.split("\\|")));
		} else {
			otherOldPatientIdPresent = false;
			oldPatientIdList = new ArrayList<>();
		}
		if (!oldPatientIdList.contains(oldPatientIdFromDocument)) {
			LOG.info("adding old patient id [{}] to the hd4resSsin [{}]", oldPatientIdFromDocument, hd4ResSsin);
			oldPatientIdList.add(oldPatientIdFromDocument);
			hd4ResSsin.setOldPatientIds(StringUtils.join(oldPatientIdList, '|'));
			if(!"R".equalsIgnoreCase(hd4ResSsin.getRecodeStatus())) {
				if(otherRecordsWithOldPatientId || otherOldPatientIdPresent) {
					hd4ResSsin.setRecodeStatus("M");
				} else {
					hd4ResSsin.setRecodeStatus("X");
				}
			}
			hd4ResSsinDao.update(hd4ResSsin);
		} else {
			LOG.info("oldPatientIdFromDocument [{}] already exists for hd4ResSsin [{}]", oldPatientIdFromDocument, hd4ResSsin);
		}
	}

	@Override
	public boolean markOtherRecordsWithOldPatientId(String landingZoneCodedNiss, String dataCollectionName, String oldPatientId){
		boolean otherRecordsFound = false;
		List<Hd4ResSsin> possibleMathes = hd4ResSsinDao.getPossibleOldIdMatches(dataCollectionName, oldPatientId);
		for (Hd4ResSsin possibleMathe : possibleMathes) {
			String oldPatientIds = possibleMathe.getOldPatientIds();
			List<String> oldPatientIdList;
			if (StringUtils.isNotBlank(oldPatientIds)) {
				oldPatientIdList = new ArrayList<>(Arrays.asList(oldPatientIds.split("\\|")));
			} else {
				oldPatientIdList = new ArrayList<>();
			}
			if (oldPatientIdList.contains(oldPatientId)) {
				if(!landingZoneCodedNiss.equals(possibleMathe.getLandingZoneCodedNiss())) {
					LOG.info("match found for old patient id [{}] on hd4resSsin [{}]", oldPatientId, possibleMathe);
					otherRecordsFound = true;
					if(!"R".equalsIgnoreCase(possibleMathe.getRecodeStatus())) {
						possibleMathe.setRecodeStatus("M");
					}
					hd4ResSsinDao.update(possibleMathe);
				}
			}
		}
		return otherRecordsFound;
	}

	private String getOldPatientIdFromDocument(RegistrationDocument document, RecodeDataCollectionName recodeDataCollectionName) {
		try {
			JSONObject json = new JSONObject(new String(document.getDocumentContent(), StandardCharsets.UTF_8));
			JSONObject value = json.getJSONObject("value");
			return value.getString(recodeDataCollectionName.getOldIdProperty());
		} catch (JSONException e) {
			LOG.error("Couldn't get the old patient id from the document with id : [" + document.getId() + "]", e);
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.INVALID_FORMAT, document.getDocumentContent(), "{\"value\" : {\"" + recodeDataCollectionName.getOldIdProperty() + "\" : \"???\", .... } }");
			throw exception;
		}
	}

	private Set<String> getOptionalPatientIdDataCollectionNames() {
		List<OptionalPatientIdDataCollectionName> optionalPatientIdDataCollectionNames = optionalPatientIdDataCollectionNameDao.getAll();
		Set<String> result = new HashSet<>();
		for (OptionalPatientIdDataCollectionName optionalPatientIdDataCollectionName : optionalPatientIdDataCollectionNames) {
			result.add(optionalPatientIdDataCollectionName.getName());
		}
		return result;
	}

	@Override
	public Set<String> getRecodeDataCollectionNames() {
		List<RecodeDataCollectionName> recodeDataCollectionNames = recodeDataCollectionNameDao.getAll();
		Set<String> result = new HashSet<>();
		for (RecodeDataCollectionName recodeDataCollectionName : recodeDataCollectionNames) {
			result.add(recodeDataCollectionName.getName());
		}
		return result;
	}

	private RecodeDataCollectionName geRecodeDataCollectionName(String dataCollectionName) {
		List<RecodeDataCollectionName> recodeDataCollectionNames = recodeDataCollectionNameDao.getAll();
		for (RecodeDataCollectionName recodeDataCollectionName : recodeDataCollectionNames) {
			if (recodeDataCollectionName.getName().equalsIgnoreCase(dataCollectionName)) {
				return recodeDataCollectionName;
			}
		}
		return null;
	}
}
