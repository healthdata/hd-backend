/*
 *  Copyright 2014-2017 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package be.wiv_isp.healthdata.hd4res.domain.dcd;

import be.wiv_isp.healthdata.common.util.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "DATA_COLLECTION_GROUP_HISTORIES")
public class DataCollectionGroupHistory implements Serializable, Comparable<DataCollectionGroupHistory> {

    private static final long serialVersionUID = 7740195697319851456L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "DATA_COLLECTION_GROUP_HISTORY_ID")
    private Long id;

    @Column(name = "DATA_COLLECTION_GROUPS_ID", nullable = false)
    private Long dataCollectionGroupId;

    @Column(name = "CREATED_ON", nullable = false)

    private Timestamp createdOn;

    @Column(name = "USER_NAME")
    private String userName;

    @Column(name = "DESCRIPTION")
    private String description;

    @Lob
    @Column(name = "PARTICIPATION_CONTENT")
    private byte[] participationContent;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDataCollectionGroupId() {
        return dataCollectionGroupId;
    }

    public void setDataCollectionGroupId(Long dataCollectionGroupId) {
        this.dataCollectionGroupId = dataCollectionGroupId;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getParticipationContent() {
        return participationContent;
    }

    public void setParticipationContent(byte[] participationContent) {
        this.participationContent = participationContent;
    }

    @PrePersist
    public void onCreate() {
        long currentTimeWithoutMilliseconds = 1000 * (new Date().getTime() / 1000);
        setCreatedOn(new Timestamp(currentTimeWithoutMilliseconds));
    }

    @Override
    public int compareTo(DataCollectionGroupHistory other) {
        if (this.createdOn == null && other.createdOn == null) {
            return 0;
        }
        if (this.createdOn == null) {
            return -1;
        }
        if (other.createdOn == null) {
            return 1;
        }
        return this.createdOn.compareTo(other.createdOn);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataCollectionGroupHistory that = (DataCollectionGroupHistory) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(userName, that.userName) &&
                Objects.equals(description, that.description) &&
                Objects.equals(participationContent, that.participationContent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, createdOn, userName, description, participationContent);
    }

    @Override
    public String toString() {
        return StringUtils.toString(this);
    }
}
