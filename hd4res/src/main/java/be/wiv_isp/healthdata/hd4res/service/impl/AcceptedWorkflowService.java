/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.hd4res.action.workflow.UpdateStatusParticipationAction;
import be.wiv_isp.healthdata.hd4res.action.workflow.UpdateStatusRegistrationAction;
import be.wiv_isp.healthdata.hd4res.dao.IAcceptedWorkflowDao;
import be.wiv_isp.healthdata.hd4res.domain.*;
import be.wiv_isp.healthdata.hd4res.service.IAcceptedWorkflowService;
import be.wiv_isp.healthdata.hd4res.service.IDataWarehouseSendingStrategy;
import be.wiv_isp.healthdata.orchestration.action.workflow.AbstractWorkflowAction;
import be.wiv_isp.healthdata.orchestration.dao.IWorkflowDao;
import be.wiv_isp.healthdata.orchestration.domain.AbstractWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.SendStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.factory.IAbstractMessageFactory;
import be.wiv_isp.healthdata.orchestration.factory.IActionFactory;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class AcceptedWorkflowService implements IAcceptedWorkflowService {

    private static final Logger LOG = LoggerFactory.getLogger(AcceptedWorkflowService.class);

    @Autowired
    private IActionFactory actionFactory;

    @Autowired
    private IMessageService messageService;

    @Autowired
    private IAbstractMessageFactory messageFactory;

    @Autowired
    private IAcceptedWorkflowDao acceptedWorkflowDao;

    @Autowired
    private IWorkflowDao<ParticipationWorkflow, ?> participationWorkflowDao;

    @Autowired
    private IWorkflowDao<RegistrationWorkflow, ?> registrationWorkflowDao;

    @Autowired
    private IDataWarehouseSendingStrategy<ParticipationDocument> databaseDataWarehouseSendingParticipationStrategy;

    @Autowired
    private IDataWarehouseSendingStrategy<RegistrationDocument> databaseDataWarehouseSendingRegistrationStrategy;

    @Override
    public void send(RegistrationDocument document) {
        databaseDataWarehouseSendingRegistrationStrategy.send(document);
    }

    @Override
    public void send(ParticipationDocument document) {
        databaseDataWarehouseSendingParticipationStrategy.send(document);
    }

    @Override
    @Transactional
    public boolean process(final AcceptedWorkflow acceptedWorkflow) {
        try {
            final Long id = acceptedWorkflow.getId();
            LOG.debug("Start sending accepted workflow with id [{}] to HD4DP", id);
            AbstractWorkflow workflow;
            final AbstractWorkflowAction action;
            final Message message;
            switch (acceptedWorkflow.getWorkflowType()) {
                case PARTICIPATION:
                    action = (UpdateStatusParticipationAction) actionFactory.getActionByActionType(WorkflowActionType.UPDATE_STATUS, WorkflowType.PARTICIPATION);
                    final ParticipationWorkflow participationWorkflow = participationWorkflowDao.get(id);
                    action.setDocumentVersion(participationWorkflow.getDocument().getHd4dpVersion());
                    action.setEndStatus(WorkflowStatus.APPROVED);
                    workflow = participationWorkflow;
                    message = messageFactory.createOutgoingMessage(action, participationWorkflow);
                    break;
                case REGISTRATION:
                default:
                    action = (UpdateStatusRegistrationAction) actionFactory.getActionByActionType(WorkflowActionType.UPDATE_STATUS, WorkflowType.REGISTRATION);
                    final RegistrationWorkflow registrationWorkflow = registrationWorkflowDao.get(id);
                    action.setDocumentVersion(registrationWorkflow.getDocument().getHd4dpVersion());
                    action.setEndStatus(WorkflowStatus.APPROVED);
                    workflow = registrationWorkflow;
                    message = messageFactory.createOutgoingMessage(action, registrationWorkflow);
            }
            messageService.create(message);
            acceptedWorkflowDao.delete(acceptedWorkflow);
            workflow.setSendStatus(SendStatus.PENDING); // TODO: this could be done in the database trigger
            switch (acceptedWorkflow.getWorkflowType()) {
                case PARTICIPATION:
                    participationWorkflowDao.update((ParticipationWorkflow)workflow);
                    break;
                case REGISTRATION:
                default:
                    registrationWorkflowDao.update((RegistrationWorkflow)workflow);
            }
            LOG.debug("Done sending accepted workflow with id [{}] to HD4DP", id);
            return true;
        } catch (Exception e) {
            switch (acceptedWorkflow.getWorkflowType()) {
                case PARTICIPATION:
                    LOG.error("Error while sending accepted participation {} to HD4DP. Deleting entry", acceptedWorkflow.getId(), e);
                    break;
                case REGISTRATION:
                default:
                    LOG.error("Error while sending accepted registration {} to HD4DP. Deleting entry", acceptedWorkflow.getId(), e);
            }
            acceptedWorkflow.setErrorProcessing(true);
            acceptedWorkflowDao.update(acceptedWorkflow);
            return false;
        }

    }
}
