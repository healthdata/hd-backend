/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.tasks;

import be.wiv_isp.healthdata.orchestration.domain.FollowUp;
import be.wiv_isp.healthdata.orchestration.domain.search.FollowUpSearch;
import be.wiv_isp.healthdata.orchestration.service.IAbstractFollowUpService;
import be.wiv_isp.healthdata.orchestration.tasks.HealthDataTask;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class FollowUpTask extends HealthDataTask {

    private static final Logger LOG = LoggerFactory.getLogger(FollowUpTask.class);

    public static final int TWO_DAYS_AS_MILLIS = 1000 * 3600 * 48;

    @Autowired
    private IAbstractFollowUpService followUpService;

    @Override
    public String getName() {
        return "FOLLOW_UP";
    }

    @Override
    public void execute() {
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -2);
        final Date activationDateUpperBound = calendar.getTime();

        final FollowUpSearch search = new FollowUpSearch();
        search.setActivationDateBefore(activationDateUpperBound);
        search.setActive(false);
        LOG.debug("Retrieving non-active follow-ups for which the activation date is before {}", activationDateUpperBound);
        final List<FollowUp> all = followUpService.getAll(search);
        if(CollectionUtils.isEmpty(all)) {
            return;
        }
        LOG.warn("{} matching follow-up item(s) found.", all.size());

        // TODO-follow-up
    }
}
