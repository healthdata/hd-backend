/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.api;

import be.wiv_isp.healthdata.orchestration.annotation.Auditable;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ApiType;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.hd4res.domain.UserRequest;
import be.wiv_isp.healthdata.hd4res.domain.search.UserRequestSearch;
import be.wiv_isp.healthdata.hd4res.dto.UserRequestDto;
import be.wiv_isp.healthdata.hd4res.service.IUserRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;

@Component
@Path("/userrequests")
public class UserRequestRestService {

	@Autowired
	private IUserRequestService userRequestService;

	@Transactional
	@POST
	@Path("/{id}/archive/{dataCollectionName}")
	@PreAuthorize("hasRole('ROLE_USER')")
	@Auditable(apiType = ApiType.USER_REQUEST)
	@Produces(MediaType.APPLICATION_JSON)
	public Response reject(@Context UriInfo info) throws Exception {
		final Long id = RestUtils.getPathLong(info, "id");
		final String dataCollectionName = RestUtils.getPathString(info, RestUtils.ParameterName.DATA_COLLECTION_NAME);

		userRequestService.archive(id, dataCollectionName);
		return Response.noContent().build();
	}

	@GET
	@PreAuthorize("hasRole('ROLE_USER')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(@Context UriInfo info) {
		final List<UserRequest> userRequests = userRequestService.getAll(new UserRequestSearch(info));
		return Response.ok(convert(userRequests)).build();
	}

	private List<UserRequestDto> convert(List<UserRequest> userRequests) {
		final List<UserRequestDto> converted = new ArrayList<>();
		for (UserRequest userRequest: userRequests) {
			converted.add(new UserRequestDto(userRequest));
		}
		return converted;
	}
}
