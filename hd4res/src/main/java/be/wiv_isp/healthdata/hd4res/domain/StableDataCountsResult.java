/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class StableDataCountsResult {

    private Map<String, CountsPerInstallation> countsPerInstallation;

    public Map<String, CountsPerInstallation> getCountsPerInstallation() {
        return countsPerInstallation;
    }

    public void setCountsPerInstallation(Map<String, CountsPerInstallation> countsPerInstallation) {
        this.countsPerInstallation = countsPerInstallation;
    }

    public void setCount(String identificationValue, String dataCollectionName, StableData.Status status, Long count) {
        if(countsPerInstallation == null) {
            countsPerInstallation = new HashMap<>();
        }
        if(countsPerInstallation.get(identificationValue) == null) {
            countsPerInstallation.put(identificationValue, new CountsPerInstallation());
        }

        countsPerInstallation.get(identificationValue).setCount(dataCollectionName, status, count);
    }

    public Long getCount(String identificationValue, String dataCollectionName, StableData.Status status) {
        if(countsPerInstallation == null) {
            return null;
        }

        if(countsPerInstallation.get(identificationValue) == null) {
            return null;
        }

        return countsPerInstallation.get(identificationValue).getCount(dataCollectionName, status);
    }

    public class CountsPerInstallation {
        private Map<String, CountsPerDataCollection> countsPerDataCollection;

        public Map<String, CountsPerDataCollection> getCountsPerDataCollection() {
            return countsPerDataCollection;
        }

        public void setCountsPerDataCollection(Map<String, CountsPerDataCollection> countsPerDataCollection) {
            this.countsPerDataCollection = countsPerDataCollection;
        }

        public void setCount(String dataCollectionName, StableData.Status status, Long count) {
            if(countsPerDataCollection == null) {
                countsPerDataCollection = new HashMap<>();
            }
            if(countsPerDataCollection.get(dataCollectionName) == null) {
                countsPerDataCollection.put(dataCollectionName, new CountsPerDataCollection());
            }

            countsPerDataCollection.get(dataCollectionName).setCount(status, count);
        }

        public Long getCount(String dataCollectionName, StableData.Status status) {
            if(countsPerDataCollection == null) {
                return null;
            }

            if(countsPerDataCollection.get(dataCollectionName) == null) {
                return null;
            }

            return countsPerDataCollection.get(dataCollectionName).getCount(status);
        }
    }

    public class CountsPerDataCollection {
        private Map<String, Long> counts;

        public Map<String, Long> getCounts() {
            return counts;
        }

        public void setCounts(Map<String, Long> counts) {
            this.counts = counts;
        }

        public void setCount(StableData.Status status, Long count) {
            if(counts == null) {
                counts = new HashMap<>();
            }
            counts.put(status.toString().toLowerCase(), count);
        }

        public long getCount() {
            return getCount(null);
        }

        public long getCount(StableData.Status status) {
            if(counts == null) {
                return 0;
            }

            if(status == null) {
                return sum(counts.values());
            }

            return counts.get(status.toString().toLowerCase()) != null ? counts.get(status.toString().toLowerCase()) : 0;
        }

        private long sum(Collection<Long> values) {
            long sum = 0;
            for (Long value : values) {
                sum += value;
            }
            return sum;
        }

        public String getGlobalStatus() {
            if(getCount(StableData.Status.ERROR_SENDING) > 0) {
                return "error";
            }

            if(getCount(StableData.Status.ERROR_UPLOADING) > 0) {
                return "error";
            }

            if(getCount(StableData.Status.UPLOADED) == getCount()) {
                return "completed";
            }

            return "in_progress";
        }
    }
}
