/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4res.api;

import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IWebServiceClientService;
import be.wiv_isp.healthdata.common.caching.ICacheManagementService;
import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.common.util.TokenUtils;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;
import java.util.List;

@Component
@Path("/cache")
public class CacheManagementRestService {

    private static final Logger LOG = LoggerFactory.getLogger(CacheManagementRestService.class);

    @Autowired
    private ICacheManagementService cacheManagementService;
    @Autowired
    private IConfigurationService configurationService;
    @Autowired
    private IWebServiceClientService webServiceClientService;


    @GET
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCacheNames(@Context UriInfo info) {
        Boolean toCatalogue = false;
        MultivaluedMap<String, String> params = info.getQueryParameters();
        for (String key : params.keySet()) {
            if ("catalogue".equalsIgnoreCase(key))
                toCatalogue = Boolean.parseBoolean(params.getFirst(key));
        }
        if (Boolean.TRUE.equals(toCatalogue)) {
            return getCatalogueCacheNames();
        }
        else {
            return Response.ok(cacheManagementService.getCacheNames(Platform.HD4RES)).build();
        }
    }

    @POST
    @Path("/evict")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Response evict(@Context UriInfo info) {
        String cacheName = null;
        Boolean toCatalogue = false;
        MultivaluedMap<String, String> params = info.getQueryParameters();
        for (String key : params.keySet()) {
            if ("cacheName".equalsIgnoreCase(key))
                cacheName = params.getFirst(key);
            else if ("catalogue".equalsIgnoreCase(key))
                toCatalogue = Boolean.parseBoolean(params.getFirst(key));
        }
        if (cacheName == null)
            return Response.status(Response.Status.BAD_REQUEST).build();

        if (Boolean.FALSE.equals(toCatalogue)) {
            LOG.debug("Evicting cache [{}]", cacheName);

            cacheManagementService.evict(cacheName);

            return Response.noContent().build();
        }
        else {
            LOG.debug("Evicting catalogue cache [{}]", cacheName);

            return catalogueEvict(cacheName);
        }

    }

    private Response getCatalogueCacheNames() {
        Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);
        Configuration catalogueUserName = configurationService.get(ConfigurationKey.CATALOGUE_USERNAME);
        Configuration cataloguePassword = configurationService.get(ConfigurationKey.CATALOGUE_PASSWORD);
        String accessToken = TokenUtils.getAccessTokenCatalogue(catalogueHost.getValue(), catalogueUserName.getValue(), cataloguePassword.getValue());

        WebServiceBuilder builder = new WebServiceBuilder();
        builder.setUrl(catalogueHost.getValue() + "/cache");
        builder.setGet(true);
        builder.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
        builder.setAccept(MediaType.APPLICATION_JSON_TYPE);
        builder.setReturnType(new GenericType<ClientResponse>(){});

        ClientResponse response = (ClientResponse) webServiceClientService.callWebService(builder);

        if (response.getStatus() == ClientResponse.Status.OK.getStatusCode())
            return Response.ok(response.getEntity(new GenericType<List<String>>() {})).build();
        else
            return Response.serverError().build();
    }

    private Response catalogueEvict(String cacheName) {
        Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);
        Configuration catalogueUserName = configurationService.get(ConfigurationKey.CATALOGUE_USERNAME);
        Configuration cataloguePassword = configurationService.get(ConfigurationKey.CATALOGUE_PASSWORD);
        String accessToken = TokenUtils.getAccessTokenCatalogue(catalogueHost.getValue(), catalogueUserName.getValue(), cataloguePassword.getValue());

        WebServiceBuilder builder = new WebServiceBuilder();
        builder.setUrl(catalogueHost.getValue() + "/cache/evict?cacheName=" + cacheName);
        builder.setPost(true);
        builder.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
        builder.setAccept(MediaType.APPLICATION_JSON_TYPE);
        builder.setReturnType(new GenericType<ClientResponse>(){});

        ClientResponse response = (ClientResponse) webServiceClientService.callWebService(builder);

        if (response.getStatus()==ClientResponse.Status.NO_CONTENT.getStatusCode())
            return Response.noContent().build();
        else
            return Response.status(response.getStatus()).entity(response.getEntityInputStream()).build();
    }

}
