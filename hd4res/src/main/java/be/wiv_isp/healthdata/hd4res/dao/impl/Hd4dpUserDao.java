/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.hd4res.dao.IHd4dpUserDao;
import be.wiv_isp.healthdata.hd4res.domain.Hd4dpAuthority;
import be.wiv_isp.healthdata.hd4res.domain.Hd4dpUser;
import be.wiv_isp.healthdata.hd4res.domain.search.Hd4dpUserSearch;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class Hd4dpUserDao extends CrudDaoV2<Hd4dpUser, Long, Hd4dpUserSearch> implements IHd4dpUserDao {

    public Hd4dpUserDao() {
        super(Hd4dpUser.class);
    }

    @Override
    protected List<Predicate> getPredicates(Hd4dpUserSearch search, CriteriaBuilder cb, Root<Hd4dpUser> rootEntry) {
        List<Predicate> predicates = new ArrayList<>();
        if (search.getHd4dpId() != null) {
            predicates.add(cb.equal(rootEntry.get("hd4dpId"), search.getHd4dpId()));
        }
        if (search.getHealthDataIdentificationType() != null) {
            predicates.add(cb.equal(rootEntry.<HealthDataIdentification>get("healthDataIdentification").get("type"), search.getHealthDataIdentificationType()));
        }
        if (search.getHealthDataIdentificationValue() != null) {
            predicates.add(cb.equal(rootEntry.<HealthDataIdentification>get("healthDataIdentification").get("value"), search.getHealthDataIdentificationValue()));
        }
        if (search.getDataCollectionName() != null) {
            final SetJoin<Hd4dpUser, String> join = rootEntry.joinSet("dataCollectionNames");
            predicates.add(cb.equal(join, search.getDataCollectionName()));
        }
        if (search.getAuthority() != null) {
            final SetJoin<Hd4dpUser, Hd4dpAuthority> join = rootEntry.joinSet("authorities");
            predicates.add(cb.equal(join.get("authority"), search.getAuthority().getAuthority()));
        }
        return predicates;
    }
}
