/*
 *  Copyright 2014-2017 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package be.wiv_isp.healthdata.hd4res.mapper;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.dto.PeriodDto;
import be.wiv_isp.healthdata.common.dto.TranslatableStringDto;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.hd4res.dto.DataCollectionGroupManagementDto;
import org.apache.commons.lang3.time.DateUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;

public class DataCollectionGroupRequestMapper {

	static public DataCollectionGroupManagementDto convert(JSONObject json) throws Exception {
		DataCollectionGroupManagementDto dataCollectionGroup = new DataCollectionGroupManagementDto();
		for (@SuppressWarnings("unchecked")
		Iterator<String> iterator = json.keys(); iterator.hasNext();) {
			String key = iterator.next();
			if (!"null".equalsIgnoreCase(json.getString(key))) {
				if ("dataCollectionGroupId".equalsIgnoreCase(key)) {
					try {
						dataCollectionGroup.setDataCollectionGroupId(Long.valueOf(json.getString(key)));
					} catch (NumberFormatException e) {
						HealthDataException exception = new HealthDataException();
						exception.setExceptionType(ExceptionType.INVALID_FORMAT, json.getString(key), "long");
						throw exception;
					}
				} else if ("dataCollectionGroupName".equalsIgnoreCase(key)) {
					dataCollectionGroup.setDataCollectionGroupName(json.getString(key));
				} else if ("label".equalsIgnoreCase(key)) {
					final TranslatableStringDto label = getTranslatableStringDto(json, key);
					dataCollectionGroup.setLabel(label);
				} else if ("description".equalsIgnoreCase(key)) {
					final TranslatableStringDto description = getTranslatableStringDto(json, key);
					dataCollectionGroup.setDescription(description);
				} else if ("participationContent".equalsIgnoreCase(key)) {
					final JSONObject contentJSON = json.getJSONObject(key);
					dataCollectionGroup.setParticipationContent(contentJSON.toString().getBytes(StandardCharsets.UTF_8));
				}  else if ("userDescription".equalsIgnoreCase(key)) {
					dataCollectionGroup.setUserDescription(json.getString(key));
				}  else if ("majorVersion".equalsIgnoreCase(key)) {
					dataCollectionGroup.setMajorVersion(json.getInt(key));
				} else if ("published".equalsIgnoreCase(key)) {
					dataCollectionGroup.setPublished(json.getBoolean(key));
				} else if ("startDate".equalsIgnoreCase(key)) {
					try {
						final Date asDate = DateUtils.parseDate(json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
						dataCollectionGroup.setStartDate(new Timestamp(asDate.getTime()));
					} catch (ParseException e) {
						HealthDataException exception = new HealthDataException();
						exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
						throw exception;
					}
				} else if ("endDateCreation".equalsIgnoreCase(key)) {
					try {
						final Date asDate = DateUtils.parseDate(json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
						dataCollectionGroup.setEndDateCreation(new Timestamp(asDate.getTime()));
					} catch (ParseException e) {
						HealthDataException exception = new HealthDataException();
						exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
						throw exception;
					}
				} else if ("endDateSubmission".equalsIgnoreCase(key)) {
					try {
						final Date asDate = DateUtils.parseDate(json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
						dataCollectionGroup.setEndDateSubmission(new Timestamp(asDate.getTime()));
					} catch (ParseException e) {
						HealthDataException exception = new HealthDataException();
						exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
						throw exception;
					}
				} else if ("endDateComments".equalsIgnoreCase(key)) {
					try {
						final Date asDate = DateUtils.parseDate(json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
						dataCollectionGroup.setEndDateComments(new Timestamp(asDate.getTime()));
					} catch (ParseException e) {
						HealthDataException exception = new HealthDataException();
						exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
						throw exception;
					}
				} else if ("createdOn".equalsIgnoreCase(key)) {
					try {
						final Date asDate = DateUtils.parseDate(json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
						dataCollectionGroup.setCreatedOn(new Timestamp(asDate.getTime()));
					} catch (ParseException e) {
						HealthDataException exception = new HealthDataException();
						exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
						throw exception;
					}
				}  else if ("updatedOn".equalsIgnoreCase(key)) {
					try {
						final Date asDate = DateUtils.parseDate(json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
						dataCollectionGroup.setUpdatedOn(new Timestamp(asDate.getTime()));
					} catch (ParseException e) {
						HealthDataException exception = new HealthDataException();
						exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
						throw exception;
					}
				} else if ("period".equalsIgnoreCase(key)) {
					final PeriodDto period = new PeriodDto();
					final JSONObject periodJSON = json.getJSONObject(key);
					if (periodJSON.names() != null) {
						for (int j = 0; j < periodJSON.names().length(); ++j) {
							final String versionKey = periodJSON.names().getString(j);
							if ("start".equalsIgnoreCase(versionKey)) {
								period.setStart(periodJSON.getString(versionKey));
							} else if ("end".equalsIgnoreCase(versionKey)) {
								period.setEnd(periodJSON.getString(versionKey));
							}
						}
					}
					dataCollectionGroup.setPeriod(period);
				}
			}
		}
		return dataCollectionGroup;
	}

	public static TranslatableStringDto getTranslatableStringDto(JSONObject json, String key) throws JSONException {
		final TranslatableStringDto label = new TranslatableStringDto();
		final JSONObject labelJSON = json.getJSONObject(key);
		if (labelJSON.names() != null) {
            for (int j = 0; j < labelJSON.names().length(); ++j) {
                final String versionKey = labelJSON.names().getString(j);
                if ("en".equalsIgnoreCase(versionKey)) {
                    label.setEn(labelJSON.getString(versionKey));
                } else if ("fr".equalsIgnoreCase(versionKey)) {
                    label.setFr(labelJSON.getString(versionKey));
                } else if ("nl".equalsIgnoreCase(versionKey)) {
                    label.setNl(labelJSON.getString(versionKey));
                }
            }
        }
		return label;
	}
}
