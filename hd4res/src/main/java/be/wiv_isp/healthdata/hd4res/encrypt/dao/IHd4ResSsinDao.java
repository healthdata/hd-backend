/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.encrypt.dao;

import be.wiv_isp.healthdata.common.dao.ICrudDao;
import be.wiv_isp.healthdata.hd4res.domain.search.Hd4ResSsinSearch;
import be.wiv_isp.healthdata.hd4res.encrypt.domain.Hd4ResSsin;

import java.util.List;

public interface IHd4ResSsinDao extends ICrudDao<Hd4ResSsin, String> {

	List<Hd4ResSsin> getAll(Hd4ResSsinSearch search);

	String getBiggest(String registerName);

	List<Hd4ResSsin> getPossibleOldIdMatches(String dataCollectionName, String oldPatientId);
}
