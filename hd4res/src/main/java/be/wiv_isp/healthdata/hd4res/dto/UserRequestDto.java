/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dto;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.orchestration.dto.HealthDataIdentificationDto;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import be.wiv_isp.healthdata.hd4res.domain.UserRequest;
import be.wiv_isp.healthdata.orchestration.dto.AbstractUserRequestDto;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.sql.Timestamp;

public class UserRequestDto extends AbstractUserRequestDto<UserRequest> {

    private Timestamp requestedOn;
    private HealthDataIdentificationDto healthDataIdentification;

    public UserRequestDto(UserRequest userRequest) {
        super(userRequest);
        requestedOn = userRequest.getRequestedOn();
        dataCollectionNames = userRequest.getDataCollectionNames();
        setHealthDataIdentification(userRequest.getHealthDataIdentification());
    }

    @JsonSerialize(using = TimestampSerializer.class)
    public Timestamp getRequestedOn() {
        return requestedOn;
    }

    public void setRequestedOn(Timestamp requestedOn) {
        this.requestedOn = requestedOn;
    }

    public HealthDataIdentificationDto getHealthDataIdentification() {
        return healthDataIdentification;
    }

    public void setHealthDataIdentification(HealthDataIdentification healthDataIdentification) {
        if(healthDataIdentification != null) {
            this.healthDataIdentification = new HealthDataIdentificationDto();
            this.healthDataIdentification.setName(healthDataIdentification.getName());
            this.healthDataIdentification.setType(healthDataIdentification.getType());
            this.healthDataIdentification.setValue(healthDataIdentification.getValue());
        }
    }
}
