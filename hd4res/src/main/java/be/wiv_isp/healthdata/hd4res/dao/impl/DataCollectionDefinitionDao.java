/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.hd4res.dao.IDataCollectionDefinitionDao;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionDefinition;
import be.wiv_isp.healthdata.hd4res.domain.search.DataCollectionDefinitionSearch;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;

@Repository
public class DataCollectionDefinitionDao extends CrudDaoV2<DataCollectionDefinition, Long, DataCollectionDefinitionSearch> implements IDataCollectionDefinitionDao {

	public DataCollectionDefinitionDao() {
		super(DataCollectionDefinition.class);
	}


	@Override
	protected List<Predicate> getPredicates(DataCollectionDefinitionSearch search, CriteriaBuilder cb, Root<DataCollectionDefinition> rootEntry) {
		final List<Predicate> predicates = new ArrayList<>();

		if (search.getDataCollectionDefinitionName() != null) {
			predicates.add(cb.equal(cb.upper(rootEntry.<String> get("dataCollectionName")), search.getDataCollectionDefinitionName().toUpperCase()));
		}
		if (search.getDataCollectionGroupId() != null) {
			predicates.add(cb.equal(rootEntry.get("dataCollectionGroupId"), search.getDataCollectionGroupId()));
		}
		if (search.getMinorVersion() != null) {
			predicates.add(cb.equal(rootEntry.get("minorVersion"), search.getMinorVersion()));
		}
		if (search.getPublished() != null) {
			predicates.add(cb.equal(rootEntry.get("published"), search.getPublished()));
		}

		return predicates;
	}

	@Override
	public List<DataCollectionDefinition> get(String name) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DataCollectionDefinition> cq = cb.createQuery(DataCollectionDefinition.class);
		Root<DataCollectionDefinition> rootEntry = cq.from(DataCollectionDefinition.class);
		final List<Predicate> predicates = new ArrayList<>();

		predicates.add(cb.equal(cb.upper(rootEntry.<String> get("dataCollectionName")), name.toUpperCase()));
		cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		cq.orderBy(cb.desc(rootEntry.get("createdOn")));

		TypedQuery<DataCollectionDefinition> query = em.createQuery(cq);
		List<DataCollectionDefinition> resultList = query.getResultList();

		return resultList.isEmpty() ? null : resultList;
	}

	@Override
	public List<DataCollectionDefinition> latestMinors(Long dataCollectionGroupId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DataCollectionDefinition> cQuery = cb.createQuery(DataCollectionDefinition.class);
		Root<DataCollectionDefinition> dcd = cQuery.from(DataCollectionDefinition.class);

		final List<Predicate> predicates = new ArrayList<>();

		predicates.add(cb.equal(dcd.<String> get("dataCollectionGroupId"), dataCollectionGroupId));
		
		cQuery.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));

		TypedQuery<DataCollectionDefinition> allQuery = em.createQuery(cQuery);
		List<DataCollectionDefinition> resultList = allQuery.getResultList();

		if (resultList.isEmpty()) {
			return Collections.emptyList();
		} else {
			Map<String,DataCollectionDefinition> latestDataCollectionDefinitions = new HashMap<>();
			for (DataCollectionDefinition dataCollectionDefinition : resultList) {
				if (latestDataCollectionDefinitions.get(dataCollectionDefinition.getDataCollectionName()) == null
						|| latestDataCollectionDefinitions.get(dataCollectionDefinition.getDataCollectionName()).getMinorVersion() < dataCollectionDefinition.getMinorVersion()) {
					latestDataCollectionDefinitions.put(dataCollectionDefinition.getDataCollectionName(), dataCollectionDefinition);
				}
			}
			return new ArrayList<>(latestDataCollectionDefinitions.values());
		}
	}

	@Override
	public List<String> getExistingDataCollections() {
		CriteriaBuilder cb = em.getCriteriaBuilder();

		CriteriaQuery<String> criteria = cb.createQuery(String.class);
		Root<DataCollectionDefinition> root = criteria.from(DataCollectionDefinition.class);
		criteria.multiselect(root.get("dataCollectionName")).distinct(true);

		return em.createQuery(criteria).getResultList();
	}
}
