/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDao;
import be.wiv_isp.healthdata.hd4res.dao.IRecodeDataCollectionNameDao;
import be.wiv_isp.healthdata.hd4res.domain.RecodeDataCollectionName;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class RecodeDataCollectionNameDao extends CrudDao<RecodeDataCollectionName, String> implements IRecodeDataCollectionNameDao {

	public RecodeDataCollectionNameDao() {
		super(RecodeDataCollectionName.class);
	}

	@Override
	public List<RecodeDataCollectionName> getAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<RecodeDataCollectionName> cq = cb.createQuery(entityClass);
		Root<RecodeDataCollectionName> rootEntry = cq.from(entityClass);

		CriteriaQuery<RecodeDataCollectionName> all = cq.select(rootEntry);

		TypedQuery<RecodeDataCollectionName> allQuery = em.createQuery(all);
		return allQuery.getResultList();
	}

}
