/*
 *  Copyright 2014-2017 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.common.api.ClientVersion;
import be.wiv_isp.healthdata.common.api.exception.NotFoundException;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupDto;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.common.util.TokenUtils;
import be.wiv_isp.healthdata.hd4res.dao.IDataCollectionGroupHistoryDao;
import be.wiv_isp.healthdata.hd4res.domain.Description;
import be.wiv_isp.healthdata.hd4res.domain.Label;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionDefinition;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionGroup;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionGroupHistory;
import be.wiv_isp.healthdata.hd4res.domain.dcd.Period;
import be.wiv_isp.healthdata.hd4res.domain.search.DataCollectionGroupHistorySearch;
import be.wiv_isp.healthdata.hd4res.domain.search.DataCollectionGroupSearch;
import be.wiv_isp.healthdata.hd4res.dto.DataCollectionGroupManagementDto;
import be.wiv_isp.healthdata.hd4res.dto.converters.DataCollectionGroupDtoConverter;
import be.wiv_isp.healthdata.hd4res.dto.converters.DataCollectionGroupManagementDtoConverter;
import be.wiv_isp.healthdata.hd4res.mapper.DataCollectionGroupRequestMapper;
import be.wiv_isp.healthdata.hd4res.service.IDataCollectionDefinitionService;
import be.wiv_isp.healthdata.hd4res.service.IDataCollectionGroupManagementService;
import be.wiv_isp.healthdata.hd4res.service.IDataCollectionGroupService;
import be.wiv_isp.healthdata.orchestration.api.uri.DataCollectionGroupUri;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.DataCollection;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.*;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class DataCollectionGroupManagementService implements IDataCollectionGroupManagementService {

    private static final Logger LOG = LoggerFactory.getLogger(DataCollectionGroupManagementService.class);

    @Autowired
    private IWebServiceClientService webServiceClientService;
    @Autowired
    private IConfigurationService configurationService;
    @Autowired
    private IDataCollectionGroupService dataCollectionGroupService;
    @Autowired
    private IDataCollectionGroupHistoryDao dataCollectionGroupHistoryDao;
    @Autowired
    private IDataCollectionDefinitionService dataCollectionDefinitionService;
    @Autowired
    protected IUserDataCollectionService userDataCollectionService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IDataCollectionService dataCollectionService;


    @Override
    public DataCollectionGroupManagementDto get(Long groupId) {
        DataCollectionGroup group = getGroup(groupId);
        checkAccess(group);
        DataCollectionGroupHistory history = getHistory(group);

        List<DataCollectionDefinition> definitions = dataCollectionDefinitionService.getAll(groupId);
        return DataCollectionGroupManagementDtoConverter.convert(group, history, definitions);
    }

    @Override
    public List<DataCollectionGroupManagementDto> getAll() {
        List<DataCollectionGroup> groups = dataCollectionGroupService.getAll();
        final List<DataCollectionGroupManagementDto> results = new ArrayList<>();
        for (DataCollectionGroup group : groups) {
            if(hasAccess(group)) {
                DataCollectionGroupHistory history = getHistory(group);
                results.add(DataCollectionGroupManagementDtoConverter.convert(group, history, false));
            }
        }
        return results;
    }

    @Override
    public DataCollectionGroupManagementDto create(JSONObject json, String userName) throws Exception {
        checkUseDDE();
        DataCollectionGroupManagementDto dto = unmarshaller(json);
        checkGroupName(dto.getDataCollectionGroupName());
        checkAccess(dto.getDataCollectionGroupName());
        DataCollectionGroup latestVersion = getLatestVersion(dto.getDataCollectionGroupName());
        if (latestVersion == null ? !dto.getMajorVersion().equals(1): !dto.getMajorVersion().equals(latestVersion.getMajorVersion() + 1)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_CONFLICT, "Version numbers must be sequential");
            throw exception;
        }
        DataCollectionGroup group = new DataCollectionGroup();
        group.setName(dto.getDataCollectionGroupName());
        group.setMajorVersion(dto.getMajorVersion());
        if(dto.getDescription() != null) {
            Description description = new Description();
            description.setEn(dto.getDescription().getEn());
            description.setFr(dto.getDescription().getFr());
            description.setNl(dto.getDescription().getNl());
            group.setDescription(description);
        }
        if(dto.getLabel() != null) {
            Label label = new Label();
            label.setEn(dto.getLabel().getEn());
            label.setFr(dto.getLabel().getFr());
            label.setNl(dto.getLabel().getNl());
            group.setLabel(label);
        }
        group.setPublished(false);
        if(dto.getPeriod() != null) {
            Period period = new Period();
            period.setStart(dto.getPeriod().getStart());
            period.setEnd(dto.getPeriod().getEnd());
        }
        group.setStartDate(dto.getStartDate());
        group.setEndDateCreation(dto.getEndDateCreation());
        group.setEndDateSubmission(dto.getEndDateSubmission());
        group.setEndDateComments(dto.getEndDateComments());
        dataCollectionGroupService.create(group);

        DataCollectionGroupHistory history = new DataCollectionGroupHistory();
        history.setDataCollectionGroupId(group.getId());
        history.setParticipationContent(dto.getParticipationContent());
        history.setDescription(dto.getUserDescription());
        history.setUserName(userName);
        dataCollectionGroupHistoryDao.create(history);

        group.setHistoryId(history.getId());
        dataCollectionGroupService.update(group);

        DataCollectionGroupDto catalogueGroup = DataCollectionGroupDtoConverter.convert(group, history);
        postToCatalogue(catalogueGroup, true);
        return DataCollectionGroupManagementDtoConverter.convert(group, history, true);
    }

    private DataCollectionGroup getGroup(Long groupId) {
        DataCollectionGroup group = dataCollectionGroupService.get(groupId);
        if(group == null) {
            throw new NotFoundException("No data collection group found with id " + groupId);
        }
        return group;
    }

    private DataCollectionGroup getLatestVersion(String groupName) {
        DataCollectionGroupSearch search = new DataCollectionGroupSearch();
        search.setName(groupName);
        List<DataCollectionGroup> groups = dataCollectionGroupService.getAll(search);
        DataCollectionGroup latestVersion = null;
        for (DataCollectionGroup group : groups) {
            if (latestVersion == null || group.getMajorVersion() > latestVersion.getMajorVersion()) {
                latestVersion = group;
            }
        }
        return latestVersion;
    }

    private void checkGroupName(String groupName) {
        Set<DataCollection> groups = dataCollectionService.getDataCollectionGroups();
        for (DataCollection group : groups) {
            if(group.getName().equals(groupName)) {
                return;
            }
        }
        HealthDataException exception = new HealthDataException();
        exception.setExceptionType(ExceptionType.GENERAL_CONFLICT, "The data collection group name [" + groupName + "] doesn't exist in salesForce");
        throw exception;
    }


    @Override
    public DataCollectionGroupManagementDto update(Long groupId, JSONObject json, String userName) throws Exception {
        checkUseDDE();
        DataCollectionGroupManagementDto dto = unmarshaller(json);
        if (groupId == null || !groupId.equals(dto.getDataCollectionGroupId())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, MessageFormat.format("The id provided in the url ({0}) does not match with the id provided in the request body ({1})", groupId, dto.getDataCollectionGroupId()));
            throw exception;
        }
        DataCollectionGroup group = getGroup(groupId);
        checkAccess(group.getName());
        DataCollectionGroupHistory history = new DataCollectionGroupHistory();
        history.setDataCollectionGroupId(groupId);
        history.setParticipationContent(dto.getParticipationContent());
        history.setDescription(dto.getUserDescription());
        history.setUserName(userName);
        dataCollectionGroupHistoryDao.create(history);

        group.setHistoryId(history.getId());
        if(dto.getDescription() != null) {
            if(group.getDescription() == null) {
                group.setDescription(new Description());
            }
            Description description = group.getDescription();
            description.setEn(dto.getDescription().getEn());
            description.setFr(dto.getDescription().getFr());
            description.setNl(dto.getDescription().getNl());
            group.setDescription(description);
        }
        if(dto.getLabel() != null) {
            if(group.getLabel() == null) {
                group.setLabel(new Label());
            }
            Label label = group.getLabel();
            label.setEn(dto.getLabel().getEn());
            label.setFr(dto.getLabel().getFr());
            label.setNl(dto.getLabel().getNl());
            group.setLabel(label);
        }
        if(dto.getPeriod() != null) {
            Period period = new Period();
            period.setStart(dto.getPeriod().getStart());
            period.setEnd(dto.getPeriod().getEnd());
            group.setPeriod(period);
        }
        group.setStartDate(dto.getStartDate());
        group.setEndDateCreation(dto.getEndDateCreation());
        group.setEndDateSubmission(dto.getEndDateSubmission());
        group.setEndDateComments(dto.getEndDateComments());

        dataCollectionGroupService.update(group);

        DataCollectionGroupDto sacDataCollectionGroup = DataCollectionGroupDtoConverter.convert(group, history);
        postToCatalogue(sacDataCollectionGroup, false);
        return DataCollectionGroupManagementDtoConverter.convert(group, history, true);
    }

    @Override
    public DataCollectionGroupManagementDto publish(Long groupId, String userName) {
        checkUseDDE();
        checkUsePublish();
        final DataCollectionGroup group = getGroup(groupId);
        checkAccess(group);
        if(group.isPublished()) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_FORBIDDEN, "This group is already published. To change the information please call the republish");
            throw exception;
        }
        DataCollectionGroupHistory history = getHistory(group);
        LOG.info("user [{}] published dataCollectionGroup [{}]", userName, group);
        group.setPublished(true);
        dataCollectionGroupService.update(group);
        DataCollectionGroupDto sacDataCollectionGroup = DataCollectionGroupDtoConverter.convert(group, history);
        postToProductionCatalogue(sacDataCollectionGroup, true);
        return DataCollectionGroupManagementDtoConverter.convert(group, history, true);
    }

    private DataCollectionGroupHistory getHistory(DataCollectionGroup group) {
        DataCollectionGroupHistory history = null ;
        if (group.getHistoryId() != null){
            history = dataCollectionGroupHistoryDao.get(group.getHistoryId());
        }
        return history;
    }

    @Override
    public DataCollectionGroupManagementDto republish(Long groupId, String userName) {
        checkUseDDE();
        checkUsePublish();
        DataCollectionGroup group = getGroup(groupId);
        checkAccess(group);
        if(!group.isPublished()) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_FORBIDDEN, "This group isn't published yep. Please first publish the group");
            throw exception;
        }
        DataCollectionGroupHistory history = getHistory(group);
        if (!group.isPublished()) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, "You can't republish a dataCollectionGroup that isn't published");
            throw exception;
        }
        LOG.info("user [{}] republishes dataCollectionGroup [{}]", userName, group);

        DataCollectionGroupDto sacDataCollectionGroup = DataCollectionGroupDtoConverter.convert(group, history);

        postToProductionCatalogue(sacDataCollectionGroup, false);
        return DataCollectionGroupManagementDtoConverter.convert(group, history, true);
    }

    @Override
    public void delete(Long groupId) {
        checkUseDDE();
        DataCollectionGroup group = getGroup(groupId);
        checkAccess(group);
        if (group.isPublished()) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_FORBIDDEN, "Cannot delete a data collection group that is published.");
            throw exception;
        }
        if (!dataCollectionDefinitionService.getAll(groupId).isEmpty()) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_CONFLICT, "Cannot delete a data collection group because it is not empty. First delete the data collection definitions attached to this data collection group");
            throw exception;
        }
        group.setHistoryId(null);
        dataCollectionGroupService.update(group);

        DataCollectionGroupHistorySearch search = new DataCollectionGroupHistorySearch();
        search.setDataCollectionGroupId(groupId);
        List<DataCollectionGroupHistory> histories = dataCollectionGroupHistoryDao.getAll(search);
        for (DataCollectionGroupHistory history : histories) {
            dataCollectionGroupHistoryDao.delete(history);
        }
        dataCollectionGroupService.delete(group);
    }

    public void postToCatalogue(DataCollectionGroupDto groupDto, boolean create) {
        postToCatalogue(groupDto, create, ConfigurationKey.CATALOGUE_HOST, ConfigurationKey.CATALOGUE_USERNAME, ConfigurationKey.CATALOGUE_PASSWORD);
    }

    public void postToProductionCatalogue(DataCollectionGroupDto groupDto, boolean create) {
        postToCatalogue(groupDto, create, ConfigurationKey.CATALOGUE_PROD_HOST, ConfigurationKey.CATALOGUE_PROD_USERNAME, ConfigurationKey.CATALOGUE_PROD_PASSWORD);
    }

    public void postToCatalogue(DataCollectionGroupDto groupDto, boolean create, ConfigurationKey hostKey, ConfigurationKey usernameKey, ConfigurationKey passwordKey) {
        final Configuration catalogueHost = configurationService.get(hostKey);
        if (StringUtils.isBlank(catalogueHost.getValue())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "Configuration", hostKey);
            throw exception;
        }

        DataCollectionGroupUri dataCollectionGroupUri;
        if (create) {
            dataCollectionGroupUri = new DataCollectionGroupUri(catalogueHost.getValue());
        } else {
            dataCollectionGroupUri = new DataCollectionGroupUri(catalogueHost.getValue(), groupDto.getId());
        }

        String accessToken = getAccessToken(catalogueHost, usernameKey, passwordKey);

        WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.addHeader(HttpHeaders.CLIENT_VERSION, ClientVersion.DataCollectionDefinition.getDefault());
        wsb.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
        wsb.setUrl(dataCollectionGroupUri.toString());
        if (create) {
            wsb.setPost(true);
        } else {
            wsb.setPut(true);
        }
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.setType(MediaType.APPLICATION_JSON_TYPE);
        wsb.setJson(groupDto);
        wsb.setReturnType(new GenericType<ClientResponse>(){});

        ClientResponse response = (ClientResponse) webServiceClientService.callWebService(wsb);
        if (response.getStatus() < 200 || response.getStatus() >= 300) {
            HealthDataException exception = new HealthDataException();
            exception.setResponseStatus(Response.Status.INTERNAL_SERVER_ERROR);
            exception.setResponseMessage("Couldn't upload the group [" + groupDto +"]");
        }
    }


    private DataCollectionGroupManagementDto unmarshaller(JSONObject json) throws Exception {
        return DataCollectionGroupRequestMapper.convert(json);
    }

    private String getAccessToken(Configuration catalogueHost, ConfigurationKey username, ConfigurationKey password) {
        Configuration catalogueUserName = configurationService.get(username);
        Configuration cataloguePassword = configurationService.get(password);

        return TokenUtils.getAccessTokenCatalogue(catalogueHost.getValue(), catalogueUserName.getValue(), cataloguePassword.getValue());
    }

    private void checkAccess(DataCollectionGroup group) {
        checkAccess(group.getName());
    }

    private void checkAccess(String groupName) {
        if(!hasAccess(groupName)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_UNAUTHORIZED);
            throw exception;
        }
    }

    private boolean hasAccess(DataCollectionGroup group) {
        return hasAccess(group.getName());
    }

    private boolean hasAccess(String groupName) {
        HDUserDetails userDetails = AuthenticationService.getUserDetails();
        if(userService.isGuestAccount(userDetails.getUsername())) {
            return false;
        }
        if(userDetails.isAdmin()) {
            return true;
        }
        return userDataCollectionService.isUserAuthorized(userDetails, groupName);
    }

    private void checkUseDDE() {
        Configuration useDDE = configurationService.get(ConfigurationKey.USE_DATA_DEFINITION_EDITOR);
        if (!Boolean.valueOf(useDDE.getValue())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_UNAUTHORIZED);
            throw exception;
        }
    }

    private void checkUsePublish() {
        Configuration usePublish = configurationService.get(ConfigurationKey.USE_DATA_DEFINITION_PUBLISH);
        if (!Boolean.valueOf(usePublish.getValue())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_UNAUTHORIZED);
            throw exception;
        }
    }

}
