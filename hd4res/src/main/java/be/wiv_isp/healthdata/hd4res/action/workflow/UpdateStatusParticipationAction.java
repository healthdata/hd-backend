/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.action.workflow;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.hd4res.domain.ParticipationDocument;
import be.wiv_isp.healthdata.hd4res.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.search.ParticipationWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.domain.CSVContent;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.service.IAbstractParticipationDocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;

public class UpdateStatusParticipationAction extends AbstractParticipationWorkflowAction {

	private static final Logger LOG = LoggerFactory.getLogger(UpdateStatusParticipationAction.class);

	@Autowired
	private IAbstractParticipationDocumentService documentService;

	@Override
	public WorkflowActionType getAction() {
		return WorkflowActionType.UPDATE_STATUS;
	}

	@Override
	public ParticipationWorkflow doExecute(ParticipationWorkflow workflow, HDUserDetails userDetails) {
		if (workflow == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.ACTION_NOT_ALLOWED_ON_NEW_REGISTRATION, this.getAction());
			throw exception;
		}
		if(WorkflowStatus.DELETED.equals(endStatus)) {
			ParticipationDocument document = workflow.getDocument();
			document.setDwhStatus(null);
			documentService.update(document);
		}
		return workflow;
	}

	@Override
	public ParticipationWorkflow retrieveWorkflow() {
		final ParticipationWorkflowSearch search = new ParticipationWorkflowSearch();
		search.setHd4dpWorkflowId(hd4dpWorkflowId);

		search.setIdentificationValue(identificationWorkflow.getValue());

		LOG.debug(MessageFormat.format("Searching for workflow with search parameters [{0}]", search));
		final ParticipationWorkflow workflow = getWorkflowService().getUnique(search);

		if (workflow == null) {
			LOG.info(MessageFormat.format("No workflow found with search parameters [{0}]", search));
		}

		return workflow;
	}

	@Override
	public void extractInfo(Message message) {
		final CSVContent csvContent = new CSVContent(new String(message.getContent(), StandardCharsets.UTF_8));
		setHd4dpWorkflowId(csvContent.getWorkflowId());
	}


	@Override
	public WorkflowType getWorkflowType() {
		return WorkflowType.PARTICIPATION;
	}

	@Override
	public String toString() {
		return MessageFormat.format("{0} (end status: {1})", this.getAction(), this.getEndStatus());
	}
}
