/*
 *  Copyright 2014-2017 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package be.wiv_isp.healthdata.hd4res.dto.converters;

import be.wiv_isp.healthdata.common.dto.DataCollectionGroupDto;
import be.wiv_isp.healthdata.common.dto.PeriodDto;
import be.wiv_isp.healthdata.common.dto.TranslatableStringDto;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionGroup;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionGroupHistory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataCollectionGroupDtoConverter {

	private static final Logger LOG = LoggerFactory.getLogger(DataCollectionGroupDtoConverter.class);

	public static DataCollectionGroupDto convert(DataCollectionGroup dataCollectionGroup, DataCollectionGroupHistory history) {
		DataCollectionGroupDto dataCollectionGroupDto = null;

		if (dataCollectionGroup != null) {
			dataCollectionGroupDto = new DataCollectionGroupDto();

			dataCollectionGroupDto.setId(dataCollectionGroup.getId());
			dataCollectionGroupDto.setName(dataCollectionGroup.getName());
			TranslatableStringDto label = new TranslatableStringDto();
			label.setEn(dataCollectionGroup.getLabel().getEn());
			label.setFr(dataCollectionGroup.getLabel().getFr());
			label.setNl(dataCollectionGroup.getLabel().getNl());
			dataCollectionGroupDto.setLabel(label);
			TranslatableStringDto description = new TranslatableStringDto();
			description.setEn(dataCollectionGroup.getDescription().getEn());
			description.setFr(dataCollectionGroup.getDescription().getFr());
			description.setNl(dataCollectionGroup.getDescription().getNl());
			dataCollectionGroupDto.setDescription(description);
			dataCollectionGroupDto.setParticipationContent(history.getParticipationContent());
			dataCollectionGroupDto.setMajorVersion(dataCollectionGroup.getMajorVersion());
			dataCollectionGroupDto.setPublished(dataCollectionGroup.isPublished());

			dataCollectionGroupDto.setStartDate(dataCollectionGroup.getStartDate());
			dataCollectionGroupDto.setEndDateCreation(dataCollectionGroup.getEndDateCreation());
			dataCollectionGroupDto.setEndDateSubmission(dataCollectionGroup.getEndDateSubmission());
			dataCollectionGroupDto.setEndDateComments(dataCollectionGroup.getEndDateComments());

			if (dataCollectionGroup.getPeriod() != null) {
				dataCollectionGroupDto.setPeriod(new PeriodDto(dataCollectionGroup.getPeriod().getStart(), dataCollectionGroup.getPeriod().getEnd()));
			}

			dataCollectionGroupDto.setCreatedOn(dataCollectionGroup.getCreatedOn());
			dataCollectionGroupDto.setUpdatedOn(dataCollectionGroup.getUpdatedOn());
		}

		LOG.info("Converted dataCollectionGroupDto : " + dataCollectionGroupDto);

		return dataCollectionGroupDto;
	}
}
