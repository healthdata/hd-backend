/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain.search;

import java.util.Objects;

public class Hd4ResSsinSearch {

	private String registerName;
	private String eHealthCodedNissBatch;
	private String landingZoneCodedNiss;

	public String getRegisterName() {
		return registerName;
	}

	public void setRegisterName(String registerName) {
		this.registerName = registerName;
	}

	public String geteHealthCodedNissBatch() {
		return eHealthCodedNissBatch;
	}

	public void seteHealthCodedNissBatch(String eHealthCodedNissBatch) {
		this.eHealthCodedNissBatch = eHealthCodedNissBatch;
	}

	public String getLandingZoneCodedNiss() {
		return landingZoneCodedNiss;
	}

	public void setLandingZoneCodedNiss(String landingZoneCodedNiss) {
		this.landingZoneCodedNiss = landingZoneCodedNiss;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof Hd4ResSsinSearch) {
			Hd4ResSsinSearch other = (Hd4ResSsinSearch) o;

			return Objects.equals(registerName, other.registerName) //
					&& Objects.equals(eHealthCodedNissBatch, other.eHealthCodedNissBatch) //
					&& Objects.equals(landingZoneCodedNiss, other.landingZoneCodedNiss);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.registerName, //
				this.eHealthCodedNissBatch, //
				this.landingZoneCodedNiss);
	}

	@Override
	public String toString() {
		return "Hd4ResSsinSearch {" + //
				"registerName = " + Objects.toString(this.registerName) + ", " + //
				"eHealthCodedNissBatch = " + Objects.toString(this.eHealthCodedNissBatch) + ", " + //
				"landingZoneCodedNiss = " + Objects.toString(this.landingZoneCodedNiss) + "}";
	}
}
