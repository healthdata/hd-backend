/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.mapper;

import be.wiv_isp.healthdata.common.domain.Baseline;
import be.wiv_isp.healthdata.common.domain.BaselineType;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.domain.enumeration.TimeUnit;
import be.wiv_isp.healthdata.common.dto.FollowUpDefinitionDto;
import be.wiv_isp.healthdata.common.dto.TranslatableStringDto;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.hd4res.dto.DataCollectionDefinitionManagementDto;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class DataCollectionDefinitionRequestMapper {

	static public DataCollectionDefinitionManagementDto convert(JSONObject json) throws Exception {
		DataCollectionDefinitionManagementDto dto = new DataCollectionDefinitionManagementDto();
		for (@SuppressWarnings("unchecked")
		Iterator<String> iterator = json.keys(); iterator.hasNext();) {
			String key = iterator.next();
			if (!"null".equalsIgnoreCase(json.getString(key))) {
				if ("dataCollectionDefinitionId".equalsIgnoreCase(key)) {
					try {
						dto.setDataCollectionDefinitionId(Long.valueOf(json.getString(key)));
					} catch (NumberFormatException e) {
						HealthDataException exception = new HealthDataException(e);
						exception.setExceptionType(ExceptionType.INVALID_FORMAT, json.getString(key), "long");
						throw exception;
					}
				} else if ("dataCollectionDefinitionName".equalsIgnoreCase(key)) {
					dto.setDataCollectionDefinitionName(json.getString(key));
				} else if ("dataCollectionGroupId".equalsIgnoreCase(key)) {
					dto.setDataCollectionGroupId(Long.valueOf(json.getString(key)));
				} else if ("label".equalsIgnoreCase(key)) {
					final TranslatableStringDto label = DataCollectionGroupRequestMapper.getTranslatableStringDto(json, key);
					dto.setLabel(label);
				} else if ("description".equalsIgnoreCase(key)) {
					final TranslatableStringDto description = DataCollectionGroupRequestMapper.getTranslatableStringDto(json, key);
					dto.setDescription(description);
				} else if ("content".equalsIgnoreCase(key)) {
					final JSONObject contentJSON = json.getJSONObject(key);
                    dto.setContent(contentJSON.toString().getBytes(StandardCharsets.UTF_8));
				}  else if ("userDescription".equalsIgnoreCase(key)) {
					dto.setUserDescription(json.getString(key));
				}  else if ("minorVersion".equalsIgnoreCase(key)) {
					dto.setMinorVersion(json.getInt(key));
				} else if ("published".equalsIgnoreCase(key)) {
					dto.setPublished(json.getBoolean(key));
				} else if ("createdOn".equalsIgnoreCase(key)) {
					try {
						final Date asDate = DateUtils.parseDate(json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
						dto.setCreatedOn(new Timestamp(asDate.getTime()));
					} catch (ParseException e) {
						HealthDataException exception = new HealthDataException(e);
						exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
						throw exception;
					}
				}  else if ("updatedOn".equalsIgnoreCase(key)) {
					try {
						final Date asDate = DateUtils.parseDate(json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
						dto.setUpdatedOn(new Timestamp(asDate.getTime()));
					} catch (ParseException e) {
						HealthDataException exception = new HealthDataException(e);
						exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
						throw exception;
					}
				} else if ("followUpDefinitions".equalsIgnoreCase(key)) {
					final List<FollowUpDefinitionDto> followUpDefinitions = new ArrayList<>();
					final JSONArray followUpDefinitionsJsonArray = json.getJSONArray(key);
					for (int i = 0; i < followUpDefinitionsJsonArray.length(); i++) {
						followUpDefinitions.add(createFollowUpDefinition(followUpDefinitionsJsonArray.getJSONObject(i)));
					}
					dto.setFollowUpDefinitions(followUpDefinitions);
				}
			}
		}
		return dto;
	}


	static private FollowUpDefinitionDto createFollowUpDefinition(JSONObject followUpDefinitionJsonObject) throws JSONException {
		final FollowUpDefinitionDto def = new FollowUpDefinitionDto();
		def.setLabel(followUpDefinitionJsonObject.getString("label"));
		def.setName(followUpDefinitionJsonObject.getString("name"));
		def.setDescription(followUpDefinitionJsonObject.getString("description"));
		if(!followUpDefinitionJsonObject.isNull("id")) {
			def.setId(followUpDefinitionJsonObject.getLong("id"));
		}
		final String timingCondition = followUpDefinitionJsonObject.getString("timing");
		validateTimingCondition(timingCondition);
		def.setTiming(timingCondition);

		final JSONObject baselineJsonObject = followUpDefinitionJsonObject.getJSONObject("baseline");
		final String baselineType = baselineJsonObject.getString("type");
		final String baselineValue = baselineJsonObject.isNull("value") ? null : baselineJsonObject.getString("value");
		def.setBaseline(new Baseline(BaselineType.valueOf(baselineType), baselineValue));

		def.setConditions(new ArrayList<String>());
		final JSONArray conditionsArray = followUpDefinitionJsonObject.getJSONArray("conditions");
		for (int j = 0; j < conditionsArray.length(); j++) {
            def.getConditions().add(conditionsArray.getString(j));
        }
		return def;
	}

	static private void validateTimingCondition(String value) {
		if(value == null || value.length() < 2) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_TIMING_CONDITION, value, ArrayUtils.toString(TimeUnit.values()));
			throw exception;
		}

		try {
			TimeUnit.getByLabel(value.substring(value.length() - 1));
		} catch (HealthDataException e) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_TIMING_CONDITION, value, ArrayUtils.toString(TimeUnit.values()));
			throw exception;
		}

		final String amountAsString = value.substring(0, value.length() - 1);
		try {
			Integer.valueOf(amountAsString);
		} catch (NumberFormatException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.INVALID_TIMING_CONDITION, value, ArrayUtils.toString(TimeUnit.values()));
			throw exception;
		}
	}
}
