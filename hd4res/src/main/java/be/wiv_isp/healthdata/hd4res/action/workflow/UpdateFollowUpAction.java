/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.action.workflow;

import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.hd4res.service.IFollowUpService;
import be.wiv_isp.healthdata.orchestration.domain.CSVContent;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.MessageFormat;

public class UpdateFollowUpAction extends AbstractRegistrationWorkflowAction {

    private static final Logger LOG = LoggerFactory.getLogger(UpdateFollowUpAction.class);

    @Autowired
    private IFollowUpService followUpService;

    @Override
    public WorkflowActionType getAction() {
        return WorkflowActionType.UPDATE_FOLLOW_UP_ACTION;
    }

    @Override
    public WorkflowType getWorkflowType() {
        return WorkflowType.REGISTRATION;
    }

    @Override
    public RegistrationWorkflow doExecute(RegistrationWorkflow workflow, HDUserDetails userName) {
        followUpService.prepareFollowUpsBeforeSaving(workflow, followUps);
        return workflow;
    }

    @Override
    public RegistrationWorkflow retrieveWorkflow() {
        final String identificationValue = identificationWorkflow.getValue();
        final RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
        search.setHd4dpWorkflowId(hd4dpWorkflowId);
        search.setIdentificationValue(identificationValue);
        LOG.debug(MessageFormat.format("Searching for workflow with hd4dp_workflow_id [{0}] and identification_value [{1}]", hd4dpWorkflowId, identificationValue));
        final RegistrationWorkflow workflow = getWorkflowService().getUnique(search);

        if (workflow == null) {
            LOG.info(MessageFormat.format("No workflow found with hd4dp_workflow_id [{0}] and identification_value [{1}]", hd4dpWorkflowId, identificationValue));
        }

        return workflow;
    }

    @Override
    public void extractInfo(Message message) {
        final CSVContent csvContent = new CSVContent(message.getContent());
        setHd4dpWorkflowId(csvContent.getWorkflowId());
    }
}
