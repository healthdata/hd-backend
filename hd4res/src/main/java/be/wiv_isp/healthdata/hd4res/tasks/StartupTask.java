/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.hd4res.tasks;

import be.wiv_isp.healthdata.orchestration.tasks.AbstractStartupTask;
import be.wiv_isp.healthdata.orchestration.tasks.ConfigurationSetupTask;
import be.wiv_isp.healthdata.orchestration.tasks.HealthDataTask;
import be.wiv_isp.healthdata.orchestration.tasks.UserConfigurationSetupTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class StartupTask extends AbstractStartupTask {

    @Autowired
    private OrganizationSetupTask organizationSetupTask;

    @Autowired
    private ConfigurationSetupTask configurationSetupTask;

    @Autowired
    private UserConfigurationSetupTask userConfigurationSetupTask;

    @Override
    protected List<HealthDataTask> getTasks() {
        final List<HealthDataTask> tasks = new ArrayList<>();
        tasks.add(organizationSetupTask);
        tasks.add(configurationSetupTask);
        tasks.add(userConfigurationSetupTask);
        return tasks;
    }

}
