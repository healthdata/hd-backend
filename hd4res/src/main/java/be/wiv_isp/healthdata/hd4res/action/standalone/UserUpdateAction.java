/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.action.standalone;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.orchestration.dto.HealthDataIdentificationDto;
import be.wiv_isp.healthdata.hd4res.domain.Hd4dpAuthority;
import be.wiv_isp.healthdata.hd4res.domain.Hd4dpUser;
import be.wiv_isp.healthdata.hd4res.domain.search.Hd4dpUserSearch;
import be.wiv_isp.healthdata.hd4res.service.IHd4dpUserService;
import be.wiv_isp.healthdata.orchestration.action.standalone.AbstractUserUpdateAction;
import be.wiv_isp.healthdata.orchestration.action.dto.UserUpdateHd4resActionDto;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.HashSet;
import java.util.Set;

public class UserUpdateAction extends AbstractUserUpdateAction {

    private static final Logger LOG = LoggerFactory.getLogger(UserUpdateAction.class);

    @Autowired
    private IHd4dpUserService hd4dpUserService;

    @Autowired
    @Qualifier("transactionManager")
    protected PlatformTransactionManager txManager;

    @Override
    public void execute() {
        TransactionTemplate tmpl = new TransactionTemplate(txManager);
        tmpl.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                doExecute();
            }
        });
    }

    private void doExecute() {
        for (UserUpdateHd4resActionDto.UserDto userDto : userUpdateDto.getUsers()) {
            final Hd4dpUserSearch search = new Hd4dpUserSearch();
            search.setHd4dpId(userDto.getHd4dpId());
            search.setHealthDataIdentificationType(userDto.getHealthDataIdentification().getType());
            search.setHealthDataIdentificationValue(userDto.getHealthDataIdentification().getValue());

            Hd4dpUser user = hd4dpUserService.getUnique(search);

            if(user == null) {
                if(!userDto.isDeleted()) {
                    user = fillUserInfo(new Hd4dpUser(), userDto);
                    LOG.debug("New user created on HD4DP: {}", user);
                    hd4dpUserService.create(user);
                }
            } else {
                if(userDto.isDeleted()) {
                    LOG.debug("User deleted on HD4DP: {}", user);
                    hd4dpUserService.delete(user);
                } else {
                    user = fillUserInfo(user, userDto);
                    LOG.debug("User updated on HD4DP: {}", user);
                    hd4dpUserService.update(user);
                }
            }
        }
    }

    private Hd4dpUser fillUserInfo(Hd4dpUser user, UserUpdateHd4resActionDto.UserDto userDto) {
        user.setHd4dpId(userDto.getHd4dpId());
        user.setUsername(userDto.getUsername());
        user.setLastName(userDto.getLastName());
        user.setFirstName(userDto.getFirstName());
        user.setEmail(userDto.getEmail());
        user.setEnabled(userDto.isEnabled());
        user.setLdapUser(userDto.isLdapUser());
        user.setDataCollectionNames(userDto.getDataCollectionNames());
        final Set<Hd4dpAuthority> hd4dpAuthorities = new HashSet<>();
        for (Authority authority : userDto.getAuthorities()) {
            hd4dpAuthorities.add(createHd4dpAuthority(authority));
        }
        user.setAuthorities(hd4dpAuthorities);
        user.setHealthDataIdentification(createHealthDataIdentification(userDto.getHealthDataIdentification()));
        return user;
    }

    private HealthDataIdentification createHealthDataIdentification(HealthDataIdentificationDto dto) {
        final HealthDataIdentification healthDataIdentification = new HealthDataIdentification();
        healthDataIdentification.setType(dto.getType());
        healthDataIdentification.setValue(dto.getValue());
        healthDataIdentification.setName(dto.getName());
        healthDataIdentification.setHd4prc(dto.isHd4prc());
        return healthDataIdentification;
    }

    private Hd4dpAuthority createHd4dpAuthority(Authority authority) {
        final Hd4dpAuthority hd4dpAuthority = new Hd4dpAuthority();
        hd4dpAuthority.setAuthority(authority.getAuthority());
        return hd4dpAuthority;
    }

}
