/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.api;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationDocument;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.hd4res.service.IAcceptedWorkflowService;
import be.wiv_isp.healthdata.orchestration.mapping.IMappingContextFactory;
import be.wiv_isp.healthdata.orchestration.service.IAbstractRegistrationWorkflowService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionDefinitionForwardService;
import be.wiv_isp.healthdata.orchestration.service.IMappingService;
import be.wiv_isp.healthdata.orchestration.service.IAbstractRegistrationDocumentService;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

@Component
@Path("/csvgenerator")
public class DocumentCsvGeneratorRestService {

    private static final Logger LOG = LoggerFactory.getLogger(DocumentCsvGeneratorRestService.class);

    @Autowired
    private IAbstractRegistrationDocumentService documentService;
    @Autowired
    private IMappingService mappingService;
    @Autowired
    private IAbstractRegistrationWorkflowService<RegistrationWorkflow, RegistrationWorkflowSearch> workflowService;
    @Autowired
    private IAcceptedWorkflowService dataWarehouseService;
    @Autowired
    private IMappingContextFactory mappingContextFactory;
    @Autowired
    private IDataCollectionDefinitionForwardService catalogueService;

    @POST
    @Path("/generate")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Produces(MediaType.APPLICATION_JSON)
    public Response generate(final String workflowIdsAsString) throws JSONException {
        final JSONArray workflowIds = new JSONArray(workflowIdsAsString);

        final Map<Long, String> results = new HashMap();
        for(int i=0; i<workflowIds.length(); ++i) {
            final Long workflowId = workflowIds.getLong(i);
            try {
                String result = generateCsv(workflowId);
                results.put(workflowId, result);
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
                results.put(workflowId, e.getMessage());
            }
        }

        return Response.ok(results).build();
    }

    private String generateCsv(Long workflowId) {
        LOG.info(MessageFormat.format("Generating CSV for workflow with id {0}", workflowId.toString()));

        final RegistrationWorkflow workflow = workflowService.get(workflowId);
        final RegistrationDocument document = workflow.getDocument();
        final DataCollectionDefinitionDtoV7 dcd = catalogueService.get(workflow.getDataCollectionDefinitionId());
        String csv = mappingService.mapFromJsonToCsv(workflow, mappingContextFactory.create(workflow), dcd.getContent(), false);
        document.setCsv(csv.getBytes(StandardCharsets.UTF_8));
        documentService.update(document);

        if ("L".equalsIgnoreCase(document.getDwhStatus())) {
            LOG.debug("Not sending document to datawarehouse as DWH Status is equal to L");
            return "OK (but DWH status not updated as it was equal to L)";
        } else {
            dataWarehouseService.send(document);
            return "OK";
        }
    }
}
