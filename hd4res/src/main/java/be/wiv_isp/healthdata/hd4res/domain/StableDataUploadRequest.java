/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;

public class StableDataUploadRequest {

    public enum PatientIdFormat {
        BATCH, MANUAL
    }

    private String dataCollectionName;
    private String base64Csv;
    private HealthDataIdentification healthDataIdentification;
    private PatientIdFormat patientIdFormat;

    public String getBase64Csv() {
        return base64Csv;
    }

    public void setBase64Csv(String base64Csv) {
        this.base64Csv = base64Csv;
    }

    public HealthDataIdentification getHealthDataIdentification() {
        return healthDataIdentification;
    }

    public void setHealthDataIdentification(HealthDataIdentification healthDataIdentification) {
        this.healthDataIdentification = healthDataIdentification;
    }

    public String getDataCollectionName() {
        return dataCollectionName;
    }

    public void setDataCollectionName(String dataCollectionName) {
        this.dataCollectionName = dataCollectionName;
    }

    public PatientIdFormat getPatientIdFormat() {
        return patientIdFormat;
    }

    public void setPatientIdFormat(PatientIdFormat patientIdFormat) {
        this.patientIdFormat = patientIdFormat;
    }

    @Override
    public String toString() {
        return new StringBuilder("[ ")
                .append("dataCollectionName = ").append(dataCollectionName).append(", ")
                .append("healthDataIdentification = ").append(healthDataIdentification).append(", ")
                .append("patientIdFormat = ").append(patientIdFormat)
                .append(" ]").toString();
    }
}
