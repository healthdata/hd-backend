/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.factory.impl;

import be.wiv_isp.healthdata.hd4res.action.standalone.StableDataUploadAction;
import be.wiv_isp.healthdata.hd4res.action.workflow.UpdateStatusRegistrationAction;
import be.wiv_isp.healthdata.hd4res.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.hd4res.factory.IMessageFactory;
import be.wiv_isp.healthdata.orchestration.action.Action;
import be.wiv_isp.healthdata.orchestration.action.standalone.StandAloneAction;
import be.wiv_isp.healthdata.orchestration.action.workflow.WorkflowAction;
import be.wiv_isp.healthdata.orchestration.util.constant.EHealthCodageConstants;
import be.wiv_isp.healthdata.orchestration.domain.AbstractWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.CSVContent;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.StandAloneActionType;
import be.wiv_isp.healthdata.orchestration.dto.EncryptionModuleMessage;
import be.wiv_isp.healthdata.orchestration.factory.impl.AbstractMessageFactory;
import be.wiv_isp.healthdata.orchestration.jaxb.json.JsonMarshaller;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

@Component
public class MessageFactory extends AbstractMessageFactory<RegistrationWorkflow, ParticipationWorkflow> implements IMessageFactory {

	private static final Logger LOG = LoggerFactory.getLogger(MessageFactory.class);

	@Autowired
	private IMessageService messageService;
	@Autowired
	private IOrganizationService organizationService;

	@Override
	public Message createOutgoingMessage(WorkflowAction action, RegistrationWorkflow workflow) {
		return createOutgoingMessage(action, workflow, MessageType.WORKFLOW_ACTION);
	}

	@Override
	public Message createOutgoingMessage(WorkflowAction action, ParticipationWorkflow workflow) {
		return createOutgoingMessage(action, workflow, MessageType.PARTICIPATION_ACTION);
	}

	public Message createOutgoingMessage(WorkflowAction action, AbstractWorkflow workflow, MessageType type) {
		LOG.debug("{}[{}]: creating outbox message for action {}", action.getWorkflowType().getLabel(), workflow.getId(), action);
		final Message replyMessage = messageService.getLast(workflow.getId(), type);
		CSVContent csvContent = createCsvContent(action, replyMessage);
		return createMessage(csvContent, replyMessage, type, workflow.getId());
	}

	@Override
	public Message createOutgoingMessage(StandAloneAction action, Message replyMessage) {
		LOG.debug(MessageFormat.format("Creating outbox message for action {0}", action.getAction()));
		CSVContent csvContent;
		if(action instanceof StableDataUploadAction) {
			csvContent = createStableDataCsvContent((StableDataUploadAction)action, replyMessage);
		} else {
			csvContent = createCsvContent(action, replyMessage);
		}
		if (StandAloneActionType.STATUS_MESSAGE_ACTION.equals(action.getAction()))
			return createMessage(csvContent, replyMessage, MessageType.ACTION, null, 1);
		else
			return createMessage(csvContent, replyMessage, MessageType.ACTION, null);
	}

	@Override
	public Message createOutgoingMessage(UpdateStatusRegistrationAction action, RegistrationWorkflow workflow, Message replyMessage) {
		LOG.debug("Registration[{}]: creating outbox message for action {}, with given reply message", workflow.getId(), action);
		CSVContent csvContent = createCsvContent(action, replyMessage);
		return createMessage(csvContent, replyMessage, MessageType.WORKFLOW_ACTION, workflow.getId());
	}

	private CSVContent createStableDataCsvContent(StableDataUploadAction action, Message replyMessage) {
		final CSVContent csvContent = new CSVContent(replyMessage.getContent());
		csvContent.putPatientId(action.getPatientId());
		if (csvContent.getSize() == 4) { // HD4DP sent a message with 3 fields, so we don't know if it can receive multi-field messages
			if (csvContent.isDummyField(1))
				csvContent.removeField(1);
			else if (csvContent.isDummyField(0))
				csvContent.removeField(0);
		}
		//csvContent.putWorkflowId(stableDataUploadAction.getPatientId());
		if (csvContent.getSize() < 3) // TODO: this should never occur
			csvContent.addDummyField();
		csvContent.setLastField(JsonMarshaller.marshalToByteArray(action));
		return csvContent;
	}

	private CSVContent createCsvContent(Action action, Message replyMessage) {
		final CSVContent csvContent = new CSVContent(replyMessage.getContent());
		csvContent.setLastField(JsonMarshaller.marshalToByteArray(action));
		return csvContent;
	}

	@Override
	protected EncryptionModuleMessage createEncryptionModuleMessage(Message message, CSVContent csvContent) {
		final EncryptionModuleMessage emMessage = new EncryptionModuleMessage();
		emMessage.setSubject(EHealthCodageConstants.DECODE_SUBJECT);
		emMessage.setAddressee(identificationService.getEHealthCodageIdentification());
		emMessage.setEncryptionAddressee(createIdentification(message.getCorrespondent()));
		emMessage.setContent(csvContent.toString());
		emMessage.addMetadata(Message.Metadata.FILE_NAME, message.getMetadata().get(EHealthCodageConstants.MetaData.FILE_NAME));
		emMessage.addMetadata(Message.Metadata.TTP_PROJECT, message.getMetadata().get(EHealthCodageConstants.MetaData.TTP_PROJECT));
		return emMessage;
	}


	@Override
	public Message createMessage(CSVContent csvContent, Message replyMessage, MessageType messageType, Long workflowId) {
		return createMessage(csvContent, replyMessage, messageType, workflowId, MAX_TRIALS);
	}

	private Message createMessage(CSVContent csvContent, Message replyMessage, MessageType messageType, Long workflowId, Integer sendingTrials) {
		final Message message = new Message();
		message.setStatus(MessageStatus.OUTBOX);
		message.setCorrespondent(replyMessage.getCorrespondent());
		message.setContent(csvContent.getBytes());
		message.addMetadata(Message.Metadata.FILE_NAME, replyMessage.getMetadata().get(EHealthCodageConstants.MetaData.FILE_NAME));
		message.addMetadata(Message.Metadata.TTP_PROJECT, replyMessage.getMetadata().get(EHealthCodageConstants.MetaData.TTP_PROJECT));
		message.setType(messageType);
		message.setOrganization(organizationService.getMain());
		message.setWorkflowId(workflowId);
		message.setRemainingSendingTrials(sendingTrials);

		LOG.debug(MessageFormat.format("Message created: {0}", message));
		return message;
	}
}
