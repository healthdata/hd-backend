/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain;

import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationDocument;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "DOCUMENTS")
public class RegistrationDocument extends AbstractRegistrationDocument<RegistrationWorkflow> {

	@Lob
	@Column(name = "CSV")
	private byte[] csv;

	@Column(name = "DWH_STATUS")
	private String dwhStatus;

	@Column(name = "HD4DP_VERSION")
	private Long hd4dpVersion;

	@JsonIgnore
	public byte[] getCsv() {
		return csv;
	}

	public void setCsv(byte[] csv) {
		this.csv = csv;
	}

	@JsonIgnore
	public String getDwhStatus() {
		return dwhStatus;
	}

	public void setDwhStatus(String dwhStatus) {
		this.dwhStatus = dwhStatus;
	}

	public Long getHd4dpVersion() {
		return hd4dpVersion;
	}

	public void setHd4dpVersion(Long hd4dpVersion) {
		this.hd4dpVersion = hd4dpVersion;
	}

	@Override
	public void setDocumentData(DocumentData documentData) {
		setDocumentContent(documentData.getContent());
		setCodedContent(documentData.getCoded());
	}
}
