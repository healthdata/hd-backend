/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.encrypt.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "LINK_NISS", schema = "ENCRYPT")
public class Hd4ResSsin {

	public static final String CODED_NO_PATIENT_ID = "rHLg9EJ21hWWJF6oI/Qovkp5B88jilpRrw8ECigsIbg=";

	@Id
	@Column(name = "LZ_COD_NISS")
	private String landingZoneCodedNiss;

	@Column(name = "TX_COD_NISS_HD_EHMAN")
	private String eHealthCodedNissManual;

	@Column(name = "TX_COD_NISS_HD_EHBATCH")
	private String eHealthCodedNissBatch;

	@Column(name = "TX_REG_NAM")
	private String registerName;

	@Column(name = "TX_PAT_IDN_OLD")
	private String oldPatientIds;

	@Column(name = "CD_STATUS_RECODE")
	private String recodeStatus;

	public String getLandingZoneCodedNiss() {
		return landingZoneCodedNiss;
	}

	public void setLandingZoneCodedNiss(String landingZoneCodedNiss) {
		this.landingZoneCodedNiss = landingZoneCodedNiss;
	}

	public String geteHealthCodedNissManual() {
		return eHealthCodedNissManual;
	}

	public void seteHealthCodedNissManual(String eHealthCodedNissManual) {
		this.eHealthCodedNissManual = eHealthCodedNissManual;
	}

	public String geteHealthCodedNissBatch() {
		return eHealthCodedNissBatch;
	}

	public void seteHealthCodedNissBatch(String eHealthCodedNissBatch) {
		this.eHealthCodedNissBatch = eHealthCodedNissBatch;
	}

	public String getRegisterName() {
		return registerName;
	}

	public void setRegisterName(String registerName) {
		this.registerName = registerName;
	}

	public String getOldPatientIds() {
		return oldPatientIds;
	}

	public void setOldPatientIds(String oldPatientIds) {
		this.oldPatientIds = oldPatientIds;
	}

	public String getRecodeStatus() {
		return recodeStatus;
	}

	public void setRecodeStatus(String recodeStatus) {
		this.recodeStatus = recodeStatus;
	}

	@Override
	public String toString() {
		return "Hd4ResSsin {" + //
				"landingZoneCodedNiss = " + Objects.toString(this.landingZoneCodedNiss) + ", " + //
				"eHealthCodedNissManual = " + Objects.toString(this.eHealthCodedNissManual) + ", " + //
				"eHealthCodedNissBatch = " + Objects.toString(this.eHealthCodedNissBatch) + ", " + //
				"registerName = " + this.registerName + ", " + //
				"oldPatientIds = " + Objects.toString(this.oldPatientIds) + ", " + //
				"recodeStatus = " + Objects.toString(this.recodeStatus) + "}";
	}
}
