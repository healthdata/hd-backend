/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.action.workflow;

import be.wiv_isp.healthdata.hd4res.domain.RegistrationDocument;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.hd4res.service.IAcceptedWorkflowService;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import org.springframework.beans.factory.annotation.Autowired;

public class ResubmitRegistrationAction extends AbstractRegistrationWorkflowAction {

	@Autowired
	private IAcceptedWorkflowService dataWarehouseService;

	@Override
	public WorkflowActionType getAction() {
		return WorkflowActionType.RESUBMIT;
	}

	@Override
	public RegistrationWorkflow doExecute(RegistrationWorkflow workflow, HDUserDetails userDetails) {
		RegistrationDocument document = workflow.getDocument();
		if ("L".equalsIgnoreCase(document.getDwhStatus())) {
			setEndStatus(WorkflowStatus.LOADED_IN_STG);
		} else {
			dataWarehouseService.send(document);
			setEndStatus(WorkflowStatus.NEW);
		}
		return workflow;
	}
}
