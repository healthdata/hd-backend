/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain;

import be.wiv_isp.healthdata.common.util.StringUtils;

public class StableDataCountResult {

    private String identificationValue;
    private String dataCollectionName;
    private StableData.Status status;
    private Long count;

    public StableDataCountResult(String identificationValue, String dataCollectionName, StableData.Status status, Long count) {
        this.identificationValue = identificationValue;
        this.dataCollectionName = dataCollectionName;
        this.status = status;
        this.count = count;
    }

    public String getIdentificationValue() {
        return identificationValue;
    }

    public void setIdentificationValue(String identificationValue) {
        this.identificationValue = identificationValue;
    }

    public String getDataCollectionName() {
        return dataCollectionName;
    }

    public void setDataCollectionName(String dataCollectionName) {
        this.dataCollectionName = dataCollectionName;
    }

    public StableData.Status getStatus() {
        return status;
    }

    public void setStatus(StableData.Status status) {
        this.status = status;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return StringUtils.toString(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof StableDataCountResult))
            return false;

        StableDataCountResult result = (StableDataCountResult) o;

        if (!identificationValue.equals(result.identificationValue))
            return false;
        if (!dataCollectionName.equals(result.dataCollectionName))
            return false;
        if (status != result.status)
            return false;
        return count.equals(result.count);

    }

    @Override
    public int hashCode() {
        int result = identificationValue.hashCode();
        result = 31 * result + dataCollectionName.hashCode();
        result = 31 * result + status.hashCode();
        result = 31 * result + count.hashCode();
        return result;
    }
}
