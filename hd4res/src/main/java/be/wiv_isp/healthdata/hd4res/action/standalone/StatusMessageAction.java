/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.action.standalone;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.hd4res.domain.StatusMessage;
import be.wiv_isp.healthdata.orchestration.action.standalone.AbstractStatusMessageAction;
import be.wiv_isp.healthdata.orchestration.domain.validator.IStatusMessageValidator;
import be.wiv_isp.healthdata.orchestration.service.IStatusMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StatusMessageAction extends AbstractStatusMessageAction {

	private static final Logger LOG = LoggerFactory.getLogger(StatusMessageAction.class);

	@Autowired
	private IStatusMessageService<StatusMessage> statusMessageService;
	@Autowired
	private IStatusMessageValidator statusMessageValidator;

	@Override
	public void execute() {
		if(content == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_STATUS_MESSAGE_CONTENT, "content is null");
			throw exception;
		}

		LOG.info("Status message received from organization {}", healthDataIdentification);

		final StatusMessage statusMessage = new StatusMessage();
		statusMessage.setContent(content);
		statusMessage.setHealthDataIdentification(healthDataIdentification);
		statusMessage.setSentOn(message.getSentOn());
		statusMessage.setMessage(message);

		if(isNull(healthDataIdentification.getType())) {
			LOG.warn("Identification type is equal to [{}]. Not saving the status message", healthDataIdentification.getType());
			return;
		}

		if(isNull(healthDataIdentification.getValue())) {
			LOG.warn("Identification value is equal to [{}]. Not saving the status message", healthDataIdentification.getValue());
			return;
		}

		statusMessageValidator.validate(statusMessage);
		statusMessageService.create(statusMessage);
	}

	private boolean isNull(String value) {
		return value == null || "null".equals(value);
	}

}
