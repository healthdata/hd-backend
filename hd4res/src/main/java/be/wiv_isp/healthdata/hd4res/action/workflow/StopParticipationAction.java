/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.action.workflow;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.hd4res.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.search.ParticipationWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import org.springframework.stereotype.Component;

public class StopParticipationAction extends AbstractParticipationWorkflowAction {

	@Override
	public WorkflowActionType getAction() {
		return WorkflowActionType.STOP;
	}

	@Override
	public ParticipationWorkflow doExecute(ParticipationWorkflow workflow, HDUserDetails userDetails) {
		workflow.setCompleted(true);
		workflow.setCompletedOn(completedOn);
		workflow.setIdentificationName(identificationWorkflow.getName());
		setEndStatus(workflow.getStatus());
		return workflow;
	}

	@Override
	public ParticipationWorkflow retrieveWorkflow() {
		ParticipationWorkflowSearch search = new ParticipationWorkflowSearch();
		search.setDataCollectionDefinitionId(dataCollectionDefinitionId);
		search.setIdentificationType(getIdentificationWorkflow().getType());
		search.setIdentificationValue(getIdentificationWorkflow().getValue());
		search.setWithDocument(null);
		final ParticipationWorkflow workflow = getWorkflowService().getUnique(search);

		if (workflow == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "WORKFLOW", workflowId);
			throw exception;
		}

		return workflow;
	}
}
