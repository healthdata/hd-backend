/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.hd4res.action.standalone.StableDataUploadAction;
import be.wiv_isp.healthdata.hd4res.dao.IStableDataDao;
import be.wiv_isp.healthdata.hd4res.domain.*;
import be.wiv_isp.healthdata.hd4res.domain.search.StableDataSearch;
import be.wiv_isp.healthdata.hd4res.factory.IMessageFactory;
import be.wiv_isp.healthdata.hd4res.service.IStableDataService;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.search.StatusMessageSearch;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import be.wiv_isp.healthdata.orchestration.service.IStatusMessageService;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.csv.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@Service
public class StableDataService implements IStableDataService {

    private static final Logger LOG = LoggerFactory.getLogger(StableDataService.class);

    public static final String PATIENT_ID_KEY = "patient_id";

    @Autowired
    private IStableDataDao dao;
    @Autowired
    private IMessageService messageService;
    @Autowired
    private IMessageFactory messageFactory;
    @Autowired
    private IStatusMessageService<StatusMessage> statusMessageService;

    @Override
    public StableData get(Long stableDataId) {
        return dao.get(stableDataId);
    }

    @Override
    @Transactional
    public StableData create(StableData stableData) {
        return dao.create(stableData);
    }

    @Override
    @Transactional
    public StableData update(StableData stableData) {
        return dao.update(stableData);
    }

    @Override
    @Transactional
    public List<StableData> execute(StableDataUploadRequest request) {
        LOG.info("Executing stable data upload request {}", request);

        try {
            final byte[] csv = Base64.decodeBase64(request.getBase64Csv());
            final Reader reader = new InputStreamReader(new ByteArrayInputStream(csv));
            final CSVFormat csvFormat = CSVFormat.DEFAULT.withDelimiter(';').withEscape('\\').withQuoteMode(QuoteMode.MINIMAL).withHeader();
            final CSVParser parser = new CSVParser(reader, csvFormat);

            final List<String> keys = new ArrayList<>(parser.getHeaderMap().keySet());
            int patientIdIndex = keys.indexOf(PATIENT_ID_KEY); // index of patient_id column must be saved before removing the key
            keys.remove(PATIENT_ID_KEY); // remove patient_id header
            final String[] keysAsArray = keys.toArray(new String[keys.size()]);

            final List<StableData> uploadedStableData = new ArrayList<>();
            for (CSVRecord record : parser) {
                final StringBuilder sb = new StringBuilder();

                final CSVPrinter printer = csvFormat.withHeader(keysAsArray).print(sb);
                final CustomCSVRecord customCSVRecord = new CustomCSVRecord(record);
                customCSVRecord.remove(patientIdIndex); // remove patient_id value
                printer.printRecord(customCSVRecord);
                printer.close();

                final StableData stableData = new StableData();
                stableData.setStatus(StableData.Status.NEW);
                stableData.setDataCollectionName(request.getDataCollectionName());
                stableData.setIdentificationType(request.getHealthDataIdentification().getType());
                stableData.setIdentificationValue(request.getHealthDataIdentification().getValue());
                stableData.setPatientId(convertPatientId(record.get("patient_id"), request.getPatientIdFormat()));
                stableData.setCsv(sb.toString().getBytes(StandardCharsets.UTF_8)); // TODO-stable-data (correct encoding?)
                uploadedStableData.add(this.create(stableData));
            }

            reader.close();
            parser.close();

            LOG.debug("{} stable data records created", uploadedStableData.size());
            return uploadedStableData;
        } catch (Exception e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.ERROR_WHILE_UPLOADING_STABLE_DATA, e.getMessage());
            throw exception;
        }
    }

    private String convertPatientId(String patientId, StableDataUploadRequest.PatientIdFormat format) {
        if(StableDataUploadRequest.PatientIdFormat.BATCH.equals(format)) {
            return patientId;
        }

        final byte[] base64ByteArray = patientId.getBytes(StandardCharsets.UTF_8);
        final byte[] base64DecodedByteArray = Base64.decodeBase64(base64ByteArray);
        final byte[] cutData = Arrays.copyOfRange(base64DecodedByteArray, 3, 27);
        return new String(cutData);
    }

    @Override
    public List<StableData> getAll(StableDataSearch search) {
        return dao.getAll(search);
    }

    @Override
    @Transactional
    public void send(StableData stableData) {
        final StableDataUploadAction action = new StableDataUploadAction();
        action.setStableDataId(stableData.getId());
        action.setHealthDataIdentification(stableData.getHealthDataIdentification());
        action.setDataCollectionName(stableData.getDataCollectionName());
        action.setPatientId(stableData.getPatientId());
        action.setCsv(stableData.getCsv());

        final StatusMessageSearch search = new StatusMessageSearch();
        search.setHealthDataIdentification(stableData.getHealthDataIdentification());
        search.setLastOnly(true);
        final StatusMessage statusMessage = statusMessageService.getUnique(search);

        final Message message = messageFactory.createOutgoingMessage(action, statusMessage.getMessage());
        messageService.create(message);

        stableData.setStatus(StableData.Status.SENT);
        update(stableData);
    }

    @Override
    public List<StableDataCountResult> getCounts(StableDataSearch search) {
        return dao.getCounts(search);
    }

    @Override
    public StableDataCountsResult convert(List<StableDataCountResult> data) {
        final StableDataCountsResult countsResult = new StableDataCountsResult();
        for (StableDataCountResult detail : data) {
            final String identificationValue = detail.getIdentificationValue();
            final String dataCollectionName = detail.getDataCollectionName();
            final StableData.Status status = detail.getStatus();
            final Long count = detail.getCount();

            countsResult.setCount(identificationValue, dataCollectionName, status, count);
        }

        return countsResult;
    }

    public class CustomCSVRecord implements Iterable<String> {

        private List<String> values;

        public CustomCSVRecord(CSVRecord csvRecord) {
            values = new ArrayList<>();
            for (String value : csvRecord) {
                values.add(value);
            }
        }

        @Override
        public Iterator<String> iterator() {
            return values.iterator();
        }

        public void remove(int index) {
            if(values == null) {
                return;
            }
            try {
                values.remove(index);
            } catch (IndexOutOfBoundsException e) {
                // nothing
            }
        }
    }
}
