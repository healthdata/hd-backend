/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.hd4res.domain.RegistrationDocument;
import be.wiv_isp.healthdata.hd4res.service.IDataWarehouseSendingStrategy;
import be.wiv_isp.healthdata.orchestration.dao.IDocumentDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;

@Service
public class DatabaseDataWarehouseSendingRegistrationStrategy implements IDataWarehouseSendingStrategy<RegistrationDocument> {

	private static final Logger LOG = LoggerFactory.getLogger(DatabaseDataWarehouseSendingRegistrationStrategy.class);
	
	@Autowired
	private IDocumentDao<RegistrationDocument, ?> documentDao;
	
	@Override
	@Transactional
	public void send(RegistrationDocument document) {
		LOG.debug(MessageFormat.format("Updating document [{0}], set status to [null]", document.getId().toString()));
		
		document = documentDao.get(document.getId());
		document.setDwhStatus(null);
		documentDao.update(document);
	}

}
