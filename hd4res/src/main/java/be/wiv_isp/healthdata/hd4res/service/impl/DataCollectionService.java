/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.common.caching.CacheManagementService;
import be.wiv_isp.healthdata.common.dto.SalesForceOrganizationDto;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.hd4res.service.IDataCollectionService;
import be.wiv_isp.healthdata.orchestration.api.uri.DataCollectionUri;
import be.wiv_isp.healthdata.orchestration.api.uri.ParticipationUri;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IWebServiceClientService;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractDataCollectionService;
import com.sun.jersey.api.client.GenericType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class DataCollectionService extends AbstractDataCollectionService implements IDataCollectionService {

	@Autowired
	private IWebServiceClientService webServiceClientService;

	@Autowired
	private IConfigurationService configurationService;

	@SuppressWarnings("unchecked")
	@Override
	@Cacheable(value = CacheManagementService.dataCollectionCache)
	public Map<String, Set<String>> getDataCollections(Organization organization) {
		Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

		final DataCollectionUri dataCollectionUri = new DataCollectionUri();
		dataCollectionUri.setHost(catalogueHost.getValue());

		WebServiceBuilder wsb = new WebServiceBuilder();
		wsb.addHeader(HttpHeaders.CLIENT_VERSION, getClientVersion());
		wsb.setUrl(dataCollectionUri.toString());
		wsb.setGet(true);
		wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb.setReturnType(new GenericType<Map<String,Set<String>>>() {
		});

		return (Map<String, Set<String>>) webServiceClientService.callWebService(wsb);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Cacheable(value = CacheManagementService.dataCollectionCache)
	public boolean accept(String registerName, String identificationType, String identificationValue) {
		Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

		final DataCollectionUri dataCollectionUri = new DataCollectionUri();
		dataCollectionUri.setHost(catalogueHost.getValue());

		WebServiceBuilder wsb = new WebServiceBuilder();
		wsb.addHeader(HttpHeaders.CLIENT_VERSION, getClientVersion());
		wsb.setUrl(dataCollectionUri.toString());
		wsb.setGet(true);
		wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb.addParameter(RestUtils.ParameterName.IDENTIFICATION_TYPE, identificationType);
		wsb.addParameter(RestUtils.ParameterName.IDENTIFICATION_VALUE, identificationValue);
		wsb.setReturnType(new GenericType<Map<String,Set<String>>>() {
		});

		final Map<String,Set<String>> dataCollections = (Map<String,Set<String>>) webServiceClientService.callWebService(wsb);
		if (dataCollections == null) {
			return false;
		} else {
			return dataCollections.keySet().contains(getGroup(registerName));
		}
	}

	@Override
	public List<SalesForceOrganizationDto> getAuthorizedOrganizations(String dataCollectionName) {
		final Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

		final ParticipationUri participationUri = new ParticipationUri();
		participationUri.setHost(catalogueHost.getValue());
		participationUri.setDataCollectionName(dataCollectionName);

		final WebServiceBuilder wsb = new WebServiceBuilder();
		wsb.setUrl(participationUri.toString());
		wsb.setGet(true);
		wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb.setReturnType(new GenericType<List<SalesForceOrganizationDto>>() {});

		return (List<SalesForceOrganizationDto>) webServiceClientService.callWebService(wsb);
	}

	@Override
	public List<SalesForceOrganizationDto> getAllOrganizations() {
		final Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

		final ParticipationUri participationUri = new ParticipationUri();
		participationUri.setHost(catalogueHost.getValue());

		final WebServiceBuilder wsb = new WebServiceBuilder();
		wsb.setUrl(participationUri.toString());
		wsb.setGet(true);
		wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb.setReturnType(new GenericType<List<SalesForceOrganizationDto>>() {});

		return (List<SalesForceOrganizationDto>) webServiceClientService.callWebService(wsb);
	}

}
