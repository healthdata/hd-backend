/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import be.wiv_isp.healthdata.common.dao.impl.CrudDao;
import be.wiv_isp.healthdata.hd4res.dao.IWorkflowNameDao;
import be.wiv_isp.healthdata.hd4res.domain.WorkflowName;

@Repository
public class WorkflowNameDao extends CrudDao<WorkflowName, Long> implements IWorkflowNameDao {

	public WorkflowNameDao() {
		super(WorkflowName.class);
	}

	@Override
	public Long getBiggest(String definitionName) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WorkflowName> cq = cb.createQuery(WorkflowName.class);
		Root<WorkflowName> rootEntry = cq.from(WorkflowName.class);
		CriteriaQuery<WorkflowName> all = cq.select(rootEntry);

		List<Predicate> predicates = new ArrayList<>();
		if (!StringUtils.isBlank(definitionName)) {
			predicates.add(cb.equal(rootEntry.get("definitionName"), definitionName));
		}
		cq.orderBy(cb.desc(rootEntry.get("number")));

		if (!predicates.isEmpty()) {
			cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		}

		TypedQuery<WorkflowName> query = em.createQuery(all);
		query.setMaxResults(1);
		List<WorkflowName> resultList = query.getResultList();
		if (resultList.isEmpty()) {
			return null;
		} else {
			return resultList.get(0).getNumber();
		}
	}
}
