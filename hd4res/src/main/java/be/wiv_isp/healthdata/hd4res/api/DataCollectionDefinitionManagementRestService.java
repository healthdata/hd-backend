/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.api;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.annotation.Auditable;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ApiType;
import be.wiv_isp.healthdata.hd4res.dto.DataCollectionDefinitionHistoryDto;
import be.wiv_isp.healthdata.hd4res.dto.DataCollectionDefinitionManagementDto;
import be.wiv_isp.healthdata.hd4res.service.IDataCollectionDefinitionManagementService;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import org.apache.commons.collections.CollectionUtils;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.List;
import java.util.Set;

@Component
@Path("/datacollectiondefinitions/manager")
public class DataCollectionDefinitionManagementRestService {

	@Autowired
	private IDataCollectionDefinitionManagementService dataCollectionDefinitionManagementService;

	@Transactional
	@GET
	@Path("/{dataCollectionDefinitionId}")
	@PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION_MANAGER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("dataCollectionDefinitionId") Long dataCollectionDefinitionId) throws Exception {
		final DataCollectionDefinitionManagementDto dataCollectionDefinitionDto = dataCollectionDefinitionManagementService.get(dataCollectionDefinitionId);
		return Response.ok(dataCollectionDefinitionDto).build();
	}

	@Transactional
	@GET
	@PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION_MANAGER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(@Context UriInfo info) throws Exception {
		Long groupId = getGroupId(info);
		if (groupId == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_PARAMETER, "groupId");
			throw exception;
		}
		final List<DataCollectionDefinitionManagementDto> dataCollectionDefinitions = dataCollectionDefinitionManagementService.getAll(groupId);
		return Response.ok(dataCollectionDefinitions).build();
	}

	@Transactional
	@POST
	@PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION_MANAGER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(String json) throws Exception {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		final HDUserDetails principal = (HDUserDetails) authentication.getPrincipal();
		final DataCollectionDefinitionManagementDto dataCollectionDefinition = dataCollectionDefinitionManagementService.create(new JSONObject(json), principal.getUsername());
		return Response.ok(dataCollectionDefinition).build();
	}

	@Transactional
	@PUT
	@Path("/{dataCollectionDefinitionId}")
	@PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION_MANAGER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("dataCollectionDefinitionId") Long dataCollectionDefinitionId, String json) throws Exception {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		final HDUserDetails principal = (HDUserDetails) authentication.getPrincipal();
		final DataCollectionDefinitionManagementDto dataCollectionDefinition = dataCollectionDefinitionManagementService.update(dataCollectionDefinitionId, new JSONObject(json), principal.getUsername());
		return Response.ok(dataCollectionDefinition).build();
	}

	@Transactional
	@DELETE
	@Path("/{dataCollectionDefinitionId}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION_MANAGER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("dataCollectionDefinitionId") Long dataCollectionDefinitionId) {
		dataCollectionDefinitionManagementService.delete(dataCollectionDefinitionId);
		return Response.noContent().build();
	}

	@Transactional
	@GET
	@Path("/{dataCollectionDefinitionId}/history")
	@PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION_MANAGER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response history(@PathParam("dataCollectionDefinitionId") Long dataCollectionDefinitionId) throws Exception {
		final List<DataCollectionDefinitionHistoryDto> dataCollectionDefinitionHistory = dataCollectionDefinitionManagementService.history(dataCollectionDefinitionId);
		return Response.ok(dataCollectionDefinitionHistory).build();
	}

	@Transactional
	@POST
	@Path("/{dataCollectionDefinitionId}/publish")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION_MANAGER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response publish(@PathParam("dataCollectionDefinitionId") Long dataCollectionDefinitionId) throws Exception {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		final HDUserDetails principal = (HDUserDetails) authentication.getPrincipal();
		dataCollectionDefinitionManagementService.publish(dataCollectionDefinitionId, principal.getUsername());
		return Response.noContent().build();
	}

	@Transactional
	@POST
	@Path("/{dataCollectionDefinitionId}/pdf/update")
	@PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION_MANAGER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response pdfUpdate(@PathParam("dataCollectionDefinitionId") Long dataCollectionDefinitionId) throws Exception {
		dataCollectionDefinitionManagementService.pdfUpdate(dataCollectionDefinitionId);
		return Response.noContent().build();
	}

	@Transactional
	@POST
	@Path("/pdf/update")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION_MANAGER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response pdfUpdateAll(@Context UriInfo info) throws Exception {
		Set<Long> errorResults = dataCollectionDefinitionManagementService.pdfUpdateAll();
		if(CollectionUtils.isEmpty(errorResults)) {
			return Response.noContent().build();
		} else {
			return Response.ok(errorResults).build();
		}
	}

	@Transactional
	@POST
	@Path("/{dataCollectionDefinitionId}/pdf/republish")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION_MANAGER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response pdfRepublish(@PathParam("dataCollectionDefinitionId") Long dataCollectionDefinitionId) throws Exception {
		dataCollectionDefinitionManagementService.pdfRepublish(dataCollectionDefinitionId);
		return Response.noContent().build();
	}

	private Long getGroupId(UriInfo info) {
		MultivaluedMap<String,String> queryParameters = info.getQueryParameters();
		Long groupId = null;
		for (String queryParameter : queryParameters.keySet()) {
			if ("groupId".equalsIgnoreCase(queryParameter))
				groupId = Long.valueOf(queryParameters.getFirst(queryParameter));
		}
		return groupId;
	}

}
