/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dto;

import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import be.wiv_isp.healthdata.hd4res.domain.ParticipationDocument;
import be.wiv_isp.healthdata.hd4res.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.orchestration.dto.AbstractParticipationWorkflowDto;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.sql.Timestamp;

public class ParticipationWorkflowDto extends AbstractParticipationWorkflowDto<ParticipationWorkflow, ParticipationDocument> {

	protected Timestamp startedOn;
	private String hd4dpWorkflowId;
	private String identificationType;
	private String identificationValue;
	private String identificationName;

	public ParticipationWorkflowDto() {

	}

	public ParticipationWorkflowDto(ParticipationWorkflow workflow) {
		super(workflow);
		setStartedOn(workflow.getStartedOn());
		setHd4dpWorkflowId(workflow.getHd4dpWorkflowId());
		setIdentificationType(workflow.getIdentificationType());
		setIdentificationValue(workflow.getIdentificationValue());
		setIdentificationName(workflow.getIdentificationName());
	}

	public ParticipationWorkflowDto(ParticipationWorkflowDto dto) {
		super(dto);
		startedOn = dto.getStartedOn();
		hd4dpWorkflowId = dto.getHd4dpWorkflowId();
		identificationType = dto.getIdentificationType();
		identificationValue = dto.getIdentificationValue();
		identificationName = dto.getIdentificationName();
	}

	@JsonSerialize(using = TimestampSerializer.class)
	public Timestamp getStartedOn() {
		return startedOn;
	}

	@JsonDeserialize(using = TimestampDeserializer.class)
	public void setStartedOn(Timestamp startedOn) {
		this.startedOn = startedOn;
	}

	public String getHd4dpWorkflowId() {
		return hd4dpWorkflowId;
	}

	public void setHd4dpWorkflowId(String hd4dpWorkflowId) {
		this.hd4dpWorkflowId = hd4dpWorkflowId;
	}

	public String getIdentificationValue() {
		return identificationValue;
	}

	public void setIdentificationValue(String identificationValue) {
		this.identificationValue = identificationValue;
	}

	public String getIdentificationType() {
		return identificationType;
	}

	public void setIdentificationType(String identificationType) {
		this.identificationType = identificationType;
	}

	public String getIdentificationName() {
		return identificationName;
	}

	public void setIdentificationName(String identificationName) {
		this.identificationName = identificationName;
	}
}