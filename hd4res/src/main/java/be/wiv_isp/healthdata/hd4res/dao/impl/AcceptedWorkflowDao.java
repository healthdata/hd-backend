/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDao;
import be.wiv_isp.healthdata.hd4res.dao.IAcceptedWorkflowDao;
import be.wiv_isp.healthdata.hd4res.domain.AcceptedWorkflow;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class AcceptedWorkflowDao extends CrudDao<AcceptedWorkflow, Long> implements IAcceptedWorkflowDao {

	public AcceptedWorkflowDao() {
		super(AcceptedWorkflow.class);
	}

	@Override
	public List<AcceptedWorkflow> getAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<AcceptedWorkflow> cq = cb.createQuery(AcceptedWorkflow.class);
		Root<AcceptedWorkflow> rootEntry = cq.from(AcceptedWorkflow.class);

		CriteriaQuery<AcceptedWorkflow> all = cq.select(rootEntry);
		all.where(cb.isFalse(rootEntry.<Boolean>get("errorProcessing")));

		TypedQuery<AcceptedWorkflow> allQuery = em.createQuery(all);
		return allQuery.getResultList();
	}

}
