/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.common.api.ClientVersion;
import be.wiv_isp.healthdata.common.api.exception.NotFoundException;
import be.wiv_isp.healthdata.common.domain.Baseline;
import be.wiv_isp.healthdata.common.domain.enumeration.Language;
import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDto;
import be.wiv_isp.healthdata.common.dto.FollowUpDefinitionDto;
import be.wiv_isp.healthdata.common.dto.PdfTemplateDto;
import be.wiv_isp.healthdata.common.dto.SalesForceDataCollection;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.common.util.TokenUtils;
import be.wiv_isp.healthdata.hd4res.dao.IDataCollectionDefinitionHistoryDao;
import be.wiv_isp.healthdata.hd4res.dao.IFollowUpDefinitionDao;
import be.wiv_isp.healthdata.hd4res.domain.Description;
import be.wiv_isp.healthdata.hd4res.domain.Label;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionDefinition;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionDefinitionHistory;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionGroup;
import be.wiv_isp.healthdata.hd4res.domain.dcd.FollowUpDefinition;
import be.wiv_isp.healthdata.hd4res.domain.search.DataCollectionDefinitionHistorySearch;
import be.wiv_isp.healthdata.hd4res.domain.search.DataCollectionDefinitionSearch;
import be.wiv_isp.healthdata.hd4res.domain.search.FollowUpDefinitionSearch;
import be.wiv_isp.healthdata.hd4res.dto.DataCollectionDefinitionHistoryDto;
import be.wiv_isp.healthdata.hd4res.dto.DataCollectionDefinitionManagementDto;
import be.wiv_isp.healthdata.hd4res.dto.converters.DataCollectionDefinitionDtoConverter;
import be.wiv_isp.healthdata.hd4res.dto.converters.DataCollectionDefinitionHistoryDtoConverter;
import be.wiv_isp.healthdata.hd4res.dto.converters.DataCollectionDefinitionManagementDtoConverter;
import be.wiv_isp.healthdata.hd4res.mapper.DataCollectionDefinitionRequestMapper;
import be.wiv_isp.healthdata.hd4res.service.*;
import be.wiv_isp.healthdata.orchestration.api.uri.DataCollectionDefinitionUri;
import be.wiv_isp.healthdata.orchestration.api.uri.PdfTemplateUri;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.DataCollection;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IUserDataCollectionService;
import be.wiv_isp.healthdata.orchestration.service.IUserService;
import be.wiv_isp.healthdata.orchestration.service.IWebServiceClientService;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.MultiPart;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.MessageFormat;
import java.util.*;

@Service
public class DataCollectionDefinitionManagementService implements IDataCollectionDefinitionManagementService {

    private static final Logger LOG = LoggerFactory.getLogger(DataCollectionDefinitionManagementService.class);

    @Autowired
    private IWebServiceClientService webServiceClientService;
    @Autowired
    private IConfigurationService configurationService;
    @Autowired
    private IDataCollectionDefinitionService dataCollectionDefinitionService;
    @Autowired
    private IDataCollectionDefinitionHistoryDao historyDao;
    @Autowired
    private IFollowUpDefinitionDao followUpDao;
    @Autowired
    private IDataCollectionGroupService dataCollectionGroupService;
    @Autowired
    private IDataCollectionService dataCollectionService;
    @Autowired
    private IMappingService mappingService;
    @Autowired
    protected IUserDataCollectionService userDataCollectionService;
    @Autowired
    private IUserService userService;

    @Override
    public DataCollectionDefinitionManagementDto get(Long definitionId) {
        DataCollectionDefinition definition = getDCD(definitionId);
        DataCollectionDefinitionHistory history = getHistory(definition);
        List<FollowUpDefinition> followUpDefinitions = getFollowUpDefinitions(history);
        checkAccess(definition);
        return DataCollectionDefinitionManagementDtoConverter.convert(definition, history, followUpDefinitions);
    }

    @Override
    public List<DataCollectionDefinitionManagementDto> getAll(Long groupId) {
        List<DataCollectionDefinition> definitions = dataCollectionDefinitionService.getAll(groupId);
        final List<DataCollectionDefinitionManagementDto> results = new ArrayList<>();
        for (DataCollectionDefinition definition : definitions) {
            if(hasAccess(definition)) {
                DataCollectionDefinitionHistory history = getHistory(definition);
                List<FollowUpDefinition> followUpDefinitions = getFollowUpDefinitions(history);
                results.add(DataCollectionDefinitionManagementDtoConverter.convert(definition, history, followUpDefinitions));
            }
        }
        return results;
    }

    @Override
    public DataCollectionDefinitionManagementDto create(JSONObject json, String userName) throws Exception {
        checkUseDDE();
        DataCollectionDefinitionManagementDto dto = unmarshaller(json);
        DataCollectionGroup group = getGroup(dto.getDataCollectionGroupId());
        checkDefinitionName(group.getName(), dto.getDataCollectionDefinitionName());
        checkAccess(dto.getDataCollectionDefinitionName());
        DataCollectionDefinition latestVersion = getLatestVersion(dto.getDataCollectionGroupId(), dto.getDataCollectionDefinitionName());
        if (latestVersion == null ? !dto.getMinorVersion().equals(0): !dto.getMinorVersion().equals(latestVersion.getMinorVersion() + 1)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_CONFLICT, "Version numbers must be sequential");
            throw exception;
        }
        if(latestVersion != null && !latestVersion.isPublished()) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_CONFLICT, "Can't create a new version if the previous version isn't published");
            throw exception;
        }
        DataCollectionDefinition definition = new DataCollectionDefinition();
        if(dto.getMinorVersion() != 0) {
            DataCollectionDefinitionSearch search = new DataCollectionDefinitionSearch();
            search.setDataCollectionDefinitionName(dto.getDataCollectionDefinitionName());
            search.setDataCollectionGroupId(dto.getDataCollectionGroupId());
            search.setMinorVersion(dto.getMinorVersion() - 1);
            DataCollectionDefinition previousDataCollectionDefinition = dataCollectionDefinitionService.getUnique(search);
            definition.setStandAloneCatalogueId(previousDataCollectionDefinition.getStandAloneCatalogueId());
        }

        definition.setDataCollectionName(dto.getDataCollectionDefinitionName());
        if(dto.getDescription() != null) {
            Description description = new Description();
            description.setEn(dto.getDescription().getEn());
            description.setFr(dto.getDescription().getFr());
            description.setNl(dto.getDescription().getNl());
            definition.setDescription(description);
        }
        if(dto.getLabel() != null) {
            Label label = new Label();
            label.setEn(dto.getLabel().getEn());
            label.setFr(dto.getLabel().getFr());
            label.setNl(dto.getLabel().getNl());
            definition.setLabel(label);
        }
        definition.setMinorVersion(dto.getMinorVersion());
        definition.setPublished(false);
        definition.setDataCollectionGroupId(dto.getDataCollectionGroupId());
        dataCollectionDefinitionService.create(definition);

        DataCollectionDefinitionHistory history = new DataCollectionDefinitionHistory();
        history.setDataCollectionDefinitionId(definition.getId());
        history.setContent(dto.getContent());
        history.setDescription(dto.getUserDescription());
        history.setUserName(userName);
        historyDao.create(history);

        if (0 == dto.getMinorVersion()) {
            definition.setStandAloneCatalogueId(definition.getId());
        }
        definition.setHistoryId(history.getId());
        dataCollectionDefinitionService.update(definition);

        List<FollowUpDefinition> followUpDefinitions = createFollowUpDefinitions(dto, history.getId());

        DataCollectionDefinitionDto catalogueDefinition = DataCollectionDefinitionDtoConverter.convert(definition, history, followUpDefinitions);
        catalogueDefinition.setId(definition.getStandAloneCatalogueId());
        uploadDcdToCatalogue(catalogueDefinition, definition.getId().equals(definition.getStandAloneCatalogueId()));
        PdfTemplateDto pdfTemplate = getPdfTemplate(definition.getStandAloneCatalogueId(), history);
        uploadPdfToCatalogue(pdfTemplate, definition.getId().equals(definition.getStandAloneCatalogueId()));
        return DataCollectionDefinitionManagementDtoConverter.convert(definition, history, followUpDefinitions);
    }

    private boolean isExistingDataCollection(String dataCollectionName) {
        Set<String> dataCollections = dataCollectionService.getDataCollectionsFlat(null);
        if (dataCollections.contains(dataCollectionName)) {
            return true;
        }
        else {
            for (String dcd : dataCollections) {
                if (dcd.equalsIgnoreCase(dataCollectionName) && !dcd.equals(dataCollectionName)) {
                    HealthDataException exception = new HealthDataException();
                    exception.setExceptionType(ExceptionType.CREATE_GENERAL);
                    throw exception;
                }
            }
            return false;
        }
    }

//    private void assignDataCollectionToUser(String userName, String dataCollectionName) {
//        User user = userService.get(userName, null);
//        if(user.isUser()) {
//            Set<String> dataCollectionNames = user.getDataCollectionNames();
//            if(dataCollectionNames == null) {
//                dataCollectionNames = new HashSet<>();
//            }
//            dataCollectionNames.add(dataCollectionName);
//            userService.update(user);
//        }
//    }

    private void createDataCollectionInSalesForce(String dataCollectionName) {
        Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);
        Configuration catalogueUserName = configurationService.get(ConfigurationKey.CATALOGUE_USERNAME);
        Configuration cataloguePassword = configurationService.get(ConfigurationKey.CATALOGUE_PASSWORD);
        String accessToken = TokenUtils.getAccessTokenCatalogue(catalogueHost.getValue(), catalogueUserName.getValue(), cataloguePassword.getValue());

        SalesForceDataCollection dataCollection = new SalesForceDataCollection();
        dataCollection.setName(dataCollectionName);

        WebServiceBuilder builder = new WebServiceBuilder();
        builder.setUrl(catalogueHost.getValue() + "/datacollections");
        builder.setPost(true);
        builder.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
        builder.setType(MediaType.APPLICATION_JSON_TYPE);
        builder.setJson(dataCollection);
        builder.setReturnType(new GenericType<ClientResponse>(){});

        ClientResponse response = (ClientResponse) webServiceClientService.callWebService(builder);

        if (response.getStatus() != ClientResponse.Status.NO_CONTENT.getStatusCode()) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, "Couldn't create the dataCollection in salesForce");
            throw exception;
        }
    }

    @Override
    public DataCollectionDefinitionManagementDto update(Long definitionId, JSONObject json, String userName) throws Exception {
        checkUseDDE();
        DataCollectionDefinitionManagementDto dto = unmarshaller(json);
        checkAccess(dto.getDataCollectionDefinitionName());
        if (definitionId == null || !definitionId.equals(dto.getDataCollectionDefinitionId())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, MessageFormat.format("The id provided in the url ({0}) does not match with the id provided in the request body ({1})", definitionId, dto.getDataCollectionDefinitionId()));
            throw exception;
        }
        DataCollectionDefinition definition = getDCD(definitionId);
        if (!dto.getDataCollectionDefinitionName().equalsIgnoreCase(definition.getDataCollectionName()) || !dto.getMinorVersion().equals(definition.getMinorVersion())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, "Cannot change name of version when updating an existing data collection definition");
            throw exception;
        }
        DataCollectionDefinition nextMinor = dataCollectionDefinitionService.getNextMinor(definition);
        if (nextMinor != null || definition.isPublished()) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, "You can only update the last minor version and only if it isn't published");
            throw exception;
        }

        DataCollectionDefinitionHistory history = new DataCollectionDefinitionHistory();
        history.setDataCollectionDefinitionId(definition.getId());
        history.setContent(dto.getContent());
        history.setDescription(dto.getUserDescription());
        history.setUserName(userName);
        historyDao.create(history);

        List<FollowUpDefinition> followUpDefinitions = createFollowUpDefinitions(dto, history.getId());

        definition.setHistoryId(history.getId());
        if(dto.getDescription() != null) {
            if(definition.getDescription() == null) {
                definition.setDescription(new Description());
            }
            Description description = definition.getDescription();
            description.setEn(dto.getDescription().getEn());
            description.setFr(dto.getDescription().getFr());
            description.setNl(dto.getDescription().getNl());
            definition.setDescription(description);
        }
        if(dto.getLabel() != null) {
            if(definition.getLabel() == null) {
                definition.setLabel(new Label());
            }
            Label label = definition.getLabel();
            label.setEn(dto.getLabel().getEn());
            label.setFr(dto.getLabel().getFr());
            label.setNl(dto.getLabel().getNl());
            definition.setLabel(label);
        }
        dataCollectionDefinitionService.update(definition);

        DataCollectionDefinitionDto catalogueDefinition = DataCollectionDefinitionDtoConverter.convert(definition, history, followUpDefinitions);
        catalogueDefinition.setId(definition.getStandAloneCatalogueId());
        uploadDcdToCatalogue(catalogueDefinition, false);
        PdfTemplateDto pdfTemplate = getPdfTemplate(definition.getStandAloneCatalogueId(), history);
        uploadPdfToCatalogue(pdfTemplate, false);
        return DataCollectionDefinitionManagementDtoConverter.convert(definition, history, followUpDefinitions);
    }

    private List<FollowUpDefinition> createFollowUpDefinitions(DataCollectionDefinitionManagementDto dto, Long historyId) {
        List<FollowUpDefinition> followUpDefinitions = new ArrayList<>();
        if(dto.getFollowUpDefinitions() != null) {
            for (FollowUpDefinitionDto followUpDefinitionDto : dto.getFollowUpDefinitions()) {
                FollowUpDefinition followUpDefinition = new FollowUpDefinition();
                followUpDefinition.setDataCollectionDefinitionHistoryId(historyId);
                followUpDefinition.setLabel(followUpDefinitionDto.getLabel());
                followUpDefinition.setName(followUpDefinitionDto.getName());
                followUpDefinition.setDescription(followUpDefinitionDto.getDescription());
                followUpDefinition.setTiming(followUpDefinitionDto.getTiming());
                if (followUpDefinitionDto.getBaseline() != null) {
                    Baseline baseline = new Baseline();
                    baseline.setType(followUpDefinitionDto.getBaseline().getType());
                    baseline.setValue(followUpDefinitionDto.getBaseline().getValue());
                    followUpDefinition.setBaseline(baseline);
                }
                followUpDefinition.setConditions(followUpDefinitionDto.getConditions());
                followUpDao.create(followUpDefinition);
                followUpDefinitions.add(followUpDefinition);
            }
        }
        return followUpDefinitions;
    }

    @Override
    public void delete(Long definitionId) {
        DataCollectionDefinition definition = getDCD(definitionId);
        DataCollectionDefinition nextMinor = dataCollectionDefinitionService.getNextMinor(definition);
        if (nextMinor != null || definition.isPublished()) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_FORBIDDEN, "You can only delete the last minor version and only if it isn't published");
            throw exception;
        }
        definition.setHistoryId(null);
        dataCollectionDefinitionService.update(definition);

        DataCollectionDefinitionHistorySearch historySearch = new DataCollectionDefinitionHistorySearch();
        historySearch.setDataCollectionDefinitionId(definitionId);
        List<DataCollectionDefinitionHistory> histories = historyDao.getAll(historySearch);
        for (DataCollectionDefinitionHistory history : histories) {
            historyDao.delete(history);
        }
        dataCollectionDefinitionService.delete(definition);

        if(definition.getMinorVersion() != 0) {
            DataCollectionDefinitionSearch search = new DataCollectionDefinitionSearch();
            search.setDataCollectionDefinitionName(definition.getDataCollectionName());
            search.setDataCollectionGroupId(definition.getDataCollectionGroupId());
            search.setMinorVersion(definition.getMinorVersion() - 1);
            DataCollectionDefinition previousDefinition = dataCollectionDefinitionService.getUnique(search);
            DataCollectionDefinitionHistory history = getHistory(previousDefinition);
            List<FollowUpDefinition> followUpDefinitions = getFollowUpDefinitions(history);

            DataCollectionDefinitionDto catalogueDefinition = DataCollectionDefinitionDtoConverter.convert(previousDefinition, history, followUpDefinitions);
            catalogueDefinition.setId(previousDefinition.getStandAloneCatalogueId());
            uploadDcdToCatalogue(catalogueDefinition, false);
            PdfTemplateDto pdfTemplate = getPdfTemplate(previousDefinition.getStandAloneCatalogueId(), history);
            uploadPdfToCatalogue(pdfTemplate, false);
        } else {
            deletePdfOnCatalogue(definitionId);
            deleteDcdOnCatalogue(definitionId);
        }
    }

    @Override
    public List<DataCollectionDefinitionHistoryDto> history(Long definitionId) {
        getDCD(definitionId);
        DataCollectionDefinitionHistorySearch search = new DataCollectionDefinitionHistorySearch();
        search.setDataCollectionDefinitionId(definitionId);
        List<DataCollectionDefinitionHistory> histories = historyDao.getAll(search);

        List<DataCollectionDefinitionHistoryDto> result = new ArrayList<>();
        for (DataCollectionDefinitionHistory history : histories) {
            List<FollowUpDefinition> followUpDefinitions = getFollowUpDefinitions(history);
            result.add(DataCollectionDefinitionHistoryDtoConverter.convert(history, followUpDefinitions));
        }
        return result;
    }

    @Override
    public void publish(Long definitionId, String userName) {
        checkUseDDE();
        checkUsePublish();
        final DataCollectionDefinition definition = getDCD(definitionId);
        DataCollectionDefinitionHistory history = getHistory(definition);
        List<FollowUpDefinition> followUpDefinitions = getFollowUpDefinitions(history);
        LOG.info("user [{}] published dataCollectionDefinition [{}]", userName, definition);
        definition.setPublished(true);
        dataCollectionDefinitionService.update(definition);
        DataCollectionDefinitionDto catalogueDefinition = DataCollectionDefinitionDtoConverter.convert(definition, history, followUpDefinitions);
        catalogueDefinition.setId(definition.getStandAloneCatalogueId());
        boolean create = false;
        if (0 == definition.getMinorVersion()) {
            create = true;
        }
        uploadDcdToProductionCatalogue(catalogueDefinition, create);
        PdfTemplateDto pdfTemplate = getPdfTemplate(definition.getStandAloneCatalogueId(), history);
        uploadPdfToProductionCatalogue(pdfTemplate, create);
    }

    private DataCollectionDefinition getDCD(Long definitionId) {
        DataCollectionDefinition dcd = dataCollectionDefinitionService.get(definitionId);
        if (dcd == null) {
            throw new NotFoundException("No data collection definition found with id " + definitionId);
        }
        return dcd;
    }

    private DataCollectionDefinitionHistory getHistory(DataCollectionDefinition definition) {
        return historyDao.get(definition.getHistoryId());
    }

    private List<FollowUpDefinition> getFollowUpDefinitions(DataCollectionDefinitionHistory history) {
        FollowUpDefinitionSearch followUpDefinitionSearch = new FollowUpDefinitionSearch();
        followUpDefinitionSearch.setDataCollectionDefinitionHistoryId(history.getId());
        return followUpDao.getAll(followUpDefinitionSearch);
    }

    @Override
    public Set<Long> pdfUpdateAll() {
        List<DataCollectionGroup> groups = dataCollectionGroupService.getAll();
        Set<Long> errorResults = new HashSet<>();
        for (DataCollectionGroup group : groups) {
            List<DataCollectionDefinition> definitions = dataCollectionDefinitionService.getLatestMinors(group.getId());
            for (DataCollectionDefinition definition : definitions) {
                try {
                    pdfUpdate(definition.getId());
                } catch (Exception e) {
                    errorResults.add(definition.getId());
                }
            }
        }
        return errorResults;
    }

    @Override
    public void pdfUpdate(Long definitionId) {
        checkUseDDE();
        DataCollectionDefinition definition = getDCD(definitionId);
        checkAccess(definition);
        DataCollectionDefinition nextMinor = dataCollectionDefinitionService.getNextMinor(definition);
        if (nextMinor != null) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, "You can only update the pdf of the last minor version.");
            throw exception;
        }
        DataCollectionDefinitionHistory history = getHistory(definition);
        PdfTemplateDto pdfTemplate = getPdfTemplate(definition.getStandAloneCatalogueId(), history);
        uploadPdfToCatalogue(pdfTemplate, false);
    }

    @Override
    public void pdfRepublish(Long definitionId) {
        checkUseDDE();
        checkUsePublish();
        DataCollectionDefinition definition = getDCD(definitionId);
        checkAccess(definition);
        DataCollectionDefinition nextMinor = dataCollectionDefinitionService.getNextMinor(definition);
        if(nextMinor == null) {
            if(!definition.isPublished()) {
                throw getRepublishPdfException();
            }
        } else {
            if(nextMinor.isPublished()) {
                throw getRepublishPdfException();
            }
        }
        DataCollectionDefinitionHistory history = getHistory(definition);
        PdfTemplateDto pdfTemplate = getPdfTemplate(definition.getStandAloneCatalogueId(), history);
        uploadPdfToProductionCatalogue(pdfTemplate, false);
    }

    private HealthDataException getRepublishPdfException() {
        HealthDataException exception = new HealthDataException();
        exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, "You can only republish the pdf of the last minor version that is published.");
        return exception;
    }

    public void uploadDcdToCatalogue(DataCollectionDefinitionDto definitionDto, Boolean create) {
        uploadDcdToCatalogue(definitionDto, ConfigurationKey.CATALOGUE_HOST, ConfigurationKey.CATALOGUE_USERNAME, ConfigurationKey.CATALOGUE_PASSWORD, create);
    }

    public void uploadDcdToProductionCatalogue(DataCollectionDefinitionDto definitionDto, Boolean create) {
        uploadDcdToCatalogue(definitionDto, ConfigurationKey.CATALOGUE_PROD_HOST, ConfigurationKey.CATALOGUE_PROD_USERNAME, ConfigurationKey.CATALOGUE_PROD_PASSWORD, create);
    }

    public void uploadDcdToCatalogue(DataCollectionDefinitionDto definitionDto, ConfigurationKey hostKey, ConfigurationKey usernameKey, ConfigurationKey passwordKey, boolean create) {
        final Configuration catalogueHost = configurationService.get(hostKey);
        if (StringUtils.isBlank(catalogueHost.getValue())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "Configuration", hostKey);
            throw exception;
        }
        DataCollectionDefinitionUri dataCollectionDefinitionUri = new DataCollectionDefinitionUri();
        dataCollectionDefinitionUri.setHost(catalogueHost.getValue());
        if (!create) {
            dataCollectionDefinitionUri.setId(definitionDto.getId());
        }

        String accessToken = getAccessToken(catalogueHost, usernameKey, passwordKey);

        WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.addHeader(HttpHeaders.CLIENT_VERSION, ClientVersion.DataCollectionDefinition.getDefault());
        wsb.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
        wsb.setUrl(dataCollectionDefinitionUri.toString());
        if (create) {
            wsb.setPost(true);
        } else {
            wsb.setPut(true);
        }
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.setType(MediaType.APPLICATION_JSON_TYPE);
        wsb.setJson(definitionDto);
        wsb.setReturnType(new GenericType<ClientResponse>() {
        });

        ClientResponse response = (ClientResponse) webServiceClientService.callWebService(wsb);
        if (response.getStatus() < 200 || response.getStatus() >= 300) {
            HealthDataException exception = new HealthDataException();
            exception.setResponseStatus(Response.Status.INTERNAL_SERVER_ERROR);
            exception.setResponseMessage(MessageFormat.format("Could not upload definition to the catalogue. Catalogue url: {0}. Definition: [{1}]", catalogueHost.getValue(), definitionDto));
            throw exception;
        }
    }


    public void deleteDcdOnCatalogue(Long definitionId) {
        deleteDcdOnCatalogue(definitionId, ConfigurationKey.CATALOGUE_HOST, ConfigurationKey.CATALOGUE_USERNAME, ConfigurationKey.CATALOGUE_PASSWORD);
    }

    public void deleteDcdOnCatalogue(Long definitionId, ConfigurationKey hostKey, ConfigurationKey usernameKey, ConfigurationKey passwordKey) {
        final Configuration catalogueHost = configurationService.get(hostKey);
        if (StringUtils.isBlank(catalogueHost.getValue())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "Configuration", hostKey);
            throw exception;
        }
        DataCollectionDefinitionUri dataCollectionDefinitionUri = new DataCollectionDefinitionUri();
        dataCollectionDefinitionUri.setHost(catalogueHost.getValue());
        dataCollectionDefinitionUri.setId(definitionId);

        String accessToken = getAccessToken(catalogueHost, usernameKey, passwordKey);

        WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.addHeader(HttpHeaders.CLIENT_VERSION, ClientVersion.DataCollectionDefinition.getDefault());
        wsb.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
        wsb.setUrl(dataCollectionDefinitionUri.toString());
        wsb.setDelete(true);

        wsb.setReturnType(new GenericType<ClientResponse>() {
        });

        ClientResponse response = (ClientResponse) webServiceClientService.callWebService(wsb);
        if (response.getStatus() < 200 || response.getStatus() >= 300) {
            HealthDataException exception = new HealthDataException();
            exception.setResponseStatus(Response.Status.INTERNAL_SERVER_ERROR);
            exception.setResponseMessage("Couldn't delete the definition [" + definitionId + "]");
        }
    }


    public void uploadPdfToCatalogue(PdfTemplateDto pdfTemplate, Boolean create) {
        uploadPdfToCatalogue(pdfTemplate, ConfigurationKey.CATALOGUE_HOST, ConfigurationKey.CATALOGUE_USERNAME, ConfigurationKey.CATALOGUE_PASSWORD, create);
    }

    public void uploadPdfToProductionCatalogue(PdfTemplateDto pdfTemplate, Boolean create) {
        uploadPdfToCatalogue(pdfTemplate, ConfigurationKey.CATALOGUE_PROD_HOST, ConfigurationKey.CATALOGUE_PROD_USERNAME, ConfigurationKey.CATALOGUE_PROD_PASSWORD, create);
    }

    public void uploadPdfToCatalogue(PdfTemplateDto pdfTemplate, ConfigurationKey hostKey, ConfigurationKey usernameKey, ConfigurationKey passwordKey, Boolean create) {
        final Configuration catalogueHost = configurationService.get(hostKey);
        if (StringUtils.isBlank(catalogueHost.getValue())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "Configuration", hostKey);
            throw exception;
        }
        PdfTemplateUri uri = new PdfTemplateUri();
        uri.setHost(catalogueHost.getValue());
        uri.setId(pdfTemplate.getId());

        MultiPart multiPart = new MultiPart().
                bodyPart(new BodyPart(pdfTemplate.getEn(), MediaType.TEXT_XML_TYPE)).
                bodyPart(new BodyPart(pdfTemplate.getFr(), MediaType.TEXT_XML_TYPE)).
                bodyPart(new BodyPart(pdfTemplate.getNl(), MediaType.TEXT_XML_TYPE));

        String accessToken = getAccessToken(catalogueHost, usernameKey, passwordKey);

        WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.addHeader(HttpHeaders.CLIENT_VERSION, ClientVersion.PdfTemplate.getDefault());
        wsb.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
        wsb.setUrl(uri.toString());
        if(create) {
            wsb.setPost(true);
        } else {
            wsb.setPut(true);
        }
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.setType(multiPart.getMediaType());
        wsb.setJson(multiPart);
        wsb.setReturnType(new GenericType<ClientResponse>(){});
        ClientResponse response = (ClientResponse) webServiceClientService.callWebService(wsb);
        if (response.getStatus() < 200 || response.getStatus() >= 300) {
            HealthDataException exception = new HealthDataException();
            exception.setResponseStatus(Response.Status.INTERNAL_SERVER_ERROR);
            exception.setResponseMessage("Couldn't upload the PDF template [" + pdfTemplate +"]");
        }
    }

    public void deletePdfOnCatalogue(Long definitionId) {
        deletePdfOnCatalogue(definitionId, ConfigurationKey.CATALOGUE_HOST, ConfigurationKey.CATALOGUE_USERNAME, ConfigurationKey.CATALOGUE_PASSWORD);
    }

    public void deletePdfOnCatalogue(Long definitionId, ConfigurationKey hostKey, ConfigurationKey usernameKey, ConfigurationKey passwordKey) {
        final Configuration catalogueHost = configurationService.get(hostKey);
        if (StringUtils.isBlank(catalogueHost.getValue())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "Configuration", hostKey);
            throw exception;
        }
        PdfTemplateUri uri = new PdfTemplateUri();
        uri.setHost(catalogueHost.getValue());
        uri.setId(definitionId);

        String accessToken = getAccessToken(catalogueHost, usernameKey, passwordKey);

        WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.addHeader(HttpHeaders.CLIENT_VERSION, ClientVersion.PdfTemplate.getDefault());
        wsb.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
        wsb.setUrl(uri.toString());
        wsb.setDelete(true);
        wsb.setReturnType(new GenericType<ClientResponse>(){});
        ClientResponse response = (ClientResponse) webServiceClientService.callWebService(wsb);
        if (response.getStatus() < 200 || response.getStatus() >= 300) {
            HealthDataException exception = new HealthDataException();
            exception.setResponseStatus(Response.Status.INTERNAL_SERVER_ERROR);
            exception.setResponseMessage("Couldn't delete the PDF template [" + definitionId +"]");
        }
    }

    private DataCollectionGroup getGroup(Long groupId) {
        DataCollectionGroup group = dataCollectionGroupService.get(groupId);
        if(group == null) {
            throw new NotFoundException("No data collection group found with id " + groupId);
        }
        return group;
    }

    private DataCollectionDefinition getLatestVersion(Long groupId, String definitionName) {
        DataCollectionDefinitionSearch search = new DataCollectionDefinitionSearch();
        search.setDataCollectionGroupId(groupId);
        search.setDataCollectionDefinitionName(definitionName);
        List<DataCollectionDefinition> definitions = dataCollectionDefinitionService.getAll(search);
        DataCollectionDefinition latestVersion = null;
        for (DataCollectionDefinition definition : definitions) {
            if (latestVersion == null || definition.getMinorVersion() > latestVersion.getMinorVersion()) {
                latestVersion = definition;
            }
        }
        return latestVersion;
    }

    private DataCollectionDefinitionManagementDto unmarshaller(JSONObject json) throws Exception {
        return DataCollectionDefinitionRequestMapper.convert(json);
    }

    private void checkDefinitionName(String groupName, String definitionName) {
        Map<DataCollection, Set<DataCollection>> dataCollections = dataCollectionService.getDataCollections();
        for (DataCollection group : dataCollections.keySet()) {
            if(group.getName().equals(groupName)) {
                Set<DataCollection> definitions = dataCollections.get(group);
                for (DataCollection definition : definitions) {
                    if(definition.getName().equals(definitionName)) {
                        return;
                    }

                }
            }
        }
        HealthDataException exception = new HealthDataException();
        exception.setExceptionType(ExceptionType.GENERAL_CONFLICT, "The data collection definition name ["+ definitionName +"] doesn't exist in salesForce for this group name [" + groupName+ "]");
        throw exception;
    }

    private String getAccessToken(Configuration catalogueHost, ConfigurationKey username, ConfigurationKey password) {
        Configuration catalogueUserName = configurationService.get(username);
        Configuration cataloguePassword = configurationService.get(password);

        return TokenUtils.getAccessTokenCatalogue(catalogueHost.getValue(), catalogueUserName.getValue(), cataloguePassword.getValue());
    }

    private PdfTemplateDto getPdfTemplate(Long catalogueId, DataCollectionDefinitionHistory history) {
        PdfTemplateDto pdfTemplateDto = getPdfTemplateDto(history.getContent());
        pdfTemplateDto.setId(catalogueId);
        return pdfTemplateDto;
    }

    private PdfTemplateDto getPdfTemplateDto(byte[] dcdContent) {
        byte[] pdfTemplateEn = mappingService.getPdfTemplate(dcdContent, Language.EN);
        byte[] pdfTemplateFr = mappingService.getPdfTemplate(dcdContent, Language.FR);
        byte[] pdfTemplateNl = mappingService.getPdfTemplate(dcdContent, Language.NL);
        PdfTemplateDto pdfTemplateDto = new PdfTemplateDto();
        pdfTemplateDto.setEn(pdfTemplateEn);
        pdfTemplateDto.setFr(pdfTemplateFr);
        pdfTemplateDto.setNl(pdfTemplateNl);
        return pdfTemplateDto;
    }

    private void checkUseDDE() {
        Configuration useDDE = configurationService.get(ConfigurationKey.USE_DATA_DEFINITION_EDITOR);
        if (!Boolean.valueOf(useDDE.getValue())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_UNAUTHORIZED);
            throw exception;
        }
    }

    private void checkUsePublish() {
        Configuration usePublish = configurationService.get(ConfigurationKey.USE_DATA_DEFINITION_PUBLISH);
        if (!Boolean.valueOf(usePublish.getValue())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_UNAUTHORIZED);
            throw exception;
        }
    }

    private void checkAccess(DataCollectionDefinition definition) {
       checkAccess(definition.getDataCollectionName());
    }

    private void checkAccess(String  definitionName) {
        if(!hasAccess(definitionName)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_UNAUTHORIZED);
            throw exception;
        }
    }

    private boolean hasAccess(DataCollectionDefinition definition) {
        return hasAccess(definition.getDataCollectionName());
    }

    private boolean hasAccess(String  definitionName) {
        HDUserDetails userDetails = AuthenticationService.getUserDetails();
        if(userService.isGuestAccount(userDetails.getUsername())) {
           return false;
        }
        if(userDetails.isAdmin()) {
            return true;
        }
        return userDataCollectionService.isUserAuthorized(userDetails, definitionName);
    }
}
