/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.tasks;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.hd4res.dao.IAcceptedWorkflowDao;
import be.wiv_isp.healthdata.hd4res.domain.AcceptedWorkflow;
import be.wiv_isp.healthdata.hd4res.service.IAcceptedWorkflowService;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.tasks.SchedulableHealthDataTask;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DataWarehouseTask extends SchedulableHealthDataTask {

	private static final Logger LOG = LoggerFactory.getLogger(DataWarehouseTask.class);

	@Autowired
	private IAcceptedWorkflowService dataWarehouseService;

	@Autowired
	private IAcceptedWorkflowDao acceptedWorkflowDao;

	@Override
	public String getName() {
		return "DATA_WAREHOUSE";
	}

	@Override
	protected String getTimingExpression() {
		return configurationService.get(ConfigurationKey.SCHEDULED_TASK_DATAWAREHOUSE_SERVICE_INTERVAL).getValue();
	}

	@Override
	public void execute() {
		final List<AcceptedWorkflow> all = acceptedWorkflowDao.getAll();

		if (CollectionUtils.isEmpty(all)) {
			return;
		}

		final Report report = new Report();
		LOG.debug("Sending accepted workflows to HD4DP installations");
		for (AcceptedWorkflow acceptedWorkflow : all) {
			final boolean success = dataWarehouseService.process(acceptedWorkflow);
			report.add(acceptedWorkflow.getWorkflowType(), success);
		}
		report.log();
	}

	private class Report {

		private Map<WorkflowType, Integer> success = new HashMap<>();
		private Map<WorkflowType, Integer> failures = new HashMap<>();

		public Report() {
			success.put(WorkflowType.REGISTRATION, 0);
			success.put(WorkflowType.PARTICIPATION, 0);
			failures.put(WorkflowType.REGISTRATION, 0);
			failures.put(WorkflowType.PARTICIPATION, 0);
		}

		public void add(WorkflowType workflowType, boolean success) {
			if(success) {
				int count = this.success.get(workflowType);
				this.success.put(workflowType, count+1);
			} else {
				int count = this.failures.get(workflowType);
				this.failures.put(workflowType, count+1);
			}
		}

		public void log() {
			int nbSuccess = success.get(WorkflowType.REGISTRATION);
			int nbFailures = failures.get(WorkflowType.REGISTRATION);

			if (nbSuccess == 0 && nbFailures == 0) {
				LOG.trace("No accepted registrations sent");
			} else if(nbFailures == 0) {
				LOG.info("{} accepted registrations(s) successfully sent", nbSuccess);
			} else {
				LOG.warn("{} accepted registrations(s) successfully sent. {} accepted registrations(s) could not be sent.", nbSuccess, nbFailures);
			}

			nbSuccess = success.get(WorkflowType.PARTICIPATION);
			nbFailures = failures.get(WorkflowType.PARTICIPATION);

			if (nbSuccess == 0 && nbFailures == 0) {
				LOG.trace("No accepted participations sent");
			} else if(nbFailures == 0) {
				LOG.info("{} accepted participation(s) successfully sent", nbSuccess);
			} else {
				LOG.warn("{} accepted participation(s) successfully sent. {} accepted participation(s) could not be sent.", nbSuccess, nbFailures);
			}
		}
	}
}
