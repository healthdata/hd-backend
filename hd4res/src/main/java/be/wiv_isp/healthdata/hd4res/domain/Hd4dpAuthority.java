/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain;

import be.wiv_isp.healthdata.common.util.StringUtils;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "HD4DP_AUTHORITIES")
public class Hd4dpAuthority implements GrantedAuthority {
	private static final long serialVersionUID = 1L;

	public static final String ADMIN = "ROLE_ADMIN";
	public static final String USER = "ROLE_USER";

	public Hd4dpAuthority() {

	}

	public Hd4dpAuthority(String authority) {
		this.authority = authority;
	}

	@Id
	@GeneratedValue
	@Column(name = "authority_id")
	private Long authorityId;

	@Column(name = "authority")
	private String authority;

	@Override
	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof Hd4dpAuthority) {
			Hd4dpAuthority other = (Hd4dpAuthority) o;

			return Objects.equals(authority, other.authority);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.authority);
	}

	@Override
	public String toString() {
		return StringUtils.toString(this);
	}
}
