/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.hd4res.dao.IEmVersionDao;
import be.wiv_isp.healthdata.hd4res.domain.EmVersion;
import be.wiv_isp.healthdata.hd4res.domain.search.EmVersionSearch;
import be.wiv_isp.healthdata.orchestration.domain.MessageCorrespondent;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class EmVersionDao extends CrudDaoV2<EmVersion, Long, EmVersionSearch> implements IEmVersionDao {

    public EmVersionDao() {
        super(EmVersion.class);
    }

    @Override
    public List<Predicate> getPredicates(EmVersionSearch search, CriteriaBuilder cb, Root<EmVersion> rootEntry) {
        final List<Predicate> predicates = new ArrayList<>();

        if (search.getMessageCorrespondent() != null) {
            predicates.add(cb.equal(rootEntry.<MessageCorrespondent>get("messageCorrespondent").get("identificationType"), search.getMessageCorrespondent().getIdentificationType()));
            predicates.add(cb.equal(rootEntry.<MessageCorrespondent>get("messageCorrespondent").get("identificationValue"), search.getMessageCorrespondent().getIdentificationValue()));
            predicates.add(cb.equal(rootEntry.<MessageCorrespondent>get("messageCorrespondent").get("applicationId"), search.getMessageCorrespondent().getApplicationId()));
        }
        if (search.isMultiLine() != null) {
            predicates.add(cb.equal(rootEntry.get("multiLine"), search.isMultiLine()));
        }

        return predicates;
    }

}
