/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4res.api.mapper;

import be.wiv_isp.healthdata.hd4res.domain.RegistrationDocument;
import be.wiv_isp.healthdata.orchestration.api.mapper.AbstractRegistrationDocumentMapper;
import be.wiv_isp.healthdata.orchestration.dto.DocumentDto;
import org.springframework.stereotype.Component;

@Component
public class RegistrationDocumentMapper extends AbstractRegistrationDocumentMapper<RegistrationDocument> {

    @Override
    public DocumentDto getDtoInstance(RegistrationDocument document) {
        DocumentDto documentDto = new DocumentDto(document);
        if (document.getCodedContent()==null || document.getCodedContent().isEmpty())
            documentDto.getDocumentData().setPatientID(document.getWorkflow().getMetaData());
        else
            documentDto.getDocumentData().setCoded(document.getCodedContent());

        return documentDto;
    }

}
