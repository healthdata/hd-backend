/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import be.wiv_isp.healthdata.common.util.StringUtils;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;

@Entity
@Table(name = "STABLE_DATA")
public class StableData {

    public enum Status {
        NEW, SENT, ERROR_SENDING, UPLOADED, ERROR_UPLOADING
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "PATIENT_ID", nullable = false)
    private String patientId;

    @Column(name = "DATA_COLLECTION_NAME", nullable = false)
    private String dataCollectionName;

    @Column(name = "IDENTIFICATION_TYPE", nullable = false)
    private String identificationType;

    @Column(name = "IDENTIFICATION_VALUE", nullable = false)
    private String identificationValue;

    @Column(name = "STATUS", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "CREATED_ON", nullable = false)
    private Timestamp createdOn;

    @Lob
    @Column(name = "CSV", nullable = false)
    private byte[] csv;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getDataCollectionName() {
        return dataCollectionName;
    }

    public void setDataCollectionName(String dataCollectionName) {
        this.dataCollectionName = dataCollectionName;
    }

    public void setIdentificationType(String identificationType) {
        this.identificationType = identificationType;
    }

    public String getIdentificationType() {
        return identificationType;
    }

    public String getIdentificationValue() {
        return identificationValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof StableData))
            return false;

        StableData that = (StableData) o;

        if (id != null ? !id.equals(that.id) : that.id != null)
            return false;
        if (!patientId.equals(that.patientId))
            return false;
        if (!dataCollectionName.equals(that.dataCollectionName))
            return false;
        if (!identificationType.equals(that.identificationType))
            return false;
        if (!identificationValue.equals(that.identificationValue))
            return false;
        if (status != that.status)
            return false;
        if (createdOn != null ? !createdOn.equals(that.createdOn) : that.createdOn != null)
            return false;
        return Arrays.equals(csv, that.csv);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + patientId.hashCode();
        result = 31 * result + dataCollectionName.hashCode();
        result = 31 * result + identificationType.hashCode();
        result = 31 * result + identificationValue.hashCode();
        result = 31 * result + status.hashCode();
        result = 31 * result + (createdOn != null ? createdOn.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(csv);
        return result;
    }

    public void setIdentificationValue(String identificationValue) {
        this.identificationValue = identificationValue;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public byte[] getCsv() {
        return csv;
    }

    public void setCsv(byte[] csv) {
        this.csv = csv;
    }

    @PrePersist
    public void onCreate() {
        setCreatedOn(new Timestamp(new Date().getTime()));
    }

    @Override
    public String toString() {
        return StringUtils.toStringExclude(this, "createdOn", "csv");
    }

    public HealthDataIdentification getHealthDataIdentification() {
        final HealthDataIdentification healthDataIdentification = new HealthDataIdentification();
        healthDataIdentification.setType(identificationType);
        healthDataIdentification.setValue(identificationValue);
        return healthDataIdentification;
    }

}
