/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain;

import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationWorkflow;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "WORKFLOWS")
public class RegistrationWorkflow extends AbstractRegistrationWorkflow<RegistrationDocument> {

	@Column(name = "READABLE_ID", unique = true)
	private String readableId;

	@Column(name = "HD4DP_WORKFLOW_ID")
	private String hd4dpWorkflowId;

	@Column(name = "IDENTIFICATION_TYPE")
	private String identificationType;

	@Column(name = "IDENTIFICATION_VALUE")
	private String identificationValue;

	@Column(name = "HD4PRC", nullable = false)
	private Boolean hd4prc = false;

	public String getReadableId() {
		return readableId;
	}

	public void setReadableId(String readableId) {
		this.readableId = readableId;
	}

	public String getHd4dpWorkflowId() {
		return hd4dpWorkflowId;
	}

	public void setHd4dpWorkflowId(String hd4dpWorkflowId) {
		this.hd4dpWorkflowId = hd4dpWorkflowId;
	}

	public String getIdentificationType() {
		return identificationType;
	}

	public void setIdentificationType(String identificationType) {
		this.identificationType = identificationType;
	}

	public String getIdentificationValue() {
		return identificationValue;
	}

	public void setIdentificationValue(String identificationValue) {
		this.identificationValue = identificationValue;
	}

	public Boolean getHd4prc() {
		return hd4prc;
	}

	public void setHd4prc(Boolean hd4prc) {
		this.hd4prc = hd4prc;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof RegistrationWorkflow) {
			RegistrationWorkflow other = (RegistrationWorkflow) o;

			return Objects.equals(id, other.id)
					&& Objects.equals(readableId, other.readableId)
					&& Objects.equals(status, other.status)
					&& Objects.equals(documentHistory, other.documentHistory)
					&& Objects.equals(dataCollectionName, other.dataCollectionName)
					&& Objects.equals(dataCollectionDefinitionId, other.dataCollectionDefinitionId)
					&& Objects.equals(metaData, other.metaData)
					&& Objects.equals(createdOn, other.createdOn)
					&& Objects.equals(updatedOn, other.updatedOn)
					&& Objects.equals(history, other.history)
					&& Objects.equals(hd4dpWorkflowId, other.hd4dpWorkflowId)
					&& Objects.equals(organization, other.organization);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				this.id,
				this.readableId,
				this.status,
				this.documentHistory,
				this.dataCollectionName,
				this.dataCollectionDefinitionId,
				this.metaData,
				this.createdOn,
				this.updatedOn,
				this.history,
				this.hd4dpWorkflowId,
				this.organization);
	}

	@Override
	public String toString() {
		return "RegistrationWorkflow {" +
				"id = " + Objects.toString(this.id) + ", " +
				"readableId = " + Objects.toString(this.readableId) + ", " +
				"status = " + Objects.toString(this.status) + ", " +
				"document = " + Objects.toString(this.documentHistory) + ", " +
				"dataCollectionName = " + Objects.toString(this.dataCollectionName) + ", " +
				"dataCollectionDefinitionId = " + Objects.toString(this.dataCollectionDefinitionId) + ", " +
				"metaData = " + Objects.toString(this.metaData) + ", " +
				"createdOn = " + Objects.toString(this.createdOn) + ", " +
				"updatedOn = " + Objects.toString(this.updatedOn) + ", " +
				"history = " + Objects.toString(this.history) + ", " +
				"hd4dpWorkflowId = " + Objects.toString(this.hd4dpWorkflowId) + ", " +
				"organization = " + Objects.toString(this.organization) + "}";
	}
}