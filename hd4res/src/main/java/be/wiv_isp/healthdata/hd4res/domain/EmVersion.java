/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4res.domain;

import be.wiv_isp.healthdata.orchestration.domain.MessageCorrespondent;

import javax.persistence.*;

@Entity
@Table(name="EM_VERSION")
public class EmVersion {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "EM_VERSION_ID")
    private Long id;
    @Embedded
    private MessageCorrespondent messageCorrespondent;
    @Column(name = "MULTI_LINE")
    private Boolean multiLine;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MessageCorrespondent getMessageCorrespondent() {
        return messageCorrespondent;
    }

    public void setMessageCorrespondent(MessageCorrespondent messageCorrespondent) {
        this.messageCorrespondent = messageCorrespondent;
    }

    public Boolean isMultiLine() {
        return multiLine;
    }

    public void setMultiLine(Boolean multiLine) {
        this.multiLine = multiLine;
    }
}
