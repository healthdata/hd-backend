/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.action.workflow;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.hd4res.domain.ElasticSearchInfo;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationDocument;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.hd4res.factory.IMessageFactory;
import be.wiv_isp.healthdata.hd4res.service.IDataCollectionService;
import be.wiv_isp.healthdata.hd4res.service.IFollowUpService;
import be.wiv_isp.healthdata.hd4res.service.IReadableIdentifierService;
import be.wiv_isp.healthdata.orchestration.action.workflow.ReportableAction;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.domain.mapper.AttachmentMapper;
import be.wiv_isp.healthdata.orchestration.domain.mapper.BooleanConfigurationMapper;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import be.wiv_isp.healthdata.orchestration.mapping.IMappingContextFactory;
import be.wiv_isp.healthdata.orchestration.service.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateForReviewRegistrationAction extends AbstractRegistrationWorkflowAction implements ReportableAction {

	private static final Logger LOG = LoggerFactory.getLogger(CreateForReviewRegistrationAction.class);

	@Autowired
	private IFollowUpService followUpService;
	@Autowired
	private IReadableIdentifierService readableIdentifierService;
	@Autowired
	private IDataCollectionService dataCollectionService;
	@Autowired
	private IDataCollectionDefinitionForwardService catalogueService;
	@Autowired
	private IConfigurationService configurationService;
	@Autowired
	private IElasticSearchInfoService elasticSearchInfoService;
	@Autowired
	private IMappingService mappingService;
	@Autowired
	private IOrganizationService organizationService;
	@Autowired
	private IMessageService messageService;
	@Autowired
	private IMessageFactory messageFactory;
	@Autowired
	private IMappingContextFactory mappingContextFactory;
	@Autowired
	private IAttachmentService attachmentService;
	@Autowired
	private INoteMigrationService noteMigrationService;
	@Autowired
	protected INoteService noteService;

	@Value("${host}")
	private String host;

	@Override
	public WorkflowActionType getAction() {
		return WorkflowActionType.CREATE_FOR_REVIEW;
	}

	@Override
	public RegistrationWorkflow retrieveWorkflow() {
		LOG.debug("Searching for workflow with coded id [{}] and identification value [{}]", hd4dpWorkflowId, identificationWorkflow.getValue());
		final RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		search.setHd4dpWorkflowId(hd4dpWorkflowId);
		search.setIdentificationValue(identificationWorkflow.getValue());

		final RegistrationWorkflow workflow = getWorkflowService().getUnique(search);

		if (workflow == null) {
			LOG.info("No registration found with coded id [{}] and identification value [{}]", hd4dpWorkflowId, identificationWorkflow.getValue());
		}

		return workflow;
	}

	@Override
	public RegistrationWorkflow doExecute(RegistrationWorkflow workflow, HDUserDetails userDetails) {
		final DataCollectionDefinitionDtoV7 dcd = catalogueService.get(dataCollectionDefinitionId);

		if (workflow == null) {
			workflow = createWorkflow(dcd);
		} else {
			workflow.setFlags(flags);
		}

		if (codedData == null) {
			updatePatientIdMetadata(workflow, dcd);
		}

		if(followUps != null) { // old installations do not send follow ups (support for version < Q2)
			followUpService.prepareFollowUpsBeforeSaving(workflow, (List<FollowUp>) followUps);
		}

		setEndStatus(WorkflowStatus.NEW);
		if (!isAuthorized(dcd, identificationWorkflow)) {
			processAuthorizationFailed(workflow);
			return workflow;
		}

		createDocument(workflow, dcd);
		createAttachments(workflow);

		// support for registrations coming from HD4DP <= 1.8 todo: remove in Q1 when all HD4DPs are at version 1.9
		noteMigrationService.migrateCommentsToNotes(workflow, dcd.getContent());

		if (!isValid(dcd, workflow)) {
			processValidationFailed(workflow);
			return workflow;
		}

		try {
			readableIdentifierService.registerOldPatiendId(workflow);
		} catch (Exception e) {
			LOG.error("Couldn't register old patientId for registration [{}]", workflow.getId());
		}

		return workflow;
	}

	private void createAttachments(RegistrationWorkflow workflow) {
		final List<Attachment> attachments = AttachmentMapper.mapToAttachments(this.attachmentDtos);
		attachmentService.linkAttachments(workflow.getDocument(), attachments);
	}

	private void createDocument(RegistrationWorkflow workflow, DataCollectionDefinitionDtoV7 dcd) {
		markNonLoadedDocumentObsolete(workflow);
		final Map<String, TypedValue> readableCodedData = createReadableCodedData(dcd);
		DocumentData documentData = new DocumentData();
		documentData.setContent(documentContent);
		documentData.setCoded(readableCodedData);
		List<DataSource> sourceOfData = createSourceOfData();
		workflowService.setDocumentData(workflow, documentData, sourceOfData, true);
		String csv = mappingService.mapFromJsonToCsv(workflow, mappingContextFactory.create(workflow), dcd.getContent(), false);
		workflow.getDocument().setCsv(csv.getBytes(StandardCharsets.UTF_8));
		workflow.getDocument().setHd4dpVersion(documentVersion);
		workflow.getDocument().setNoteMaxOrder(noteMaxOrder);
		noteService.updateNotesFromMessage(WorkflowType.REGISTRATION, workflow.getDocument().getId(), notesMessage);
	}

	private Map<String, TypedValue> createReadableCodedData(DataCollectionDefinitionDtoV7 dcd) {
		if (codedData == null)
			return new HashMap<>();

		Map<String, TypedValue> readableCodedData = new HashMap<>();
		for (Map.Entry<String, TypedValue> entry : codedData.entrySet()) {
			final String codedKey = entry.getKey();
			TypedValue codedValue = entry.getValue();
			if ("patientID".equals(codedValue.getType())) {
				codedValue.setValue(readableIdentifierService.getReadableSsin(dcd.getDataCollectionName(), codedValue.getValue()));
			}
			readableCodedData.put(codedKey, codedValue);
		}

		return readableCodedData;
	}

	private List<DataSource> createSourceOfData() {
		List<DataSource> sourceOfData = new ArrayList<>();
		if (dataSources != null) {
			for (Map.Entry<String,String> entry : dataSources.entrySet()) {
				DataSource dataSource = new DataSource();
				dataSource.setFieldPath(entry.getKey());
				dataSource.setSource(entry.getValue());
				sourceOfData.add(dataSource);
			}
		}
		return sourceOfData;
	}

	private RegistrationWorkflow createWorkflow(DataCollectionDefinitionDtoV7 dcd) {
		LOG.debug("Registration does not exist, it must be created");
		final RegistrationWorkflow workflow = getWorkflowService().create(organizationService.getMain());

		workflow.setDataCollectionDefinitionId(dataCollectionDefinitionId);
		workflow.setDataCollectionName(dcd.getDataCollectionName());
		workflow.setHd4dpWorkflowId(hd4dpWorkflowId);
		workflow.setSubmittedOn(message.getSentOn());
		final String readableId = readableIdentifierService.createWorkflowName(dcd.getDataCollectionName());
		workflow.setReadableId(readableId);
		if (codedData == null) {
			final String readableSsin = readableIdentifierService.getReadableSsin(dcd.getDataCollectionName(), ehBoxCodedNiss);
			workflow.getMetaData().put(AbstractRegistrationWorkflow.MetaData.CODED_PATIENT_IDENTIFIER, readableSsin);
		}

		final ElasticSearchInfo elasticSearchInfo = new ElasticSearchInfo();
		elasticSearchInfo.setWorkflow(workflow);
		elasticSearchInfo.setStatus(ElasticSearchInfo.Status.NOT_INDEXED);
		elasticSearchInfoService.create(elasticSearchInfo);
		workflow.setIdentificationType(identificationWorkflow.getType());
		workflow.setIdentificationValue(identificationWorkflow.getValue());
		if(identificationWorkflow.getHd4prc() != null) {
			workflow.setHd4prc(identificationWorkflow.getHd4prc());
		} else {
			workflow.setHd4prc(false);
		}

		return workflow;
	}

	private void updatePatientIdMetadata(RegistrationWorkflow workflow, DataCollectionDefinitionDtoV7 dcd) {
		final String readableSsin = readableIdentifierService.getReadableSsin(dcd.getDataCollectionName(), ehBoxCodedNiss);
		final String codedPatientIdentifier = workflow.getMetaData().get(AbstractRegistrationWorkflow.MetaData.CODED_PATIENT_IDENTIFIER);

		if (readableSsin.equals(codedPatientIdentifier)) {
			return;
		}

		final String oldCodedPatientIdentifier = workflow.getMetaData().get(AbstractRegistrationWorkflow.MetaData.OLD_CODED_PATIENT_IDENTIFIER);
		if (StringUtils.isBlank(oldCodedPatientIdentifier)) {
			workflow.getMetaData().put(AbstractRegistrationWorkflow.MetaData.OLD_CODED_PATIENT_IDENTIFIER, codedPatientIdentifier);
		} else {
			workflow.getMetaData().put(AbstractRegistrationWorkflow.MetaData.OLD_CODED_PATIENT_IDENTIFIER, oldCodedPatientIdentifier + "|" + codedPatientIdentifier);
		}
		workflow.getMetaData().put(AbstractRegistrationWorkflow.MetaData.CODED_PATIENT_IDENTIFIER, readableSsin);
	}

	private void markNonLoadedDocumentObsolete(RegistrationWorkflow workflow) {
		if(workflow.getDocumentHistory() == null) {
			return;
		}

		for(RegistrationDocument document : workflow.getDocumentHistory()) {
			if(document.getDwhStatus() == null) {
				document.setDwhStatus(AbstractRegistrationDocument.DwhStatus.O);
			}
		}
	}

	private void processValidationFailed(AbstractRegistrationWorkflow workflow) {
		LOG.error("Validation failed for registration {}", workflow.getId());
		setEndStatus(WorkflowStatus.VALIDATION_FAILED);
	}

	private void processAuthorizationFailed(RegistrationWorkflow workflow) {
		LOG.error("Authorization failed for registration {}", workflow.getId());
		setEndStatus(WorkflowStatus.AUTHORIZATION_FAILED);
		final UpdateStatusRegistrationAction action = new UpdateStatusRegistrationAction();
		action.setEndStatus(WorkflowStatus.UNAUTHORIZED);
		final Message message = messageFactory.createOutgoingMessage(action, workflow, this.message);
		messageService.create(message);
	}

	private boolean isValid(DataCollectionDefinitionDtoV7 dcd, AbstractRegistrationWorkflow workflow) {
		boolean useValidation = BooleanConfigurationMapper.map(configurationService.get(ConfigurationKey.USE_VALIDATION));

		if (useValidation) {
			return mappingService.isValid(workflow, mappingContextFactory.create(workflow), dcd.getContent());
		}

		return true;
	}

	private boolean isAuthorized(DataCollectionDefinitionDtoV7 dcd, HealthDataIdentification identification) {
		boolean useSalesForce = BooleanConfigurationMapper.map(configurationService.get(ConfigurationKey.USE_SALESFORCE));

		if (useSalesForce) {
			return dataCollectionService.accept(dcd.getDataCollectionName(), identification.getType(), identification.getValue());
		}

		return true;
	}

	@Override
	public void extractInfo(Message message) {
		final CSVContent csvContent = new CSVContent(message.getContent());
		this.hd4dpWorkflowId = csvContent.getWorkflowId();
		if (csvContent.isUseDescriptors()) {
			this.codedData = new HashMap<>();
			for (int i = 0; i < csvContent.getSize() - 1; i++) {
				final String fieldName = csvContent.getFieldName(i);
				if (csvContent.isTechnicalField(i) || csvContent.isDummyField(i))
					continue;
				this.codedData.put(fieldName, new TypedValue(csvContent.getField(i), csvContent.getFieldType(i)));
			}
			this.ehBoxCodedNiss = null;
			if (this.codedData.size() == 1 && this.codedData.containsKey(CSVContent.PATIENT_IDENTIFIER)) { // old format
				this.ehBoxCodedNiss = csvContent.getPatientId();
				this.codedData = null;
			}
		}
		else {
			this.ehBoxCodedNiss = csvContent.getPatientId();
			this.codedData = null;
		}

		this.message = message;
	}

	@Override
	public WorkflowType getWorkflowType() {
		return WorkflowType.REGISTRATION;
	}

	@Override
	public String getSource() {
		return source;
	}

	@Override
	public String getFile() {
		return file;
	}

	@Override
	public String getMessageType() {
		return messageType;
	}

	@Override
	public Progress getProgress() {
		return null;
	}

}
