/*
 *  Copyright 2014-2017 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package be.wiv_isp.healthdata.hd4res.domain.dcd;


import be.wiv_isp.healthdata.common.util.StringUtils;
import be.wiv_isp.healthdata.hd4res.domain.Description;
import be.wiv_isp.healthdata.hd4res.domain.Label;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "DATA_COLLECTION_GROUPS")
public class DataCollectionGroup implements Serializable {

	private static final long serialVersionUID = -3770857148167696369L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "DATA_COLLECTION_GROUPS_ID")
	private Long id;

	@Column(name = "NAME", nullable = false)
	private String name;

	@Column(name = "HISTORY_ID")
	private Long historyId;

	@Embedded
	private Label label;

	@Embedded
	private Description description;

	@Column(name = "MAJOR_VERSION", nullable = false)
	private Integer majorVersion;

	@Column(name = "PUBLISHED", nullable = false)
	private boolean published;

	@Embedded
	private Period period;

	@Column(name = "START_DATE")
	private Timestamp startDate;

	@Column(name = "END_DATE_CREATION")
	private Timestamp endDateCreation;

	@Column(name = "END_DATE_SUBMISSION")
	private Timestamp endDateSubmission;

	@Column(name = "END_DATE_COMMENTS")
	private Timestamp endDateComments;

	@Column(name = "CREATED_ON", nullable = false)
	private Timestamp createdOn;

	@Column(name = "UPDATED_ON", nullable = false)
	private Timestamp updatedOn;

	public DataCollectionGroup() {
	}

	@PrePersist
	public void onCreate() {
		long currentTimeWithoutMilliseconds = new Date().getTime();
		setCreatedOn(new Timestamp(currentTimeWithoutMilliseconds));
		setUpdatedOn(new Timestamp(currentTimeWithoutMilliseconds));
	}

	@PreUpdate
	public void onUpdate() {
		long currentTimeWithoutMilliseconds = new Date().getTime();
		setUpdatedOn(new Timestamp(currentTimeWithoutMilliseconds));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getHistoryId() {
		return historyId;
	}

	public void setHistoryId(Long historyId) {
		this.historyId = historyId;
	}

	public Label getLabel() {
		return label;
	}

	public void setLabel(Label label) {
		this.label = label;
	}

	public Description getDescription() {
		return description;
	}

	public void setDescription(Description description) {
		this.description = description;
	}

	public Integer getMajorVersion() {
		return majorVersion;
	}

	public void setMajorVersion(Integer majorVersion) {
		this.majorVersion = majorVersion;
	}

	public boolean isPublished() {
		return published;
	}

	public void setPublished(boolean published) {
		this.published = published;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDateCreation() {
		return endDateCreation;
	}

	public void setEndDateCreation(Timestamp endDateCreation) {
		this.endDateCreation = endDateCreation;
	}

	public Timestamp getEndDateSubmission() {
		return endDateSubmission;
	}

	public void setEndDateSubmission(Timestamp endDateSubmission) {
		this.endDateSubmission = endDateSubmission;
	}

	public Timestamp getEndDateComments() {
		return endDateComments;
	}

	public void setEndDateComments(Timestamp endDateComments) {
		this.endDateComments = endDateComments;
	}

	public boolean isValidForCreation(Date date) {
		return isBetween(startDate, date, endDateCreation);
	}

	public boolean isValidForSubmission(Date date) {
		return isBetween(startDate, date, endDateSubmission);
	}

	public boolean isValidForComments(Date date) {
		return isBetween(startDate, date, endDateComments);
	}

	private boolean isBetween(Date start, Date middle, Date end) {
		return start.before(middle) && middle.before(end);
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}


	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof DataCollectionGroup) {
			DataCollectionGroup other = (DataCollectionGroup) o;

			return Objects.equals(id, other.id)
					&& Objects.equals(name, other.name)
					&& Objects.equals(label, other.label)
					&& Objects.equals(description, other.description)
					&& Objects.equals(majorVersion, majorVersion)
					&& Objects.equals(startDate, other.startDate)
					&& Objects.equals(endDateCreation, other.endDateCreation)
					&& Objects.equals(endDateSubmission, other.endDateSubmission)
					&& Objects.equals(endDateComments, other.endDateComments)
					&& Objects.equals(createdOn, other.createdOn)
					&& Objects.equals(updatedOn, other.updatedOn);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				this.id,
				this.name,
				this.label,
				this.description,
				this.majorVersion,
				this.startDate,
				this.endDateCreation,
				this.endDateSubmission,
				this.endDateComments,
				this.createdOn,
				this.updatedOn);
	}

	@Override
	public String toString() {
		return StringUtils.toStringExclude(this);
	}
}
