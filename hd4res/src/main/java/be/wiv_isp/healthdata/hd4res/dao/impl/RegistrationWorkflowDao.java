/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.dao.impl.AbstractRegistrationWorkflowDao;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class RegistrationWorkflowDao extends AbstractRegistrationWorkflowDao<RegistrationWorkflow, RegistrationWorkflowSearch> {

	public RegistrationWorkflowDao() {
		super(RegistrationWorkflow.class);
	}

	@Override
	protected List<Predicate> getPredicates(RegistrationWorkflowSearch search, CriteriaBuilder cb, Root<RegistrationWorkflow> rootEntry) {
		final List<Predicate> predicates = super.getPredicates(search, cb, rootEntry);

		if (search.getReadableId() != null) {
			predicates.add(cb.equal(rootEntry.get("readableId"), search.getReadableId()));
		}
		if (search.getHd4dpWorkflowId() != null) {
			predicates.add(cb.equal(rootEntry.get("hd4dpWorkflowId"), search.getHd4dpWorkflowId()));
		}
		if (search.getIdentificationValue() != null) {
			predicates.add(cb.equal(rootEntry.get("identificationValue"), search.getIdentificationValue()));
		}

		return predicates;
	}
}
