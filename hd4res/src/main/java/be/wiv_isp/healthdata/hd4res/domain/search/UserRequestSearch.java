/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain.search;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.orchestration.domain.search.AbstractUserRequestSearch;

import javax.ws.rs.core.UriInfo;
import java.util.Objects;

public class UserRequestSearch extends AbstractUserRequestSearch {

    private String dataCollectionName;
    private boolean displayArchived = false;
    private HealthDataIdentification healthDataIdentification;
    private Long hd4dpId;

    public UserRequestSearch() {

    }

    public UserRequestSearch(UriInfo info) {
        super(info);
        displayArchived = Boolean.TRUE.equals(RestUtils.getParameterBoolean(info, "displayArchived"));
        dataCollectionName = RestUtils.getParameterString(info, "dataCollectionName");
        healthDataIdentification = new HealthDataIdentification();
        healthDataIdentification.setType(RestUtils.getParameterString(info, "identificationType"));
        healthDataIdentification.setValue(RestUtils.getParameterString(info, "identificationValue"));
        healthDataIdentification.setName(RestUtils.getParameterString(info, "identificationName"));
    }

    public String getDataCollectionName() {
        return dataCollectionName;
    }

    public void setDataCollectionName(String dataCollectionName) {
        this.dataCollectionName = dataCollectionName;
    }

    public boolean isDisplayArchived() {
        return displayArchived;
    }

    public void setDisplayArchived(boolean displayArchived) {
        this.displayArchived = displayArchived;
    }

    public HealthDataIdentification getHealthDataIdentification() {
        return healthDataIdentification;
    }

    public void setHealthDataIdentification(HealthDataIdentification healthDataIdentification) {
        this.healthDataIdentification = healthDataIdentification;
    }

    public Long getHd4dpId() {
        return hd4dpId;
    }

    public void setHd4dpId(Long hd4dpId) {
        this.hd4dpId = hd4dpId;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof UserRequestSearch) {
            UserRequestSearch other = (UserRequestSearch) o;

            return Objects.equals(firstName, other.firstName)
                    && Objects.equals(lastName, other.lastName)
                    && Objects.equals(email, other.email)
                    && Objects.equals(approved, other.approved)
                    && Objects.equals(healthDataIdentification, other.healthDataIdentification)
                    && Objects.equals(hd4dpId, other.hd4dpId)
                    && Objects.equals(displayArchived, other.displayArchived);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                this.firstName,
                this.lastName,
                this.email,
                this.approved,
                this.healthDataIdentification,
                this.hd4dpId,
                this.displayArchived);
    }

    @Override
    public String toString() {
        return "UserRequest {" +
                "firstName = " + Objects.toString(this.firstName) + ", " +
                "lastName = " + Objects.toString(this.lastName) + ", " +
                "email = " + Objects.toString(this.email) + ", " +
                "approved = " + Objects.toString(this.approved) + ", " +
                "healthDataIdentification = " + Objects.toString(this.healthDataIdentification) + ", " +
                "hd4dpId = " + Objects.toString(this.hd4dpId) + ", " +
                "displayArchived = " + Objects.toString(this.displayArchived) + "}";
    }
}
