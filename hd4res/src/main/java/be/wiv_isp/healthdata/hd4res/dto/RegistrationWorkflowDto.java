/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dto;

import be.wiv_isp.healthdata.hd4res.domain.MappingContext;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationDocument;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.dto.AbstractRegistrationWorkflowDto;

public class RegistrationWorkflowDto extends AbstractRegistrationWorkflowDto<RegistrationWorkflow, RegistrationDocument, MappingContext> {

	private String readableId;
	private String hd4dpWorkflowId;
	private String identificationType;
	private String identificationValue;

	public RegistrationWorkflowDto() {

	}

	public RegistrationWorkflowDto(RegistrationWorkflow workflow) {
		super(workflow);
		if (workflow.getDocument().getCodedContent() != null && !workflow.getDocument().getCodedContent().isEmpty())
			metaData = null; // do not return metadata
		setReadableId(workflow.getReadableId());
		setHd4dpWorkflowId(workflow.getHd4dpWorkflowId());
		setIdentificationType(workflow.getIdentificationType());
		setIdentificationValue(workflow.getIdentificationValue());
	}

	public RegistrationWorkflowDto(RegistrationWorkflowDto dto) {
		super(dto);
		readableId = dto.getReadableId();
		hd4dpWorkflowId = dto.getHd4dpWorkflowId();
		identificationType = dto.getIdentificationType();
		identificationValue = dto.getIdentificationValue();
	}

	public String getReadableId() {
		return readableId;
	}

	public void setReadableId(String readableId) {
		this.readableId = readableId;
	}

	public String getHd4dpWorkflowId() {
		return hd4dpWorkflowId;
	}

	public void setHd4dpWorkflowId(String hd4dpWorkflowId) {
		this.hd4dpWorkflowId = hd4dpWorkflowId;
	}

	public String getIdentificationValue() {
		return identificationValue;
	}

	public void setIdentificationValue(String identificationValue) {
		this.identificationValue = identificationValue;
	}

	public String getIdentificationType() {
		return identificationType;
	}

	public void setIdentificationType(String identificationType) {
		this.identificationType = identificationType;
	}

}