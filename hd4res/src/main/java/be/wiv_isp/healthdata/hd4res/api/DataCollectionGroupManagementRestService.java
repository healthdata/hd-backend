/*
 *  Copyright 2014-2017 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package be.wiv_isp.healthdata.hd4res.api;

import be.wiv_isp.healthdata.hd4res.dto.DataCollectionGroupManagementDto;
import be.wiv_isp.healthdata.hd4res.service.IDataCollectionGroupManagementService;
import be.wiv_isp.healthdata.orchestration.annotation.Auditable;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ApiType;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Component
@Path("/datacollectiongroups/manager")
public class DataCollectionGroupManagementRestService {

	@Autowired
	private IDataCollectionGroupManagementService dataCollectionGroupManagementService;

	@Transactional
	@GET
	@Path("/{dataCollectionGroupId}")
	@PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION_MANAGER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("dataCollectionGroupId") Long dataCollectionGroupId) throws Exception {
		final DataCollectionGroupManagementDto dataCollectionGroup = dataCollectionGroupManagementService.get(dataCollectionGroupId);
		return Response.ok(dataCollectionGroup).build();
	}

	@Transactional
	@GET
	@PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION_MANAGER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() throws Exception {
		final List<DataCollectionGroupManagementDto> dataCollectionGroups = dataCollectionGroupManagementService.getAll();
		return Response.ok(dataCollectionGroups).build();
	}

	@Transactional
	@POST
	@PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION_MANAGER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(String json) throws Exception {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		final HDUserDetails principal = (HDUserDetails) authentication.getPrincipal();
		final DataCollectionGroupManagementDto dataCollectionGroup = dataCollectionGroupManagementService.create(new JSONObject(json), principal.getUsername());
		return Response.ok(dataCollectionGroup).build();
	}

	@Transactional
	@PUT
	@Path("/{dataCollectionGroupId}")
	@PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION_MANAGER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("dataCollectionGroupId") Long dataCollectionGroupId, String json) throws Exception {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		final HDUserDetails principal = (HDUserDetails) authentication.getPrincipal();
		final DataCollectionGroupManagementDto dataCollectionGroup = dataCollectionGroupManagementService.update(dataCollectionGroupId, new JSONObject(json), principal.getUsername());
		return Response.ok(dataCollectionGroup).build();
	}

	@Transactional
	@DELETE
	@Path("/{dataCollectionGroupId}")
	@PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION_MANAGER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("dataCollectionGroupId") Long dataCollectionGroupId) {
		dataCollectionGroupManagementService.delete(dataCollectionGroupId);
		return Response.noContent().build();
	}
	
	@Transactional
	@POST
	@Path("/{dataCollectionGroupId}/publish")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION_MANAGER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response publish(@PathParam("dataCollectionGroupId") Long dataCollectionGroupId) throws Exception {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		final HDUserDetails principal = (HDUserDetails) authentication.getPrincipal();
		dataCollectionGroupManagementService.publish(dataCollectionGroupId, principal.getUsername());
		return Response.noContent().build();
	}

	@Transactional
	@POST
	@Path("/{dataCollectionGroupId}/republish")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION_MANAGER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response republish(@PathParam("dataCollectionGroupId") Long dataCollectionGroupId) throws Exception {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		final HDUserDetails principal = (HDUserDetails) authentication.getPrincipal();
		final DataCollectionGroupManagementDto dataCollectionGroup = dataCollectionGroupManagementService.republish(dataCollectionGroupId, principal.getUsername());
		return Response.ok(dataCollectionGroup).build();
	}
}
