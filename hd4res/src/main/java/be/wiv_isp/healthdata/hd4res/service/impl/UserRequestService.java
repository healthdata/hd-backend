/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.common.api.exception.ForbiddenException;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.hd4res.domain.UserRequest;
import be.wiv_isp.healthdata.hd4res.domain.search.UserRequestSearch;
import be.wiv_isp.healthdata.hd4res.service.IUserRequestService;
import be.wiv_isp.healthdata.orchestration.dao.IUserRequestDao;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.service.IUserDataCollectionService;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
public class UserRequestService extends AbstractService<UserRequest, Long, UserRequestSearch, IUserRequestDao<UserRequest, UserRequestSearch>> implements IUserRequestService {

    @Autowired
    protected IUserRequestDao<UserRequest, UserRequestSearch> dao;

    @Autowired
    private IUserDataCollectionService userDataCollectionService;

    public UserRequestService() {
        super(UserRequest.class);
    }

    @Override
    public IUserRequestDao<UserRequest, UserRequestSearch> getDao() {
        return dao;
    }

    @Override
    public List<UserRequest> getAll(UserRequestSearch search) {
        verifyDataCollectionAccess(search.getDataCollectionName());
        return dao.getAll(search);
    }

    @Override
    public void archive(Long id, String dataCollectionName) {
        final UserRequest userRequest = dao.get(id);
        if(userRequest == null) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "USER_REQUEST", id);
            throw exception;
        }

        if(!userRequest.getDataCollectionNames().contains(dataCollectionName)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_USER_REQUEST_PROPERTY, dataCollectionName);
            throw exception;
        }

        verifyDataCollectionAccess(dataCollectionName);

        Set<String> archivedDataCollectionNames = userRequest.getArchivedDataCollectionNames();
        if(archivedDataCollectionNames == null) {
            archivedDataCollectionNames = new HashSet<>();
        }
        archivedDataCollectionNames.add(dataCollectionName);
        userRequest.setArchivedDataCollectionNames(archivedDataCollectionNames);
        dao.update(userRequest);
    }

    private void verifyDataCollectionAccess(String dataCollectionName) {
        if(dataCollectionName != null) {
            final HDUserDetails principal = AuthenticationService.getUserDetails();
            if (!userDataCollectionService.isUserAuthorized(principal, dataCollectionName)) {
                throw new ForbiddenException();

            }
        }
    }

    @Override
    public UserRequest get(Long hd4dpId, HealthDataIdentification healthDataIdentification) {
        final HealthDataIdentification searchHealthDataIdentification = new HealthDataIdentification();
        searchHealthDataIdentification.setType(healthDataIdentification.getType());
        searchHealthDataIdentification.setValue(healthDataIdentification.getValue());

        final UserRequestSearch search = new UserRequestSearch();
        search.setHd4dpId(hd4dpId);
        search.setHealthDataIdentification(searchHealthDataIdentification);
        search.setDisplayArchived(true);

        return getUnique(search);

    }
}
