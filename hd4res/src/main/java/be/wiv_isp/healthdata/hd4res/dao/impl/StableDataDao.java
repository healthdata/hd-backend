/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.hd4res.dao.IStableDataDao;
import be.wiv_isp.healthdata.hd4res.domain.StableData;
import be.wiv_isp.healthdata.hd4res.domain.StableDataCountResult;
import be.wiv_isp.healthdata.hd4res.domain.search.StableDataSearch;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class StableDataDao extends CrudDaoV2<StableData, Long, StableDataSearch> implements IStableDataDao {

    public StableDataDao() {
        super(StableData.class);
    }

    @Override
    public List<StableDataCountResult> getCounts(StableDataSearch search) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<StableDataCountResult> cq = cb.createQuery(StableDataCountResult.class);
        Root<StableData> rootEntry = cq.from(StableData.class);

        cq.multiselect(rootEntry.get("identificationValue"), rootEntry.get("dataCollectionName"), rootEntry.get("status"), cb.count(rootEntry))
                .groupBy(rootEntry.get("identificationValue"), rootEntry.get("dataCollectionName"), rootEntry.get("status"));

        final List<Predicate> predicates = getPredicates(search, cb, rootEntry);
        if(CollectionUtils.isNotEmpty(predicates)) {
            cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
        }

        return em.createQuery(cq).getResultList();
    }

    protected List<Predicate> getPredicates(StableDataSearch search, CriteriaBuilder cb, Root<StableData> rootEntry) {
        if(search == null) {
            return null;
        }

        final List<Predicate> predicates = new ArrayList<>();
        if (search.getStatus() != null) {
            predicates.add(cb.equal(rootEntry.get("status"), search.getStatus()));
        }
        if (search.getDataCollectionName() != null) {
            predicates.add(cb.equal(rootEntry.get("dataCollectionName"), search.getDataCollectionName()));
        }
        if (search.getIdentificationType() != null) {
            predicates.add(cb.equal(rootEntry.get("identificationType"), search.getIdentificationType()));
        }
        if (search.getIdentificationValue() != null) {
            predicates.add(cb.equal(rootEntry.get("identificationValue"), search.getIdentificationValue()));
        }
        return predicates;
    }
}
