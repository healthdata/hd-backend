/*
 *  Copyright 2014-2017 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.hd4res.dto.converters;


import be.wiv_isp.healthdata.common.dto.PeriodDto;
import be.wiv_isp.healthdata.common.dto.TranslatableStringDto;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionDefinition;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionGroup;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionGroupHistory;
import be.wiv_isp.healthdata.hd4res.dto.DataCollectionDefinitionManagementDto;
import be.wiv_isp.healthdata.hd4res.dto.DataCollectionGroupManagementDto;

import java.util.ArrayList;
import java.util.List;

public class DataCollectionGroupManagementDtoConverter {

    public static DataCollectionGroupManagementDto convert(DataCollectionGroup dataCollectionGroup, DataCollectionGroupHistory history, List<DataCollectionDefinition> dataCollectionDefinitions) {
        DataCollectionGroupManagementDto convert = convert(dataCollectionGroup, history, true);
        List<DataCollectionDefinitionManagementDto> definitionDtos = new ArrayList<>();
        for (DataCollectionDefinition dataCollectionDefinition : dataCollectionDefinitions) {
            definitionDtos.add(DataCollectionDefinitionManagementDtoConverter.convert(dataCollectionDefinition, null, null));
        }
        convert.setDataCollectionDefinitions(definitionDtos);
        return convert;
    }

    public static DataCollectionGroupManagementDto convert(DataCollectionGroup dataCollectionGroup, DataCollectionGroupHistory history, boolean withContent) {
        DataCollectionGroupManagementDto dataCollectionGroupManagementDto = null;

        if (dataCollectionGroup != null) {
            dataCollectionGroupManagementDto = new DataCollectionGroupManagementDto();
            dataCollectionGroupManagementDto.setDataCollectionGroupId(dataCollectionGroup.getId());
            dataCollectionGroupManagementDto.setDataCollectionGroupName(dataCollectionGroup.getName());
            TranslatableStringDto label = new TranslatableStringDto();
            label.setEn(dataCollectionGroup.getLabel().getEn());
            label.setFr(dataCollectionGroup.getLabel().getFr());
            label.setNl(dataCollectionGroup.getLabel().getNl());
            dataCollectionGroupManagementDto.setLabel(label);
            TranslatableStringDto description = new TranslatableStringDto();
            description.setEn(dataCollectionGroup.getDescription().getEn());
            description.setFr(dataCollectionGroup.getDescription().getFr());
            description.setNl(dataCollectionGroup.getDescription().getNl());
            dataCollectionGroupManagementDto.setDescription(description);
            if (withContent && history != null) {
                dataCollectionGroupManagementDto.setParticipationContent(history.getParticipationContent());
                dataCollectionGroupManagementDto.setUserDescription(history.getDescription());
            }
            dataCollectionGroupManagementDto.setMajorVersion(dataCollectionGroup.getMajorVersion());
            dataCollectionGroupManagementDto.setPublished(dataCollectionGroup.isPublished());
            dataCollectionGroupManagementDto.setCreatedOn(dataCollectionGroup.getCreatedOn());
            dataCollectionGroupManagementDto.setUpdatedOn(dataCollectionGroup.getUpdatedOn());
            dataCollectionGroupManagementDto.setStartDate(dataCollectionGroup.getStartDate());
            dataCollectionGroupManagementDto.setEndDateCreation(dataCollectionGroup.getEndDateCreation());
            dataCollectionGroupManagementDto.setEndDateComments(dataCollectionGroup.getEndDateComments());
            dataCollectionGroupManagementDto.setEndDateSubmission(dataCollectionGroup.getEndDateSubmission());
            if (dataCollectionGroup.getPeriod() != null) {
                PeriodDto periodDto = new PeriodDto(dataCollectionGroup.getPeriod().getStart(), dataCollectionGroup.getPeriod().getEnd());
                dataCollectionGroupManagementDto.setPeriod(periodDto);
            }
        }
        return dataCollectionGroupManagementDto;
    }
    
    
}
