/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain.search;

import be.wiv_isp.healthdata.common.domain.search.EntitySearch;
import be.wiv_isp.healthdata.common.util.StringUtils;

public class DataCollectionDefinitionSearch extends EntitySearch {

	private String dataCollectionDefinitionName;
	private Long dataCollectionGroupId;
	private Integer minorVersion;
	private Boolean published;

	public String getDataCollectionDefinitionName() {
		return dataCollectionDefinitionName;
	}

	public void setDataCollectionDefinitionName(String dataCollectionName) {
		this.dataCollectionDefinitionName = dataCollectionName;
	}

	public Long getDataCollectionGroupId() {
		return dataCollectionGroupId;
	}

	public void setDataCollectionGroupId(Long dataCollectionGroupId) {
		this.dataCollectionGroupId = dataCollectionGroupId;
	}

	public Integer getMinorVersion() {
		return minorVersion;
	}

	public void setMinorVersion(Integer minorVersion) {
		this.minorVersion = minorVersion;
	}

	public Boolean getPublished() {
		return published;
	}

	public void setPublished(Boolean published) {
		this.published = published;
	}

	@Override
	public String toString() {
		return StringUtils.toString(this);
	}
}
