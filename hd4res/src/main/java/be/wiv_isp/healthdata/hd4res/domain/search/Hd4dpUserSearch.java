/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain.search;

import be.wiv_isp.healthdata.common.domain.search.EntitySearch;
import be.wiv_isp.healthdata.hd4res.domain.Hd4dpAuthority;

public class Hd4dpUserSearch extends EntitySearch {

    private Long hd4dpId;
    private String healthDataIdentificationType;
    private String healthDataIdentificationValue;
    private String dataCollectionName;
    private Hd4dpAuthority authority;

    public Long getHd4dpId() {
        return hd4dpId;
    }

    public void setHd4dpId(Long hd4dpId) {
        this.hd4dpId = hd4dpId;
    }

    public String getHealthDataIdentificationType() {
        return healthDataIdentificationType;
    }

    public void setHealthDataIdentificationType(String healthDataIdentificationType) {
        this.healthDataIdentificationType = healthDataIdentificationType;
    }

    public String getHealthDataIdentificationValue() {
        return healthDataIdentificationValue;
    }

    public void setHealthDataIdentificationValue(String healthDataIdentificationValue) {
        this.healthDataIdentificationValue = healthDataIdentificationValue;
    }

    public String getDataCollectionName() {
        return dataCollectionName;
    }

    public void setDataCollectionName(String dataCollectionName) {
        this.dataCollectionName = dataCollectionName;
    }

    public Hd4dpAuthority getAuthority() {
        return authority;
    }

    public void setAuthority(Hd4dpAuthority authority) {
        this.authority = authority;
    }
}
