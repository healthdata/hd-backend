/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.hd4res.domain.ElasticSearchInfo;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationDocument;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractRegistrationWorkflowService;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service
public class RegistrationWorkflowService extends AbstractRegistrationWorkflowService<RegistrationWorkflow, RegistrationWorkflowSearch, RegistrationDocument, ElasticSearchInfo> {

	public RegistrationWorkflowService() {
		super(RegistrationWorkflow.class);
	}

	@Override
	public RegistrationWorkflow getEntityInstance() {
		return new RegistrationWorkflow();
	}

	@Override
	public RegistrationWorkflowSearch getSearchInstance() {
		return new RegistrationWorkflowSearch();
	}

	@Override
	protected Platform getPlatform() {
		return Platform.HD4RES;
	}

	@Override
	public boolean isActionAvailable(RegistrationWorkflow workflow, WorkflowActionType action) {
		if(!super.isActionAvailable(workflow, action)) {
			return false;
		}
		if(WorkflowActionType.SUBMIT_ANNOTATIONS.equals(action)) {
			return isSubmittable(workflow);
		}
		return true;
	}

	private boolean isSubmittable(RegistrationWorkflow workflow) {
		final Long dcdId = workflow.getDataCollectionDefinitionId();
		final DataCollectionDefinitionDtoV7 dcd = catalogueService.get(dcdId);
		return dcd.getDataCollectionGroup().isValidForComments(new Date());
	}
}