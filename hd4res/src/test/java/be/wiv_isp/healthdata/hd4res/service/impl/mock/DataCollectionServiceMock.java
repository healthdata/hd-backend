/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl.mock;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.common.dto.SalesForceOrganizationDto;
import be.wiv_isp.healthdata.hd4res.service.IDataCollectionService;
import be.wiv_isp.healthdata.orchestration.domain.DataCollection;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class DataCollectionServiceMock implements IDataCollectionService {

	@Override
	public Map<DataCollection, Set<DataCollection>> getDataCollections() {
		return null;
	}

	@Override
	public Set<DataCollection> getDataCollectionGroups() {
		return null;
	}

	@Override
	public Set<DataCollection> getDataCollectionsFlat() {
		return null;
	}

	@Override
	public Map<String, Set<String>> getDataCollections(Organization organization) {
		return null;
	}

	@Override
	public Set<String> getDataCollectionGroups(Organization organization) {
		return null;
	}

	@Override
	public Set<String> getDataCollectionsFlat(Organization organization) {
		return null;
	}

	@Override
	public Map<String, Set<String>> getPublicDataCollections() {
		return null;
	}

	@Override
	public Set<String> getPublicDataCollectionGroups() {
		return null;
	}

	@Override
	public String getGroup(String dataCollectionName) {
		return null;
	}

	@Override
	public boolean accept(String registerName, String identificationType, String identificationValue) {
		return false;
	}

	@Override
	public List<SalesForceOrganizationDto> getAuthorizedOrganizations(String dataCollectionName) {
		return null;
	}

	@Override
	public List<SalesForceOrganizationDto> getAllOrganizations() {
		return null;
	}

	@Override
	public Set<String> getMembers(String groupName) {
		return null;
	}
}
