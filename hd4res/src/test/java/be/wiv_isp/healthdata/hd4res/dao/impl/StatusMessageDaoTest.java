/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.hd4res.domain.StatusMessage;
import be.wiv_isp.healthdata.orchestration.action.StatusMessageContent;
import be.wiv_isp.healthdata.orchestration.dao.IStatusMessageDao;
import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.orchestration.domain.search.StatusMessageSearch;
import org.codehaus.jettison.json.JSONException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class StatusMessageDaoTest {

	public static final HealthDataIdentification HEALTH_DATA_IDENTIFICATION_1 = new HealthDataIdentification();
	public static final HealthDataIdentification HEALTH_DATA_IDENTIFICATION_2 = new HealthDataIdentification();
	public static final HealthDataIdentification HEALTH_DATA_IDENTIFICATION_3 = new HealthDataIdentification();

	static {
		HEALTH_DATA_IDENTIFICATION_1.setType("HD_ID_TYPE_1");
		HEALTH_DATA_IDENTIFICATION_1.setValue("HD_ID_VALUE_1");
		HEALTH_DATA_IDENTIFICATION_1.setName("NAME_1");
		HEALTH_DATA_IDENTIFICATION_1.setHd4prc(false);

		HEALTH_DATA_IDENTIFICATION_2.setType("HD_ID_TYPE_2");
		HEALTH_DATA_IDENTIFICATION_2.setValue("HD_ID_VALUE_2");
		HEALTH_DATA_IDENTIFICATION_2.setName("NAME_2");
		HEALTH_DATA_IDENTIFICATION_2.setHd4prc(false);

		HEALTH_DATA_IDENTIFICATION_3.setType("HD_ID_TYPE_3");
		HEALTH_DATA_IDENTIFICATION_3.setValue("HD_ID_VALUE_3");
		HEALTH_DATA_IDENTIFICATION_3.setName("NAME_3");
		HEALTH_DATA_IDENTIFICATION_3.setHd4prc(false);
	}
	private static final long _24_HOURS_AS_MILLIS = 24L * 3600L * 1000L;

	@Autowired
	private IStatusMessageDao<StatusMessage> dao;

	private StatusMessage statusMessage1;
	private StatusMessage statusMessage2;
	private StatusMessage statusMessage3;

	@Before
	public void setUp() throws JSONException {
		statusMessage1 = new StatusMessage();
		statusMessage1.setHealthDataIdentification(HEALTH_DATA_IDENTIFICATION_1);
		statusMessage1.setContent(StatusMessageContent.create("{ \"content\" : \"content_1\"}"));
		statusMessage1.setSentOn(new Timestamp(new Date().getTime()));

		statusMessage2 = new StatusMessage();
		statusMessage2.setHealthDataIdentification(HEALTH_DATA_IDENTIFICATION_2);
		statusMessage2.setContent(StatusMessageContent.create("{ \"content\" : \"content_2\"}"));
		statusMessage2.setSentOn(new Timestamp(new Date().getTime()));

		statusMessage3 = new StatusMessage();
		statusMessage3.setHealthDataIdentification(HEALTH_DATA_IDENTIFICATION_3);
		statusMessage3.setContent(StatusMessageContent.create("{ \"content\" : \"content_3\"}"));
		statusMessage3.setSentOn(new Timestamp(new Date().getTime()));

		statusMessage1 = dao.create(statusMessage1);
		statusMessage2 = dao.create(statusMessage2);
		statusMessage3 = dao.create(statusMessage3);
	}

	@Test
	public void testGetAllWithLastOnly() throws JSONException {
		StatusMessage statusMessage = new StatusMessage();
		statusMessage.setHealthDataIdentification(HEALTH_DATA_IDENTIFICATION_1);
		statusMessage.setContent(StatusMessageContent.create("{ \"content\" : \"content_1\"}"));
		statusMessage.setSentOn(new Timestamp(new Date().getTime()));
		statusMessage = dao.create(statusMessage);

		final StatusMessageSearch search = new StatusMessageSearch();
		search.setLastOnly(true);

		final List<StatusMessage> statusMessages = dao.getAll(search);
		Assert.assertEquals(3, statusMessages.size());
		Assert.assertFalse(statusMessages.contains(statusMessage1));
		Assert.assertTrue(statusMessages.contains(statusMessage));
		Assert.assertTrue(statusMessages.contains(statusMessage2));
		Assert.assertTrue(statusMessages.contains(statusMessage3));
	}

	@Test
	 public void testGetAllWithCreatedOnTodayReturnsAll() {
		final StatusMessageSearch search = new StatusMessageSearch();
		search.setCreatedOn(new Date());

		final List<StatusMessage> statusMessages = dao.getAll(search);
		Assert.assertEquals(3, statusMessages.size());
		Assert.assertTrue(statusMessages.contains(statusMessage1));
		Assert.assertTrue(statusMessages.contains(statusMessage2));
		Assert.assertTrue(statusMessages.contains(statusMessage3));
	}

	@Test
	public void testGetAllWithCreatedOnYesterdayReturnsNothing() {
		final StatusMessageSearch search = new StatusMessageSearch();
		search.setCreatedOn(new Date(new Date().getTime() - _24_HOURS_AS_MILLIS));

		final List<StatusMessage> statusMessages = dao.getAll(search);
		Assert.assertEquals(0, statusMessages.size());
		Assert.assertFalse(statusMessages.contains(statusMessage1));
		Assert.assertFalse(statusMessages.contains(statusMessage2));
		Assert.assertFalse(statusMessages.contains(statusMessage3));
	}

	@Test
	public void testGetAllWithCreatedOnTomorrowReturnsNothing() {
		final StatusMessageSearch search = new StatusMessageSearch();
		search.setCreatedOn(new Date(new Date().getTime() + _24_HOURS_AS_MILLIS));

		final List<StatusMessage> statusMessages = dao.getAll(search);
		Assert.assertEquals(0, statusMessages.size());
		Assert.assertFalse(statusMessages.contains(statusMessage1));
		Assert.assertFalse(statusMessages.contains(statusMessage2));
		Assert.assertFalse(statusMessages.contains(statusMessage3));
	}

	@Test
	public void testGetAllWithHealthDataIdentification() {
		final StatusMessageSearch search = new StatusMessageSearch();
		search.setHealthDataIdentification(HEALTH_DATA_IDENTIFICATION_1);

		final List<StatusMessage> statusMessages = dao.getAll(search);
		Assert.assertEquals(1, statusMessages.size());
		Assert.assertTrue(statusMessages.contains(statusMessage1));
		Assert.assertFalse(statusMessages.contains(statusMessage2));
		Assert.assertFalse(statusMessages.contains(statusMessage3));
	}

	@Test
	public void testGetAllWithCreatedOnAfter() throws Exception{
		Thread.sleep(1000);
		final StatusMessageSearch search = new StatusMessageSearch();
		search.setCreatedOnAfter(new Date());

		StatusMessage statusMessage = new StatusMessage();
		statusMessage.setHealthDataIdentification(HEALTH_DATA_IDENTIFICATION_1);
		statusMessage.setContent(StatusMessageContent.create("{ \"content\" : \"content_1\"}"));
		statusMessage.setSentOn(new Timestamp(new Date().getTime()));
		statusMessage = dao.create(statusMessage);

		final List<StatusMessage> statusMessages = dao.getAll(search);
		Assert.assertEquals(1, statusMessages.size());
		Assert.assertTrue(statusMessages.contains(statusMessage));
	}
}
