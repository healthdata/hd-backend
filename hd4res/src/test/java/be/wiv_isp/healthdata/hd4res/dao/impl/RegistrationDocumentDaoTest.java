/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.hd4res.domain.RegistrationDocument;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.search.RegistrationDocumentSearch;
import be.wiv_isp.healthdata.hd4res.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.dao.IDocumentDao;
import be.wiv_isp.healthdata.orchestration.dao.IWorkflowDao;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class RegistrationDocumentDaoTest {

	private RegistrationDocument document;

	@Autowired
	private IDocumentDao<RegistrationDocument, RegistrationDocumentSearch> documentDao;

	@Autowired
	private IWorkflowDao<RegistrationWorkflow, RegistrationWorkflowSearch> workflowDao;

	@Before
	public void setUp() throws Exception {
		document = buildDocument();
		document = documentDao.create(document);
	}

	@Test
	public void testWithCsv() throws Exception {
		RegistrationDocument document2 = buildDocument();
		document2.setCsv("Some CSV content".getBytes());
		documentDao.create(document2);
		RegistrationDocument document3 = buildDocument();
		documentDao.create(document3);

		RegistrationDocumentSearch search = new RegistrationDocumentSearch();
		search.setWithCsv(true);
		List<RegistrationDocument> result = documentDao.getAll(search);

		Assert.assertEquals(1, result.size());
	}

	@Test
	public void testWithoutCsv() throws Exception {
		RegistrationDocument document2 = buildDocument();
		document2.setCsv("Some CSV content".getBytes());
		documentDao.create(document2);
		RegistrationDocument document3 = buildDocument();
		documentDao.create(document3);

		RegistrationDocumentSearch search = new RegistrationDocumentSearch();
		search.setWithCsv(false);
		List<RegistrationDocument> result = documentDao.getAll(search);

		Assert.assertEquals(2, result.size());
	}

	private RegistrationDocument buildDocument() {
		RegistrationDocument document = new RegistrationDocument();
		document.setDocumentContent("Some JSON content".getBytes());
		return document;
	}
}
