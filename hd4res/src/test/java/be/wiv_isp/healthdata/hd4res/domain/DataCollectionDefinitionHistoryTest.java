/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.domain;

import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionDefinitionHistory;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.*;

public class DataCollectionDefinitionHistoryTest {

    @Test
    public void testCompareTo() {
        final List<DataCollectionDefinitionHistory> dataCollectionDefinitionHistories = new ArrayList<>();
        final Timestamp now = new Timestamp(System.currentTimeMillis());
        Calendar cal = GregorianCalendar.getInstance();
        cal.add( Calendar.DAY_OF_YEAR, -7);
        final Timestamp oneWeekAgo = new Timestamp(cal.getTime().getTime());

        final DataCollectionDefinitionHistory dataCollectionDefinitionHistory1 = new DataCollectionDefinitionHistory();
        dataCollectionDefinitionHistory1.setId(1L);
        dataCollectionDefinitionHistory1.setCreatedOn(oneWeekAgo);
        dataCollectionDefinitionHistory1.setContent("CONTENT".getBytes());
        dataCollectionDefinitionHistory1.setUserName("username");
        dataCollectionDefinitionHistories.add(dataCollectionDefinitionHistory1);
        final DataCollectionDefinitionHistory dataCollectionDefinitionHistory2 = new DataCollectionDefinitionHistory();
        dataCollectionDefinitionHistory2.setId(2L);
        dataCollectionDefinitionHistory2.setCreatedOn(now);
        dataCollectionDefinitionHistory2.setContent("CONTENT".getBytes());
        dataCollectionDefinitionHistory2.setUserName("username");
        dataCollectionDefinitionHistories.add(dataCollectionDefinitionHistory2);
        Collections.sort(dataCollectionDefinitionHistories);
        Assert.assertEquals(dataCollectionDefinitionHistory2, dataCollectionDefinitionHistories.get(dataCollectionDefinitionHistories.size()-1));
    }
}
