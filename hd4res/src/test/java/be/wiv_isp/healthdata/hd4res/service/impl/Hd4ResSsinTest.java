/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.hd4res.dao.IOptionalPatientIdDataCollectionNameDao;
import be.wiv_isp.healthdata.hd4res.domain.OptionalPatientIdDataCollectionName;
import be.wiv_isp.healthdata.hd4res.domain.search.Hd4ResSsinSearch;
import be.wiv_isp.healthdata.hd4res.encrypt.dao.ICombiRegisterDao;
import be.wiv_isp.healthdata.hd4res.encrypt.dao.IHd4ResSsinDao;
import be.wiv_isp.healthdata.hd4res.encrypt.domain.Hd4ResSsin;
import be.wiv_isp.healthdata.hd4res.service.IReadableIdentifierService;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.easymock.EasyMock.*;

public class Hd4ResSsinTest {

	IReadableIdentifierService readableIdentifierService = new ReadableIdentifierService();

	private final IHd4ResSsinDao landingZoneSsinDao = EasyMock.createNiceMock(IHd4ResSsinDao.class);
	private final ICombiRegisterDao combiRegisterDao = EasyMock.createNiceMock(ICombiRegisterDao.class);
	private final IOptionalPatientIdDataCollectionNameDao optionalPatientIdDataCollectionNameDao = EasyMock.createNiceMock(IOptionalPatientIdDataCollectionNameDao.class);

	@Before
	public void setup() {
		ReflectionTestUtils.setField(readableIdentifierService, "hd4ResSsinDao", landingZoneSsinDao);
		ReflectionTestUtils.setField(readableIdentifierService, "combiRegisterDao", combiRegisterDao);
		ReflectionTestUtils.setField(readableIdentifierService, "optionalPatientIdDataCollectionNameDao", optionalPatientIdDataCollectionNameDao);
	}

	@Test
	public void testGetReadableSsin1() throws Exception {
		final String ENCODED_SSIN = "encodedSsin";
		final String REGISTER_NAME = "HIV";
		final Hd4ResSsin hd4ResSsin = new Hd4ResSsin();
		hd4ResSsin.setRegisterName(REGISTER_NAME);
		hd4ResSsin.seteHealthCodedNissBatch(ENCODED_SSIN);
		hd4ResSsin.setLandingZoneCodedNiss(REGISTER_NAME + "_PATIENTID_0000-0001");
		final Hd4ResSsinSearch search = new Hd4ResSsinSearch();
		search.setRegisterName(REGISTER_NAME);
		search.seteHealthCodedNissBatch(ENCODED_SSIN);

		expect(combiRegisterDao.get(REGISTER_NAME)).andReturn(null);
		expect(landingZoneSsinDao.getAll(search)).andReturn(Arrays.asList(hd4ResSsin));
		List<OptionalPatientIdDataCollectionName> optionalPatientIdDataCollectionNames = new ArrayList<>();
		expect(optionalPatientIdDataCollectionNameDao.getAll()).andReturn(optionalPatientIdDataCollectionNames);
		replay(combiRegisterDao, landingZoneSsinDao, optionalPatientIdDataCollectionNameDao);

		String readableSsin = readableIdentifierService.getReadableSsin(REGISTER_NAME, ENCODED_SSIN);

		verify(combiRegisterDao, landingZoneSsinDao, optionalPatientIdDataCollectionNameDao);

		Assert.assertEquals("HIV_PATIENTID_0000-0001", readableSsin);

	}

	@Test
	public void testGetReadableSsin2() throws Exception {
		Hd4ResSsin hd4ResSsin = new Hd4ResSsin();
		hd4ResSsin.seteHealthCodedNissBatch("encodedSsin");
		hd4ResSsin.setRegisterName("HIV");
		hd4ResSsin.setLandingZoneCodedNiss("HIV_PATIENTID_0000-0002");

		expect(combiRegisterDao.get("HIV")).andReturn(null);
		expect(landingZoneSsinDao.getAll((Hd4ResSsinSearch) EasyMock.anyObject())).andReturn(new ArrayList<Hd4ResSsin>());
		expect(landingZoneSsinDao.getBiggest("HIV")).andReturn("HIV_PATIENTID_0000-0001");
		expect(landingZoneSsinDao.create((Hd4ResSsin) EasyMock.anyObject())).andReturn(hd4ResSsin);
		List<OptionalPatientIdDataCollectionName> optionalPatientIdDataCollectionNames = new ArrayList<>();
		expect(optionalPatientIdDataCollectionNameDao.getAll()).andReturn(optionalPatientIdDataCollectionNames);
		replay(combiRegisterDao, landingZoneSsinDao, optionalPatientIdDataCollectionNameDao);

		String readableSsin = readableIdentifierService.getReadableSsin("HIV", "encodedSsin");

		Assert.assertEquals("HIV_PATIENTID_0000-0002", readableSsin);

		verify(combiRegisterDao, landingZoneSsinDao, optionalPatientIdDataCollectionNameDao);
	}

	@Test
	public void testGetEncodedSsin1() throws Exception {
		Hd4ResSsin hd4ResSsin = new Hd4ResSsin();
		hd4ResSsin.seteHealthCodedNissBatch("encodedSsin1");

		expect(landingZoneSsinDao.getAll((Hd4ResSsinSearch) EasyMock.anyObject())).andReturn(Arrays.asList(hd4ResSsin));
		replay(landingZoneSsinDao);

		String readableSsin = readableIdentifierService.getEncodedSsin("HIV_PATIENTID_0000-0001");

		Assert.assertEquals("encodedSsin1", readableSsin);

		verify(landingZoneSsinDao);
	}

	@Test
	public void testGetEncodedSsin2() throws Exception {
		expect(landingZoneSsinDao.getAll((Hd4ResSsinSearch) EasyMock.anyObject())).andReturn(new ArrayList<Hd4ResSsin>());
		replay(landingZoneSsinDao);

		String readableSsin = readableIdentifierService.getEncodedSsin("HIV_PATIENTID_0000-0001");

		Assert.assertNull(readableSsin);

		verify(landingZoneSsinDao);
	}
}
