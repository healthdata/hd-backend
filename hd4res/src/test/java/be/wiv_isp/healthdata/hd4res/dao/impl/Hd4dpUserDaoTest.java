/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.hd4res.dao.IHd4dpUserDao;
import be.wiv_isp.healthdata.hd4res.domain.Hd4dpAuthority;
import be.wiv_isp.healthdata.hd4res.domain.Hd4dpUser;
import be.wiv_isp.healthdata.hd4res.domain.search.Hd4dpUserSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class Hd4dpUserDaoTest {

	@Autowired
	private IHd4dpUserDao dao;

	@Test
	public void testCreateAndGet() {
		Hd4dpUser user = createHd4dpUser();
		user = dao.create(user);

		final Hd4dpUser retrievedUser = dao.get(user.getId());
		Assert.assertNotNull(retrievedUser);
		Assert.assertEquals(user, retrievedUser);
	}

	@Test
	public void testSearchByHd4dpId() {
		final Hd4dpUser user1 = createHd4dpUser();
		final Hd4dpUser created1 = dao.create(user1);

		final Hd4dpUser user2 = createHd4dpUser();
		user2.setHd4dpId(created1.getHd4dpId()+1);
		final Hd4dpUser created2 = dao.create(user2);

		Assert.assertNotEquals(created1.getId(), created2.getId());

		Hd4dpUserSearch search = new Hd4dpUserSearch();
		List<Hd4dpUser> all = dao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new Hd4dpUserSearch();
		search.setHd4dpId(created1.getHd4dpId());
		all = dao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertFalse(all.contains(created2));
	}

	@Test
	public void testSearchByIdentificationType() {
		final Hd4dpUser user1 = createHd4dpUser();
		final Hd4dpUser created1 = dao.create(user1);

		final Hd4dpUser user2 = createHd4dpUser();
		user2.getHealthDataIdentification().setType("FOO");
		final Hd4dpUser created2 = dao.create(user2);

		Assert.assertEquals("RIZIV", created1.getHealthDataIdentification().getType());

		Hd4dpUserSearch search = new Hd4dpUserSearch();
		List<Hd4dpUser> all = dao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new Hd4dpUserSearch();
		search.setHealthDataIdentificationType("RIZIV");
		all = dao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertFalse(all.contains(created2));
	}

	@Test
	public void testSearchByIdentificationValue() {
		final Hd4dpUser user1 = createHd4dpUser();
		final Hd4dpUser created1 = dao.create(user1);

		final Hd4dpUser user2 = createHd4dpUser();
		user2.getHealthDataIdentification().setValue("FOO");
		final Hd4dpUser created2 = dao.create(user2);

		Assert.assertEquals("11111111", created1.getHealthDataIdentification().getValue());

		Hd4dpUserSearch search = new Hd4dpUserSearch();
		List<Hd4dpUser> all = dao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new Hd4dpUserSearch();
		search.setHealthDataIdentificationValue("11111111");
		all = dao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertFalse(all.contains(created2));
	}

	@Test
	public void testSearchByDataCollectionName() {
		final Hd4dpUser user1 = createHd4dpUser();
		final Hd4dpUser created1 = dao.create(user1);

		final Hd4dpUser user2 = createHd4dpUser();
		user2.setDataCollectionNames(new HashSet<>(Arrays.asList("FOO", "BAR")));
		final Hd4dpUser created2 = dao.create(user2);

		Assert.assertTrue(created1.getDataCollectionNames().contains("TEST"));

		Hd4dpUserSearch search = new Hd4dpUserSearch();
		List<Hd4dpUser> all = dao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new Hd4dpUserSearch();
		search.setDataCollectionName("TEST");
		all = dao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertFalse(all.contains(created2));
	}

	@Test
	public void testSearchByAuthority() {
		final Hd4dpUser user1 = createHd4dpUser();
		final Hd4dpUser created1 = dao.create(user1);

		final Hd4dpUser user2 = createHd4dpUser();
		user2.setAuthorities(new HashSet<>(Collections.singletonList(new Hd4dpAuthority(Hd4dpAuthority.ADMIN))));
		final Hd4dpUser created2 = dao.create(user2);

		Assert.assertEquals(new HashSet<>(Collections.singletonList(new Hd4dpAuthority(Hd4dpAuthority.USER))), created1.getAuthorities());
		Assert.assertNotEquals(created1.getAuthorities(), created2.getAuthorities());

		Hd4dpUserSearch search = new Hd4dpUserSearch();
		List<Hd4dpUser> all = dao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new Hd4dpUserSearch();
		search.setAuthority(new Hd4dpAuthority(Hd4dpAuthority.USER));
		all = dao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertFalse(all.contains(created2));
	}

	private Hd4dpUser createHd4dpUser() {
		Hd4dpUser user = new Hd4dpUser();
		user.setHd4dpId(1L);
		user.setUsername("username");
		user.setLastName("lastname");
		user.setFirstName("firstname");
		user.setEmail("email@test.com");
		user.setLdapUser(false);
		user.setEnabled(true);
		user.setAuthorities(new HashSet<>(Collections.singletonList(new Hd4dpAuthority(Hd4dpAuthority.USER))));
		user.setDataCollectionNames(new HashSet<>(Arrays.asList("TEST", "TEST1")));
		final HealthDataIdentification healthDataIdentification = new HealthDataIdentification();
		healthDataIdentification.setName("My Organization");
		healthDataIdentification.setType("RIZIV");
		healthDataIdentification.setValue("11111111");
		healthDataIdentification.setHd4prc(false);
		user.setHealthDataIdentification(healthDataIdentification);
		return user;
	}
}
