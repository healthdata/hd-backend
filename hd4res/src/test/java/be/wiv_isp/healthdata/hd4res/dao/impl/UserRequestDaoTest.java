/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.hd4res.domain.UserRequest;
import be.wiv_isp.healthdata.hd4res.domain.search.UserRequestSearch;
import be.wiv_isp.healthdata.orchestration.dao.IUserRequestDao;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class UserRequestDaoTest {

    @Autowired
    private IUserRequestDao<UserRequest, UserRequestSearch> dao;


    private UserRequest userRequest1;
    private UserRequest userRequest2;
    private UserRequest userRequest3;

    private HealthDataIdentification healthDataIdentification1 = new HealthDataIdentification();
    {
        healthDataIdentification1.setName("Hospital 1");
        healthDataIdentification1.setType("RIZIV");
        healthDataIdentification1.setValue("1");
    }

    private HealthDataIdentification healthDataIdentification2 = new HealthDataIdentification();
    {
        healthDataIdentification2.setName("Hospital 2");
        healthDataIdentification2.setType("RIZIV");
        healthDataIdentification2.setValue("2");
    }

    private void setup() {

        userRequest1 = new UserRequest();
        userRequest1.setUsername("username1");
        userRequest1.setFirstName("FirstName1");
        userRequest1.setLastName("LastName1");
        userRequest1.setEmail("firstname1.lastname1@hospital1.be");
        userRequest1.setEmailRequester("requester1@hospital1.be");
        userRequest1.setHealthDataIdentification(healthDataIdentification1);
        userRequest1.setHd4dpId(1L);
        userRequest1.setRequestedOn(new Timestamp(new Date().getTime()));
        userRequest1.setApproved(null);
        userRequest1.setDataCollectionNames(new HashSet<>(Arrays.asList("TEST")));
        userRequest1.setHd4dpUrl("https://hospital1.be/hd4dp");

        userRequest2 = new UserRequest();
        userRequest2.setUsername("username2");
        userRequest2.setFirstName("FirstName2");
        userRequest2.setLastName("LastName2");
        userRequest2.setEmail("firstname2.lastname2@hospital1.be");
        userRequest2.setEmailRequester("requester1@hospital1.be");
        userRequest2.setHealthDataIdentification(healthDataIdentification1);
        userRequest2.setHd4dpId(2L);
        userRequest2.setRequestedOn(new Timestamp(new Date().getTime()));
        userRequest2.setApproved(false);
        userRequest2.setDataCollectionNames(new HashSet<>(Arrays.asList("TEST", "TEST2")));
        userRequest2.setHd4dpUrl("https://hospital1.be/hd4dp");

        userRequest3 = new UserRequest();
        userRequest3.setUsername("username2");
        userRequest3.setFirstName("FirstName1");
        userRequest3.setLastName("LastName3");
        userRequest3.setEmail("firstname1.lastname3@hospital2.be");
        userRequest3.setHealthDataIdentification(healthDataIdentification2);
        userRequest3.setHd4dpId(1L);
        userRequest3.setRequestedOn(new Timestamp(new Date().getTime()));
        userRequest3.setApproved(true);
        userRequest3.setDataCollectionNames(new HashSet<>(Arrays.asList("TEST", "TEST2")));
        userRequest3.setHd4dpUrl("https://hospital2.be/hd4dp");

        userRequest1 = dao.create(userRequest1);
        userRequest2 = dao.create(userRequest2);
        userRequest3 = dao.create(userRequest3);

    }

    @Test
    public void  testGetAllFirstName() {
        setup();
        final UserRequestSearch search = new UserRequestSearch();
        search.setFirstName("FirstName1");

        final List<UserRequest> userRequests = dao.getAll(search);
        Assert.assertEquals(2, userRequests.size());
        Assert.assertTrue(userRequests.contains(userRequest1));
        Assert.assertFalse(userRequests.contains(userRequest2));
        Assert.assertTrue(userRequests.contains(userRequest3));
    }

    @Test
    public void  testGetAllLastName() {
        setup();
        final UserRequestSearch search = new UserRequestSearch();
        search.setLastName("LastName1");

        final List<UserRequest> userRequests = dao.getAll(search);
        Assert.assertEquals(1, userRequests.size());
        Assert.assertTrue(userRequests.contains(userRequest1));
        Assert.assertFalse(userRequests.contains(userRequest2));
        Assert.assertFalse(userRequests.contains(userRequest3));
    }

    @Test
    public void  testGetAllEmail() {
        setup();
        final UserRequestSearch search = new UserRequestSearch();
        search.setEmail("firstname1.lastname3@hospital2.be");

        final List<UserRequest> userRequests = dao.getAll(search);
        Assert.assertEquals(1, userRequests.size());
        Assert.assertFalse(userRequests.contains(userRequest1));
        Assert.assertFalse(userRequests.contains(userRequest2));
        Assert.assertTrue(userRequests.contains(userRequest3));
    }

    @Test
    public void  testGetAllEmailRequester() {
        setup();
        final UserRequestSearch search = new UserRequestSearch();
        search.setEmailRequester("requester1@hospital1.be");

        final List<UserRequest> userRequests = dao.getAll(search);
        Assert.assertEquals(2, userRequests.size());
        Assert.assertTrue(userRequests.contains(userRequest1));
        Assert.assertTrue(userRequests.contains(userRequest2));
        Assert.assertFalse(userRequests.contains(userRequest3));
    }

    @Test
    public void  testGetAllHealthDataIdentification() {
        setup();
        final UserRequestSearch search = new UserRequestSearch();
        search.setHealthDataIdentification(healthDataIdentification1);

        final List<UserRequest> userRequests = dao.getAll(search);
        Assert.assertEquals(2, userRequests.size());
        Assert.assertTrue(userRequests.contains(userRequest1));
        Assert.assertTrue(userRequests.contains(userRequest2));
        Assert.assertFalse(userRequests.contains(userRequest3));
    }

    @Test
    public void  testGetAllHd4dpId() {
        setup();
        final UserRequestSearch search = new UserRequestSearch();
        search.setHd4dpId(1L);

        final List<UserRequest> userRequests = dao.getAll(search);
        Assert.assertEquals(2, userRequests.size());
        Assert.assertTrue(userRequests.contains(userRequest1));
        Assert.assertFalse(userRequests.contains(userRequest2));
        Assert.assertTrue(userRequests.contains(userRequest3));
    }

    @Test
    public void  testGetAllDataCollectionNames() {
        setup();
        final UserRequestSearch searchTest = new UserRequestSearch();
        searchTest.setDataCollectionName("TEST");

        final List<UserRequest> userRequestsTest = dao.getAll(searchTest);
        Assert.assertEquals(3, userRequestsTest.size());
        Assert.assertTrue(userRequestsTest.contains(userRequest1));
        Assert.assertTrue(userRequestsTest.contains(userRequest2));
        Assert.assertTrue(userRequestsTest.contains(userRequest3));

        final UserRequestSearch searchTest2 = new UserRequestSearch();
        searchTest2.setDataCollectionName("TEST2");

        final List<UserRequest> userRequestsTest2 = dao.getAll(searchTest2);
        Assert.assertEquals(2, userRequestsTest2.size());
        Assert.assertFalse(userRequestsTest2.contains(userRequest1));
        Assert.assertTrue(userRequestsTest2.contains(userRequest2));
        Assert.assertTrue(userRequestsTest2.contains(userRequest3));
    }


        @Test
    public void  testGetAllApproved() {
        setup();
        final UserRequestSearch search = new UserRequestSearch();

        final List<UserRequest> userRequests = dao.getAll(search);
        Assert.assertEquals(3, userRequests.size());
        Assert.assertTrue(userRequests.contains(userRequest1));
        Assert.assertTrue(userRequests.contains(userRequest2));
        Assert.assertTrue(userRequests.contains(userRequest3));

        final UserRequestSearch searchApproved = new UserRequestSearch();
        searchApproved.setApproved(true);

        final List<UserRequest> userRequestsApproved = dao.getAll(searchApproved);
        Assert.assertEquals(1, userRequestsApproved.size());
        Assert.assertFalse(userRequestsApproved.contains(userRequest1));
        Assert.assertFalse(userRequestsApproved.contains(userRequest2));
        Assert.assertTrue(userRequestsApproved.contains(userRequest3));

        final UserRequestSearch searchRejected = new UserRequestSearch();
        searchRejected.setApproved(false);

        final List<UserRequest> userRequestsRejected = dao.getAll(searchRejected);
        Assert.assertEquals(1, userRequestsRejected.size());
        Assert.assertFalse(userRequestsRejected.contains(userRequest1));
        Assert.assertTrue(userRequestsRejected.contains(userRequest2));
        Assert.assertFalse(userRequestsRejected.contains(userRequest3));


        final UserRequestSearch searchPending = new UserRequestSearch();

        final List<UserRequest> userRequestsPending = dao.getAll(searchPending);
        Assert.assertEquals(3, userRequestsPending.size());
        Assert.assertTrue(userRequestsPending.contains(userRequest1));
        Assert.assertTrue(userRequestsPending.contains(userRequest2));
        Assert.assertTrue(userRequestsPending.contains(userRequest3));
    }

    @Test
    public void  testGetAllDisplayArchived() {
        setup();
        UserRequest archivedUserRequest = new UserRequest();
        archivedUserRequest.setUsername("username4");
        archivedUserRequest.setFirstName("FirstName4");
        archivedUserRequest.setLastName("LastName4");
        archivedUserRequest.setEmail("firstname4.lastname4@hospital1.be");
        archivedUserRequest.setHealthDataIdentification(healthDataIdentification1);
        archivedUserRequest.setRequestedOn(new Timestamp(new Date().getTime()));
        archivedUserRequest.setApproved(true);
        archivedUserRequest.setArchivedDataCollectionNames(new HashSet<>(Arrays.asList("TEST")));
        archivedUserRequest.setHd4dpId(3L);
        archivedUserRequest.setDataCollectionNames(new HashSet<>(Arrays.asList("TEST")));
        archivedUserRequest.setHd4dpUrl("https://hospital1.be/hd4dp");

        archivedUserRequest = dao.create(archivedUserRequest);

        final UserRequestSearch search = new UserRequestSearch();
        search.setDataCollectionName("TEST");

        final List<UserRequest> userRequests = dao.getAll(search);
        Assert.assertEquals(3, userRequests.size());
        Assert.assertTrue(userRequests.contains(userRequest1));
        Assert.assertTrue(userRequests.contains(userRequest2));
        Assert.assertTrue(userRequests.contains(userRequest3));
        Assert.assertFalse(userRequests.contains(archivedUserRequest));

        final UserRequestSearch searchHidden = new UserRequestSearch();
        searchHidden.setDataCollectionName("TEST");
        searchHidden.setDisplayArchived(true);

        final List<UserRequest> userRequestsHidden = dao.getAll(searchHidden);
        Assert.assertEquals(4, userRequestsHidden.size());
        Assert.assertTrue(userRequestsHidden.contains(userRequest1));
        Assert.assertTrue(userRequestsHidden.contains(userRequest2));
        Assert.assertTrue(userRequestsHidden.contains(userRequest3));
        Assert.assertTrue(userRequestsHidden.contains(archivedUserRequest));
    }
}
