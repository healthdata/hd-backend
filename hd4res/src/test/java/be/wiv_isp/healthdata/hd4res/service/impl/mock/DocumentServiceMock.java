/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl.mock;

import be.wiv_isp.healthdata.hd4res.domain.RegistrationDocument;
import be.wiv_isp.healthdata.hd4res.domain.search.RegistrationDocumentSearch;
import be.wiv_isp.healthdata.orchestration.service.IAbstractRegistrationDocumentService;

import java.util.List;

public class DocumentServiceMock implements IAbstractRegistrationDocumentService<RegistrationDocument, RegistrationDocumentSearch> {

	@Override
	public RegistrationDocument getEntityInstance() {
		return new RegistrationDocument();
	}

	@Override
	public RegistrationDocument create(RegistrationDocument document) {
		return null;
	}

	@Override
	public RegistrationDocument get(long id) {
		return null;
	}

	@Override
	public RegistrationDocument update(RegistrationDocument document) {
		return null;
	}

	@Override
	public void delete(long id) {
	}

	@Override
	public void delete(RegistrationDocument document) {

	}

	@Override
	public List<RegistrationDocument> getAll(RegistrationDocumentSearch search) {
		return null;
	}

	@Override
	public long count() {
		return 0;
	}

}
