/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.hd4res.dao.IAcceptedWorkflowDao;
import be.wiv_isp.healthdata.hd4res.domain.AcceptedWorkflow;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class AcceptedWorkflowDaoTest {

	@Autowired
	private IAcceptedWorkflowDao dao;

	private AcceptedWorkflow acceptedWorkflow1;
	private AcceptedWorkflow acceptedWorkflow2;
	private AcceptedWorkflow acceptedWorkflow3;

	private void setup() {
		acceptedWorkflow1 = new AcceptedWorkflow();
		acceptedWorkflow1.setId(1L);

		acceptedWorkflow2 = new AcceptedWorkflow();
		acceptedWorkflow2.setId(2L);

		acceptedWorkflow3 = new AcceptedWorkflow();
		acceptedWorkflow3.setId(3L);
		acceptedWorkflow3.setErrorProcessing(true);

		acceptedWorkflow1 = dao.create(acceptedWorkflow1);
		acceptedWorkflow2 = dao.create(acceptedWorkflow2);
		acceptedWorkflow3 = dao.create(acceptedWorkflow3);
	}

	@Test
	public void testGetAll() {
		setup();
		List<AcceptedWorkflow> all = dao.getAll();
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(acceptedWorkflow1));
		Assert.assertTrue(all.contains(acceptedWorkflow2));
		Assert.assertFalse(all.contains(acceptedWorkflow3));
	}
}
