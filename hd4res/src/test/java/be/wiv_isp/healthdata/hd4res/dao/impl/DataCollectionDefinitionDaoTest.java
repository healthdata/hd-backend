/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.hd4res.dao.IDataCollectionDefinitionDao;
import be.wiv_isp.healthdata.hd4res.dao.IDataCollectionGroupDao;
import be.wiv_isp.healthdata.hd4res.domain.Description;
import be.wiv_isp.healthdata.hd4res.domain.Label;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionDefinition;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionGroup;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.*;

@Transactional
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "/applicationContext-test-dao.xml" })
public class DataCollectionDefinitionDaoTest {

	private static final long NB_INSERTS = 10;

	private static int versionCounter = 1;

	@Autowired
	private IDataCollectionGroupDao groupDao;
	@Autowired
	private IDataCollectionDefinitionDao dao;

	private final Map<Long, DataCollectionDefinition> insertedEntities = new HashMap<>();

	public void initialize() throws ParseException {
		for (int i = 1; i <= NB_INSERTS; i++) {
			DataCollectionDefinition dcd = createDataCollectionDefinition(i);
			insertedEntities.put(dcd.getId(), dcd);
		}
	}

	@Test
	public void testGet() throws Exception {
		initialize();
		for (long id : insertedEntities.keySet()) {
			DataCollectionDefinition dataCollectionDefinition = dao.get(id);
			Assert.assertTrue(dataCollectionDefinition.equals(insertedEntities.get(id)));
		}
	}

	@Test
	public void testGetAll() throws Exception {
		initialize();
		List<DataCollectionDefinition> dataCollectionDefinitions = dao.getAll();
		Assert.assertNotNull(dataCollectionDefinitions);
		Assert.assertFalse(dataCollectionDefinitions.isEmpty());
		Assert.assertEquals(NB_INSERTS, dataCollectionDefinitions.size());
		for (DataCollectionDefinition insertedDataCollectionDefinition : insertedEntities.values()) {
			Assert.assertTrue(dataCollectionDefinitions.contains(insertedDataCollectionDefinition));
		}
	}

	@Test
	public void testGetByName() throws Exception {
		final DataCollectionDefinition dataCollectionDefinition1 = new DataCollectionDefinition();
		dataCollectionDefinition1.setDataCollectionName("UT_DATA_COLLECTION_NAME_1");
		dataCollectionDefinition1.setMinorVersion(1);
		dataCollectionDefinition1.setPublished(false);
		Label label = new Label();
		label.setEn("LABEL EN");
		label.setFr("LABEL FR");
		label.setNl("LABEL NL");
		dataCollectionDefinition1.setLabel(label);
		Description description = new Description();
		description.setEn("DESCRIPTION EN");
		description.setFr("DESCRIPTION FR");
		description.setNl("DESCRIPTION NL");
		dataCollectionDefinition1.setDescription(description);
		DataCollectionGroup group1 = new DataCollectionGroup();
		group1.setName("GROUP1");
		group1.setDescription(new Description("en", "fr","nl"));
		group1.setLabel(new Label("en", "fr","nl"));
		group1.setMajorVersion(1);
		groupDao.create(group1);
		dataCollectionDefinition1.setDataCollectionGroupId(group1.getId());
		dao.create(dataCollectionDefinition1);
		final DataCollectionDefinition dataCollectionDefinition2 = new DataCollectionDefinition();
		dataCollectionDefinition2.setDataCollectionName("UT_DATA_COLLECTION_NAME_1");
		dataCollectionDefinition2.setMinorVersion(1);
		dataCollectionDefinition2.setPublished(false);
		dataCollectionDefinition2.setLabel(label);
		dataCollectionDefinition2.setDescription(description);
		dataCollectionDefinition2.setDataCollectionGroupId(group1.getId());
		dao.create(dataCollectionDefinition2);
		final DataCollectionDefinition dataCollectionDefinition3 = new DataCollectionDefinition();
		dataCollectionDefinition3.setDataCollectionName("UT_DATA_COLLECTION_NAME_2");
		dataCollectionDefinition3.setMinorVersion(0);
		dataCollectionDefinition3.setPublished(true);
		dataCollectionDefinition3.setLabel(label);
		dataCollectionDefinition3.setDescription(description);
		DataCollectionGroup group3 = new DataCollectionGroup();
		group3.setName("GROUP3");
		group3.setDescription(new Description("en", "fr","nl"));
		group3.setLabel(new Label("en", "fr","nl"));
		group3.setMajorVersion(2);
		groupDao.create(group3);
		dataCollectionDefinition3.setDataCollectionGroupId(group3.getId());
		dao.create(dataCollectionDefinition3);
		List<DataCollectionDefinition> dataCollectionDefinitions = dao.get("UT_DATA_COLLECTION_NAME_1");
		Assert.assertNotNull(dataCollectionDefinitions);
		Assert.assertEquals(2, dataCollectionDefinitions.size());
		Assert.assertTrue(dataCollectionDefinitions.contains(dataCollectionDefinition1));
		Assert.assertTrue(dataCollectionDefinitions.contains(dataCollectionDefinition2));
	}

	@Test
	public void testLatestMajor() throws Exception {
		final DataCollectionDefinition dataCollectionDefinition1 = new DataCollectionDefinition();
		dataCollectionDefinition1.setDataCollectionName("LATEST");
		dataCollectionDefinition1.setMinorVersion(0);
		dataCollectionDefinition1.setPublished(false);
		Label label = new Label();
		label.setEn("LABEL EN");
		label.setFr("LABEL FR");
		label.setNl("LABEL NL");
		dataCollectionDefinition1.setLabel(label);
		Description description = new Description();
		description.setEn("DESCRIPTION EN");
		description.setFr("DESCRIPTION FR");
		description.setNl("DESCRIPTION NL");
		dataCollectionDefinition1.setDescription(description);
		DataCollectionGroup group1 = new DataCollectionGroup();
		group1.setName("GROUP1");
		group1.setDescription(new Description("en", "fr","nl"));
		group1.setLabel(new Label("en", "fr","nl"));
		group1.setMajorVersion(1);
		groupDao.create(group1);
		dataCollectionDefinition1.setDataCollectionGroupId(group1.getId());
		dao.create(dataCollectionDefinition1);
		Thread.sleep(1000);
		final DataCollectionDefinition dataCollectionDefinition2 = new DataCollectionDefinition();
		dataCollectionDefinition2.setDataCollectionName("LATEST");
		dataCollectionDefinition2.setMinorVersion(1);
		dataCollectionDefinition2.setPublished(false);
		dataCollectionDefinition2.setLabel(label);
		dataCollectionDefinition2.setDescription(description);
		dataCollectionDefinition2.setDataCollectionGroupId(group1.getId());
		dao.create(dataCollectionDefinition2);
		Thread.sleep(1000);
		final DataCollectionDefinition dataCollectionDefinition3 = new DataCollectionDefinition();
		dataCollectionDefinition3.setDataCollectionName("LATEST");
		dataCollectionDefinition3.setMinorVersion(0);
		dataCollectionDefinition3.setPublished(false);
		dataCollectionDefinition3.setLabel(label);
		dataCollectionDefinition3.setDescription(description);
		DataCollectionGroup group3 = new DataCollectionGroup();
		group3.setName("GROUP3");
		group3.setDescription(new Description("en", "fr","nl"));
		group3.setLabel(new Label("en", "fr","nl"));
		group3.setMajorVersion(2);
		groupDao.create(group3);
		dataCollectionDefinition3.setDataCollectionGroupId(group3.getId());
		dao.create(dataCollectionDefinition3);

		List<DataCollectionDefinition> latestMinors = dao.latestMinors(group1.getId());
		Assert.assertEquals(1, latestMinors.size());
		Assert.assertTrue(latestMinors.contains(dataCollectionDefinition2));
	}

	@Test
	public void testLatest2() throws Exception {
		List<DataCollectionDefinition> latestMinors = dao.latestMinors(1L);
		Assert.assertTrue(latestMinors.isEmpty());
	}

	@Test
	public void testGetExistingDataCollections() throws Exception {
		final DataCollectionDefinition dataCollectionDefinition1 = new DataCollectionDefinition();
		dataCollectionDefinition1.setDataCollectionName("EXISTING");
		dataCollectionDefinition1.setMinorVersion(0);
		dataCollectionDefinition1.setPublished(false);
		Label label = new Label();
		label.setEn("LABEL EN");
		label.setFr("LABEL FR");
		label.setNl("LABEL NL");
		dataCollectionDefinition1.setLabel(label);
		Description description = new Description();
		description.setEn("DESCRIPTION EN");
		description.setFr("DESCRIPTION FR");
		description.setNl("DESCRIPTION NL");
		dataCollectionDefinition1.setDescription(description);
		DataCollectionGroup group1 = new DataCollectionGroup();
		group1.setName("GROUP2");
		group1.setDescription(new Description("en", "fr","nl"));
		group1.setLabel(new Label("en", "fr","nl"));
		group1.setMajorVersion(1);
		groupDao.create(group1);
		dataCollectionDefinition1.setDataCollectionGroupId(group1.getId());
		dao.create(dataCollectionDefinition1);
		Thread.sleep(1000);
		final DataCollectionDefinition dataCollectionDefinition2 = new DataCollectionDefinition();
		dataCollectionDefinition2.setDataCollectionName("EXISTING");
		dataCollectionDefinition2.setMinorVersion(1);
		dataCollectionDefinition2.setPublished(false);
		dataCollectionDefinition2.setLabel(label);
		dataCollectionDefinition2.setDescription(description);
		dataCollectionDefinition2.setDataCollectionGroupId(group1.getId());
		dao.create(dataCollectionDefinition2);
		Thread.sleep(1000);
		final DataCollectionDefinition dataCollectionDefinition3 = new DataCollectionDefinition();
		dataCollectionDefinition3.setDataCollectionName("EXISTING");
		dataCollectionDefinition3.setMinorVersion(0);
		dataCollectionDefinition3.setPublished(true);
		dataCollectionDefinition3.setLabel(label);
		dataCollectionDefinition3.setDescription(description);
		DataCollectionGroup group3 = new DataCollectionGroup();
		group3.setName("GROUP3");
		group3.setDescription(new Description("en", "fr","nl"));
		group3.setLabel(new Label("en", "fr","nl"));
		group3.setMajorVersion(2);
		groupDao.create(group3);
		dataCollectionDefinition3.setDataCollectionGroupId(group3.getId());
		dao.create(dataCollectionDefinition3);
		List<String> dataCollectionNames = dao.getExistingDataCollections();
		Assert.assertFalse(dataCollectionNames.isEmpty());
		Assert.assertEquals(1, dataCollectionNames.size());
		Assert.assertEquals("EXISTING", dataCollectionNames.get(0));
	}

	/**
	 * Creates and inserts a data collection definition in the database.
	 */
	private DataCollectionDefinition createDataCollectionDefinition(long count) {
		return createDataCollectionDefinition(count, null, null);
	}

	/**
	 * Creates and inserts a data collection definition in the database.
	 */
	private DataCollectionDefinition createDataCollectionDefinition(long count, Timestamp startDate, Timestamp endDateCreation) {
		final DataCollectionDefinition dcd = new DataCollectionDefinition();
		dcd.setCreatedOn(new Timestamp(new Date().getTime()));
		dcd.setDataCollectionName("UT_DATA_COLLECTION_NAME_" + count);
		dcd.setMinorVersion(versionCounter++);
		Label label = new Label();
		label.setEn("LABEL EN " + count);
		label.setFr("LABEL FR " + count);
		label.setNl("LABEL NL " + count);
		dcd.setLabel(label);
		Description description = new Description();
		description.setEn("UT_DATA_COLLECTION_DESC_" + count);
		description.setFr("UT_DATA_COLLECTION_DESC_" + count);
		description.setNl("UT_DATA_COLLECTION_DESC_" + count);
		dcd.setDescription(description);
		DataCollectionGroup group = new DataCollectionGroup();
		group.setName("GROUP");
		group.setDescription(new Description("en", "fr","nl"));
		group.setLabel(new Label("en", "fr","nl"));
		group.setMajorVersion(1);
		groupDao.create(group);
		dcd.setDataCollectionGroupId(group.getId());
		return dao.create(dcd);
	}

	private long getRandomValue(final int min, final int max) {
		Random r = new Random();
		return r.nextInt(max - min) + min;
	}
}
