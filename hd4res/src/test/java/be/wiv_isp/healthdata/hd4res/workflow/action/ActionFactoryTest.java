/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.workflow.action;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.hd4res.action.standalone.StatusMessageAction;
import be.wiv_isp.healthdata.hd4res.action.workflow.CreateForReviewRegistrationAction;
import be.wiv_isp.healthdata.hd4res.action.workflow.UpdateStatusRegistrationAction;
import be.wiv_isp.healthdata.hd4res.domain.StatusMessage;
import be.wiv_isp.healthdata.hd4res.factory.impl.ActionFactory;
import be.wiv_isp.healthdata.orchestration.action.Action;
import be.wiv_isp.healthdata.orchestration.action.StatusMessageContent;
import be.wiv_isp.healthdata.orchestration.action.workflow.WorkflowAction;
import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.factory.IActionFactory;
import be.wiv_isp.healthdata.orchestration.service.IAutowireService;
import org.apache.commons.codec.binary.Base64;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ActionFactoryTest {

	private IActionFactory actionFactory;

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Before
	public void setup() throws IllegalAccessException, InvocationTargetException {
		final ActionFactory actionFactory = new ActionFactory();

		final IAutowireService autowireService = new IAutowireService() {
			@Override
			public <T> T autowire(T object) {
				return object;
			}
		};
		ReflectionTestUtils.setField(actionFactory, "autowireService", autowireService);
		this.actionFactory = actionFactory;
	}

	@Test
	public void testGetCreateForReviewAction() throws Exception {
		final SimpleDateFormat sdf = new SimpleDateFormat(DateFormat.DATE_AND_TIME.getPattern());
		final Date date = sdf.parse(sdf.format(new Date()));

		final HealthDataIdentification identificationWorkflow = new HealthDataIdentification();
		identificationWorkflow.setType("RIZIV");
		identificationWorkflow.setValue("123456789");

		final String actionType = "CREATE_FOR_REVIEW";
		final String documentContent = "{\"key\":\"value\"}";
		final String identificationJson = "{\"type\":\"NIHII\", \"value\":\"123456789\", \"applicationId\":\"healthdata\"}";
		final String workflowIdentificationJson = "{\"type\":\"RIZIV\", \"value\":\"123456789\"}";
		final String sentOn = sdf.format(date);

		final String json = "{\"action\":\"" + actionType + "\",\"documentContent\":" + documentContent + ", \"identification\":" + identificationJson + ", \"identificationWorkflow\":" + workflowIdentificationJson + ", \"sentOn\":\"" + sentOn
				+ "\"}";

		WorkflowAction action = (WorkflowAction) actionFactory.createAction(json.getBytes(StandardCharsets.UTF_8));

		Assert.assertNotNull(action);
		Assert.assertTrue(action instanceof CreateForReviewRegistrationAction);
		//Assert.assertEquals(((CreateForReviewAction) action).getIdentification(), identification);
		Assert.assertEquals(((CreateForReviewRegistrationAction) action).getIdentificationWorkflow(), identificationWorkflow);
		Assert.assertArrayEquals(documentContent.getBytes(StandardCharsets.UTF_8), ((CreateForReviewRegistrationAction) action).getDocumentContent());
	}

	@Test
	public void testGetCreateForReviewActionWithInvalidSentOnAttributeThrowsException() throws Exception {
		final SimpleDateFormat sdf = new SimpleDateFormat(DateFormat.DATE_AND_TIME.getPattern());
		final Date date = sdf.parse(sdf.format(new Date()));

		final String actionType = "CREATE_FOR_REVIEW";
		final String sentOn = new SimpleDateFormat("yyyy-MM-dd").format(date);

		final String json = "{\"action\":\"" + actionType + "\", \"sentOn\":\"" + sentOn + "\"}";

		exception.expect(HealthDataException.class);
		exception.expectMessage(MessageFormat.format(ExceptionType.INVALID_DATE_FORMAT.getMessage(), sentOn, DateFormat.DATE_AND_TIME.getPattern()));

		actionFactory.createAction(json.getBytes(StandardCharsets.UTF_8));
	}

	@Test
	public void testUpdateStatus() throws Exception {
		final String actionType = "UPDATE_STATUS";

		final String json = "{\"action\":\"" + actionType + "\",\"endStatus\":\"APPROVED\"}";

		WorkflowAction action = (WorkflowAction) actionFactory.createAction(json.getBytes(StandardCharsets.UTF_8));

		Assert.assertNotNull(action);
		Assert.assertTrue(action instanceof UpdateStatusRegistrationAction);
		Assert.assertEquals(action.getEndStatus(), WorkflowStatus.APPROVED);
	}

	@Test
	public void testGetUnknown() throws Exception {

		final String json = "{\"action\":\"UNKNOWN\",\"wrongInput\":\"wrongInput\"}";

		exception.expect(HealthDataException.class);
		exception.expectMessage(MessageFormat.format(ExceptionType.INVALID_ACTION.getMessage(), "UNKNOWN"));

		actionFactory.createAction(json.getBytes(StandardCharsets.UTF_8));
	}

	@Test
	public void testStatusMessageAction() throws IOException, JSONException {
		final String contentBase64 = "ew0KCQkib3JnYW5pemF0aW9ucyIgOiBbew0KCQkJCSJuYW1lIiA6ICJIb3NwMSIsDQoJCQkJIkhFQUxUSERBVEFfSURfVFlQRSIgOiAiUklaSVYiLA0KCQkJCSJIRUFMVEhEQVRBX0lEX1ZBTFVFIiA6ICI3MTAwMDAwMSIsDQoJCQkJIm1haW4iIDogdHJ1ZSwNCgkJCQkiY29uZmlndXJhdGlvbnMiIDogew0KCQkJCQkiSURFTlRJRklDQVRJT05fVFlQRSIgOiAiTklISUktSE9TUElUQUwiLA0KCQkJCQkiSURFTlRJRklDQVRJT05fVkFMVUUiIDogIjcxMDAwMDAxIiwNCgkJCQkJIkFQUExJQ0FUSU9OX0lEIiA6ICIiLA0KCQkJCQkiQVVUSEVOVElDQVRJT05fVFlQRSIgOiAiTERBUCIsDQoJCQkJCSJFTV9JTlRFUkZBQ0VfSU4iIDogIlJFU1QiLA0KCQkJCQkiRU1fSU5URVJGQUNFX09VVCIgOiAiUkVTVCIsDQoJCQkJCSJOQVRJT05BTF9SRUdJU1RFUl9DT05ORUNUT1IiIDogIk5PTkUiDQoJCQkJfQ0KCQkJfSwgew0KCQkJCSJuYW1lIiA6ICJIb3NwMiIsDQoJCQkJIkhFQUxUSERBVEFfSURfVFlQRSIgOiAiUklaSVYiLA0KCQkJCSJIRUFMVEhEQVRBX0lEX1ZBTFVFIiA6ICI3MTAwMDAwMiIsDQoJCQkJIm1haW4iIDogZmFsc2UsDQogICAgICAgICAgICAgICAgImNvbmZpZ3VyYXRpb25zIiA6IHsNCiAgICAgICAgICAgICAgICAgICAgIklERU5USUZJQ0FUSU9OX1RZUEUiIDogIk5JSElJLUhPU1BJVEFMIiwNCiAgICAgICAgICAgICAgICAgICAgIklERU5USUZJQ0FUSU9OX1ZBTFVFIiA6ICI3MTAwMDAwMSIsDQogICAgICAgICAgICAgICAgICAgICJBUFBMSUNBVElPTl9JRCIgOiAiIiwNCiAgICAgICAgICAgICAgICAgICAgIkFVVEhFTlRJQ0FUSU9OX1RZUEUiIDogIkxEQVAiLA0KICAgICAgICAgICAgICAgICAgICAiRU1fSU5URVJGQUNFX0lOIiA6ICJSRVNUIiwNCiAgICAgICAgICAgICAgICAgICAgIkVNX0lOVEVSRkFDRV9PVVQiIDogIlJFU1QiLA0KICAgICAgICAgICAgICAgICAgICAiTkFUSU9OQUxfUkVHSVNURVJfQ09OTkVDVE9SIiA6ICJOT05FIg0KICAgICAgICAgICAgICAgIH0NCgkJCX0NCgkJXSwNCgkJImNvbmZpZ3VyYXRpb25zIiA6IHsNCgkJCSJDQVRBTE9HVUVfSE9TVCIgOiAiaHR0cHM6Ly9jYXRhbG9ndWUuaGVhbHRoZGF0YS5iZS9oZWFsdGhkYXRhX2NhdGFsb2d1ZSINCgkJfSwNCgkJInNlbnRfcmVnaXN0cmF0aW9ucyIgOiB7DQoJCQkiQk5NRFIiIDogMzAsDQoJCQkiVEVTVCIgOiA0MDYsDQoJCQkiSVFFREZvb3QiIDogMTAsDQoJCQkiQkNGUiIgOiAyNSwNCgkJCSJJUUVDQUQiIDogNTYsDQoJCQkiQ1JSRCIgOiA1MzQsDQoJCQkiVEVTVENPTExFQ1RJT04iIDogMSwNCgkJCSJOU0lIX3RyeW91dCIgOiA5LA0KCQkJIklRRUQiIDogMQ0KCQl9LA0KCQkidmVyc2lvbnMiIDogew0KCQkJIkhENERQX1ZFUlNJT04iIDogIjEuNC4wIiwNCgkJCSJCQUNLRU5EIFZFUlNJT05TIiA6IHsNCgkJCQkiY29tbWl0IiA6ICJmZDY2MDVhY2Y2YmQ1Njg2OGRlYjQ0NjIyMTVhNmE2YTg0OGE0ZDE3IDIwMTUtMTItMjMgMTc6MDg6NDEgKzAxMDAiDQoJCQl9LA0KCQkJIkZST05URU5EIFZFUlNJT05TIiA6IHsNCgkJCQkiY29tbWl0IiA6ICJlZDAyOWZkNmUwOTRlYzg0NTcyYTNhZjcxZTQzZmM1OTI4MzM0ZmQxIDIwMTUtMTItMTggMTc6NDE6MTggKzAxMDAiDQoJCQl9LA0KCQkJIk1BUFBJTkcgVkVSU0lPTlMiIDogew0KCQkJCSJkb2N1bWVudE1vZGVsIiA6ICIwLjAuNjkiLA0KCQkJCSJjb21taXQiIDogImI0ZTRlZjJkM2VhNGE5MTMzNmU2Yzk2N2RmNDA0OTcxM2E5NjFmZTYgMjAxNS0xMi0yMiAxMjoyMToyNSArMDEwMCINCgkJCX0sDQoJCQkiVkFMSURBVElPTiBWRVJTSU9OUyIgOiB7DQoJCQkJImRvY3VtZW50TW9kZWwiIDogIjAuMC42OSIsDQoJCQkJImNvbW1pdCIgOiAiYjRlNGVmMmQzZWE0YTkxMzM2ZTZjOTY3ZGY0MDQ5NzEzYTk2MWZlNiAyMDE1LTEyLTIyIDEyOjIxOjI1ICswMTAwIg0KCQkJfQ0KCQl9LA0KCQkic3RhdHVzZXMiIDogew0KCQkJImNhdGFsb2d1ZSIgOiAiVVAiLA0KCQkJImRhdGFiYXNlIiA6ICJVUCIsDQoJCQkiZWxhc3RpY3NlYXJjaCIgOiAiVVAiLA0KCQkJIm1hcHBpbmciIDogIlVQIiwNCgkJCSJ2YWxpZGF0aW9uIiA6ICJVUCIsDQoJCQkibGRhcCIgOiAiVVAiDQoJCX0NCgl9";
		final StatusMessageContent content = StatusMessageContent.create(Base64.decodeBase64(contentBase64));

		final StatusMessageAction action = new StatusMessageAction();
		action.setContent(content);

		final String actionAsJson = new ObjectMapper().writeValueAsString(action);

		System.out.println(actionAsJson);

		final Action parsedAction = actionFactory.createAction(actionAsJson.getBytes(StandardCharsets.UTF_8));

		Assert.assertTrue(parsedAction instanceof StatusMessageAction);
		final StatusMessageAction smAction = (StatusMessageAction) parsedAction;

		final StatusMessage statusMessage = new StatusMessage();
		statusMessage.setContent(smAction.getContent());
	}
}
