/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl.mock;

import be.wiv_isp.healthdata.common.domain.search.QueryResult;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.hd4res.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.domain.DataSource;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import be.wiv_isp.healthdata.orchestration.service.IAbstractRegistrationWorkflowService;

import java.util.List;
import java.util.Set;

public class WorkflowServiceMock implements IAbstractRegistrationWorkflowService<RegistrationWorkflow, RegistrationWorkflowSearch> {

	private static long CREATE_COUNTER = 1;

	@Override
	public RegistrationWorkflow create(Organization organization) {
		RegistrationWorkflow workflow = new RegistrationWorkflow() {};
		workflow.setId(CREATE_COUNTER++);
		workflow.setOrganization(organization);
		return workflow;
	}

	@Override
	public RegistrationWorkflow getEntityInstance() {
		return new RegistrationWorkflow();
	}

	@Override
	public RegistrationWorkflowSearch getSearchInstance() {
		return new RegistrationWorkflowSearch();
	}

	@Override
	public RegistrationWorkflow create(RegistrationWorkflow registrationWorkflow) {
		return null;
	}

	@Override
	public RegistrationWorkflow get(Long id) {
		return null;
	}

	@Override
	public RegistrationWorkflow update(RegistrationWorkflow workflow) {
		return null;
	}

	@Override
	public void delete(RegistrationWorkflow registrationWorkflow) {

	}

	@Override
	public List<RegistrationWorkflow> getAll() {
		return null;
	}

	@Override
	public RegistrationWorkflow getUnique(RegistrationWorkflowSearch search) {
		return null;
	}

	@Override
	public List<RegistrationWorkflow> getAll(RegistrationWorkflowSearch search) {
		return null;
	}

	@Override
	public QueryResult<RegistrationWorkflow> getAllAsQueryResult(RegistrationWorkflowSearch search) {
		return null;
	}

	@Override
	public List<Long> getWorkflowIds(RegistrationWorkflowSearch search) {
		return null;
	}

	@Override
	public List<Long> getDataCollectionDefinitionIds() {
		return null;
	}

	@Override
	public List<String> getDataCollectionNames(Long organizationId) {
		return null;
	}

	@Override
	public long count(RegistrationWorkflowSearch search) {
		return 0;
	}

	@Override
	public void delete(List<RegistrationWorkflow> workflows, HDUserDetails principal) {
	}

	@Override
	public void setDocumentData(RegistrationWorkflow workflow, DocumentData documentData, boolean addHistoryEntry) {
	}

	@Override
	public void setDocumentData(RegistrationWorkflow workflow, DocumentData documentData, List<DataSource> dataSources, boolean addHistoryEntry) {
	}

	@Override
	public Set<WorkflowActionType> getAvailableActions(RegistrationWorkflow workflow) {
		return null;
	}

	@Override
	public boolean isActionAvailable(RegistrationWorkflow workflow, WorkflowActionType action) {
		return false;
	}

	@Override
	public void processOutboxWorkflows() {

	}

	@Override
	public void updateSendStatus(Long workflowId, String sendStatus) {

	}
}
