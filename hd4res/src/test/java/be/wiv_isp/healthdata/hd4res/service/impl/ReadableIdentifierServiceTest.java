/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.hd4res.dao.IRecodeDataCollectionNameDao;
import be.wiv_isp.healthdata.hd4res.domain.RecodeDataCollectionName;
import be.wiv_isp.healthdata.hd4res.encrypt.dao.ICombiRegisterDao;
import be.wiv_isp.healthdata.hd4res.encrypt.dao.IHd4ResSsinDao;
import be.wiv_isp.healthdata.hd4res.encrypt.domain.CombiRegister;
import be.wiv_isp.healthdata.hd4res.encrypt.domain.Hd4ResSsin;
import be.wiv_isp.healthdata.hd4res.service.IReadableIdentifierService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-hd4res-test-service.xml" })
@Transactional
public class ReadableIdentifierServiceTest {

	@Autowired
	private IReadableIdentifierService readableIdentifierService;
	@Autowired
	private ICombiRegisterDao combiRegisterDao;
	@Autowired
	private IRecodeDataCollectionNameDao recodeDataCollectionNameDao;
	@Autowired
	private IHd4ResSsinDao hd4ResSsinDao;

	@Test
	public void testConvertBatchToManual() {

		Assert.assertEquals("O0Y7M015dWIrVTBmZEM0WDVwZWcybGlIQT09", readableIdentifierService.convertBatchToManual("3Myub+U0fdC4X5peg2liHA=="));
		Assert.assertEquals("O0Y7QTNGMTR0S2ZDRDVxUnZwYTBlRHRUZz09", readableIdentifierService.convertBatchToManual("A3F14tKfCD5qRvpa0eDtTg=="));
		Assert.assertEquals("O0Y7c1QwYllEMHpVYmhOOEIzRHFUTkRtdz09", readableIdentifierService.convertBatchToManual("sT0bYD0zUbhN8B3DqTNDmw=="));
		Assert.assertEquals("O0Y7WFNkRzFZTWxSKzNJT2lvWUFMOXl5QT09", readableIdentifierService.convertBatchToManual("XSdG1YMlR+3IOioYAL9yyA=="));
		Assert.assertEquals("O0Y7Sy9QRWJJZjFUS0hIYUhwU2F3M2w0UT09", readableIdentifierService.convertBatchToManual("K/PEbIf1TKHHaHpSaw3l4Q=="));

	}

	@Test
	public void testReadableSsin() {
		CombiRegister combiRegister1 = new CombiRegister();
		combiRegister1.setCombiName("TEST3");
		combiRegister1.setRegisterName("TEST3");
		combiRegisterDao.create(combiRegister1);

		CombiRegister combiRegister2 = new CombiRegister();
		combiRegister2.setCombiName("TEST3");
		combiRegister2.setRegisterName("TEST4");
		combiRegisterDao.create(combiRegister2);

		String readableSsin1 = readableIdentifierService.getReadableSsin("TEST", "encodedSsin1");
		String readableSsin2 = readableIdentifierService.getReadableSsin("TEST", "encodedSsin2");
		String readableSsin3 = readableIdentifierService.getReadableSsin("TEST2", "encodedSsin1");
		String readableSsin4 = readableIdentifierService.getReadableSsin("TEST", "encodedSsin1");

		Assert.assertEquals("TEST_PATIENTID_0000-0001", readableSsin1);
		Assert.assertEquals("TEST_PATIENTID_0000-0002", readableSsin2);
		Assert.assertEquals("TEST2_PATIENTID_0000-0001", readableSsin3);
		Assert.assertEquals("TEST_PATIENTID_0000-0001", readableSsin4);

		String encodedSsin1 = readableIdentifierService.getEncodedSsin(readableSsin1);
		String encodedSsin2 = readableIdentifierService.getEncodedSsin(readableSsin2);
		String encodedSsin3 = readableIdentifierService.getEncodedSsin(readableSsin3);
		String encodedSsin4 = readableIdentifierService.getEncodedSsin(readableSsin4);
		String encodedSsin5 = readableIdentifierService.getEncodedSsin("unknownReadableId");

		Assert.assertEquals("encodedSsin1", encodedSsin1);
		Assert.assertEquals("encodedSsin2", encodedSsin2);
		Assert.assertEquals("encodedSsin1", encodedSsin3);
		Assert.assertEquals("encodedSsin1", encodedSsin4);
		Assert.assertNull(encodedSsin5);

		String readableSsin5 = readableIdentifierService.getReadableSsin("TEST3", "encodedSsin1");
		String readableSsin6 = readableIdentifierService.getReadableSsin("TEST3", "encodedSsin2");
		String readableSsin7 = readableIdentifierService.getReadableSsin("TEST4", "encodedSsin1");
		String readableSsin8 = readableIdentifierService.getReadableSsin("TEST4", "encodedSsin3");

		Assert.assertEquals("TEST3_PATIENTID_0000-0001", readableSsin5);
		Assert.assertEquals("TEST3_PATIENTID_0000-0002", readableSsin6);
		Assert.assertEquals("TEST3_PATIENTID_0000-0001", readableSsin7);
		Assert.assertEquals("TEST3_PATIENTID_0000-0003", readableSsin8);

		String encodedSsin6 = readableIdentifierService.getEncodedSsin(readableSsin5);
		String encodedSsin7 = readableIdentifierService.getEncodedSsin(readableSsin6);
		String encodedSsin8 = readableIdentifierService.getEncodedSsin(readableSsin7);
		String encodedSsin9 = readableIdentifierService.getEncodedSsin(readableSsin8);

		Assert.assertEquals("encodedSsin1", encodedSsin6);
		Assert.assertEquals("encodedSsin2", encodedSsin7);
		Assert.assertEquals("encodedSsin1", encodedSsin8);
		Assert.assertEquals("encodedSsin3", encodedSsin9);
	}

	@Test
	public void testReadableWorkflow() {
		String workflowName1 = readableIdentifierService.createWorkflowName("TEST");
		String workflowName2 = readableIdentifierService.createWorkflowName("TEST");
		String workflowName3 = readableIdentifierService.createWorkflowName("TEST2");

		Assert.assertEquals("TEST_0000-0001", workflowName1);
		Assert.assertEquals("TEST_0000-0002", workflowName2);
		Assert.assertEquals("TEST2_0000-0001", workflowName3);

	}


	@Test
	public void testMarkOtherRecordsWithOldPatientId() {
		RecodeDataCollectionName recodeDataCollectionName1 = new RecodeDataCollectionName();
		recodeDataCollectionName1.setName("TEST5");
		recodeDataCollectionNameDao.create(recodeDataCollectionName1);

		RecodeDataCollectionName recodeDataCollectionName2 = new RecodeDataCollectionName();
		recodeDataCollectionName2.setName("TEST6");
		recodeDataCollectionNameDao.create(recodeDataCollectionName2);

		String readableSsin1 = readableIdentifierService.getReadableSsin("TEST5", "encodedSsin1");
		String readableSsin2 = readableIdentifierService.getReadableSsin("TEST5", "encodedSsin2");
		String readableSsin3 = readableIdentifierService.getReadableSsin("TEST5", "encodedSsin3");
		String readableSsin4 = readableIdentifierService.getReadableSsin("TEST6", "encodedSsin1");


		Hd4ResSsin hd4ResSsin1 = hd4ResSsinDao.get(readableSsin1);
		hd4ResSsin1.setOldPatientIds("oldId1");
		hd4ResSsinDao.update(hd4ResSsin1);

		Hd4ResSsin hd4ResSsin2 = hd4ResSsinDao.get(readableSsin2);
		hd4ResSsin2.setOldPatientIds("oldId12");
		hd4ResSsinDao.update(hd4ResSsin2);

		Hd4ResSsin hd4ResSsin3 = hd4ResSsinDao.get(readableSsin3);
		hd4ResSsin3.setOldPatientIds("oldId1|oldId2");
		hd4ResSsinDao.update(hd4ResSsin3);

		Hd4ResSsin hd4ResSsin4 = hd4ResSsinDao.get(readableSsin4);
		hd4ResSsin4.setOldPatientIds("oldId1");
		hd4ResSsinDao.update(hd4ResSsin4);


		readableIdentifierService.markOtherRecordsWithOldPatientId(readableSsin1, "TEST5", "oldId1");

		hd4ResSsin1 = hd4ResSsinDao.get(readableSsin1);
		hd4ResSsin2 = hd4ResSsinDao.get(readableSsin2);
		hd4ResSsin3 = hd4ResSsinDao.get(readableSsin3);
		hd4ResSsin4 = hd4ResSsinDao.get(readableSsin4);

		Assert.assertNotEquals("M", hd4ResSsin1.getRecodeStatus());
		Assert.assertNotEquals("M", hd4ResSsin2.getRecodeStatus());
		Assert.assertEquals("M", hd4ResSsin3.getRecodeStatus());
		Assert.assertNotEquals("M", hd4ResSsin4.getRecodeStatus());
	}

}
