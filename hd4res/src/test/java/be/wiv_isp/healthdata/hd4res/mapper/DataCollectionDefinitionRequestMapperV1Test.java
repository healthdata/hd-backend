/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.mapper;


import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.text.MessageFormat;

public class DataCollectionDefinitionRequestMapperV1Test {

    @Rule
    public ExpectedException ex = ExpectedException.none();

    @Test
    public void testInvalidKeyCreateOperation() throws Exception {
        JSONObject json = new JSONObject("{\"minorVersion\": 2, \"dataCollectionDefinitionId\": {}}");
        ex.expect(HealthDataException.class);
        ex.expectMessage(MessageFormat.format(ExceptionType.INVALID_FORMAT.getMessage(), "{}", "long"));
        DataCollectionDefinitionRequestMapper.convert(json);
    }


}
