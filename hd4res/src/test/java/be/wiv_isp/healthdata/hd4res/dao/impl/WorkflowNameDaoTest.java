/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.hd4res.dao.IWorkflowNameDao;
import be.wiv_isp.healthdata.hd4res.domain.WorkflowName;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class WorkflowNameDaoTest {

	@Autowired
	private IWorkflowNameDao dao;

	private WorkflowName workflowName1;
	private WorkflowName workflowName2;
	private WorkflowName workflowName3;

	@Before
	public void setup() {
		workflowName1 = new WorkflowName();
		workflowName1.setDefinitionName("testName1");
		workflowName1.setNumber(1L);

		dao.create(workflowName1);

		workflowName2 = new WorkflowName();
		workflowName2.setDefinitionName("testName1");
		workflowName2.setNumber(2L);

		dao.create(workflowName2);

		workflowName3 = new WorkflowName();
		workflowName3.setDefinitionName("testName2");
		workflowName3.setNumber(1L);

		dao.create(workflowName3);
	}

	@Test
	public void testGetBiggest1() {
		Long biggest = dao.getBiggest("testName1");
		Assert.assertEquals(2L, biggest.longValue());
	}

	@Test
	public void testGetBiggest2() {
		Long biggest = dao.getBiggest("testName2");
		Assert.assertEquals(1L, biggest.longValue());
	}

	@Test
	public void testGetBiggest3() {
		Long biggest = dao.getBiggest("testName3");
		Assert.assertNull(biggest);
	}
}
