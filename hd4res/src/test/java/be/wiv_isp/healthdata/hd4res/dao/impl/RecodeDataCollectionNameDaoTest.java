/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.hd4res.dao.IRecodeDataCollectionNameDao;
import be.wiv_isp.healthdata.hd4res.domain.RecodeDataCollectionName;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class RecodeDataCollectionNameDaoTest {

	@Autowired
	private IRecodeDataCollectionNameDao dao;

	private RecodeDataCollectionName recodeDataCollectionName1;
	private RecodeDataCollectionName recodeDataCollectionName2;
	private RecodeDataCollectionName recodeDataCollectionName3;

	private void setup() {
		recodeDataCollectionName1 = new RecodeDataCollectionName();
		recodeDataCollectionName1.setName("NAME_1");

		recodeDataCollectionName2 = new RecodeDataCollectionName();
		recodeDataCollectionName2.setName("NAME_2");

		recodeDataCollectionName3 = new RecodeDataCollectionName();
		recodeDataCollectionName3.setName("NAME_3");

		recodeDataCollectionName1 = dao.create(recodeDataCollectionName1);
		recodeDataCollectionName2 = dao.create(recodeDataCollectionName2);
		recodeDataCollectionName3 = dao.create(recodeDataCollectionName3);
	}

	@Test
	public void testGetAll() {
		setup();
		List<RecodeDataCollectionName> all = dao.getAll();
		Assert.assertEquals(3, all.size());
		Assert.assertTrue(all.contains(recodeDataCollectionName1));
		Assert.assertTrue(all.contains(recodeDataCollectionName2));
		Assert.assertTrue(all.contains(recodeDataCollectionName3));
	}
}
