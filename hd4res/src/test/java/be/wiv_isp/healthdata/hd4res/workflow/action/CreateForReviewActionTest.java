/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.workflow.action;

import be.wiv_isp.healthdata.orchestration.dao.impl.OrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.hd4res.action.workflow.CreateForReviewRegistrationAction;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.User;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-hd4res-test-service.xml" })
@Transactional
@Ignore
public class CreateForReviewActionTest {

	@Autowired
	private CreateForReviewRegistrationAction action;

	@Autowired
	private OrganizationDao organizationDao;

	@Test
	public void testExecute() throws Exception {
		Organization organization = new Organization();
		organization.setMain(true);
		organization.setHealthDataIDType("RIZIV");
		organization.setHealthDataIDValue("11111111");
		organization.setName("Main Organization");
		organization.setDeleted(false);
		organizationDao.create(organization);

		HealthDataIdentification identification = new HealthDataIdentification();
		identification.setType("RIZIV");
		identification.setValue("11111111");
		action.setDataCollectionDefinitionId(1L);
		action.setIdentificationWorkflow(identification);
		//action.setEhBoxCodedNiss("ehBoxCodedNiss1");

		User user = new User();
		RegistrationWorkflow result1 = action.execute(null, user);

		RegistrationWorkflow expected1 = new RegistrationWorkflow();
		expected1.setId(1L);
		Map<String, String> expected1MetaData = new HashMap<>();
		expected1MetaData.put(AbstractRegistrationWorkflow.MetaData.CODED_PATIENT_IDENTIFIER, "TEST_PATIENTID_0000-0001");
		expected1.setMetaData(expected1MetaData);
		expected1.setOrganization(organization);
		expected1.setReadableId("TEST_0000-0001");
		expected1.setDataCollectionDefinitionId(1L);
		expected1.setDataCollectionName("TEST");

		Assert.assertEquals(expected1, result1);

		//action.setEhBoxCodedNiss("ehBoxCodedNiss2");
		RegistrationWorkflow result2 = action.execute(expected1, user);

		RegistrationWorkflow expected2 = new RegistrationWorkflow();
		expected2.setId(1L);
		Map<String, String> expected2MetaData = new HashMap<>();
		expected2MetaData.put(AbstractRegistrationWorkflow.MetaData.CODED_PATIENT_IDENTIFIER, "TEST_PATIENTID_0000-0002");
		expected2MetaData.put(AbstractRegistrationWorkflow.MetaData.OLD_CODED_PATIENT_IDENTIFIER, "TEST_PATIENTID_0000-0001");
		expected2.setMetaData(expected2MetaData);
		expected2.setOrganization(organization);
		expected2.setReadableId("TEST_0000-0001");
		expected2.setDataCollectionDefinitionId(1L);
		expected2.setDataCollectionName("TEST");

		Assert.assertEquals(expected2, result2);

		//action.setEhBoxCodedNiss("ehBoxCodedNiss1");
		RegistrationWorkflow result3 = action.execute(expected2, user);

		RegistrationWorkflow expected3 = new RegistrationWorkflow();
		expected3.setId(1L);
		Map<String, String> expected3MetaData = new HashMap<>();
		expected3MetaData.put(AbstractRegistrationWorkflow.MetaData.CODED_PATIENT_IDENTIFIER, "TEST_PATIENTID_0000-0001");
		expected3MetaData.put(AbstractRegistrationWorkflow.MetaData.OLD_CODED_PATIENT_IDENTIFIER, "TEST_PATIENTID_0000-0001|TEST_PATIENTID_0000-0002");
		expected3.setMetaData(expected3MetaData);
		expected3.setOrganization(organization);
		expected3.setReadableId("TEST_0000-0001");
		expected3.setDataCollectionDefinitionId(1L);
		expected3.setDataCollectionName("TEST");

		Assert.assertEquals(expected3, result3);

	}

}
