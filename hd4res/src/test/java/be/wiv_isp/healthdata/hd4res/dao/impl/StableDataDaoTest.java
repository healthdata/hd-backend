/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.hd4res.dao.IStableDataDao;
import be.wiv_isp.healthdata.hd4res.domain.StableData;
import be.wiv_isp.healthdata.hd4res.domain.StableDataCountResult;
import be.wiv_isp.healthdata.hd4res.domain.search.StableDataSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class StableDataDaoTest {

	@Autowired
	private IStableDataDao dao;

	@Test
	public void testSearchByDataCollectionName() {
		StableData stableData1 = createStableData();
		stableData1 = dao.create(stableData1);

		StableData stableData2 = createStableData();
		stableData2.setDataCollectionName("FOO");
		stableData2 = dao.create(stableData2);

		Assert.assertEquals("TEST", stableData1.getDataCollectionName());

		final StableDataSearch search = new StableDataSearch();
		search.setDataCollectionName("TEST");
		final List<StableData> all = dao.getAll(search);

		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(stableData1));
		Assert.assertFalse(all.contains(stableData2));
	}

	@Test
 	public void testSearchByStatus() {
		StableData stableData1 = createStableData();
		stableData1 = dao.create(stableData1);

		StableData stableData2 = createStableData();
		stableData2.setStatus(StableData.Status.SENT);
		stableData2 = dao.create(stableData2);

		Assert.assertEquals(StableData.Status.UPLOADED, stableData1.getStatus());

		final StableDataSearch search = new StableDataSearch();
		search.setStatus(StableData.Status.UPLOADED);
		final List<StableData> all = dao.getAll(search);

		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(stableData1));
		Assert.assertFalse(all.contains(stableData2));
	}

	@Test
	public void testSearchByIdentificationType() {
		StableData stableData1 = createStableData();
		stableData1 = dao.create(stableData1);

		StableData stableData2 = createStableData();
		stableData2.setIdentificationType("FOO");
		stableData2 = dao.create(stableData2);

		Assert.assertEquals("RIZIV", stableData1.getIdentificationType());

		final StableDataSearch search = new StableDataSearch();
		search.setIdentificationType("RIZIV");
		final List<StableData> all = dao.getAll(search);

		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(stableData1));
		Assert.assertFalse(all.contains(stableData2));
	}

	@Test
	public void testSearchByIdentificationValue() {
		StableData stableData1 = createStableData();
		stableData1 = dao.create(stableData1);

		StableData stableData2 = createStableData();
		stableData2.setIdentificationValue("22222222");
		stableData2 = dao.create(stableData2);

		Assert.assertEquals("11111111", stableData1.getIdentificationValue());

		final StableDataSearch search = new StableDataSearch();
		search.setIdentificationValue("11111111");
		final List<StableData> all = dao.getAll(search);

		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(stableData1));
		Assert.assertFalse(all.contains(stableData2));
	}

	@Test
	public void testCountGroupBy() {
		persistStableData("11111111", "TEST_A", StableData.Status.UPLOADED);

		persistStableData("11111111", "TEST_A", StableData.Status.UPLOADED);
		persistStableData("11111111", "TEST_A", StableData.Status.SENT);
		persistStableData("11111111", "TEST_B", StableData.Status.UPLOADED);
		persistStableData("11111111", "TEST_B", StableData.Status.SENT);
		persistStableData("22222222", "TEST_A", StableData.Status.UPLOADED);
		persistStableData("22222222", "TEST_A", StableData.Status.SENT);
		persistStableData("22222222", "TEST_B", StableData.Status.UPLOADED);
		persistStableData("22222222", "TEST_B", StableData.Status.SENT);

		List<StableDataCountResult> counts = dao.getCounts(null);
		List<StableDataCountResult> expectedCounts = new ArrayList<>();
		expectedCounts.add(new StableDataCountResult("11111111", "TEST_A", StableData.Status.UPLOADED, 2L));
		expectedCounts.add(new StableDataCountResult("11111111", "TEST_A", StableData.Status.SENT, 1L));
		expectedCounts.add(new StableDataCountResult("11111111", "TEST_B", StableData.Status.UPLOADED, 1L));
		expectedCounts.add(new StableDataCountResult("11111111", "TEST_B", StableData.Status.SENT, 1L));
		expectedCounts.add(new StableDataCountResult("22222222", "TEST_A", StableData.Status.UPLOADED, 1L));
		expectedCounts.add(new StableDataCountResult("22222222", "TEST_A", StableData.Status.SENT, 1L));
		expectedCounts.add(new StableDataCountResult("22222222", "TEST_B", StableData.Status.UPLOADED, 1L));
		expectedCounts.add(new StableDataCountResult("22222222", "TEST_B", StableData.Status.SENT, 1L));
		Assert.assertTrue(expectedCounts.containsAll(counts) && counts.containsAll(expectedCounts));

		final StableDataSearch search = new StableDataSearch();
		search.setIdentificationValue("11111111");
		counts = dao.getCounts(search);
		expectedCounts = new ArrayList<>();
		expectedCounts.add(new StableDataCountResult("11111111", "TEST_A", StableData.Status.UPLOADED, 2L));
		expectedCounts.add(new StableDataCountResult("11111111", "TEST_A", StableData.Status.SENT, 1L));
		expectedCounts.add(new StableDataCountResult("11111111", "TEST_B", StableData.Status.UPLOADED, 1L));
		expectedCounts.add(new StableDataCountResult("11111111", "TEST_B", StableData.Status.SENT, 1L));
		Assert.assertTrue(expectedCounts.containsAll(counts) && counts.containsAll(expectedCounts));
	}

	private StableData createStableData() {
		final StableData stableData = new StableData();
		stableData.setPatientId("123456789");
		stableData.setStatus(StableData.Status.UPLOADED);
		stableData.setIdentificationValue("11111111");
		stableData.setIdentificationType("RIZIV");
		stableData.setDataCollectionName("TEST");
		stableData.setCsv("h1;h2;h3\\nd1;d2;d3".getBytes(StandardCharsets.UTF_8));
		return stableData;
	}

	private StableData persistStableData(String identificationValue, String dataCollectionName, StableData.Status status) {
		final StableData stableData;
		stableData= createStableData();
		stableData.setIdentificationValue(identificationValue);
		stableData.setDataCollectionName(dataCollectionName);
		stableData.setStatus(status);
		return dao.create(stableData);
	}
}
