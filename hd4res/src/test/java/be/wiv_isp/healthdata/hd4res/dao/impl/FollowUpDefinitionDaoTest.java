/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.hd4res.dao.IDataCollectionDefinitionDao;
import be.wiv_isp.healthdata.hd4res.dao.IDataCollectionDefinitionHistoryDao;
import be.wiv_isp.healthdata.hd4res.dao.IDataCollectionGroupDao;
import be.wiv_isp.healthdata.hd4res.dao.IFollowUpDefinitionDao;
import be.wiv_isp.healthdata.hd4res.domain.Description;
import be.wiv_isp.healthdata.hd4res.domain.Label;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionDefinition;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionDefinitionHistory;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionGroup;
import be.wiv_isp.healthdata.hd4res.domain.dcd.FollowUpDefinition;
import be.wiv_isp.healthdata.hd4res.domain.search.FollowUpDefinitionSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

@Transactional
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class FollowUpDefinitionDaoTest {

	@Autowired
	private IFollowUpDefinitionDao dao;
	@Autowired
	private IDataCollectionDefinitionHistoryDao historyDao;
	@Autowired
	private IDataCollectionDefinitionDao dataCollectionDefinitionDao;
	@Autowired
	private IDataCollectionGroupDao dataCollectionGroupDao;

	private DataCollectionDefinition dcd1;
	private DataCollectionDefinition dcd2;
	private DataCollectionGroup dcg1;

	@Test
 	public void testSearchByDataCollectionDefinitionId() {
		DataCollectionDefinitionHistory history1 = getHistory();
		final FollowUpDefinition fud1 = createFollowUpDefinition(history1);
		final FollowUpDefinition created1 = dao.create(fud1);
		Assert.assertNotNull(created1);

		DataCollectionDefinitionHistory history2 = getHistory(2);
		final FollowUpDefinition fud2 = createFollowUpDefinition(history2);
		final FollowUpDefinition created2 = dao.create(fud2);
		Assert.assertNotNull(created1);

		FollowUpDefinitionSearch search = new FollowUpDefinitionSearch();
		List<FollowUpDefinition> all = dao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new FollowUpDefinitionSearch();
		search.setDataCollectionDefinitionHistoryId(history1.getId());
		all = dao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	@Test
	public void testSearchByName() {
		DataCollectionDefinitionHistory history = getHistory();
		final FollowUpDefinition fud1 = createFollowUpDefinition(history);
		final FollowUpDefinition created1 = dao.create(fud1);
		Assert.assertNotNull(created1);

		final FollowUpDefinition fud2 = createFollowUpDefinition(history);
		fud2.setName("MyFollowUp-1");
		final FollowUpDefinition created2 = dao.create(fud2);
		Assert.assertNotNull(created1);

		Assert.assertNotEquals(created1.getName(), created2.getName());

		FollowUpDefinitionSearch search = new FollowUpDefinitionSearch();
		List<FollowUpDefinition> all = dao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new FollowUpDefinitionSearch();
		search.setName("MyFollowUp");
		all = dao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	private FollowUpDefinition createFollowUpDefinition(DataCollectionDefinitionHistory history) {
		final FollowUpDefinition fud = new FollowUpDefinition();
		fud.setName("MyFollowUp");
		fud.setDescription("Description");
		fud.setLabel("MyLabel");
		fud.setTiming("7d");
		fud.setConditions(Arrays.asList("condition-1", "condition-2"));
		fud.setDataCollectionDefinitionHistoryId(history.getId());
		return fud;
	}


	private DataCollectionDefinitionHistory getHistory() {
		return getHistory(1);
	}
	private DataCollectionDefinitionHistory getHistory(int id) {
		DataCollectionDefinition dcd = getDataCollectionDefinition(id);
		DataCollectionDefinitionHistory history = new DataCollectionDefinitionHistory();
		history.setDataCollectionDefinitionId(dcd.getId());
		history.setContent("someContent".getBytes(StandardCharsets.UTF_8));
		historyDao.create(history);
		return history;
	}

	private DataCollectionDefinition getDataCollectionDefinition(int id) {
		if(id == 1) {
			if(dcd1 == null) {
				final DataCollectionDefinition dcd = new DataCollectionDefinition();
				dcd.setDataCollectionName("TEST");
				dcd.setDescription(new Description("Description", "Description", "Description"));
				dcd.setLabel(new Label("label", "label", "label"));
				dcd.setMinorVersion(0);
				dcd.setDataCollectionGroupId(getDataCollectionGroup().getId());
				dcd1 = dataCollectionDefinitionDao.create(dcd);
			}
			return dcd1;
		}
		if(id == 2) {
			if (dcd2 == null) {
				final DataCollectionDefinition dcd = new DataCollectionDefinition();
				dcd.setDataCollectionName("TEST");
				dcd.setDescription(new Description("Description", "Description", "Description"));
				dcd.setLabel(new Label("label", "label", "label"));
				dcd.setMinorVersion(1);
				dcd.setDataCollectionGroupId(getDataCollectionGroup().getId());
				dcd2 = dataCollectionDefinitionDao.create(dcd);
			}
			return dcd2;
		}
		return null;
	}

	private DataCollectionGroup getDataCollectionGroup() {
		if(dcg1 == null) {
			DataCollectionGroup dcg = new DataCollectionGroup();
			dcg.setName("GROUP");
			dcg.setDescription(new Description("Description", "Description", "Description"));
			dcg.setLabel(new Label("label", "label", "label"));
			dcg.setMajorVersion(1);
			dcg1 = dataCollectionGroupDao.create(dcg);
		}
		return dcg1;
	}
}
