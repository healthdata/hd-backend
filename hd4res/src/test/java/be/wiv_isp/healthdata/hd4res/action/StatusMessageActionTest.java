/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.action;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.hd4res.action.standalone.StatusMessageAction;
import be.wiv_isp.healthdata.hd4res.domain.StatusMessage;
import be.wiv_isp.healthdata.orchestration.action.StatusMessageContent;
import be.wiv_isp.healthdata.orchestration.domain.validator.IStatusMessageValidator;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import be.wiv_isp.healthdata.orchestration.service.IStatusMessageService;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

@Ignore
public class StatusMessageActionTest {

    final StatusMessageAction action = new StatusMessageAction();

    private IStatusMessageService<StatusMessage> statusMessageService = EasyMock.createNiceMock(IStatusMessageService.class);
    private IStatusMessageValidator statusMessageValidator = EasyMock.createNiceMock(IStatusMessageValidator.class);
    private IOrganizationService organizationService = EasyMock.createNiceMock(IOrganizationService.class);

    @Before
    public void before() {
        ReflectionTestUtils.setField(action, "statusMessageService", statusMessageService);
        ReflectionTestUtils.setField(action, "statusMessageValidator", statusMessageValidator);
        ReflectionTestUtils.setField(action, "organizationService", organizationService);
    }

    @Test
    public void test() {
        action.setContent(new StatusMessageContent());
        /*action.setIdentification(new IdentificationBuilder()
            .withType("NIHII-HOSPITAL")
            .withValue("11111111")
            .withApplicationId("HEALTHDATA")
            .build());*/

        HealthDataIdentification identification = new HealthDataIdentification();
        identification.setType("RIZIV");
        identification.setValue("11111111");
        identification.setName("Organization name");
        identification.setHd4prc(false);


        action.setHealthDataIdentification(identification);
        //action.setSentOn(new Date());

        final StatusMessage statusMessage = new StatusMessage();
        statusMessage.setContent(action.getContent());
        statusMessage.setHealthDataIdentification(action.getHealthDataIdentification());
        //statusMessage.setSentOn(new Timestamp(action.getSentOn().getTime()));

        statusMessageValidator.validate(statusMessage);
        EasyMock.expectLastCall();
        EasyMock.expect(statusMessageService.create(statusMessage)).andReturn(statusMessage);

        EasyMock.replay(statusMessageService, statusMessageValidator, organizationService);
        action.execute();
        EasyMock.verify(statusMessageService, statusMessageValidator, organizationService);
    }
}
