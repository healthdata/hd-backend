/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import be.wiv_isp.healthdata.hd4res.dao.IWorkflowNameDao;
import be.wiv_isp.healthdata.hd4res.domain.WorkflowName;
import be.wiv_isp.healthdata.hd4res.service.IReadableIdentifierService;

public class WorkflowNameTest {

	@Autowired
	private final IReadableIdentifierService readableIdentifierService = new ReadableIdentifierService();

	private IWorkflowNameDao workflowNameDao;

	@Before
	public void setup() {
		workflowNameDao = EasyMock.createMock(IWorkflowNameDao.class);
		ReflectionTestUtils.setField(readableIdentifierService, "workflowNameDao", workflowNameDao);
	}

	@Test
	public void testCreateWorkflowName1() throws Exception {
		expect(workflowNameDao.getBiggest("HIV")).andReturn(1L);
		expect(workflowNameDao.create((WorkflowName) EasyMock.anyObject())).andReturn(new WorkflowName());
		replay(workflowNameDao);

		String workflowName = readableIdentifierService.createWorkflowName("HIV");

		Assert.assertEquals("HIV_0000-0002", workflowName);

		verify(workflowNameDao);
	}

	@Test
	public void testCreateWorkflowName2() throws Exception {
		expect(workflowNameDao.getBiggest("HIV")).andReturn(null);
		expect(workflowNameDao.create((WorkflowName) EasyMock.anyObject())).andReturn(new WorkflowName());
		replay(workflowNameDao);

		String workflowName = readableIdentifierService.createWorkflowName("HIV");

		Assert.assertEquals("HIV_0000-0001", workflowName);

		verify(workflowNameDao);
	}
}
