/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.common.dto.TranslatableStringDto;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.hd4res.domain.Description;
import be.wiv_isp.healthdata.hd4res.domain.dcd.DataCollectionDefinition;
import be.wiv_isp.healthdata.hd4res.domain.search.DataCollectionDefinitionSearch;
import be.wiv_isp.healthdata.hd4res.dto.DataCollectionDefinitionManagementDto;
import be.wiv_isp.healthdata.hd4res.service.IDataCollectionDefinitionService;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IWebServiceClientService;
import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONObject;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Ignore;
import org.springframework.test.util.ReflectionTestUtils;

@Ignore
public class DataCollectionDefinitionManagementServiceTest {

	final private DataCollectionDefinitionManagementService dataCollectionDefinitionManagementService = new DataCollectionDefinitionManagementService();

	private final IWebServiceClientService webServiceClientService = EasyMock.createMock(IWebServiceClientService.class);
	private final IConfigurationService configurationService = EasyMock.createMock(IConfigurationService.class);
	private final IDataCollectionDefinitionService dataCollectionDefinitionService = EasyMock.createMock(IDataCollectionDefinitionService.class);

	// @Before
	public void before() {
		ReflectionTestUtils.setField(dataCollectionDefinitionManagementService, "webServiceClientService", webServiceClientService);
		ReflectionTestUtils.setField(dataCollectionDefinitionManagementService, "configurationService", configurationService);
		ReflectionTestUtils.setField(dataCollectionDefinitionManagementService, "dataCollectionDefinitionService", dataCollectionDefinitionService);
	}

	// @Test
	public void testCreate() throws Exception {
		String testDcd1_0JsonString = IOUtils.toString(DataCollectionDefinitionManagementServiceTest.class.getResourceAsStream("testDcd1_0.json"));
		String testDcd1_1JsonString = IOUtils.toString(DataCollectionDefinitionManagementServiceTest.class.getResourceAsStream("testDcd1_1.json"));
		String testDcd2_0JsonString = IOUtils.toString(DataCollectionDefinitionManagementServiceTest.class.getResourceAsStream("testDcd2_0.json"));
		// combined mocks
		Configuration catalogueHost = new Configuration();
		catalogueHost.setKey(ConfigurationKey.CATALOGUE_HOST);
		catalogueHost.setValue("http://test:8080/healthdata_catalogue");
		Configuration catalogueUsername = new Configuration();
		catalogueUsername.setKey(ConfigurationKey.CATALOGUE_USERNAME);
		catalogueUsername.setValue("username");
		Configuration cataloguePassword = new Configuration();
		cataloguePassword.setKey(ConfigurationKey.CATALOGUE_PASSWORD);
		cataloguePassword.setValue("password");
		EasyMock.expect(configurationService.get(ConfigurationKey.CATALOGUE_HOST)).andReturn(catalogueHost).times(3);
		EasyMock.expect(configurationService.get(ConfigurationKey.CATALOGUE_USERNAME)).andReturn(catalogueUsername).times(3);
		EasyMock.expect(configurationService.get(ConfigurationKey.CATALOGUE_PASSWORD)).andReturn(cataloguePassword).times(3);
		EasyMock.expect(webServiceClientService.callWebService(EasyMock.anyObject(WebServiceBuilder.class))).andReturn(EasyMock.anyObject(DataCollectionDefinitionDtoV7.class)).times(3);

		// mock 1.0
		DataCollectionDefinition beforeCreate1_0 = getTest1_0();

		DataCollectionDefinition afterCreate1_0 = getTest1_0();
		afterCreate1_0.setId(1L);

		DataCollectionDefinition beforeUpdate1_0 = getTest1_0();
		beforeUpdate1_0.setId(1L);
		beforeUpdate1_0.setStandAloneCatalogueId(1L);

		DataCollectionDefinition afterUpdate1_0 = getTest1_0();
		afterUpdate1_0.setId(1L);
		afterUpdate1_0.setStandAloneCatalogueId(1L);

		EasyMock.expect(dataCollectionDefinitionService.create(beforeCreate1_0)).andReturn(afterCreate1_0);
		EasyMock.expect(dataCollectionDefinitionService.update(beforeUpdate1_0)).andReturn(afterUpdate1_0);

		// mock 1.1
		DataCollectionDefinitionSearch search = new DataCollectionDefinitionSearch();
		search.setDataCollectionDefinitionName("TEST");
//		Version version = new Version();
//		version.setMajor(1);
//		version.setMinor(0);
//		search.setVersion(version);

		DataCollectionDefinition beforeCreate1_1 = getTest1_1();
		beforeCreate1_1.setStandAloneCatalogueId(1L);

		DataCollectionDefinition afterCreate1_1 = getTest1_1();
		afterCreate1_1.setId(2L);
		afterCreate1_1.setStandAloneCatalogueId(1L);

		DataCollectionDefinition beforeUpdate1_1 = getTest1_0();
		beforeUpdate1_1.setId(1L);

		DataCollectionDefinition afterUpdate1_1 = getTest1_0();
		afterUpdate1_1.setId(1L);

		EasyMock.expect(dataCollectionDefinitionService.getUnique(search)).andReturn(afterUpdate1_0);
		EasyMock.expect(dataCollectionDefinitionService.create(beforeCreate1_1)).andReturn(afterCreate1_1);
		EasyMock.expect(dataCollectionDefinitionService.update(beforeUpdate1_1)).andReturn(afterUpdate1_1);

		// mock 2.0
		DataCollectionDefinition beforeCreate2_0 = getTest2_0();

		DataCollectionDefinition afterCreate2_0 = getTest2_0();
		afterCreate2_0.setId(3L);

		DataCollectionDefinition beforeUpdate2_0 = getTest2_0();
		beforeUpdate2_0.setId(3L);
		beforeUpdate2_0.setStandAloneCatalogueId(3L);

		DataCollectionDefinition afterUpdate2_0 = getTest2_0();
		afterUpdate2_0.setId(3L);
		afterUpdate2_0.setStandAloneCatalogueId(3L);

		EasyMock.expect(dataCollectionDefinitionService.create(beforeCreate2_0)).andReturn(afterCreate2_0);
		EasyMock.expect(dataCollectionDefinitionService.update(beforeUpdate2_0)).andReturn(afterUpdate2_0);

		EasyMock.replay(webServiceClientService, configurationService, dataCollectionDefinitionService);

		DataCollectionDefinitionManagementDto dcd1_0 = dataCollectionDefinitionManagementService.create(new JSONObject(testDcd1_0JsonString), "user");

		DataCollectionDefinitionManagementDto expected1_0 = new DataCollectionDefinitionManagementDto();
		expected1_0.setDataCollectionDefinitionId(1L);
		expected1_0.setDataCollectionDefinitionName("TEST");
		TranslatableStringDto descriptionDto1_0 = new TranslatableStringDto();
		descriptionDto1_0.setEn("CREATE");
		descriptionDto1_0.setFr("CREATE");
		descriptionDto1_0.setNl("CREATE");
		expected1_0.setDescription(descriptionDto1_0);
//		VersionDto version1_0 = new VersionDto();
//		version1_0.setMajor(1);
//		version1_0.setMinor(0);
//		expected1_0.setVersion(version1_0);
		Assert.assertEquals(expected1_0, dcd1_0);

		DataCollectionDefinitionManagementDto dcd1_1 = dataCollectionDefinitionManagementService.create(new JSONObject(testDcd1_1JsonString), "user");

		DataCollectionDefinitionManagementDto expected1_1 = new DataCollectionDefinitionManagementDto();
		expected1_1.setDataCollectionDefinitionId(1L);
		expected1_1.setDataCollectionDefinitionName("TEST");
		TranslatableStringDto descriptionDto1_1 = new TranslatableStringDto();
		descriptionDto1_1.setEn("UPDATE MINOR");
		descriptionDto1_1.setFr("UPDATE MINOR");
		descriptionDto1_1.setNl("UPDATE MINOR");
		expected1_1.setDescription(descriptionDto1_1);
//		VersionDto version1_1 = new VersionDto();
//		version1_1.setMajor(1);
//		version1_1.setMinor(1);
//		expected1_1.setVersion(version1_1);
		Assert.assertEquals(expected1_1, dcd1_1);

		DataCollectionDefinitionManagementDto dcd2_0 = dataCollectionDefinitionManagementService.create(new JSONObject(testDcd2_0JsonString), "user");

		DataCollectionDefinitionManagementDto expected2_0 = new DataCollectionDefinitionManagementDto();
		expected2_0.setDataCollectionDefinitionId(1L);
		expected2_0.setDataCollectionDefinitionName("TEST");
		TranslatableStringDto descriptionDto2_0 = new TranslatableStringDto();
		descriptionDto2_0.setEn("UPDATE MINOR");
		descriptionDto2_0.setFr("UPDATE MINOR");
		descriptionDto2_0.setNl("UPDATE MINOR");
		expected2_0.setDescription(descriptionDto2_0);
//		VersionDto version2_0 = new VersionDto();
//		version2_0.setMajor(2);
//		version2_0.setMinor(0);
//		expected2_0.setVersion(version2_0);
		Assert.assertEquals(expected2_0, dcd2_0);

		EasyMock.verify(webServiceClientService, configurationService, dataCollectionDefinitionService);
	}

	private DataCollectionDefinition getTest1_0() {
		DataCollectionDefinition dataCollectionDefinition = getTestDataCollectionDefinition();
//		Version version = new Version();
//		version.setMajor(1);
//		version.setMinor(0);
//		dataCollectionDefinition.setVersion(version);
		Description description = new Description();
		description.setEn("CREATE");
		description.setFr("CREATE");
		description.setNl("CREATE");
		dataCollectionDefinition.setDescription(description);
		return dataCollectionDefinition;
	}

	private DataCollectionDefinition getTest1_1() {
		DataCollectionDefinition dataCollectionDefinition = getTestDataCollectionDefinition();
//		Version version = new Version();
//		version.setMajor(1);
//		version.setMinor(1);
//		dataCollectionDefinition.setVersion(version);
		Description description = new Description();
		description.setEn("UPDATE MINOR");
		description.setFr("UPDATE MINOR");
		description.setNl("UPDATE MINOR");
		dataCollectionDefinition.setDescription(description);
		return dataCollectionDefinition;
	}

	private DataCollectionDefinition getTest2_0() {
		DataCollectionDefinition dataCollectionDefinition = getTestDataCollectionDefinition();
//		Version version = new Version();
//		version.setMajor(2);
//		version.setMinor(0);
//		dataCollectionDefinition.setVersion(version);
		Description description = new Description();
		description.setEn("UPDATE MAJOR");
		description.setFr("UPDATE MAJOR");
		description.setNl("UPDATE MAJOR");
		dataCollectionDefinition.setDescription(description);
		return dataCollectionDefinition;
	}

	private DataCollectionDefinition getTestDataCollectionDefinition() {
		DataCollectionDefinition dataCollectionDefinition = new DataCollectionDefinition();
		dataCollectionDefinition.setDataCollectionName("TEST");
		return dataCollectionDefinition;
	}

}
