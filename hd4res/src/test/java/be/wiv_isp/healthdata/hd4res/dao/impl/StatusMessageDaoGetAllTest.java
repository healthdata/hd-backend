/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.dao.impl;

import be.wiv_isp.healthdata.hd4res.domain.StatusMessage;
import be.wiv_isp.healthdata.orchestration.action.StatusMessageContent;
import be.wiv_isp.healthdata.orchestration.dao.IStatusMessageDao;
import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.orchestration.domain.search.StatusMessageSearch;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class StatusMessageDaoGetAllTest {

	@Autowired
	private IStatusMessageDao<StatusMessage> dao;

	private StatusMessage statusMessage1Date1;
	private StatusMessage statusMessage1Date2;
	private StatusMessage statusMessage1Date3;

	private StatusMessage statusMessage2Date1;
	private StatusMessage statusMessage2Date2;

	private StatusMessage statusMessage3Date2;
	private StatusMessage statusMessage3Date3;

	private Date date1;
	private Date date2;
	private Date date3;

	@Before
	public void setUp() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

		date1 = sdf.parse("01/01/2015 09:00");
		date2 = sdf.parse("02/01/2015 09:00");
		date3 = sdf.parse("03/01/2015 09:00");

		HealthDataIdentification identification1 = new HealthDataIdentification();
		HealthDataIdentification identification2 = new HealthDataIdentification();
		HealthDataIdentification identification3 = new HealthDataIdentification();

		identification1.setType("HD_ID_TYPE_1");
		identification1.setValue("HD_ID_VALUE_1");
		identification1.setName("NAME_1");
		identification1.setHd4prc(false);

		identification2.setType("HD_ID_TYPE_2");
		identification2.setValue("HD_ID_VALUE_2");
		identification2.setName("NAME_2");
		identification2.setHd4prc(false);

		identification3.setType("HD_ID_TYPE_3");
		identification3.setValue("HD_ID_VALUE_3");
		identification3.setName("NAME_3");
		identification3.setHd4prc(false);


		statusMessage1Date1 = new StatusMessage();
		statusMessage1Date1.setHealthDataIdentification(identification1);
		statusMessage1Date1.setContent(StatusMessageContent.create("{ \"content\" : \"content_1\"}"));
		statusMessage1Date1.setSentOn(new Timestamp(new Date().getTime()));

		statusMessage1Date2 = new StatusMessage();
		statusMessage1Date2.setHealthDataIdentification(identification1);
		statusMessage1Date2.setContent(StatusMessageContent.create("{ \"content\" : \"content_1\"}"));
		statusMessage1Date2.setSentOn(new Timestamp(new Date().getTime()));

		statusMessage1Date3 = new StatusMessage();
		statusMessage1Date3.setHealthDataIdentification(identification1);
		statusMessage1Date3.setContent(StatusMessageContent.create("{ \"content\" : \"content_1\"}"));
		statusMessage1Date3.setSentOn(new Timestamp(new Date().getTime()));

		statusMessage2Date1 = new StatusMessage();
		statusMessage2Date1.setHealthDataIdentification(identification2);
		statusMessage2Date1.setContent(StatusMessageContent.create("{ \"content\" : \"content_2\"}"));
		statusMessage2Date1.setSentOn(new Timestamp(new Date().getTime()));

		statusMessage2Date2 = new StatusMessage();
		statusMessage2Date2.setHealthDataIdentification(identification2);
		statusMessage2Date2.setContent(StatusMessageContent.create("{ \"content\" : \"content_2\"}"));
		statusMessage2Date2.setSentOn(new Timestamp(new Date().getTime()));

		statusMessage3Date2 = new StatusMessage();
		statusMessage3Date2.setHealthDataIdentification(identification3);
		statusMessage3Date2.setContent(StatusMessageContent.create("{ \"content\" : \"content_3\"}"));
		statusMessage3Date2.setSentOn(new Timestamp(new Date().getTime()));

		statusMessage3Date3 = new StatusMessage();
		statusMessage3Date3.setHealthDataIdentification(identification3);
		statusMessage3Date3.setContent(StatusMessageContent.create("{ \"content\" : \"content_3\"}"));
		statusMessage3Date3.setSentOn(new Timestamp(new Date().getTime()));

		statusMessage1Date1 = dao.create(statusMessage1Date1);
		statusMessage1Date1.setCreatedOn(new Timestamp(date1.getTime()));
		dao.update(statusMessage1Date1);
		statusMessage1Date2 = dao.create(statusMessage1Date2);
		statusMessage1Date2.setCreatedOn(new Timestamp(date2.getTime()));
		dao.update(statusMessage1Date2);
		statusMessage1Date3 = dao.create(statusMessage1Date3);
		statusMessage1Date3.setCreatedOn(new Timestamp(date3.getTime()));
		dao.update(statusMessage1Date3);

		statusMessage2Date1 = dao.create(statusMessage2Date1);
		statusMessage2Date1.setCreatedOn(new Timestamp(date1.getTime()));
		dao.update(statusMessage2Date1);
		statusMessage2Date2 = dao.create(statusMessage2Date2);
		statusMessage2Date2.setCreatedOn(new Timestamp(date2.getTime()));
		dao.update(statusMessage2Date2);

		statusMessage3Date2 = dao.create(statusMessage3Date2);
		statusMessage3Date2.setCreatedOn(new Timestamp(date2.getTime()));
		dao.update(statusMessage3Date2);
		statusMessage3Date3 = dao.create(statusMessage3Date3);
		statusMessage3Date3.setCreatedOn(new Timestamp(date3.getTime()));
		dao.update(statusMessage3Date3);

	}

	@Test
	public void testGetAll() {
		StatusMessageSearch search = new StatusMessageSearch();
		search.setLastOnly(true);
		List<StatusMessage> allForApi = dao.getAll(search);
		Assert.assertEquals(3, allForApi.size());
		Assert.assertTrue(allForApi.contains(statusMessage1Date3));
		Assert.assertTrue(allForApi.contains(statusMessage2Date2));
		Assert.assertTrue(allForApi.contains(statusMessage3Date3));
	}

	@Test
	public void testGetAllDate1() {
		StatusMessageSearch search = new StatusMessageSearch();
		search.setCreatedOn(date1);
		List<StatusMessage> allForApi = dao.getAll(search);
		Assert.assertEquals(2, allForApi.size());
		Assert.assertTrue(allForApi.contains(statusMessage1Date1));
		Assert.assertTrue(allForApi.contains(statusMessage2Date1));
	}

	@Test
	public void testGetAllDate2() {
		StatusMessageSearch search = new StatusMessageSearch();
		search.setCreatedOn(date2);
		List<StatusMessage> allForApi = dao.getAll(search);
		Assert.assertEquals(3, allForApi.size());
		Assert.assertTrue(allForApi.contains(statusMessage1Date2));
		Assert.assertTrue(allForApi.contains(statusMessage2Date2));
		Assert.assertTrue(allForApi.contains(statusMessage3Date2));
	}

	@Test
	public void testGetAllDate3() {
		StatusMessageSearch search = new StatusMessageSearch();
		search.setCreatedOn(date3);
		List<StatusMessage> allForApi = dao.getAll(search);
		Assert.assertEquals(2, allForApi.size());
		Assert.assertTrue(allForApi.contains(statusMessage1Date3));
		Assert.assertTrue(allForApi.contains(statusMessage3Date3));
	}

}
