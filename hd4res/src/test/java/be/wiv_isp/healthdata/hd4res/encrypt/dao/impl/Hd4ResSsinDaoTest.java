/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.encrypt.dao.impl;

import be.wiv_isp.healthdata.hd4res.domain.search.Hd4ResSsinSearch;
import be.wiv_isp.healthdata.hd4res.encrypt.dao.IHd4ResSsinDao;
import be.wiv_isp.healthdata.hd4res.encrypt.domain.Hd4ResSsin;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class Hd4ResSsinDaoTest {

	@Autowired
	private IHd4ResSsinDao dao;

	private Hd4ResSsin hd4ResSsin1;
	private Hd4ResSsin hd4ResSsin2;
	private Hd4ResSsin hd4ResSsin3;
	private Hd4ResSsin hd4ResSsin4;
	private Hd4ResSsin hd4ResSsin5;
	private Hd4ResSsin hd4ResSsin6;
	private Hd4ResSsin hd4ResSsin7;

	public void setup() {
		hd4ResSsin1 = new Hd4ResSsin();
		hd4ResSsin1.seteHealthCodedNissBatch("encodedSsin1");
		hd4ResSsin1.setRegisterName("TEST1");
		hd4ResSsin1.setLandingZoneCodedNiss("TEST1_PATIENTID_0000-0001");
		hd4ResSsin2 = new Hd4ResSsin();
		hd4ResSsin2.seteHealthCodedNissBatch("encodedSsin2");
		hd4ResSsin2.setRegisterName("TEST1");
		hd4ResSsin2.setLandingZoneCodedNiss("TEST1_PATIENTID_0000-0002");
		hd4ResSsin3 = new Hd4ResSsin();
		hd4ResSsin3.seteHealthCodedNissBatch("encodedSsin1");
		hd4ResSsin3.setRegisterName("TEST2");
		hd4ResSsin3.setLandingZoneCodedNiss("TEST2_PATIENTID_0000-0001");

		dao.create(hd4ResSsin1);
		dao.create(hd4ResSsin2);
		dao.create(hd4ResSsin3);
	}

	private void setup2() {
		hd4ResSsin1 = new Hd4ResSsin();
		hd4ResSsin1.setLandingZoneCodedNiss("1");
		hd4ResSsin1.setRegisterName("TEST_1");
		hd4ResSsin1.setOldPatientIds("oldPatient1");
		hd4ResSsin1 = dao.create(hd4ResSsin1);

		hd4ResSsin2 = new Hd4ResSsin();
		hd4ResSsin2.setLandingZoneCodedNiss("2");
		hd4ResSsin2.setRegisterName("TEST_1");
		hd4ResSsin2.setOldPatientIds("oldPatient1|oldPatient2");
		hd4ResSsin2 = dao.create(hd4ResSsin2);


		hd4ResSsin3 = new Hd4ResSsin();
		hd4ResSsin3.setLandingZoneCodedNiss("3");
		hd4ResSsin3.setRegisterName("TEST_1");
		hd4ResSsin3 = dao.create(hd4ResSsin3);

		hd4ResSsin4 = new Hd4ResSsin();
		hd4ResSsin4.setLandingZoneCodedNiss("4");
		hd4ResSsin4.setRegisterName("TEST_1");
		hd4ResSsin4.setOldPatientIds("oldPatient1oldPatient2");
		hd4ResSsin4 = dao.create(hd4ResSsin4);

		hd4ResSsin5 = new Hd4ResSsin();
		hd4ResSsin5.setLandingZoneCodedNiss("5");
		hd4ResSsin5.setRegisterName("TEST_1");
		hd4ResSsin5.setOldPatientIds("oldPatient2|oldPatient1");
		hd4ResSsin5 = dao.create(hd4ResSsin5);

		hd4ResSsin6 = new Hd4ResSsin();
		hd4ResSsin6.setLandingZoneCodedNiss("6");
		hd4ResSsin6.setRegisterName("TEST_1");
		hd4ResSsin6.setOldPatientIds("oldPatient1");
		hd4ResSsin6 = dao.create(hd4ResSsin6);

		hd4ResSsin7 = new Hd4ResSsin();
		hd4ResSsin7.setLandingZoneCodedNiss("7");
		hd4ResSsin7.setRegisterName("TEST_2");
		hd4ResSsin7.setOldPatientIds("oldPatient0|oldPatient1|oldPatient2");
		hd4ResSsin7 = dao.create(hd4ResSsin7);
	}

	@Test
	public void testGetBiggest1() {
		setup();
		String biggest = dao.getBiggest("TEST1");

		Assert.assertEquals("TEST1_PATIENTID_0000-0002", biggest);
	}

	@Test
	public void testGetBiggest2() {
		setup();
		String biggest = dao.getBiggest("TEST2");

		Assert.assertEquals("TEST2_PATIENTID_0000-0001", biggest);
	}

	@Test
	public void testGetBiggest3() {
		String biggest = dao.getBiggest("TEST3");

		Assert.assertNull(biggest);
	}

	@Test
	public void testGetAll1() {
		setup();
		Hd4ResSsinSearch search = new Hd4ResSsinSearch();
		search.seteHealthCodedNissBatch("encodedSsin1");
		search.setRegisterName("TEST1");

		List<Hd4ResSsin> hd4ResSsins = dao.getAll(search);

		Assert.assertNotNull(hd4ResSsins);
		Assert.assertEquals(1, hd4ResSsins.size());
		Assert.assertEquals(hd4ResSsin1, hd4ResSsins.get(0));
	}

	@Test
	public void testGetAll2() {
		setup();
		Hd4ResSsinSearch search = new Hd4ResSsinSearch();
		search.setRegisterName("TEST1");
		search.setLandingZoneCodedNiss("TEST1_PATIENTID_0000-0002");

		List<Hd4ResSsin> hd4ResSsins = dao.getAll(search);

		Assert.assertNotNull(hd4ResSsins);
		Assert.assertEquals(1, hd4ResSsins.size());
		Assert.assertEquals(hd4ResSsin2, hd4ResSsins.get(0));
	}


	@Test
	public void testGetPossibleOldIdMatches() {
		setup2();
		List<Hd4ResSsin> all = dao.getPossibleOldIdMatches("TEST_1", "oldPatient1");
		Assert.assertEquals(5, all.size());
		Assert.assertTrue(all.contains(hd4ResSsin1));
		Assert.assertTrue(all.contains(hd4ResSsin2));
		Assert.assertFalse(all.contains(hd4ResSsin3));
		Assert.assertTrue(all.contains(hd4ResSsin4));
		Assert.assertTrue(all.contains(hd4ResSsin5));
		Assert.assertTrue(all.contains(hd4ResSsin6));
		Assert.assertFalse(all.contains(hd4ResSsin7));
	}
}
