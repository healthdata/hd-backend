/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.hd4res.service.impl;

import be.wiv_isp.healthdata.common.dto.DataCollectionGroupDtoV7;
import be.wiv_isp.healthdata.orchestration.domain.PeriodExpression;
import be.wiv_isp.healthdata.orchestration.util.DateUtils;
import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.common.domain.enumeration.TimeUnit;
import be.wiv_isp.healthdata.hd4res.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionDefinitionForwardService;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

public class AvailableRegistrationWorkflowActionsTest {

	private final RegistrationWorkflowService workflowService = new RegistrationWorkflowService();

	private final IDataCollectionDefinitionForwardService dataCollectionDefinitionForwardService = EasyMock.createNiceMock(IDataCollectionDefinitionForwardService.class);

	@Before
	public void before() {
		ReflectionTestUtils.setField(workflowService, "catalogueService", dataCollectionDefinitionForwardService);
	}

	@Test
	public void testInitialAction() throws Exception {
		final Set<WorkflowActionType> availableActions = workflowService.getAvailableActions(null);
		Assert.assertEquals(1, availableActions.size());
		Assert.assertTrue(availableActions.contains(WorkflowActionType.CREATE_FOR_REVIEW));
	}

	@Test
	public void testWorkflowCanBeSubmitted() throws Exception {
		final DataCollectionDefinitionDtoV7 dcd = createDataCollectionDefinition();
		dcd.getDataCollectionGroup().setStartDate(oneWeekAgo());
		dcd.getDataCollectionGroup().setEndDateComments(inOneWeek());

		final RegistrationWorkflow workflow = new RegistrationWorkflow();
		workflow.setDataCollectionDefinitionId(dcd.getId());

		EasyMock.expect(dataCollectionDefinitionForwardService.get(1L)).andReturn(dcd).anyTimes();
		EasyMock.replay(dataCollectionDefinitionForwardService);

		workflow.setStatus(WorkflowStatus.ANNOTATIONS_NEEDED);
		Assert.assertTrue(workflowService.isActionAvailable(workflow, WorkflowActionType.SUBMIT_ANNOTATIONS));

		EasyMock.verify(dataCollectionDefinitionForwardService);
	}

	@Test
	public void testWorkflowCanNotBeSubmittedIfEndDateCommentsIsOver() throws Exception {
		final DataCollectionDefinitionDtoV7 dcd = createDataCollectionDefinition();
		dcd.getDataCollectionGroup().setStartDate(twoWeeksAgo());
		dcd.getDataCollectionGroup().setEndDateComments(oneWeekAgo());

		final RegistrationWorkflow workflow = new RegistrationWorkflow();
		workflow.setDataCollectionDefinitionId(dcd.getId());

		EasyMock.expect(dataCollectionDefinitionForwardService.get(1L)).andReturn(dcd).anyTimes();
		EasyMock.replay(dataCollectionDefinitionForwardService);

		workflow.setStatus(WorkflowStatus.ANNOTATIONS_NEEDED);
		Assert.assertFalse(workflowService.isActionAvailable(workflow, WorkflowActionType.SUBMIT_ANNOTATIONS));

		EasyMock.verify(dataCollectionDefinitionForwardService);
	}

	@Test
	public void testWorkflowCanBeResubmitted() throws Exception {
		RegistrationWorkflow workflow = new RegistrationWorkflow();
		workflow.setStatus(WorkflowStatus.ANNOTATIONS_NEEDED);
		Assert.assertTrue(workflowService.isActionAvailable(workflow, WorkflowActionType.RESUBMIT));
	}

	private Timestamp twoWeeksAgo() {
		return new Timestamp(DateUtils.remove(new Date(), new PeriodExpression(2, TimeUnit.WEEK)).getTime());
	}

	private Timestamp oneWeekAgo() {
		return new Timestamp(DateUtils.remove(new Date(), new PeriodExpression(1, TimeUnit.WEEK)).getTime());
	}

	private Timestamp inOneWeek() {
		return new Timestamp(DateUtils.add(new Date(), new PeriodExpression(1, TimeUnit.WEEK)).getTime());
	}

	private DataCollectionDefinitionDtoV7 createDataCollectionDefinition() {
		DataCollectionDefinitionDtoV7 dcd = new DataCollectionDefinitionDtoV7();
		dcd.setId(1L);
		dcd.setDataCollectionGroup(new DataCollectionGroupDtoV7());
		return dcd;
	}
}
