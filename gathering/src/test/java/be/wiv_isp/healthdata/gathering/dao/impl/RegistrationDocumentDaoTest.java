/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.dao.impl;

import be.wiv_isp.healthdata.gathering.domain.RegistrationDocument;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationDocumentSearch;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.dao.IDocumentDao;
import be.wiv_isp.healthdata.orchestration.dao.IWorkflowDao;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class RegistrationDocumentDaoTest {

	private RegistrationDocument document;

	private Organization organization;

	@Autowired
	private IDocumentDao<RegistrationDocument, RegistrationDocumentSearch> documentDao;

	@Autowired
	private IWorkflowDao<RegistrationWorkflow, RegistrationWorkflowSearch> workflowDao;

	@Autowired
	private IOrganizationDao organizationDao;


	@Before
	public void setUp() throws Exception {
		document = buildDocument();
		document = documentDao.create(document);
		organization = new Organization();
		organization = organizationDao.create(organization);
	}

	@Test
	public void testGet() throws Exception {
		final RegistrationDocument returnedDocument = documentDao.get(document.getId());
		Assert.assertNotNull(returnedDocument);
		Assert.assertEquals(document.getId(), returnedDocument.getId());
		Assert.assertArrayEquals(document.getDocumentContent(), returnedDocument.getDocumentContent());
	}

	@Test
	public void testUpdate() throws Exception {
		document.setDocumentContent("Some new JSON content".getBytes());
		documentDao.update(document);

		final RegistrationDocument returnedDocument = documentDao.get(document.getId());
		Assert.assertNotNull(returnedDocument);
		Assert.assertEquals(document.getId(), returnedDocument.getId());
		Assert.assertArrayEquals(document.getDocumentContent(), returnedDocument.getDocumentContent());
	}

	@Test
	public void testUpdateBigContent() throws Exception {
		document.setDocumentContent(new byte[1000000]);
		documentDao.update(document);

		final RegistrationDocument returnedDocument = documentDao.get(document.getId());
		Assert.assertEquals(document.getId(), returnedDocument.getId());
		Assert.assertArrayEquals(document.getDocumentContent(), returnedDocument.getDocumentContent());
	}

	@Test
	public void testDelete() throws Exception {
		documentDao.delete(document);

		final RegistrationDocument returnedDocument = documentDao.get(document.getId());
		Assert.assertNull(returnedDocument);
	}

	@Test
	public void testCount() {
		RegistrationWorkflow workflow1 = new RegistrationWorkflow();
		workflow1.setOrganization(organization);
		workflow1 = workflowDao.create(workflow1);

		RegistrationWorkflow workflow2 = new RegistrationWorkflow();
		workflow2.setOrganization(organization);
		workflow2 = workflowDao.create(workflow2);

		for (RegistrationDocument registrationDocument : documentDao.getAll())
			System.out.println(registrationDocument.toString());
		Assert.assertEquals(0, documentDao.count());
		
		RegistrationDocument document1 = new RegistrationDocument();
		document1.setDocumentContent("content".getBytes());
		document1.setWorkflow(workflow1);
		documentDao.create(document1);

		long count = documentDao.count();
		Assert.assertEquals(1, count);

		RegistrationDocument document2 = new RegistrationDocument();
		document2.setDocumentContent("content".getBytes());
		document2.setWorkflow(workflow2);
		documentDao.create(document2);

		count = documentDao.count();
		Assert.assertEquals(2, count);

		RegistrationDocument document3 = new RegistrationDocument();
		document3.setDocumentContent("content".getBytes());
		document3.setWorkflow(workflow1);
		documentDao.create(document3);

		count = documentDao.count();
		Assert.assertEquals(2, count);
	}

	private RegistrationDocument buildDocument() {
		RegistrationDocument document = new RegistrationDocument();
		document.setDocumentContent("Some JSON content".getBytes());
		return document;
	}
}
