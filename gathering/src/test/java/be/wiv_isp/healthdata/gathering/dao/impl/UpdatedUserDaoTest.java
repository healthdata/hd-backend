/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.dao.impl;

import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.gathering.dao.IUpdatedUserDao;
import be.wiv_isp.healthdata.gathering.domain.UpdatedUser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UpdatedUserDaoTest {

	@Autowired
	private IUpdatedUserDao dao;

	@Autowired
	private IOrganizationDao organizationDao;

	@Test
	public void testCreateAndGet() {
		Organization organization = new Organization();
		organization.setMain(true);
		organization.setDeleted(false);
		organization.setName("My Organization");
		organization.setHealthDataIDType("RIZIV");
		organization.setHealthDataIDValue("11111111");
		organization = organizationDao.create(organization);

		final UpdatedUser updatedUser = new UpdatedUser();
		updatedUser.setUserID(1L);
		updatedUser.setOrganization(organization);

		dao.create(updatedUser);

		final UpdatedUser retrievedEntity = dao.get(1L);
		Assert.assertNotNull(retrievedEntity);

		final UpdatedUser nonExistingEntity = dao.get(2L);
		Assert.assertNull(nonExistingEntity);
	}

	@Test
	public void testGetAll() {
		Organization organization = new Organization();
		organization.setMain(true);
		organization.setDeleted(false);
		organization.setName("My Organization");
		organization.setHealthDataIDType("RIZIV");
		organization.setHealthDataIDValue("11111111");
		organization = organizationDao.create(organization);

		UpdatedUser updatedUser1 = new UpdatedUser();
		updatedUser1.setUserID(1L);
		updatedUser1.setOrganization(organization);
		updatedUser1 = dao.create(updatedUser1);

		UpdatedUser updatedUser2 = new UpdatedUser();
		updatedUser2.setUserID(2L);
		updatedUser2.setOrganization(organization);
		updatedUser2 = dao.create(updatedUser2);

		UpdatedUser updatedUser3 = new UpdatedUser();
		updatedUser3.setUserID(3L);
		updatedUser3.setOrganization(organization);
		updatedUser3 = dao.create(updatedUser3);

		final List<UpdatedUser> all = dao.getAll(null);
		Assert.assertEquals(3, all.size());
		Assert.assertTrue(all.contains(updatedUser1));
		Assert.assertTrue(all.contains(updatedUser2));
		Assert.assertTrue(all.contains(updatedUser3));
	}
}
