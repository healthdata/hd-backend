/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.common.domain.enumeration.TemplateKey;
import be.wiv_isp.healthdata.common.domain.enumeration.TimeUnit;
import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.dao.IPasswordResetTokenDao;
import be.wiv_isp.healthdata.orchestration.dao.IUserDao;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.mail.PasswordResetContext;
import be.wiv_isp.healthdata.orchestration.domain.search.PasswordResetTokenSearch;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IMailService;
import be.wiv_isp.healthdata.orchestration.service.IPlatformService;
import be.wiv_isp.healthdata.orchestration.service.IUserService;
import be.wiv_isp.healthdata.orchestration.service.impl.PasswordResetService;
import be.wiv_isp.healthdata.orchestration.service.impl.PasswordResetTokenService;
import org.easymock.EasyMock;
import org.easymock.LogicalOperator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.Comparator;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PasswordResetServiceTest {

    // service to test
    private PasswordResetService passwordResetService = new PasswordResetService();

    // other services
    private PasswordResetTokenService passwordResetTokenService = new PasswordResetTokenService();
    private IUserService userService = new UserService();

    // mocks
    private IConfigurationService configurationService = EasyMock.createNiceMock(IConfigurationService.class);
    private IPlatformService platformService = EasyMock.createNiceMock(IPlatformService.class);
    private IMailService mailService = EasyMock.createNiceMock(IMailService.class);

    @Autowired
    private IOrganizationDao organizationDao;

    @Autowired
    private IPasswordResetTokenDao passwordResetTokenDao;

    @Autowired
    private IUserDao userDao;

    @Resource(name = "passwordEncoder")
    protected PasswordEncoder passwordEncoder;

    private TokenStore tokenStore = EasyMock.createNiceMock(TokenStore.class);

    private Organization organization;

    @Before
    public void before() {
        Assert.assertTrue(organizationDao.getAll().isEmpty());

        final Organization org = new Organization();
        org.setMain(true);
        org.setHealthDataIDType("RIZIV");
        org.setHealthDataIDValue("11111111");
        org.setName("Test-Organization");
        organization = organizationDao.create(org);

        ReflectionTestUtils.setField(userService, "userDao", userDao);
        ReflectionTestUtils.setField(userService, "passwordEncoder", passwordEncoder);
        ReflectionTestUtils.setField(userService, "tokenStore", tokenStore);
        ReflectionTestUtils.setField(passwordResetTokenService, "dao", passwordResetTokenDao);
        ReflectionTestUtils.setField(passwordResetTokenService, "configurationService", configurationService);

        ReflectionTestUtils.setField(passwordResetService, "passwordResetTokenService", passwordResetTokenService);
        ReflectionTestUtils.setField(passwordResetService, "userService", userService);
        ReflectionTestUtils.setField(passwordResetService, "configurationService", configurationService);
        ReflectionTestUtils.setField(passwordResetService, "platformService", platformService);
        ReflectionTestUtils.setField(passwordResetService, "mailService", mailService);
    }

    @Test
    public void test() {
        EasyMock.expect(configurationService.get(ConfigurationKey.PASSWORD_RESET_TOKEN_VALIDITY_PERIOD)).andReturn(buildConfiguration("10m")).anyTimes();
        EasyMock.expect(configurationService.get(ConfigurationKey.SUPPORT_EMAIL)).andReturn(buildConfiguration("support@test.com")).anyTimes();
        EasyMock.expect(configurationService.get(ConfigurationKey.GUI_HOST)).andReturn(buildConfiguration("http://hd4dp")).anyTimes();
        EasyMock.expect(platformService.getCurrentPlatform()).andReturn(Platform.HD4DP).anyTimes();
        final PasswordResetContext context = buildExpectedMailContext();
        mailService.createAndSendMail(EasyMock.eq(TemplateKey.PASSWORD_RESET), EasyMock.cmp(context, getComparator(), LogicalOperator.EQUAL), EasyMock.eq("john.doe@test.com"));
        EasyMock.expectLastCall();

        EasyMock.replay(configurationService, platformService, mailService);

        User user = new User();
        user.setUsername("john.doe");
        user.setFirstName("John");
        user.setLastName("Doe");
        user.setEmail("john.doe@test.com");
        user.setPassword(passwordEncoder.encode("originalPassword"));
        user.setOrganization(organization);
        userDao.create(user);

        passwordResetService.sendPasswordResetInstructions(organization, "john.doe@test.com");

        // verify current password
        Assert.assertTrue(passwordEncoder.matches("originalPassword", getUser().getPassword()));
        // reset password
        final PasswordResetToken token = passwordResetTokenService.getUnique(new PasswordResetTokenSearch());
        passwordResetService.resetPassword(organization, token.getToken(), "newPassword");
        // verify user password has been reset
        Assert.assertTrue(passwordEncoder.matches("newPassword", getUser().getPassword()));

        EasyMock.verify(configurationService, platformService, mailService);
    }

    private User getUser() {
        final UserSearch userSearch = new UserSearch();
        userSearch.setUsername("john.doe");
        return userService.getUnique(userSearch);
    }

    private PasswordResetContext buildExpectedMailContext() {
        final PasswordResetContext context = new PasswordResetContext();
        context.setFirstName("John");
        context.setLastName("Doe");
        context.setResetPasswordLink(MessageFormat.format("http://hd4dp/#/resetPassword?organizationId={0}&token=", organization.getId()));
        context.setValidityPeriod(new PeriodExpression(10, TimeUnit.MINUTE));
        context.setSupportEmail("support@test.com");
        context.setPlatform(Platform.HD4DP);
        return context;
    }

    private Configuration buildConfiguration(String value) {
        final Configuration configuration = new Configuration();
        configuration.setValue(value);
        return configuration;
    }

    private Comparator<PasswordResetContext> getComparator() {
        return new Comparator<PasswordResetContext>() {

            @Override
            public int compare(PasswordResetContext o1, PasswordResetContext o2) {
                final int index = o1.getResetPasswordLink().lastIndexOf("=");
                final String token = o1.getResetPasswordLink().substring(index + 1);

                o2.setResetPasswordLink(o2.getResetPasswordLink() + token);

                if (o1.equals(o2)) {
                    return 0;
                }
                return 1;
            }
        };
    }

}
