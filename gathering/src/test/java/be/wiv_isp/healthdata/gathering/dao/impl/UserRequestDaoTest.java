/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.dao.impl;

import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.gathering.domain.UserRequest;
import be.wiv_isp.healthdata.gathering.domain.search.UserRequestSearch;
import be.wiv_isp.healthdata.orchestration.dao.IUserRequestDao;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserRequestDaoTest {

    @Autowired
    private IUserRequestDao<UserRequest, UserRequestSearch> dao;


    @Autowired
    private IOrganizationDao organizationDao;

    private UserRequest userRequest1;
    private UserRequest userRequest2;
    private UserRequest userRequest3;

    private Organization organization;
    private Organization organization2;

    private void setup() {
        organization = createOrganization(true);
        organization = organizationDao.create(organization);
        organization2 = createOrganization(false);
        organization2 = organizationDao.create(organization2);

        userRequest1 = new UserRequest();
        userRequest1.setFirstName("FirstName1");
        userRequest1.setLastName("LastName1");
        userRequest1.setEmail("firstname1.lastname1@hospital1.be");
        userRequest1.setUsername("userName1");
        userRequest1.setPassword("password1");
        userRequest1.setApproved(null);
        userRequest1.setOrganization(organization);

        userRequest2 = new UserRequest();
        userRequest2.setFirstName("FirstName2");
        userRequest2.setLastName("LastName2");
        userRequest2.setEmail("firstname2.lastname2@hospital1.be");
        userRequest2.setUsername("userName2");
        userRequest2.setPassword("password2");
        userRequest2.setApproved(false);
        userRequest2.setOrganization(organization);

        userRequest3 = new UserRequest();
        userRequest3.setFirstName("FirstName1");
        userRequest3.setLastName("LastName3");
        userRequest3.setEmail("firstname1.lastname3@hospital2.be");
        userRequest3.setUsername("userName3");
        userRequest3.setPassword("password3");
        userRequest3.setApproved(true);
        userRequest3.setOrganization(organization2);

        userRequest1 = dao.create(userRequest1);
        userRequest2 = dao.create(userRequest2);
        userRequest3 = dao.create(userRequest3);

    }

    @Test
    public void  testGetAllUserName() {
        setup();
        final UserRequestSearch search = new UserRequestSearch();
        search.setUsername("userName1");

        final List<UserRequest> userRequests = dao.getAll(search);
        Assert.assertEquals(1, userRequests.size());
        Assert.assertTrue(userRequests.contains(userRequest1));
        Assert.assertFalse(userRequests.contains(userRequest2));
        Assert.assertFalse(userRequests.contains(userRequest3));
    }


    @Test
    public void  testGetAllOrganizationId() {
        setup();

        UserRequest userRequest4 = new UserRequest();
        userRequest4.setFirstName("FirstName1");
        userRequest4.setLastName("LastName1");
        userRequest4.setEmail("firstname1.lastname1@hospital1.be");
        userRequest4.setUsername("userName1");
        userRequest4.setPassword("password1");
        userRequest4.setApproved(null);
        userRequest4.setOrganization(organization);

        userRequest1 = dao.create(userRequest4);


        final UserRequestSearch search = new UserRequestSearch();
        search.setOrganizationId(organization.getId());

        final List<UserRequest> userRequests = dao.getAll(search);
        Assert.assertEquals(2, userRequests.size());
        Assert.assertTrue(userRequests.contains(userRequest1));
        Assert.assertFalse(userRequests.contains(userRequest2));
        Assert.assertFalse(userRequests.contains(userRequest3));
        Assert.assertTrue(userRequests.contains(userRequest4));
    }


    @Test
    public void  testGetAllApproved() {
        setup();
        final UserRequestSearch search = new UserRequestSearch();

        final List<UserRequest> userRequests = dao.getAll(search);
        Assert.assertEquals(1, userRequests.size());
        Assert.assertTrue(userRequests.contains(userRequest1));
        Assert.assertFalse(userRequests.contains(userRequest2));
        Assert.assertFalse(userRequests.contains(userRequest3));

        final UserRequestSearch searchApproved = new UserRequestSearch();
        searchApproved.setApproved(true);

        final List<UserRequest> userRequestsApproved = dao.getAll(searchApproved);
        Assert.assertEquals(1, userRequestsApproved.size());
        Assert.assertFalse(userRequestsApproved.contains(userRequest1));
        Assert.assertFalse(userRequestsApproved.contains(userRequest2));
        Assert.assertTrue(userRequestsApproved.contains(userRequest3));

        final UserRequestSearch searchRejected = new UserRequestSearch();
        searchRejected.setApproved(false);

        final List<UserRequest> userRequestsRejected = dao.getAll(searchRejected);
        Assert.assertEquals(1, userRequestsRejected.size());
        Assert.assertFalse(userRequestsRejected.contains(userRequest1));
        Assert.assertTrue(userRequestsRejected.contains(userRequest2));
        Assert.assertFalse(userRequestsRejected.contains(userRequest3));


        final UserRequestSearch searchPending = new UserRequestSearch();

        final List<UserRequest> userRequestsPending = dao.getAll(searchPending);
        Assert.assertEquals(1, userRequestsPending.size());
        Assert.assertTrue(userRequestsPending.contains(userRequest1));
        Assert.assertFalse(userRequestsPending.contains(userRequest2));
        Assert.assertFalse(userRequestsPending.contains(userRequest3));
    }

    private Organization createOrganization(boolean main) {
        Organization organization = new Organization();
        organization.setMain(main);
        return organization;
    }
}
