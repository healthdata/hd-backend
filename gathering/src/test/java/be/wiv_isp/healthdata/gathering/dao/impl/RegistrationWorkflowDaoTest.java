/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.dao.impl;

import be.wiv_isp.healthdata.common.domain.Baseline;
import be.wiv_isp.healthdata.common.domain.BaselineType;
import be.wiv_isp.healthdata.common.domain.search.QueryResult;
import be.wiv_isp.healthdata.common.pagination.PaginationRange;
import be.wiv_isp.healthdata.gathering.dao.IRegistrationWorkflowDao;
import be.wiv_isp.healthdata.gathering.domain.RegistrationDocument;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationCountGroupByOrganizationDcdStatus;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationDocumentSearch;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.gathering.dto.ParticipationCreationDto;
import be.wiv_isp.healthdata.orchestration.dao.IDocumentDao;
import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.FollowUp;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.RegistrationWorkflowHistory;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class RegistrationWorkflowDaoTest {

	@Autowired
	private IRegistrationWorkflowDao dao;

	@Autowired
	private IDocumentDao<RegistrationDocument, RegistrationDocumentSearch> documentDao;

	@Autowired
	private IOrganizationDao organizationDao;

	@Test
	public void testGet() throws Exception {
		RegistrationWorkflow registration = buildRegistration();
		registration = dao.create(registration);
		final AbstractRegistrationWorkflow returnedWorkflow = dao.get(registration.getId());
		Assert.assertNotNull(returnedWorkflow);
		Assert.assertEquals(registration.getId(), returnedWorkflow.getId());
		Assert.assertEquals(registration.getStatus(), returnedWorkflow.getStatus());
	}

	@Test
	public void testGetAllSortedByUpdatedOn() throws Exception {
		dao.create(buildRegistration());

		Thread.sleep(10); // sleep for ensuring that there is a gap between theworkflow that is being created and the workflowpreviously created
		dao.create(buildRegistration());

		Thread.sleep(10); // sleep for ensuring that there is a gap between theworkflow that is being created and the workflowpreviously created
		dao.create(buildRegistration());

		final List<RegistrationWorkflow> registrations = dao.getAll(new RegistrationWorkflowSearch());

		Assert.assertEquals(3, registrations.size());
		Assert.assertTrue(registrations.get(0).getUpdatedOn().after(registrations.get(1).getUpdatedOn()));
		Assert.assertTrue(registrations.get(1).getUpdatedOn().after(registrations.get(2).getUpdatedOn()));
	}

	@Test
	public void testSearchByUniqueId() throws Exception {
		RegistrationWorkflow registration1 = buildRegistration();
		registration1.setUniqueID("unique-id-1");
		registration1 = dao.create(registration1);

		RegistrationWorkflow registration2 = buildRegistration();
		registration2.setUniqueID("unique-id-2");
		registration2 = dao.create(registration2);

		RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		List<RegistrationWorkflow> registrations = dao.getAll(search);
		Assert.assertEquals(2, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
		Assert.assertTrue(registrations.contains(registration2));

		search = new RegistrationWorkflowSearch();
		search.setUniqueID("unique-id-1");
		registrations = dao.getAll(search);
		Assert.assertEquals(1, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
	}

	@Test
	public void testSearchByDataCollectionName() throws Exception {
		RegistrationWorkflow registration1 = buildRegistration();
		registration1.setDataCollectionName("dataCollection1");
		registration1 = dao.create(registration1);

		RegistrationWorkflow registration2 = buildRegistration();
		registration2.setDataCollectionName("dataCollection2");
		registration2 = dao.create(registration2);

		RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		List<RegistrationWorkflow> registrations = dao.getAll(search);
		Assert.assertEquals(2, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
		Assert.assertTrue(registrations.contains(registration2));

		search = new RegistrationWorkflowSearch();
		search.setDataCollectionName("dataCollection1");
		registrations = dao.getAll(search);
		Assert.assertEquals(1, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
	}

	@Test
	public void testSearchByDataCollectionDefinitionId() throws Exception {
		RegistrationWorkflow registration1 = buildRegistration();
		registration1.setDataCollectionDefinitionId(1L);
		registration1 = dao.create(registration1);

		RegistrationWorkflow registration2 = buildRegistration();
		registration2.setDataCollectionDefinitionId(2L);
		registration2 = dao.create(registration2);

		RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		List<RegistrationWorkflow> registrations = dao.getAll(search);
		Assert.assertEquals(2, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
		Assert.assertTrue(registrations.contains(registration2));

		search = new RegistrationWorkflowSearch();
		search.setDataCollectionDefinitionId(1L);
		registrations = dao.getAll(search);
		Assert.assertEquals(1, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
	}

	@Test
	public void testSearchByNotWorkflowId() throws Exception {
		RegistrationWorkflow registration1 = buildRegistration();
		registration1 = dao.create(registration1);

		RegistrationWorkflow registration2 = buildRegistration();
		registration2 = dao.create(registration2);

		RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		List<RegistrationWorkflow> registrations = dao.getAll(search);
		Assert.assertEquals(2, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
		Assert.assertTrue(registrations.contains(registration2));

		search = new RegistrationWorkflowSearch();
		search.setNotWorkflowId(registration2.getId());
		registrations = dao.getAll(search);
		Assert.assertEquals(1, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
	}

	@Test
	public void testReturnDeleted() throws Exception {
		RegistrationWorkflow registration1 = buildRegistration();
		registration1 = dao.create(registration1);

		RegistrationWorkflow registration2 = buildRegistration();
		registration2.setStatus(WorkflowStatus.DELETED);
		registration2 = dao.create(registration2);

		RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		List<RegistrationWorkflow> registrations = dao.getAll(search);
		Assert.assertEquals(1, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));

		search = new RegistrationWorkflowSearch();
		search.setReturnDeleted(true);
		registrations = dao.getAll(search);
		Assert.assertEquals(2, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
		Assert.assertTrue(registrations.contains(registration2));
	}

	@Test
	public void testSearchByStatus() throws Exception {
		RegistrationWorkflow registration1 = buildRegistration();
		registration1.setStatus(WorkflowStatus.IN_PROGRESS);
		registration1 = dao.create(registration1);

		RegistrationWorkflow registration2 = buildRegistration();
		registration2.setStatus(WorkflowStatus.SUBMITTED);
		registration2 = dao.create(registration2);

		RegistrationWorkflow registration3 = buildRegistration();
		registration3.setStatus(WorkflowStatus.APPROVED);
		registration3 = dao.create(registration3);

		RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		List<RegistrationWorkflow> registrations = dao.getAll(search);
		Assert.assertEquals(3, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
		Assert.assertTrue(registrations.contains(registration2));
		Assert.assertTrue(registrations.contains(registration3));

		search = new RegistrationWorkflowSearch();
		search.setStatuses(new HashSet<>(Arrays.asList(WorkflowStatus.IN_PROGRESS, WorkflowStatus.SUBMITTED)));
		registrations = dao.getAll(search);
		Assert.assertEquals(2, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
		Assert.assertTrue(registrations.contains(registration2));

		search = new RegistrationWorkflowSearch();
		search.setStatuses(new HashSet<WorkflowStatus>());
		registrations = dao.getAll(search);
		Assert.assertEquals(0, registrations.size());
	}

	@Test
	public void testSearchByWorkflowIds() throws Exception {
		RegistrationWorkflow registration1 = buildRegistration();
		registration1 = dao.create(registration1);

		RegistrationWorkflow registration2 = buildRegistration();
		registration2 = dao.create(registration2);

		RegistrationWorkflow registration3 = buildRegistration();
		registration3 = dao.create(registration3);

		RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		List<RegistrationWorkflow> registrations = dao.getAll(search);
		Assert.assertEquals(3, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
		Assert.assertTrue(registrations.contains(registration2));
		Assert.assertTrue(registrations.contains(registration3));

		search = new RegistrationWorkflowSearch();
		search.setWorkflowIds(Arrays.asList(registration1.getId(), registration2.getId()));
		registrations = dao.getAll(search);
		Assert.assertEquals(2, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
		Assert.assertTrue(registrations.contains(registration2));

		search = new RegistrationWorkflowSearch();
		search.setWorkflowIds(new ArrayList<Long>());
		registrations = dao.getAll(search);
		Assert.assertEquals(0, registrations.size());
	}

	@Test
	public void testSearchByOrganization() throws Exception {
		final Organization organization1 = organizationDao.create(buildOrganization("11111111"));
		final Organization organization2 = organizationDao.create(buildOrganization("22222222"));

		RegistrationWorkflow registration1 = buildRegistration();
		registration1.setOrganization(organization1);
		registration1 = dao.create(registration1);

		RegistrationWorkflow registration2 = buildRegistration();
		registration2.setOrganization(organization2);
		registration2 = dao.create(registration2);

		RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		List<RegistrationWorkflow> registrations = dao.getAll(search);
		Assert.assertEquals(2, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
		Assert.assertTrue(registrations.contains(registration2));

		search = new RegistrationWorkflowSearch();
		search.setOrganization(organization1);
		registrations = dao.getAll(search);
		Assert.assertEquals(1, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
	}

	@Test
	public void testSearchBySendStatus() throws Exception {
		RegistrationWorkflow registration1 = buildRegistration();
		registration1.setSendStatus("send-status-1");
		registration1 = dao.create(registration1);

		RegistrationWorkflow registration2 = buildRegistration();
		registration2.setSendStatus("send-status-2");
		registration2 = dao.create(registration2);

		RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		List<RegistrationWorkflow> registrations = dao.getAll(search);
		Assert.assertEquals(2, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
		Assert.assertTrue(registrations.contains(registration2));

		search = new RegistrationWorkflowSearch();
		search.setSendStatus("send-status-1");
		registrations = dao.getAll(search);
		Assert.assertEquals(1, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
	}

	@Test
	public void testSearchByNotUpdatedAfter() throws Exception {
		final RegistrationWorkflow registration1 = dao.create(buildRegistration());
		Thread.sleep(10);
		final Date betweenTwoCreations = new Date();
		Thread.sleep(10);
		final RegistrationWorkflow registration2 = dao.create(buildRegistration());

		RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		List<RegistrationWorkflow> registrations = dao.getAll(search);
		Assert.assertEquals(2, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
		Assert.assertTrue(registrations.contains(registration2));

		search = new RegistrationWorkflowSearch();
		search.setNotUpdatedAfterDate(betweenTwoCreations);
		registrations = dao.getAll(search);
		Assert.assertEquals(1, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
	}

	@Test
	public void testSearchByCreatedAfter() throws Exception {
		final RegistrationWorkflow registration1 = dao.create(buildRegistration());
		Thread.sleep(10);
		final Date betweenTwoCreations = new Date();
		Thread.sleep(10);
		final RegistrationWorkflow registration2 = dao.create(buildRegistration());

		RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		List<RegistrationWorkflow> registrations = dao.getAll(search);
		Assert.assertEquals(2, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
		Assert.assertTrue(registrations.contains(registration2));

		search = new RegistrationWorkflowSearch();
		search.setCreatedAfterDate(betweenTwoCreations);
		registrations = dao.getAll(search);
		Assert.assertEquals(1, registrations.size());
		Assert.assertTrue(registrations.contains(registration2));
	}

	@Test
	public void testSearchByUpdatedAfter() throws Exception {
		final RegistrationWorkflow registration1 = dao.create(buildRegistration());
		final RegistrationWorkflow registration2 = dao.create(buildRegistration());

		registration1.setStatus(WorkflowStatus.IN_PROGRESS);
		dao.update(registration1);

		Thread.sleep(10);
		final Date betweenTwoUpdates = new Date();
		Thread.sleep(10);

		registration2.setStatus(WorkflowStatus.IN_PROGRESS);
		dao.update(registration2);

		RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		List<RegistrationWorkflow> registrations = dao.getAll(search);
		Assert.assertEquals(2, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
		Assert.assertTrue(registrations.contains(registration2));

		search = new RegistrationWorkflowSearch();
		search.setUpdatedAfterDate(betweenTwoUpdates);
		registrations = dao.getAll(search);
		Assert.assertEquals(1, registrations.size());
		Assert.assertTrue(registrations.contains(registration2));
	}

	@Test
	public void testSearchByFollowUpId() {
		RegistrationWorkflow registration1 = buildRegistration();
		registration1.setFollowUps(buildFollowUps());
		registration1 = dao.create(registration1);

		RegistrationWorkflow registration2 = buildRegistration();
		registration2.setFollowUps(buildFollowUps());
		registration2 = dao.create(registration2);

		final FollowUp followUp1 = registration1.getFollowUps().get(0);
		final FollowUp followUp2 = registration2.getFollowUps().get(0);

		Assert.assertNotEquals(followUp1.getId(), followUp2.getId());

		RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		List<RegistrationWorkflow> registrations = dao.getAll(search);
		Assert.assertEquals(2, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
		Assert.assertTrue(registrations.contains(registration2));

		search = new RegistrationWorkflowSearch();
		search.setFollowUpId(followUp1.getId());
		registrations = dao.getAll(search);
		Assert.assertEquals(1, registrations.size());
		Assert.assertTrue(registrations.contains(registration1));
	}

	@Test
	public void testGetAllPagination() {
		final RegistrationWorkflow registration = dao.create(buildRegistration());

		Map<Long, RegistrationWorkflow> workflowMap = new HashMap<>();
		for (long i = 1; i <= 11; i++) {
			RegistrationWorkflow workflow = new RegistrationWorkflow();
			workflow.setStatus(WorkflowStatus.IN_PROGRESS);
			workflow.setDataCollectionName("TEST");
			dao.create(workflow);
			workflowMap.put(i, workflow);
		}

		final PaginationRange range = new PaginationRange(1, 10);
		final RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		search.setPaginationRange(range);

		final QueryResult<RegistrationWorkflow> queryResult = dao.getAllAsQueryResult(search);
		final List<RegistrationWorkflow> workflows = queryResult.getResult();
		Assert.assertEquals(10, workflows.size());
		Assert.assertEquals(12, queryResult.getRange().getTotalCount());
	}

	@Test
	public void testCreatedOn() throws Exception {
		RegistrationWorkflow registration = buildRegistration();
		registration = dao.create(registration);
		Assert.assertNotNull(registration.getCreatedOn());
	}

	@Test
	public void testDelete() throws Exception {
		RegistrationWorkflow registration = buildRegistration();
		registration = dao.create(registration);
		dao.delete(registration);

		final RegistrationWorkflow returnedWorkflow = dao.get(registration.getId());
		Assert.assertNull(returnedWorkflow);
	}

	@Test
	public void testGetAllDocument() {
		RegistrationDocument document1 = buildDocument();
		document1 = documentDao.create(document1);
		RegistrationDocument document2 = buildDocument();
		document2 = documentDao.create(document2);

		RegistrationWorkflow workflow1InProgress = new RegistrationWorkflow();
		workflow1InProgress.setStatus(WorkflowStatus.IN_PROGRESS);
		workflow1InProgress.setDataCollectionName("TEST");
		workflow1InProgress.setDocument(document1);
		RegistrationWorkflow workflow2InProgress = new RegistrationWorkflow();
		workflow2InProgress.setStatus(WorkflowStatus.IN_PROGRESS);
		workflow2InProgress.setDataCollectionName("TEST");
		workflow2InProgress.setDocument(document2);
		RegistrationWorkflow workflow3InProgress = new RegistrationWorkflow();
		workflow3InProgress.setStatus(WorkflowStatus.IN_PROGRESS);
		workflow3InProgress.setDataCollectionName("TEST");

		dao.create(workflow1InProgress);
		dao.create(workflow2InProgress);
		dao.create(workflow3InProgress);

		RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		search.setWithDocument(null);

		final List<RegistrationWorkflow> returnedWorkflow = dao.getAll(new RegistrationWorkflowSearch());

		Assert.assertEquals(3, returnedWorkflow.size());
	}

	@Test
	public void testGetAllForUniqueIDCalculation() {
		RegistrationWorkflow workflow1 = new RegistrationWorkflow();
		workflow1.setStatus(WorkflowStatus.IN_PROGRESS);
		workflow1.setDataCollectionDefinitionId(1L);
		workflow1 = dao.create(workflow1);

		RegistrationWorkflow workflow2 = new RegistrationWorkflow();
		workflow2.setStatus(WorkflowStatus.IN_PROGRESS);
		workflow2.setDataCollectionDefinitionId(1L);
		workflow2.setUniqueID("");
		workflow2 = dao.create(workflow2);

		RegistrationWorkflow workflow3 = new RegistrationWorkflow();
		workflow3.setStatus(WorkflowStatus.IN_PROGRESS);
		workflow3.setDataCollectionDefinitionId(2L);
		workflow3.setUniqueID("unique_1");
		workflow3 = dao.create(workflow3);

		final List<RegistrationWorkflow> result1 = dao.getAllForUniqueIDCalculation();

		Assert.assertEquals(1, result1.size());
		Assert.assertTrue(result1.contains(workflow1));
		Assert.assertFalse(result1.contains(workflow2));
		Assert.assertFalse(result1.contains(workflow3));
	}

	@Test
	public void testGetParticipationCreationInfo() {
		Organization organization1 = new Organization();
		organization1 = organizationDao.create(organization1);
		Organization organization2 = new Organization();
		organization2 = organizationDao.create(organization2);


		RegistrationWorkflow workflow1 = new RegistrationWorkflow();
		workflow1.setStatus(WorkflowStatus.IN_PROGRESS);
		workflow1.setDataCollectionDefinitionId(1L);
		workflow1.setDataCollectionName("TEST");
		workflow1.setOrganization(organization1);
		dao.create(workflow1);

		RegistrationWorkflow workflow2 = new RegistrationWorkflow();
		workflow2.setStatus(WorkflowStatus.IN_PROGRESS);
		workflow2.setDataCollectionDefinitionId(1L);
		workflow2.setDataCollectionName("TEST");
		workflow2.setOrganization(organization2);
		dao.create(workflow2);

		RegistrationWorkflow workflow3 = new RegistrationWorkflow();
		workflow3.setStatus(WorkflowStatus.IN_PROGRESS);
		workflow3.setDataCollectionDefinitionId(2L);
		workflow3.setDataCollectionName("TEST");
		workflow3.setOrganization(organization1);
		dao.create(workflow3);

		ParticipationCreationDto expected1 = new ParticipationCreationDto(1L, "TEST", organization1);
		ParticipationCreationDto expected2 = new ParticipationCreationDto(1L, "TEST", organization2);
		ParticipationCreationDto expected3 = new ParticipationCreationDto(2L, "TEST", organization1);

		final List<ParticipationCreationDto> result1 = dao.getParticipationCreationInfo();

		Assert.assertEquals(3, result1.size());
		Assert.assertTrue(result1.contains(expected1));
		Assert.assertTrue(result1.contains(expected2));
		Assert.assertTrue(result1.contains(expected3));
	}

	@Test
	public void testGetCountGroupByOrganizationDcdStatus() {
		Organization organization1 = new Organization();
		organization1.setHealthDataIDValue("11111111");
		organization1 = organizationDao.create(organization1);

		Organization organization2 = new Organization();
		organization2.setHealthDataIDValue("22222222");
		organization2 = organizationDao.create(organization2);

		for (Organization organization : Arrays.asList(organization1, organization2)) {
			RegistrationWorkflow workflow1 = new RegistrationWorkflow();
			workflow1.setStatus(WorkflowStatus.IN_PROGRESS);
			workflow1.setDataCollectionDefinitionId(1L);
			workflow1.setDataCollectionName("TEST");
			workflow1.setOrganization(organization);
			dao.create(workflow1);

			RegistrationWorkflow workflow2 = new RegistrationWorkflow();
			workflow2.setStatus(WorkflowStatus.IN_PROGRESS);
			workflow2.setDataCollectionDefinitionId(1L);
			workflow2.setDataCollectionName("TEST");
			workflow2.setOrganization(organization);
			dao.create(workflow2);

			RegistrationWorkflow workflow3 = new RegistrationWorkflow();
			workflow3.setStatus(WorkflowStatus.IN_PROGRESS);
			workflow3.setDataCollectionDefinitionId(2L);
			workflow3.setDataCollectionName("TEST");
			workflow3.setOrganization(organization);
			dao.create(workflow3);

			RegistrationWorkflow workflow4 = new RegistrationWorkflow();
			workflow4.setStatus(WorkflowStatus.ACTION_NEEDED);
			workflow4.setDataCollectionDefinitionId(1L);
			workflow4.setDataCollectionName("TEST");
			workflow4.setOrganization(organization);
			dao.create(workflow4);
		}

		final List<RegistrationCountGroupByOrganizationDcdStatus> countGroupByOrganizationDcdStatusList = dao.getCountGroupByOrganizationDcdStatus();

		final List<RegistrationCountGroupByOrganizationDcdStatus> expectedCountGroupByOrganizationDcdStatusList = new ArrayList<>();
		expectedCountGroupByOrganizationDcdStatusList.add(new RegistrationCountGroupByOrganizationDcdStatus("11111111", 1L, WorkflowStatus.IN_PROGRESS, 2L));
		expectedCountGroupByOrganizationDcdStatusList.add(new RegistrationCountGroupByOrganizationDcdStatus("11111111", 2L, WorkflowStatus.IN_PROGRESS, 1L));
		expectedCountGroupByOrganizationDcdStatusList.add(new RegistrationCountGroupByOrganizationDcdStatus("11111111", 1L, WorkflowStatus.ACTION_NEEDED, 1L));
		expectedCountGroupByOrganizationDcdStatusList.add(new RegistrationCountGroupByOrganizationDcdStatus("22222222", 1L, WorkflowStatus.IN_PROGRESS, 2L));
		expectedCountGroupByOrganizationDcdStatusList.add(new RegistrationCountGroupByOrganizationDcdStatus("22222222", 2L, WorkflowStatus.IN_PROGRESS, 1L));
		expectedCountGroupByOrganizationDcdStatusList.add(new RegistrationCountGroupByOrganizationDcdStatus("22222222", 1L, WorkflowStatus.ACTION_NEEDED, 1L));

		Assert.assertTrue(countGroupByOrganizationDcdStatusList.containsAll(expectedCountGroupByOrganizationDcdStatusList));
		Assert.assertTrue(expectedCountGroupByOrganizationDcdStatusList.containsAll(countGroupByOrganizationDcdStatusList));
	}

	@Test
	public void testGetWorkflowIds() throws InterruptedException {
		final Organization organization1 = organizationDao.create(buildOrganization("11111111"));
		final Organization organization2 = organizationDao.create(buildOrganization("22222222"));

		final Date beforeCreations = new Date();
		Thread.sleep(10);

		RegistrationWorkflow registration1 = buildRegistration();
		registration1.setStatus(WorkflowStatus.IN_PROGRESS);
		registration1.setDataCollectionDefinitionId(1L);
		registration1.setSendStatus("OK");
		registration1.setUniqueID("UNIQUE_ID_1");
		registration1.setDataCollectionName("DATA_COLLECTION_1");
		registration1.setOrganization(organization1);
		registration1 = dao.create(registration1);

		Thread.sleep(10);
		final Date betweenCreations = new Date();
		Thread.sleep(10);

		RegistrationWorkflow registration2 = buildRegistration();
		registration2.setStatus(WorkflowStatus.DELETED);
		registration2.setDataCollectionDefinitionId(2L);
		registration2.setSendStatus("NOK");
		registration2.setUniqueID("UNIQUE_ID_2");
		registration2.setDataCollectionName("DATA_COLLECTION_2");
		registration2.setOrganization(organization2);
		registration2 = dao.create(registration2);

		final RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		search.setWorkflowIds(Arrays.asList(registration1.getId()));
		search.setStatuses(new HashSet<>(Arrays.asList(WorkflowStatus.IN_PROGRESS)));
		search.setDataCollectionDefinitionId(1L);
		search.setSendStatus("OK");
		search.setUniqueID("UNIQUE_ID_1");
		search.setDataCollectionName("DATA_COLLECTION_1");
		search.setNotWorkflowId(registration2.getId());
		search.setOrganization(organization1);
		search.setReturnDeleted(false);
		search.setCreatedAfterDate(beforeCreations);
		search.setUpdatedAfterDate(beforeCreations);
		search.setNotUpdatedAfterDate(betweenCreations);

		final List<Long> registrationIds = dao.getWorkflowIds(search);
		Assert.assertEquals(1, registrationIds.size());
		Assert.assertTrue(registrationIds.contains(registration1.getId()));
	}

	@Test
 	public void testGetDataCollectionDefinitionIds() {
		RegistrationWorkflow registration1 = buildRegistration();
		registration1.setDataCollectionDefinitionId(1L);
		registration1 = dao.create(registration1);

		RegistrationWorkflow registration2 = buildRegistration();
		registration2.setDataCollectionDefinitionId(1L);
		registration2 = dao.create(registration2);

		RegistrationWorkflow registration3 = buildRegistration();
		registration3.setDataCollectionDefinitionId(2L);
		registration3 = dao.create(registration3);

		final List<Long> dataCollectionDefinitionIds = dao.getDataCollectionDefinitionIds();
		Assert.assertEquals(2, dataCollectionDefinitionIds.size());
		Assert.assertTrue(dataCollectionDefinitionIds.contains(1L));
		Assert.assertTrue(dataCollectionDefinitionIds.contains(2L));
	}

	@Test
	public void testGetDataCollectionName() {
		final Organization organization1 = organizationDao.create(buildOrganization("11111111"));
		final Organization organization2 = organizationDao.create(buildOrganization("22222222"));

		RegistrationWorkflow registration1 = buildRegistration();
		registration1.setDataCollectionName("dc-1");
		registration1.setOrganization(organization1);
		registration1 = dao.create(registration1);

		RegistrationWorkflow registration2 = buildRegistration();
		registration2.setDataCollectionName("dc-1");
		registration2.setOrganization(organization1);
		registration2 = dao.create(registration2);

		RegistrationWorkflow registration3 = buildRegistration();
		registration3.setDataCollectionName("dc-2");
		registration3.setOrganization(organization1);
		registration3 = dao.create(registration3);

		RegistrationWorkflow registration4 = buildRegistration();
		registration4.setDataCollectionName("dc-3");
		registration4.setOrganization(organization2);
		registration4 = dao.create(registration4);

		List<String> dataCollectionNames = dao.getDataCollectionNames(organization1.getId());
		Assert.assertEquals(2, dataCollectionNames.size());
		Assert.assertTrue(dataCollectionNames.contains("dc-1"));
		Assert.assertTrue(dataCollectionNames.contains("dc-2"));

		dataCollectionNames = dao.getDataCollectionNames(organization2.getId());
		Assert.assertEquals(1, dataCollectionNames.size());
		Assert.assertTrue(dataCollectionNames.contains("dc-3"));
	}

	private RegistrationWorkflow buildRegistration() {
		final RegistrationWorkflow workflow = new RegistrationWorkflow();
		workflow.setStatus(WorkflowStatus.SUBMITTED);
		workflow.setHistory(new ArrayList<RegistrationWorkflowHistory>());
		return workflow;
	}

	private RegistrationDocument buildDocument() {
		final RegistrationDocument document = new RegistrationDocument();
		document.setDocumentContent("{}".getBytes());
		return document;
	}

	private Organization buildOrganization(String identificationValue) {
		final Organization organization = new Organization();
		organization.setHealthDataIDValue(identificationValue);
		return organization;
	}

	private List<FollowUp> buildFollowUps() {
		final List<FollowUp> followUps = new ArrayList<>();

		final FollowUp followUp = new FollowUp();
		followUp.setName("name");
		followUp.setLabel("label");
		followUp.setDescription("description");
		followUp.setTiming("2w");
		followUp.setBaseline(new Baseline(BaselineType.FOLLOW_UP_SUBMISSION_DATE, null));
		followUp.setConditions(Collections.singletonList("condition"));
		followUp.setActive(false);
		followUp.setActivationDate(null);
		followUp.setSubmittedOn(null);

		followUps.add(followUp);
		return followUps;
	}

}
