/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.gathering.dao.impl;

import be.wiv_isp.healthdata.gathering.dao.IFastTrackRecordDao;
import be.wiv_isp.healthdata.gathering.dao.IFastTrackUploadDao;
import be.wiv_isp.healthdata.gathering.domain.FastTrackRecord;
import be.wiv_isp.healthdata.gathering.domain.FastTrackUpload;
import be.wiv_isp.healthdata.gathering.domain.enumeration.FastTrackStatus;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class FastTrackRecordDaoTest {

	@Autowired
	private IFastTrackUploadDao fastTrackUploadDao;
	@Autowired
	private IFastTrackRecordDao fastTrackRecordDao;


	@Test
	public void testGetByHash() {
		FastTrackUpload upload1 = new FastTrackUpload();
		upload1.setDataCollectionDefinitionId(1L);
		upload1.setMode(FastTrackUpload.Mode.FAST_TRACK);
		upload1.setOrganizationId(1L);
		upload1.setFileName("file1.csv");
		upload1.setContent("some content");
		fastTrackUploadDao.create(upload1);

		FastTrackRecord upload1record1 = new FastTrackRecord();
		upload1record1.setStatus(FastTrackStatus.SUCCESS);
		upload1record1.setHash("some hash 1");
		upload1record1.setLineNumber(0L);
		upload1record1.setUploadId(upload1.getId());
		upload1record1.setWorkflowId(1L);
		fastTrackRecordDao.create(upload1record1);

		FastTrackRecord upload1record2 = new FastTrackRecord();
		upload1record2.setStatus(FastTrackStatus.SUCCESS);
		upload1record2.setHash("some hash 2");
		upload1record2.setLineNumber(1L);
		upload1record2.setUploadId(upload1.getId());
		upload1record2.setWorkflowId(2L);
		fastTrackRecordDao.create(upload1record2);

		FastTrackUpload upload2 = new FastTrackUpload();
		upload2.setDataCollectionDefinitionId(1L);
		upload2.setMode(FastTrackUpload.Mode.FAST_TRACK);
		upload2.setOrganizationId(1L);
		upload2.setFileName("file1.csv");
		upload2.setContent("some content");
		fastTrackUploadDao.create(upload2);

		FastTrackRecord upload2record1 = new FastTrackRecord();
		upload2record1.setStatus(FastTrackStatus.ERROR);
		upload2record1.setHash("some hash 1");
		upload2record1.setLineNumber(0L);
		upload2record1.setUploadId(upload2.getId());
		fastTrackRecordDao.create(upload2record1);

		FastTrackUpload upload3 = new FastTrackUpload();
		upload3.setDataCollectionDefinitionId(2L);
		upload3.setMode(FastTrackUpload.Mode.FAST_TRACK);
		upload3.setOrganizationId(1L);
		upload3.setFileName("file2.csv");
		upload3.setContent("some content");
		fastTrackUploadDao.create(upload3);

		FastTrackRecord upload3record1 = new FastTrackRecord();
		upload3record1.setStatus(FastTrackStatus.SUCCESS);
		upload3record1.setHash("some hash 1");
		upload3record1.setLineNumber(0L);
		upload3record1.setUploadId(upload3.getId());
		upload3record1.setWorkflowId(3L);
		fastTrackRecordDao.create(upload3record1);

		FastTrackRecord upload3record2 = new FastTrackRecord();
		upload3record2.setStatus(FastTrackStatus.IGNORE);
		upload3record2.setHash("some hash 1");
		upload3record2.setLineNumber(1L);
		upload3record2.setUploadId(upload3.getId());
		fastTrackRecordDao.create(upload3record2);

		List<FastTrackRecord> result = fastTrackRecordDao.getForHash(1L, "some hash 1");
		Assert.assertEquals(1, result.size());
		Assert.assertTrue(result.contains(upload1record1));
		Assert.assertFalse(result.contains(upload1record2));
		Assert.assertFalse(result.contains(upload2record1));
		Assert.assertFalse(result.contains(upload3record1));
		Assert.assertFalse(result.contains(upload3record2));
	}

}
