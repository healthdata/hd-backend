/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.workflow.action;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.action.standalone.StatusMessageAction;
import be.wiv_isp.healthdata.gathering.action.workflow.SaveRegistrationAction;
import be.wiv_isp.healthdata.gathering.action.workflow.SubmitRegistrationAction;
import be.wiv_isp.healthdata.gathering.factory.impl.ActionFactory;
import be.wiv_isp.healthdata.orchestration.action.standalone.StandAloneAction;
import be.wiv_isp.healthdata.orchestration.action.workflow.WorkflowAction;
import be.wiv_isp.healthdata.orchestration.domain.Progress;
import be.wiv_isp.healthdata.orchestration.factory.IActionFactory;
import be.wiv_isp.healthdata.orchestration.service.IAutowireService;
import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.test.util.ReflectionTestUtils;

import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

public class ActionFactoryTest {
	private static final String PERSON = "PERSON";
	private static final String YEAR = "YEAR";

	private IActionFactory actionFactory;

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Before
	public void setup() throws IllegalAccessException, InvocationTargetException {
		final ActionFactory actionFactory = new ActionFactory();

		final IAutowireService autowireService = new IAutowireService() {
			@Override
			public <T> T autowire(T object) {
				return object;
			}
		};
		ReflectionTestUtils.setField(actionFactory, "autowireService", autowireService);
		this.actionFactory = actionFactory;
	}

	@Test
	public void testParseSaveAction1() throws Exception {
		byte[] actionJson = IOUtils.toByteArray(ActionFactoryTest.class.getResourceAsStream("saveAction1.json"));
		SaveRegistrationAction saveAction = (SaveRegistrationAction) actionFactory.createAction(actionJson);

		Map<String, String> expectedMetaData = new HashMap<>();
		expectedMetaData.put("sex", "{\"selectedOption\":\"M\"}");
		expectedMetaData.put("PATIENT_IDENTIFIER", "19851011MEROM");
		expectedMetaData.put("name", "\"Merckx\"");
		expectedMetaData.put("first_name", "\"Robin\"");
		expectedMetaData.put("date_of_birth", "\"1985-10-11T00:00:00+02:00\"");
		expectedMetaData.put("codepat", "\"19851011MEROM\"");

		Progress expectedProgress = new Progress();
		expectedProgress.setValid(30);
		expectedProgress.setInvalid(2);
		expectedProgress.setOptional(13);
		expectedProgress.setTotal(45);
		expectedProgress.setValidRatio(0.6666666666666666);
		expectedProgress.setInvalidRatio(0.044444444444444446);
		expectedProgress.setOptionalRatio(0.28888888888888886);
		expectedProgress.setErrors(3);
		expectedProgress.setWarnings(1);

		Assert.assertNotNull(saveAction);
		Assert.assertEquals(1L, saveAction.getWorkflowId().longValue());
		Assert.assertEquals(1L, saveAction.getDataCollectionDefinitionId().longValue());
		Assert.assertEquals(expectedMetaData, saveAction.getMetaData());
		Assert.assertEquals(expectedProgress, saveAction.getProgress());
		Assert.assertArrayEquals("{}".getBytes(), saveAction.getDocumentData().getContent());
	}

	@Test
	public void testParseSaveAction2() throws Exception {
		byte[] actionJson = IOUtils.toByteArray(ActionFactoryTest.class.getResourceAsStream("saveAction2.json"));
		SaveRegistrationAction saveAction = (SaveRegistrationAction) actionFactory.createAction(actionJson);

		Map<String, String> expectedMetaData = new HashMap<>();
		expectedMetaData.put("name", "\"Merckx\"");
		expectedMetaData.put("first_name", "\"Robin\"");

		Progress expectedProgress = new Progress();
		expectedProgress.setValid(30);
		expectedProgress.setInvalid(2);
		expectedProgress.setOptional(13);
		expectedProgress.setTotal(45);
		expectedProgress.setValidRatio(0.6666666666666666);
		expectedProgress.setInvalidRatio(0.044444444444444446);
		expectedProgress.setOptionalRatio(0.28888888888888886);
		expectedProgress.setErrors(3);
		expectedProgress.setWarnings(1);

		Assert.assertNotNull(saveAction);
		Assert.assertEquals(1L, saveAction.getWorkflowId().longValue());
		Assert.assertEquals(1L, saveAction.getDataCollectionDefinitionId().longValue());
		Assert.assertEquals(expectedMetaData, saveAction.getMetaData());
		Assert.assertEquals(expectedProgress, saveAction.getProgress());
		Assert.assertArrayEquals("{}".getBytes(), saveAction.getDocumentData().getContent());
	}

	@Test
	public void testGetSaveAction() throws Exception {
		Map<String, Object> actionArgs = new HashMap<>();
		actionArgs.put("action", "SAVE");
		actionArgs.put("documentContent", "some content");
		Map<String, String> metaData = new HashMap<>();
		metaData.put(PERSON, "Robin Merckx");
		metaData.put(YEAR, "2014");
		actionArgs.put("metaData", metaData);

		final JSONObject documentContentJsonObject = new JSONObject();
		documentContentJsonObject.put("key", "some content");

		final JSONObject documentDataJsonObject = new JSONObject();
		documentDataJsonObject.put("content", new JSONObject());

		final JSONObject jsonObject = new JSONObject();
		jsonObject.put("action", "SAVE");
		jsonObject.put("documentContent", documentContentJsonObject);
		jsonObject.put("metaData", new JSONObject(metaData));
		jsonObject.put("documentData", documentDataJsonObject);

		System.out.println(jsonObject.toString());

		WorkflowAction action = (WorkflowAction) actionFactory.createAction(jsonObject.toString().getBytes(StandardCharsets.UTF_8));

		Assert.assertNotNull(action);
		Assert.assertTrue(action instanceof SaveRegistrationAction);
		Assert.assertArrayEquals(documentContentJsonObject.toString().getBytes(StandardCharsets.UTF_8), ((SaveRegistrationAction) action).getDocumentContent());
		Assert.assertArrayEquals(documentDataJsonObject.getJSONObject("content").toString().getBytes(StandardCharsets.UTF_8), ((SaveRegistrationAction) action).getDocumentData().getContent());
		Assert.assertEquals(metaData, ((SaveRegistrationAction) action).getMetaData());
	}

	@Test
	public void testGetSubmitAction() throws Exception {
		final String actionType = "SUBMIT";

		final String json = "{\"action\":\"" + actionType + "\"}";

		WorkflowAction action = (WorkflowAction) actionFactory.createAction(json.getBytes(StandardCharsets.UTF_8));

		Assert.assertNotNull(action);
		Assert.assertTrue(action instanceof SubmitRegistrationAction);
	}

	@Test
	public void testGetStatusMessageAction() throws Exception {
		final String actionType = "STATUS_MESSAGE_ACTION";

		final String json = "{\"action\":\"" + actionType + "\"}";

		StandAloneAction action = (StandAloneAction) actionFactory.createAction(json.getBytes(StandardCharsets.UTF_8));

		Assert.assertNotNull(action);
		Assert.assertTrue(action instanceof StatusMessageAction);
	}

	@Test
	public void testGetUnknown() throws Exception {

		final String json = "{\"action\":\"UNKNOWN\",\"wrongInput\":\"wrongInput\"}";

		exception.expect(HealthDataException.class);
		exception.expectMessage(MessageFormat.format(ExceptionType.INVALID_ACTION.getMessage(), "UNKNOWN"));

		actionFactory.createAction(json.getBytes(StandardCharsets.UTF_8));
	}

}
