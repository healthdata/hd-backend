/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.dao.impl;

import be.wiv_isp.healthdata.gathering.dao.IRegistrationWorkflowDao;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.dao.impl.FollowUpDao;
import be.wiv_isp.healthdata.orchestration.domain.FollowUp;
import be.wiv_isp.healthdata.orchestration.domain.search.FollowUpSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class FollowUpDaoTest {

	@Autowired
	private FollowUpDao followUpDao;

	@Autowired
	private IRegistrationWorkflowDao workflowDao;

	@Test
	public void testSearchByName() {
		final RegistrationWorkflow registration = createRegistration();

		final FollowUp fu1 = buildFollowUp();
		final FollowUp created1 = addFollowUp(registration, fu1);
		Assert.assertNotNull(created1);

		final FollowUp fu2 = buildFollowUp();
		fu2.setName("MyFollowUp-1");
		final FollowUp created2 = addFollowUp(registration, fu2);
		Assert.assertNotNull(created2);

		Assert.assertNotEquals(created1.getName(), created2.getName());

		FollowUpSearch search = new FollowUpSearch();
		List<FollowUp> all = followUpDao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new FollowUpSearch();
		search.setName("MyFollowUp");
		all = followUpDao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	@Test
	public void testSearchByActiveFlag() {
		final RegistrationWorkflow registration = createRegistration();

		final FollowUp fu1 = buildFollowUp();
		final FollowUp created1 = addFollowUp(registration, fu1);
		Assert.assertNotNull(created1);

		final FollowUp fu2 = buildFollowUp();
		fu2.setActive(false);
		final FollowUp created2 = addFollowUp(registration, fu2);
		Assert.assertNotNull(created2);

		Assert.assertNotEquals(created1.isActive(), created2.isActive());

		FollowUpSearch search = new FollowUpSearch();
		List<FollowUp> all = followUpDao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new FollowUpSearch();
		search.setActive(true);
		all = followUpDao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	@Test
	public void testSearchByActivationDate() {
		final RegistrationWorkflow registration = createRegistration();

		final FollowUp fu1 = buildFollowUp();
		final FollowUp created1 = addFollowUp(registration, fu1);
		Assert.assertNotNull(created1);

		final FollowUp fu2 = buildFollowUp();
		fu2.setActivationDate(createTimestamp(2016, Calendar.SEPTEMBER, 15));
		final FollowUp created2 = addFollowUp(registration, fu2);
		Assert.assertNotNull(created2);

		Assert.assertNotEquals(created1.getActivationDate(), created2.getActivationDate());

		FollowUpSearch search = new FollowUpSearch();
		List<FollowUp> all = followUpDao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new FollowUpSearch();
		search.setActivationDateBefore(createTimestamp(2016, Calendar.SEPTEMBER, 10));
		all = followUpDao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	private FollowUp buildFollowUp() {
		final FollowUp followUp = new FollowUp();
		followUp.setName("MyFollowUp");
		followUp.setLabel("MyFollowUpLabel");
		followUp.setTiming("7d");
		followUp.setConditions(new ArrayList<String>());
		followUp.setActive(true);
		followUp.setDescription("Description");
		followUp.setActivationDate(createTimestamp(2016, Calendar.SEPTEMBER, 5));
		return followUp;
	}

	private RegistrationWorkflow createRegistration() {
		return workflowDao.create(new RegistrationWorkflow());
	}

	private FollowUp addFollowUp(RegistrationWorkflow registration, FollowUp followUp) {
		registration.addFollowUp(followUp);
		final RegistrationWorkflow updated = workflowDao.update(registration);
		return updated.getFollowUps().get(updated.getFollowUps().size()-1);
	}

	private Timestamp createTimestamp(int year, int month, int day) {
		final Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(year, month, day, 12, 0, 0);
		return new Timestamp(calendar.getTimeInMillis());
	}
}
