/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.factory;

import be.wiv_isp.healthdata.gathering.factory.impl.StatusMessageContentFactory;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.gathering.domain.StatusMessage;
import be.wiv_isp.healthdata.orchestration.action.StatusMessageContent;
import be.wiv_isp.healthdata.orchestration.domain.search.StatusMessageSearch;
import be.wiv_isp.healthdata.orchestration.service.*;
import org.codehaus.jackson.map.ObjectMapper;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.util.ReflectionTestUtils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

public class StatusMessageContentFactoryTest {

    private static final Logger LOG = LoggerFactory.getLogger(StatusMessageContentFactoryTest.class);

    private IStatusMessageContentFactory factory = new StatusMessageContentFactory();

    private IOrganizationService organizationService = EasyMock.createNiceMock(IOrganizationService.class);
    private IStatusMessageService<StatusMessage> statusMessageService = EasyMock.createNiceMock(IStatusMessageService.class);

    @Before
    public void before() throws Exception {
        ReflectionTestUtils.setField(factory, "organizationService", organizationService);
        ReflectionTestUtils.setField(factory, "statusMessageService", statusMessageService);
    }

    @Test
    public void testCreateWithMainOrganization() throws Exception {
        templateCreate(true, true);
    }

    @Test
    public void testCreateWithNonMainOrganization() throws Exception {
        templateCreate(false, true);
    }

    @Test
    public void testCreateWithMainOrganizationAndNoStatusMessageAvailable() throws Exception {
        templateCreate(true, false);
    }

    private void templateCreate(boolean isMainOrganization, boolean alreadyReceivedStatusMessage) throws Exception {
        final Organization organization = new Organization();
        organization.setId(1L);
        organization.setMain(isMainOrganization);

        // expected content
        final StatusMessageContent expectedContent = new StatusMessageContent();

        // main organization
        if(!isMainOrganization) {
            final Organization mainOrganization = new Organization();
            mainOrganization.setName("main organization");
            mainOrganization.setHealthDataIDType("RIZIV");
            mainOrganization.setHealthDataIDValue("11111111");
            mainOrganization.setHd4prc(false);
            EasyMock.expect(organizationService.getMain()).andReturn(mainOrganization);

            final StatusMessageContent organizationContent = new StatusMessageContent();
            organizationContent.set(StatusMessageContent.HD_IDENTIFICATION_NAME, mainOrganization.getName());
            organizationContent.set(StatusMessageContent.HD_IDENTIFICATION_TYPE, mainOrganization.getHealthDataIDType());
            organizationContent.set(StatusMessageContent.HD_IDENTIFICATION_VALUE, mainOrganization.getHealthDataIDValue());

            expectedContent.set(StatusMessageContent.IS_MAIN_ORGANIZATION, false);
            expectedContent.set(StatusMessageContent.MAIN_ORGANIZATION, organizationContent);
        } else {
            expectedContent.set(StatusMessageContent.IS_MAIN_ORGANIZATION, true);
        }

        // last hd4res status message received
        final StatusMessage statusMessage = new StatusMessage();
        statusMessage.setCreatedOn(new Timestamp(new Date().getTime()));
        final List<StatusMessage> statusMessages = Arrays.asList(statusMessage);
        final StatusMessageSearch search = new StatusMessageSearch();
        search.setLastOnly(true);
        search.setHealthDataIdentification(new HealthDataIdentification(organization));
        if(alreadyReceivedStatusMessage) {
            EasyMock.expect(statusMessageService.getAll(search)).andReturn(statusMessages);
            expectedContent.set(StatusMessageContent.LAST_HD4RES_STATUS_MESSAGE_RECEIVED, new SimpleDateFormat(DateFormat.DATE_AND_TIME.getPattern()).format(statusMessage.getCreatedOn()));
        } else {
            EasyMock.expect(statusMessageService.getAll(search)).andReturn(new ArrayList<StatusMessage>());
        }

        // run
        EasyMock.replay(statusMessageService, organizationService);
        final StatusMessageContent content = factory.createContent(organization);
        EasyMock.verify(statusMessageService, organizationService);

        Assert.assertEquals(expectedContent.toString(), content.toString());

        LOG.info(new ObjectMapper().writeValueAsString(content));
    }

}
