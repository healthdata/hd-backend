/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.gathering.service.IINSSService;
import org.junit.Assert;
import org.junit.Test;

public class INSSServiceTest {

	private final IINSSService inssService = new INSSService();

	@Test
	public void testTrim1() {
		String trim = inssService.trim("11-11-11-111-11");
		Assert.assertEquals("11111111111", trim);
	}

	@Test
	public void testTrim2() {
		String trim = inssService.trim("111111 111 11");
		Assert.assertEquals("11111111111", trim);
	}

	@Test
	public void testTrim3() {
		String trim = inssService.trim("11.11.11/111/11");
		Assert.assertEquals("11111111111", trim);
	}

	@Test
	public void testTrim4() {
		String trim = inssService.trim("111111_11111");
		Assert.assertEquals("111111_11111", trim);
	}

	@Test
	public void testValidate1() {
		boolean validate = inssService.validate("12011310188");
		Assert.assertTrue(validate);
	}

	@Test
	public void testValidate2() {
		boolean validate = inssService.validate("10041933937");
		Assert.assertTrue(validate);
	}

	@Test
	public void testValidate3() {
		boolean validate = inssService.validate("85101138771");
		Assert.assertTrue(validate);
	}

	@Test
	public void testValidate4() {
		boolean validate = inssService.validate("851-1138771");
		Assert.assertFalse(validate);
	}

	@Test
	public void testValidate5() {
		boolean validate = inssService.validate("8A101138771");
		Assert.assertFalse(validate);
	}

	@Test
	public void testValidate6() {
		boolean validate = inssService.validate("851A1138771");
		Assert.assertFalse(validate);
	}

	@Test
	public void testValidate7() {
		boolean validate = inssService.validate("85131138771");
		Assert.assertFalse(validate);
	}

	@Test
	public void testValidate8() {
		boolean validate = inssService.validate("85101A38771");
		Assert.assertFalse(validate);
	}

	@Test
	public void testValidate9() {
		boolean validate = inssService.validate("85104538771");
		Assert.assertFalse(validate);
	}

	@Test
	public void testValidate10() {
		boolean validate = inssService.validate("851011A8771");
		Assert.assertFalse(validate);
	}

	@Test
	public void testValidate11() {
		boolean validate = inssService.validate("85101100071");
		Assert.assertFalse(validate);
	}

	@Test
	public void testValidate12() {
		boolean validate = inssService.validate("85101199971");
		Assert.assertFalse(validate);
	}

	@Test
	public void testValidate13() {
		boolean validate = inssService.validate("851011387A1");
		Assert.assertFalse(validate);
	}

	@Test
	public void testValidate14() {
		boolean validate = inssService.validate("85315138771");
		Assert.assertFalse(validate);
	}

	@Test
	public void testValidate15() {
		boolean validate = inssService.validate("85515138771");
		Assert.assertFalse(validate);
	}

	@Test
	public void testValidate16() {
		boolean validate = inssService.validate("85101138700");
		Assert.assertFalse(validate);
	}

	@Test
	public void testValidate17() {
		boolean validate = inssService.validate("09111066775");
		Assert.assertTrue(validate);
	}

}
