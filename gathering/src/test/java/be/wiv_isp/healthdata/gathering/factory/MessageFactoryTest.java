/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.factory;

import be.wiv_isp.healthdata.common.util.IdentificationType;
import be.wiv_isp.healthdata.gathering.action.workflow.CreateForReviewRegistrationAction;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.factory.impl.MessageFactory;
import be.wiv_isp.healthdata.orchestration.action.workflow.WorkflowAction;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.factory.IAbstractMessageFactory;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IIdentificationService;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

@Ignore
public class MessageFactoryTest {

	private IConfigurationService configurationService;
	private IIdentificationService identificationService;

	private final IAbstractMessageFactory messageFactory = new MessageFactory();

	@Before
	public void setUp() {
		configurationService = EasyMock.createNiceMock(IConfigurationService.class);
		identificationService = EasyMock.createNiceMock(IIdentificationService.class);

		ReflectionTestUtils.setField(messageFactory, "configurationService", configurationService);
		ReflectionTestUtils.setField(messageFactory, "identificationService", identificationService);
	}

	@Test
	public void testMapWorkflowActionWithPatientIdentifier() {
		testMapWorkflowAction(true);
	}

	@Test
	public void testMapWorkflowActionWithoutPatientIdentifier() {
		testMapWorkflowAction(false);
	}

	private void testMapWorkflowAction(boolean withPatientIdentifier) {
		final WorkflowAction action = new CreateForReviewRegistrationAction();

		String patientIdentifier = "patientIdentifier";
		final String marshalledAction = "marshalledAction";
		final String ttpProject = "ttpProject";
		final String hd4dpWorkflowId = "hd4dpWorkflowId";

		final RegistrationWorkflow workflow = new RegistrationWorkflow();

		if (withPatientIdentifier) {
			workflow.getMetaData().put(AbstractRegistrationWorkflow.MetaData.PATIENT_IDENTIFIER, patientIdentifier);
		} else {
			patientIdentifier = "NO_PATIENT_ID_DEFINED";
		}

		final Identification eHealthCodageIdentification = new Identification();
		eHealthCodageIdentification.setType(IdentificationType.CBE);
		eHealthCodageIdentification.setValue("eHealthCodageIdentificationValue");
		eHealthCodageIdentification.setApplicationId("eHealthCodageApplicationId");
		eHealthCodageIdentification.setQuality("eHealthCodageQuality");

		final Identification hd4resIdentification = new Identification();
		hd4resIdentification.setType(IdentificationType.CBE);
		hd4resIdentification.setValue("hd4resIdentificationValue");
		hd4resIdentification.setApplicationId("hd4resApplicationId");
		hd4resIdentification.setQuality("hd4resQuality");

		EasyMock.expect(identificationService.getEHealthCodageIdentification()).andReturn(eHealthCodageIdentification);
		EasyMock.expect(identificationService.getHd4ResIdentification()).andReturn(hd4resIdentification);
		EasyMock.expect(configurationService.get(ConfigurationKey.EHEALTH_CODAGE_TTP_PROJECT)).andReturn(createConfiguration(ttpProject));

		EasyMock.replay(identificationService);
		EasyMock.replay(configurationService);

		Message message = messageFactory.createOutgoingMessage(action, workflow);

		final CSVContent expectedCSVContent = new CSVContent();
		expectedCSVContent.putPatientId(patientIdentifier);
		expectedCSVContent.putWorkflowId(hd4dpWorkflowId);
		expectedCSVContent.setLastField(marshalledAction);
		expectedCSVContent.setUseDescriptors(false);

//		Assert.assertEquals(eHealthCodageIdentification, message.getAddressee());
//		Assert.assertEquals(hd4resIdentification, message.getEncryptionAddressee());
		Assert.assertEquals(ttpProject, message.getMetadata().get(Message.Metadata.TTP_PROJECT));
		Assert.assertArrayEquals(expectedCSVContent.toString().getBytes(), message.getContent());

		EasyMock.verify(identificationService);
		EasyMock.verify(configurationService);
	}

	private Configuration createConfiguration(final String value) {
		final Configuration configuration = new Configuration();
		configuration.setValue(value);
		return configuration;
	}
}
