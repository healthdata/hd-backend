/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.workflow.action;

import be.wiv_isp.healthdata.common.domain.Baseline;
import be.wiv_isp.healthdata.common.domain.BaselineType;
import be.wiv_isp.healthdata.common.domain.RegistryDependentIdGenerationDefinition;
import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupDtoV7;
import be.wiv_isp.healthdata.common.dto.FollowUpDefinitionDto;
import be.wiv_isp.healthdata.common.domain.enumeration.TimeUnit;
import be.wiv_isp.healthdata.gathering.action.workflow.SendRegistrationAction;
import be.wiv_isp.healthdata.gathering.centralplatform.service.ICentralPlatformService;
import be.wiv_isp.healthdata.gathering.dao.IRegistrationWorkflowDao;
import be.wiv_isp.healthdata.gathering.dao.IStableDataDao;
import be.wiv_isp.healthdata.gathering.domain.MappingContext;
import be.wiv_isp.healthdata.gathering.domain.RegistrationDocument;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.StableData;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationDocumentSearch;
import be.wiv_isp.healthdata.gathering.factory.IMappingContextFactory;
import be.wiv_isp.healthdata.gathering.factory.IMessageFactory;
import be.wiv_isp.healthdata.gathering.factory.impl.MessageFactory;
import be.wiv_isp.healthdata.gathering.service.IDataCollectionService;
import be.wiv_isp.healthdata.gathering.service.IFollowUpService;
import be.wiv_isp.healthdata.gathering.service.IRegistrationWorkflowService;
import be.wiv_isp.healthdata.gathering.service.IStableDataService;
import be.wiv_isp.healthdata.gathering.service.impl.*;
import be.wiv_isp.healthdata.orchestration.dao.*;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.*;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import be.wiv_isp.healthdata.orchestration.dto.VersionDetailsDto;
import be.wiv_isp.healthdata.orchestration.mapping.IMappingContext;
import be.wiv_isp.healthdata.orchestration.service.*;
import be.wiv_isp.healthdata.orchestration.service.impl.IdentificationService;
import be.wiv_isp.healthdata.orchestration.service.impl.MessageService;
import be.wiv_isp.healthdata.orchestration.util.DateUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.easymock.EasyMock;
import org.easymock.LogicalOperator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = {"classpath:/applicationContext-test-dao.xml"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SendRegistrationActionTest {

    @Autowired
    private IRegistrationWorkflowDao workflowDao;

    @Autowired
    private IFollowUpDao<FollowUp> followUpDao;

    @Autowired
    private IElasticSearchInfoDao elasticSearchInfoDao;

    @Autowired
    private IRegistrationDocumentDao<RegistrationDocument, RegistrationDocumentSearch> documentDao;

    @Autowired
    private IAttachmentDao attachmentDao;

    @Autowired
    private IAttachmentContentDao attachmentContentDao;

    @Autowired
    private IOrganizationDao organizationDao;

    @Autowired
    private IMessageDao messageDao;

    @Autowired
    private IStableDataDao stableDataDao;

    // test parameters
    private boolean followUpConditionValue = false;
    private boolean followUpMustBeScheduled = false;
    private boolean generateRegistryDependentId = false;
    private boolean saveStableData = false;

    @Autowired
    @Qualifier("transactionManager")
    protected PlatformTransactionManager txManager;

    private IRegistrationWorkflowService workflowService = new RegistrationWorkflowService();
    private IFollowUpService followUpService = new FollowUpService();
    private IElasticSearchInfoService elasticSearchInfoService = new ElasticSearchInfoService();
    private IAbstractRegistrationDocumentService documentService = new RegistrationDocumentService();
    private IAttachmentService attachmentService = new AttachmentService();
    private IMessageFactory messageFactory = new MessageFactory();
    private IMessageService messageService = new MessageService();
    private IdentificationService identificationService = new IdentificationService();
    private IStableDataService stableDataService = new StableDataService();

    private IUserDataCollectionService userDataCollectionService = EasyMock.createNiceMock(IUserDataCollectionService.class);
    private IConfigurationService configurationService = EasyMock.createNiceMock(IConfigurationService.class);
    private IDataCollectionDefinitionForwardService catalogueService = EasyMock.createNiceMock(IDataCollectionDefinitionForwardService.class);
    private INoteMigrationService<RegistrationWorkflow> noteMigrationService = EasyMock.createNiceMock(INoteMigrationService.class);
    private ICentralPlatformService centralPlatformService = EasyMock.createNiceMock(ICentralPlatformService.class);
    private IElasticSearchService elasticSearchService = EasyMock.createNiceMock(IElasticSearchService.class);
    private IVersionService versionService = EasyMock.createNiceMock(IVersionService.class);
    private IDataCollectionService dataCollectionService = EasyMock.createNiceMock(IDataCollectionService.class);
    private INoteService noteService = EasyMock.createNiceMock(INoteService.class);
    private IMappingContextFactory mappingContextFactory = EasyMock.createNiceMock(IMappingContextFactory.class);
    private be.wiv_isp.healthdata.gathering.service.IMappingService mappingService = EasyMock.createNiceMock(be.wiv_isp.healthdata.gathering.service.IMappingService.class);

    @Before
    public void init() throws SQLException {
        ReflectionTestUtils.setField(workflowService, "workflowDao", workflowDao);
        ReflectionTestUtils.setField(workflowService, "documentService", documentService);
        ReflectionTestUtils.setField(followUpService, "dao", followUpDao);
        ReflectionTestUtils.setField(elasticSearchInfoService, "dao", elasticSearchInfoDao);
        ReflectionTestUtils.setField(documentService, "documentDao", documentDao);
        ReflectionTestUtils.setField(attachmentService, "attachmentDao", attachmentDao);
        ReflectionTestUtils.setField(attachmentService, "attachmentContentDao", attachmentContentDao);
        ReflectionTestUtils.setField(attachmentService, "documentDao", documentDao);
        ReflectionTestUtils.setField(messageService, "dao", messageDao);
        ReflectionTestUtils.setField(stableDataService, "stableDataDao", stableDataDao);

        ReflectionTestUtils.setField(attachmentService, "configurationService", configurationService);
        ReflectionTestUtils.setField(identificationService, "configurationService", configurationService);
        ReflectionTestUtils.setField(messageFactory, "configurationService", configurationService);
        ReflectionTestUtils.setField(messageFactory, "identificationService", identificationService);
        ReflectionTestUtils.setField(followUpService, "registrationWorkflowService", workflowService);

        EasyMock.expect(configurationService.get(ConfigurationKey.MAX_ATTACHMENT_SIZE)).andReturn(buildConfiguration("1000000")).anyTimes();
        EasyMock.expect(configurationService.get(ConfigurationKey.EHEALTH_CODAGE_TTP_PROJECT)).andReturn(buildConfiguration("healthdata")).anyTimes();
        EasyMock.expect(configurationService.get(ConfigurationKey.HD4RES_IDENTIFICATION_VALUE)).andReturn(buildConfiguration("99999999")).anyTimes();
        EasyMock.expect(configurationService.get(ConfigurationKey.HD4RES_IDENTIFICATION_TYPE)).andReturn(buildConfiguration("EHP")).anyTimes();
        EasyMock.expect(configurationService.get(ConfigurationKey.HD4RES_APPLICATION_ID)).andReturn(buildConfiguration("HEALTHDATA")).anyTimes();
        EasyMock.replay(configurationService);
    }

    @After
    public void after() {
        EasyMock.verify(configurationService);
    }

    @Test
    public void testExecuteSendAction() throws JSONException {
        executeSendAction();
    }

    @Test
    public void testFollowUpsIsScheduled() throws JSONException {
        followUpConditionValue = true;
        followUpMustBeScheduled = true;
        executeSendAction();
    }

    @Test
    public void testGenerateRegistryDependentId() throws JSONException {
        generateRegistryDependentId = true;
        executeSendAction();
    }

    @Test
    public void testSaveStableData() throws JSONException {
        saveStableData = true;
        executeSendAction();
    }

    private void executeSendAction() throws JSONException {
        final Organization organization = createOrganization();
        RegistrationWorkflow registration = createRegistration(organization);

        final SendRegistrationAction action = new SendRegistrationAction();

        // set autowired service
        ReflectionTestUtils.setField(action, "workflowService", workflowService);
        ReflectionTestUtils.setField(action, "followUpService", followUpService);
        ReflectionTestUtils.setField(action, "identificationService", identificationService);
        ReflectionTestUtils.setField(action, "followUpService", followUpService);
        ReflectionTestUtils.setField(action, "messageFactory", messageFactory);
        ReflectionTestUtils.setField(action, "messageService", messageService);
        ReflectionTestUtils.setField(action, "stableDataService", stableDataService);
        ReflectionTestUtils.setField(action, "txManager", txManager);

        // set mocks
        ReflectionTestUtils.setField(action, "userDataCollectionService", userDataCollectionService);
        ReflectionTestUtils.setField(action, "catalogueService", catalogueService);
        ReflectionTestUtils.setField(action, "noteMigrationService", noteMigrationService);
        ReflectionTestUtils.setField(action, "centralPlatformService", centralPlatformService);
        ReflectionTestUtils.setField(action, "elasticSearchService", elasticSearchService);
        ReflectionTestUtils.setField(action, "versionService", versionService);
        ReflectionTestUtils.setField(action, "dataCollectionService", dataCollectionService);
        ReflectionTestUtils.setField(action, "noteService", noteService);
        ReflectionTestUtils.setField(action, "mappingService", mappingService);
        ReflectionTestUtils.setField(action, "mappingContextFactory", mappingContextFactory);

        final DataCollectionDefinitionDtoV7 dcd = buidDataCollectionDefinition();
        EasyMock.expect(catalogueService.get(1L, organization)).andReturn(dcd);
        final HDServiceUser userDetails = HDServiceUser.create("username", organization);
        EasyMock.expect(userDataCollectionService.isUserAuthorized(EasyMock.eq(userDetails), EasyMock.cmp(registration, idBasedComparator(), LogicalOperator.EQUAL))).andReturn(true);

        EasyMock.expect(noteMigrationService.isMigrated(EasyMock.cmp(registration, idBasedComparator(), LogicalOperator.EQUAL))).andReturn(true).anyTimes();
        EasyMock.expect(noteService.getByDocumentId(WorkflowType.REGISTRATION, registration.getDocument().getId())).andReturn(new ArrayList<Note>());
        final JSONObject computedExpression = new JSONObject();
        computedExpression.put("followUpCondition", followUpConditionValue);
        EasyMock.expect(mappingService.getComputedExpressions(EasyMock.cmp(registration, idBasedComparator(), LogicalOperator.EQUAL))).andReturn(computedExpression);
        EasyMock.expect(versionService.getVersionDetails()).andReturn(buildVersionDetails()).anyTimes();
        elasticSearchService.index(registration.getId());
        EasyMock.expectLastCall();

        if(generateRegistryDependentId) {
            final RegistryDependentIdGenerationDefinition def = new RegistryDependentIdGenerationDefinition();
            def.setGenerationEnabled(true);
            def.setType(RegistryDependentIdGenerationDefinition.Type.TYPE_1);
            def.setPhase(RegistryDependentIdGenerationDefinition.Phase.SUBMISSION);
            EasyMock.expect(centralPlatformService.getRegistryDependentIdGenerationDefinition("TEST")).andReturn(def);
            EasyMock.expect(centralPlatformService.getRegistryDependentId(organization, "TEST", RegistryDependentIdGenerationDefinition.Phase.SUBMISSION)).andReturn("__registry_dependent_id__");

            final DocumentData documentData = new DocumentData();
            documentData.setContent(registration.getDocument().getDocumentContent());
            documentData.setCoded(registration.getDocument().getCodedContent());
            documentData.setPrivate(registration.getDocument().getPrivateContent());
            EasyMock.expect(mappingContextFactory.create(organization, registration.getFollowUps(), "__registry_dependent_id__")).andReturn(new MappingContext());
            EasyMock.expect(mappingService.refresh((MappingContext) EasyMock.anyObject(), EasyMock.aryEq(dcd.getContent()), EasyMock.cmp(registration, idBasedComparator(), LogicalOperator.EQUAL))).andReturn(documentData);
        } else {
            final RegistryDependentIdGenerationDefinition def = new RegistryDependentIdGenerationDefinition();
            def.setGenerationEnabled(false);
            EasyMock.expect(centralPlatformService.getRegistryDependentIdGenerationDefinition("TEST")).andReturn(def).anyTimes();
        }

        if(saveStableData) {
            EasyMock.expect(dataCollectionService.getStableData()).andReturn(new HashSet<>(Collections.singletonList("TEST"))).anyTimes();
            EasyMock.expect(mappingContextFactory.create(EasyMock.cmp(registration, idBasedComparator(), LogicalOperator.EQUAL))).andReturn(new MappingContext());
            EasyMock.expect(mappingService.mapFromJsonToCsv(EasyMock.cmp(registration, idBasedComparator(), LogicalOperator.EQUAL), (IMappingContext) EasyMock.anyObject(), EasyMock.aryEq(dcd.getContent()), EasyMock.eq(true))).andReturn("value1;value2");
        } else {
            EasyMock.expect(dataCollectionService.getStableData()).andReturn(new HashSet<String>()).anyTimes();
        }

        Assert.assertTrue(messageDao.getAll().isEmpty());
        Assert.assertTrue(stableDataDao.getAll().isEmpty());
        List<FollowUp> followUps = followUpDao.getAll();
        Assert.assertEquals(1, followUps.size());
        FollowUp followUp = followUps.get(0);
        Assert.assertFalse(followUp.isScheduled());

        EasyMock.replay(catalogueService, userDataCollectionService, noteMigrationService, centralPlatformService, versionService, dataCollectionService, noteService, mappingService, elasticSearchService, mappingContextFactory);

        registration = action.preExecute(registration, userDetails);
        registration = action.execute(registration, userDetails);
        registration = action.postExecute(registration, userDetails);

        EasyMock.verify(catalogueService, userDataCollectionService, noteMigrationService, centralPlatformService, versionService, dataCollectionService, noteService, mappingService, elasticSearchService, mappingContextFactory);

        followUps = followUpDao.getAll();
        Assert.assertEquals(1, followUps.size());
        followUp = followUps.get(0);
        Assert.assertEquals(followUpMustBeScheduled, followUp.isScheduled());

        final List<StableData> stableDataList = stableDataDao.getAll();
        if(saveStableData) {
            Assert.assertEquals(1, stableDataList.size());
            final StableData stableData = stableDataList.get(0);
            Assert.assertEquals("TEST", stableData.getDataCollectionName());
            Assert.assertEquals("123456789", stableData.getPatientId());
            Assert.assertEquals(organization, stableData.getOrganization());
            Assert.assertArrayEquals("value1;value2".getBytes(), stableData.getCsv());
        } else {
            Assert.assertTrue(stableDataDao.getAll().isEmpty());
        }

        if(generateRegistryDependentId) {
            Assert.assertEquals("__registry_dependent_id__", registration.getRegistryDependentId());
        } else {
            Assert.assertNull(registration.getRegistryDependentId());
        }

        final List<Message> messages = messageDao.getAll();
        Assert.assertEquals(1, messages.size());
        final Message message = messages.get(0);
        Assert.assertEquals(MessageStatus.OUTBOX, message.getStatus());
        Assert.assertEquals(MessageType.WORKFLOW_ACTION, message.getType());
        Assert.assertEquals(organization, message.getOrganization());
        Assert.assertEquals(buildMessageCorrespondent("EHP", "99999999", "HEALTHDATA"), message.getCorrespondent());
        Assert.assertEquals(registration.getId(), message.getWorkflowId());
    }

    private DataCollectionDefinitionDtoV7 buidDataCollectionDefinition() {
        final DataCollectionGroupDtoV7 dcg = new DataCollectionGroupDtoV7();
        dcg.setStartDate(new Timestamp(DateUtils.remove(new Date(), new PeriodExpression(1, TimeUnit.DAY)).getTime()));
        dcg.setEndDateCreation(new Timestamp(DateUtils.add(new Date(), new PeriodExpression(1, TimeUnit.DAY)).getTime()));

        final DataCollectionDefinitionDtoV7 dcd = new DataCollectionDefinitionDtoV7();
        dcd.setId(1L);
        dcd.setDataCollectionName("TEST");
        dcd.setDataCollectionGroup(dcg);
        dcd.setFollowUpDefinitions(createModifiableList(buildFollowUpDefinition()));
        return dcd;
    }

    private MessageCorrespondent buildMessageCorrespondent(String type, String value, String applicationId) {
        final MessageCorrespondent messageCorrespondent = new MessageCorrespondent();
        messageCorrespondent.setIdentificationType(type);
        messageCorrespondent.setIdentificationValue(value);
        messageCorrespondent.setApplicationId(applicationId);
        return messageCorrespondent;
    }

    private Attachment buildAttachment() {
        final AttachmentContent content = new AttachmentContent();
        content.setContent("content".getBytes());

        final Attachment attachment = new Attachment();
        attachment.setContent(content);
        attachment.setUuid("664051b8-08c7-11e7-93ae-92361f002671");
        attachment.setFilename("test.txt");
        return attachment;
    }

    private VersionDetailsDto buildVersionDetails() {
        final VersionDetailsDto version = new VersionDetailsDto();
        version.setBackendCommit("backend-commit");
        return version;
    }

    private FollowUpDefinitionDto buildFollowUpDefinition() {
        final FollowUpDefinitionDto def = new FollowUpDefinitionDto();
        def.setName("FollowUpName");
        def.setLabel("FollowUpLabel");
        def.setDescription("FollowUpDescription");
        def.setTiming("2w");
        def.setBaseline(new Baseline(BaselineType.REGISTRATION_SUBMISSION_DATE, null));
        def.setConditions(createModifiableList("followUpCondition"));
        return def;
    }

    private FollowUp buildFollowUp() {
        final FollowUp followUp = new FollowUp();
        followUp.setName("FollowUpName");
        followUp.setLabel("FollowUpLabel");
        followUp.setDescription("FollowUpDescription");
        followUp.setTiming("2w");
        followUp.setBaseline(new Baseline(BaselineType.REGISTRATION_SUBMISSION_DATE, null));
        followUp.setConditions(createModifiableList("followUpCondition"));
        followUp.setActive(false);
        followUp.setActivationDate(null);
        followUp.setSubmittedOn(null);
        return followUp;
    }

    public Comparator<? super RegistrationWorkflow> idBasedComparator() {
        return new Comparator<RegistrationWorkflow>() {
            @Override
            public int compare(RegistrationWorkflow o1, RegistrationWorkflow o2) {
                return o1.getId().compareTo(o2.getId());
            }
        };
    }

    private <T> List<T> createModifiableList(T... objects) {
        final List<T> list = new ArrayList<>();
        for (T o : objects) {
            list.add(o);
        }
        return list;
    }

    private Configuration buildConfiguration(String value) {
        final Configuration configuration = new Configuration();
        configuration.setValue(value);
        return configuration;
    }

    private Organization createOrganization() {
        return new TransactionTemplate(txManager).execute(new TransactionCallback<Organization>() {
            @Override
            public Organization doInTransaction(TransactionStatus status) {
                final Organization org = new Organization();
                org.setMain(true);
                org.setHealthDataIDType("RIZIV");
                org.setHealthDataIDValue("11111111");
                org.setName("Test-Organization");
                return organizationDao.create(org);
            }
        });
    }

    private RegistrationWorkflow createRegistration(final Organization organization) {
        return new TransactionTemplate(txManager).execute(new TransactionCallback<RegistrationWorkflow>() {
            @Override
            public RegistrationWorkflow doInTransaction(TransactionStatus status) {
                final Attachment attachment = attachmentDao.create(buildAttachment());

                final Map<String, TypedValue> coded = new HashMap<>();
                coded.put(AbstractWorkflow.CodedDocumentData.PATIENT_ID, new TypedValue("123456789", "patientID"));

                final DocumentData documentData = new DocumentData();
                documentData.setContent("content".getBytes());
                documentData.setCoded(coded);

                RegistrationDocument document = new RegistrationDocument();
                document.setDocumentData(documentData);
                document.setAttachments(createModifiableList(attachment));
                document.setVersion(1L);

                document = documentDao.create(document);

                final RegistrationWorkflowHistory history = new RegistrationWorkflowHistory();
                history.setAction(WorkflowActionType.SUBMIT);
                history.setNewStatus(WorkflowStatus.OUTBOX);
                history.setSource("CSV");
                history.setFile("/input/TEST.csv");
                history.setMessageType("MessageType");
                history.setTransitionType(WorkflowStatusTransitionType.NORMAL);

                final RegistrationWorkflow registration = new RegistrationWorkflow();
                registration.setStatus(WorkflowStatus.OUTBOX);
                registration.setOrganization(organization);
                registration.setDataCollectionName("TEST");
                registration.setDataCollectionDefinitionId(1L);
                registration.setDocument(document);
                registration.setFollowUps(createModifiableList(buildFollowUp()));
                registration.addHistory(history);
                return workflowDao.create(registration);
            }
        });
    }
}
