/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.gathering.service.IUpdatedUserService;
import be.wiv_isp.healthdata.orchestration.dao.IUserDao;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.Identification;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import be.wiv_isp.healthdata.orchestration.domain.validator.IUserValidator;
import be.wiv_isp.healthdata.orchestration.service.IUserService;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IUserConfigurationService;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.*;

import static org.easymock.EasyMock.*;

public class UserServiceTest {

	private final IUserService userService = new UserService();

	private final PasswordEncoder passwordEncoder = new StandardPasswordEncoder();

	private final IUserDao userDao = createNiceMock(IUserDao.class);
	private final IUserValidator userValidator = createNiceMock(IUserValidator.class);
	private final IUpdatedUserService updatedUserService = createNiceMock(IUpdatedUserService.class);
	private final IUserConfigurationService userConfigurationService = createNiceMock(IUserConfigurationService.class);
	private final IConfigurationService configurationService = createNiceMock(IConfigurationService.class);
	private final TokenStore tokenStore = createNiceMock(TokenStore.class);

	@Rule
	public ExpectedException exception = ExpectedException.none();

	private Organization organization;

	@Before
	public void setup() {
		organization = new Organization();
		organization.setId(1L);

		ReflectionTestUtils.setField(userService, "userDao", userDao);
		ReflectionTestUtils.setField(userService, "passwordEncoder", passwordEncoder);
		ReflectionTestUtils.setField(userService, "userValidator", userValidator);
		ReflectionTestUtils.setField(userService, "updatedUserService", updatedUserService);
		ReflectionTestUtils.setField(userService, "userConfigurationService", userConfigurationService);
		ReflectionTestUtils.setField(userService, "configurationService", configurationService);
		ReflectionTestUtils.setField(userService, "tokenStore", tokenStore);
	}

	@Test
	public void testGetString() throws Exception {
		final User user = new User();
		user.setId(1L);

		expect(userDao.get(1L)).andReturn(user);
		replay(userDao);

		User get = userService.get(1L);

		Assert.assertNotNull(get);

		verify(userDao);
	}

	@Test
	public void testGetAll() throws Exception {
		List<User> users = new ArrayList<>();
		for (long i = 1; i <= 3; i++) {
			final User user = new User();
			user.setId(i);
			user.setUsername("username" + "_" + i);
			user.setPassword("password");
			user.setEnabled(true);
			user.setOrganization(organization);
			user.setAuthorities(new HashSet<>(Collections.singletonList(new Authority("ROLE_USER"))));
			users.add(user);
		}

		UserSearch userSearch = new UserSearch();
		userSearch.setOrganization(organization);
		expect(userDao.getAll(userSearch)).andReturn(users);
		replay(userDao);

		List<User> retrievedUsers = userService.getAll(userSearch);

		Assert.assertNotNull(retrievedUsers);
		Assert.assertEquals(3, retrievedUsers.size());

		verify(userDao);
	}

	@Test
	public void testUpdate() throws Exception {
		final String USER_PASSWORD = "testUserPassword";

		final User user = new User();
		user.setId(1L);
		user.setUsername("testUserName");
		user.setPassword(USER_PASSWORD);
		user.setEnabled(true);
		user.setOrganization(organization);
		user.setAuthorities(new HashSet<>(Collections.singletonList(new Authority("ROLE_USER"))));

		expect(configurationService.isTrue(ConfigurationKey.USE_GUEST_ACCOUNT)).andReturn(false);
		expect(userDao.get(1L)).andReturn(user);
		expect(userDao.update(user)).andReturn(user);
		Identification identification = new Identification();
		identification.setType("NIHII");
		identification.setValue("123456789");
		expectLastCall();
		replay(configurationService, userDao, userValidator);

		User returnedUser = userService.update(user, true);

		verify(configurationService, userDao, userValidator);

		Assert.assertEquals(user, returnedUser);
		Assert.assertTrue(passwordEncoder.matches(USER_PASSWORD, returnedUser.getPassword()));
	}

	@Test
	public void testUpdateNoPasswordEncoding() throws Exception {
		final String USER_PASSWORD = "testUserPassword";
		final boolean NO_PASSWORD_ENCODING = false;

		final User user = new User();
		user.setId(1L);
		user.setUsername("testUserName");
		user.setPassword(USER_PASSWORD);
		user.setEnabled(true);
		user.setOrganization(organization);
		user.setAuthorities(new HashSet<>(Collections.singletonList(new Authority("ROLE_USER"))));

		expect(configurationService.isTrue(ConfigurationKey.USE_GUEST_ACCOUNT)).andReturn(false);
		expect(userDao.get(1L)).andReturn(user);
		expect(userDao.update(user)).andReturn(user);
		expectLastCall();

		Identification identification = new Identification();
		identification.setType("NIHII");
		identification.setValue("123456789");
		replay(configurationService, userDao, userValidator);

		User returnedUser = userService.update(user, NO_PASSWORD_ENCODING);

		verify(configurationService, userDao, userValidator);

		Assert.assertEquals(user, returnedUser);
		Assert.assertEquals(user.getPassword(), returnedUser.getPassword());
	}

	@Test
	public void testFailedGetNotExisiting() throws Exception {
		expect(userDao.get(-1L)).andReturn(null);
		replay(userDao);

		User get = userService.get(-1L);

		Assert.assertNull(get);
	}
}
