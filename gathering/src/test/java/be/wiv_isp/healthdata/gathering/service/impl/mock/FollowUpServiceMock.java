/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl.mock;

import be.wiv_isp.healthdata.common.domain.search.QueryResult;
import be.wiv_isp.healthdata.common.dto.FollowUpDefinitionDto;
import be.wiv_isp.healthdata.gathering.service.IFollowUpService;
import be.wiv_isp.healthdata.orchestration.domain.FollowUp;
import be.wiv_isp.healthdata.orchestration.domain.search.FollowUpSearch;
import org.codehaus.jettison.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FollowUpServiceMock implements IFollowUpService {

    @Override
    public List<FollowUp> getAll(FollowUpSearch search) {
        return new ArrayList<>();
    }

    @Override
    public QueryResult<FollowUp> getAllAsQueryResult(FollowUpSearch search) {
        return null;
    }

    @Override
    public FollowUp create(FollowUp followUp) {
        return null;
    }

    @Override
    public FollowUp get(Long id) {
        return null;
    }

    @Override
    public FollowUp getUnique(FollowUpSearch search) {
        return null;
    }

    @Override
    public FollowUp update(FollowUp followUp) {
        return followUp;
    }

    @Override
    public void delete(FollowUp followUp) {
    }

    @Override
    public List<FollowUp> getAll() {
        return null;
    }

    @Override
    public List<FollowUp> createFollowUps(List<FollowUpDefinitionDto> followUpDefinitionDtos) {
        return null;
    }

    @Override
    public void updateScheduling(List<FollowUp> followUps, JSONObject computedExpressions) {

    }

    @Override
    public boolean activateIfValid(List<FollowUp> followUps, JSONObject computedExpressions) {
        return false;
    }

    @Override
    public void markActiveFollowUpsSubmitted(List<FollowUp> followUps) {

    }

    @Override
    public void unschedule(FollowUp followUp) {

    }
}
