/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.gathering.dao.IRegistrationWorkflowDao;
import be.wiv_isp.healthdata.gathering.domain.RegistrationDocument;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.easymock.EasyMock.*;

public class WorkflowServiceTest {

	private final RegistrationWorkflowService workflowService = new RegistrationWorkflowService();

	private final IRegistrationWorkflowDao workflowDao = EasyMock.createMock(IRegistrationWorkflowDao.class);

	private Organization organization;

	@Before
	public void setup() {
		organization = new Organization();
		organization.setId(1L);
		ReflectionTestUtils.setField(workflowService, "workflowDao", workflowDao);
	}

	@Test
	public void testCreate() throws Exception {
		RegistrationWorkflow workflow = new RegistrationWorkflow();
		workflow.setId(1L);

		expect(workflowDao.create(EasyMock.anyObject(RegistrationWorkflow.class))).andReturn(workflow);
		replay(workflowDao);

		AbstractRegistrationWorkflow created = workflowService.create(organization);

		Assert.assertNotNull(created);

		verify(workflowDao);
	}

	@Test
	public void testGetString() throws Exception {
		RegistrationWorkflow workflow = new RegistrationWorkflow();
		workflow.setId(1L);

		expect(workflowDao.get(1L)).andReturn(workflow);
		replay(workflowDao);

		AbstractRegistrationWorkflow get = workflowService.get(1L);

		Assert.assertNotNull(get);

		verify(workflowDao);
	}

	@Test
	public void testGetLong() throws Exception {
		RegistrationWorkflow workflow = new RegistrationWorkflow();
		workflow.setId(1L);

		expect(workflowDao.get(1L)).andReturn(workflow);
		replay(workflowDao);

		AbstractRegistrationWorkflow get = workflowService.get(1L);

		Assert.assertNotNull(get);

		verify(workflowDao);
	}

	@Test
	public void testFailedGetNotExisiting() throws Exception {
		expect(workflowDao.get(1L)).andReturn(null);
		replay(workflowDao);

		AbstractRegistrationWorkflow get = workflowService.get(1L);

		Assert.assertNull(get);
	}

	@Test
	public void testGetAllWithSearch() throws Exception {
		RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		search.setOrganization(organization);

		List<RegistrationWorkflow> result;
		List<RegistrationWorkflow> getAll = new ArrayList<>();
		expect(workflowDao.getAll(search)).andReturn(getAll);
		replay(workflowDao);

		result = workflowService.getAll(search);

		Assert.assertNotNull(result);

		verify(workflowDao);
	}

	@Test
	public void testGetAllSearchWithDocument() throws Exception {
		RegistrationDocument document1 = new RegistrationDocument();
		document1.setDocumentContent("{}".getBytes());

		RegistrationDocument document2 = new RegistrationDocument();
		document2.setDocumentContent("{}".getBytes());

		RegistrationWorkflow workflow1InProgress = new RegistrationWorkflow();
		workflow1InProgress.setStatus(WorkflowStatus.IN_PROGRESS);
		workflow1InProgress.setDataCollectionName("TEST");
		workflow1InProgress.setDocument(document1);

		RegistrationWorkflow workflow2InProgress = new RegistrationWorkflow();
		workflow2InProgress.setStatus(WorkflowStatus.IN_PROGRESS);
		workflow2InProgress.setDataCollectionName("TEST");
		workflow2InProgress.setDocument(document2);

		RegistrationWorkflow workflow3InProgress = new RegistrationWorkflow();
		workflow3InProgress.setStatus(WorkflowStatus.IN_PROGRESS);
		workflow3InProgress.setDataCollectionName("TEST");

		List<RegistrationWorkflow> getAll = new ArrayList<>();
		getAll.add(workflow1InProgress);
		getAll.add(workflow2InProgress);
		getAll.add(workflow3InProgress);

		RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		search.setOrganization(organization);
		search.setWithDocument(true);
		expect(workflowDao.getAll(search)).andReturn(getAll);
		replay(workflowDao);

		List<RegistrationWorkflow> result = workflowService.getAll(search);

		Assert.assertNotNull(result);
		Assert.assertEquals(2, result.size());

		verify(workflowDao);
	}

	@Test
	public void testGetAllSearchWithoutDocument() throws Exception {
		RegistrationDocument document1 = new RegistrationDocument();
		document1.setDocumentContent("{}".getBytes());

		RegistrationDocument document2 = new RegistrationDocument();
		document2.setDocumentContent("{}".getBytes());

		RegistrationWorkflow workflow1InProgress = new RegistrationWorkflow();
		workflow1InProgress.setStatus(WorkflowStatus.IN_PROGRESS);
		workflow1InProgress.setDataCollectionName("TEST");
		workflow1InProgress.setDocument(document1);

		RegistrationWorkflow workflow2InProgress = new RegistrationWorkflow();
		workflow2InProgress.setStatus(WorkflowStatus.IN_PROGRESS);
		workflow2InProgress.setDataCollectionName("TEST");
		workflow2InProgress.setDocument(document2);

		RegistrationWorkflow workflow3InProgress = new RegistrationWorkflow();
		workflow3InProgress.setStatus(WorkflowStatus.IN_PROGRESS);
		workflow3InProgress.setDataCollectionName("TEST");

		List<RegistrationWorkflow> getAll = new ArrayList<>();
		getAll.add(workflow1InProgress);
		getAll.add(workflow2InProgress);
		getAll.add(workflow3InProgress);

		RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		search.setOrganization(organization);
		search.setWithDocument(false);
		expect(workflowDao.getAll(search)).andReturn(getAll);
		replay(workflowDao);

		List<RegistrationWorkflow> result = workflowService.getAll(search);

		Assert.assertNotNull(result);
		Assert.assertEquals(1, result.size());

		verify(workflowDao);
	}

	@Test
	public void testGetAllSearchAll() throws Exception {
		RegistrationDocument document1 = new RegistrationDocument();
		document1.setDocumentContent("{}".getBytes());

		RegistrationDocument document2 = new RegistrationDocument();
		document2.setDocumentContent("{}".getBytes());

		RegistrationWorkflow workflow1InProgress = new RegistrationWorkflow();
		workflow1InProgress.setStatus(WorkflowStatus.IN_PROGRESS);
		workflow1InProgress.setDataCollectionName("TEST");
		workflow1InProgress.setDocument(document1);

		RegistrationWorkflow workflow2InProgress = new RegistrationWorkflow();
		workflow2InProgress.setStatus(WorkflowStatus.IN_PROGRESS);
		workflow2InProgress.setDataCollectionName("TEST");
		workflow2InProgress.setDocument(document2);

		RegistrationWorkflow workflow3InProgress = new RegistrationWorkflow();
		workflow3InProgress.setStatus(WorkflowStatus.IN_PROGRESS);
		workflow3InProgress.setDataCollectionName("TEST");

		List<RegistrationWorkflow> getAll = new ArrayList<>();
		getAll.add(workflow1InProgress);
		getAll.add(workflow2InProgress);
		getAll.add(workflow3InProgress);

		RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		search.setOrganization(organization);
		search.setWithDocument(null);
		expect(workflowDao.getAll(search)).andReturn(getAll);
		replay(workflowDao);

		List<RegistrationWorkflow> result = workflowService.getAll(search);

		Assert.assertNotNull(result);
		Assert.assertEquals(3, result.size());

		verify(workflowDao);
	}

	@Test
	public void testUpdate() throws Exception {
		RegistrationWorkflow workflow = new RegistrationWorkflow();
		workflow.setId(1L);

		expect(workflowDao.update(workflow)).andReturn(workflow);
		replay(workflowDao);

		RegistrationWorkflow update = workflowService.update(workflow);

		Assert.assertNotNull(update);
		verify(workflowDao);
	}
}
