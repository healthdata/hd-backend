/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.gathering.dao.impl;

import be.wiv_isp.healthdata.orchestration.util.DateUtils;
import be.wiv_isp.healthdata.gathering.dao.IUpgradeDao;
import be.wiv_isp.healthdata.gathering.domain.Upgrade;
import be.wiv_isp.healthdata.gathering.domain.search.UpgradeSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UpgradeDaoTest {

    @Autowired
    private IUpgradeDao dao;

    @Test
    public void testSearchByVersion() {
        final Upgrade upgrade1 = createUpgrade();

        final Upgrade upgrade2 = createUpgrade();
        upgrade2.setVersion("2.0.0");

        Assert.assertNotEquals(upgrade1.getVersion(), upgrade2.getVersion());

        final Upgrade created1 = dao.create(upgrade1);
        final Upgrade created2 = dao.create(upgrade2);

        UpgradeSearch search = new UpgradeSearch();
        List<Upgrade> all = dao.getAll(search);
        Assert.assertEquals(2, all.size());

        search.setVersion("1.0.0");
        Assert.assertEquals("1.0.0", created1.getVersion());
        all = dao.getAll(search);
        Assert.assertEquals(1, all.size());

        Assert.assertEquals(created1, all.get(0));
    }

    @Test
    public void testSearchByFrontendCommit() {
        final Upgrade upgrade1 = createUpgrade();

        final Upgrade upgrade2 = createUpgrade();
        upgrade2.setFrontendCommit("frontend-commit2");

        Assert.assertNotEquals(upgrade1.getFrontendCommit(), upgrade2.getFrontendCommit());

        final Upgrade created1 = dao.create(upgrade1);
        final Upgrade created2 = dao.create(upgrade2);

        UpgradeSearch search = new UpgradeSearch();
        List<Upgrade> all = dao.getAll(search);
        Assert.assertEquals(2, all.size());

        search.setFrontendCommit("frontend-commit");
        Assert.assertEquals("frontend-commit", created1.getFrontendCommit());
        all = dao.getAll(search);
        Assert.assertEquals(1, all.size());

        Assert.assertEquals(created1, all.get(0));
    }

    @Test
    public void testSearchByBackendCommit() {
        final Upgrade upgrade1 = createUpgrade();

        final Upgrade upgrade2 = createUpgrade();
        upgrade2.setBackendCommit("backend-commit2");

        Assert.assertNotEquals(upgrade1.getBackendCommit(), upgrade2.getBackendCommit());

        final Upgrade created1 = dao.create(upgrade1);
        final Upgrade created2 = dao.create(upgrade2);

        UpgradeSearch search = new UpgradeSearch();
        List<Upgrade> all = dao.getAll(search);
        Assert.assertEquals(2, all.size());

        search.setBackendCommit("backend-commit");
        Assert.assertEquals("backend-commit", created1.getBackendCommit());
        all = dao.getAll(search);
        Assert.assertEquals(1, all.size());

        Assert.assertEquals(created1, all.get(0));
    }

    @Test
    public void testSearchByMappingCommit() {
        final Upgrade upgrade1 = createUpgrade();

        final Upgrade upgrade2 = createUpgrade();
        upgrade2.setMappingCommit("mapping-commit2");

        Assert.assertNotEquals(upgrade1.getMappingCommit(), upgrade2.getMappingCommit());

        final Upgrade created1 = dao.create(upgrade1);
        final Upgrade created2 = dao.create(upgrade2);

        UpgradeSearch search = new UpgradeSearch();
        List<Upgrade> all = dao.getAll(search);
        Assert.assertEquals(2, all.size());

        search.setMappingCommit("mapping-commit");
        Assert.assertEquals("mapping-commit", created1.getMappingCommit());
        all = dao.getAll(search);
        Assert.assertEquals(1, all.size());

        Assert.assertEquals(created1, all.get(0));
    }

    private Upgrade createUpgrade() {
        final Upgrade upgrade = new Upgrade();
        upgrade.setVersion("1.0.0");
        upgrade.setFrontendCommit("frontend-commit");
        upgrade.setBackendCommit("backend-commit");
        upgrade.setMappingCommit("mapping-commit");
        upgrade.setExecutedOn(DateUtils.currentTimestamp());
        return upgrade;
    }

}