/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.common.domain.Baseline;
import be.wiv_isp.healthdata.common.domain.BaselineType;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.domain.enumeration.TimeUnit;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.service.IFollowUpService;
import be.wiv_isp.healthdata.gathering.service.IRegistrationWorkflowService;
import be.wiv_isp.healthdata.orchestration.domain.FollowUp;
import be.wiv_isp.healthdata.orchestration.domain.PeriodExpression;
import be.wiv_isp.healthdata.orchestration.util.DateUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

public class FollowUpServiceTest {

    final IFollowUpService followUpService = new FollowUpService();

    @Test
    public void testMarkActiveFollowUpSubmitted() {
        FollowUp followUp = new FollowUp();
        followUp.setActive(false);
        followUpService.markActiveFollowUpsSubmitted(Collections.singletonList(followUp));
        Assert.assertFalse(followUp.isSubmitted());

        followUp = new FollowUp();
        followUp.setActive(true);
        followUpService.markActiveFollowUpsSubmitted(Collections.singletonList(followUp));
        Assert.assertTrue(followUp.isSubmitted());
    }

    @Test
    public void testMarkActiveFollowUpSubmittedDoesNothingIfFollowUpIsAlreadySubmitted() {
        final Timestamp submitDate = new Timestamp(DateUtils.remove(new Date(), new PeriodExpression(1, TimeUnit.HOUR)).getTime());

        final FollowUp followUp = new FollowUp();
        followUp.setActive(true);
        followUp.setSubmittedOn(submitDate);
        followUpService.markActiveFollowUpsSubmitted(Collections.singletonList(followUp));
        Assert.assertTrue(followUp.isSubmitted());
        Assert.assertEquals(submitDate, followUp.getSubmittedOn());
    }

    @Test
    public void testActivateIfValidAllConditionsTrue() throws JSONException {
        final JSONObject computedExpressions = new JSONObject();
        computedExpressions.put("expression1", true);
        computedExpressions.put("expression2", true);
        computedExpressions.put("expression3", false);

        FollowUp followUp = new FollowUp();
        followUp.setActive(false);
        followUp.setActivationDate(new Timestamp(DateUtils.remove(new Date(), new PeriodExpression(1, TimeUnit.HOUR)).getTime())); // time condition
        followUp.setConditions(Arrays.asList("expression1", "expression2")); // document conditions

        final boolean activated = followUpService.activateIfValid(Collections.singletonList(followUp), computedExpressions);
        Assert.assertTrue(activated);
        Assert.assertTrue(followUp.isActive());
    }

    @Test
    public void testActivateIfValidNotAllConditionsTrue() throws JSONException {
        final JSONObject computedExpressions = new JSONObject();
        computedExpressions.put("expression1", true);
        computedExpressions.put("expression2", false);
        computedExpressions.put("expression3", false);

        FollowUp followUp = new FollowUp();
        followUp.setActive(false);
        followUp.setActivationDate(new Timestamp(DateUtils.remove(new Date(), new PeriodExpression(1, TimeUnit.HOUR)).getTime())); // time condition
        followUp.setConditions(Arrays.asList("expression1", "expression2")); // document conditions

        final boolean activated = followUpService.activateIfValid(Collections.singletonList(followUp), computedExpressions);
        Assert.assertFalse(activated);
        Assert.assertFalse(followUp.isActive());
    }

    @Test
    public void testActivateIfValidWithMissingCondition() throws JSONException {
        final JSONObject computedExpressions = new JSONObject();
        computedExpressions.put("expression1", true);
        computedExpressions.put("expression3", false);

        FollowUp followUp = new FollowUp();
        followUp.setActive(false);
        followUp.setActivationDate(new Timestamp(DateUtils.remove(new Date(), new PeriodExpression(1, TimeUnit.HOUR)).getTime())); // time condition
        followUp.setConditions(Arrays.asList("expression1", "expression2")); // document conditions

        final boolean activated = followUpService.activateIfValid(Collections.singletonList(followUp), computedExpressions);
        Assert.assertFalse(activated);
        Assert.assertFalse(followUp.isActive());
    }

    @Test
     public void testActivateIfValidWithValidationDateNotOver() throws JSONException {
        final JSONObject computedExpressions = new JSONObject();
        computedExpressions.put("expression1", true);
        computedExpressions.put("expression2", true);
        computedExpressions.put("expression3", false);

        FollowUp followUp = new FollowUp();
        followUp.setActive(false);
        followUp.setActivationDate(new Timestamp(DateUtils.add(new Date(), new PeriodExpression(1, TimeUnit.HOUR)).getTime())); // time condition
        followUp.setConditions(Arrays.asList("expression1", "expression2")); // document conditions

        final boolean activated = followUpService.activateIfValid(Collections.singletonList(followUp), computedExpressions);
        Assert.assertFalse(activated);
        Assert.assertFalse(followUp.isActive());
    }

    @Test
    public void testActivateIfValidWithAlreadyActiveFollowUp() throws JSONException {
        final JSONObject computedExpressions = new JSONObject();
        computedExpressions.put("expression1", true);
        computedExpressions.put("expression2", true);
        computedExpressions.put("expression3", false);

        FollowUp followUp = new FollowUp();
        followUp.setActive(true);
        followUp.setActivationDate(new Timestamp(DateUtils.remove(new Date(), new PeriodExpression(1, TimeUnit.HOUR)).getTime())); // time condition
        followUp.setConditions(Arrays.asList("expression1", "expression2")); // document conditions

        final boolean activated = followUpService.activateIfValid(Collections.singletonList(followUp), computedExpressions);
        Assert.assertFalse(activated);
        Assert.assertTrue(followUp.isActive());
    }

    @Test
    public void testUnscheduledFollowUpWithValidConditionsIsScheduledWithComputedExpressionBaseline() throws JSONException {
        final JSONObject computedExpressions = new JSONObject();
        computedExpressions.put("expression1", true);
        computedExpressions.put("expression2", true);
        computedExpressions.put("expression3", false);
        computedExpressions.put("baseline", "2017-01-01");

        final FollowUp followUp = new FollowUp();
        followUp.setTiming("2w"); // 2 weeks
        followUp.setBaseline(new Baseline(BaselineType.COMPUTED_EXPRESSION, "baseline"));
        followUp.setConditions(Arrays.asList("expression1", "expression2")); // document conditions

        Assert.assertFalse(followUp.isScheduled());
        followUpService.updateScheduling(Collections.singletonList(followUp), computedExpressions);
        Assert.assertTrue(followUp.isScheduled());

        final Timestamp activationDate = new Timestamp(DateUtils.add(DateFormat.DATE.parse("2017-01-01"), new PeriodExpression(2, TimeUnit.WEEK)).getTime());
        Assert.assertEquals(activationDate, followUp.getActivationDate());
    }

    @Test
    public void testUnscheduledFollowUpWithValidConditionsWithMissingComputedExpressionBaselineIsNotScheduled() throws JSONException {
        final JSONObject computedExpressions = new JSONObject();
        computedExpressions.put("expression1", true);
        computedExpressions.put("expression2", true);
        computedExpressions.put("expression3", false);

        final FollowUp followUp = new FollowUp();
        followUp.setTiming("2w"); // 2 weeks
        followUp.setBaseline(new Baseline(BaselineType.COMPUTED_EXPRESSION, "baseline"));
        followUp.setConditions(Arrays.asList("expression1", "expression2")); // document conditions

        Assert.assertFalse(followUp.isScheduled());
        followUpService.updateScheduling(Collections.singletonList(followUp), computedExpressions);
        Assert.assertFalse(followUp.isScheduled());
    }

    @Test
    public void testUnscheduledFollowUpWithValidConditionsIsScheduledWithRegistrationSubmissionDateBaseline() throws JSONException {
        final JSONObject computedExpressions = new JSONObject();
        computedExpressions.put("expression1", true);
        computedExpressions.put("expression2", true);
        computedExpressions.put("expression3", false);

        final FollowUp followUp = new FollowUp();
        followUp.setId(1L);
        followUp.setTiming("2w"); // 2 weeks
        followUp.setBaseline(new Baseline(BaselineType.REGISTRATION_SUBMISSION_DATE, null));
        followUp.setConditions(Arrays.asList("expression1", "expression2")); // document conditions

        final IRegistrationWorkflowService registrationWorkflowService = EasyMock.createNiceMock(IRegistrationWorkflowService.class);
        ReflectionTestUtils.setField(followUpService, "registrationWorkflowService", registrationWorkflowService);
        final RegistrationWorkflow registration = new RegistrationWorkflow();
        registration.setSubmittedOn(new Timestamp(DateFormat.DATE.parse("2017-01-01").getTime()));
        EasyMock.expect(registrationWorkflowService.getByFollowUpId(1L)).andReturn(registration).once();
        EasyMock.replay(registrationWorkflowService);

        Assert.assertFalse(followUp.isScheduled());
        followUpService.updateScheduling(Collections.singletonList(followUp), computedExpressions);
        Assert.assertTrue(followUp.isScheduled());

        final Timestamp activationDate = new Timestamp(DateUtils.add(DateFormat.DATE.parse("2017-01-01"), new PeriodExpression(2, TimeUnit.WEEK)).getTime());
        Assert.assertEquals(activationDate, followUp.getActivationDate());

        EasyMock.verify(registrationWorkflowService);
    }

    @Test
    public void testUnscheduledFollowUpWithValidConditionsIsScheduledWithUndefinedRegistrationSubmissionDateBaseline() throws JSONException {
        final JSONObject computedExpressions = new JSONObject();
        computedExpressions.put("expression1", true);
        computedExpressions.put("expression2", true);
        computedExpressions.put("expression3", false);

        final FollowUp followUp = new FollowUp();
        followUp.setId(1L);
        followUp.setTiming("2w"); // 2 weeks
        followUp.setBaseline(new Baseline(BaselineType.REGISTRATION_SUBMISSION_DATE, null));
        followUp.setConditions(Arrays.asList("expression1", "expression2")); // document conditions

        final IRegistrationWorkflowService registrationWorkflowService = EasyMock.createNiceMock(IRegistrationWorkflowService.class);
        ReflectionTestUtils.setField(followUpService, "registrationWorkflowService", registrationWorkflowService);
        final RegistrationWorkflow registration = new RegistrationWorkflow();
        registration.setSubmittedOn(null);
        EasyMock.expect(registrationWorkflowService.getByFollowUpId(1L)).andReturn(registration).once();
        EasyMock.replay(registrationWorkflowService);

        Assert.assertFalse(followUp.isScheduled());
        final Date beforeUpdate = new Date();
        followUpService.updateScheduling(Collections.singletonList(followUp), computedExpressions);
        final Date afterUpdate = new Date();
        Assert.assertTrue(followUp.isScheduled());

        final Date activationDateLowerBound = DateUtils.add(beforeUpdate, new PeriodExpression(2, TimeUnit.WEEK));
        final Date activationDateUpperBound = DateUtils.add(afterUpdate, new PeriodExpression(2, TimeUnit.WEEK));

        Assert.assertTrue(followUp.getActivationDate().getTime() >= activationDateLowerBound.getTime());
        Assert.assertTrue(followUp.getActivationDate().getTime() <= activationDateUpperBound.getTime());

        EasyMock.verify(registrationWorkflowService);
    }

    @Test
    public void testScheduledFollowUpWithInvalidConditionsIsUnscheduled() throws JSONException {
        final JSONObject computedExpressions = new JSONObject();
        computedExpressions.put("expression1", true);
        computedExpressions.put("expression2", false);
        computedExpressions.put("expression3", false);

        FollowUp followUp = new FollowUp();
        followUp.setActive(false);
        followUp.setActivationDate(new Timestamp(DateUtils.add(new Date(), new PeriodExpression(1, TimeUnit.HOUR)).getTime())); // time condition
        followUp.setConditions(Arrays.asList("expression1", "expression2")); // document conditions

        Assert.assertTrue(followUp.isScheduled());
        followUpService.updateScheduling(Collections.singletonList(followUp), computedExpressions);
        Assert.assertFalse(followUp.isScheduled());
    }

}
