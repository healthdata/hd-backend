/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.dao.impl;

import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.gathering.dao.IStableDataDao;
import be.wiv_isp.healthdata.gathering.domain.StableData;
import be.wiv_isp.healthdata.gathering.domain.search.StableDataCountGroupByOrganizationDataCollection;
import be.wiv_isp.healthdata.gathering.domain.search.StableDataSearch;
import be.wiv_isp.healthdata.gathering.domain.view.StableDataView;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class StableDataDaoTest {

	public static final String PATIENT_1 = "patient1";
	public static final String PATIENT_2 = "patient2";
	public static final String SOME_CSV = "someCsv";
	public static final String DATA_COLLECTION_1 = "dataCollection1";
	public static final String DATA_COLLECTION_2 = "dataCollection2";

	@Autowired
	private IStableDataDao dao;

	@Autowired
	private IOrganizationDao organizationDao;

	private StableData stableData1;
	private StableData stableData2;
	private StableData stableData3;
	private StableData stableData4;
	private StableData stableData5;

	private StableDataView stableDataView1;
	private StableDataView stableDataView2;
	private StableDataView stableDataView3;
	private StableDataView stableDataView4;


	private Organization organizationMain;
	private Organization organizationSub1;

	private void setup() throws ParseException{
		organizationMain = createOrganization("11111111", true);
		organizationMain = organizationDao.create(organizationMain);
		organizationSub1 = createOrganization("22222222", false);
		organizationSub1 = organizationDao.create(organizationSub1);



		SimpleDateFormat sdf = new SimpleDateFormat(DateFormat.DDMMYYYY.getPattern());
		Timestamp validFrom1 = new Timestamp(sdf.parse("01/01/2016").getTime());
		Timestamp validFrom2 = new Timestamp(sdf.parse("02/01/2016").getTime());
		Timestamp validFrom3 = new Timestamp(sdf.parse("03/01/2016").getTime());

		stableData1 = new StableData();
		stableData1.setPatientId(PATIENT_1);
		stableData1.setDataCollectionName(DATA_COLLECTION_1);
		stableData1.setValidFrom(validFrom1);
		stableData1.setCsv(SOME_CSV.getBytes());
		stableData1.setOrganization(organizationMain);
		dao.create(stableData1);

		stableData2 = new StableData();
		stableData2.setPatientId(PATIENT_1);
		stableData2.setDataCollectionName(DATA_COLLECTION_1);
		stableData2.setValidFrom(validFrom2);
		stableData2.setCsv(SOME_CSV.getBytes());
		stableData2.setOrganization(organizationMain);
		dao.create(stableData2);

		stableData3 = new StableData();
		stableData3.setPatientId(PATIENT_1);
		stableData3.setDataCollectionName(DATA_COLLECTION_2);
		stableData3.setValidFrom(validFrom3);
		stableData3.setCsv(SOME_CSV.getBytes());
		stableData3.setOrganization(organizationMain);
		dao.create(stableData3);

		stableData4 = new StableData();
		stableData4.setPatientId(PATIENT_2);
		stableData4.setDataCollectionName(DATA_COLLECTION_1);
		stableData4.setValidFrom(validFrom1);
		stableData4.setCsv(SOME_CSV.getBytes());
		stableData4.setOrganization(organizationMain);
		dao.create(stableData4);

		stableData5 = new StableData();
		stableData5.setPatientId(PATIENT_1);
		stableData5.setDataCollectionName(DATA_COLLECTION_1);
		stableData5.setValidFrom(validFrom1);
		stableData5.setCsv(SOME_CSV.getBytes());
		stableData5.setOrganization(organizationSub1);
		dao.create(stableData5);
	}


	private void setupView() throws ParseException {
		organizationMain = createOrganization("11111111", true);
		organizationMain = organizationDao.create(organizationMain);
		organizationSub1 = createOrganization("22222222", false);
		organizationSub1 = organizationDao.create(organizationSub1);

		SimpleDateFormat sdf = new SimpleDateFormat(DateFormat.DDMMYYYY.getPattern());
		Timestamp validFrom1 = new Timestamp(sdf.parse("01/01/2016").getTime());
		Timestamp validFrom3 = new Timestamp(sdf.parse("03/01/2016").getTime());

		stableDataView1 = new StableDataView();
		stableDataView1.setId(1L);
		stableDataView1.setPatientId(PATIENT_1);
		stableDataView1.setDataCollectionName(DATA_COLLECTION_1);
		stableDataView1.setValidFrom(validFrom1);
		stableDataView1.setCsv(SOME_CSV.getBytes());
		stableDataView1.setOrganization(organizationMain);
		dao.create(stableDataView1);

		stableDataView2 = new StableDataView();
		stableDataView2.setId(2L);
		stableDataView2.setPatientId(PATIENT_1);
		stableDataView2.setDataCollectionName(DATA_COLLECTION_2);
		stableDataView2.setValidFrom(validFrom3);
		stableDataView2.setCsv(SOME_CSV.getBytes());
		stableDataView2.setOrganization(organizationMain);
		dao.create(stableDataView2);

		stableDataView3 = new StableDataView();
		stableDataView3.setId(3L);
		stableDataView3.setPatientId(PATIENT_2);
		stableDataView3.setDataCollectionName(DATA_COLLECTION_1);
		stableDataView3.setValidFrom(validFrom1);
		stableDataView3.setCsv(SOME_CSV.getBytes());
		stableDataView3.setOrganization(organizationMain);
		dao.create(stableDataView3);

		stableDataView4 = new StableDataView();
		stableDataView4.setId(4L);
		stableDataView4.setPatientId(PATIENT_1);
		stableDataView4.setDataCollectionName(DATA_COLLECTION_1);
		stableDataView4.setValidFrom(validFrom1);
		stableDataView4.setCsv(SOME_CSV.getBytes());
		stableDataView4.setOrganization(organizationSub1);
		dao.create(stableDataView4);

		stableData1 = new StableData();
		stableData1.setPatientId(PATIENT_1);
		stableData1.setDataCollectionName(DATA_COLLECTION_1);
		stableData1.setValidFrom(validFrom1);
		stableData1.setCsv(SOME_CSV.getBytes());
		stableData1.setOrganization(organizationMain);
		dao.create(stableData1);

	}

	@Test
	public void testGetAll() throws ParseException {
		setup();
		StableDataSearch search = new StableDataSearch();
		List<StableData> stableData = dao.getAll(search);
		Assert.assertEquals(5, stableData.size());
		Assert.assertTrue(stableData.contains(stableData1));
		Assert.assertTrue(stableData.contains(stableData2));
		Assert.assertTrue(stableData.contains(stableData3));
		Assert.assertTrue(stableData.contains(stableData4));
		Assert.assertTrue(stableData.contains(stableData5));
	}

	@Test
	public void testGetAllOrganization() throws ParseException {
		setup();
		StableDataSearch search = new StableDataSearch();
		search.setOrganization(organizationMain);

		List<StableData> stableData = dao.getAll(search);
		Assert.assertEquals(4, stableData.size());
		Assert.assertTrue(stableData.contains(stableData1));
		Assert.assertTrue(stableData.contains(stableData2));
		Assert.assertTrue(stableData.contains(stableData3));
		Assert.assertTrue(stableData.contains(stableData4));
		Assert.assertFalse(stableData.contains(stableData5));
	}

	@Test
	public void testGetAllPatientId() throws ParseException {
		setup();
		StableDataSearch search = new StableDataSearch();
		search.setPatientId(PATIENT_1);

		List<StableData> stableData = dao.getAll(search);
		Assert.assertEquals(4, stableData.size());
		Assert.assertTrue(stableData.contains(stableData1));
		Assert.assertTrue(stableData.contains(stableData2));
		Assert.assertTrue(stableData.contains(stableData3));
		Assert.assertFalse(stableData.contains(stableData4));
		Assert.assertTrue(stableData.contains(stableData5));
	}

	@Test
	public void testGetAllDataCollectionName() throws ParseException {
		setup();
		StableDataSearch search = new StableDataSearch();
		search.setDataCollectionName(DATA_COLLECTION_1);

		List<StableData> stableData = dao.getAll(search);
		Assert.assertEquals(4, stableData.size());
		Assert.assertTrue(stableData.contains(stableData1));
		Assert.assertTrue(stableData.contains(stableData2));
		Assert.assertFalse(stableData.contains(stableData3));
		Assert.assertTrue(stableData.contains(stableData4));
		Assert.assertTrue(stableData.contains(stableData5));
	}

	@Test
	public void testGetView() throws ParseException {
		setupView();

		final StableDataSearch search = new StableDataSearch();
		search.setPatientId(PATIENT_1);
		search.setDataCollectionName(DATA_COLLECTION_1);
		search.setOrganization(organizationMain);
		final StableData stableData = dao.getUnique(search);

		Assert.assertEquals(stableDataView1.getPatientId(), stableData.getPatientId());
		Assert.assertEquals(stableDataView1.getDataCollectionName(), stableData.getDataCollectionName());
		Assert.assertEquals(stableDataView1.getValidFrom(), stableData.getValidFrom());
		Assert.assertArrayEquals(stableDataView1.getCsv(), stableData.getCsv());
		Assert.assertEquals(stableDataView1.getOrganization(), stableData.getOrganization());
	}

	@Test
	public void testGetCountGroupByOrganizationDcdStatus() throws ParseException {
		setup();
		final List<StableDataCountGroupByOrganizationDataCollection> countGroupByOrganizationDataCollectionList = dao.getCountGroupByOrganizationDataCollection();

		final List<StableDataCountGroupByOrganizationDataCollection> expectedCountGroupByOrganizationDataCollectionList = new ArrayList<>();
		expectedCountGroupByOrganizationDataCollectionList.add(new StableDataCountGroupByOrganizationDataCollection("11111111", DATA_COLLECTION_1, 3L, 2L));
		expectedCountGroupByOrganizationDataCollectionList.add(new StableDataCountGroupByOrganizationDataCollection("11111111", DATA_COLLECTION_2, 1L, 1L));
		expectedCountGroupByOrganizationDataCollectionList.add(new StableDataCountGroupByOrganizationDataCollection("22222222", DATA_COLLECTION_1, 1L, 1L));

		Assert.assertTrue(countGroupByOrganizationDataCollectionList.containsAll(expectedCountGroupByOrganizationDataCollectionList));
		Assert.assertTrue(expectedCountGroupByOrganizationDataCollectionList.containsAll(countGroupByOrganizationDataCollectionList));
	}

	private Organization createOrganization(String identificationValue, boolean main) {
		Organization organization = new Organization();
		organization.setHealthDataIDValue(identificationValue);
		organization.setMain(main);
		return organization;
	}
}
