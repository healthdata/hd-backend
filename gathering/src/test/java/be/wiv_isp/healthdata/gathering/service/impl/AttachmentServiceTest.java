/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.gathering.domain.RegistrationDocument;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationDocumentSearch;
import be.wiv_isp.healthdata.orchestration.dao.IAttachmentContentDao;
import be.wiv_isp.healthdata.orchestration.dao.IAttachmentDao;
import be.wiv_isp.healthdata.orchestration.dao.IRegistrationDocumentDao;
import be.wiv_isp.healthdata.orchestration.domain.Attachment;
import be.wiv_isp.healthdata.orchestration.domain.AttachmentContent;
import be.wiv_isp.healthdata.orchestration.service.IAttachmentService;
import org.apache.commons.io.FileUtils;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class AttachmentServiceTest {

    @Autowired
    private IAttachmentDao attachmentDao;

    @Autowired
    private IAttachmentContentDao attachmentContentDao;

    @Autowired
    private IRegistrationDocumentDao<RegistrationDocument, RegistrationDocumentSearch> documentDao;

    private IConfigurationService configurationService = EasyMock.createNiceMock(IConfigurationService.class);

    private IAttachmentService attachmentService;

    @Before
    public void before() {
        attachmentService = new AttachmentService();
        ReflectionTestUtils.setField(attachmentService, "attachmentDao", attachmentDao);
        ReflectionTestUtils.setField(attachmentService, "attachmentContentDao", attachmentContentDao);
        ReflectionTestUtils.setField(attachmentService, "configurationService", configurationService);
        ReflectionTestUtils.setField(attachmentService, "documentDao", documentDao);
    }

    @Test
    public void testCreateAndDelete() throws URISyntaxException, IOException {
        final Configuration configuration = new Configuration();
        configuration.setValue("10000000");
        EasyMock.expect(configurationService.get(ConfigurationKey.MAX_ATTACHMENT_SIZE)).andReturn(configuration).anyTimes();
        EasyMock.replay(configurationService);

        final File file1 = new File(AttachmentServiceTest.class.getClassLoader().getResource("attachments/attachment1.txt").toURI());
        final File file2 = new File(AttachmentServiceTest.class.getClassLoader().getResource("attachments/attachment2.txt").toURI());

        Attachment attachment = new Attachment();
        attachment.setUuid("111111111-aaaaaaaaa");
        attachment.setContent(createContent(FileUtils.readFileToByteArray(file1)));
        attachment.setFilename(file1.getAbsolutePath());
        final Attachment attachment1 = attachmentService.create(attachment);

        Assert.assertEquals(1, attachmentDao.getAll().size());
        Assert.assertEquals(1, attachmentContentDao.getAll().size());

        attachment = new Attachment();
        attachment.setUuid("222222222-aaaaaaaaa");
        attachment.setContent(createContent(FileUtils.readFileToByteArray(file1)));
        attachment.setFilename(file1.getAbsolutePath());
        final Attachment attachment2 = attachmentService.create(attachment);

        Assert.assertEquals(2, attachmentDao.getAll().size());
        Assert.assertEquals(1, attachmentContentDao.getAll().size());

        attachment = new Attachment();
        attachment.setUuid("333333333-aaaaaaaaa");
        attachment.setContent(createContent(FileUtils.readFileToByteArray(file2)));
        attachment.setFilename(file2.getAbsolutePath());
        final Attachment attachment3 = attachmentService.create(attachment);

        Assert.assertEquals(3, attachmentDao.getAll().size());
        Assert.assertEquals(2, attachmentContentDao.getAll().size());

        attachmentService.delete(attachment1);
        Assert.assertEquals(2, attachmentDao.getAll().size());
        Assert.assertEquals(2, attachmentContentDao.getAll().size());

        attachmentService.delete(attachment2);
        Assert.assertEquals(1, attachmentDao.getAll().size());
        Assert.assertEquals(1, attachmentContentDao.getAll().size());

        attachmentService.delete(attachment3);
        Assert.assertEquals(0, attachmentDao.getAll().size());
        Assert.assertEquals(0, attachmentContentDao.getAll().size());

        Assert.assertNull(attachmentDao.get(attachment1.getId()));
        Assert.assertNull(attachmentDao.get(attachment2.getId()));
        Assert.assertNull(attachmentDao.get(attachment3.getId()));

        EasyMock.verify(configurationService);
    }

    @Test
    public void testLinkAttachments() {
        final Configuration configuration = new Configuration();
        configuration.setValue("10000000");
        EasyMock.expect(configurationService.get(ConfigurationKey.MAX_ATTACHMENT_SIZE)).andReturn(configuration).anyTimes();
        EasyMock.replay(configurationService);

        RegistrationDocument document = new RegistrationDocument();
        document.setDocumentContent("Some JSON content".getBytes());

        document = documentDao.create(document);

        final List<Attachment> attachments = new ArrayList();
        final Attachment attachment = createAttachment();
        attachments.add(attachment);

        attachmentService.linkAttachments(document, attachments);

        RegistrationDocument retrievedDocument = documentDao.get(document.getId());
        Assert.assertEquals(1, retrievedDocument.getAttachments().size());

        attachmentService.delete(attachment);

        retrievedDocument = documentDao.get(document.getId());
        Assert.assertEquals(0, retrievedDocument.getAttachments().size());

        EasyMock.verify(configurationService);
    }

    private Attachment createAttachment() {
        final Attachment header = new Attachment();
        header.setUuid("18255e82-48a5-4bc1-b3ba-7214ec693a29");
        header.setFilename("test.txt");
        header.setMimeType("plain/text");
        header.setContent(createAttachmentContent());
        return header;
    }

    private AttachmentContent createAttachmentContent() {
        final byte[] bytes = "content".getBytes();
        final AttachmentContent content = new AttachmentContent();
        content.setContent(bytes);
        content.setSize(bytes.length);
        content.setHash(String.valueOf(Arrays.hashCode(bytes)));
        return content;
    }

    private Set<Long> createSet(Long l) {
        final Set<Long> list = new HashSet<>();
        list.add(l);
        return list;
    }

    private AttachmentContent createContent(byte[] bytes) {
        final AttachmentContent content = new AttachmentContent();
        content.setContent(bytes);
        return content;
    }
}