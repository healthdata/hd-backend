/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.gathering.domain.RegistrationDocument;
import be.wiv_isp.healthdata.orchestration.dao.IDataSourceDao;
import be.wiv_isp.healthdata.orchestration.dao.IRegistrationDocumentDao;
import be.wiv_isp.healthdata.orchestration.domain.DataSource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class DocumentServiceTest {

	@Autowired
	private IRegistrationDocumentDao dao;

	@Autowired
	private IDataSourceDao dataSourceDao;

	private final RegistrationDocumentService documentService = new RegistrationDocumentService();

	@Before
	public void setup() {
		ReflectionTestUtils.setField(documentService, "documentDao", dao);
	}

	@Test
	public void testCreateWithDataSources() throws Exception {
		final RegistrationDocument document = buildDocument();
		document.setDataSources(createDataSources());
		final RegistrationDocument created = documentService.create(document);

		List<DataSource> all = dataSourceDao.getAll();
		Assert.assertEquals(3, all.size());

		created.setDataSources(createDataSources());
		documentService.update(created);

		all = dataSourceDao.getAll();
		Assert.assertEquals(3, all.size()); // verify orphan entities have been removed
	}

	private List<DataSource> createDataSources() {
		final List<DataSource> dataSources = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			final DataSource dataSource = new DataSource();
			dataSource.setFieldPath("field-"+i);
			dataSource.setSource("source-" + i);
			dataSources.add(dataSource);
		}
		return dataSources;
	}

	private RegistrationDocument buildDocument() {
		RegistrationDocument document = new RegistrationDocument();
		document.setDocumentContent("Some JSON content".getBytes());
		return document;
	}
}
