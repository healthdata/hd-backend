/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.dao.IExceptionReportDao;
import be.wiv_isp.healthdata.gathering.factory.impl.MessageFactory;
import be.wiv_isp.healthdata.gathering.service.IExceptionService;
import be.wiv_isp.healthdata.orchestration.dao.IMessageDao;
import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageStatus;
import be.wiv_isp.healthdata.orchestration.dto.EncryptionModuleMessage;
import be.wiv_isp.healthdata.orchestration.factory.IAbstractMessageFactory;
import be.wiv_isp.healthdata.orchestration.service.IEncryptionModuleService;
import be.wiv_isp.healthdata.orchestration.service.*;
import be.wiv_isp.healthdata.orchestration.service.impl.MessageService;
import be.wiv_isp.healthdata.orchestration.service.impl.MessagingService;
import be.wiv_isp.healthdata.orchestration.service.impl.OrganizationService;
import org.apache.commons.io.IOUtils;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = {"classpath:/applicationContext-test-dao.xml"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class MessagingServiceTest {

	private final IMessagingService messagingService = new MessagingService();

	private IOrganizationService organizationService = new OrganizationService();
	private IAbstractMessageFactory messageFactory = new MessageFactory();
	private IMessageService messageService = new MessageService();
	private IExceptionService exceptionService = new ExceptionService();

	private IEncryptionModuleService encryptionModuleService = EasyMock.createStrictMock(IEncryptionModuleService.class);
	private IIdentificationService identificationService = EasyMock.createStrictMock(IIdentificationService.class);
	private IConfigurationService configurationService = EasyMock.createStrictMock(IConfigurationService.class);

	@Autowired
	private IMessageDao messageDao;

	@Autowired
	private IOrganizationDao organizationDao;

	@Autowired
	private IExceptionReportDao exceptionReportDao;

	private Organization organization;

	@Before
	public void before() throws IOException, URISyntaxException {

		final Organization org = new Organization();
		org.setMain(true);
		org.setHealthDataIDType("RIZIV");
		org.setHealthDataIDValue("11111111");
		org.setName("Test-Organization");
		organization = organizationDao.create(org);

		ReflectionTestUtils.setField(messagingService, "messageFactory", messageFactory);
		ReflectionTestUtils.setField(messagingService, "messageService", messageService);
		ReflectionTestUtils.setField(messagingService, "organizationService", organizationService);
		ReflectionTestUtils.setField(messagingService, "encryptionModuleService", encryptionModuleService);
		ReflectionTestUtils.setField(messagingService, "exceptionService", exceptionService);
		ReflectionTestUtils.setField(messageFactory, "identificationService", identificationService);
		ReflectionTestUtils.setField(messageFactory, "configurationService", configurationService);
		ReflectionTestUtils.setField(exceptionService, "exceptionReportDao", exceptionReportDao);
		ReflectionTestUtils.setField(organizationService, "organizationDao", organizationDao);

		ReflectionTestUtils.setField(messageService, "dao", messageDao);
	}

	@Test
	public void testSend() {
		EasyMock.expect(identificationService.getEtkIdentification(organization)).andReturn(buildIdentification("NIHII-HOSPITAL", "11111111", "HEALTHDATA"));
		EasyMock.expect(identificationService.getEHealthCodageIdentification()).andReturn(buildIdentification("CBE", "22222222", "HEALTHDATA"));
		EasyMock.expect(identificationService.getHd4ResIdentification()).andReturn(buildIdentification("NIHII-HOSPITAL", "99999999", "HEALTHDATA"));
		EasyMock.expect(configurationService.get(ConfigurationKey.EHEALTH_CODAGE_TTP_PROJECT)).andReturn(buildConfiguration("healthdata"));
		encryptionModuleService.queueOutgoing((EncryptionModuleMessage) EasyMock.anyObject(), EasyMock.eq(organization)); // TODO build expected EncryptionModuleMessage
		EasyMock.expectLastCall();
		encryptionModuleService.flushAll();
		EasyMock.expectLastCall();

		final Message message = new Message();
		message.setStatus(MessageStatus.OUTBOX);
		message.setRemainingSendingTrials(5);
		message.setOrganization(organization);
		message.setContent("value1;value2;e30=".getBytes());
		message.setCorrespondent(buildCorrespondent("EHP", "99999999", "HEALTHDATA"));
		messageDao.create(message);

		Assert.assertEquals(1, messageDao.getAll().size());

		EasyMock.replay(encryptionModuleService, identificationService, configurationService);
		messagingService.send();
		EasyMock.verify(encryptionModuleService, identificationService, configurationService);

		final List<Message> messages = messageDao.getAll();
		Assert.assertEquals(1, messages.size());
		final Message sentMessage = messages.get(0);
		Assert.assertEquals(MessageStatus.SENT, sentMessage.getStatus());
		Assert.assertEquals(new Integer(4), sentMessage.getRemainingSendingTrials());
	}

	@Test
	public void testErrorSending() {
		EasyMock.expect(identificationService.getEtkIdentification(organization)).andReturn(buildIdentification("NIHII-HOSPITAL", "11111111", "HEALTHDATA"));
		EasyMock.expect(identificationService.getEHealthCodageIdentification()).andReturn(buildIdentification("CBE", "22222222", "HEALTHDATA"));
		EasyMock.expect(identificationService.getHd4ResIdentification()).andReturn(buildIdentification("NIHII-HOSPITAL", "99999999", "HEALTHDATA"));
		EasyMock.expect(configurationService.get(ConfigurationKey.EHEALTH_CODAGE_TTP_PROJECT)).andReturn(buildConfiguration("healthdata"));
		encryptionModuleService.queueOutgoing((EncryptionModuleMessage) EasyMock.anyObject(), EasyMock.eq(organization)); // TODO build expected EncryptionModuleMessage
		EasyMock.expectLastCall().andThrow(new HealthDataException());
		encryptionModuleService.flushAll();
		EasyMock.expectLastCall();

		final Message message = new Message();
		message.setStatus(MessageStatus.OUTBOX);
		message.setRemainingSendingTrials(1); // settings this value to > 1 gives an error (should be fixed)
		message.setOrganization(organization);
		message.setContent("value1;value2;e30=".getBytes());
		message.setCorrespondent(buildCorrespondent("EHP", "99999999", "HEALTHDATA"));
		messageDao.create(message);

		Assert.assertEquals(1, messageDao.getAll().size());
		Assert.assertTrue(exceptionReportDao.getAll().isEmpty());

		EasyMock.replay(encryptionModuleService, identificationService, configurationService);
		messagingService.send();
		EasyMock.verify(encryptionModuleService, identificationService, configurationService);

		Assert.assertEquals(1, exceptionReportDao.getAll().size());
		final List<Message> messages = messageDao.getAll();
		Assert.assertEquals(1, messages.size());
		final Message sentMessage = messages.get(0);
		Assert.assertEquals(MessageStatus.ERROR_SENDING, sentMessage.getStatus());
		Assert.assertEquals(new Integer(0), sentMessage.getRemainingSendingTrials());
	}

	@Test
	public void testReceive() throws IOException {
		final EncryptionModuleMessage incomingMessage1 = new EncryptionModuleMessage();
		incomingMessage1.setContent(IOUtils.toString(MessagingServiceTest.class.getClassLoader().getResourceAsStream("incoming-message.csv"), StandardCharsets.UTF_8));
		incomingMessage1.addMetadata(Message.Metadata.MESSAGE_ID, "123");

		final EncryptionModuleMessage incomingMessage2 = new EncryptionModuleMessage();
		incomingMessage2.setContent(IOUtils.toString(MessagingServiceTest.class.getClassLoader().getResourceAsStream("incoming-message.csv"), StandardCharsets.UTF_8));
		incomingMessage2.addMetadata(Message.Metadata.MESSAGE_ID, "456");

		final List<EncryptionModuleMessage> incomingMessages = new ArrayList<>();
		incomingMessages.add(incomingMessage1);
		incomingMessages.add(incomingMessage2); // duplicate message

		EasyMock.expect(encryptionModuleService.receive(organization)).andReturn(incomingMessages);
		encryptionModuleService.queueOutgoing((EncryptionModuleMessage) EasyMock.anyObject(), EasyMock.eq(organization)); // TODO build expected EncryptionModuleMessage
		EasyMock.expectLastCall().times(2); // acknowledgment sending
		EasyMock.expect(encryptionModuleService.receive(organization)).andReturn(new ArrayList<EncryptionModuleMessage>());

		EasyMock.expect(identificationService.getEtkIdentification(organization)).andReturn(buildIdentification("NIHII-HOSPITAL", "11111111", "HEALTHDATA"));
		EasyMock.expect(identificationService.getEHealthCodageIdentification()).andReturn(buildIdentification("CBE", "22222222", "HEALTHDATA"));
		EasyMock.expect(identificationService.getHd4ResIdentification()).andReturn(buildIdentification("NIHII-HOSPITAL", "99999999", "HEALTHDATA"));
		EasyMock.expect(configurationService.get(ConfigurationKey.EHEALTH_CODAGE_TTP_PROJECT)).andReturn(buildConfiguration("healthdata"));

		EasyMock.expect(identificationService.getEtkIdentification(organization)).andReturn(buildIdentification("NIHII-HOSPITAL", "11111111", "HEALTHDATA"));
		EasyMock.expect(identificationService.getEHealthCodageIdentification()).andReturn(buildIdentification("CBE", "22222222", "HEALTHDATA"));
		EasyMock.expect(identificationService.getHd4ResIdentification()).andReturn(buildIdentification("NIHII-HOSPITAL", "99999999", "HEALTHDATA"));
		EasyMock.expect(configurationService.get(ConfigurationKey.EHEALTH_CODAGE_TTP_PROJECT)).andReturn(buildConfiguration("healthdata"));

		encryptionModuleService.flushAll();
		EasyMock.expectLastCall();

		Assert.assertTrue(messageDao.getAll().isEmpty());

		EasyMock.replay(encryptionModuleService, identificationService, configurationService);
		messagingService.receive();
		EasyMock.verify(encryptionModuleService, identificationService, configurationService);

		final List<Message> messages = messageDao.getAll();
		Assert.assertEquals(1, messages.size());
		final Message message = messages.get(0);
		Assert.assertEquals(MessageStatus.INBOX, message.getStatus());
	}

	private Identification buildIdentification(String type, String value, String applicationId) {
		final Identification identification = new Identification();
		identification.setType(type);
		identification.setValue(value);
		identification.setApplicationId(applicationId);
		return identification;
	}

	private MessageCorrespondent buildCorrespondent(String type, String value, String applicationId) {
		final MessageCorrespondent correspondent = new MessageCorrespondent();
		correspondent.setIdentificationType(type);
		correspondent.setIdentificationValue(value);
		correspondent.setApplicationId(applicationId);
		return correspondent;
	}

	private Configuration buildConfiguration(String value) {
		final Configuration configuration = new Configuration();
		configuration.setValue(value);
		return configuration;
	}

}
