/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.api.mapper;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupDtoV7;
import be.wiv_isp.healthdata.common.dto.VersionDto;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.gathering.domain.Registration;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.dto.RegistrationResponseDtoV1;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Test;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class RegistrationResponseMapperV1Test {

	private final RegistrationResponseMapperV1 mapper = new RegistrationResponseMapperV1("http://www.healthdata.be", "https://catalogue.healthdata.be/healthdata_catalogue");

	@Test
	public void testConvertCreate() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat(DateFormat.DATE_AND_TIME.getPattern());
		sdf.setTimeZone(TimeZone.getTimeZone("Europe/Brussels"));

		RegistrationWorkflow workflow = new RegistrationWorkflow();
		workflow.setId(1L);
		workflow.setStatus(WorkflowStatus.SUBMITTED);

		DataCollectionGroupDtoV7 dcg = new DataCollectionGroupDtoV7();
		dcg.setMajorVersion(1);
		dcg.setStartDate(new Timestamp(sdf.parse("1985-01-01T00:00:00+01:00").getTime()));

		DataCollectionDefinitionDtoV7 dcd = new DataCollectionDefinitionDtoV7(); //
		dcd.setId(1L);
		dcd.setMinorVersion(1);
		dcd.setDataCollectionName("TEST");
		dcd.setContent("{}".getBytes(StandardCharsets.UTF_8));
		dcd.setDataCollectionGroup(dcg);

		Registration registration = new Registration();
		registration.setId(1L);
		registration.setWorkflow(workflow);
		registration.setCreatedOn(new Timestamp(sdf.parse("1985-10-11T00:00:00+01:00").getTime()));
		registration.setUpdatedOn(new Timestamp(sdf.parse("1985-10-11T00:00:00+01:00").getTime()));

		RegistrationResponseDtoV1 response = new RegistrationResponseDtoV1();
		response.addRegistration(dcd, registration);

		String xml = mapper.convert(response);

		XMLUnit.setIgnoreWhitespace(true);

		InputStream inputAsStream = RegistrationRequestMapperV1Test.class.getResourceAsStream("responseXmlCreate.xml");
		String result = IOUtils.toString(inputAsStream);
		XMLAssert.assertXMLEqual(result, xml);
	}

	@Test
	public void testConvertList() throws Exception {
		RegistrationResponseDtoV1 response = new RegistrationResponseDtoV1();
		SimpleDateFormat sdf = new SimpleDateFormat(DateFormat.DATE_AND_TIME.getPattern());
		sdf.setTimeZone(TimeZone.getTimeZone("Europe/Brussels"));


		DataCollectionGroupDtoV7 dcg = new DataCollectionGroupDtoV7();
		dcg.setMajorVersion(1);
		dcg.setStartDate(new Timestamp(sdf.parse("1985-01-01T00:00:00+01:00").getTime()));
		DataCollectionDefinitionDtoV7 dcd = new DataCollectionDefinitionDtoV7();
		dcd.setId(1L);
		dcd.setMinorVersion(1);
		dcd.setDataCollectionName("TEST");
		dcd.setContent("{}".getBytes(StandardCharsets.UTF_8));
		dcd.setDataCollectionGroup(dcg);

		RegistrationWorkflow workflow = new RegistrationWorkflow();
		workflow.setId(1L);
		workflow.setStatus(WorkflowStatus.SUBMITTED);

		Registration registration = new Registration();
		registration.setId(1L);
		registration.setWorkflow(workflow);
		registration.setCreatedOn(new Timestamp(sdf.parse("1985-10-11T00:00:00+01:00").getTime()));
		registration.setUpdatedOn(new Timestamp(sdf.parse("1985-10-11T00:00:00+01:00").getTime()));

		response.addRegistration(dcd, registration);

		RegistrationWorkflow workflow2 = new RegistrationWorkflow();
		workflow2.setId(2L);
		workflow2.setStatus(WorkflowStatus.SUBMITTED);

		Registration registration2 = new Registration();
		registration2.setId(2L);
		registration2.setWorkflow(workflow2);
		registration2.setCreatedOn(new Timestamp(sdf.parse("1985-10-11T00:00:00+01:00").getTime()));
		registration2.setUpdatedOn(new Timestamp(sdf.parse("1985-10-11T00:00:00+01:00").getTime()));

		response.addRegistration(dcd, registration2);

		DataCollectionGroupDtoV7 dcg2 = new DataCollectionGroupDtoV7();
		dcg2.setMajorVersion(1);
		dcg2.setStartDate(new Timestamp(sdf.parse("1985-01-01T00:00:00+01:00").getTime()));
		DataCollectionDefinitionDtoV7 dcd2 = new DataCollectionDefinitionDtoV7();
		dcd2.setId(2L);
		dcd2.setMinorVersion(1);
		dcd2.setDataCollectionName("TEST2");
		dcd2.setContent("{}".getBytes(StandardCharsets.UTF_8));
		dcd2.setDataCollectionGroup(dcg2);

		RegistrationWorkflow workflow3 = new RegistrationWorkflow();
		workflow3.setId(3L);
		workflow3.setStatus(WorkflowStatus.SUBMITTED);

		Registration registration3 = new Registration();
		registration3.setId(3L);
		registration3.setWorkflow(workflow3);
		registration3.setCreatedOn(new Timestamp(sdf.parse("1985-10-11T00:00:00+01:00").getTime()));
		registration3.setUpdatedOn(new Timestamp(sdf.parse("1985-10-11T00:00:00+01:00").getTime()));

		response.addRegistration(dcd2, registration3);

		String xml = mapper.convert(response);

		XMLUnit.setIgnoreWhitespace(true);

		InputStream inputAsStream = RegistrationRequestMapperV1Test.class.getResourceAsStream("responseXmlList.xml");
		String result = IOUtils.toString(inputAsStream);
		XMLAssert.assertXMLEqual(result, xml);
	}

	@Test
	public void testConvertCreateInvalidKmehr() throws Exception {
		RegistrationResponseDtoV1 response = new RegistrationResponseDtoV1();
		response.setErrorMessage("Invalid KMEHR");

		String xml = mapper.convert(response);

		XMLUnit.setIgnoreWhitespace(true);

		InputStream inputAsStream = RegistrationRequestMapperV1Test.class.getResourceAsStream("responseXmlCreateInvalidKmehr.xml");
		String result = IOUtils.toString(inputAsStream);
		XMLAssert.assertXMLEqual(result, xml);
	}

	@Test
	public void testConvertCreateValidationErrors() throws Exception {
		RegistrationResponseDtoV1 response = new RegistrationResponseDtoV1();
		response.setValidationResponse("<validation-errors><validation-error><field>patient/id[44071154303]/deathdate</field><value></value><message>is a required field</message></validation-error></validation-errors>");

		String xml = mapper.convert(response);

		XMLUnit.setIgnoreWhitespace(true);

		InputStream inputAsStream = RegistrationRequestMapperV1Test.class.getResourceAsStream("responseXmlCreateValidationErrors.xml");
		String result = IOUtils.toString(inputAsStream);
		XMLAssert.assertXMLEqual(result, xml);
	}

	private VersionDto buildVersionDto(int major, int minor) {
		final VersionDto versionDto = new VersionDto();
		versionDto.setMajor(major);
		versionDto.setMinor(minor);
		return versionDto;
	}

}
