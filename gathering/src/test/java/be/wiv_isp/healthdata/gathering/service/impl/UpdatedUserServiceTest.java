/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.gathering.dao.IUpdatedUserDao;
import be.wiv_isp.healthdata.gathering.domain.UpdatedUser;
import be.wiv_isp.healthdata.gathering.service.IUpdatedUserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.PlatformTransactionManager;

import javax.transaction.Transactional;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UpdatedUserServiceTest {

	private final IUpdatedUserService updatedUserService = new UpdatedUserService();

	@Autowired
	private IUpdatedUserDao updatedUserDao;

	@Autowired
	private IOrganizationDao organizationDao;

	@Autowired
	@Qualifier("transactionManager")
	protected PlatformTransactionManager txManager;

	private Organization organization;

	@Before
	public void setup() {
		organization = organizationDao.create(new Organization());
		ReflectionTestUtils.setField(updatedUserService, "dao", updatedUserDao);
	}

	@Test
	public void testAdd() {
		UpdatedUser updatedUser;

		Assert.assertEquals(0, updatedUserDao.getAll().size());

		updatedUser = new UpdatedUser();
		updatedUser.setUserID(1L);
		updatedUser.setOrganization(organization);
		updatedUserService.add(updatedUser);

		Assert.assertEquals(1, updatedUserDao.getAll().size());

		updatedUser = new UpdatedUser();
		updatedUser.setUserID(1L);
		updatedUser.setOrganization(organization);
		updatedUserService.add(updatedUser);

		Assert.assertEquals(1, updatedUserDao.getAll().size());

		updatedUser = new UpdatedUser();
		updatedUser.setUserID(2L);
		updatedUser.setOrganization(organization);
		updatedUserService.add(updatedUser);

		Assert.assertEquals(2, updatedUserDao.getAll().size());
	}
}
