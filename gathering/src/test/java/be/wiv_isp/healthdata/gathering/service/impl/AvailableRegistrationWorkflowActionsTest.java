/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.common.dto.DataCollectionGroupDtoV7;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.PeriodExpression;
import be.wiv_isp.healthdata.orchestration.util.DateUtils;
import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.common.domain.enumeration.TimeUnit;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.RegistrationWorkflowHistory;
import be.wiv_isp.healthdata.orchestration.domain.WorkflowFlags;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionDefinitionForwardService;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.Set;

public class AvailableRegistrationWorkflowActionsTest {

	private final RegistrationWorkflowService workflowService = new RegistrationWorkflowService();

	private final IDataCollectionDefinitionForwardService dataCollectionDefinitionForwardService = EasyMock.createNiceMock(IDataCollectionDefinitionForwardService.class);

	@Before
	public void before() {
		ReflectionTestUtils.setField(workflowService, "catalogueService", dataCollectionDefinitionForwardService);
	}

	@Test
	public void testInitialAction() throws Exception {
		final Set<WorkflowActionType> availableActions = workflowService.getAvailableActions(null);
		Assert.assertEquals(1, availableActions.size());
		Assert.assertTrue(availableActions.contains(WorkflowActionType.SAVE));
	}

	@Test
	public void testWorkflowCanBeSaved() throws Exception {
		RegistrationWorkflow workflow = new RegistrationWorkflow();
		workflow.setStatus(WorkflowStatus.IN_PROGRESS);
		Assert.assertTrue(workflowService.isActionAvailable(workflow, WorkflowActionType.SAVE));

		workflow = new RegistrationWorkflow();
		workflow.setStatus(WorkflowStatus.ACTION_NEEDED);
		Assert.assertTrue(workflowService.isActionAvailable(workflow, WorkflowActionType.SAVE));
	}

	@Test
	public void testWorkflowCanBeSubmitted() throws Exception {
		final Organization organization = createOrganization();

		final DataCollectionDefinitionDtoV7 dcd = createDataCollectionDefinition();
		dcd.getDataCollectionGroup().setStartDate(oneWeekAgo());
		dcd.getDataCollectionGroup().setEndDateSubmission(inOneWeek());

		final RegistrationWorkflow workflow = new RegistrationWorkflow();
		workflow.setOrganization(organization);
		workflow.setDataCollectionDefinitionId(dcd.getId());

		EasyMock.expect(dataCollectionDefinitionForwardService.get(1L, organization)).andReturn(dcd).anyTimes();
		EasyMock.replay(dataCollectionDefinitionForwardService);

		workflow.setStatus(WorkflowStatus.IN_PROGRESS);
		Assert.assertTrue(workflowService.isActionAvailable(workflow, WorkflowActionType.SUBMIT));

		workflow.setStatus(WorkflowStatus.ACTION_NEEDED);
		Assert.assertTrue(workflowService.isActionAvailable(workflow, WorkflowActionType.SUBMIT));

		EasyMock.verify(dataCollectionDefinitionForwardService);
	}

	@Test
	public void testWorkflowCanNotBeSubmittedIfEndDateSubmissionIsOver() throws Exception {
		final Organization organization = createOrganization();

		final DataCollectionDefinitionDtoV7 dcd = createDataCollectionDefinition();
		dcd.getDataCollectionGroup().setStartDate(twoWeeksAgo());
		dcd.getDataCollectionGroup().setEndDateSubmission(oneWeekAgo());

		final RegistrationWorkflow workflow = new RegistrationWorkflow();
		workflow.setOrganization(organization);
		workflow.setDataCollectionDefinitionId(dcd.getId());

		EasyMock.expect(dataCollectionDefinitionForwardService.get(1L, organization)).andReturn(dcd).anyTimes();
		EasyMock.replay(dataCollectionDefinitionForwardService);

		workflow.setStatus(WorkflowStatus.IN_PROGRESS);
		Assert.assertFalse(workflowService.isActionAvailable(workflow, WorkflowActionType.SUBMIT));
	}

	@Test
	public void testWorkflowCanBeSubmittedIfEndDateSubmissionIsOverAndCorrectionsWereRequested() throws Exception {
		final Organization organization = createOrganization();

		final DataCollectionDefinitionDtoV7 dcd = createDataCollectionDefinition();
		dcd.getDataCollectionGroup().setStartDate(twoWeeksAgo());
		dcd.getDataCollectionGroup().setEndDateSubmission(oneWeekAgo());

		final RegistrationWorkflow workflow = new RegistrationWorkflow();
		workflow.setOrganization(organization);
		workflow.setDataCollectionDefinitionId(dcd.getId());
		workflow.setHistory(Collections.singletonList(createCorrectionsNeededHistory()));

		EasyMock.expect(dataCollectionDefinitionForwardService.get(1L, organization)).andReturn(dcd).anyTimes();
		EasyMock.replay(dataCollectionDefinitionForwardService);

		workflow.setStatus(WorkflowStatus.IN_PROGRESS);
		Assert.assertTrue(workflowService.isActionAvailable(workflow, WorkflowActionType.SUBMIT));
	}

	@Test
	public void testWorkflowCanBeDeleted() throws Exception {
		RegistrationWorkflow workflow = new RegistrationWorkflow();
		workflow.setStatus(WorkflowStatus.IN_PROGRESS);
		Assert.assertTrue(workflowService.isActionAvailable(workflow, WorkflowActionType.DELETE));

		workflow = new RegistrationWorkflow();
		workflow.setStatus(WorkflowStatus.ACTION_NEEDED);
		Assert.assertTrue(workflowService.isActionAvailable(workflow, WorkflowActionType.DELETE));
	}

	@Test
	public void testWorkflowCanBeRopened() throws Exception {
		RegistrationWorkflow workflow = new RegistrationWorkflow();
		workflow.setStatus(WorkflowStatus.SUBMITTED);
		Assert.assertTrue(workflowService.isActionAvailable(workflow, WorkflowActionType.REOPEN));
	}

	private Timestamp twoWeeksAgo() {
		return new Timestamp(DateUtils.remove(new Date(), new PeriodExpression(2, TimeUnit.WEEK)).getTime());
	}

	private Timestamp oneWeekAgo() {
		return new Timestamp(DateUtils.remove(new Date(), new PeriodExpression(1, TimeUnit.WEEK)).getTime());
	}

	private Timestamp inOneWeek() {
		return new Timestamp(DateUtils.add(new Date(), new PeriodExpression(1, TimeUnit.WEEK)).getTime());
	}

	private Organization createOrganization() {
		Organization organization = new Organization();
		organization.setId(1L);
		return organization;
	}

	private DataCollectionDefinitionDtoV7 createDataCollectionDefinition() {
		DataCollectionDefinitionDtoV7 dcd = new DataCollectionDefinitionDtoV7();
		dcd.setId(1L);
		dcd.setDataCollectionGroup(new DataCollectionGroupDtoV7());
		return dcd;
	}

	private RegistrationWorkflowHistory createCorrectionsNeededHistory() {
		final RegistrationWorkflowHistory history = new RegistrationWorkflowHistory();
		history.setNewStatus(WorkflowStatus.ACTION_NEEDED);
		final WorkflowFlags flags = new WorkflowFlags();
		flags.setCorrections(true);
		history.setFlags(flags);
		return history;
	}
}
