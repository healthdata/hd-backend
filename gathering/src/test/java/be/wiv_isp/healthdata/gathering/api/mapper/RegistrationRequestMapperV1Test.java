/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.api.mapper;

import be.wiv_isp.healthdata.gathering.domain.enumeration.ExecutionOption;
import be.wiv_isp.healthdata.gathering.dto.RegistrationRequestDtoV1;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class RegistrationRequestMapperV1Test {

	private final RegistrationRequestMapperV1 mapper = new RegistrationRequestMapperV1();

	@Test
	public void testConvert() throws Exception {

		InputStream inputAsStream = RegistrationRequestMapperV1Test.class.getResourceAsStream("requestXml.xml");
		String xmlInput = IOUtils.toString(inputAsStream);

		RegistrationRequestDtoV1 expected = new RegistrationRequestDtoV1();
		expected.addOption(ExecutionOption.STOP_ON_VALID_ERROR);
		expected.setDocument("<kmehr/>".getBytes(StandardCharsets.UTF_8));
		expected.setDataCollectionDefinitionId(1L);

		RegistrationRequestDtoV1 request = mapper.convert(xmlInput);

		Assert.assertEquals(expected, request);
	}

}
