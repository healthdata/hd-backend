/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.dao.impl;

import be.wiv_isp.healthdata.gathering.domain.ElasticSearchInfo;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.dao.IElasticSearchInfoDao;
import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.AbstractElasticSearchInfo.Status;
import be.wiv_isp.healthdata.orchestration.domain.ElasticSearchInfoCountDetails;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.search.ElasticSearchInfoSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = {"classpath:/applicationContext-test-dao.xml"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ElasticSearchInfoDaoTest {

	private static final String ERROR_MESSAGE = "error message";

	@Autowired
	private IElasticSearchInfoDao<ElasticSearchInfo> dao;

	@Autowired
	private RegistrationWorkflowDao workflowDao;

	@Autowired
	private IOrganizationDao organizationDao;

	@Test
	public void testGet() throws Exception {
		final RegistrationWorkflow workflow = workflowDao.create(buildWorkflow());
		final ElasticSearchInfo elasticSearchInfo = dao.create(buildElasticSearchInfo(workflow, Status.INDEXED, ERROR_MESSAGE));
		
		final ElasticSearchInfo returnedElasticSearchInfo = dao.get(elasticSearchInfo.getWorkflowId());
		Assert.assertNotNull(returnedElasticSearchInfo);
		Assert.assertEquals(workflow.getId(), returnedElasticSearchInfo.getWorkflowId());
		Assert.assertEquals(ERROR_MESSAGE, returnedElasticSearchInfo.getError());
		Assert.assertNotNull(returnedElasticSearchInfo.getUpdatedOn());
	}
	
	@Test
	public void testUpdatedOn() throws Exception {
		final Date creationDate = new Date();
		final RegistrationWorkflow workflow = workflowDao.create(buildWorkflow());
		ElasticSearchInfo elasticSearchInfo = dao.create(buildElasticSearchInfo(workflow, Status.INDEXED, ERROR_MESSAGE));
				
		Assert.assertTrue(elasticSearchInfo.getUpdatedOn().compareTo(creationDate) >= 0);
		Assert.assertTrue(elasticSearchInfo.getUpdatedOn().compareTo(new Date()) <= 0);
		
		final Date updateDate = new Date();
		elasticSearchInfo.setError("new error message");
		elasticSearchInfo = dao.update(elasticSearchInfo);

		Assert.assertTrue(elasticSearchInfo.getUpdatedOn().compareTo(updateDate) >= 0);
		Assert.assertTrue(elasticSearchInfo.getUpdatedOn().compareTo(new Date()) <= 0);
	}

	@Test
	public void testSearchByStatus() {
		final Organization organization = organizationDao.create(buildOrganization("11111111"));
		final ElasticSearchInfo info1 = dao.create(buildElasticSearchInfo(workflowDao.create(buildWorkflow(organization, "DC_1")), Status.INDEXED, null));
		final ElasticSearchInfo info2 = dao.create(buildElasticSearchInfo(workflowDao.create(buildWorkflow(organization, "DC_1")), Status.NOT_INDEXED, null));

		ElasticSearchInfoSearch search = new ElasticSearchInfoSearch();
		List<ElasticSearchInfo> infos = dao.getAll(search);
		Assert.assertEquals(2, infos.size());

		search = new ElasticSearchInfoSearch();
		search.setStatus(Status.INDEXED);
		infos = dao.getAll(search);
		Assert.assertEquals(1, infos.size());
		Assert.assertTrue(infos.contains(info1));
	}

	@Test
	public void testSearchByDataCollectionName() {
		final Organization organization = organizationDao.create(buildOrganization("11111111"));
		final ElasticSearchInfo info1 = dao.create(buildElasticSearchInfo(workflowDao.create(buildWorkflow(organization, "DC_1")), Status.INDEXED, null));
		final ElasticSearchInfo info2 = dao.create(buildElasticSearchInfo(workflowDao.create(buildWorkflow(organization, "DC_2")), Status.INDEXED, null));

		ElasticSearchInfoSearch search = new ElasticSearchInfoSearch();
		List<ElasticSearchInfo> infos = dao.getAll(search);
		Assert.assertEquals(2, infos.size());

		search = new ElasticSearchInfoSearch();
		search.setDataCollectionName("DC_1");
		infos = dao.getAll(search);
		Assert.assertEquals(1, infos.size());
		Assert.assertTrue(infos.contains(info1));
	}

	@Test
	public void testSearchByWorkflowId() {
		final Organization organization = organizationDao.create(buildOrganization("11111111"));

		final RegistrationWorkflow registration1 = workflowDao.create(buildWorkflow(organization, "DC_1"));
		final RegistrationWorkflow registration2 = workflowDao.create(buildWorkflow(organization, "DC_1"));

		final ElasticSearchInfo info1 = dao.create(buildElasticSearchInfo(registration1, Status.INDEXED, null));
		final ElasticSearchInfo info2 = dao.create(buildElasticSearchInfo(registration2, Status.INDEXED, null));

		ElasticSearchInfoSearch search = new ElasticSearchInfoSearch();
		List<ElasticSearchInfo> infos = dao.getAll(search);
		Assert.assertEquals(2, infos.size());

		search = new ElasticSearchInfoSearch();
		search.setWorkflowId(registration1.getId());
		infos = dao.getAll(search);
		Assert.assertEquals(1, infos.size());
		Assert.assertTrue(infos.contains(info1));
	}

	@Test
	public void testSearchByOrganization() {
		final Organization organization1 = organizationDao.create(buildOrganization("11111111"));
		final Organization organization2 = organizationDao.create(buildOrganization("22222222"));

		final RegistrationWorkflow registration1 = workflowDao.create(buildWorkflow(organization1, "DC_1"));
		final RegistrationWorkflow registration2 = workflowDao.create(buildWorkflow(organization2, "DC_1"));

		final ElasticSearchInfo info1 = dao.create(buildElasticSearchInfo(registration1, Status.INDEXED, null));
		final ElasticSearchInfo info2 = dao.create(buildElasticSearchInfo(registration2, Status.INDEXED, null));

		ElasticSearchInfoSearch search = new ElasticSearchInfoSearch();
		List<ElasticSearchInfo> infos = dao.getAll(search);
		Assert.assertEquals(2, infos.size());

		search = new ElasticSearchInfoSearch();
		search.setOrganizationId(organization1.getId());
		infos = dao.getAll(search);
		Assert.assertEquals(1, infos.size());
		Assert.assertTrue(infos.contains(info1));
	}

	@Test
	public void testUpdateAll() {
		final Organization organization = organizationDao.create(buildOrganization("11111111"));
		final RegistrationWorkflow registration1 = workflowDao.create(buildWorkflow(organization, "DC_1"));
		final RegistrationWorkflow registration2 = workflowDao.create(buildWorkflow(organization, "DC_2"));

		dao.create(buildElasticSearchInfo(registration1, Status.NOT_INDEXED, null));
		dao.create(buildElasticSearchInfo(registration2, Status.NOT_INDEXED, null));

		dao.updateAll(Status.INDEXED);
		final List<ElasticSearchInfo> all = dao.getAll();
		for (ElasticSearchInfo info : all) {
			dao.refresh(info);
		}
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(verifyCount(all, Status.INDEXED, 2));
	}

	@Test
	public void testUpdateAllByOrganization() {

		final Organization organization1 = organizationDao.create(buildOrganization("11111111"));
		final Organization organization2 = organizationDao.create(buildOrganization("22222222"));

		final RegistrationWorkflow registration1 = workflowDao.create(buildWorkflow(organization1, "DC_1"));
		final RegistrationWorkflow registration2 = workflowDao.create(buildWorkflow(organization2, "DC_1"));

		dao.create(buildElasticSearchInfo(registration1, Status.NOT_INDEXED, null));
		dao.create(buildElasticSearchInfo(registration2, Status.NOT_INDEXED, null));

		final ElasticSearchInfoSearch search = new ElasticSearchInfoSearch();
		search.setOrganizationId(organization1.getId());
		dao.updateAll(search, Status.INDEXED);

		final List<ElasticSearchInfo> all = dao.getAll();
		for (ElasticSearchInfo info : all) {
			dao.refresh(info);
		}
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(verifyCount(all, Status.INDEXED, 1));
		Assert.assertTrue(verifyCount(all, Status.NOT_INDEXED, 1));
		for (ElasticSearchInfo info : all) {
			if(info.getWorkflow().getOrganization().equals(organization1)) {
				Assert.assertEquals(Status.INDEXED, info.getStatus());
			} else if(info.getWorkflow().getOrganization().equals(organization2)) {
				Assert.assertEquals(Status.NOT_INDEXED, info.getStatus());
			}
		}
	}

	@Test
	public void testUpdateAllByOrganizationAndDataCollection() {

		final Organization organization1 = organizationDao.create(buildOrganization("11111111"));
		final Organization organization2 = organizationDao.create(buildOrganization("22222222"));

		final RegistrationWorkflow registration1 = workflowDao.create(buildWorkflow(organization1, "DC_1"));
		final RegistrationWorkflow registration2 = workflowDao.create(buildWorkflow(organization1, "DC_2"));
		final RegistrationWorkflow registration3 = workflowDao.create(buildWorkflow(organization2, "DC_1"));
		final RegistrationWorkflow registration4 = workflowDao.create(buildWorkflow(organization2, "DC_2"));

		dao.create(buildElasticSearchInfo(registration1, Status.NOT_INDEXED, null));
		dao.create(buildElasticSearchInfo(registration2, Status.NOT_INDEXED, null));
		dao.create(buildElasticSearchInfo(registration3, Status.NOT_INDEXED, null));
		dao.create(buildElasticSearchInfo(registration4, Status.NOT_INDEXED, null));

		final ElasticSearchInfoSearch search = new ElasticSearchInfoSearch();
		search.setOrganizationId(organization1.getId());
		search.setDataCollectionName("DC_1");
		dao.updateAll(search, Status.INDEXED);

		final List<ElasticSearchInfo> all = dao.getAll();
		for (ElasticSearchInfo info : all) {
			dao.refresh(info);
		}
		Assert.assertEquals(4, all.size());
		Assert.assertTrue(verifyCount(all, Status.INDEXED, 1));
		Assert.assertTrue(verifyCount(all, Status.NOT_INDEXED, 3));
		for (ElasticSearchInfo info : all) {
			if(info.getWorkflow().getOrganization().equals(organization1) && "DC_1".equals(info.getWorkflow().getDataCollectionName())) {
				Assert.assertEquals(Status.INDEXED, info.getStatus());
			} else if(info.getWorkflow().getOrganization().equals(organization2)) {
				Assert.assertEquals(Status.NOT_INDEXED, info.getStatus());
			}
		}
	}

	@Test
	public void testUpdateAllByStatus() {

		final Organization organization = organizationDao.create(buildOrganization("11111111"));

		final RegistrationWorkflow registration1 = workflowDao.create(buildWorkflow(organization, "DC_1"));
		final RegistrationWorkflow registration2 = workflowDao.create(buildWorkflow(organization, "DC_1"));

		dao.create(buildElasticSearchInfo(registration1, Status.NOT_INDEXED, null));
		dao.create(buildElasticSearchInfo(registration2, Status.INDEXING_FAILED, null));

		final ElasticSearchInfoSearch search = new ElasticSearchInfoSearch();
		search.setStatus(Status.NOT_INDEXED);
		dao.updateAll(search, Status.INDEXED);

		final List<ElasticSearchInfo> all = dao.getAll();
		for (ElasticSearchInfo info : all) {
			dao.refresh(info);
		}
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(verifyCount(all, Status.INDEXED, 1));
		Assert.assertTrue(verifyCount(all, Status.INDEXING_FAILED, 1));
		for (ElasticSearchInfo info : all) {
			if(info.getWorkflow().getId().equals(registration1.getId())) {
				Assert.assertEquals(Status.INDEXED, info.getStatus());
			} else if(info.getWorkflow().getId().equals(registration2.getId())) {
				Assert.assertEquals(Status.INDEXING_FAILED, info.getStatus());
			}
		}
	}

	@Test
	public void testUpdateAllByWorkflowId() {

		final Organization organization = organizationDao.create(buildOrganization("11111111"));

		final RegistrationWorkflow registration1 = workflowDao.create(buildWorkflow(organization, "DC_1"));
		final RegistrationWorkflow registration2 = workflowDao.create(buildWorkflow(organization, "DC_1"));

		dao.create(buildElasticSearchInfo(registration1, Status.NOT_INDEXED, null));
		dao.create(buildElasticSearchInfo(registration2, Status.NOT_INDEXED, null));

		final ElasticSearchInfoSearch search = new ElasticSearchInfoSearch();
		search.setWorkflowId(registration1.getId());
		dao.updateAll(search, Status.INDEXED);

		final List<ElasticSearchInfo> all = dao.getAll();
		for (ElasticSearchInfo info : all) {
			dao.refresh(info);
		}
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(verifyCount(all, Status.INDEXED, 1));
		Assert.assertTrue(verifyCount(all, Status.NOT_INDEXED, 1));
		for (ElasticSearchInfo info : all) {
			if(info.getWorkflow().getId().equals(registration1.getId())) {
				Assert.assertEquals(Status.INDEXED, info.getStatus());
			} else if(info.getWorkflow().getId().equals(registration2.getId())) {
				Assert.assertEquals(Status.NOT_INDEXED, info.getStatus());
			}
		}
	}

	@Test
	public void testCount() {
		final String DCD = "DCD_";
		final long nbIndexed = 20L;
		final long nbNotIndexed = 4L;
		final long nbIndexingFailed = 5L;
		final int nbOrganizations = 2;
		final Long nbDCDs = 3L;

		Organization[] organizations = 	new Organization[nbOrganizations];
		for (int i=0 ; i<nbOrganizations; i++) {
			final Organization organization = buildOrganization("11111111");
			organizationDao.create(organization);
			organizations[i] = organization;
		}

		long org0CountIndexed = 0L;
		long org0dcd0CountIndexed = 0L;

		for(int i=1; i<=nbIndexed; ++i) {
			final RegistrationWorkflow workflow = buildWorkflow();
			workflow.setOrganization(organizations[i % nbOrganizations]);
			Long dcdId = (i*997) % nbDCDs + 1;
			workflow.setDataCollectionDefinitionId(dcdId);
			workflow.setDataCollectionName(DCD + dcdId);
			if ((i % nbOrganizations) == 0) {
				org0CountIndexed++;
				if (((i * 997) % nbDCDs) + 1 == 1)
					org0dcd0CountIndexed++;
			}
			workflowDao.create(workflow);
			final ElasticSearchInfo info = buildElasticSearchInfo(workflow, Status.INDEXED, null);
			dao.create(info);
		}

		for(int i=1; i<=nbNotIndexed; ++i) {
			final RegistrationWorkflow workflow = buildWorkflow();
			workflow.setOrganization(organizations[i % nbOrganizations]);
			Long dcdId = (i*97) % nbDCDs + 1;
			workflow.setDataCollectionDefinitionId(dcdId);
			workflow.setDataCollectionName(DCD + dcdId);
			workflowDao.create(workflow);
			final ElasticSearchInfo info = buildElasticSearchInfo(workflow, Status.NOT_INDEXED, null);
			dao.create(info);
		}

		for(int i=1; i<=nbIndexingFailed; ++i) {
			final RegistrationWorkflow workflow = buildWorkflow();
			workflow.setOrganization(organizations[i % nbOrganizations]);
			Long dcdId = (i*97) % nbDCDs + 1;
			workflow.setDataCollectionDefinitionId(dcdId);
			workflow.setDataCollectionName(DCD + dcdId);
			workflowDao.create(workflow);
			final ElasticSearchInfo info = buildElasticSearchInfo(workflow, Status.INDEXING_FAILED, "error message " + i);
			dao.create(info);
		}

		Assert.assertEquals(nbIndexed, dao.count(buildSearch(Status.INDEXED)));
		Assert.assertEquals(nbNotIndexed, dao.count(buildSearch(Status.NOT_INDEXED)));
		Assert.assertEquals(nbIndexingFailed, dao.count(buildSearch(Status.INDEXING_FAILED)));

		ElasticSearchInfoSearch search = new ElasticSearchInfoSearch();
		search.setStatus(Status.INDEXED);
		search.setOrganizationId(organizations[0].getId());
		Assert.assertEquals(org0CountIndexed, dao.count(search));

		search = new ElasticSearchInfoSearch();
		search.setStatus(Status.INDEXED);
		search.setOrganizationId(organizations[0].getId());
		search.setDataCollectionName("DCD_" + 1L);
		Assert.assertEquals(org0dcd0CountIndexed, dao.count(search));

	}

	@Test
	public void testTooLongErrorMessageIsTruncated() {
		final String longError = new String(new char[4001]).replace("\0", "a"); // 4001 repetitions of the 'a' character
		final String expectedError = new String(new char[(int) (4000*0.9-3)]).replace("\0", "a") + "..."; // 3997 repetitions of the 'a' character, followed by '...'
		System.out.println(longError);

		final ElasticSearchInfo elasticSearchInfo = new ElasticSearchInfo();

		elasticSearchInfo.setError(longError);

		final String error = elasticSearchInfo.getError();
		Assert.assertEquals(expectedError, error);
	}

	@Test
	public void testCountDetailed() {
		final Organization organization1 = organizationDao.create(buildOrganization("11111111"));
		final Organization organization2 = organizationDao.create(buildOrganization("22222222"));

		final RegistrationWorkflow registration1 = workflowDao.create(buildWorkflow(organization1, "DC_1"));
		final RegistrationWorkflow registration2 = workflowDao.create(buildWorkflow(organization1, "DC_2"));
		final RegistrationWorkflow registration3 = workflowDao.create(buildWorkflow(organization2, "DC_1"));
		final RegistrationWorkflow registration4 = workflowDao.create(buildWorkflow(organization2, "DC_2"));
		final RegistrationWorkflow registration5 = workflowDao.create(buildWorkflow(organization1, "DC_1"));
		final RegistrationWorkflow registration6 = workflowDao.create(buildWorkflow(organization1, "DC_2"));
		final RegistrationWorkflow registration7 = workflowDao.create(buildWorkflow(organization2, "DC_1"));
		final RegistrationWorkflow registration8 = workflowDao.create(buildWorkflow(organization2, "DC_2"));
		final RegistrationWorkflow registration9 = workflowDao.create(buildWorkflow(organization2, "DC_2"));

		dao.create(buildElasticSearchInfo(registration1, Status.INDEXED, null));
		dao.create(buildElasticSearchInfo(registration2, Status.INDEXED, null));
		dao.create(buildElasticSearchInfo(registration3, Status.INDEXED, null));
		dao.create(buildElasticSearchInfo(registration4, Status.INDEXED, null));
		dao.create(buildElasticSearchInfo(registration5, Status.NOT_INDEXED, null));
		dao.create(buildElasticSearchInfo(registration6, Status.NOT_INDEXED, null));
		dao.create(buildElasticSearchInfo(registration7, Status.NOT_INDEXED, null));
		dao.create(buildElasticSearchInfo(registration8, Status.NOT_INDEXED, null));
		dao.create(buildElasticSearchInfo(registration9, Status.NOT_INDEXED, null));

		final List<ElasticSearchInfoCountDetails> details = dao.countDetailed();
		Assert.assertEquals(8, details.size());
		Assert.assertTrue(details.contains(new ElasticSearchInfoCountDetails(organization1.getId(), "DC_1", Status.INDEXED, 1L)));
		Assert.assertTrue(details.contains(new ElasticSearchInfoCountDetails(organization1.getId(), "DC_2", Status.INDEXED, 1L)));
		Assert.assertTrue(details.contains(new ElasticSearchInfoCountDetails(organization2.getId(), "DC_1", Status.INDEXED, 1L)));
		Assert.assertTrue(details.contains(new ElasticSearchInfoCountDetails(organization2.getId(), "DC_2", Status.INDEXED, 1L)));
		Assert.assertTrue(details.contains(new ElasticSearchInfoCountDetails(organization1.getId(), "DC_1", Status.NOT_INDEXED, 1L)));
		Assert.assertTrue(details.contains(new ElasticSearchInfoCountDetails(organization1.getId(), "DC_2", Status.NOT_INDEXED, 1L)));
		Assert.assertTrue(details.contains(new ElasticSearchInfoCountDetails(organization2.getId(), "DC_1", Status.NOT_INDEXED, 1L)));
		Assert.assertTrue(details.contains(new ElasticSearchInfoCountDetails(organization2.getId(), "DC_2", Status.NOT_INDEXED, 2L)));
	}

	@Test
	public void testGetWorkflowIds() {
		final Organization organization1 = organizationDao.create(buildOrganization("11111111"));

		final RegistrationWorkflow registration1 = workflowDao.create(buildWorkflow(organization1, "DC_1"));
		final RegistrationWorkflow registration2 = workflowDao.create(buildWorkflow(organization1, "DC_1"));
		final RegistrationWorkflow registration3 = workflowDao.create(buildWorkflow(organization1, "DC_1"));

		dao.create(buildElasticSearchInfo(registration1, Status.INDEXED, null));
		dao.create(buildElasticSearchInfo(registration2, Status.INDEXED, null));
		dao.create(buildElasticSearchInfo(registration3, Status.NOT_INDEXED, null));

		final List<Long> ids = dao.getWorkflowIds(Status.INDEXED);
		Assert.assertEquals(2, ids.size());
		Assert.assertTrue(ids.contains(registration1.getId()));
		Assert.assertTrue(ids.contains(registration2.getId()));
	}

	private ElasticSearchInfo buildElasticSearchInfo(RegistrationWorkflow workflow, Status status, String error) {
		final ElasticSearchInfo info = new ElasticSearchInfo();
		info.setStatus(status);
		info.setError(error);
		info.setWorkflow(workflow);
		return info;
	}

	private RegistrationWorkflow buildWorkflow() {
		return buildWorkflow(null, null);
	}

	private RegistrationWorkflow buildWorkflow(Organization organization, String dataCollectionName) {
		final RegistrationWorkflow workflow = new RegistrationWorkflow();
		workflow.setOrganization(organization);
		workflow.setDataCollectionName(dataCollectionName);
		return workflow;
	}

	private Organization buildOrganization(String identificationValue) {
		final Organization organization = new Organization();
		organization.setHealthDataIDValue(identificationValue);
		return organization;
	}

	private ElasticSearchInfoSearch buildSearch(Status status) {
		final ElasticSearchInfoSearch search = new ElasticSearchInfoSearch();
		search.setStatus(status);
		return search;
	}

	private boolean verifyCount(List<ElasticSearchInfo> infos, Status status, int expectedCount) {
		int count = 0;
		for (ElasticSearchInfo info : infos) {
			if(status.equals(info.getStatus())) {
				count++;
			}
		}
		return expectedCount == count;
	}
}
