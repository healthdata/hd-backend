/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.gathering.domain.ElasticSearchInfo;
import be.wiv_isp.healthdata.gathering.domain.RegistrationDocument;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.gathering.dto.RegistrationWorkflowDto;
import be.wiv_isp.healthdata.orchestration.api.mapper.IAbstractWorkflowMapper;
import be.wiv_isp.healthdata.orchestration.domain.AbstractElasticSearchInfo.Status;
import be.wiv_isp.healthdata.orchestration.domain.ElasticSearchInfoCountDetails;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.domain.search.ElasticSearchInfoSearch;
import be.wiv_isp.healthdata.orchestration.elasticsearch.client.IElasticSearchClient;
import be.wiv_isp.healthdata.orchestration.elasticsearch.client.impl.ElasticSearchBulkResponse;
import be.wiv_isp.healthdata.orchestration.elasticsearch.client.impl.IndexingRequest;
import be.wiv_isp.healthdata.orchestration.service.*;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractElasticSearchService;
import be.wiv_isp.healthdata.orchestration.tasks.ElasticSearchTask;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class ElasticSearchServiceTest {

	final IElasticSearchService elasticSearchService = new ElasticSearchService();

	final IAbstractRegistrationWorkflowService workflowService = EasyMock.createNiceMock(IAbstractRegistrationWorkflowService.class);
	final IAbstractWorkflowMapper workflowMapper = EasyMock.createNiceMock(IAbstractWorkflowMapper.class);
	final IMappingService mappingService = EasyMock.createNiceMock(IMappingService.class);
	final IAbstractRegistrationDocumentService documentService = EasyMock.createNiceMock(IAbstractRegistrationDocumentService.class);
	final INoteService noteService = EasyMock.createNiceMock(INoteService.class);
	final IElasticSearchClient elasticSearchClient = EasyMock.createNiceMock(IElasticSearchClient.class);
	final IElasticSearchInfoService elasticSearchInfoService = EasyMock.createNiceMock(IElasticSearchInfoService.class);
	final TaskScheduler elasticSearchIndexingTaskScheduler = EasyMock.createNiceMock(TaskScheduler.class);
	final IOrganizationService organizationService = EasyMock.createNiceMock(IOrganizationService.class);
	final ElasticSearchTask elasticSearchIndexingTask = EasyMock.createNiceMock(ElasticSearchTask.class);

	@Before
	public void before() {
		ReflectionTestUtils.setField(elasticSearchService, "elasticSearchClient", elasticSearchClient);
		ReflectionTestUtils.setField(elasticSearchService, "workflowService", workflowService);
		ReflectionTestUtils.setField(elasticSearchService, "workflowMapper", workflowMapper);
		ReflectionTestUtils.setField(elasticSearchService, "mappingService", mappingService);
		ReflectionTestUtils.setField(elasticSearchService, "elasticSearchInfoService", elasticSearchInfoService);
		ReflectionTestUtils.setField(elasticSearchService, "documentService", documentService);
		ReflectionTestUtils.setField(elasticSearchService, "noteService", noteService);
		ReflectionTestUtils.setField(elasticSearchService, "elasticSearchIndexingTaskScheduler", elasticSearchIndexingTaskScheduler);
		ReflectionTestUtils.setField(elasticSearchService, "organizationService", organizationService);
		ReflectionTestUtils.setField(elasticSearchService, "elasticSearchIndexingTask", elasticSearchIndexingTask);
	}

	@Test
	public void testRebuild() {
		elasticSearchClient.deleteAllIndices();
		EasyMock.expectLastCall().once();

		EasyMock.expect(elasticSearchIndexingTaskScheduler.schedule(EasyMock.eq(elasticSearchIndexingTask), (Date) EasyMock.anyObject())).andReturn(null);

		elasticSearchInfoService.updateAll(Status.NOT_INDEXED);
		EasyMock.expectLastCall();

		EasyMock.replay(elasticSearchClient, elasticSearchInfoService, elasticSearchIndexingTaskScheduler);

		elasticSearchService.rebuild();

		EasyMock.verify(elasticSearchClient, elasticSearchInfoService, elasticSearchIndexingTaskScheduler);
	}

	@Test
	public void testIndexWorkflowWithDocumentNullDoesNothing() {
		final Long workflowId = 1000L;

		RegistrationWorkflow workflow = new RegistrationWorkflow();
		workflow.setId(workflowId);
		EasyMock.expect(workflowService.get(workflowId)).andReturn(workflow);

		final ElasticSearchInfo elasticSearchInfo = new ElasticSearchInfo();
		elasticSearchInfo.setStatus(Status.INDEXED);
		elasticSearchInfo.setWorkflowId(workflowId);

		EasyMock.expect(elasticSearchInfoService.get(workflowId)).andReturn(elasticSearchInfo);
		EasyMock.expect(elasticSearchInfoService.update(elasticSearchInfo)).andReturn(elasticSearchInfo);

		EasyMock.replay(workflowService, elasticSearchInfoService);
		elasticSearchService.index(workflowId);
		EasyMock.verify(workflowService, elasticSearchInfoService);
	}

	@Test
	public void testIndexWorkflowIndexAndTypeExist() throws JSONException {
		runIndexTemplate(true, true);
	}

	@Test
	public void testIndexWorkflowIndexExistsTypeDoesNotExist() throws JSONException {
		runIndexTemplate(true, false);
	}

	@Test
	public void testIndexWorkflowIndexAndTypeDoNotExist() throws JSONException {
		runIndexTemplate(false, false);
	}

	private void runIndexTemplate(boolean indexExists, boolean typeExists) throws JSONException {
		final String dataCollectionName = "TEST";
		final Long workflowId = 3000L;
		final String workflowIdString = "3000";
		final Long dataCollectionDefinitionId = 2000L;
		final String dataCollectionDefinitionIdString = "2000";

		Organization organization = new Organization();
		organization.setId(1L);

		RegistrationDocument document = new RegistrationDocument();
		document.setDocumentContent("content".getBytes());

		final Map<String, String> metadata = new HashMap<String, String>();
		metadata.put("key", "value");

		RegistrationWorkflow workflow = new RegistrationWorkflow();
		workflow.setId(workflowId);
		workflow.setDocument(document);
		workflow.setDataCollectionName(dataCollectionName);
		workflow.setDataCollectionDefinitionId(dataCollectionDefinitionId);
		workflow.setOrganization(organization);
		workflow.setMetaData(metadata);

		final RegistrationWorkflowDto workflowDto = new RegistrationWorkflowDto(workflow);

		EasyMock.expect(elasticSearchClient.indexExists("1_" + dataCollectionName.toLowerCase())).andReturn(indexExists);
		EasyMock.expect(elasticSearchClient.typeExists("1_" + dataCollectionName.toLowerCase(), dataCollectionDefinitionIdString)).andReturn(typeExists);
		
		final IndexingRequest indexingRequest = new IndexingRequest();
		indexingRequest.setIndex("1_" + dataCollectionName.toLowerCase());
		indexingRequest.setType(dataCollectionDefinitionIdString);
		indexingRequest.setId(workflowIdString);
		indexingRequest.setContent("some content");

		EasyMock.expect(elasticSearchClient.index(indexingRequest)).andReturn(ElasticSearchBulkResponse.ok());

		EasyMock.expect(workflowService.get(workflowId)).andReturn(workflow);
		EasyMock.expect(workflowMapper.convert(workflow)).andReturn(workflowDto);

		EasyMock.expect(mappingService.mapToElasticSearchDocument(workflow)).andReturn(new JSONObject());

		final ElasticSearchInfo elasticSearchInfo = new ElasticSearchInfo();
		elasticSearchInfo.setStatus(Status.INDEXED);
		elasticSearchInfo.setWorkflowId(workflowId);

		EasyMock.expect(elasticSearchInfoService.get(workflowId)).andReturn(elasticSearchInfo);
		EasyMock.expect(elasticSearchInfoService.update(elasticSearchInfo)).andReturn(elasticSearchInfo);

		if (!indexExists) {
			final String settings = "settings";
			EasyMock.expect(mappingService.getElasticSearchIndexSettings()).andReturn(settings);
			elasticSearchClient.createIndex("1_" + dataCollectionName.toLowerCase(), settings);
			EasyMock.expectLastCall();
		}

		if (!typeExists) {
			final JSONObject mappingResponse2 = new JSONObject("{\"mapping\" : \"value\"}");

			EasyMock.expect(mappingService.getElasticSearchMapping(dataCollectionDefinitionId)).andReturn(mappingResponse2);

			final JSONObject result = new JSONObject();
			result.put(dataCollectionDefinitionIdString, new JSONObject("{\"mapping\" : \"value\"}"));

			elasticSearchClient.putMapping("1_" + dataCollectionName.toLowerCase(), dataCollectionDefinitionIdString, result.toString());
		}

		EasyMock.expect(documentService.getEntityInstance()).andReturn(new RegistrationDocument());

		EasyMock.expect(noteService.getByDocumentId(EasyMock.eq(WorkflowType.REGISTRATION), (Long)EasyMock.anyObject())).andReturn(null);

		EasyMock.replay(workflowMapper, workflowService, elasticSearchClient, mappingService, elasticSearchInfoService, documentService, noteService);
		elasticSearchService.index(workflowId);
		EasyMock.verify(workflowMapper, workflowService, elasticSearchClient, mappingService, elasticSearchInfoService, documentService, noteService);
	}

	@Test
	public void testSearch() {
		final String[] indices = new String[] { "test" };
		final String[] types = new String[] { "1234" };
		final String searchQuery = "query";
		final String searchResult = "{\"result\" : \"value\"}";

		EasyMock.expect(elasticSearchClient.search(EasyMock.aryEq(indices), EasyMock.aryEq(types), EasyMock.eq(searchQuery))).andReturn(searchResult);

		EasyMock.replay(elasticSearchClient);
		Assert.assertEquals(searchResult, elasticSearchService.search(indices, types, searchQuery));
		EasyMock.verify(elasticSearchClient);
	}

	@Test
	public void test() throws IOException {
		final Organization organization1 = buildOrganization(1L, "RIZIV", "11111111");
		final Organization organization2 = buildOrganization(2L, "RIZIV", "22222222");

		final List<Organization> organizations = new ArrayList<>();
		organizations.add(organization1);
		organizations.add(organization2);

		final List<ElasticSearchInfoCountDetails> countDetails = new ArrayList();
		countDetails.add(new ElasticSearchInfoCountDetails(1L, "TEST_1", Status.INDEXED, 20L));
		countDetails.add(new ElasticSearchInfoCountDetails(1L, "TEST_2", Status.INDEXED, 20L));
		countDetails.add(new ElasticSearchInfoCountDetails(2L, "TEST_1", Status.INDEXED, 30L));
		countDetails.add(new ElasticSearchInfoCountDetails(2L, "TEST_2", Status.INDEXED, 30L));
		countDetails.add(new ElasticSearchInfoCountDetails(1L, "TEST_1", Status.INDEXING_FAILED, 0L));
		countDetails.add(new ElasticSearchInfoCountDetails(1L, "TEST_2", Status.INDEXING_FAILED, 0L));
		countDetails.add(new ElasticSearchInfoCountDetails(2L, "TEST_1", Status.INDEXING_FAILED, 0L));
		countDetails.add(new ElasticSearchInfoCountDetails(2L, "TEST_2", Status.INDEXING_FAILED, 0L));
		countDetails.add(new ElasticSearchInfoCountDetails(1L, "TEST_1", Status.NOT_INDEXED, 0L));
		countDetails.add(new ElasticSearchInfoCountDetails(1L, "TEST_2", Status.NOT_INDEXED, 0L));
		countDetails.add(new ElasticSearchInfoCountDetails(2L, "TEST_1", Status.NOT_INDEXED, 0L));
		countDetails.add(new ElasticSearchInfoCountDetails(2L, "TEST_2", Status.NOT_INDEXED, 0L));

		final List<ElasticSearchInfo> indexingFailed = new ArrayList<>();
		//indexingFailed.add(buildElasticSearchInfo(1L, Status.INDEXING_FAILED, "error-message-1", 0));
		//indexingFailed.add(buildElasticSearchInfo(2L, Status.INDEXING_FAILED, "error-message-2", 0));

		EasyMock.expect(elasticSearchClient.status()).andReturn(true);
		EasyMock.expect(elasticSearchClient.getDocumentCount()).andReturn(100L);

		EasyMock.expect(elasticSearchInfoService.count(buildSearch(Status.INDEXED))).andReturn(100L);
		EasyMock.expect(elasticSearchInfoService.count(buildSearch(Status.NOT_INDEXED))).andReturn(0L);
		EasyMock.expect(elasticSearchInfoService.count(buildSearch(Status.INDEXING_FAILED))).andReturn(0L);
		EasyMock.expect(elasticSearchInfoService.getAll(buildSearch(Status.INDEXING_FAILED))).andReturn(indexingFailed);
		EasyMock.expect(elasticSearchInfoService.countDetailed()).andReturn(countDetails);

		EasyMock.expect(organizationService.getAll()).andReturn(organizations).anyTimes();
		EasyMock.expect(organizationService.get(1L)).andReturn(organization1).anyTimes();
		EasyMock.expect(organizationService.get(2L)).andReturn(organization2).anyTimes();

		EasyMock.expect(elasticSearchIndexingTask.isRunning()).andReturn(false);
		EasyMock.expect(elasticSearchIndexingTask.getStartTime()).andReturn(null);
		//EasyMock.expect(elasticSearchIndexingTask.getEndTime()).andReturn(new Date());

		EasyMock.expect(workflowService.count(buildRegistrationSearch(organization1, null))).andReturn(40L);
		EasyMock.expect(workflowService.count(buildRegistrationSearch(organization2, null))).andReturn(60L);
		EasyMock.expect(workflowService.getDataCollectionNames(1L)).andReturn(Arrays.asList("TEST_1", "TEST_2"));
		EasyMock.expect(workflowService.getDataCollectionNames(2L)).andReturn(Arrays.asList("TEST_1", "TEST_2"));
		EasyMock.expect(workflowService.count(buildRegistrationSearch(organization1, "TEST_1"))).andReturn(20L);
		EasyMock.expect(workflowService.count(buildRegistrationSearch(organization1, "TEST_2"))).andReturn(20L);
		EasyMock.expect(workflowService.count(buildRegistrationSearch(organization2, "TEST_1"))).andReturn(30L);
		EasyMock.expect(workflowService.count(buildRegistrationSearch(organization2, "TEST_2"))).andReturn(30L);

		EasyMock.expect(documentService.count()).andReturn(100L);

		EasyMock.replay(elasticSearchClient, documentService, elasticSearchInfoService, organizationService, elasticSearchIndexingTask, workflowService);
		final AbstractElasticSearchService.AdvancedStatus advancedStatus = elasticSearchService.advancedStatus();
		EasyMock.verify(elasticSearchClient, documentService, elasticSearchInfoService, organizationService, elasticSearchIndexingTask, workflowService);

		// verify output
		final String json = new ObjectMapper().writeValueAsString(advancedStatus);
		final String expectedOutput = IOUtils.toString(ElasticSearchServiceTest.class.getClassLoader().getResourceAsStream("es-advanced-status.json"), StandardCharsets.UTF_8);

		final Map map1 = new ObjectMapper().readValue(json, Map.class);
		final Map map2 = new ObjectMapper().readValue(expectedOutput, Map.class);
		Assert.assertEquals(map1, map2);
	}

	private ElasticSearchInfo buildElasticSearchInfo(Long workflowId, Status status, String error, int retryCount) {
		final ElasticSearchInfo info = new ElasticSearchInfo();
		info.setWorkflowId(workflowId);
		info.setStatus(status);
		info.setError(error);
		info.setRetryCount(retryCount);
		return info;
	}

	private Organization buildOrganization(Long id, String type, String value) {
		final Organization organization = new Organization();
		organization.setId(id);
		organization.setHealthDataIDType(type);
		organization.setHealthDataIDValue(value);
		return organization;
	}

	private ElasticSearchInfoSearch buildSearch(Status status) {
		final ElasticSearchInfoSearch search = new ElasticSearchInfoSearch();
		search.setStatus(status);
		return search;
	}

	private RegistrationWorkflowSearch buildRegistrationSearch(Organization organization, String dataCollectionName) {
		final RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		search.setOrganization(organization);
		search.setDataCollectionName(dataCollectionName);
		return search;
	}
}
