/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl.mock;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.gathering.dto.ElasticSearchWorkflowDto;
import be.wiv_isp.healthdata.gathering.dto.RegistrationWorkflowDto;
import be.wiv_isp.healthdata.orchestration.dto.NoteDto;
import be.wiv_isp.healthdata.orchestration.service.IElasticSearchService;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractElasticSearchService.AdvancedStatus;

import java.util.List;

public class ElasticSearchServiceMock implements IElasticSearchService<ElasticSearchWorkflowDto, RegistrationWorkflowDto> {

	@Override
	public ElasticSearchWorkflowDto getDtoInstance(RegistrationWorkflowDto dto, List<NoteDto> notes) {
		return new ElasticSearchWorkflowDto(dto, notes);
	}

	@Override
	public void index(Long workflowId) {
	}

	@Override
	public void index(List<Long> workflowIds) {
	}

	@Override
	public String search(String index, String type, String searchQuery) {
		return null;
	}

	@Override
	public String search(String[] indices, String[] types, String searchQuery) {
		return null;
	}

	@Override
	public void rebuild() {

	}

	@Override
	public void rebuild(Long org) { }

	@Override
	public void rebuild(Long org, String dcd) { }

	@Override
	public void repair() { }

	@Override
	public void repair(Long org) { }

	@Override
	public void repair(Long org, String dcd) { }

	@Override
	public boolean status() {
		return false;
	}

	@Override
	public boolean status(Long organizationId) {
		return false;
	}

	@Override
	public boolean status(Long organizationId, String dcd) {
		return false;
	}

	@Override
	public AdvancedStatus advancedStatus() {
		return null;
	}

	@Override
	public String buildIndexName(Organization organization, String dataCollectionName) {
		return null;
	}

	@Override
	public void delete(Long id) {

	}

}
