/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.workflow.action;

import be.wiv_isp.healthdata.common.domain.Baseline;
import be.wiv_isp.healthdata.common.domain.BaselineType;
import be.wiv_isp.healthdata.common.domain.RegistryDependentIdGenerationDefinition;
import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupDtoV7;
import be.wiv_isp.healthdata.common.dto.FollowUpDefinitionDto;
import be.wiv_isp.healthdata.common.domain.enumeration.TimeUnit;
import be.wiv_isp.healthdata.gathering.action.workflow.SaveRegistrationAction;
import be.wiv_isp.healthdata.gathering.centralplatform.service.ICentralPlatformService;
import be.wiv_isp.healthdata.gathering.dao.IRegistrationWorkflowDao;
import be.wiv_isp.healthdata.gathering.domain.MappingContext;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.factory.IMappingContextFactory;
import be.wiv_isp.healthdata.gathering.service.IFollowUpService;
import be.wiv_isp.healthdata.gathering.service.IRegistrationWorkflowService;
import be.wiv_isp.healthdata.gathering.service.impl.*;
import be.wiv_isp.healthdata.orchestration.dao.*;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import be.wiv_isp.healthdata.orchestration.dto.VersionDetailsDto;
import be.wiv_isp.healthdata.orchestration.service.*;
import be.wiv_isp.healthdata.orchestration.util.DateUtils;
import org.easymock.EasyMock;
import org.easymock.LogicalOperator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = {"classpath:/applicationContext-test-dao.xml"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SaveRegistrationActionTest {

    private static final Logger LOG = LoggerFactory.getLogger(SaveRegistrationActionTest.class);

    @Autowired
    private IRegistrationWorkflowDao workflowDao;

    @Autowired
    private IFollowUpDao followUpDao;

    @Autowired
    private IElasticSearchInfoDao elasticSearchInfoDao;

    @Autowired
    private IRegistrationDocumentDao documentDao;

    @Autowired
    private IAttachmentDao attachmentDao;

    @Autowired
    private IAttachmentContentDao attachmentContentDao;

    @Autowired
    private IOrganizationDao organizationDao;

    @Autowired
    @Qualifier("transactionManager")
    protected PlatformTransactionManager txManager;

    private IRegistrationWorkflowService workflowService = new RegistrationWorkflowService();
    private IFollowUpService followUpService = new FollowUpService();
    private IElasticSearchInfoService elasticSearchInfoService = new ElasticSearchInfoService();
    private IAbstractRegistrationDocumentService documentService = new RegistrationDocumentService();
    private IAttachmentService attachmentService = new AttachmentService();

    private final IConfigurationService configurationService = EasyMock.createNiceMock(IConfigurationService.class);

    private Organization organization;

    // test parameters
    private boolean generateRegistryDependentId = false;
    private Date endDateCreation = null;

    @Before
    public void init() throws SQLException {
        ReflectionTestUtils.setField(workflowService, "workflowDao", workflowDao);
        ReflectionTestUtils.setField(workflowService, "documentService", documentService);
        ReflectionTestUtils.setField(followUpService, "dao", followUpDao);
        ReflectionTestUtils.setField(elasticSearchInfoService, "dao", elasticSearchInfoDao);
        ReflectionTestUtils.setField(documentService, "documentDao", documentDao);
        ReflectionTestUtils.setField(attachmentService, "attachmentDao", attachmentDao);
        ReflectionTestUtils.setField(attachmentService, "attachmentContentDao", attachmentContentDao);
        ReflectionTestUtils.setField(attachmentService, "documentDao", documentDao);

        ReflectionTestUtils.setField(attachmentService, "configurationService", configurationService);

        EasyMock.expect(configurationService.get(ConfigurationKey.MAX_ATTACHMENT_SIZE)).andReturn(buildConfiguration("1000000")).anyTimes();
        EasyMock.replay(configurationService);

        organization = new TransactionTemplate(txManager).execute(new TransactionCallback<Organization>() {
            @Override
            public Organization doInTransaction(TransactionStatus status) {
            final Organization org = new Organization();
            org.setMain(true);
            org.setHealthDataIDType("RIZIV");
            org.setHealthDataIDValue("11111111");
            org.setName("Test-Organization");
            return organizationDao.create(org);
            }
        });
    }

    @After
    public void after() {
        EasyMock.verify(configurationService);
    }

    @Test
    public void testExecuteSaveAction() {
        final RegistrationWorkflow registration = executeFirstSave();
        executeSecondSave(registration);
    }

    @Test
    public void testGenerateRegistryDependentId() {
        generateRegistryDependentId = true;
        final RegistrationWorkflow registration = executeFirstSave();
        executeSecondSave(registration);
    }

    @Test // WDC-3060
    public void existingRegistrationCanBeSavedWhenCreationDateIsOver() {
        final RegistrationWorkflow registration = executeFirstSave();
        endDateCreation = DateUtils.remove(new Date(), new PeriodExpression(1, TimeUnit.DAY));
        executeSecondSave(registration);
    }

    @Test
    public void testExecuteWithMultipleThreads() throws InterruptedException {
        final int NB_INSERTS = 20;

        Assert.assertTrue(workflowDao.getAll().isEmpty());

        LOG.info("Creating threads");
        final List<FirstSaveCustomThread> threads = new ArrayList();
        for (int i = 0; i < NB_INSERTS; i++) {
            threads.add(new FirstSaveCustomThread());
        }

        LOG.info("Starting threads");
        for (FirstSaveCustomThread thread : threads) {
            thread.start();
        }

        LOG.info("Waiting for threads to terminate");
        while(!allTerminated(threads)) {
            Thread.sleep(1000);
        }

        for (final FirstSaveCustomThread thread : threads) {
            final Exception exception = thread.getException();
            if(exception != null) {
                LOG.error(exception.getMessage(), exception);
                Assert.fail();
            }
        }

        Assert.assertEquals(NB_INSERTS, workflowDao.getAll().size());
    }

    private RegistrationWorkflow executeFirstSave() {
        Assert.assertTrue(workflowDao.getAll().isEmpty());
        Assert.assertTrue(documentDao.getAll().isEmpty());
        Assert.assertTrue(followUpDao.getAll().isEmpty());
        Assert.assertTrue(elasticSearchInfoDao.getAll().isEmpty());
        Assert.assertTrue(attachmentDao.getAll().isEmpty());
        Assert.assertTrue(attachmentContentDao.getAll().isEmpty());

        RegistrationWorkflow registration = executeSave();
        Assert.assertEquals(WorkflowStatus.IN_PROGRESS, registration.getStatus());
        Assert.assertEquals(1, registration.getHistory().size());

        Assert.assertEquals(1, workflowDao.getAll().size());
        Assert.assertEquals(1, documentDao.getAll().size());
        Assert.assertEquals(1, followUpDao.getAll().size());
        Assert.assertEquals(1, elasticSearchInfoDao.getAll().size());
        Assert.assertEquals(1, attachmentDao.getAll().size());
        Assert.assertEquals(1, attachmentContentDao.getAll().size());
        return registration;
    }

    private void executeSecondSave(RegistrationWorkflow registration) {
        registration = executeSave(registration);

        Assert.assertEquals(WorkflowStatus.IN_PROGRESS, registration.getStatus());
        Assert.assertEquals(2, registration.getHistory().size());

        Assert.assertEquals(1, workflowDao.getAll().size());
        Assert.assertEquals(1, documentDao.getAll().size());
        Assert.assertEquals(1, followUpDao.getAll().size());
        Assert.assertEquals(1, elasticSearchInfoDao.getAll().size());
        Assert.assertEquals(1, attachmentDao.getAll().size());
        Assert.assertEquals(1, attachmentContentDao.getAll().size());
    }

    private boolean allTerminated(List<? extends Thread> threads) {
        for (final Thread t : threads) {
            if (!Thread.State.TERMINATED.equals(t.getState())) {
                return false;
            }
        }
        return true;
    }

    private RegistrationWorkflow executeSave() {
        return executeSave(null);
    }

    private RegistrationWorkflow executeSave(RegistrationWorkflow registration) {
        final DocumentData documentData = new DocumentData();
        documentData.setContent("test".getBytes());
        documentData.setPrivate("private".getBytes());
        documentData.setPatientID(createMap("key", "value"));

        final SaveRegistrationAction action = new SaveRegistrationAction();
        action.setDataCollectionDefinitionId(1L);
        action.setDocumentData(documentData);
        action.setToBeCoded(createMap("key", new TypedValue("value", "type")));
        action.setUniqueID(UUID.randomUUID().toString());

        if (registration != null) {
            action.setWorkflowId(registration.getId());
        } else {
            action.setAttachments(createModifiableList(buildAttachment()));
        }

        // create mocks
        final IDataCollectionDefinitionForwardService catalogueService = EasyMock.createNiceMock(IDataCollectionDefinitionForwardService.class);
        final IUserDataCollectionService userDataCollectionService = EasyMock.createNiceMock(IUserDataCollectionService.class);
        final INoteMigrationService<RegistrationWorkflow> noteMigrationService = EasyMock.createNiceMock(INoteMigrationService.class);
        final ICentralPlatformService centralPlatformService = EasyMock.createNiceMock(ICentralPlatformService.class);
        final IElasticSearchService elasticSearchService = EasyMock.createNiceMock(IElasticSearchService.class);
        final IVersionService versionService = EasyMock.createNiceMock(IVersionService.class);
        final IMappingContextFactory mappingContextFactory = EasyMock.createNiceMock(IMappingContextFactory.class);
        final be.wiv_isp.healthdata.gathering.service.IMappingService mappingService = EasyMock.createNiceMock(be.wiv_isp.healthdata.gathering.service.IMappingService.class);

        // set mocks
        ReflectionTestUtils.setField(action, "catalogueService", catalogueService);
        ReflectionTestUtils.setField(action, "userDataCollectionService", userDataCollectionService);
        ReflectionTestUtils.setField(action, "noteMigrationService", noteMigrationService);
        ReflectionTestUtils.setField(action, "centralPlatformService", centralPlatformService);
        ReflectionTestUtils.setField(action, "elasticSearchService", elasticSearchService);
        ReflectionTestUtils.setField(action, "versionService", versionService);
        ReflectionTestUtils.setField(action, "mappingContextFactory", mappingContextFactory);
        ReflectionTestUtils.setField(action, "mappingService", mappingService);

        // set autowired services
        ReflectionTestUtils.setField(action, "workflowService", workflowService);
        ReflectionTestUtils.setField(action, "followUpService", followUpService);
        ReflectionTestUtils.setField(action, "elasticSearchInfoService", elasticSearchInfoService);
        ReflectionTestUtils.setField(action, "attachmentService", attachmentService);
        ReflectionTestUtils.setField(action, "txManager", txManager);

        // prepare mocks
        final DataCollectionDefinitionDtoV7 dcd = buildDataCollectionDefinition();
        EasyMock.expect(catalogueService.get(1L, organization)).andReturn(buildDataCollectionDefinition());
        final HDServiceUser userDetails = HDServiceUser.create("username", organization);
        EasyMock.expect(userDataCollectionService.isUserAuthorized(userDetails, "TEST")).andReturn(true);
        if (registration != null) {
            EasyMock.expect(userDataCollectionService.isUserAuthorized(EasyMock.eq(userDetails), EasyMock.cmp(registration, idBasedComparator(), LogicalOperator.EQUAL))).andReturn(true);
        }
        EasyMock.expect(noteMigrationService.isMigrated(EasyMock.<RegistrationWorkflow>anyObject())).andReturn(true).anyTimes();
        EasyMock.expect(versionService.getVersionDetails()).andReturn(buildVersionDetails()).anyTimes();
        elasticSearchService.index(EasyMock.anyLong());
        EasyMock.expectLastCall();

        if (registration == null) {
            if (generateRegistryDependentId) {
                final RegistryDependentIdGenerationDefinition def = new RegistryDependentIdGenerationDefinition();
                def.setGenerationEnabled(true);
                def.setType(RegistryDependentIdGenerationDefinition.Type.TYPE_1);
                def.setPhase(RegistryDependentIdGenerationDefinition.Phase.CREATION);
                EasyMock.expect(centralPlatformService.getRegistryDependentIdGenerationDefinition("TEST")).andReturn(def);
                EasyMock.expect(centralPlatformService.getRegistryDependentId(organization, "TEST", RegistryDependentIdGenerationDefinition.Phase.CREATION)).andReturn("__registry_dependent_id__");

                EasyMock.expect(mappingContextFactory.create(organization, null, "__registry_dependent_id__")).andReturn(new MappingContext()).anyTimes();
                EasyMock.expect(mappingService.refresh((MappingContext) EasyMock.anyObject(), EasyMock.aryEq(dcd.getContent()), EasyMock.eq(documentData), EasyMock.eq(documentData.getPatientID()))).andReturn(documentData);
            } else {
                final RegistryDependentIdGenerationDefinition def = new RegistryDependentIdGenerationDefinition();
                def.setGenerationEnabled(false);
                EasyMock.expect(centralPlatformService.getRegistryDependentIdGenerationDefinition("TEST")).andReturn(def).anyTimes();
            }
        }

        EasyMock.replay(catalogueService, userDataCollectionService, noteMigrationService, centralPlatformService, versionService, mappingContextFactory, mappingService);

        final RegistrationWorkflow initialRegistration = action.retrieveWorkflow();
        final RegistrationWorkflow preExecuteRegistration = action.preExecute(initialRegistration, userDetails);
        final RegistrationWorkflow mainRegistration = action.execute(preExecuteRegistration, userDetails);
        final RegistrationWorkflow postExecuteRegistration = action.postExecute(mainRegistration, userDetails);

        if(generateRegistryDependentId) {
            Assert.assertEquals("__registry_dependent_id__", postExecuteRegistration.getRegistryDependentId());
        } else {
            Assert.assertNull(postExecuteRegistration.getRegistryDependentId());
        }

        EasyMock.verify(catalogueService, userDataCollectionService, noteMigrationService, centralPlatformService, versionService, mappingContextFactory, mappingService);

        return postExecuteRegistration;
    }

    private DataCollectionDefinitionDtoV7 buildDataCollectionDefinition() {
        final DataCollectionGroupDtoV7 dcg = new DataCollectionGroupDtoV7();
        dcg.setStartDate(new Timestamp(DateUtils.remove(new Date(), new PeriodExpression(1, TimeUnit.DAY)).getTime()));
        if(endDateCreation != null) {
            dcg.setEndDateCreation(new Timestamp(endDateCreation.getTime()));
        } else {
            dcg.setEndDateCreation(new Timestamp(DateUtils.add(new Date(), new PeriodExpression(1, TimeUnit.DAY)).getTime()));
        }

        final DataCollectionDefinitionDtoV7 dcd = new DataCollectionDefinitionDtoV7();
        dcd.setId(1L);
        dcd.setDataCollectionName("TEST");
        dcd.setDataCollectionGroup(dcg);
        dcd.setFollowUpDefinitions(createModifiableList(buildFollowUpDefinition()));
        return dcd;
    }

    private Map<String, String> createMap(String key, String value) {
        final Map<String, String> map = new HashMap<>();
        map.put(key, value);
        return map;
    }

    private Map<String, TypedValue> createMap(String key, TypedValue value) {
        final Map<String, TypedValue> map = new HashMap<>();
        map.put(key, value);
        map.put("emptyValue", new TypedValue("", "key"));
        return map;
    }

    private Attachment buildAttachment() {
        final AttachmentContent content = new AttachmentContent();
        content.setContent(UUID.randomUUID().toString().getBytes()); // TODO - same content gives an error in AbstractAttachmentService (fix and write test)

        final Attachment attachment = new Attachment();
        attachment.setContent(content);
        attachment.setUuid(UUID.randomUUID().toString());
        attachment.setFilename("test.txt");
        return attachment;
    }

    private VersionDetailsDto buildVersionDetails() {
        final VersionDetailsDto version = new VersionDetailsDto();
        version.setBackendCommit("backend-commit");
        return version;
    }

    private FollowUpDefinitionDto buildFollowUpDefinition() {
        final FollowUpDefinitionDto def = new FollowUpDefinitionDto();
        def.setName("FollowUpName");
        def.setLabel("FollowUpLabel");
        def.setDescription("FollowUpDescription");
        def.setTiming("2w");
        def.setBaseline(new Baseline(BaselineType.REGISTRATION_SUBMISSION_DATE, null));
        def.setConditions(createModifiableList("followUpCondition"));
        return def;
    }

    public Comparator<? super RegistrationWorkflow> idBasedComparator() {
        return new Comparator<RegistrationWorkflow>() {
            @Override
            public int compare(RegistrationWorkflow o1, RegistrationWorkflow o2) {
                return o1.getId().compareTo(o2.getId());
            }
        };
    }

    private class FirstSaveCustomThread extends Thread {

        private Exception exception;

        @Override
        public void run() {
            try {
                executeSave();
            } catch (Exception e) {
                exception = e;
            }
        }

        public Exception getException() {
            return exception;
        }

    }

    private <T> List<T> createModifiableList(T... objects) {
        final List<T> list = new ArrayList<>();
        for (T o : objects) {
            list.add(o);
        }
        return list;
    }

    private Configuration buildConfiguration(String value) {
        final Configuration configuration = new Configuration();
        configuration.setValue(value);
        return configuration;
    }
}
