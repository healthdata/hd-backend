/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.gathering.dao.impl;

import be.wiv_isp.healthdata.gathering.dao.IExceptionReportDao;
import be.wiv_isp.healthdata.gathering.domain.ExceptionReport;
import be.wiv_isp.healthdata.gathering.domain.search.ExceptionReportSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ExceptionReportDaoTest {

    public static final String UUID1 = "11111111-1111-1111-1111-111111111111";
    public static final String UUID2 = "22222222-2222-2222-2222-222222222222";
    public static final String UUID3 = "33333333-3333-3333-3333-333333333333";
    public static final String UUID4 = "44444444-4444-4444-4444-444444444444";

    @Autowired
    private IExceptionReportDao dao;

    private ExceptionReport exceptionReport1;
    private ExceptionReport exceptionReport2;
    private ExceptionReport exceptionReport3;


    private void setup() {
        exceptionReport1 = new ExceptionReport();
        exceptionReport1.setMessage("error1");
        exceptionReport1.setStackTrace("stackTrace1");
        Set<String> uuids1 = new HashSet<>();
        uuids1.add(UUID1);
        uuids1.add(UUID2);
        exceptionReport1.setUuids(uuids1);

        dao.create(exceptionReport1);


        exceptionReport2 = new ExceptionReport();
        exceptionReport2.setMessage("error1");
        Set<String> uuids2 = new HashSet<>();
        uuids2.add(UUID3);
        exceptionReport2.setUuids(uuids2);

        dao.create(exceptionReport2);


        exceptionReport3 = new ExceptionReport();
        exceptionReport3.setMessage("error3");
        Set<String> uuids3 = new HashSet<>();
        uuids3.add(UUID4);
        exceptionReport3.setUuids(uuids3);
        exceptionReport3.setReportedOn(new Timestamp(new Date().getTime()));

        dao.create(exceptionReport3);
    }

    @Test
    public void testSearchByHash() {
        setup();

        Assert.assertNotEquals(exceptionReport1.getHash(), exceptionReport2.getHash());

        ExceptionReportSearch search = new ExceptionReportSearch();
        List<ExceptionReport> all = dao.getAll(search);
        Assert.assertEquals(2, all.size());
        Assert.assertTrue(all.contains(exceptionReport1));
        Assert.assertTrue(all.contains(exceptionReport2));

        search = new ExceptionReportSearch();
        search.setHash(exceptionReport1.getHash());
        all = dao.getAll(search);
        Assert.assertEquals(1, all.size());
        Assert.assertTrue(all.contains(exceptionReport1));

        search = new ExceptionReportSearch();
        search.setUuid(UUID3);
        all = dao.getAll(search);
        Assert.assertEquals(1, all.size());
        Assert.assertTrue(all.contains(exceptionReport2));

        search = new ExceptionReportSearch();
        search.setReported(true);
        all = dao.getAll(search);
        Assert.assertEquals(1, all.size());
        Assert.assertTrue(all.contains(exceptionReport3));

        search = new ExceptionReportSearch();
        search.setReported(false);
        all = dao.getAll(search);
        Assert.assertEquals(2, all.size());
        Assert.assertTrue(all.contains(exceptionReport1));
        Assert.assertTrue(all.contains(exceptionReport2));
    }

}
