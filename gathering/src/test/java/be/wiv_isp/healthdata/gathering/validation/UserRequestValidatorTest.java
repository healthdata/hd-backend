/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.validation;

import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.domain.UserRequest;
import be.wiv_isp.healthdata.gathering.domain.search.UserRequestSearch;
import be.wiv_isp.healthdata.gathering.domain.validation.IUserRequestValidator;
import be.wiv_isp.healthdata.gathering.domain.validation.impl.UserRequestValidator;
import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.dao.IUserDao;
import be.wiv_isp.healthdata.orchestration.dao.IUserRequestDao;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.UserManagementType;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionService;
import be.wiv_isp.healthdata.orchestration.service.IUserManagementService;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserRequestValidatorTest {

	@Autowired
	private IUserRequestDao<UserRequest, UserRequestSearch> userRequestDao;

	@Autowired
	private IOrganizationDao organizationDao;

	@Autowired
	private IUserDao userDao;

	private IDataCollectionService dataCollectionService = EasyMock.createStrictMock(IDataCollectionService.class);
	private IUserManagementService userManagementService = EasyMock.createStrictMock(IUserManagementService.class);

	private IUserRequestValidator validator = new UserRequestValidator();

	@Before
	public void before() {
		ReflectionTestUtils.setField(validator, "userDao", userDao);
		ReflectionTestUtils.setField(validator, "dataCollectionService", dataCollectionService);
		ReflectionTestUtils.setField(validator, "userManagementService", userManagementService);
	}

	@Test
	public void testValidUserRequest() {
		Assert.assertTrue(organizationDao.getAll().isEmpty());
		Assert.assertTrue(userDao.getAll().isEmpty());

		final Organization organization = createOrganization();
		createUser("username", organization, "email@test.com");

		final UserRequest request = buildUserRequest("username_1", organization, "password", "lastname", "firstname", "email_1@test.com", "email-requester@test.com", "TEST");

		EasyMock.expect(userManagementService.getUserManagementType(organization)).andReturn(UserManagementType.DATABASE);
		EasyMock.expect(dataCollectionService.getDataCollectionsFlat(organization)).andReturn(new HashSet<>(Collections.singletonList("TEST")));

		EasyMock.replay(dataCollectionService, userManagementService);
		validator.validate(request);
		EasyMock.verify(dataCollectionService, userManagementService);
	}

	@Test
	public void testUsernameAlreadyExists() {
		Assert.assertTrue(organizationDao.getAll().isEmpty());
		Assert.assertTrue(userDao.getAll().isEmpty());

		final Organization organization = createOrganization();
		createUser("username", organization, "email@test.com");

		final UserRequest request = buildUserRequest("username", organization, "password", "lastname", "firstname", "email_1@test.com", "email-requester@test.com", "TEST");

		EasyMock.expect(userManagementService.getUserManagementType(organization)).andReturn(UserManagementType.DATABASE);
		EasyMock.expect(dataCollectionService.getDataCollectionsFlat(organization)).andReturn(new HashSet<>(Collections.singletonList("TEST")));

		EasyMock.replay(dataCollectionService, userManagementService);
		try {
			validator.validate(request);
			Assert.fail();
		} catch (HealthDataException e) {
			Assert.assertEquals("username already exists", e.getMessage());
		}
		EasyMock.verify(dataCollectionService, userManagementService);
	}

	@Test
	public void testEmailAlreadyExists() {
		Assert.assertTrue(organizationDao.getAll().isEmpty());
		Assert.assertTrue(userDao.getAll().isEmpty());

		final Organization organization = createOrganization();
		createUser("username", organization, "email@test.com");

		final UserRequest request = buildUserRequest("username_1", organization, "password", "lastname", "firstname", "email@test.com", "email-requester@test.com", "TEST");

		EasyMock.expect(userManagementService.getUserManagementType(organization)).andReturn(UserManagementType.DATABASE);
		EasyMock.expect(dataCollectionService.getDataCollectionsFlat(organization)).andReturn(new HashSet<>(Collections.singletonList("TEST")));

		EasyMock.replay(dataCollectionService, userManagementService);
		try {
			validator.validate(request);
			Assert.fail();
		} catch (HealthDataException e) {
			Assert.assertEquals("email already exists", e.getMessage());
		}
		EasyMock.verify(dataCollectionService, userManagementService);
	}

	@Test
	public void testDataCollectionsAreFilteredOut() {
		Assert.assertTrue(organizationDao.getAll().isEmpty());
		Assert.assertTrue(userDao.getAll().isEmpty());

		final Organization organization = createOrganization();
		createUser("username", organization, "email@test.com");

		final UserRequest request = buildUserRequest("username_1", organization, "password", "lastname", "firstname", "email_1@test.com", "email-requester@test.com", "TEST_1", "TEST_2", "TEST_3");

		EasyMock.expect(userManagementService.getUserManagementType(organization)).andReturn(UserManagementType.DATABASE);
		EasyMock.expect(dataCollectionService.getDataCollectionsFlat(organization)).andReturn(new HashSet<>(Arrays.asList("TEST_1", "TEST_2", "TEST_4")));

		Assert.assertEquals(new HashSet<>(Arrays.asList("TEST_1", "TEST_2", "TEST_3")), request.getDataCollectionNames());
		EasyMock.replay(dataCollectionService, userManagementService);
		validator.validate(request);
		EasyMock.verify(dataCollectionService, userManagementService);
		Assert.assertEquals(new HashSet<>(Arrays.asList("TEST_1", "TEST_2")), request.getDataCollectionNames());
	}

	@Test
	public void testUserRequestWithSameEmailOrUsernameIsAccepted() {
		Assert.assertTrue(organizationDao.getAll().isEmpty());
		Assert.assertTrue(userDao.getAll().isEmpty());

		final Organization organization = createOrganization();
		userRequestDao.create(buildUserRequest("username", organization, "password", "lastname", "firstname", "email@test.com", "email-requester@test.com", "TEST"));

		final UserRequest request = buildUserRequest("username", organization, "password", "lastname", "firstname", "email@test.com", "email-requester@test.com", "TEST");

		EasyMock.expect(userManagementService.getUserManagementType(organization)).andReturn(UserManagementType.DATABASE);
		EasyMock.expect(dataCollectionService.getDataCollectionsFlat(organization)).andReturn(new HashSet<>(Collections.singletonList("TEST")));

		EasyMock.replay(dataCollectionService, userManagementService);
		validator.validate(request);
		EasyMock.verify(dataCollectionService, userManagementService);
		userRequestDao.create(request);

		Assert.assertEquals(2, userRequestDao.getAll().size());
	}

	private UserRequest buildUserRequest(String username, Organization organization, String password, String lastname, String firstname, String email, String emailRequester, String... dataCollectionNames) {
		final UserRequest request = new UserRequest();
		request.setUsername(username);
		request.setOrganization(organization);
		request.setPassword(password);
		request.setLastName(lastname);
		request.setFirstName(firstname);
		request.setEmail(email);
		request.setEmailRequester(emailRequester);
		request.setDataCollectionNames(new HashSet<>(Arrays.asList(dataCollectionNames)));
		return request;
	}

	private User createUser(String username, Organization organization, String email) {
		final User user = new User();
		user.setUsername(username);
		user.setOrganization(organization);
		user.setEmail(email);
		return userDao.create(user);
	}

	private Organization createOrganization() {
		return organizationDao.create(new Organization());
	}
}
