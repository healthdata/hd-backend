/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.api.mapper;

import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.domain.FastTrackUpload;
import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class FastTrackUploadMapperTest {

	@Test
	public void testConvertCorrect() throws Exception {
		String request = IOUtils.toString(FastTrackUploadMapperTest.class.getResourceAsStream("preloadingRequestMapperTest1.json"));
		JSONObject json = new JSONObject(request);

		FastTrackUpload expected = new FastTrackUpload();
		expected.setMode(FastTrackUpload.Mode.OVERWRITE);
		expected.setMaster(FastTrackUpload.Master.CSV);
		expected.setDataCollectionDefinitionId(1L);
		expected.setFileName("file.csv");
		expected.setContent("some CSV content");

		FastTrackUpload convert = FastTrackUploadMapper.convert(json);
		Assert.assertEquals(expected, convert);
	}

	@Test(expected = HealthDataException.class)
	public void testConvertWrongMode() throws Exception {
		String request = IOUtils.toString(FastTrackUploadMapperTest.class.getResourceAsStream("preloadingRequestMapperTest2.json"));
		JSONObject json = new JSONObject(request);

		FastTrackUploadMapper.convert(json);
	}

	@Test(expected = HealthDataException.class)
	public void testConvertWrongMaster() throws Exception {
		String request = IOUtils.toString(FastTrackUploadMapperTest.class.getResourceAsStream("preloadingRequestMapperTest3.json"));
		JSONObject json = new JSONObject(request);

		FastTrackUploadMapper.convert(json);
	}
}