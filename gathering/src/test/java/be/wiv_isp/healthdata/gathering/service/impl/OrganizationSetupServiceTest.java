/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.common.dto.SalesForceOrganizationDto;
import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.gathering.dao.IUpdatedUserDao;
import be.wiv_isp.healthdata.gathering.service.IUpdatedUserService;
import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.dao.IUserConfigurationDao;
import be.wiv_isp.healthdata.orchestration.dao.IUserDao;
import be.wiv_isp.healthdata.orchestration.domain.AbstractElasticSearchInfo;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.search.ElasticSearchInfoSearch;
import be.wiv_isp.healthdata.orchestration.domain.search.OrganizationSearch;
import be.wiv_isp.healthdata.orchestration.domain.validator.IUserValidator;
import be.wiv_isp.healthdata.orchestration.domain.validator.impl.UserValidator;
import be.wiv_isp.healthdata.orchestration.service.*;
import be.wiv_isp.healthdata.orchestration.service.impl.OrganizationService;
import be.wiv_isp.healthdata.orchestration.service.impl.OrganizationSetupService;
import be.wiv_isp.healthdata.orchestration.service.impl.UserConfigurationService;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class OrganizationSetupServiceTest {

    // service to test
    private IOrganizationSetupService organizationSetupService = new OrganizationSetupService();

    private IOrganizationService organizationService = new OrganizationService();
    private IUserService userService = new UserService();
    private IUpdatedUserService updatedUserService = new UpdatedUserService();
    private IUserValidator userValidator = new UserValidator();
    private IUserConfigurationService userConfigurationService = new UserConfigurationService();

    // mocks
    private ISalesForceOrganizationService salesForceOrganizationService = EasyMock.createNiceMock(ISalesForceOrganizationService.class);
    private IPlatformService platformService = EasyMock.createNiceMock(IPlatformService.class);
    private IConfigurationService configurationService = EasyMock.createNiceMock(IConfigurationService.class);
    private IElasticSearchInfoService elasticSearchInfoService = EasyMock.createNiceMock(IElasticSearchInfoService.class);

    @Autowired
    private CacheManager cacheManager;

    @Resource(name = "passwordEncoder")
    protected PasswordEncoder passwordEncoder;

    @Autowired
    private IOrganizationDao organizationDao;
    @Autowired
    private IUserDao userDao;
    @Autowired
    private IUpdatedUserDao updatedUserDao;
    @Autowired
    private IUserConfigurationDao userConfigurationDao;

    @Before
    public void before() {
        // DAOs
        ReflectionTestUtils.setField(organizationService, "organizationDao", organizationDao);
        ReflectionTestUtils.setField(updatedUserService, "dao", updatedUserDao);
        ReflectionTestUtils.setField(userValidator, "userDao", userDao);
        ReflectionTestUtils.setField(userConfigurationService, "dao", userConfigurationDao);
        ReflectionTestUtils.setField(userService, "userDao", userDao);

        // Other services
        ReflectionTestUtils.setField(organizationService, "cacheManager", cacheManager);
        ReflectionTestUtils.setField(userConfigurationService, "platformService", platformService);
        ReflectionTestUtils.setField(userService, "updatedUserService", updatedUserService);
        ReflectionTestUtils.setField(userService, "passwordEncoder", passwordEncoder);
        ReflectionTestUtils.setField(userService, "userValidator", userValidator);
        ReflectionTestUtils.setField(userService, "userConfigurationService", userConfigurationService);
        ReflectionTestUtils.setField(organizationSetupService, "organizationService", organizationService);
        ReflectionTestUtils.setField(organizationSetupService, "salesForceOrganizationService", salesForceOrganizationService);
        ReflectionTestUtils.setField(organizationSetupService, "userService", userService);
        ReflectionTestUtils.setField(organizationSetupService, "configurationService", configurationService);
        ReflectionTestUtils.setField(organizationSetupService, "elasticSearchInfoService", elasticSearchInfoService);
    }

    @Test
    public void testMainOrganizationIsCreated() {
        Assert.assertTrue(userDao.getAll().isEmpty());
        Assert.assertTrue(organizationDao.getAll().isEmpty());

        // Mocks expectations
        EasyMock.expect(platformService.getCurrentPlatform()).andReturn(Platform.HD4DP).anyTimes();

        EasyMock.replay(platformService);
        organizationSetupService.updateOrganizations();
        EasyMock.verify(platformService);

        // verify main organization is created
        final List<Organization> organizations = organizationDao.getAll();
        Assert.assertEquals(1, organizations.size());
        final Organization organization = organizations.get(0);
        Assert.assertNull(organization.getHealthDataIDType());
        Assert.assertNull(organization.getHealthDataIDValue());
        Assert.assertEquals("Main organization not configured", organization.getName());
        Assert.assertTrue(organization.isMain());
        Assert.assertFalse(organization.isDeleted());

        // verify admin is created
        final List<User> users = userDao.getAll();
        Assert.assertEquals(1, users.size());
        final User user = users.get(0);
        Assert.assertEquals("admin", user.getUsername());
        Assert.assertTrue(user.isAdmin());
        Assert.assertTrue(user.isSuperAdmin());
        Assert.assertEquals(organization, user.getOrganization());
    }

    @Test
    public void testMainOrganizationNotConfigured() {
        final Organization main = buildOrganization(null, null, "Main organization not configured", true, false, false);
        organizationDao.create(main);
        Assert.assertEquals(1, organizationDao.getAll().size());

        organizationSetupService.updateOrganizations();

        final List<Organization> organizations = organizationDao.getAll();
        Assert.assertEquals(1, organizations.size());
        final Organization organization = organizations.get(0);
        Assert.assertNull(organization.getHealthDataIDType());
        Assert.assertNull(organization.getHealthDataIDValue());
        Assert.assertEquals("Main organization not configured", organization.getName());
        Assert.assertTrue(organization.isMain());
        Assert.assertFalse(organization.isDeleted());
    }

    @Test
    public void testSubOrganizationsAreCreatedWhenMainOrganizationIsConfigured() {
        final Organization main = buildOrganization("RIZIV", "11111111", "Main Organization", true, false, false);
        organizationDao.create(main);
        Assert.assertEquals(1, organizationDao.getAll().size());

        final SalesForceOrganizationDto mainOrganizationDto = buildOrganizationDto("RIZIV", "11111111", "Organization-11111111", false);
        final List<SalesForceOrganizationDto> subOrganizationDtos = new ArrayList<>();
        subOrganizationDtos.add(buildOrganizationDto("RIZIV", "22222222", "Sub-Organization-22222222", false));
        subOrganizationDtos.add(buildOrganizationDto("RIZIV", "33333333", "Sub-Organization-33333333", false));

        // Mocks expectations
        EasyMock.expect(salesForceOrganizationService.getSalesForceOrganization("RIZIV", "11111111")).andReturn(mainOrganizationDto).once();
        EasyMock.expect(salesForceOrganizationService.getSalesForceSubOrganizations("RIZIV", "11111111")).andReturn(subOrganizationDtos).once();
        EasyMock.expect(platformService.getCurrentPlatform()).andReturn(Platform.HD4DP).anyTimes();
        configurationService.createMissing();
        EasyMock.expectLastCall().once();

        EasyMock.replay(platformService, salesForceOrganizationService, configurationService);
        organizationSetupService.updateOrganizations();
        EasyMock.verify(platformService, salesForceOrganizationService, configurationService);

        final List<Organization> organizations = organizationDao.getAll();
        Assert.assertEquals(3, organizations.size());

        OrganizationSearch search = new OrganizationSearch();
        search.setHealthDataIDValue("11111111");
        Organization organization = organizationDao.getUnique(search);
        Assert.assertEquals("RIZIV", organization.getHealthDataIDType());
        Assert.assertEquals("11111111", organization.getHealthDataIDValue());
        Assert.assertEquals("Organization-11111111", organization.getName());
        Assert.assertTrue(organization.isMain());
        Assert.assertFalse(organization.isDeleted());

        search = new OrganizationSearch();
        search.setHealthDataIDValue("22222222");
        organization = organizationDao.getUnique(search);
        Assert.assertEquals("RIZIV", organization.getHealthDataIDType());
        Assert.assertEquals("22222222", organization.getHealthDataIDValue());
        Assert.assertEquals("Sub-Organization-22222222", organization.getName());
        Assert.assertFalse(organization.isMain());
        Assert.assertFalse(organization.isDeleted());

        search = new OrganizationSearch();
        search.setHealthDataIDValue("33333333");
        organization = organizationDao.getUnique(search);
        Assert.assertEquals("RIZIV", organization.getHealthDataIDType());
        Assert.assertEquals("33333333", organization.getHealthDataIDValue());
        Assert.assertEquals("Sub-Organization-33333333", organization.getName());
        Assert.assertFalse(organization.isMain());
        Assert.assertFalse(organization.isDeleted());

        // TODO: verify users have been created
    }

    @Test
    public void testSubOrganizationIsDeactivated() {
        organizationDao.create(buildOrganization("RIZIV", "11111111", "Organization-11111111", true, false, false));
        final Organization subOrganization = organizationDao.create(buildOrganization("RIZIV", "22222222", "Sub-Organization-22222222", false, false, false));
        Assert.assertEquals(2, organizationDao.getAll().size());

        final SalesForceOrganizationDto mainOrganizationDto = buildOrganizationDto("RIZIV", "11111111", "Organization-11111111", false);
        final List<SalesForceOrganizationDto> subOrganizationDtos = new ArrayList<>();

        // Mocks expectations
        EasyMock.expect(salesForceOrganizationService.getSalesForceOrganization("RIZIV", "11111111")).andReturn(mainOrganizationDto).once();
        EasyMock.expect(salesForceOrganizationService.getSalesForceSubOrganizations("RIZIV", "11111111")).andReturn(subOrganizationDtos).once();
        EasyMock.expect(platformService.getCurrentPlatform()).andReturn(Platform.HD4DP).anyTimes();
        configurationService.createMissing();
        EasyMock.expectLastCall().once();
        final ElasticSearchInfoSearch esSearch = new ElasticSearchInfoSearch();
        esSearch.setOrganizationId(subOrganization.getId());
        elasticSearchInfoService.updateAll(esSearch, AbstractElasticSearchInfo.Status.DELETED);
        EasyMock.expectLastCall();

        EasyMock.replay(platformService, salesForceOrganizationService, configurationService, elasticSearchInfoService);
        organizationSetupService.updateOrganizations();
        EasyMock.verify(platformService, salesForceOrganizationService, configurationService, elasticSearchInfoService);

        final List<Organization> organizations = organizationDao.getAll();
        Assert.assertEquals(1, organizations.size());

        OrganizationSearch search = new OrganizationSearch();
        search.setHealthDataIDValue("11111111");
        Organization organization = organizationDao.getUnique(search);
        Assert.assertEquals("RIZIV", organization.getHealthDataIDType());
        Assert.assertEquals("11111111", organization.getHealthDataIDValue());
        Assert.assertEquals("Organization-11111111", organization.getName());
        Assert.assertTrue(organization.isMain());
        Assert.assertFalse(organization.isDeleted());
    }

    @Test
    public void testSubOrganizationIsReactivated() {
        organizationDao.create(buildOrganization("RIZIV", "11111111", "Organization-11111111", true, false, false));
        final Organization subOrganization = organizationDao.create(buildOrganization("RIZIV", "22222222", "Sub-Organization-22222222", false, true, false));
        Assert.assertEquals(1, organizationDao.getAll().size());

        final SalesForceOrganizationDto mainOrganizationDto = buildOrganizationDto("RIZIV", "11111111", "Organization-11111111", false);
        final List<SalesForceOrganizationDto> subOrganizationDtos = new ArrayList<>();
        subOrganizationDtos.add(buildOrganizationDto("RIZIV", "22222222", "Sub-Organization-22222222", false));

        // Mocks expectations
        EasyMock.expect(salesForceOrganizationService.getSalesForceOrganization("RIZIV", "11111111")).andReturn(mainOrganizationDto).once();
        EasyMock.expect(salesForceOrganizationService.getSalesForceSubOrganizations("RIZIV", "11111111")).andReturn(subOrganizationDtos).once();
        EasyMock.expect(platformService.getCurrentPlatform()).andReturn(Platform.HD4DP).anyTimes();
        configurationService.createMissing();
        EasyMock.expectLastCall().once();
        final ElasticSearchInfoSearch esSearch = new ElasticSearchInfoSearch();
        esSearch.setOrganizationId(subOrganization.getId());
        elasticSearchInfoService.updateAll(esSearch, AbstractElasticSearchInfo.Status.NOT_INDEXED);
        EasyMock.expectLastCall();

        EasyMock.replay(platformService, salesForceOrganizationService, configurationService, elasticSearchInfoService);
        organizationSetupService.updateOrganizations();
        EasyMock.verify(platformService, salesForceOrganizationService, configurationService, elasticSearchInfoService);

        final List<Organization> organizations = organizationDao.getAll();
        Assert.assertEquals(2, organizations.size());

        OrganizationSearch search = new OrganizationSearch();
        search.setHealthDataIDValue("11111111");
        Organization organization = organizationDao.getUnique(search);
        Assert.assertEquals("RIZIV", organization.getHealthDataIDType());
        Assert.assertEquals("11111111", organization.getHealthDataIDValue());
        Assert.assertEquals("Organization-11111111", organization.getName());
        Assert.assertTrue(organization.isMain());
        Assert.assertFalse(organization.isDeleted());

        search = new OrganizationSearch();
        search.setHealthDataIDValue("22222222");
        organization = organizationDao.getUnique(search);
        Assert.assertEquals("RIZIV", organization.getHealthDataIDType());
        Assert.assertEquals("22222222", organization.getHealthDataIDValue());
        Assert.assertEquals("Sub-Organization-22222222", organization.getName());
        Assert.assertFalse(organization.isMain());
        Assert.assertFalse(organization.isDeleted());
    }

    @Test
    public void testSubOrganizationIsUpdated() {
        organizationDao.create(buildOrganization("RIZIV", "11111111", "Organization-11111111", true, false, false));
        organizationDao.create(buildOrganization("RIZIV", "22222222", "Sub-Organization-22222222", false, false, true));
        Assert.assertEquals(2, organizationDao.getAll().size());

        final SalesForceOrganizationDto mainOrganizationDto = buildOrganizationDto("RIZIV", "11111111", "Organization-11111111-Renamed", true);
        final List<SalesForceOrganizationDto> subOrganizationDtos = new ArrayList<>();
        subOrganizationDtos.add(buildOrganizationDto("RIZIV", "22222222", "Sub-Organization-22222222-Renamed", false));

        // Mocks expectations
        EasyMock.expect(salesForceOrganizationService.getSalesForceOrganization("RIZIV", "11111111")).andReturn(mainOrganizationDto).once();
        EasyMock.expect(salesForceOrganizationService.getSalesForceSubOrganizations("RIZIV", "11111111")).andReturn(subOrganizationDtos).once();
        EasyMock.expect(platformService.getCurrentPlatform()).andReturn(Platform.HD4DP).anyTimes();
        configurationService.createMissing();
        EasyMock.expectLastCall();

        EasyMock.replay(platformService, salesForceOrganizationService, configurationService);
        organizationSetupService.updateOrganizations();
        EasyMock.verify(platformService, salesForceOrganizationService, configurationService);

        final List<Organization> organizations = organizationDao.getAll();
        Assert.assertEquals(2, organizations.size());

        OrganizationSearch search = new OrganizationSearch();
        search.setHealthDataIDValue("11111111");
        Organization organization = organizationDao.getUnique(search);
        Assert.assertEquals("RIZIV", organization.getHealthDataIDType());
        Assert.assertEquals("11111111", organization.getHealthDataIDValue());
        Assert.assertEquals("Organization-11111111-Renamed", organization.getName());
        Assert.assertTrue(organization.isMain());
        Assert.assertTrue(organization.isHd4prc());
        Assert.assertFalse(organization.isDeleted());

        search = new OrganizationSearch();
        search.setHealthDataIDValue("22222222");
        organization = organizationDao.getUnique(search);
        Assert.assertEquals("RIZIV", organization.getHealthDataIDType());
        Assert.assertEquals("22222222", organization.getHealthDataIDValue());
        Assert.assertEquals("Sub-Organization-22222222-Renamed", organization.getName());
        Assert.assertFalse(organization.isMain());
        Assert.assertFalse(organization.isHd4prc());
        Assert.assertFalse(organization.isDeleted());
    }

    @Test
    public void testInvalidOrganizationIsIgnored() {
        organizationDao.create(buildOrganization("RIZIV", "11111111", "Organization-11111111", true, false, false));
        Assert.assertEquals(1, organizationDao.getAll().size());

        final SalesForceOrganizationDto mainOrganizationDto = buildOrganizationDto("RIZIV", "11111111", "Organization-11111111", false);
        final List<SalesForceOrganizationDto> subOrganizationDtos = new ArrayList<>();
        subOrganizationDtos.add(buildOrganizationDto(null, null, "Sub-Organization-22222222", false));
        subOrganizationDtos.add(buildOrganizationDto("null", "33333333", "Sub-Organization-33333333", false));
        subOrganizationDtos.add(buildOrganizationDto("RIZIV", "null", "Sub-Organization-44444444", false));
        subOrganizationDtos.add(buildOrganizationDto("RIZIV", "55555555", "Sub-Organization-55555555", false));

        // Mocks expectations
        EasyMock.expect(salesForceOrganizationService.getSalesForceOrganization("RIZIV", "11111111")).andReturn(mainOrganizationDto).once();
        EasyMock.expect(salesForceOrganizationService.getSalesForceSubOrganizations("RIZIV", "11111111")).andReturn(subOrganizationDtos).once();
        EasyMock.expect(platformService.getCurrentPlatform()).andReturn(Platform.HD4DP).anyTimes();
        configurationService.createMissing();
        EasyMock.expectLastCall();

        EasyMock.replay(platformService, salesForceOrganizationService, configurationService);
        organizationSetupService.updateOrganizations();
        EasyMock.verify(platformService, salesForceOrganizationService, configurationService);

        final List<Organization> organizations = organizationDao.getAll();
        Assert.assertEquals(2, organizations.size());

        OrganizationSearch search = new OrganizationSearch();
        search.setHealthDataIDValue("11111111");
        Organization organization = organizationDao.getUnique(search);
        Assert.assertEquals("RIZIV", organization.getHealthDataIDType());
        Assert.assertEquals("11111111", organization.getHealthDataIDValue());
        Assert.assertEquals("Organization-11111111", organization.getName());
        Assert.assertTrue(organization.isMain());
        Assert.assertFalse(organization.isDeleted());

        search = new OrganizationSearch();
        search.setHealthDataIDValue("55555555");
        organization = organizationDao.getUnique(search);
        Assert.assertEquals("RIZIV", organization.getHealthDataIDType());
        Assert.assertEquals("55555555", organization.getHealthDataIDValue());
        Assert.assertEquals("Sub-Organization-55555555", organization.getName());
        Assert.assertFalse(organization.isMain());
        Assert.assertFalse(organization.isDeleted());
    }

    private Organization buildOrganization(String idType, String idValue, String name, boolean isMain, boolean isDeleted, boolean isHd4prc) {
        final Organization organization = new Organization();
        organization.setHealthDataIDType(idType);
        organization.setHealthDataIDValue(idValue);
        organization.setName(name);
        organization.setMain(isMain);
        organization.setHd4prc(isHd4prc);
        organization.setDeleted(isDeleted);
        return organization;
    }

    private SalesForceOrganizationDto buildOrganizationDto(String identificationType, String identificationValue, String name, boolean isHd4prc) {
        final SalesForceOrganizationDto dto = new SalesForceOrganizationDto();
        dto.setIdentificationType(identificationType);
        dto.setIdentificationValue(identificationValue);
        dto.setName(name);
        dto.setHd4prc(isHd4prc);
        return dto;
    }

}
