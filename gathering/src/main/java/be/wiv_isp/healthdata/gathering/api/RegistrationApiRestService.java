/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package be.wiv_isp.healthdata.gathering.api;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.pagination.PaginationHelper;
import be.wiv_isp.healthdata.common.pagination.PaginationRange;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.gathering.dto.RegistrationApiDto;
import be.wiv_isp.healthdata.gathering.service.IRegistrationApiService;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;


@Component
@Path("/api/{version}")
public class RegistrationApiRestService {

    private static final Logger LOG = LoggerFactory.getLogger(RegistrationApiRestService.class);

    @Autowired
    private IRegistrationApiService registrationApiService;

    @GET
    @Path("/registrations/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_API')")
    @Produces(MediaType.APPLICATION_XML)
    public Response get(@PathParam("version") String version, @PathParam("id") Long id, @QueryParam("validation") Boolean validation) {
        if (!checkVersion(version))
            return badVersionResponse(version);
        try {
            return registrationApiService.get(id, validation);
        }
        catch (HealthDataException hde) {
            return buildErrorResponse(hde);
        }
    }

    @GET
    @Path("/registrations")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_API')")
    @Produces(MediaType.APPLICATION_XML)
    public Response getAll(@PathParam("version") String version, @Context UriInfo info, @HeaderParam("range") String range) {
        if (!checkVersion(version))
            return badVersionResponse(version);

        try {
            RegistrationWorkflowSearch search = buildSearch(info);
            Boolean validation = getValidation(info);
            PaginationRange paginationRange = PaginationHelper.buildPaginationRange(range);
            return registrationApiService.getAll(search, paginationRange, validation);
        }
        catch (HealthDataException hde) {
            return buildErrorResponse(hde);
        }
    }

    @POST
    @Path("/registrations")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_API')")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public Response create(@PathParam("version") String version, String xml) {
        if (!checkVersion(version))
            return badVersionResponse(version);
        try {
            return registrationApiService.create(xml);
        }
        catch (HealthDataException hde) {
            return buildErrorResponse(hde);
        }
    }

    @PUT
    @Path("/registrations")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_API')")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public Response update(@PathParam("version") String version, String xml) {
        LOG.warn("Request to update registration, not yet implemented.");
        if (!checkVersion(version))
            return badVersionResponse(version);
        if (version.equalsIgnoreCase("v1"))
            return buildErrorResponse(501, "Registration updates are not implemented in this version.");
        else {
            return registrationApiService.update(xml);
        }
    }

    @DELETE
    @Path("/registrations/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_API')")
    @Produces(MediaType.APPLICATION_XML)
    public Response delete(@PathParam("version") String version, @PathParam("id") Long id) {
        if (!checkVersion(version))
            return badVersionResponse(version);
        try {
            return registrationApiService.delete(id);
        }
        catch (HealthDataException hde) {
            return buildErrorResponse(hde);
        }
    }

    private boolean checkVersion(String version) {
        return version.equalsIgnoreCase(registrationApiService.getVersion());
    }

    private static Response badVersionResponse(String version) {
        RegistrationApiDto dto = new RegistrationApiDto();
        dto.setError(MessageFormat.format("Bad API version: {0}", version));
        return Response.status(Response.Status.BAD_REQUEST).entity(dto).build();
    }

    private static Response buildErrorResponse(HealthDataException hde) {
        return buildErrorResponse(hde.getResponseStatus().getStatusCode(), hde.getMessage());
    }

    private static Response buildErrorResponse(int status, String message, Object ... objects) {
        LOG.error(status + ": " + MessageFormat.format(message, objects));
        RegistrationApiDto dto = new RegistrationApiDto();
        dto.setError(MessageFormat.format(message, objects));
        return Response.status(status).entity(dto).build();
    }

    private static RegistrationWorkflowSearch buildSearch(UriInfo info) {
        RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
        final MultivaluedMap<String, String> params = info.getQueryParameters();
        for (String key : params.keySet()) {
            if (key.equalsIgnoreCase(Parameters.DATA_COLLECTION)) {
                search.setDataCollectionName(params.getFirst(key).toUpperCase());
            } else if (key.equalsIgnoreCase(Parameters.STATUS)) {
                try {
                    WorkflowStatus workflowStatus = WorkflowStatus.valueOf(params.getFirst(key).toUpperCase());
                    Set<WorkflowStatus> workflowStatuses = new HashSet<>();
                    workflowStatuses.add(workflowStatus);
                    search.setStatuses(workflowStatuses);
                } catch (IllegalArgumentException iae) {
                    HealthDataException exception = new HealthDataException(iae);
                    exception.setExceptionType(ExceptionType.INVALID_PARAMETER, params.getFirst(key));
                    throw exception;
                }
            } else if (key.equalsIgnoreCase(Parameters.CREATED_AFTER)) {
                SimpleDateFormat formatter = new SimpleDateFormat(Parameters.DATE_FORMAT);
                try {
                    search.setCreatedAfterDate(formatter.parse(params.getFirst(key)));
                } catch (ParseException pe) {
                    HealthDataException exception = new HealthDataException(pe);
                    exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, params.getFirst(key), Parameters.DATE_FORMAT);
                    throw exception;
                }
            } else if (key.equalsIgnoreCase(Parameters.UPDATED_AFTER)) {
                SimpleDateFormat formatter = new SimpleDateFormat(Parameters.DATE_FORMAT);
                try {
                    search.setUpdatedAfterDate(formatter.parse(params.getFirst(key)));
                } catch (ParseException pe) {
                    HealthDataException exception = new HealthDataException(pe);
                    exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, params.getFirst(key), Parameters.DATE_FORMAT);
                    throw exception;
                }
            }
        }
        return search;
    }

    private static boolean getValidation(UriInfo info) {
        final MultivaluedMap<String, String> params = info.getQueryParameters();
        for (String key : params.keySet()) {
            if (Parameters.VALIDATION.equalsIgnoreCase(key))
                return Boolean.parseBoolean(params.getFirst(key));
        }
        return false;
    }

    public class Parameters {
        public static final String DATA_COLLECTION = "dataCollection";
        public static final String STATUS = "status";
        public static final String CREATED_AFTER = "createdAfter";
        public static final String UPDATED_AFTER = "updatedAfter";
        public static final String DATE_FORMAT = "yyyy-MM-dd";
        public static final String VALIDATION = "validation";
    }

}
