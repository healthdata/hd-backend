/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package be.wiv_isp.healthdata.gathering.dto;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.jaxb.xml.SqlTimestampAdapter;
import org.apache.commons.lang3.StringUtils;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


@XmlRootElement(name="healthdata")
@XmlAccessorType(XmlAccessType.NONE)
@XmlType(propOrder = {"error", "options", "registration", "registrations"})
public class RegistrationApiDto implements Serializable {

    @XmlElement
    private String error;
    @XmlElement
    private ApiOptionsDto options;
    @XmlElement
    private RegistrationDto registration;
    @XmlElementWrapper
    @XmlElement(name="registration")
    private List<RegistrationDto> registrations;
    private int httpStatusCode;


    public RegistrationApiDto() {
    }

    public RegistrationDto getRegistration() {
        return registration;
    }

    public void setRegistration(RegistrationDto registration) {
        this.registration = registration;
    }

    public void setRegistration(RegistrationWorkflow registration) {
        this.registration = new RegistrationDto(registration);
    }

    public List<RegistrationDto> getRegistrations() {
        return registrations;
    }

    public void setRegistrations(List<RegistrationDto> registrations) {
        this.registrations = registrations;
    }

    public void addRegistration(RegistrationDto registrationDto) {
        if (this.registrations == null)
            this.registrations = new ArrayList<>();
        this.registrations.add(registrationDto);
    }

    public void setRegistrationList(List<RegistrationWorkflow> registrations) {
        this.registrations = new ArrayList<>();
        for (RegistrationWorkflow registrationWorkflow : registrations) {
            this.registrations.add(new RegistrationDto(registrationWorkflow));
        }
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean hasError() {
        return !StringUtils.isBlank(this.error);
    }

    public ApiOptionsDto getOptions() {
        return options;
    }

    public void setOptions(ApiOptionsDto options) {
        this.options = options;
    }

    public int getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(int httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }


    @XmlAccessorType(XmlAccessType.NONE)
    @XmlRootElement(name="registration")
    @XmlType(propOrder = {"id", "status", "datacollection", "payload", "createdOn", "updatedOn", "validationErrors"})
    static public class RegistrationDto {

        @XmlElement
        private Long id;
        @XmlElement
        private WorkflowStatus status;
        @XmlElement
        private DataCollectionDto datacollection;
        @XmlAnyElement(lax = true)
        private Object payload;
        @XmlJavaTypeAdapter(SqlTimestampAdapter.class)
        private Timestamp createdOn;
        @XmlJavaTypeAdapter(SqlTimestampAdapter.class)
        private Timestamp updatedOn;
        @XmlElementWrapper
        @XmlElement(name="validationError")
        private List<ValidationErrorDto> validationErrors;


        public RegistrationDto() {
            this.id = 0L;
        }

        public RegistrationDto(RegistrationWorkflow registrationWorkflow) {
            this.id = registrationWorkflow.getId();
            this.status = registrationWorkflow.getStatus();
            this.datacollection = new DataCollectionDto();
            this.datacollection.setUri(registrationWorkflow.getDataCollectionDefinitionId());
            this.datacollection.setName(registrationWorkflow.getDataCollectionName());
            this.payload = null; // v2
            this.createdOn = registrationWorkflow.getCreatedOn();
            this.updatedOn = registrationWorkflow.getUpdatedOn();
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public WorkflowStatus getStatus() {
            return status;
        }

        public void setStatus(WorkflowStatus status) {
            this.status = status;
        }

        public DataCollectionDto getDataCollection() {
            return datacollection;
        }

        public void setDataCollection(DataCollectionDto dataCollection) {
            this.datacollection = dataCollection;
        }

        public Object getPayload() {
            return payload;
        }

        public void setPayload(Object payload) {
            this.payload = payload;
        }

        public Timestamp getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(Timestamp createdOn) {
            this.createdOn = createdOn;
        }

        public Timestamp getUpdatedOn() {
            return updatedOn;
        }

        public void setUpdatedOn(Timestamp updatedOn) {
            this.updatedOn = updatedOn;
        }

        public List<ValidationErrorDto> getValidationErrors() {
            return validationErrors;
        }

        public void setValidationErrors(List<ValidationErrorDto> validationErrors) {
            this.validationErrors = validationErrors;
        }

        public boolean hasValidationErrors() {
            return this.getValidationErrors() != null && this.getValidationErrors().size() > 0;
        }

        public void addValidationError(ValidationErrorDto validationErrorDto) {
            if (this.validationErrors == null)
                this.validationErrors = new ArrayList<>();
            this.validationErrors.add(validationErrorDto);
        }

    }

    @XmlAccessorType(XmlAccessType.NONE)
    @XmlRootElement(name="datacollection")
    static public class DataCollectionDto {

        private static String baseURI = "https://catalogue.healthdata.be/healthdata_catalogue" + "/datacollectiondefinitions/";

        @XmlElement
        private String uri;
        @XmlElement
        private String name;
        @XmlElement
        private String version;
        @XmlJavaTypeAdapter(SqlTimestampAdapter.class)
        private Timestamp startDate;
        @XmlJavaTypeAdapter(SqlTimestampAdapter.class)
        private Timestamp endDateCreation;
        @XmlJavaTypeAdapter(SqlTimestampAdapter.class)
        private Timestamp endDateSubmission;
        @XmlJavaTypeAdapter(SqlTimestampAdapter.class)
        private Timestamp endDateComments;


        public DataCollectionDto() {

        }

        public DataCollectionDto(DataCollectionDefinitionDtoV7 dcdDtoV7) {
            this.uri = DataCollectionDto.baseURI + dcdDtoV7.getId();
            this.name = dcdDtoV7.getDataCollectionName();
            this.version = dcdDtoV7.getVersion();
            this.startDate = dcdDtoV7.getDataCollectionGroup().getStartDate();
            this.endDateCreation = dcdDtoV7.getDataCollectionGroup().getEndDateCreation();
            this.endDateSubmission = dcdDtoV7.getDataCollectionGroup().getEndDateSubmission();
            this.endDateComments = dcdDtoV7.getDataCollectionGroup().getEndDateComments();
        }

        public static void setCatalogueHost(String catalogueHost) {
            DataCollectionDto.baseURI = catalogueHost + "/datacollectiondefinitions/";
        }

        public static String getBaseURI() {
            return DataCollectionDto.baseURI;
        }

        public String getUri() {
            return uri;
        }

        public void setUri(String uri) {
            this.uri = uri;
        }

        public void setUri(Long dcdId) {
            this.uri = DataCollectionDto.baseURI + dcdId;
        }

        public void setUri(String catalogueHost, Long dcdId) {
            setCatalogueHost(catalogueHost);
            this.uri = DataCollectionDto.baseURI + dcdId;
        }

        public Long getId(String catalogueHost) {
            setCatalogueHost(catalogueHost);
            return getId();
        }

        public Long getId() {
            if (this.uri.startsWith(DataCollectionDto.baseURI)) {
                String id = this.uri.substring(DataCollectionDto.baseURI.length());
                try {
                    return Long.parseLong(id);
                }
                catch (NumberFormatException e) {
                    return null;
                }
            }
            return null;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public Timestamp getStartDate() {
            return startDate;
        }

        public void setStartDate(Timestamp startDate) {
            this.startDate = startDate;
        }

        public Timestamp getEndDateCreation() {
            return endDateCreation;
        }

        public void setEndDateCreation(Timestamp endDateCreation) {
            this.endDateCreation = endDateCreation;
        }

        public Timestamp getEndDateSubmission() {
            return endDateSubmission;
        }

        public void setEndDateSubmission(Timestamp endDateSubmission) {
            this.endDateSubmission = endDateSubmission;
        }

        public Timestamp getEndDateComments() {
            return endDateComments;
        }

        public void setEndDateComments(Timestamp endDateComments) {
            this.endDateComments = endDateComments;
        }
    }

}
