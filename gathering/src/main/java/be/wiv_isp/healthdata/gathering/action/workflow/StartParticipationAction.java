/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.action.workflow;

import be.wiv_isp.healthdata.common.caching.CacheManagementService;
import be.wiv_isp.healthdata.common.caching.ICacheManagementService;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupDto;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.search.ParticipationWorkflowSearch;
import be.wiv_isp.healthdata.gathering.service.IParticipationWorkflowService;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionGroupForwardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

public class StartParticipationAction extends AbstractParticipationWorkflowAction {

	private static final Logger LOG = LoggerFactory.getLogger(StartParticipationAction.class);

	@Autowired
	private IParticipationWorkflowService workflowService;
	@Autowired
	private IDataCollectionGroupForwardService catalogueService;
	@Autowired
	private ICacheManagementService cacheManagementService;

	@Override
	public WorkflowActionType getAction() {
		return WorkflowActionType.START;
	}

	@Override
	public ParticipationWorkflow doExecute(ParticipationWorkflow workflow, HDUserDetails userDetails) {
		final DataCollectionGroupDto group = catalogueService.get(dataCollectionDefinitionId);
		if (workflow == null) {
			LOG.debug("Workflow does not exist, creating new workflow.");
			if (!userDataCollectionService.isUserAuthorized(userDetails, group.getName())) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.ACTION_NOT_ALLOWED, userDetails.getUsername(), "NEW");
				throw exception;
			}
			if (!group.isValidForCreation(new Date())) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.INVALID_DATE_FOR_DATA_COLLECTION_DEFINITION, dataCollectionDefinitionId, "creation");
				throw exception;
			}
			ParticipationWorkflowSearch search = new ParticipationWorkflowSearch();
			search.setDataCollectionDefinitionId(dataCollectionDefinitionId);
			search.setOrganization(userDetails.getOrganization());
			search.setWithDocument(null);
			long participationCount = workflowService.count(search);
			if (participationCount != 0L) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.CREATE_DUPLICATE_KEY, dataCollectionDefinitionId);
				throw exception;
			}
			workflow = workflowService.create(userDetails.getOrganization());

			workflow.setDataCollectionDefinitionId(group.getId());
			workflow.setDataCollectionName(group.getName());
		}
		workflow.setCompleted(false);
		workflow.setCompletedOn(null);

		if(workflow.getStatus() == null) {
			setEndStatus(WorkflowStatus.IN_PROGRESS);
		} else {
			setEndStatus(workflow.getStatus());
		}
		workflowService.sendStart(workflow, group);
		cacheManagementService.evict(CacheManagementService.dataCollectionGroupCache);
		return workflow;
	}
}
