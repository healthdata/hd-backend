/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.gathering.dto;

import be.wiv_isp.healthdata.orchestration.domain.TypedValue;
import be.wiv_isp.healthdata.common.json.deserializer.JsonToByteArrayDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.ByteArrayToJsonSerializer;
import be.wiv_isp.healthdata.gathering.domain.RegistrationDocument;
import be.wiv_isp.healthdata.orchestration.dto.AbstractRegistrationDocumentDto;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.Map;


public class RegistrationDocumentDto extends AbstractRegistrationDocumentDto {

    public RegistrationDocumentDto() {

    }

    public RegistrationDocumentDto(RegistrationDocument document) {
        super(document);
        privateContent = document.getPrivateContent();
    }

    @JsonSerialize(using = ByteArrayToJsonSerializer.class)
    @JsonDeserialize(using = JsonToByteArrayDeserializer.class)
    private byte[] privateContent;

    public byte[] getPrivateContent() {
        return privateContent;
    }

    public void setPrivateContent(byte[] privateContent) {
        this.privateContent = privateContent;
    }

    @Override
    @JsonIgnore // do not return on HD4DP
    public Map<String, TypedValue> getCodedContent() {
        return codedContent;
    }

}
