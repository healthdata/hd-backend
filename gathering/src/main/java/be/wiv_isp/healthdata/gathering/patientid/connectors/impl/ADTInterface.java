/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.patientid.connectors.impl;

import be.wiv_isp.healthdata.adt_connector.AdtConnector;
import be.wiv_isp.healthdata.adt_connector.domain.Request;
import be.wiv_isp.healthdata.adt_connector.domain.Response;
import be.wiv_isp.healthdata.adt_connector.exception.AdtConnectorException;
import be.wiv_isp.healthdata.orchestration.domain.mapper.IntegerConfigurationMapper;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.NationalRegisterConnector;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.dto.PersonDto;
import be.wiv_isp.healthdata.gathering.patientid.connectors.INationalRegisterConnector;
import be.wiv_isp.healthdata.gathering.patientid.mapper.IPersonDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ADTInterface implements INationalRegisterConnector {

	@Autowired
	private IPersonDtoMapper<Response> ADTResponseMapper;

	@Autowired
	private IConfigurationService configurationService;

	@Override
	public PersonDto getBySsinNumber(String inss, Organization organization) {
		return process(inss, organization);
	}

	@Override
	public PersonDto getByInternalNumber(String internalId, Organization organization) {
		return process(internalId, organization);
	}

	@Override
	public NationalRegisterConnector getNationalRegisterConnector() {
		return NationalRegisterConnector.ADT;
	}

	private PersonDto process(String patientIdentifier, Organization organization) {
		final Request request = new Request();
		request.setPatientIdentifier(patientIdentifier);
		request.setHost(configurationService.get(ConfigurationKey.ADT_SERVICE_HOST, organization).getValue());
		request.setPort(IntegerConfigurationMapper.map(configurationService.get(ConfigurationKey.ADT_SERVICE_PORT, organization)));

		Response response;
		try {
			response = new AdtConnector().execute(request);
		} catch (AdtConnectorException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.ADT, e.getMessage());
			throw exception;
		}
		return ADTResponseMapper.map(response);
	}
}
