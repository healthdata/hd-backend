/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.tasks;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.search.OrganizationSearch;
import be.wiv_isp.healthdata.orchestration.util.FileSystemUtils;
import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.gathering.domain.FastTrackRecord;
import be.wiv_isp.healthdata.gathering.domain.FastTrackUpload;
import be.wiv_isp.healthdata.gathering.domain.enumeration.FastTrackStatus;
import be.wiv_isp.healthdata.gathering.service.IFastTrackService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionDefinitionForwardService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import be.wiv_isp.healthdata.orchestration.tasks.SchedulableHealthDataTask;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.*;

@Service
public class ProvisioningTask extends SchedulableHealthDataTask {

	private static final Logger LOG = LoggerFactory.getLogger(ProvisioningTask.class);

	private static final String CSV_EXTENSION = ".csv";

	@Autowired
	private IFastTrackService fastTrackService;
	@Autowired
	private IOrganizationService organizationService;
	@Autowired
	private IDataCollectionDefinitionForwardService catalogueService;
	@Autowired
	private IDataCollectionService dataCollectionService;

	@Override
	public String getName() {
		return "PROVISIONING";
	}

	@Override
	protected String getTimingExpression() {
		return configurationService.get(ConfigurationKey.SCHEDULED_TASK_PROVISIONING_INTERVAL).getValue();
	}

	@Override
	public void execute() {
		final String directory = configurationService.get(ConfigurationKey.PROVISIONING_DIRECTORY).getValue();
		setupDirectories(directory);
		final File[] folders = new File(directory).listFiles((FileFilter) DirectoryFileFilter.DIRECTORY);

		if (folders == null) {
			LOG.error(MessageFormat.format("Error while reading files from directory [{0}]", directory));
		} else if (folders.length != 0) {
			Organization organization = organizationService.getMain();
			if (organization != null) {
				for (File folder : folders) {
					processFolder(folder, organization, true);
				}
			}
		}
	}

	private void setupDirectories(String directory) {
		List<Organization> allOrganizations = organizationService.getAll();
		Set<String> subFolders = new HashSet<>();
		Map<String, Set<String>> folderStructure = new HashMap<>();
		for (Organization organization : allOrganizations) {
			if(organization.isMain()) {
				subFolders.add("ERROR");
				subFolders.add("ARCHIVE");
				subFolders.addAll(dataCollectionService.getDataCollectionsFlat(organization));
			} else if(!organization.isDeleted()) {
				subFolders.add(organization.getHealthDataIDValue());
				Set<String> subSubFolders = new HashSet<>();
				subSubFolders.addAll(dataCollectionService.getDataCollectionsFlat(organization));
				subSubFolders.add("ERROR");
				subSubFolders.add("ARCHIVE");
				folderStructure.put(organization.getHealthDataIDValue(), subSubFolders);
			}
		}
		setupDirectories(new File(directory), subFolders, folderStructure);
	}

	private void setupDirectories(File directory, Set<String> subFolders, Map<String, Set<String>> subFolderStructure) {
		for (String subFolder : subFolders) {
			File subDirectory = getOrCreateDirectory(directory, subFolder);
			Set<String> subSubFolders = subFolderStructure.get(subFolder);
			if(subSubFolders != null) {
				for (String subSubFolder : subSubFolders) {
					getOrCreateDirectory(subDirectory, subSubFolder);
				}
			}
		}
	}

	private void processFolder(File folder, Organization organization, boolean processNextLevel) {
		if ("ERROR".equalsIgnoreCase(folder.getName())) {
			return;
		}
		if ("ARCHIVE".equalsIgnoreCase(folder.getName())) {
			return;
		}
		if (isRegister(folder.getName(), organization)) {
			File[] csvFiles = folder.listFiles((FilenameFilter) new SuffixFileFilter(CSV_EXTENSION));
			if (csvFiles == null) {
				LOG.error(MessageFormat.format("Error while reading files from directory [{0}]", folder.getAbsolutePath()));
			} else if (csvFiles.length != 0) {
				LOG.info(MessageFormat.format("{0} files found in {1}", csvFiles.length, folder.getAbsolutePath()));
				for (File csvFile : csvFiles) {
					try {
						if (!csvFile.canWrite()) {
							LOG.error(MessageFormat.format("File[{0}]: no permission to delete the file so it is skipped.", csvFile.getName()));
							continue;
						}
						DataCollectionDefinitionDtoV7 dataCollectionDefinition = catalogueService.getLatest(folder.getName(), organization);
						if (dataCollectionDefinition != null) {
							FastTrackUpload upload = new FastTrackUpload();
							upload.setOrganizationId(organization.getId());
							upload.setMode(FastTrackUpload.Mode.MERGE);
							upload.setMaster(FastTrackUpload.Master.CSV);
							upload.setDataCollectionDefinitionId(dataCollectionDefinition.getId());
							upload.setFileName(csvFile.getName());
							upload.setPath(csvFile.getParentFile().getAbsolutePath());
							FileInputStream fis = FileUtils.openInputStream(csvFile);
							upload.setContent(IOUtils.toString(new BOMInputStream(fis), StandardCharsets.UTF_8));
							fis.close();
							Date startTime = new Date();
							upload.setStartedOn(new Timestamp(startTime.getTime()));
							fastTrackService.createUpload(upload);
							fastTrackService.upload(upload);
							Date endTime = new Date();
							upload.setStoppedOn(new Timestamp(endTime.getTime()));

							fastTrackService.updateUpload(upload);
							processResponse(folder, csvFile, upload);
						}

					} catch (Exception e) {
						LOG.error(MessageFormat.format("File[{0}]: file unsuccessfully processed, moving file to error directory.", csvFile.getName()), e);
						FileSystemUtils.moveFilesToDirectory(getOrCreateErrorDirectory(folder.getParentFile()), true, csvFile);
					}
				}
			}
			return;
		}
		Organization subOrganization = getOrganisation(folder.getName());
		if (subOrganization != null) {
			if (processNextLevel) {
				File[] subfolders = folder.listFiles((FileFilter) DirectoryFileFilter.DIRECTORY);
				if (subfolders == null) {
					LOG.error(MessageFormat.format("Error while reading files from directory [{0}]", folder.getAbsolutePath()));
				} else if (subfolders.length != 0) {
					for (File subFolder : subfolders) {
						processFolder(subFolder, subOrganization, false);
					}
				}
			}
		}
	}

	private void processResponse(final File folder, final File csvFile, final FastTrackUpload upload) throws IOException {
		if (StringUtils.isNoneBlank(upload.getErrors())) {
			LOG.warn(MessageFormat.format("File[{0}]: file contains errors. Moving file to error directory.", csvFile.getName()));

			final StringBuilder sb = new StringBuilder();
			Map<Long, FastTrackRecord> records = upload.getRecords();
			for (FastTrackRecord record : records.values()) {
				if (FastTrackStatus.ERROR.equals(record.getStatus())) {
					sb.append(record).append("\n");
				}
			}

			final File reportFile = new File(csvFile.getParentFile(), MessageFormat.format("{0}_report.txt", FilenameUtils.getBaseName(csvFile.getName())));
			FileUtils.writeStringToFile(reportFile, sb.toString(), StandardCharsets.UTF_8);

			FileSystemUtils.moveFilesToDirectory(getOrCreateErrorDirectory(folder.getParentFile()), true, FastTrackTask.getFilesToMove(csvFile, upload));
			FileSystemUtils.moveFilesToDirectory(getOrCreateErrorDirectory(folder.getParentFile()), true, reportFile);
		} else {
			LOG.info(MessageFormat.format("File[{0}]: file successfully processed, moving file to archive directory.", csvFile.getName()));
			FileSystemUtils.moveFilesToDirectory(getOrCreateArvhiveDirectory(folder.getParentFile()), true, FastTrackTask.getFilesToMove(csvFile, upload));
		}
	}

	private boolean isRegister(String name, Organization organization) {
		DataCollectionDefinitionDtoV7 validForCreation = catalogueService.getValidForCreation(name, organization);
		return validForCreation != null;
	}

	private File getOrCreateErrorDirectory(File folder) {
		return getOrCreateDirectory(folder, "ERROR");
	}

	private File getOrCreateArvhiveDirectory(File folder) {
		return getOrCreateDirectory(folder, "ARCHIVE");
	}

	private File getOrCreateDirectory(File folder, String directoryName) {
		for (File file : folder.listFiles()) {
			if (file.isDirectory() && directoryName.equalsIgnoreCase(file.getName())) {
				return file;
			}
		}
		final File direcotory = Paths.get(folder.getAbsolutePath(), directoryName).toFile();
		if (!direcotory.exists()) {
			direcotory.mkdir();
		}
		return direcotory;
	}

	private Organization getOrganisation(String value) {
		OrganizationSearch search = new OrganizationSearch();
		search.setHealthDataIDValue(value);
		List<Organization> organizations = organizationService.getAll(search);
		if (organizations.size() != 1) {
			return null;
		} else {
			return organizations.get(0);
		}
	}

}