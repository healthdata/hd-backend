/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.dto;

import be.wiv_isp.healthdata.gathering.domain.MappingContext;
import be.wiv_isp.healthdata.gathering.domain.RegistrationDocument;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.service.impl.RegistrationWorkflowService;
import be.wiv_isp.healthdata.orchestration.dto.AbstractRegistrationWorkflowDto;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class RegistrationWorkflowDto extends AbstractRegistrationWorkflowDto<RegistrationWorkflow, RegistrationDocument, MappingContext> {

	private boolean submittedOnce;
	private boolean deletable;
	protected String registryDependentId;

	public RegistrationWorkflowDto() {

	}

	public RegistrationWorkflowDto(RegistrationWorkflow workflow) {
		super(workflow);
		if (workflow.getDocument().getPrivateContent() != null)
			metaData = null; // do not return metadata
		setSubmittedOnce(workflow.getSubmittedOn() != null);
		setDeletable(RegistrationWorkflowService.isDeletable(workflow));
		setRegistryDependentId(workflow.getRegistryDependentId());
	}

	public RegistrationWorkflowDto(RegistrationWorkflowDto dto) {
		super(dto);
		submittedOnce = dto.isSubmittedOnce();
		deletable = dto.isDeletable();
		registryDependentId = dto.getRegistryDependentId();
	}

	public boolean isSubmittedOnce() {
		return submittedOnce;
	}

	public void setSubmittedOnce(boolean submittedOnce) {
		this.submittedOnce = submittedOnce;
	}

	public boolean isDeletable() {
		return deletable;
	}

	public void setDeletable(boolean deletable) {
		this.deletable = deletable;
	}

	public String getRegistryDependentId() {
		return registryDependentId;
	}

	public void setRegistryDependentId(String registryDependentId) {
		this.registryDependentId = registryDependentId;
	}
}