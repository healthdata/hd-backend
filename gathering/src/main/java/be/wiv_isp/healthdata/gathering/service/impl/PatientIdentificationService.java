/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.NationalRegisterConnector;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.gathering.dto.PersonDto;
import be.wiv_isp.healthdata.gathering.patientid.connectors.INationalRegisterConnector;
import be.wiv_isp.healthdata.gathering.patientid.connectors.impl.ADTInterface;
import be.wiv_isp.healthdata.gathering.patientid.connectors.impl.CustomRestNrcInterface;
import be.wiv_isp.healthdata.gathering.patientid.connectors.impl.RRNConnectorInterface;
import be.wiv_isp.healthdata.gathering.patientid.connectors.impl.XConnectInterface;
import be.wiv_isp.healthdata.gathering.service.IINSSService;
import be.wiv_isp.healthdata.gathering.service.IPatientIdentificationService;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

@Service
public class PatientIdentificationService implements IPatientIdentificationService {

	private static final Logger LOG = LoggerFactory.getLogger(PatientIdentificationService.class);

	@Autowired
	private IINSSService inssService;
	@Autowired
	private IConfigurationService configurationService;
	@Autowired
	private IOrganizationService organizationService;
	@Autowired
	private RRNConnectorInterface rrnConnectorInterface;
	@Autowired
	private CustomRestNrcInterface customRestNrcInterface;
	@Autowired
	private XConnectInterface xConnectInterface;
	@Autowired
	private ADTInterface adtInterface;

	private Map<NationalRegisterConnector, INationalRegisterConnector> connectors;

	@PostConstruct
	public void init() {		
		connectors = new HashMap<>();
		
		connectors.put(customRestNrcInterface.getNationalRegisterConnector(), customRestNrcInterface);
		connectors.put(rrnConnectorInterface.getNationalRegisterConnector(), rrnConnectorInterface);
		connectors.put(adtInterface.getNationalRegisterConnector(), adtInterface);
		connectors.put(xConnectInterface.getNationalRegisterConnector(), xConnectInterface);
	}

	@Override
	public PersonDto get(String patientId) {
		LOG.debug(MessageFormat.format("Lookup patient info with for patient id [{0}]", patientId));
		PersonDto person;
		try {
			Organization organization;
			if(AuthenticationService.isAuthenticated()) {
				final HDUserDetails principal = AuthenticationService.getUserDetails();
				organization = principal.getOrganization();
			} else {
				organization = organizationService.getMain();
			}
			final NationalRegisterConnector nationalRegisterConnector = getConnector(organization);
			final INationalRegisterConnector connector = connectors.get(nationalRegisterConnector);

			if (inssService.validate(patientId)) {
				person = lookupBySsin(patientId, organization, connector);
			} else {
				person = lookupByInternalId(patientId, organization, connector);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
			LOG.trace(e.getMessage(), e);
			person = new PersonDto();
		}
		
		LOG.debug(MessageFormat.format("Returning: [{0}]", person));
		return person;
	}
	
	private PersonDto lookupBySsin(String patientIdentifier, final Organization organization, final INationalRegisterConnector connector) {
		LOG.debug("Lookup patient by SSIN number");
		
		PersonDto person = connector.getBySsinNumber(patientIdentifier, organization);
		
		if (person == null) {
			person = new PersonDto();
			person.setSsin(patientIdentifier);
		}

		return person;
	}

	private PersonDto lookupByInternalId(String patientIdentifier, final Organization organization, final INationalRegisterConnector connector) {
		LOG.debug("Lookup patient by internal number");

		PersonDto person = connector.getByInternalNumber(patientIdentifier, organization);
		
		if (person == null) {
			person = new PersonDto();
			person.setInternalId(patientIdentifier);
		}

		return person;
	}

	private NationalRegisterConnector getConnector(Organization organization) {
		LOG.debug("Retrieving national register connector type from configuration");

		final Configuration configuration = configurationService.get(ConfigurationKey.NATIONAL_REGISTER_CONNECTOR, organization);
		final NationalRegisterConnector connectorType = NationalRegisterConnector.valueOf(configuration.getValue());

		LOG.debug(MessageFormat.format("National register connector type: {0}", connectorType));

		return connectorType;
	}
}
