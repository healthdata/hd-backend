/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.gathering.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.gathering.dao.IUpgradeDao;
import be.wiv_isp.healthdata.gathering.domain.Upgrade;
import be.wiv_isp.healthdata.gathering.domain.search.UpgradeSearch;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UpgradeDao extends CrudDaoV2<Upgrade, Long, UpgradeSearch> implements IUpgradeDao {

    public UpgradeDao() {
        super(Upgrade.class);
    }

    @Override
    protected List<Predicate> getPredicates(UpgradeSearch search, CriteriaBuilder cb, Root<Upgrade> rootEntry) {
        final List<Predicate> predicates = new ArrayList<>();
        if (search.getVersion() != null) {
            predicates.add(cb.equal(rootEntry.get("version"), search.getVersion()));
        }
        if (search.getFrontendCommit() != null) {
            predicates.add(cb.equal(rootEntry.get("frontendCommit"), search.getFrontendCommit()));
        }
        if (search.getBackendCommit() != null) {
            predicates.add(cb.equal(rootEntry.get("backendCommit"), search.getBackendCommit()));
        }
        if (search.getMappingCommit() != null) {
            predicates.add(cb.equal(rootEntry.get("mappingCommit"), search.getMappingCommit()));
        }
        return predicates;
    }
}
