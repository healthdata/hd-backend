/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.gathering.dao.IStableDataDao;
import be.wiv_isp.healthdata.gathering.domain.StableData;
import be.wiv_isp.healthdata.gathering.domain.search.StableDataCountGroupByOrganizationDataCollection;
import be.wiv_isp.healthdata.gathering.domain.search.StableDataSearch;
import be.wiv_isp.healthdata.gathering.dto.StableDataDto;
import be.wiv_isp.healthdata.gathering.service.IDataCollectionService;
import be.wiv_isp.healthdata.gathering.service.IStableDataService;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.service.IUserService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Service
public class StableDataService implements IStableDataService {

    private static final Logger LOG = LoggerFactory.getLogger(StableDataService.class);

    @Autowired
    private IStableDataDao stableDataDao;
    @Autowired
    private IDataCollectionService dataCollectionService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IOrganizationService organizationService;

    @Override
    public Response get(UriInfo info) {
        String patientId = getPatientId(info);
        String dataCollectionName = getDataCollectionName(info);
        LOG.debug("Request received for stable data with patient id [{}] and dataCollectionName [{}]", patientId, dataCollectionName);

        Organization organization;
        if(AuthenticationService.isAuthenticated()) {
            final HDUserDetails principal = AuthenticationService.getUserDetails();
            organization = principal.getOrganization();
            User user = userService.get(principal.getUsername(), organization);
            if (!user.isAccessGranted(dataCollectionName)) {
                HealthDataException exception = new HealthDataException();
                exception.setExceptionType(ExceptionType.GENERAL_UNAUTHORIZED);
                throw exception;
            }
        } else {
            organization = organizationService.getMain();
        }
        if(!dataCollectionService.isAccessGranted(organization, dataCollectionName)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_UNAUTHORIZED);
            throw exception;
        }

        final StableDataSearch search = new StableDataSearch();
        search.setPatientId(patientId);
        search.setDataCollectionName(dataCollectionName);
        search.setOrganization(organization);
        final StableData stableData = stableDataDao.getUnique(search);
        if(stableData == null) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "stableData", patientId);
            throw exception;
        }
        return Response.ok(StableDataDto.convert(stableData)).build();
    }

    @Override
    @Transactional
    public StableData create(StableData stableData) {
        return stableDataDao.create(stableData);
    }

    @Override
    public List<StableDataCountGroupByOrganizationDataCollection> getCountGroupByOrganizationDataCollection() {
        return stableDataDao.getCountGroupByOrganizationDataCollection();
    }

    private String getPatientId(UriInfo info) {
        return RestUtils.getPathString(info, "patientId");
    }

    private String getDataCollectionName(UriInfo info) {
        return RestUtils.getPathString(info, "dataCollectionName");
    }

}
