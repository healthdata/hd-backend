/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.action.workflow;

import be.wiv_isp.healthdata.common.dto.DataCollectionGroupDto;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.domain.mapper.NoteMessageDtoMapper;
import be.wiv_isp.healthdata.orchestration.factory.IAbstractMessageFactory;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionGroupForwardService;
import be.wiv_isp.healthdata.orchestration.service.IIdentificationService;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import be.wiv_isp.healthdata.orchestration.service.INoteService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

public class SendParticipationAction extends AbstractParticipationWorkflowAction {

	private static final Logger LOG = LoggerFactory.getLogger(SendParticipationAction.class);

	@Autowired
	private IIdentificationService identificationService;
	@Autowired
	private IDataCollectionGroupForwardService catalogueService;
	@Autowired
	private IMessageService messageService;
	@Autowired
	private IAbstractMessageFactory<RegistrationWorkflow, ParticipationWorkflow> messageFactory;
	@Autowired
	protected INoteService noteService;

	@Override
	public WorkflowActionType getAction() {
		return WorkflowActionType.SEND;
	}

	@Override
	public String getSendStatus() {
		return SendStatus.PENDING;
	}

	@Override
	public ParticipationWorkflow doExecute(ParticipationWorkflow workflow, HDUserDetails userDetails) {
		try {
			Long dataCollectionDefinitionId = workflow.getDataCollectionDefinitionId();
			DataCollectionGroupDto group = catalogueService.get(dataCollectionDefinitionId);
			if (workflow.getSubmittedOn() == null) {
				if (!group.isValidForSubmission(new Date())) {
					HealthDataException exception = new HealthDataException();
					exception.setExceptionType(ExceptionType.INVALID_DATE_FOR_DATA_COLLECTION_DEFINITION, dataCollectionDefinitionId, "submit");
					throw exception;
				}
			} else {
				if (!group.isValidForComments(new Date())) {
					HealthDataException exception = new HealthDataException();
					exception.setExceptionType(ExceptionType.INVALID_DATE_FOR_DATA_COLLECTION_DEFINITION, dataCollectionDefinitionId, "comments");
					throw exception;
				}
			}

			final CreateForReviewParticipationAction action = new CreateForReviewParticipationAction();
			action.setDataCollectionDefinitionId(workflow.getDataCollectionDefinitionId());
			action.setDocumentContent(workflow.getDocument().getDocumentContent());
			action.setIdentificationWorkflow(identificationService.getHealthDataIdentification(workflow.getOrganization()));
			action.setDocumentVersion(workflow.getDocument().getVersion());

			action.setNoteMaxOrder(workflow.getDocument().getNoteMaxOrder());
			List<Note> notes = noteService.getByDocumentId(WorkflowType.REGISTRATION, workflow.getDocument().getId());
			action.setNotesMessage(NoteMessageDtoMapper.mapToNoteDtos(notes));


			List<ParticipationWorkflowHistory> history = workflow.getHistory();
			for (ParticipationWorkflowHistory workflowHistory : history) {
				if (StringUtils.isNotBlank(workflowHistory.getSource())) {
					action.setSource(workflowHistory.getSource());
					action.setFile(workflowHistory.getFile());
					action.setMessageType(workflowHistory.getMessageType());
					break;
				}
			}

			final Message message = messageFactory.createOutgoingMessage(action, workflow);
			messageService.create(message);

			if(workflow.getSubmittedOn() == null) {
				workflow.setSubmittedOn(new Timestamp(new Date().getTime()));
			}

			setEndStatus(WorkflowStatus.SUBMITTED);
		} catch (Exception e) {
			LOG.error(MessageFormat.format("error while submitting worklfow: {0}", e.getMessage()), e);
		}
		return workflow;
	}


	@Override
	public WorkflowType getWorkflowType() {
		return WorkflowType.PARTICIPATION;
	}
}
