/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.domain;

import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationDocument;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "DOCUMENTS")
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegistrationDocument extends AbstractRegistrationDocument<RegistrationWorkflow> {

    @Lob
    @Column(name = "PRIVATE_CONTENT", nullable = true)
    protected byte[] privateContent;


    public byte[] getPrivateContent() {
        return privateContent;
    }

    public void setPrivateContent(byte[] privateContent) {
        this.privateContent = privateContent;
    }

    @Override
    public void setDocumentData(DocumentData documentData) {
        setDocumentContent(documentData.getContent());
        setCodedContent(documentData.getCoded());
        setPrivateContent(documentData.getPrivate());
    }

}
