/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.domain.search;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.orchestration.domain.search.AbstractUserRequestSearch;

import javax.ws.rs.core.UriInfo;
import java.util.Objects;

public class UserRequestSearch extends AbstractUserRequestSearch {

    private String username;
    private Long organizationId;

    public UserRequestSearch() {

    }

    public UserRequestSearch(UriInfo info, Organization organization) {
        super(info);
        username = RestUtils.getParameterString(info, "username");
        organizationId = organization.getId();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof UserRequestSearch) {
            UserRequestSearch other = (UserRequestSearch) o;

            return Objects.equals(firstName, other.firstName)
                    && Objects.equals(lastName, other.lastName)
                    && Objects.equals(email, other.email)
                    && Objects.equals(approved, other.approved)
                    && Objects.equals(username, other.username)
                    && Objects.equals(organizationId, other.organizationId);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                this.firstName,
                this.lastName,
                this.email,
                this.approved,
                this.username,
                this.organizationId);
    }

    @Override
    public String toString() {
        return "UserRequestSearch {" +
                "firstName = " + Objects.toString(this.firstName) + ", " +
                "lastName = " + Objects.toString(this.lastName) + ", " +
                "email = " + Objects.toString(this.email) + ", " +
                "approved = " + Objects.toString(this.approved) + ", " +
                "username = " + Objects.toString(this.username) + ", " +
                "organizationId = " + Objects.toString(this.organizationId) + "}";
    }

}
