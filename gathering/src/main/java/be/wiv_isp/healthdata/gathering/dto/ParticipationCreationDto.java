/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.dto;

import be.wiv_isp.healthdata.orchestration.domain.Organization;

import java.util.Objects;

public class ParticipationCreationDto {

    private Long dataCollectionDefinitionId;

    private String dataCollectionName;

    private Organization organization;

    public ParticipationCreationDto(Long dataCollectionDefinitionId, String dataCollectionName, Organization organization) {
        this.dataCollectionDefinitionId = dataCollectionDefinitionId;
        this.dataCollectionName = dataCollectionName;
        this.organization = organization;
    }

    public Long getDataCollectionDefinitionId() {
        return dataCollectionDefinitionId;
    }

    public void setDataCollectionDefinitionId(Long dataCollectionDefinitionId) {
        this.dataCollectionDefinitionId = dataCollectionDefinitionId;
    }

    public String getDataCollectionName() {
        return dataCollectionName;
    }

    public void setDataCollectionName(String dataCollectionName) {
        this.dataCollectionName = dataCollectionName;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof ParticipationCreationDto) {
            ParticipationCreationDto other = (ParticipationCreationDto) o;

            return Objects.equals(dataCollectionDefinitionId, other.dataCollectionDefinitionId)
                    && Objects.equals(dataCollectionName, other.dataCollectionName)
                    && Objects.equals(organization, other.organization);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                this.dataCollectionDefinitionId,
                this.dataCollectionName,
                this.organization);
    }

    @Override
    public String toString() {
        return "ParticipationCreationDto {" +
                "dataCollectionDefinitionId = " + Objects.toString(this.dataCollectionDefinitionId) + ", " +
                "dataCollectionName = " + Objects.toString(this.dataCollectionName) + ", " +
                "organization = " + Objects.toString(this.organization) + "}";
    }
}
