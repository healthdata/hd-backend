/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.dto;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.gathering.domain.Registration;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class RegistrationResponseDtoV1 {

	private int httpStatus;

	private String errorMessage;

	private String validationResponse;

	private final Map<DataCollectionDefinitionDtoV7, List<Registration>> registrations = new LinkedHashMap<>();

	public int getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	public Map<DataCollectionDefinitionDtoV7, List<Registration>> getRegistrations() {
		return registrations;
	}

	public List<Registration> getRegistrations(DataCollectionDefinitionDtoV7 dcd) {
		return registrations.get(dcd);
	}

	public void addRegistration(DataCollectionDefinitionDtoV7 dcd, Registration registration) {
		List<Registration> list = registrations.get(dcd);
		if (list == null) {
			list = new ArrayList<>();
			registrations.put(dcd, list);
		}
		list.add(registration);
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getValidationResponse() {
		return validationResponse;
	}

	public void setValidationResponse(String validationResponse) {
		this.validationResponse = validationResponse;
	}
}
