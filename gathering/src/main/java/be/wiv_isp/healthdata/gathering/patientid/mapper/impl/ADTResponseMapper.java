/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.patientid.mapper.impl;

import be.wiv_isp.healthdata.adt_connector.domain.Response;
import be.wiv_isp.healthdata.gathering.dto.PersonDto;
import be.wiv_isp.healthdata.gathering.patientid.mapper.IPersonDtoMapper;
import org.springframework.stereotype.Service;

@Service
public class ADTResponseMapper implements IPersonDtoMapper<Response> {

	@Override
	public PersonDto map(Response response) {
		final PersonDto personDto = new PersonDto();

		personDto.setSsin(response.getSsin());
		personDto.setInternalId(response.getInternalId());
		personDto.setLastName(response.getLastName());
		personDto.setFirstName(response.getFirstName());
		switch (response.getGender()) {
		case "M":
			personDto.setGender(PersonDto.Gender.MALE);
			break;
		case "F":
			personDto.setGender(PersonDto.Gender.FEMALE);
			break;
		default:
			personDto.setGender(PersonDto.Gender.UNKNOWN);
		}
		personDto.setDateOfBirth(response.getDateOfBirth());
		personDto.setDeceased(response.isDeceased());
		personDto.setDateOfDeath(response.getDateOfDeath());
		personDto.setDistrict(response.getDistrict());

		return personDto;
	}
}
