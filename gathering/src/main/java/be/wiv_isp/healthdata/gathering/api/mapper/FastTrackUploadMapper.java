/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.api.mapper;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.domain.FastTrackUpload;
import org.codehaus.jettison.json.JSONObject;

import java.util.Iterator;

public class FastTrackUploadMapper {

	static public FastTrackUpload convert(JSONObject json) throws Exception {
		FastTrackUpload request = new FastTrackUpload();
		for (Iterator<String> iterator = json.keys(); iterator.hasNext();) {
			String key = iterator.next();
			if ("mode".equalsIgnoreCase(key)) {
				String modeValue = json.getString(key);
				if(FastTrackUpload.Mode.MERGE.toString().equalsIgnoreCase(modeValue)) {
					request.setMode(FastTrackUpload.Mode.MERGE);
				} else if(FastTrackUpload.Mode.OVERWRITE.toString().equalsIgnoreCase(modeValue)) {
					request.setMode(FastTrackUpload.Mode.OVERWRITE);
				} else {
					HealthDataException exception = new HealthDataException();
					exception.setExceptionType(ExceptionType.INVALID_FORMAT, modeValue, "overwrite|merge");
					throw exception;
				}
			} else if ("master".equalsIgnoreCase(key)) {
				String masterValue = json.getString(key);
				if(FastTrackUpload.Master.CSV.toString().equalsIgnoreCase(masterValue)) {
					request.setMaster(FastTrackUpload.Master.CSV);
				} else if(FastTrackUpload.Master.DATABASE.toString().equalsIgnoreCase(masterValue)) {
					request.setMaster(FastTrackUpload.Master.DATABASE);
				} else {
					HealthDataException exception = new HealthDataException();
					exception.setExceptionType(ExceptionType.INVALID_FORMAT, masterValue, "csv|database");
					throw exception;
				}

			} else if ("dataCollectionDefinitionId".equalsIgnoreCase(key)) {
				request.setDataCollectionDefinitionId(json.getLong(key));
			} else if ("fileName".equalsIgnoreCase(key)) {
				request.setFileName(json.getString(key));
			} else if ("csv".equalsIgnoreCase(key)) {
				request.setContent(json.getString(key));
			}
		}
		return request;
	}
}
