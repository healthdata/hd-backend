/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.gathering.dao.IStableDataDao;
import be.wiv_isp.healthdata.gathering.domain.StableData;
import be.wiv_isp.healthdata.gathering.domain.search.StableDataCountGroupByOrganizationDataCollection;
import be.wiv_isp.healthdata.gathering.domain.search.StableDataSearch;
import be.wiv_isp.healthdata.gathering.domain.view.StableDataView;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class StableDataDao extends CrudDaoV2<StableData, Long, StableDataSearch> implements IStableDataDao {

	public StableDataDao() {
		super(StableData.class);
	}

	@Override
	public StableDataView create(StableDataView stableDataView) {
		em.persist(stableDataView);
		em.flush();
		return stableDataView;
	}

	@Override
	public List<StableDataCountGroupByOrganizationDataCollection> getCountGroupByOrganizationDataCollection() {
		final CriteriaBuilder cb = em.getCriteriaBuilder();
		final CriteriaQuery<StableDataCountGroupByOrganizationDataCollection> cq = cb.createQuery(StableDataCountGroupByOrganizationDataCollection.class);
		final Root<StableData> rootEntry = cq.from(StableData.class);
		cq.multiselect(rootEntry.<Organization>get("organization").get("healthDataIDValue"), rootEntry.get("dataCollectionName"), cb.count(rootEntry), cb.countDistinct(rootEntry.get("patientId")));
		cq.groupBy(rootEntry.<Organization>get("organization").get("healthDataIDValue"), rootEntry.get("dataCollectionName"));
		final TypedQuery<StableDataCountGroupByOrganizationDataCollection> allQuery = em.createQuery(cq);
		return allQuery.getResultList();
	}

	@Override
	protected List<Predicate> getPredicates(StableDataSearch search, CriteriaBuilder cb, Root<StableData> rootEntry) {
		final List<Predicate> predicates = new ArrayList<>();

		if (StringUtils.isNotBlank(search.getPatientId())) {
			predicates.add(cb.equal(rootEntry.get("patientId"), search.getPatientId()));
		}

		if (StringUtils.isNotBlank(search.getDataCollectionName())) {
			predicates.add(cb.equal(rootEntry.get("dataCollectionName"), search.getDataCollectionName()));
		}

		if (search.getOrganization() != null) {
			predicates.add(cb.equal(rootEntry.get("organization"), search.getOrganization()));
		}

		return predicates;
	}
}
