/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.action.workflow;

import be.wiv_isp.healthdata.gathering.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.FollowUp;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.factory.IAbstractMessageFactory;
import be.wiv_isp.healthdata.orchestration.service.IIdentificationService;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class DeleteAction extends AbstractRegistrationWorkflowAction {

	private static final Logger LOG = LoggerFactory.getLogger(DeleteAction.class);

	@Autowired
	private IMessageService messageService;
	@Autowired
	private IAbstractMessageFactory<RegistrationWorkflow, ParticipationWorkflow> messageFactory;
	@Autowired
	private IIdentificationService identificationService;

	@Override
	public WorkflowActionType getAction() {
		return WorkflowActionType.DELETE;
	}

	@Override
	public WorkflowType getWorkflowType() {
		return WorkflowType.REGISTRATION;
	}

	@Override
	public WorkflowStatus getEndStatus() {
		return WorkflowStatus.DELETED;
	}

	@Override
	public RegistrationWorkflow doExecute(RegistrationWorkflow workflow, HDUserDetails userDetails) {

		if (workflow.getSubmittedOn() != null) {
			LOG.debug("workflow was already submitted. Notifying HD4RES that the workflow is being deleted.");
			final DeleteAction action = new DeleteAction();
			action.setIdentificationWorkflow(identificationService.getHealthDataIdentification(workflow.getOrganization()));
			final Message message = messageFactory.createOutgoingMessage(action, workflow);
			messageService.create(message);
		}

		final List<FollowUp> followUps = workflow.getFollowUps();
		if(CollectionUtils.isNotEmpty(followUps)) {
			LOG.debug("unschedule and deactivate follow-ups");
			for (FollowUp followUp : followUps) {
				followUp.setActive(false);
				followUp.setActivationDate(null);
			}
		}

		return workflow;
	}
}
