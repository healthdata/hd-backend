/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.action.standalone;

import be.wiv_isp.healthdata.orchestration.action.standalone.AbstractResendMessagesAction;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageStatus;
import be.wiv_isp.healthdata.orchestration.domain.search.MessageSearch;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.List;

@Component
public class ResendMessagesAction extends AbstractResendMessagesAction {

    private static final Logger LOG = LoggerFactory.getLogger(ResendMessagesAction.class);

    @Autowired
    private IMessageService messageService;

    @Override
    public void execute() {
        LOG.info(MessageFormat.format("Executing action {0}", getAction()));

        final MessageSearch search = new MessageSearch();
        search.setStatuses(MessageStatus.UNACKNOWLEDGED, MessageStatus.ERROR_SENDING);
        search.setHasRemainingSendingTrials(false);

        final List<Message> messages = messageService.getAll(search);

        for(Message message : messages) {
            message.setStatus(MessageStatus.OUTBOX);
            message.setRemainingSendingTrials(Message.SENDING_TRIALS);
            messageService.update(message);
        }

        if(messages.isEmpty()) {
            LOG.info("No message must be resent");
        } else {
            LOG.info(MessageFormat.format("The status of {0} message(s) was updated to {1}", messages.size(), MessageStatus.OUTBOX));
        }
    }
}
