/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.common.domain.search.EntitySearch;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.dao.IUpdatedUserDao;
import be.wiv_isp.healthdata.gathering.domain.UpdatedUser;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.MessageFormat;
import java.util.List;

@Repository
public class UpdatedUserDao extends CrudDaoV2<UpdatedUser, Long, EntitySearch> implements IUpdatedUserDao {

    public UpdatedUserDao() {
        super(UpdatedUser.class);
    }

    @Override
    protected List<Predicate> getPredicates(EntitySearch search, CriteriaBuilder cb, Root<UpdatedUser> rootEntry) {
        HealthDataException exception = new HealthDataException();
        exception.setExceptionType(ExceptionType.METHOD_NOT_ALLOWED, MessageFormat.format("[getPredicates] on {0}", this.getClass().getSimpleName()));
        throw exception;

    }
}
