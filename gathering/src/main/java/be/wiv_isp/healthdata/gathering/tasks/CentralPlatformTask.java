/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.gathering.tasks;

import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.search.ConfigurationSearch;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.centralplatform.dto.*;
import be.wiv_isp.healthdata.gathering.centralplatform.service.ICentralPlatformService;
import be.wiv_isp.healthdata.gathering.domain.UserRequest;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationCountGroupByOrganizationDcdStatus;
import be.wiv_isp.healthdata.gathering.domain.search.StableDataCountGroupByOrganizationDataCollection;
import be.wiv_isp.healthdata.gathering.dto.UserRequestDto;
import be.wiv_isp.healthdata.gathering.service.IRegistrationWorkflowService;
import be.wiv_isp.healthdata.gathering.service.IStableDataService;
import be.wiv_isp.healthdata.gathering.service.IUserRequestService;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageStatus;
import be.wiv_isp.healthdata.orchestration.service.IUserService;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import be.wiv_isp.healthdata.orchestration.service.IStatusService;
import be.wiv_isp.healthdata.orchestration.tasks.HealthDataTask;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class CentralPlatformTask extends HealthDataTask {

    private static final Logger LOG = LoggerFactory.getLogger(CentralPlatformTask.class);

    @Autowired
    private ICentralPlatformService centralPlatformService;
    @Autowired
    private IRegistrationWorkflowService workflowService;
    @Autowired
    private IStableDataService stableDataService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IUserRequestService userRequestService;
    @Autowired
    private IMessageService messageService;
    @Autowired
    private IOrganizationService organizationService;
    @Autowired
    private IStatusService statusService;

    @Override
    public String getName() {
        return "CENTRAL_PLATFORM";
    }

    @Override
    protected void execute() {
        updateRegistrationStatistics();
        updateStableDataStatistics();
        updateUsers();
        updateUserRequests();
        updateConfigurations();
        updateMessageStatistics();
        updateStatuses();
    }

    private void updateRegistrationStatistics() {
        try {
            final List<RegistrationCountGroupByOrganizationDcdStatus> registrationCounts = workflowService.getCountGroupByOrganizationDcdStatus();
            final List<RegistrationStatisticDto> registrationStatisticDtos = mapRegistrationStatistics(registrationCounts);
            centralPlatformService.updateRegistrationStatistics(registrationStatisticDtos);
        } catch (HealthDataException e) {
            LOG.error("Could not update registration statistics to the catalogue. Reason: {}", e.getMessage());
        }
    }

    private void updateStableDataStatistics() {
        try {
            final List<StableDataCountGroupByOrganizationDataCollection> stableDataCounts = stableDataService.getCountGroupByOrganizationDataCollection();
            final List<StableDataStatisticDto> stableDataStatisticDtos = mapStableDataStatistics(stableDataCounts);
            centralPlatformService.updateStableDataStatistics(stableDataStatisticDtos);
        } catch (HealthDataException e) {
            LOG.error("Could not update stable data statistics to the catalogue. Reason: {}", e.getMessage());
        }
    }

    private void updateUsers() {
        try {
            final List<User> users = userService.getAll();
            final List<UserDto> userDtos = mapUsers(users);
            centralPlatformService.updateUsers(userDtos);
        } catch (HealthDataException e) {
            LOG.error("Could not update users to the catalogue. Reason: {}", e.getMessage());
        }
    }

    private void updateUserRequests() {
        try {
            final List<UserRequest> userRequests = userRequestService.getAll();
            final List<UserRequestDto> userRequestDtos = mapUserRequests(userRequests);
            centralPlatformService.updateUserRequests(userRequestDtos);
        } catch (HealthDataException e) {
            LOG.error("Could not update user requests to the catalogue. Reason: {}", e.getMessage());
        }
    }

    private void updateConfigurations() {
        try {
            final List<ConfigurationDto> configurations = getConfigurations();
            centralPlatformService.updateConfigurations(configurations);
        } catch (HealthDataException e) {
            LOG.error("Could not update configurations to the catalogue. Reason: {}", e.getMessage());
        }
    }

    private void updateMessageStatistics() {
        try {
            final List<MessageStatisticDto> messages = getMessageStatistics();
            centralPlatformService.updateMessageStatistics(messages);
        } catch (HealthDataException e) {
            LOG.error("Could not update message statistics to the catalogue. Reason: {}", e.getMessage());
        }
    }

    private void updateStatuses() {
        try {
            final JSONObject statuses = getStatuses();
            centralPlatformService.updateStatuses(statuses);
        } catch (HealthDataException e) {
            LOG.error("Could not update statuses to the catalogue. Reason: {}", e.getMessage());
        }
    }

    private List<RegistrationStatisticDto> mapRegistrationStatistics(List<RegistrationCountGroupByOrganizationDcdStatus> registrationCounts) {
        final List<RegistrationStatisticDto> dtos = new ArrayList<>();
        for (final RegistrationCountGroupByOrganizationDcdStatus registrationCount : registrationCounts) {
            final RegistrationStatisticDto dto = new RegistrationStatisticDto();
            dto.setOrganizationIdentificationValue(registrationCount.getOrganizationIdentificationValue());
            dto.setDataCollectionDefinitionId(registrationCount.getDataCollectionDefinitionId());
            dto.setStatus(registrationCount.getStatus());
            dto.setCount(registrationCount.getCount());
            dtos.add(dto);
        }
        return dtos;
    }

    private List<StableDataStatisticDto> mapStableDataStatistics(final List<StableDataCountGroupByOrganizationDataCollection> stableDataCounts) {
        final List<StableDataStatisticDto> dtos = new ArrayList<>();
        for (final StableDataCountGroupByOrganizationDataCollection stableDataCount : stableDataCounts) {
            final StableDataStatisticDto dto = new StableDataStatisticDto();
            dto.setOrganizationIdentificationValue(stableDataCount.getOrganizationIdentificationValue());
            dto.setDataCollectionName(stableDataCount.getDataCollectionName());
            dto.setCount(stableDataCount.getCount());
            dto.setDistinctPatientIdCount(stableDataCount.getDistinctPatientIdCount());
            dtos.add(dto);
        }
        return dtos;
    }

    private List<UserDto> mapUsers(final List<User> users) {
        final List<UserDto> dtos = new ArrayList<>();
        for (final User user : users) {
            final UserDto dto = new UserDto();
            dto.setUsername(user.getUsername());
            dto.setOrganizationIdentificationValue(user.getOrganization().getHealthDataIDValue());
            dto.setFirstName(user.getFirstName());
            dto.setLastName(user.getLastName());
            dto.setEmail(user.getEmail());
            dto.setAuthorities(toStringSet(user.getAuthorities()));
            dto.setDataCollectionNames(user.getDataCollectionNames());
            dto.setEnabled(user.isEnabled());
            dto.setLdapUser(user.isLdapUser());
            dtos.add(dto);
        }
        return dtos;
    }

    private List<UserRequestDto> mapUserRequests(List<UserRequest> userRequests) {
        final List<UserRequestDto> dtos = new ArrayList<>();
        for (final UserRequest userRequest : userRequests) {
            dtos.add(new UserRequestDto(userRequest));
        }
        return dtos;
    }

    private List<ConfigurationDto> getConfigurations() {
        List<ConfigurationDto> configurations = new ArrayList<>();

        List<ConfigurationKey> configurationKeys = new ArrayList<>();
        configurationKeys.add(ConfigurationKey.USER_MANAGEMENT_TYPE);
        configurationKeys.add(ConfigurationKey.NATIONAL_REGISTER_CONNECTOR);
        configurationKeys.add(ConfigurationKey.CATALOGUE_HOST);
        configurationKeys.add(ConfigurationKey.EM_INTERFACE_TYPE);
        configurationKeys.add(ConfigurationKey.EM_REST_URL_IN);
        configurationKeys.add(ConfigurationKey.EM_REST_URL_OUT);
        configurationKeys.add(ConfigurationKey.EM_INTERFACE_IN);
        configurationKeys.add(ConfigurationKey.EM_INTERFACE_OUT);

        for (ConfigurationKey key : configurationKeys) {
            ConfigurationSearch search = new ConfigurationSearch();
            search.setKey(key);
            for (Configuration configuration : configurationService.getAll(search)) {
                ConfigurationDto dto = new ConfigurationDto();
                if (configuration.getOrganization() == null)
                    dto.setOrganizationIdentificationValue(organizationService.getMain().getHealthDataIDValue());
                else
                    dto.setOrganizationIdentificationValue(configuration.getOrganization().getHealthDataIDValue());
                dto.setKey(key.toString());
                dto.setValue(configuration.getValue());
                configurations.add(dto);
            }
        }

        return configurations;
    }

    private List<MessageStatisticDto> getMessageStatistics() {
        List<MessageStatisticDto> messages = new ArrayList<>();

        final List<Organization> organizations = organizationService.getAll();
        for (Organization organization : organizations) {
            final Map<MessageStatus, Long> countByStatuses = messageService.getCountByStatuses(organization);
            for (Map.Entry<MessageStatus, Long> entry : countByStatuses.entrySet()) {
                MessageStatisticDto dto = new MessageStatisticDto();
                dto.setOrganizationIdentificationValue(organization.getHealthDataIDValue());
                dto.setStatus(entry.getKey().toString());
                dto.setCount(entry.getValue());
                messages.add(dto);
            }
        }

        return messages;
    }

    private JSONObject getStatuses() {
        try {
            return statusService.getStatus(true);
        }
        catch (Exception e) {
            return null;
        }
    }

    private Set<String> toStringSet(final Set<Authority> authorities) {
        final Set<String> set = new HashSet<>();
        for (Authority authority : authorities) {
            set.add(authority.getAuthority());
        }
        return set;
    }
}
