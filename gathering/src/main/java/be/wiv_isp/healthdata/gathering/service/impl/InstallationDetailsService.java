/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.gathering.dao.IInstallationDetailsDao;
import be.wiv_isp.healthdata.gathering.domain.InstallationDetails;
import be.wiv_isp.healthdata.gathering.domain.search.InstallationDetailSearch;
import be.wiv_isp.healthdata.gathering.service.IInstallationDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InstallationDetailsService extends AbstractService<InstallationDetails, Long, InstallationDetailSearch, IInstallationDetailsDao> implements IInstallationDetailsService {

    @Autowired
    private IInstallationDetailsDao dao;

    public InstallationDetailsService() {
        super(InstallationDetails.class);
    }

    @Override
    protected IInstallationDetailsDao getDao() {
        return dao;
    }

    @Override
    public InstallationDetails get() {
        return getUnique(null);
    }
}
