/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.EMInterfaceType;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.dto.EncryptionModuleMessage;
import be.wiv_isp.healthdata.orchestration.service.IEncryptionModuleService;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractEncryptionModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.List;

@Component
public class EncryptionModuleService extends AbstractEncryptionModuleService {

	@Autowired
	private IEncryptionModuleService fileSystemEncryptionModuleService;

	@Autowired
	private IEncryptionModuleService restEncryptionModuleService;

	@Autowired
	private IConfigurationService configurationService;

	@Override
	public void send(EncryptionModuleMessage message, Organization organization) {
		final EMInterfaceType emInterfaceType = EMInterfaceType.valueOf(configurationService.get(ConfigurationKey.EM_INTERFACE_TYPE, organization).getValue());

		switch (emInterfaceType) {
			case REST:
				restEncryptionModuleService.send(message, organization);
				break;
			case FILE_SYSTEM:
				fileSystemEncryptionModuleService.send(message, organization);
				break;
		}
	}

	@Override
	public List<EncryptionModuleMessage> receive(Organization organization) {
		final EMInterfaceType type = EMInterfaceType.valueOf(configurationService.get(ConfigurationKey.EM_INTERFACE_TYPE, organization).getValue());

		switch (type) {
			case REST:
				return restEncryptionModuleService.receive(organization);
			case FILE_SYSTEM:
				return fileSystemEncryptionModuleService.receive(organization);
		}

		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, MessageFormat.format("Unknown EMInterfaceType [{0}]", type));
		throw exception;
	}
}
