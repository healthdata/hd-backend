/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.WorkflowsToMigrateComments;
import be.wiv_isp.healthdata.gathering.service.IWorkflowsToMigrateCommentsService;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractNoteMigrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;


@Service
public class NoteMigrationService extends AbstractNoteMigrationService<RegistrationWorkflow>{

    @Autowired
    private IWorkflowsToMigrateCommentsService workflowsToMigrateCommentsService;

    @Override
    protected DocumentData getDocumentData(RegistrationWorkflow workflow) {
        DocumentData documentData = new DocumentData();
        documentData.setContent(workflow.getDocument().getDocumentContent());
        documentData.setPrivate(workflow.getDocument().getPrivateContent());
        if (workflow.getDocument().getCodedContent() != null)
            documentData.setCoded(new HashMap<>(workflow.getDocument().getCodedContent()));
        return documentData;
    }

    @Override
    public boolean isMigrated(RegistrationWorkflow workflow) {
        return workflowsToMigrateCommentsService.get(workflow.getId()) == null;
    }

    @Override
    protected void setMigrated(Long workflowId) {
        final WorkflowsToMigrateComments workflowsToMigrateComments = workflowsToMigrateCommentsService.get(workflowId);
        if (workflowsToMigrateComments != null)
            workflowsToMigrateCommentsService.delete(workflowId);
    }

}
