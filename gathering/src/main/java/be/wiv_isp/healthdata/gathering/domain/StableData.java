/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.domain;

import be.wiv_isp.healthdata.orchestration.domain.Organization;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "STABLE_DATA")
public class StableData extends AbstractStableData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "STABLE_DATA_ID", nullable = false)
    private Long id;

    public StableData() {
    }

    public StableData(Long id, String patientId, String dataCollectionName, Date validFrom, byte[] csv, Organization organization) {
        this.id = id;
        setPatientId(patientId);
        setDataCollectionName(dataCollectionName);
        setValidFrom(new Timestamp(validFrom.getTime()));
        setCsv(csv);
        setOrganization(organization);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
