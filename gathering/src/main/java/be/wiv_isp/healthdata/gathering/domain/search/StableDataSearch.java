/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.domain.search;

import be.wiv_isp.healthdata.common.domain.search.EntitySearch;
import be.wiv_isp.healthdata.orchestration.domain.Organization;

import java.util.Objects;

public class StableDataSearch extends EntitySearch {

	private String patientId;
	private String dataCollectionName;
	private Organization organization;

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getDataCollectionName() {
		return dataCollectionName;
	}

	public void setDataCollectionName(String dataCollectionName) {
		this.dataCollectionName = dataCollectionName;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	@Override
	public String toString() {
		return "StableDataSearch {" +
				"patientId = " + Objects.toString(this.patientId) + ", " +
				"dataCollectionName = " + Objects.toString(this.dataCollectionName) + ", " +
				"organization = " + Objects.toString(this.organization) + "}";
	}
}
