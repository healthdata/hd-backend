/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationCountGroupByOrganizationDcdStatus;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.gathering.dto.ParticipationCreationDto;
import be.wiv_isp.healthdata.orchestration.service.IAbstractRegistrationWorkflowService;

import java.util.List;

public interface IRegistrationWorkflowService extends IAbstractRegistrationWorkflowService<RegistrationWorkflow, RegistrationWorkflowSearch> {

	boolean isUnique(long dataCollectionDefinitionId, String uniqueID, Long notWorkflowId, Organization organization);

	List<RegistrationWorkflow> getUnique(long dataCollectionDefinitionId, String uniqueID, Organization organization);

	List<ParticipationCreationDto> getParticipationCreationInfo();

	void createNewDocumentVersion(RegistrationWorkflow workflow);

	List<RegistrationCountGroupByOrganizationDcdStatus> getCountGroupByOrganizationDcdStatus();

	RegistrationWorkflow getByFollowUpId(Long followUpId);

}
