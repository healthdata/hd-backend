/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.domain.validation.impl;

import be.wiv_isp.healthdata.gathering.domain.StatusMessage;
import be.wiv_isp.healthdata.orchestration.domain.validator.impl.AbstractStatusMessageValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class StatusMessageValidator extends AbstractStatusMessageValidator<StatusMessage> {

    private static final Logger LOG = LoggerFactory.getLogger(StatusMessageValidator.class);

    @Override
    public void validate(StatusMessage statusMessage) {
        LOG.info("Validating status message (nothing to do)");
    }
}
