/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.action.standalone;

import be.wiv_isp.healthdata.orchestration.domain.CSVContent;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.search.OrganizationSearch;
import be.wiv_isp.healthdata.gathering.domain.StableData;
import be.wiv_isp.healthdata.gathering.service.IStableDataService;
import be.wiv_isp.healthdata.gathering.service.IStableDataUploadService;
import be.wiv_isp.healthdata.orchestration.action.standalone.AbstractStableDataUploadAction;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.gathering.domain.StableDataUpload;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;

@Component
public class StableDataUploadAction extends AbstractStableDataUploadAction {

    private static final Logger LOG = LoggerFactory.getLogger(StableDataUploadAction.class);

    @Autowired
    private IStableDataService stableDataService;

    @Autowired
    private IStableDataUploadService stableDataUploadService;

    @Autowired
    private IOrganizationService organizationService;

    @Override
    public void execute() {
        try {
            final OrganizationSearch search = new OrganizationSearch();
            search.setHealthDataIDType(healthDataIdentification.getType());
            search.setHealthDataIDValue(healthDataIdentification.getValue());
            final Organization organization = organizationService.getUnique(search);

            final StableData stableData = new StableData();
            stableData.setDataCollectionName(dataCollectionName);
            stableData.setPatientId(patientId);
            stableData.setCsv(csv);
            stableData.setOrganization(organization);
            stableData.setValidFrom(new Timestamp(new Date().getTime()));
            stableDataService.create(stableData);

            createStableDataUpload(true);
        } catch (Exception e) {
            createStableDataUpload(false);
            throw e;
        }
    }

    private void createStableDataUpload(boolean success) {
        final StableDataUpload upload = new StableDataUpload();
        upload.setStableDataId(stableDataId);
        upload.setSuccess(success);
        upload.setDataCollectionName(dataCollectionName);
        upload.setIdentificationType(healthDataIdentification.getType());
        upload.setIdentificationValue(healthDataIdentification.getValue());
        stableDataUploadService.create(upload);
    }

    @Override
    public void extractInfo(Message message) {
        final CSVContent csvContent = new CSVContent(message.getContent());
        if (csvContent.isUseDescriptors()) {
            this.patientId = csvContent.getPatientId();
            if (this.patientId == null) {
                // this assumes that the first patient ID is the one to store stable data for
                for (int i = 0; i < csvContent.getSize() - 1; i++) {
                    if (csvContent.isPatientIdField(i)) {
                        this.patientId = csvContent.getField(i);
                        break;
                    }
                }
            }
        }
        else {
            this.patientId = csvContent.getPatientId();
        }
    }

    public void addSuccess(Long id) {
        successes.add(id);
    }

    public void addFailure(Long id) {
        failures.add(id);
    }
}
