/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.common.exception.ExceptionUtils;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.gathering.api.uri.ExportReportRegistrationUri;
import be.wiv_isp.healthdata.gathering.dao.IExceptionReportDao;
import be.wiv_isp.healthdata.gathering.domain.ExceptionReport;
import be.wiv_isp.healthdata.gathering.domain.search.ExceptionReportSearch;
import be.wiv_isp.healthdata.gathering.dto.ExceptionReportDto;
import be.wiv_isp.healthdata.gathering.service.IExceptionService;
import be.wiv_isp.healthdata.gathering.service.IInstallationDetailsService;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.ICatalogueClient;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import com.sun.jersey.api.client.ClientResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.core.MediaType;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ExceptionService implements IExceptionService {

    private static final Logger LOG = LoggerFactory.getLogger(ExceptionService.class);

    @Autowired
    private IExceptionReportDao exceptionReportDao;
    @Autowired
    private IConfigurationService configurationService;
    @Autowired
    private IInstallationDetailsService installationDetailsService;
    @Autowired
    private ICatalogueClient catalogueClient;

    @Transactional
    @Override
    public String report(String message, Throwable exception) {
        String uuid = ExceptionUtils.log(exception);
        ExceptionReport report = new ExceptionReport();
        report.setMessage(message);
        report.setStackTrace(org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(exception));

        ExceptionReportSearch search = new ExceptionReportSearch();
        search.setHash(report.getHash());
        List<ExceptionReport> exceptionReports = exceptionReportDao.getAll(search);
        if(exceptionReports.isEmpty()) {
            Set<String> uuids = new HashSet<>();
            uuids.add(uuid);
            report.setUuids(uuids);
            exceptionReportDao.create(report);
            return uuid;
        } else if(exceptionReports.size() != 1) {
            LOG.error("more then one unreported exception for hash [{}], current exception uuid [{}]", report.getHash(), uuid);
        }
        report = exceptionReports.get(0);
        report.getUuids().add(uuid);
        exceptionReportDao.update(report);
        return uuid;
    }

    @Transactional
    @Override
    public void transferToCatalogue() {
        ExceptionReportSearch search = new ExceptionReportSearch();
        search.setReported(false);
        List<ExceptionReport> exceptionReports = exceptionReportDao.getAll(search);
        Timestamp reportDate = new Timestamp(new Date().getTime());
        for (ExceptionReport exceptionReport : exceptionReports) {
            try {
                LOG.info("Registering exception report to the catalogue (id {})", exceptionReport.getId());

                ExceptionReportDto dto = new ExceptionReportDto();
                dto.setMessage(exceptionReport.getMessage());
                dto.setStackTrace(exceptionReport.getStackTrace());
                dto.setStartTime(exceptionReport.getCreatedOn());
                dto.setEndTime(reportDate);
                Set<String> uuids = new HashSet<>();
                uuids.addAll(exceptionReport.getUuids());
                dto.setUuids(uuids);


                final ExportReportRegistrationUri uri = new ExportReportRegistrationUri();
                uri.setHost(configurationService.get(ConfigurationKey.CATALOGUE_HOST).getValue());
                uri.setInstallationId(installationDetailsService.get().getInstallationId());

                final WebServiceBuilder wsb = new WebServiceBuilder();
                wsb.setLogResponseBody(false); //TODO set to false after testing
                wsb.setUrl(uri.toString());
                wsb.setPost(true);
                wsb.setType(MediaType.APPLICATION_JSON_TYPE);
                wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
                wsb.setJson(dto);
                ClientResponse clientResponse = catalogueClient.addAuthorizationHeaderAndCallWebService(wsb);
                if(clientResponse.getStatus() == 204) {
                    exceptionReport.setReportedOn(reportDate);
                    exceptionReportDao.update(exceptionReport);
                    LOG.info("Exception report successfully registered to the catalogue (id {})", exceptionReport.getId());
                } else {
                    LOG.error("Error while reporting exception with id [{}]", exceptionReport.getId());
                }
            } catch (Exception e) {
                LOG.error("Error while reporting exception with id [{}]", exceptionReport.getId());
                LOG.error(String.valueOf(exceptionReport.getId()), e);
            }
        }

    }
}
