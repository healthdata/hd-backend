/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.gathering.service.IRegistrationWorkflowService;
import be.wiv_isp.healthdata.gathering.service.IUniqueIDService;
import be.wiv_isp.healthdata.orchestration.dao.IWorkflowDao;
import be.wiv_isp.healthdata.orchestration.service.IMappingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.PostConstruct;
import java.text.MessageFormat;
import java.util.List;

@Component
public class UniqueIDService implements IUniqueIDService {

	private static final Logger LOG = LoggerFactory.getLogger(UniqueIDService.class);

	@Autowired
	private IWorkflowDao<RegistrationWorkflow, RegistrationWorkflowSearch> workflowDao;

	@Autowired
	private IRegistrationWorkflowService workflowService;

	@Autowired
	private IMappingService mappingService;

	@Autowired
	@Qualifier("transactionManager")
	protected PlatformTransactionManager txManager;

	@Override
	@PostConstruct
	public void setup() {
		List<RegistrationWorkflow> workflows = workflowDao.getAllForUniqueIDCalculation();
		LOG.info(MessageFormat.format("{0} workflows need a uniqueID to be calculated", workflows.size()));
		for (RegistrationWorkflow workflow : workflows) {
			setup(workflow);
		}
	}

	public void setup(final RegistrationWorkflow workflow) {
		TransactionTemplate tmpl = new TransactionTemplate(txManager);
		tmpl.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
				LOG.info(MessageFormat.format("Retrieving unique id for workflow [{0}]", workflow.getId()));
				if (workflow.getDocument() != null) {
					String uniqueID = mappingService.getUniqueID(workflow);
					if (uniqueID == null) {
						LOG.info("mapping returned not uniqueId for workflow with dataCollectionName [{}] and dataCollectiondefinitionId [{}]", workflow.getDataCollectionName(), workflow.getDataCollectionDefinitionId());
						uniqueID = "";
					} else {
						List<RegistrationWorkflow> uniques = workflowService.getUnique(workflow.getDataCollectionDefinitionId(), uniqueID, workflow.getOrganization());
						if (!uniques.isEmpty()) {
							StringBuilder sb = new StringBuilder();
							sb.append(workflow.getDataCollectionName());
							sb.append(" duplicate: workflowId [");
							sb.append(workflow.getId());
							sb.append("]");
							for (RegistrationWorkflow unique : uniques) {
								sb.append(" workflowId [");
								sb.append(unique.getId());
								sb.append("]");
							}
							LOG.info(sb.toString());
						}
					}
					LOG.info(MessageFormat.format("Unique id: {0}", uniqueID));
					workflow.setUniqueID(uniqueID);
					workflowDao.update(workflow);
				}
			}
		});
	}
}
