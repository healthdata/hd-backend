/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.gathering.centralplatform.dto;

import org.codehaus.jackson.annotate.JsonProperty;

public class StableDataStatisticDto {

    private String organizationIdentificationValue;
    private String dataCollectionName;
    private Long count;
    private Long distinctPatientIdCount;


    @JsonProperty("organizationIdentificationValue")
    public String getOrganizationIdentificationValue() {
        return organizationIdentificationValue;
    }

    public void setOrganizationIdentificationValue(String organizationIdentificationValue) {
        this.organizationIdentificationValue = organizationIdentificationValue;
    }

    @JsonProperty("dataCollectionName")
    public String getDataCollectionName() {
        return dataCollectionName;
    }

    public void setDataCollectionName(String dataCollectionName) {
        this.dataCollectionName = dataCollectionName;
    }

    @JsonProperty("count")
    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @JsonProperty("distinctPatientIdCount")
    public Long getDistinctPatientIdCount() {
        return distinctPatientIdCount;
    }

    public void setDistinctPatientIdCount(Long distinctPatientIdCount) {
        this.distinctPatientIdCount = distinctPatientIdCount;
    }
}
