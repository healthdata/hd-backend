/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.gathering.service.IINSSService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

@Component
public class INSSService implements IINSSService {

	private static final Logger LOG = LoggerFactory.getLogger(INSSService.class);

	@Override
	public String trim(String inss) {
		LOG.debug(MessageFormat.format("Trim SSIN value: {0}", inss));
		inss = StringUtils.replace(inss, "-", "");
		inss = StringUtils.replace(inss, " ", "");
		inss = StringUtils.replace(inss, "/", "");
		inss = StringUtils.replace(inss, ".", "");
		LOG.debug(MessageFormat.format("Trimmed SSIN value: {0}", inss));
		return inss;
	}

	@Override
	public boolean validate(String inss) {
		LOG.debug(MessageFormat.format("Validating SSIN number [{0}]", inss));

		inss = trim(inss);
		if (inss.length() != 11) {
			LOG.debug(MessageFormat.format("Length ({0}) is different from 11. SSIN not valid.", inss.length()));
			return false;
		}
		if (!inss.matches("\\d+")) {
			LOG.debug("SSIN contains non-digit characters");
			return false;
		}
		String modulo97 = inss.substring(9, 11);
		if (Long.valueOf(modulo97) != (97 - (Long.valueOf(inss.substring(0, 9))) % 97) && Long.valueOf(modulo97) != (97 - (Long.valueOf("2" + inss.substring(0, 9))) % 97)) {
			LOG.debug("Module 97 validation failed. SSIN is not valid.");
			return false;
		}

		LOG.debug("SSIN is valid");

		return true;
	}
}
