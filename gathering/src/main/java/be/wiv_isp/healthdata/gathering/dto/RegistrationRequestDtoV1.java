/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.gathering.domain.enumeration.ExecutionOption;

public class RegistrationRequestDtoV1 {

	private final List<ExecutionOption> options = new ArrayList<>();

	private Long dataCollectionDefinitionId;

	private Organization organization;

	private byte[] document;

	public List<ExecutionOption> getOptions() {
		return options;
	}

	public void addOption(ExecutionOption option) {
		this.options.add(option);
	}

	public Long getDataCollectionDefinitionId() {
		return dataCollectionDefinitionId;
	}

	public void setDataCollectionDefinitionId(Long dataCollectionDefinitionId) {
		this.dataCollectionDefinitionId = dataCollectionDefinitionId;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public byte[] getDocument() {
		return document;
	}

	public void setDocument(byte[] document) {
		this.document = document;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof RegistrationRequestDtoV1) {
			RegistrationRequestDtoV1 other = (RegistrationRequestDtoV1) o;

			return Objects.equals(options, other.options) //
					&& Objects.equals(dataCollectionDefinitionId, other.dataCollectionDefinitionId) //
					&& Arrays.equals(document, other.document);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.options, this.dataCollectionDefinitionId, this.document);
	}

	@Override
	public String toString() {
		return "User {" + //
				"options = " + Objects.toString(this.options) + ", " + //
				"dataCollectionDefinitionId = " + dataCollectionDefinitionId + ", " + //
				"document = " + Arrays.toString(this.document) + "}";
	}
}
