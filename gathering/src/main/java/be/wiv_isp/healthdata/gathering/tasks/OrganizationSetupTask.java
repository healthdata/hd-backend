/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.gathering.tasks;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.search.OrganizationSearch;
import be.wiv_isp.healthdata.orchestration.dto.OrganizationDto;
import be.wiv_isp.healthdata.gathering.centralplatform.service.ICentralPlatformService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationSetupService;
import be.wiv_isp.healthdata.orchestration.tasks.HealthDataTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class OrganizationSetupTask extends HealthDataTask {

    @Autowired
    private IOrganizationSetupService organizationSetupService;

    @Autowired
    private IOrganizationService organizationService;

    @Autowired
    private ICentralPlatformService centralPlatformService;

    @Override
    public String getName() {
        return "ORGANIZATION_SETUP";
    }

    @Override
    protected void execute() {
        organizationSetupService.updateOrganizations();

        OrganizationSearch search = new OrganizationSearch();
        search.setDeleted(null); // also send deleted organizations to central platform
        final List<Organization> organizations = organizationService.getAll(search);
        final List<OrganizationDto> dtos = mapToDtos(organizations);
        centralPlatformService.updateOrganizations(dtos);
    }

    private List<OrganizationDto> mapToDtos(final List<Organization> organizations) {
        final List<OrganizationDto> dtos = new ArrayList<>();
        for (final Organization organization : organizations) {
            dtos.add(new OrganizationDto(organization));
        }
        return dtos;
    }
}
