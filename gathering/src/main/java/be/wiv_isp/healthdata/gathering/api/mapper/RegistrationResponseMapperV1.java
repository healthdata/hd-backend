/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.api.mapper;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.gathering.domain.Registration;
import be.wiv_isp.healthdata.gathering.dto.RegistrationResponseDtoV1;
import be.wiv_isp.healthdata.orchestration.api.uri.DataCollectionDefinitionUri;
import be.wiv_isp.healthdata.orchestration.api.uri.DocumentUrl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;

public class RegistrationResponseMapperV1 implements RegistrationMapper<String, RegistrationResponseDtoV1> {

	public RegistrationResponseMapperV1(String guiHost, String catalogueHost) {
		this.guiHost = guiHost;
		this.catalogueHost = catalogueHost;
	}

	final private String guiHost;
	final private String catalogueHost;

	@Override
	public String convert(RegistrationResponseDtoV1 response) throws Exception {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document document = dBuilder.newDocument();
		Element healthdataElement = document.createElement("healthdata");
		document.appendChild(healthdataElement);

		if (response.getErrorMessage() != null) {
			Element errorElement = document.createElement("error");
			errorElement.setTextContent(String.valueOf(response.getErrorMessage()));
			healthdataElement.appendChild(errorElement);
		}

		if (response.getValidationResponse() != null) {
			Element validationsElement = document.createElement("validations");

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			DocumentBuilder builder = factory.newDocumentBuilder();

			Document validations = builder.parse(new InputSource(new ByteArrayInputStream(response.getValidationResponse().getBytes("utf-8"))));
			Element validationElement = validations.getDocumentElement();
			validations.removeChild(validationElement);
			validationsElement.appendChild(document.importNode(validationElement, true));

			healthdataElement.appendChild(validationsElement);
		}

		Map<DataCollectionDefinitionDtoV7, List<Registration>> registrations = response.getRegistrations();
		if (!registrations.isEmpty()) {
			Element registrationsElement = document.createElement("registrations");
			healthdataElement.appendChild(registrationsElement);
			for (Entry<DataCollectionDefinitionDtoV7, List<Registration>> entry : registrations.entrySet()) {
				for (Registration registration : entry.getValue()) {
					registrationsElement.appendChild(convertRegistation(document, entry.getKey(), registration));
				}
			}
		}

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource domSource = new DOMSource(document);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		StreamResult streamResult = new StreamResult(bos);
		transformer.transform(domSource, streamResult);

		return new String(bos.toByteArray(), StandardCharsets.UTF_8);
	}

	private Element convertRegistation(Document document, DataCollectionDefinitionDtoV7 dcd, Registration registration) {
		SimpleDateFormat sdf = new SimpleDateFormat(DateFormat.DATE_AND_TIME.getPattern());
		sdf.setTimeZone(TimeZone.getTimeZone("Europe/Brussels"));
		Element registrationElement = document.createElement("registration");
		if (registration != null) {
			Element idElement = document.createElement("id");
			idElement.setTextContent(String.valueOf(registration.getId()));
			registrationElement.appendChild(idElement);

			Element createdOnElement = document.createElement("createdon");
			createdOnElement.setTextContent(sdf.format(registration.getCreatedOn()));
			registrationElement.appendChild(createdOnElement);

			Element updatedOnElement = document.createElement("updatedon");
			updatedOnElement.setTextContent(sdf.format(registration.getUpdatedOn()));
			registrationElement.appendChild(updatedOnElement);

			Element documentElement = document.createElement("document");
			registrationElement.appendChild(documentElement);

			Element documentIdElement = document.createElement("id");
			documentIdElement.setTextContent(String.valueOf(registration.getWorkflow().getId()));
			documentElement.appendChild(documentIdElement);

			Element statusElement = document.createElement("status");
			statusElement.setTextContent(registration.getWorkflow().getStatus().toString());
			documentElement.appendChild(statusElement);

			Element urlElement = document.createElement("url");
			DocumentUrl url = new DocumentUrl();
			url.setHost(guiHost);
			url.setId(registration.getWorkflow().getId());
			urlElement.setTextContent(url.toString());
			documentElement.appendChild(urlElement);
		}
		registrationElement.appendChild(convertDataCollectionDefinition(document, dcd));
		return registrationElement;
	}

	private Element convertDataCollectionDefinition(Document document, DataCollectionDefinitionDtoV7 dcd) {
		SimpleDateFormat sdf = new SimpleDateFormat(DateFormat.DATE_AND_TIME.getPattern());
		sdf.setTimeZone(TimeZone.getTimeZone("Europe/Brussels"));
		Element dataCollectionElement = document.createElement("datacollection");

		Element uriElement = document.createElement("uri");
		final DataCollectionDefinitionUri uri = new DataCollectionDefinitionUri();
		uri.setHost(catalogueHost);
		uri.setId(dcd.getId());

		uriElement.setTextContent(uri.toString());
		dataCollectionElement.appendChild(uriElement);

		Element nameElement = document.createElement("name");
		nameElement.setTextContent(dcd.getDataCollectionName());
		dataCollectionElement.appendChild(nameElement);

		Element versionElement = document.createElement("version");
		versionElement.setTextContent(dcd.getDataCollectionGroup().getMajorVersion() + "." + dcd.getMinorVersion());
		dataCollectionElement.appendChild(versionElement);

		Element startDateElement = document.createElement("startdate");
		startDateElement.setTextContent(sdf.format(dcd.getDataCollectionGroup().getStartDate()));
		dataCollectionElement.appendChild(startDateElement);

		Element endDateCreationElement = document.createElement("enddatecreation");
		if (dcd.getDataCollectionGroup().getEndDateCreation() != null) {
			endDateCreationElement.setTextContent(sdf.format(dcd.getDataCollectionGroup().getEndDateCreation()));
		}
		dataCollectionElement.appendChild(endDateCreationElement);

		Element endDateSubmissionElement = document.createElement("enddatesubmission");
		if (dcd.getDataCollectionGroup().getEndDateSubmission() != null) {
			endDateSubmissionElement.setTextContent(sdf.format(dcd.getDataCollectionGroup().getEndDateSubmission()));
		}
		dataCollectionElement.appendChild(endDateSubmissionElement);

		Element endDateCommentsElement = document.createElement("enddatecomments");
		if (dcd.getDataCollectionGroup().getEndDateComments() != null) {
			endDateCommentsElement.setTextContent(sdf.format(dcd.getDataCollectionGroup().getEndDateComments()));
		}
		dataCollectionElement.appendChild(endDateCommentsElement);

		return dataCollectionElement;
	}

}
