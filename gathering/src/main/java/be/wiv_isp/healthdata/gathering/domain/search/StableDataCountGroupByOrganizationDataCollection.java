/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.gathering.domain.search;

public class StableDataCountGroupByOrganizationDataCollection {

    private String organizationIdentificationValue;
    private String dataCollectionName;
    private Long count;
    private Long distinctPatientIdCount;

    public StableDataCountGroupByOrganizationDataCollection(String organizationIdentificationValue, String dataCollectionName, Long count, Long distinctPatientIdCount) {
        this.organizationIdentificationValue = organizationIdentificationValue;
        this.dataCollectionName = dataCollectionName;
        this.count = count;
        this.distinctPatientIdCount = distinctPatientIdCount;
    }

    public String getOrganizationIdentificationValue() {
        return organizationIdentificationValue;
    }

    public void setOrganizationIdentificationValue(String organizationIdentificationValue) {
        this.organizationIdentificationValue = organizationIdentificationValue;
    }

    public String getDataCollectionName() {
        return dataCollectionName;
    }

    public void setDataCollectionName(String dataCollectionName) {
        this.dataCollectionName = dataCollectionName;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getDistinctPatientIdCount() {
        return distinctPatientIdCount;
    }

    public void setDistinctPatientIdCount(Long distinctPatientIdCount) {
        this.distinctPatientIdCount = distinctPatientIdCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof StableDataCountGroupByOrganizationDataCollection))
            return false;

        StableDataCountGroupByOrganizationDataCollection that = (StableDataCountGroupByOrganizationDataCollection) o;

        if (organizationIdentificationValue != null ? !organizationIdentificationValue.equals(that.organizationIdentificationValue) : that.organizationIdentificationValue != null)
            return false;
        if (dataCollectionName != null ? !dataCollectionName.equals(that.dataCollectionName) : that.dataCollectionName != null)
            return false;
        if (count != null ? !count.equals(that.count) : that.count != null)
            return false;
        return !(distinctPatientIdCount != null ? !distinctPatientIdCount.equals(that.distinctPatientIdCount) : that.distinctPatientIdCount != null);

    }

    @Override
    public int hashCode() {
        int result = organizationIdentificationValue != null ? organizationIdentificationValue.hashCode() : 0;
        result = 31 * result + (dataCollectionName != null ? dataCollectionName.hashCode() : 0);
        result = 31 * result + (count != null ? count.hashCode() : 0);
        result = 31 * result + (distinctPatientIdCount != null ? distinctPatientIdCount.hashCode() : 0);
        return result;
    }
}
