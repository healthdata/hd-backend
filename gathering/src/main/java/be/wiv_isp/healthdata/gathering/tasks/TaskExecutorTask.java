/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.tasks;

import be.wiv_isp.healthdata.orchestration.tasks.AbstractTaskExecutorTask;
import be.wiv_isp.healthdata.orchestration.tasks.HealthDataTask;
import be.wiv_isp.healthdata.orchestration.tasks.ParticipationGroupMigrationTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TaskExecutorTask extends AbstractTaskExecutorTask {

    @Autowired
    private FollowUpCreationTask followUpCreationTask;
    @Autowired
    private ElasticSearchRebuildTask elasticSearchRebuildTask;
    @Autowired
    private NoteMigrationTask noteMigrationTask;
    @Autowired
    private ParticipationGroupMigrationTask participationGroupMigrationTask;

    @Override
    protected List<HealthDataTask> getExecutableTasks() {
        final List<HealthDataTask> tasks = new ArrayList<>();
        tasks.add(followUpCreationTask);
        tasks.add(elasticSearchRebuildTask);
        tasks.add(noteMigrationTask);
        tasks.add(participationGroupMigrationTask);
        return tasks;
    }
}
