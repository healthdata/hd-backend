/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.gathering.dto;

import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import be.wiv_isp.healthdata.common.util.StringUtils;
import be.wiv_isp.healthdata.gathering.domain.FastTrackRecord;
import be.wiv_isp.healthdata.gathering.domain.FastTrackUpload;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;

public class FastTrackUploadStatusDto {

    public FastTrackUploadStatusDto(FastTrackUpload upload) {
        mode = upload.getMode();
        master = upload.getMaster();
        dataCollectionDefinitionId = upload.getDataCollectionDefinitionId();
        status = upload.getStatus();
        totalCount = upload.getTotalCount();
        startedOn = upload.getStartedOn();
        stoppedOn = upload.getStoppedOn();
    }


    private Long dataCollectionDefinitionId;
    private FastTrackUpload.Mode mode;
    private FastTrackUpload.Master master;
    private Long totalCount;
    private long successCount;
    private long ignoreCount;
    private long errorCount;
    private FastTrackUpload.Status status;
    private String errors;
    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp createdOn;
    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp startedOn;
    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp stoppedOn;

    private final SortedSet<FastTrackUploadStatusDto.LineStatus> records = new TreeSet<>();

    public SortedSet<LineStatus> getRecords() {
        return records;
    }

    public void setRecords(List<FastTrackRecord> records) {
        for (FastTrackRecord record : records) {
            FastTrackUploadStatusDto.LineStatus lineStatus = new FastTrackUploadStatusDto.LineStatus();
            lineStatus.setLineNumber(record.getLineNumber());
            lineStatus.setStatus(record.getStatus().toString());
            switch (record.getStatus()) {
                case SUCCESS:
                    successCount++;
                    break;
                case ERROR:
                    errorCount++;
                    break;
                case IGNORE:
                    ignoreCount++;
                    break;
            }
            lineStatus.setMessage(record.getErrors());
            this.records.add(lineStatus);
        }
    }

    public Long getDataCollectionDefinitionId() {
        return dataCollectionDefinitionId;
    }

    public void setDataCollectionDefinitionId(Long dataCollectionDefinitionId) {
        this.dataCollectionDefinitionId = dataCollectionDefinitionId;
    }

    public FastTrackUpload.Mode getMode() {
        return mode;
    }

    public void setMode(FastTrackUpload.Mode mode) {
        this.mode = mode;
    }

    public FastTrackUpload.Master getMaster() {
        return master;
    }

    public void setMaster(FastTrackUpload.Master master) {
        this.master = master;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    public long getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(long successCount) {
        this.successCount = successCount;
    }

    public long getIgnoreCount() {
        return ignoreCount;
    }

    public void setIgnoreCount(long ignoreCount) {
        this.ignoreCount = ignoreCount;
    }

    public long getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(long errorCount) {
        this.errorCount = errorCount;
    }

    public FastTrackUpload.Status getStatus() {
        return status;
    }

    public void setStatus(FastTrackUpload.Status status) {
        this.status = status;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getStartedOn() {
        return startedOn;
    }

    public void setStartedOn(Timestamp startedOn) {
        this.startedOn = startedOn;
    }

    public Timestamp getStoppedOn() {
        return stoppedOn;
    }

    public void setStoppedOn(Timestamp stoppedOn) {
        this.stoppedOn = stoppedOn;
    }

    public class LineStatus implements Comparable<FastTrackUploadStatusDto.LineStatus> {

        private long lineNumber;

        private String status;

        private String line;

        private String message;

        public long getLineNumber() {
            return lineNumber;
        }

        public void setLineNumber(long lineNumber) {
            this.lineNumber = lineNumber;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getLine() {
            return line;
        }

        public void setLine(String line) {
            this.line = line;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }

            if (o instanceof FastTrackUploadStatusDto.LineStatus) {
                FastTrackUploadStatusDto.LineStatus other = (FastTrackUploadStatusDto.LineStatus) o;

                return Objects.equals(lineNumber, other.lineNumber)
                        && Objects.equals(status, other.status)
                        && Objects.equals(message, other.message);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return Objects.hash(
                    this.lineNumber,
                    this.status,
                    this.message);
        }

        @Override
        public String toString() {
            return StringUtils.toString(this);
        }

        @Override
        public int compareTo(FastTrackUploadStatusDto.LineStatus compare) {
            final int BEFORE = -1;
            final int EQUAL = 0;
            final int AFTER = 1;

            if (this == compare)
                return EQUAL;

            if (this.lineNumber < compare.lineNumber)
                return BEFORE;
            if (this.lineNumber > compare.lineNumber)
                return AFTER;

            int comparison = nullSafeStringComparator(this.status, compare.status);
            if (comparison != EQUAL) {
                return comparison;
            }
            comparison = nullSafeStringComparator(this.message, compare.message);
            if (comparison != EQUAL) {
                return comparison;
            }

            assert this.equals(compare) : "compareTo inconsistent with equals.";

            return EQUAL;
        }

        public int nullSafeStringComparator(final String one, final String two) {
            final int BEFORE = -1;
            final int EQUAL = 0;
            final int AFTER = 1;

            if (one == null ^ two == null) {
                return (one == null) ? BEFORE : AFTER;
            }

            if (one == null && two == null) {
                return EQUAL;
            }

            return one.compareToIgnoreCase(two);
        }
    }


    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof FastTrackUploadStatusDto) {
            FastTrackUploadStatusDto other = (FastTrackUploadStatusDto) o;

            return Objects.deepEquals(records, other.records);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                this.status);
    }

    @Override
    public String toString() {
        return StringUtils.toString(this);
    }
}
