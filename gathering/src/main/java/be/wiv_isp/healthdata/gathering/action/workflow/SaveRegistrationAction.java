/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.action.workflow;

import be.wiv_isp.healthdata.common.domain.RegistryDependentIdGenerationDefinition;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataAssert;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.centralplatform.service.ICentralPlatformService;
import be.wiv_isp.healthdata.gathering.domain.ElasticSearchInfo;
import be.wiv_isp.healthdata.gathering.domain.MappingContext;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.factory.IMappingContextFactory;
import be.wiv_isp.healthdata.gathering.service.IFollowUpService;
import be.wiv_isp.healthdata.gathering.service.IMappingService;
import be.wiv_isp.healthdata.orchestration.action.workflow.ReportableAction;
import be.wiv_isp.healthdata.orchestration.api.mapper.MappingResponseMapper;
import be.wiv_isp.healthdata.orchestration.domain.DataSource;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.TypedValue;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import be.wiv_isp.healthdata.orchestration.service.IAttachmentService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionDefinitionForwardService;
import be.wiv_isp.healthdata.orchestration.service.IElasticSearchInfoService;
import be.wiv_isp.healthdata.orchestration.service.INoteMigrationService;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.core.Response;
import java.util.*;

public class SaveRegistrationAction extends AbstractRegistrationWorkflowAction implements ReportableAction {

	private static final Logger LOG = LoggerFactory.getLogger(SaveRegistrationAction.class);

	@Autowired
	private IElasticSearchInfoService<ElasticSearchInfo> elasticSearchInfoService;
	@Autowired
	private IDataCollectionDefinitionForwardService catalogueService;
	@Autowired
	private IFollowUpService followUpService;
	@Autowired
	private IAttachmentService attachmentService;
	@Autowired
	private INoteMigrationService<RegistrationWorkflow> noteMigrationService;
	@Autowired
	private ICentralPlatformService centralPlatformService;
	@Autowired
	private IMappingService mappingService;
	@Autowired
	private IMappingContextFactory mappingContextFactory;

	@Override
	public WorkflowActionType getAction() {
		return WorkflowActionType.SAVE;
	}

	@Override
	public RegistrationWorkflow preExecute(RegistrationWorkflow workflow, HDUserDetails userDetails) {
		workflow = super.preExecute(workflow, userDetails);

		dataCollectionDefinition = catalogueService.get(dataCollectionDefinitionId, userDetails.getOrganization());
		if (!userDataCollectionService.isUserAuthorized(userDetails, dataCollectionDefinition.getDataCollectionName())) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.UNAUTHORIZED_ACTION_ON_WORKFLOW, userDetails.getUsername(), "NEW");
			throw exception;
		}

		if (workflow == null && !dataCollectionDefinition.getDataCollectionGroup().isValidForCreation(new Date())) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_DATE_FOR_DATA_COLLECTION_DEFINITION, dataCollectionDefinition.getDataCollectionName(), "creation");
			throw exception;
		}

		if(StringUtils.isNotBlank(uniqueID)) {
			Long workflowId = null;
			Organization organization = userDetails.getOrganization();

			if(workflow != null) {
				workflowId = workflow.getId();
				organization = workflow.getOrganization();
				dataCollectionDefinitionId = workflow.getDataCollectionDefinitionId();
			}

			if (!workflowService.isUnique(dataCollectionDefinitionId, uniqueID, workflowId, organization)) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.CREATE_DUPLICATE_KEY, uniqueID);
				throw exception;
			}
		}

		if (documentData == null) {
			documentData = new DocumentData();
			documentData.setContent(documentContent);
		} else if (metaData == null || metaData.isEmpty()) {
			metaData = null;
		}

		if(toBeCoded != null) {
			// remove empty entries, otherwise they would be saved as NULL by Oracle
			for (Iterator<Map.Entry<String, TypedValue>> it = toBeCoded.entrySet().iterator(); it.hasNext(); ) {
				Map.Entry<String, TypedValue> entry = it.next();
				if (entry.getValue().getValue().isEmpty())
					it.remove();
			}
		}

		if (documentData.getPrivate() != null) {
			documentData.setCoded(toBeCoded);
		}

		if(workflow == null) { // first save action
			LOG.debug("Checking whether a registry dependent identifier must be generated");
			final RegistryDependentIdGenerationDefinition def = centralPlatformService.getRegistryDependentIdGenerationDefinition(dataCollectionDefinition.getDataCollectionName());
			final boolean generateRegistryDependentId = def.isGenerationEnabled() && RegistryDependentIdGenerationDefinition.Phase.CREATION.equals(def.getPhase());
			LOG.debug("Registry dependent identifier must be generated: {}", generateRegistryDependentId);
			if (generateRegistryDependentId) {
				registryDependentId = centralPlatformService.getRegistryDependentId(userDetails.getOrganization(), dataCollectionDefinition.getDataCollectionName(), RegistryDependentIdGenerationDefinition.Phase.CREATION);
				HealthDataAssert.assertNotNull(registryDependentId, Response.Status.INTERNAL_SERVER_ERROR, "generated registry dependent identifier is empty");

				LOG.debug("Injecting registry dependent identifier in the document data");
				final MappingContext context = mappingContextFactory.create(userDetails.getOrganization(), null, registryDependentId);
				documentData = mappingService.refresh(context, dataCollectionDefinition.getContent(), documentData, metaData != null ? metaData : documentData.getPatientID());
			}
		}

		return workflow;
	}

	@Override
	public RegistrationWorkflow doExecute(RegistrationWorkflow workflow, HDUserDetails userDetails) {

		if (workflow == null) {
			LOG.debug("Workflow does not exist, creating new workflow.");

			workflow = workflowService.create(userDetails.getOrganization());
			workflow.setDataCollectionDefinitionId(dataCollectionDefinition.getId());
			workflow.setDataCollectionName(dataCollectionDefinition.getDataCollectionName());
			workflow.setFollowUps(followUpService.createFollowUps(dataCollectionDefinition.getFollowUpDefinitions()));
			workflow.setRegistryDependentId(registryDependentId);

			final ElasticSearchInfo elasticSearchInfo = new ElasticSearchInfo();
			elasticSearchInfo.setWorkflow(workflow);
			elasticSearchInfo.setStatus(ElasticSearchInfo.Status.NOT_INDEXED);
			elasticSearchInfo.setRetryCount(0);
			elasticSearchInfoService.create(elasticSearchInfo);
		}

		final List<DataSource> sourceOfData = createSourceOfData(workflow);
		workflowService.setDocumentData(workflow, this.documentData, sourceOfData, false);
		attachmentService.linkAttachments(workflow.getDocument(), attachments);
		workflow.setMetaData(metaData != null ? metaData : this.documentData.getPatientID());
		workflow.setUniqueID(uniqueID);

		if(workflow.getStatus() == null || WorkflowStatus.IN_PROGRESS.equals(workflow.getStatus())) {
			setEndStatus(WorkflowStatus.IN_PROGRESS);
		} else if(WorkflowStatus.ACTION_NEEDED.equals(workflow.getStatus())) {
			setEndStatus(WorkflowStatus.ACTION_NEEDED);

			followUpService.updateScheduling(workflow.getFollowUps(), computedExpressions);
			boolean newActiveFollowUp = followUpService.activateIfValid(workflow.getFollowUps(), computedExpressions);
			if(newActiveFollowUp) {
				workflow.getFlags().setFollowUp(true);
				workflowService.createNewDocumentVersion(workflow);
			}
		} else {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.ACTION_NOT_ALLOWED_ON_STATUS, this.getAction(), workflow.getStatus());
			throw exception;
		}

		if (!noteMigrationService.isMigrated(workflow))
			noteMigrationService.migrateCommentsToNotes(workflow);

		return workflow;
	}

	private List<DataSource> createSourceOfData(RegistrationWorkflow workflow) {
		final List<DataSource> sourceOfData = (workflow.getDocument()==null) ? new ArrayList<DataSource>() : workflow.getDocument().getDataSources();

		if (MapUtils.isEmpty(affectedFields))
			return sourceOfData;

		final Map<String, JSONObject> mappedAffectedFields = MappingResponseMapper.mapDataSources(affectedFields, "Manual");

		final Set<String> existingFieldPaths = new HashSet<>();

		for (DataSource dataSource : sourceOfData) {
			try {
				final String fieldPath = dataSource.getFieldPath();
				existingFieldPaths.add(fieldPath);
				if (mappedAffectedFields.keySet().contains(fieldPath))
					dataSource.setSource(mappedAffectedFields.get(fieldPath).getString("reason"));
			}
			catch (JSONException e) {
				continue;
			}
		}

		for (String fieldPath : mappedAffectedFields.keySet()) {
			try {
				if (!existingFieldPaths.contains(fieldPath)) {
					DataSource dataSource = new DataSource();
					dataSource.setFieldPath(fieldPath);
					dataSource.setSource(mappedAffectedFields.get(fieldPath).getString("reason"));
					sourceOfData.add(dataSource);
				}
			}
			catch (JSONException e) {
				continue;
			}
		}

		return sourceOfData;
	}

}
