/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.patientid.connectors.impl;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.NationalRegisterConnector;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.dto.PersonDto;
import be.wiv_isp.healthdata.gathering.patientid.connectors.INationalRegisterConnector;
import be.wiv_isp.healthdata.gathering.patientid.mapper.IPersonDtoMapper;
import be.wiv_isp.healthdata.xml_connector.HealthDataXmlConnector;
import be.wiv_isp.healthdata.xml_connector.domain.Person;
import be.wiv_isp.healthdata.xml_connector.domain.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

@Component
public class CustomRestNrcInterface implements INationalRegisterConnector {

	private static final Logger LOG = LoggerFactory.getLogger(CustomRestNrcInterface.class);

	@Autowired
	private IPersonDtoMapper<Person> customRestNrcResponseMapper;

	@Autowired
	private IConfigurationService configurationService;

	@Override
	public PersonDto getBySsinNumber(String inss, Organization organization) {

		final Request request = new Request();
		request.setSsin(inss);
		request.setUrl(configurationService.get(ConfigurationKey.CUSTOM_REST_NRC_URL, organization).getValue());

		try {
			request.setUser(configurationService.get(ConfigurationKey.CUSTOM_REST_NRC_USER, organization).getValue());
		} catch (Exception e) {
			LOG.warn("No USER configured for the PatientIdentification");
		}

		try {
			request.setPassword(configurationService.get(ConfigurationKey.CUSTOM_REST_NRC_PASSWORD, organization).getValue());
		} catch (Exception e) {
			LOG.warn("No PASSWORD configured for the PatientIdentification");
		}

		final Person person = new HealthDataXmlConnector().execute(request);
		return customRestNrcResponseMapper.map(person);
	}

	@Override
	public PersonDto getByInternalNumber(String id, Organization organization) {
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.METHOD_NOT_ALLOWED, MessageFormat.format("getByInternalNumber using {0} interface", getNationalRegisterConnector()));
		throw exception;
	}

	@Override
	public NationalRegisterConnector getNationalRegisterConnector() {
		return NationalRegisterConnector.CUSTOM_REST_NRC;
	}
}
