/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.gathering.api;

import be.wiv_isp.healthdata.common.caching.ICacheManagementService;
import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;

@Component
@Path("/cache")
public class CacheManagementRestService {

    private static final Logger LOG = LoggerFactory.getLogger(CacheManagementRestService.class);

    @Autowired
    private ICacheManagementService cacheManagementService;

    @GET
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCacheNames() {
        return Response.ok(cacheManagementService.getCacheNames(Platform.HD4DP)).build();
    }

    @POST
    @Path("/evict")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Response evict(@Context UriInfo info) {
        String cacheName = null;
        MultivaluedMap<String, String> params = info.getQueryParameters();
        for (String key : params.keySet()) {
            if ("cacheName".equalsIgnoreCase(key))
                cacheName = params.getFirst(key);
        }
        if (cacheName == null)
            return Response.status(Response.Status.BAD_REQUEST).build();

        LOG.debug("Evicting cache [{}]", cacheName);

        cacheManagementService.evict(cacheName);

        return Response.noContent().build();
    }

}
