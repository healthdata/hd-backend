/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.common.api.ClientVersion;
import be.wiv_isp.healthdata.common.caching.CacheManagementService;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupListDto;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupListMap;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.gathering.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.search.ParticipationWorkflowSearch;
import be.wiv_isp.healthdata.gathering.service.IParticipationWorkflowService;
import be.wiv_isp.healthdata.orchestration.api.uri.DataCollectionGroupUri;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionGroupForwardService;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractDataCollectionGroupForwardService;
import com.sun.jersey.api.client.GenericType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;
import java.util.List;

@Service
public class DataCollectionGroupForwardService extends AbstractDataCollectionGroupForwardService implements IDataCollectionGroupForwardService {

	@Autowired
	private IParticipationWorkflowService participationWorkflowService;

	@Override
	@Cacheable(value = CacheManagementService.dataCollectionGroupCache)
	public DataCollectionGroupListMap list(Organization organization, Boolean withContent, Boolean latestVersion) {

		final Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

		final DataCollectionGroupUri dataCollectionGroupUri= new DataCollectionGroupUri(catalogueHost.getValue());

		final WebServiceBuilder wsb = new WebServiceBuilder();

		wsb.setUrl(dataCollectionGroupUri.toString() + "/list");
		wsb.setGet(true);
		wsb.addParameter(RestUtils.ParameterName.IDENTIFICATION_TYPE, organization.getHealthDataIDType());
		wsb.addParameter(RestUtils.ParameterName.IDENTIFICATION_VALUE, organization.getHealthDataIDValue());
		if (withContent != null)
			wsb.addParameter("withContent", withContent.toString());
		if (latestVersion != null)
			wsb.addParameter("latestVersion", latestVersion.toString());
		wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb.setReturnType(new GenericType<DataCollectionGroupListMap>() {});
		wsb.addHeader(HttpHeaders.CLIENT_VERSION, ClientVersion.DataCollectionDefinition.getDefault());
		wsb.setLogResponseBody(false);

		DataCollectionGroupListMap dataCollectionGroupListMap = (DataCollectionGroupListMap) webServiceClientService.callWebService(wsb);
		for (List<DataCollectionGroupListDto> dataCollectionGroupListDtos : dataCollectionGroupListMap.values()) {
			for (DataCollectionGroupListDto dataCollectionGroupListDto : dataCollectionGroupListDtos) {
				ParticipationWorkflowSearch search = new ParticipationWorkflowSearch();
				search.setOrganization(organization);
				search.setDataCollectionDefinitionId(dataCollectionGroupListDto.getDataCollectionGroupId());
				search.setWithDocument(null);
				List<ParticipationWorkflow> participationWorkflows = participationWorkflowService.getAll(search);
				if(!participationWorkflows.isEmpty()) {
					dataCollectionGroupListDto.setParticipationId(participationWorkflows.get(0).getId());
				}
			}
		}
		return dataCollectionGroupListMap;
	}
}
