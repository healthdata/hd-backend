/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.dao.impl;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.gathering.dao.IRegistrationWorkflowDao;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationCountGroupByOrganizationDcdStatus;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.gathering.dto.ParticipationCreationDto;
import be.wiv_isp.healthdata.orchestration.dao.impl.AbstractRegistrationWorkflowDao;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class RegistrationWorkflowDao extends AbstractRegistrationWorkflowDao<RegistrationWorkflow, RegistrationWorkflowSearch> implements IRegistrationWorkflowDao{

	public RegistrationWorkflowDao() {
		super(RegistrationWorkflow.class);
	}

	@Override
	protected List<Predicate> getPredicates(RegistrationWorkflowSearch search, CriteriaBuilder cb, Root<RegistrationWorkflow> rootEntry) {
		List<Predicate> predicates = super.getPredicates(search, cb, rootEntry);

		if (search.getUniqueID() != null) {
			predicates.add(cb.equal(rootEntry.get("uniqueID"), search.getUniqueID()));
		}
		if (search.getFollowUpId() != null) {
			predicates.add(cb.equal(rootEntry.join("followUps").get("id"), search.getFollowUpId()));
		}
		return predicates;
	}

	@Override
	public List<ParticipationCreationDto> getParticipationCreationInfo() {
		TypedQuery<ParticipationCreationDto> query = em.createNamedQuery("ParticipationCreationInfo", ParticipationCreationDto.class);
		return query.getResultList();
	}

	@Override
	public List<RegistrationCountGroupByOrganizationDcdStatus> getCountGroupByOrganizationDcdStatus() {
		final CriteriaBuilder cb = em.getCriteriaBuilder();
		final CriteriaQuery<RegistrationCountGroupByOrganizationDcdStatus> cq = cb.createQuery(RegistrationCountGroupByOrganizationDcdStatus.class);
		final Root<RegistrationWorkflow> rootEntry = cq.from(RegistrationWorkflow.class);
		cq.multiselect(rootEntry.<Organization>get("organization").get("healthDataIDValue"), rootEntry.get("dataCollectionDefinitionId"), rootEntry.get("status"), cb.countDistinct(rootEntry));
		cq.groupBy(rootEntry.<Organization>get("organization").get("healthDataIDValue"), rootEntry.get("dataCollectionDefinitionId"), rootEntry.get("status"));
		final TypedQuery<RegistrationCountGroupByOrganizationDcdStatus> allQuery = em.createQuery(cq);
		return allQuery.getResultList();
	}

}
