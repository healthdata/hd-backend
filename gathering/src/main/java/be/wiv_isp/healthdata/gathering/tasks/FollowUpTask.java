/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.tasks;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.gathering.action.workflow.UpdateFollowUpAction;
import be.wiv_isp.healthdata.gathering.action.workflow.UpdateStatusRegistrationAction;
import be.wiv_isp.healthdata.gathering.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.service.IFollowUpService;
import be.wiv_isp.healthdata.gathering.service.IRegistrationWorkflowService;
import be.wiv_isp.healthdata.orchestration.action.workflow.executor.IRegistrationWorkflowActionExecutor;
import be.wiv_isp.healthdata.orchestration.domain.FollowUp;
import be.wiv_isp.healthdata.orchestration.domain.HDServiceUser;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.domain.search.FollowUpSearch;
import be.wiv_isp.healthdata.orchestration.factory.IAbstractMessageFactory;
import be.wiv_isp.healthdata.orchestration.factory.IActionFactory;
import be.wiv_isp.healthdata.orchestration.service.IIdentificationService;
import be.wiv_isp.healthdata.orchestration.service.IMappingService;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import be.wiv_isp.healthdata.orchestration.tasks.HealthDataTask;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.*;

@Component
public class FollowUpTask extends HealthDataTask {

	private static final Logger LOG = LoggerFactory.getLogger(FollowUpTask.class);

	@Autowired
	private IFollowUpService followUpService;

	@Autowired
	private IRegistrationWorkflowService workflowService;

	@Autowired
	private IAbstractMessageFactory<RegistrationWorkflow, ParticipationWorkflow> messageFactory;

	@Autowired
	private IMessageService messageService;

	@Autowired
	private IIdentificationService identificationService;

	@Autowired
	private IRegistrationWorkflowActionExecutor<RegistrationWorkflow> workflowActionExecutor;

	@Autowired
	private IMappingService<RegistrationWorkflow> mappingService;

	@Autowired
	private IActionFactory actionFactory;

	@Autowired
	@Qualifier("transactionManager")
	protected PlatformTransactionManager txManager;

	@Override
	public String getName() {
		return "FOLLOW_UP";
	}

	@Override
	public void execute() {
		final Date activationDateUpperBound = Calendar.getInstance().getTime();
		final FollowUpSearch search = new FollowUpSearch();
		search.setActivationDateBefore(Calendar.getInstance().getTime());
		search.setActive(false);
		LOG.debug("Retrieving non-active follow-ups for which the activation date is before {}", DateFormat.DATE_AND_TIME.format(activationDateUpperBound));
		final List<FollowUp> all = followUpService.getAll(search);
		LOG.debug("{} matching follow-up item(s) found", all.size());

		final Set<Long> workflowIds = new HashSet<>();
		for (final FollowUp followUp : all) {
			workflowIds.add(workflowService.getByFollowUpId(followUp.getId()).getId());
		}

		for (final Long workflowId : workflowIds) {
			LOG.debug("Registration[{}]: processing follow-ups", workflowId);

			TransactionTemplate tmpl = new TransactionTemplate(txManager);
			tmpl.execute(new TransactionCallbackWithoutResult() {
				@Override
				protected void doInTransactionWithoutResult(TransactionStatus status) {
					RegistrationWorkflow workflow = workflowService.get(workflowId);
					final JSONObject computedExpressions = mappingService.getComputedExpressions(workflow);
					followUpService.updateScheduling(workflow.getFollowUps(), computedExpressions);
					boolean newActiveFollowUps = followUpService.activateIfValid(workflow.getFollowUps(), computedExpressions);
					if (newActiveFollowUps) {
						LOG.debug("Notifying HD4RES of follow-up changes and update workflow flags and status to {} (if necessary)", WorkflowStatus.ACTION_NEEDED);

						workflow.getFlags().setFollowUp(true);
						workflowService.createNewDocumentVersion(workflow);
						workflow = workflowService.update(workflow);

						if (!WorkflowStatus.ACTION_NEEDED.equals(workflow.getStatus())) {
							final UpdateStatusRegistrationAction updateStatusAction = (UpdateStatusRegistrationAction) actionFactory.getActionByActionType(WorkflowActionType.UPDATE_STATUS, WorkflowType.REGISTRATION);
							updateStatusAction.setEndStatus(WorkflowStatus.ACTION_NEEDED);
							workflowActionExecutor.execute(updateStatusAction, HDServiceUser.create(HDServiceUser.Username.FOLLOW_UP_UPDATER), workflow);
						}

						final UpdateFollowUpAction action = new UpdateFollowUpAction();
						action.setFollowUps(workflow.getFollowUps());
						action.setIdentificationWorkflow(identificationService.getHealthDataIdentification(workflow.getOrganization()));
						final Message message = messageFactory.createOutgoingMessage(action, workflow);
						messageService.create(message);
					}
				}
			});
		}
	}
}
