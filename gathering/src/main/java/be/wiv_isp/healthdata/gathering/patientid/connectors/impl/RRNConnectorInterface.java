/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.patientid.connectors.impl;

import be.healthconnect.rrnconnector.ws._1_0.person.PersonType;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.NationalRegisterConnector;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.dto.PersonDto;
import be.wiv_isp.healthdata.gathering.patientid.connectors.INationalRegisterConnector;
import be.wiv_isp.healthdata.gathering.patientid.mapper.IPersonDtoMapper;
import be.wiv_isp.healthdata.healthconnect_connector.HealthConnectConnector;
import be.wiv_isp.healthdata.healthconnect_connector.domain.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

@Component
public class RRNConnectorInterface implements INationalRegisterConnector {

	@Autowired
	private IPersonDtoMapper<PersonType> RRNConnectorResponseMapper;

	@Autowired
	private IConfigurationService configurationService;

	@Override
	public PersonDto getBySsinNumber(String ssin, Organization organization) {
		final Request request = new Request();
		request.setSsin(ssin);
		request.setUrl(configurationService.get(ConfigurationKey.RRNCONNECTOR_URL, organization).getValue());
		request.setApplicationToken(configurationService.get(ConfigurationKey.RRNCONNECTOR_APPLICATION_TOKEN, organization).getValue());
		request.setUserId(configurationService.get(ConfigurationKey.RRNCONNECTOR_USER_ID, organization).getValue());
		final PersonType response = new HealthConnectConnector().execute(request);
		return RRNConnectorResponseMapper.map(response);
	}

	@Override
	public PersonDto getByInternalNumber(String id, Organization organization) {
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.METHOD_NOT_ALLOWED, MessageFormat.format("getByInternalNumber using {0} interface", getNationalRegisterConnector()));
		throw exception;
	}

	@Override
	public NationalRegisterConnector getNationalRegisterConnector() {
		return NationalRegisterConnector.RRNCONNECTOR;
	}

}
