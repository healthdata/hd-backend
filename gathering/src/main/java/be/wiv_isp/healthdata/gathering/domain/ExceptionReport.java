/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.gathering.domain;

import be.wiv_isp.healthdata.orchestration.annotation.Truncate;
import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "EXCEPTION_REPORT")
public class ExceptionReport {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "EXCEPTION_ID")
    private Long id;

    @Access(AccessType.PROPERTY)
    @Column(name = "MESSAGE")
    private String message;

    @Access(AccessType.PROPERTY)
    @Column(name = "HASH", nullable = false)
    private String hash;

    @Lob
    @Access(AccessType.PROPERTY)
    @Column(name = "STACK_TRACE")
    private String stackTrace;

    @Fetch(FetchMode.JOIN)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "EXCEPTION_REPORT_UUIDS", joinColumns = @JoinColumn(name = "EXCEPTION_ID"))
    @Column(name = "UUID")
    private Set<String> uuids;

    @Column(name = "CREATED_ON", nullable = false)
    private Timestamp createdOn;

    @Column(name = "REPORTED_ON")
    private Timestamp reportedOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    @Truncate(maxLength = 255)
    public void setMessage(String message) {
        this.message = message;
        this.hash = null;
    }

    public String getHash() {
        if(hash == null) {
            computeHash();
        }
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
        this.hash = null;
    }

    public Set<String> getUuids() {
        return uuids;
    }

    public void setUuids(Set<String> uuids) {
        this.uuids = uuids;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getReportedOn() {
        return reportedOn;
    }

    public void setReportedOn(Timestamp reportedOn) {
        this.reportedOn = reportedOn;
    }

    @PrePersist
    public void onCreate() {
        createdOn = new Timestamp(new Date().getTime());
    }

    private void computeHash() {
        if(getStackTrace() != null) {
            hash = DigestUtils.md5Hex(getStackTrace());
        } else if(getMessage() != null) {
            hash = DigestUtils.md5Hex(getMessage());
        }
    }
}
