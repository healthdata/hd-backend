/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.factory.impl;

import be.wiv_isp.healthdata.gathering.domain.MappingContext;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.factory.IMappingContextFactory;
import be.wiv_isp.healthdata.orchestration.domain.FollowUp;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MappingContextFactory implements IMappingContextFactory {

    @Autowired
    private IConfigurationService configurationService;

    @Override
    public MappingContext create(RegistrationWorkflow workflow) {
        final MappingContext context = new MappingContext();
        context.setInstallationId(configurationService.get(ConfigurationKey.HEALTHDATA_ID_VALUE, workflow.getOrganization()).getValue());
        context.setFollowUps(workflow.getFollowUps());
        context.setRegistryDependentId(workflow.getRegistryDependentId());
        return context;
    }

    @Override
    public MappingContext create(Organization organization) {
        return create(organization, null, null);
    }

    @Override
    public MappingContext create(Organization organization, List<FollowUp> followUps, String registryDependentId) {
        final MappingContext context = new MappingContext();
        context.setInstallationId(organization.getHealthDataIDValue());
        context.setFollowUps(followUps);
        context.setRegistryDependentId(registryDependentId);
        return context;
    }

}

