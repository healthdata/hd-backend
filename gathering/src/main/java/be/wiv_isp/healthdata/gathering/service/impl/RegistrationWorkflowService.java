/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.gathering.dao.IRegistrationWorkflowDao;
import be.wiv_isp.healthdata.gathering.domain.ElasticSearchInfo;
import be.wiv_isp.healthdata.gathering.domain.RegistrationDocument;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationCountGroupByOrganizationDcdStatus;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.gathering.dto.ParticipationCreationDto;
import be.wiv_isp.healthdata.gathering.service.IRegistrationWorkflowService;
import be.wiv_isp.healthdata.orchestration.domain.DataSource;
import be.wiv_isp.healthdata.orchestration.domain.FollowUp;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.RegistrationWorkflowHistory;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractRegistrationWorkflowService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.*;

@Service
public class RegistrationWorkflowService extends AbstractRegistrationWorkflowService<RegistrationWorkflow, RegistrationWorkflowSearch, RegistrationDocument, ElasticSearchInfo> implements IRegistrationWorkflowService {

	private static final Logger LOG = LoggerFactory.getLogger(RegistrationWorkflowService.class);

	public RegistrationWorkflowService() {
		super(RegistrationWorkflow.class);
	}

	@Override
	public RegistrationWorkflow getEntityInstance() {
		return new RegistrationWorkflow();
	}

	@Override
	public RegistrationWorkflowSearch getSearchInstance() {
		return new RegistrationWorkflowSearch();
	}

	@Autowired
	protected IRegistrationWorkflowDao workflowDao;

	@Override
	public IRegistrationWorkflowDao getDao() {
		return workflowDao;
	}

	@Override
	public boolean isUnique(long dataCollectionDefinitionId, String uniqueID, Long notWorkflowId, Organization organization) {
		RegistrationWorkflowSearch search = getSearchInstance();
		search.setDataCollectionDefinitionId(dataCollectionDefinitionId);
		search.setNotWorkflowId(notWorkflowId);
		search.setUniqueID(uniqueID);
		search.setOrganization(organization);
		long count = getDao().count(search);
		return count == 0L;
	}

	@Override
	public List<RegistrationWorkflow> getUnique(long dataCollectionDefinitionId, String uniqueID, Organization organization) {
		RegistrationWorkflowSearch search = getSearchInstance();
		search.setDataCollectionDefinitionId(dataCollectionDefinitionId);
		search.setUniqueID(uniqueID);
		search.setOrganization(organization);
		return getDao().getAll(search);
	}

	@Override
	public List<ParticipationCreationDto> getParticipationCreationInfo() {
		return getDao().getParticipationCreationInfo();
	}

	@Override
	public void setDocumentData(RegistrationWorkflow workflow, DocumentData documentData, List<DataSource> dataSources, boolean addHistoryEntry) {
		LOG.debug(MessageFormat.format("Setting document content for workflow [{0}]", workflow.getId().toString()));
		RegistrationDocument document =  workflow.getDocument();

		if (document == null) {
			LOG.debug("Document does not exist, creating new document");
			document = getDocumentService().getEntityInstance();
			document.setDocumentData(documentData);
			if (dataSources != null)
				document.setDataSources(dataSources);
			document.setWorkflow(workflow);
			document.setVersion((long) workflow.getDocumentHistory().size());
			document = getDocumentService().create(document);
			workflow.setDocument(document);
			workflow.getDocumentHistory().add(document);
		} else if (addHistoryEntry) {
			LOG.debug("Document exists and new history entry must be added, creating new document");
			document = getDocumentService().getEntityInstance();
			document.setDocumentData(documentData);
			if (dataSources != null)
				document.setDataSources(dataSources);
			document.setWorkflow(workflow);
			document.setVersion((long) workflow.getDocumentHistory().size());
			document = getDocumentService().create(document);
			workflow.setDocument(document);
			workflow.getDocumentHistory().add(document);
		} else {
			LOG.debug("Document exists and new history entry must not be added, updating existing document");
			document.setDocumentData(documentData);
			if (dataSources != null)
				document.setDataSources(dataSources);
			getDocumentService().update(document);
		}
	}

	@Override
	@Transactional
	public void createNewDocumentVersion(RegistrationWorkflow workflow) {
		DocumentData documentData = new DocumentData();
		documentData.setContent(workflow.getDocument().getDocumentContent());
		documentData.setPrivate(workflow.getDocument().getPrivateContent());
		documentData.setCoded(new HashMap<>(workflow.getDocument().getCodedContent()));
		List<DataSource> dataSources = new ArrayList<>(workflow.getDocument().getDataSources());
		setDocumentData(workflow, documentData, dataSources, true); // to add a history entry
	}

	@Override
	public List<RegistrationCountGroupByOrganizationDcdStatus> getCountGroupByOrganizationDcdStatus() {
		return workflowDao.getCountGroupByOrganizationDcdStatus();
	}

	@Override
	public RegistrationWorkflow getByFollowUpId(Long followUpId) {
		final RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
		search.setFollowUpId(followUpId);
		return getUnique(search);
	}

	@Override
	protected Platform getPlatform() {
		return Platform.HD4DP;
	}

	@Override
	public boolean isActionAvailable(RegistrationWorkflow workflow, WorkflowActionType action) {
		if(!super.isActionAvailable(workflow, action)) {
			return false;
		}
		if(WorkflowActionType.SUBMIT.equals(action)) {
			return isSubmittable(workflow);
		}
		if(WorkflowActionType.DELETE.equals(action)) {
			return isDeletable(workflow);
		}
		return true;
	}

	private boolean isSubmittable(RegistrationWorkflow workflow) {
		final Long dcdId = workflow.getDataCollectionDefinitionId();
		final Organization organization = workflow.getOrganization();
		final DataCollectionDefinitionDtoV7 dcd = catalogueService.get(dcdId, organization);

		if(isFirstSubmission(workflow)) {
			return dcd.getDataCollectionGroup().isValidForSubmission(new Date());
		} else {
			return dcd.getDataCollectionGroup().isValidForComments(new Date());
		}
	}

	private boolean isFirstSubmission(RegistrationWorkflow workflow) {
		if(workflow.getHistory() != null) {
			for (RegistrationWorkflowHistory history : workflow.getHistory()) {
				if (WorkflowStatus.ACTION_NEEDED.equals(history.getNewStatus()) && history.getFlags().isCorrections()) {
					return false;
				}
			}
		}
		return true;
	}

	public static boolean isDeletable(RegistrationWorkflow workflow) {
		final List<FollowUp> followUps = workflow.getFollowUps();
		if(CollectionUtils.isNotEmpty(followUps)) {
			for (FollowUp followUp : followUps) {
				if (followUp.isActive() || followUp.getSubmittedOn() != null) {
					return false;
				}
			}
		}

		final List<RegistrationWorkflowHistory> history = workflow.getHistory();
		if(CollectionUtils.isNotEmpty(history)) {
			for (RegistrationWorkflowHistory h : history) {
				if(WorkflowStatus.APPROVED.equals(h.getNewStatus())) {
					return false;
				}
			}
		}

		// if the last action that was executed between REOPEN and UPDATE_FOR_CORRECTION is
		// REOPEN: the registration can not be deleted
		// UPDATE_FOR_CORRECTION: the registration can be deleted
		if(CollectionUtils.isNotEmpty(history)) {
			Collections.sort(history, new HistoryComparator());
			final ListIterator<RegistrationWorkflowHistory> it = history.listIterator(history.size());
			while (it.hasPrevious()) {
				final RegistrationWorkflowHistory h = it.previous();
				if (WorkflowActionType.REOPEN.equals(h.getAction())) {
					return false;
				}
				if (WorkflowActionType.UPDATE_FOR_CORRECTION.equals(h.getAction())) {
					return true;
				}
			}
		}

		return true;
	}

	private static class HistoryComparator implements java.util.Comparator<RegistrationWorkflowHistory> {
		@Override
		public int compare(RegistrationWorkflowHistory h1, RegistrationWorkflowHistory h2) {
			return h1.getExecutedOn().compareTo(h2.getExecutedOn());
		}
	}
}