/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.patientid.mapper.impl;

import be.healthconnect.rrnconnector.ws._1_0.person.Genders;
import be.healthconnect.rrnconnector.ws._1_0.person.PersonType;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.gathering.dto.PersonDto;
import be.wiv_isp.healthdata.gathering.patientid.mapper.IPersonDtoMapper;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Service
public class RRNConnectorResponseMapper implements IPersonDtoMapper<PersonType> {

	private static final SimpleDateFormat SDF = new SimpleDateFormat(DateFormat.DATE.getPattern());

	@Override
	public PersonDto map(PersonType rnPerson) {
		final PersonDto personDto = new PersonDto();
		personDto.setSsin(rnPerson.getINSS());
		personDto.setLastName(rnPerson.getName().getLastName());
		personDto.setFirstName(rnPerson.getName().getFirstName());
		if (rnPerson.getBirth() != null) {
			try {
				personDto.setDateOfBirth(SDF.parse(rnPerson.getBirth().getDate()));
			} catch (ParseException e) {
				personDto.setDateOfBirth(null);
			}
		}
		if (Genders.MALE.equals(rnPerson.getGender().getValue())) {
			personDto.setGender(PersonDto.Gender.MALE);
		} else if (Genders.FEMALE.equals(rnPerson.getGender().getValue())) {
			personDto.setGender(PersonDto.Gender.FEMALE);
		} else {
			personDto.setGender(PersonDto.Gender.UNKNOWN);
		}
		personDto.setDistrict(rnPerson.getAddress().getMunicipality().getPostalCode());
		if (rnPerson.getDecease() != null) {
			personDto.setDeceased(true);
			try {
				personDto.setDateOfDeath(SDF.parse(rnPerson.getDecease().getDate()));
			} catch (ParseException e) {
				personDto.setDateOfDeath(null);
			}
		} else {
			personDto.setDeceased(false);
		}
		return personDto;
	}
}
