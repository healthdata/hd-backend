/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.patientid.connectors.impl;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.NationalRegisterConnector;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.dto.PersonDto;
import be.wiv_isp.healthdata.gathering.patientid.connectors.INationalRegisterConnector;
import be.wiv_isp.healthdata.gathering.patientid.mapper.IPersonDtoMapper;
import be.wiv_isp.healthdata.xconnect_connector.XConnectConnector;
import be.wiv_isp.healthdata.xconnect_connector.domain.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xconnect.RNSearchBySSINResponse;

import java.text.MessageFormat;
import java.util.Calendar;

@Component
public class XConnectInterface implements INationalRegisterConnector {

    @Autowired
    private IConfigurationService configurationService;

    @Autowired
    private IPersonDtoMapper<RNSearchBySSINResponse> xConnectResponseMapper;

    @Override
    public NationalRegisterConnector getNationalRegisterConnector() {
        return NationalRegisterConnector.XCONNECT;
    }

    @Override
    public PersonDto getBySsinNumber(String ssin, Organization organization) {
        final Request request = new Request();
        request.setSsin(ssin);
        request.setUrl(configurationService.get(ConfigurationKey.XCONNECT_CONNECTOR_URL, organization).getValue());
        request.setIdentificationValue(configurationService.get(ConfigurationKey.XCONNECT_CONNECTOR_IDENTIFICATION_VALUE, organization).getValue());
        request.setApplicationId(configurationService.get(ConfigurationKey.XCONNECT_CONNECTOR_APPLICATION_ID, organization).getValue());
        request.setCampus(configurationService.get(ConfigurationKey.XCONNECT_CONNECTOR_CAMPUS, organization).getValue());

        Calendar calendar = Calendar.getInstance();
        final String start = DateFormat.DATE.format(calendar.getTime());
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        final String end = DateFormat.DATE.format(calendar.getTime());
        request.setStart(start);
        request.setEnd(end);

        final RNSearchBySSINResponse response = new XConnectConnector().execute(request);
        return xConnectResponseMapper.map(response);
    }

    @Override
    public PersonDto getByInternalNumber(String id, Organization organization) {
        HealthDataException exception = new HealthDataException();
        exception.setExceptionType(ExceptionType.METHOD_NOT_ALLOWED, MessageFormat.format("getByInternalNumber using {0} interface", getNationalRegisterConnector()));
        throw exception;
    }
}
