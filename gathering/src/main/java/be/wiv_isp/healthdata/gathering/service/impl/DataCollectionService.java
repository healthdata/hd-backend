/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.common.caching.CacheManagementService;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.gathering.service.IDataCollectionService;
import be.wiv_isp.healthdata.orchestration.api.uri.DataCollectionUri;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractDataCollectionService;
import com.sun.jersey.api.client.GenericType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Service
public class DataCollectionService extends AbstractDataCollectionService implements IDataCollectionService {

    @SuppressWarnings("unchecked")
    @Override
    @Cacheable(value = CacheManagementService.dataCollectionCache)
    public Map<String, Set<String>> getDataCollections(Organization organization) {
        Map<String, Set<String>> union = new HashMap<>();
        union.putAll(getPrivateDataCollections(organization));
        union.putAll(getPublicDataCollections());

        return union;
    }

    private Map<String, Set<String>> getPrivateDataCollections(Organization organization) {
        if (StringUtils.isAnyBlank(organization.getHealthDataIDType(), organization.getHealthDataIDValue()))
            return new HashMap<>();

        Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

        final DataCollectionUri dataCollectionUri = new DataCollectionUri();
        dataCollectionUri.setHost(catalogueHost.getValue());

        WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.setUrl(dataCollectionUri.toString());
        wsb.addHeader(HttpHeaders.CLIENT_VERSION, getClientVersion());
        wsb.setGet(true);
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.addParameter(RestUtils.ParameterName.IDENTIFICATION_TYPE, organization.getHealthDataIDType());
        wsb.addParameter(RestUtils.ParameterName.IDENTIFICATION_VALUE, organization.getHealthDataIDValue());
        wsb.setReturnType(new GenericType<Map<String,Set<String>>>() {
        });

        return (Map<String, Set<String>>) webServiceClientService.callWebService(wsb);
    }

    @Override
    public Set<String> getStableData() {
        Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

        final DataCollectionUri dataCollectionUri = new DataCollectionUri();
        dataCollectionUri.setHost(catalogueHost.getValue());

        WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.setUrl(dataCollectionUri + "/stabledata");
        wsb.addHeader(HttpHeaders.CLIENT_VERSION, getClientVersion());
        wsb.setGet(true);
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.setReturnType(new GenericType<Set<String>>() {
        });

        return (Set<String>) webServiceClientService.callWebService(wsb);
    }

    @Override
    @Cacheable(value = CacheManagementService.dataCollectionCache)
    public boolean isAccessGranted(Organization organization, String dataCollectionName) {
        Map<String, Set<String>> dataCollectionNames = getDataCollections(organization);
        for (Map.Entry<String, Set<String>> collection : dataCollectionNames.entrySet()) {
            if (collection.getKey().equalsIgnoreCase(dataCollectionName)) {
                return true;
            }
            for (String c : collection.getValue()) {
                if (c.equalsIgnoreCase(dataCollectionName))
                    return true;
            }
        }
        return false;
    }
}
