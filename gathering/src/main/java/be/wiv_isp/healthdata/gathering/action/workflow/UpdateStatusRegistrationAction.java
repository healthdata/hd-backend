/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.action.workflow;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.CSVContent;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.Note;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.service.INoteService;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.List;

public class UpdateStatusRegistrationAction extends AbstractRegistrationWorkflowAction {

    @Autowired
    private INoteService noteService;

    @Override
    public WorkflowActionType getAction() {
        return WorkflowActionType.UPDATE_STATUS;
    }

    @Override
    public RegistrationWorkflow doExecute(RegistrationWorkflow workflow, HDUserDetails userDetails) {
        if (WorkflowStatus.APPROVED.equals(getEndStatus()) && !workflow.getDocument().getVersion().equals(documentVersion)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.IGNORE_ACTION, MessageFormat.format("executing {0} action with end status {1} on old document version ({2})", getAction(), getEndStatus(), documentVersion));
            throw exception;
        }
        if (WorkflowStatus.APPROVED.equals(getEndStatus())) {
            List<Note> notes = noteService.getByDocumentId(WorkflowType.REGISTRATION, workflow.getDocument().getId());
            for (Note note : notes) {
                note.setResolved(true);
                noteService.update(note);
            }
        }
        return workflow;
    }

    @Override
    public void extractInfo(Message message) {
        final CSVContent csvContent = new CSVContent(new String(message.getContent(), StandardCharsets.UTF_8));
        setWorkflowId(Long.valueOf(csvContent.getWorkflowId()));
    }

    @Override
    public WorkflowType getWorkflowType() {
        return WorkflowType.REGISTRATION;
    }

    @Override
    public String toString() {
        return MessageFormat.format("{0} (end status: {1})", this.getAction(), this.getEndStatus());
    }
}
