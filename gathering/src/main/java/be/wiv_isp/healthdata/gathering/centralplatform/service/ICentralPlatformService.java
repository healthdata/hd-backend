/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.gathering.centralplatform.service;

import be.wiv_isp.healthdata.common.domain.RegistryDependentIdGenerationDefinition;
import be.wiv_isp.healthdata.gathering.centralplatform.dto.*;
import be.wiv_isp.healthdata.gathering.domain.Upgrade;
import be.wiv_isp.healthdata.gathering.dto.InstallationDto;
import be.wiv_isp.healthdata.gathering.dto.UserRequestDto;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.dto.OrganizationDto;
import org.codehaus.jettison.json.JSONObject;

import java.util.List;

public interface ICentralPlatformService {

    InstallationDto registerInstallation();

    void registerUpgrade(Upgrade upgrade);

    void updateOrganizations(List<OrganizationDto> organizations);

    void updateRegistrationStatistics(List<RegistrationStatisticDto> counts);

    void updateStableDataStatistics(List<StableDataStatisticDto> counts);

    void updateUsers(List<UserDto> users);

    void updateUserRequests(List<UserRequestDto> userRequestDtos);

    void updateConfigurations(List<ConfigurationDto> configurations);

    void updateMessageStatistics(List<MessageStatisticDto> messages);

    void updateStatuses(JSONObject statuses);

    RegistryDependentIdGenerationDefinition getRegistryDependentIdGenerationDefinition(String dataCollectionName);

    String getRegistryDependentId(Organization organization, String dataCollectionName, RegistryDependentIdGenerationDefinition.Phase phase);

}
