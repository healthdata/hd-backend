/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.domain;

import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationWorkflow;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "WORKFLOWS")
@NamedQuery(name = "ParticipationCreationInfo",
		query = "Select NEW be.wiv_isp.healthdata.gathering.dto.ParticipationCreationDto(rw.dataCollectionDefinitionId, rw.dataCollectionName, rw.organization) from RegistrationWorkflow rw group by rw.dataCollectionDefinitionId, rw.dataCollectionName, rw.organization")
public class RegistrationWorkflow extends AbstractRegistrationWorkflow<RegistrationDocument> {

	public static final String NO_UNIQUE_ID = "NO_UNIQUE_ID";

	@Column(name = "UNIQUE_ID")
	protected String uniqueID;

	@Column(name = "REGISTRY_DEPENDENT_ID")
	protected String registryDependentId;


	public String getUniqueID() {
		return uniqueID;
	}

	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}

	public String getRegistryDependentId() {
		return registryDependentId;
	}

	public void setRegistryDependentId(String registryDependentId) {
		this.registryDependentId = registryDependentId;
	}

	@PreUpdate
	public void onUpdate() {
		super.onUpdate();
		if (StringUtils.isBlank(uniqueID)) {
			setUniqueID(NO_UNIQUE_ID);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof RegistrationWorkflow) {
			RegistrationWorkflow other = (RegistrationWorkflow) o;

			return Objects.equals(id, other.id)
					&& Objects.equals(uniqueID, other.uniqueID)
					&& Objects.equals(status, other.status)
					&& Objects.equals(documentHistory, other.documentHistory)
					&& Objects.equals(dataCollectionName, other.dataCollectionName)
					&& Objects.equals(dataCollectionDefinitionId, other.dataCollectionDefinitionId)
					&& Objects.equals(metaData, other.metaData)
					&& Objects.equals(createdOn, other.createdOn)
					&& Objects.equals(updatedOn, other.updatedOn)
					&& Objects.equals(history, other.history)
					&& Objects.equals(submittedOn, other.submittedOn)
					&& Objects.equals(organization, other.organization);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				this.id,
				this.uniqueID,
				this.status,
				this.documentHistory,
				this.dataCollectionName,
				this.dataCollectionDefinitionId,
				this.metaData,
				this.createdOn,
				this.updatedOn,
				this.history,
				this.submittedOn,
				this.organization);
	}

	@Override
	public String toString() {
		return "RegistrationWorkflow {" +
				"id = " + Objects.toString(this.id) + ", " +
				"uniqueID = " + Objects.toString(this.uniqueID) + ", " +
				"status = " + Objects.toString(this.status) + ", " +
				"document = " + Objects.toString(this.documentHistory) + ", " +
				"dataCollectionName = " + Objects.toString(this.dataCollectionName) + ", " +
				"dataCollectionDefinitionId = " + Objects.toString(this.dataCollectionDefinitionId) + ", " +
				"metaData = " + Objects.toString(this.metaData) + ", " +
				"createdOn = " + Objects.toString(this.createdOn) + ", " +
				"updatedOn = " + Objects.toString(this.updatedOn) + ", " +
				"history = " + Objects.toString(this.history) + ", " +
				"submittedOn = " + Objects.toString(this.submittedOn) + ", " +
				"organization = " + Objects.toString(this.organization) + "}";
	}
}