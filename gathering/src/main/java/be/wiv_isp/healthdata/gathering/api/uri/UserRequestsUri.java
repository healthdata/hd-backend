/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.gathering.api.uri;

import java.text.MessageFormat;

public class UserRequestsUri {

    public static final String URI_PATTERN = "{0}/centralplatform/installations/{1}/userRequests";

    private String host;
    private Long installationId;

    public void setHost(String host) {
        this.host = host;
    }

    public void setInstallationId(Long installationId) {
        this.installationId = installationId;
    }

    @Override
    public String toString() {
        return MessageFormat.format(URI_PATTERN, host, installationId);
    }

}
