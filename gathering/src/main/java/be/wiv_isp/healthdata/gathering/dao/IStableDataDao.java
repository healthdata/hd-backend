/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.dao;

import be.wiv_isp.healthdata.common.dao.ICrudDaoV2;
import be.wiv_isp.healthdata.gathering.domain.StableData;
import be.wiv_isp.healthdata.gathering.domain.search.StableDataCountGroupByOrganizationDataCollection;
import be.wiv_isp.healthdata.gathering.domain.search.StableDataSearch;
import be.wiv_isp.healthdata.gathering.domain.view.StableDataView;

import java.util.List;

public interface IStableDataDao extends ICrudDaoV2<StableData, Long, StableDataSearch> {

	StableDataView create(StableDataView stableDataView);

	List<StableDataCountGroupByOrganizationDataCollection> getCountGroupByOrganizationDataCollection();

}
