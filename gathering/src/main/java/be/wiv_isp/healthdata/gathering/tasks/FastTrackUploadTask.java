/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.gathering.tasks;

import be.wiv_isp.healthdata.gathering.domain.FastTrackUpload;
import be.wiv_isp.healthdata.gathering.service.IFastTrackService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.Date;

public class FastTrackUploadTask implements Runnable {
	
	private static final Logger LOG = LoggerFactory.getLogger(FastTrackUploadTask.class);

	private String name;
	private FastTrackUpload upload;

	@Autowired
	private IFastTrackService fastTrackService;

	public FastTrackUpload getUpload() {
		return upload;
	}

	public void setUpload(FastTrackUpload upload) {
		this.upload = upload;
	}

	@Override
	public void run() {
		LOG.debug("Running [{}] task", getName());
		Date startTime = new Date();
		upload.setStartedOn(new Timestamp(startTime.getTime()));
		fastTrackService.createUpload(upload);
		fastTrackService.upload(upload);
		Date endTime = new Date();
		upload.setStoppedOn(new Timestamp(endTime.getTime()));
		fastTrackService.updateUpload(upload);
		LOG.debug("[{}] task finished (duration: {} seconds)", getName(), (endTime.getTime() - startTime.getTime()) / 1000);
	}

	public String getName() {
		if(StringUtils.isBlank(name)) {
			name = be.wiv_isp.healthdata.common.util.StringUtils.toStringExclude(upload, "content", "totalCount", "successCount", "ignoreCount", "errorCount", "status", "errors", "records", "startedOn", "stoppedOn");
		}
		return name;
	}

	public void execute() {

	}
}
