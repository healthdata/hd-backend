/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.tasks;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.impl.CsvService;
import be.wiv_isp.healthdata.orchestration.util.FileSystemUtils;
import be.wiv_isp.healthdata.gathering.domain.FastTrackRecord;
import be.wiv_isp.healthdata.gathering.domain.FastTrackUpload;
import be.wiv_isp.healthdata.gathering.service.IFastTrackService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionDefinitionForwardService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import be.wiv_isp.healthdata.orchestration.tasks.SchedulableHealthDataTask;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.csv.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.*;

@Service
public class FastTrackTask  extends SchedulableHealthDataTask {

    private static final Logger LOG = LoggerFactory.getLogger(FastTrackTask.class);
    private static final String CSV_EXTENSION = ".csv";
    public static final String SUCCESS = "SUCCESS";
    public static final String ERROR = "ERROR";
    public static final String ARCHIVE = "ARCHIVE";
    public static final String ERROR_ARCHIVE = "ERROR_ARCHIVE";
    public static final String IGNORE = "IGNORE";

    @Autowired
    protected IFastTrackService fastTrackService;
    @Autowired
    protected IDataCollectionService dataCollectionService;
    @Autowired
    protected IOrganizationService organizationService;
    @Autowired
    protected IDataCollectionDefinitionForwardService catalogueService;

    @Override
    public String getName() {
        return "FAST_TRACK";
    }

    @Override
    protected String getTimingExpression() {
        return configurationService.get(ConfigurationKey.SCHEDULED_TASK_PROVISIONING_INTERVAL).getValue();
    }

    @Override
    public void execute() {
        final String directory = configurationService.get(ConfigurationKey.FAST_TRACK_DIRECTORY).getValue();
        if(StringUtils.isNotBlank(directory)) {
            File folder = new File(directory);
            processFolders(folder);
        } else {
            LOG.warn("no fast track directory was set in the configurations");
        }
    }

    private void processFolders(File folder) {
        final List<Organization> allOrganizations = organizationService.getAll();
        for (Organization organization : allOrganizations) {
            LOG.trace("Start fast track for organization [{}]", organization);
            File subFolder = getOrCreateDirectory(folder, organization.getHealthDataIDValue());
            Set<String> dataCollections = dataCollectionService.getDataCollectionsFlat(organization);
            for (String dataCollection : dataCollections) {
                LOG.trace("Start fast track for organization [{}] and dataCollection [{}]", organization, dataCollection);
                File subSubFolder = getOrCreateDirectory(subFolder, dataCollection);
                getOrCreateDirectory(subSubFolder, SUCCESS);
                getOrCreateDirectory(subSubFolder, ERROR);
                getOrCreateDirectory(subSubFolder, ARCHIVE);
                getOrCreateDirectory(subSubFolder, IGNORE);
                processFolder(subSubFolder, organization, dataCollection);
            }
        }
    }

    private void processFolder(File folder, Organization organization, String dataCollection) {
        File[] csvFiles = folder.listFiles((FilenameFilter) new SuffixFileFilter(CSV_EXTENSION));
        if (csvFiles == null) {
            LOG.error(MessageFormat.format("Error while reading files from directory [{0}]", folder.getAbsolutePath()));
        } else if (csvFiles.length != 0) {
            LOG.info(MessageFormat.format("[{0}] files found in [{1}]", csvFiles.length, folder.getAbsolutePath()));
            for (File csvFile : csvFiles) {
                try {
                    if (!csvFile.canWrite()) {
                        LOG.error(MessageFormat.format("File[{0}]: no permission to delete the file so it is skipped.", csvFile.getName()));
                        continue;
                    }
                    DataCollectionDefinitionDtoV7 dataCollectionDefinition = catalogueService.getLatest(dataCollection, organization);
                    if (dataCollectionDefinition != null) {
                        LOG.trace("using DCD [{}]", dataCollectionDefinition);
                        FastTrackUpload upload = new FastTrackUpload();
                        upload.setDataCollectionDefinitionId(dataCollectionDefinition.getId());
                        upload.setPath(csvFile.getParent());
                        upload.setFileName(csvFile.getName());
                        FileInputStream fis = FileUtils.openInputStream(csvFile);
                        upload.setContent(IOUtils.toString(new BOMInputStream(fis), StandardCharsets.UTF_8));
                        fis.close();
                        upload.setOrganizationId(organization.getId());
                        upload.setMode(FastTrackUpload.Mode.FAST_TRACK);
                        Date startTime = new Date();
                        upload.setStartedOn(new Timestamp(startTime.getTime()));
                        fastTrackService.createUpload(upload);
                        fastTrackService.upload(upload);
                        Date endTime = new Date();
                        upload.setStoppedOn(new Timestamp(endTime.getTime()));
                        fastTrackService.updateUpload(upload);
                        if(StringUtils.isEmpty(upload.getErrors())) {
                            processResponse(folder, upload);
                            LOG.info(MessageFormat.format("File[{0}]: file successfully processed, moving file to archive directory.", csvFile.getName()));
                            FileSystemUtils.moveFilesToDirectory(getOrCreateDirectory(folder, ARCHIVE), true, getFilesToMove(csvFile, upload));
                        } else {
                            LOG.error(MessageFormat.format("File[{0}]: file unsuccessfully processed, moving file to error archive directory.", csvFile.getName()), upload.getErrors());
                            FileSystemUtils.moveFilesToDirectory(getOrCreateDirectory(folder, ERROR_ARCHIVE), true, getFilesToMove(csvFile, upload));
                        }
                    } else {
                        LOG.warn("no active data collection found for [{}]", dataCollection);
                    }
                } catch (Exception e) {
                    LOG.error(MessageFormat.format("File[{0}]: file unsuccessfully processed, moving file to error archive directory.", csvFile.getName()), e);
                    FileSystemUtils.moveFilesToDirectory(getOrCreateDirectory(folder, ERROR_ARCHIVE), true, csvFile);
                }
            }
        }
    }

    private void processResponse(File folder, FastTrackUpload upload) throws IOException {
        LOG.trace("processing response for file [{}].[{}]", upload.getPath(), upload.getFileName());
        String fileName = FilenameUtils.removeExtension(upload.getFileName());

        CSVPrinter successPrinter = null;
        CSVPrinter ignorePrinter = null;
        CSVPrinter errorPrinter = null;
        Reader reader = null ;
        try {
            reader = new StringReader(upload.getContent());
            final CSVFormat csvFormat = CSVFormat.DEFAULT.withDelimiter(';')
                    .withEscape('\\')
                    .withQuoteMode(QuoteMode.MINIMAL)
                    .withHeader();
            CSVParser parser = new CSVParser(reader, csvFormat);

            final List<String> keys = new ArrayList<>(parser.getHeaderMap().keySet());
            final List<String> errorKeys = new ArrayList<>(parser.getHeaderMap().keySet());
            errorKeys.add("errorMessage");

            if (upload.hasSuccess()) {
                successPrinter = getPrinter(folder, fileName, csvFormat, SUCCESS, keys);
            }
            if (upload.hasIgnore()) {
                ignorePrinter = getPrinter(folder, fileName, csvFormat, IGNORE, keys);
            }
            if (upload.hasErrors()) {
                errorPrinter = getPrinter(folder, fileName, csvFormat, ERROR, errorKeys);
            }
            List<CSVRecord> records = parser.getRecords();

            Map<Long, FastTrackRecord> fastTrackRecords = upload.getRecords();
            for (CSVRecord record : records) {
                FastTrackRecord fastTrackRecord = fastTrackRecords.get(record.getRecordNumber());
                switch (fastTrackRecord.getStatus()) {
                    case SUCCESS:
                        successPrinter.printRecord(CsvService.getList(record));
                        break;
                    case IGNORE:
                        ignorePrinter.printRecord(CsvService.getList(record));
                        break;
                    case ERROR:
                        List<String> list = CsvService.getList(record);
                        list.add(fastTrackRecord.getErrors());
                        errorPrinter.printRecord(list);
                        break;
                }
            }
        } catch (IOException e) {
            LOG.error("couldn't process response", e);
            throw e;
        } finally {
            if(successPrinter != null) {
                successPrinter.close();
            }
            if(ignorePrinter != null) {
                ignorePrinter.close();
            }
            if(errorPrinter != null) {
                errorPrinter.close();
            }
            if(reader != null) {
                reader.close();
            }
        }
    }

    private CSVPrinter getPrinter(File folder, String fileNamePrefix, CSVFormat csvFormat, String status, List<String> keys) throws IOException {
        File statusFolder = getOrCreateDirectory(folder, status);
        StringBuilder fileName = new StringBuilder();
        fileName.append(fileNamePrefix);
        fileName.append("_");
        fileName.append(status);
        fileName.append(".csv");

        File daySubFolderUnique = FileSystemUtils.getDaySubFolderUnique(statusFolder, fileName.toString());
        FileUtils.forceMkdir(daySubFolderUnique);

        StringBuilder canonicalFileName = new StringBuilder();
        canonicalFileName.append(daySubFolderUnique.getCanonicalPath());
        canonicalFileName.append(File.separator);
        canonicalFileName.append(fileName);
        PrintWriter writer = new PrintWriter(canonicalFileName.toString(), "UTF-8");

        return csvFormat.withHeader(keys.toArray(new String[keys.size()])).print(writer);
    }

    private PrintWriter getWriter(File folder, String name, String status) throws IOException {
        File successFolder = getOrCreateDirectory(folder, status);
        StringBuilder fileName = new StringBuilder();
        fileName.append(successFolder.getCanonicalPath());
        fileName.append(File.separator);
        fileName.append(name);
        fileName.append("_");
        fileName.append(status);
        fileName.append(".csv");
        return new PrintWriter(fileName.toString(), "UTF-8");
    }

    private File getOrCreateDirectory(File folder, String directoryName) {
        LOG.trace("get or create directory [{}] in folder [{}]", directoryName, folder.getAbsolutePath());
        for (File file : folder.listFiles()) {
            if (file.isDirectory() && directoryName.equalsIgnoreCase(file.getName())) {
                return file;
            }
        }
        final File direcotory = Paths.get(folder.getAbsolutePath(), directoryName).toFile();
        if (!direcotory.exists()) {
            LOG.info("create directory [{}] in folder [{}]", directoryName, folder.getAbsolutePath());
            direcotory.mkdir();
        }
        return direcotory;
    }

    static public File[] getFilesToMove(File csvFile, FastTrackUpload upload) {
        final List<File> filesToMove = new ArrayList<>();
        filesToMove.add(csvFile);
        if(CollectionUtils.isNotEmpty(upload.getAttachmentFiles())) {
            filesToMove.addAll(upload.getAttachmentFiles());
        }
        return filesToMove.toArray(new File[filesToMove.size()]);
    }
}



