/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.api.mapper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import be.wiv_isp.healthdata.gathering.domain.enumeration.ExecutionOption;
import be.wiv_isp.healthdata.gathering.dto.RegistrationRequestDtoV1;

public class RegistrationRequestMapperV1 implements RegistrationMapper<RegistrationRequestDtoV1, String> {

	@Override
	public RegistrationRequestDtoV1 convert(String xml) throws Exception {
		RegistrationRequestDtoV1 request = new RegistrationRequestDtoV1();

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(new InputSource(new ByteArrayInputStream(xml.getBytes("utf-8"))));
		Element documentElement = document.getDocumentElement();
		NodeList childNodes = documentElement.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			Node child = childNodes.item(i);
			if ("options".equalsIgnoreCase(child.getNodeName())) {
				NodeList optionList = child.getChildNodes();
				for (int j = 0; j < optionList.getLength(); j++) {
					Node option = optionList.item(j);
					if ("saveOnValidationError".equalsIgnoreCase(option.getNodeName())) {
						if (Boolean.valueOf(option.getTextContent())) {
							request.addOption(ExecutionOption.STOP_ON_VALID_ERROR);
						}
					} else if ("autoSubmit".equalsIgnoreCase(option.getNodeName())) {
						if (Boolean.valueOf(option.getTextContent())) {
							request.addOption(ExecutionOption.AUTO_SUBMIT);
						}
					} else if ("dryRun".equalsIgnoreCase(option.getNodeName())) {
						if (Boolean.valueOf(option.getTextContent())) {
							request.addOption(ExecutionOption.DRY_RUN);
						}
					}
				}
			} else if ("dataCollectionDefinitionId".equalsIgnoreCase(child.getNodeName())) {
				request.setDataCollectionDefinitionId(Long.valueOf(child.getTextContent()));
			} else if ("payload".equalsIgnoreCase(child.getNodeName())) {
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
				NodeList kmehrList = child.getChildNodes();
				DOMSource domSource = new DOMSource(kmehrList.item(1));
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				StreamResult streamResult = new StreamResult(bos);
				transformer.transform(domSource, streamResult);
				request.setDocument(bos.toByteArray());
			}
		}
		return request;
	}
}
