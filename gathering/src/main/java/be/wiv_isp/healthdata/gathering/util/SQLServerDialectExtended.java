/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.gathering.util;

import org.hibernate.dialect.SQLServerDialect;

import java.sql.Types;

public class SQLServerDialectExtended extends SQLServerDialect {

    public SQLServerDialectExtended() {
        super();
        registerColumnType( Types.VARBINARY, "varbinary" );
        registerColumnType( Types.LONGVARBINARY, "varbinary" );
        registerColumnType( Types.BLOB, "varbinary" );
        registerColumnType( Types.CLOB, "varchar" );
    }

}
