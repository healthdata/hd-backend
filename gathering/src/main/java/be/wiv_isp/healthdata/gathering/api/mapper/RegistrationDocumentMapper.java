/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.gathering.api.mapper;

import be.wiv_isp.healthdata.gathering.domain.RegistrationDocument;
import be.wiv_isp.healthdata.orchestration.api.mapper.AbstractRegistrationDocumentMapper;
import be.wiv_isp.healthdata.orchestration.dto.DocumentDto;
import org.springframework.stereotype.Component;

@Component
public class RegistrationDocumentMapper extends AbstractRegistrationDocumentMapper<RegistrationDocument> {

    @Override
    public DocumentDto getDtoInstance(RegistrationDocument document) {
        DocumentDto documentDto = new DocumentDto(document);
        if (document.getPrivateContent() == null)
            documentDto.getDocumentData().setPatientID(document.getWorkflow().getMetaData());
        else
            documentDto.getDocumentData().setPrivate(document.getPrivateContent());
        documentDto.getDocumentData().setCoded(null); // do not return on HD4DP

        return documentDto;
    }

}
