/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.gathering.dto.ElasticSearchWorkflowDto;
import be.wiv_isp.healthdata.gathering.dto.RegistrationWorkflowDto;
import be.wiv_isp.healthdata.orchestration.dto.NoteDto;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractElasticSearchService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ElasticSearchService extends AbstractElasticSearchService<RegistrationWorkflow, RegistrationWorkflowSearch, ElasticSearchWorkflowDto, RegistrationWorkflowDto> {

	@Override
	public ElasticSearchWorkflowDto getDtoInstance(RegistrationWorkflowDto dto, List<NoteDto> notes) {
		return new ElasticSearchWorkflowDto(dto, notes);
	}

	@Override
	protected RegistrationWorkflowSearch getWorkflowSearchInstance() {
		return new RegistrationWorkflowSearch();
	}

	@Override
	protected boolean ignoreDeletedWorkflows() {
		return true;
	}
}
