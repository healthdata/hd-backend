/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.dao.impl;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.gathering.domain.UserRequest;
import be.wiv_isp.healthdata.gathering.domain.search.UserRequestSearch;
import be.wiv_isp.healthdata.orchestration.dao.impl.AbstractUserRequestDao;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRequestDao extends AbstractUserRequestDao<UserRequest, UserRequestSearch> {

    public UserRequestDao() {
        super(UserRequest.class);
    }

    @Override
    protected List<Predicate> getPredicates(UserRequestSearch search, CriteriaBuilder cb, Root<UserRequest> rootEntry) {
        final List<Predicate> predicates = new ArrayList<>();

        predicates.addAll(getSharedPredicates(search, cb, rootEntry));
        if (StringUtils.isNotBlank(search.getUsername())) {
            predicates.add(cb.equal(rootEntry.get("username"), search.getUsername()));
        }
        if(search.getOrganizationId() != null) {
            Join<UserRequest, Organization> organization = rootEntry.join("organization");
            predicates.add(cb.equal(organization.get("id"), search.getOrganizationId()));
        }
        if (search.getApproved() == null) {
            predicates.add(cb.isNull(rootEntry.<Boolean>get("approved")));
        } else if(search.getApproved()) {
            predicates.add(cb.isTrue(rootEntry.<Boolean>get("approved")));
        } else {
            predicates.add(cb.isFalse(rootEntry.<Boolean>get("approved")));
        }
        return predicates;
    }
}
