/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.gathering.domain;

import be.wiv_isp.healthdata.common.util.StringUtils;
import be.wiv_isp.healthdata.gathering.domain.enumeration.FastTrackStatus;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "FAST_TRACK_RECORD")
@NamedQuery(name = "getForHash",
        query = "Select ftr from FastTrackRecord ftr, FastTrackUpload ftu where ftr.uploadId = ftu.id and ftr.status != be.wiv_isp.healthdata.gathering.domain.enumeration.FastTrackStatus.ERROR and ftu.dataCollectionDefinitionId = :dataCollectionDefinitionId and ftr.hash = :hash")
public class FastTrackRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "RECORD_ID")
    private Long id;
    @Column(name = "UPLOAD_ID")
    private Long uploadId;
    @Column(name = "HASH", nullable = false)
    private String hash;
    @Lob
    @Column(name = "ERRORS")
    private String errors;
    @Column(name = "LINE_NUMBER", nullable = false)
    private Long lineNumber;
    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS", nullable = false)
    private FastTrackStatus status;
    @Column(name = "WORKFLOW_ID")
    private Long workflowId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUploadId() {
        return uploadId;
    }

    public void setUploadId(Long uploadId) {
        this.uploadId = uploadId;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    public Long getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Long lineNumber) {
        this.lineNumber = lineNumber;
    }

    public FastTrackStatus getStatus() {
        return status;
    }

    public void setStatus(FastTrackStatus status) {
        this.status = status;
    }

    public Long getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(Long workflowId) {
        this.workflowId = workflowId;
    }


    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof FastTrackRecord) {
            FastTrackRecord other = (FastTrackRecord) o;

            return Objects.equals(id, other.id)
                    && Objects.equals(hash, other.hash)
                    && Objects.equals(errors, other.errors)
                    && Objects.equals(lineNumber, other.lineNumber)
                    && Objects.equals(status, other.status)
                    && Objects.equals(workflowId, other.workflowId);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                this.id,
                this.hash,
                this.errors,
                this.lineNumber,
                this.status,
                this.workflowId);
    }

    @Override
    public String toString() {
        return StringUtils.toStringExclude(this, "upload");
    }
}
