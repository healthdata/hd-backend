/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.gathering.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "INSTALLATION_DETAILS")
public class InstallationDetails {

    @Id
    @Column(name = "INSTALLATION_ID", nullable = true)
    private Long installationId;

    @Column(name = "CATALOGUE_USERNAME", nullable = true)
    private String catalogueUsername;

    @Column(name = "CATALOGUE_PASSWORD", nullable = true)
    private String cataloguePassword;

    public Long getInstallationId() {
        return installationId;
    }

    public void setInstallationId(Long installationId) {
        this.installationId = installationId;
    }

    public String getCatalogueUsername() {
        return catalogueUsername;
    }

    public void setCatalogueUsername(String catalogueUsername) {
        this.catalogueUsername = catalogueUsername;
    }

    public String getCataloguePassword() {
        return cataloguePassword;
    }

    public void setCataloguePassword(String cataloguePassword) {
        this.cataloguePassword = cataloguePassword;
    }
}
