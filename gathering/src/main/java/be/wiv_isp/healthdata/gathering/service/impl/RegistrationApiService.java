/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.common.domain.search.QueryResult;
import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.pagination.PaginationHelper;
import be.wiv_isp.healthdata.common.pagination.PaginationRange;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.gathering.action.workflow.SaveRegistrationAction;
import be.wiv_isp.healthdata.gathering.action.workflow.SubmitRegistrationAction;
import be.wiv_isp.healthdata.gathering.domain.RegistrationDocument;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.gathering.dto.ApiOptionsDto;
import be.wiv_isp.healthdata.gathering.dto.RegistrationApiDto;
import be.wiv_isp.healthdata.gathering.dto.ValidationErrorDto;
import be.wiv_isp.healthdata.gathering.factory.IMappingContextFactory;
import be.wiv_isp.healthdata.gathering.service.IDataCollectionService;
import be.wiv_isp.healthdata.gathering.service.IRegistrationApiService;
import be.wiv_isp.healthdata.gathering.service.IRegistrationWorkflowService;
import be.wiv_isp.healthdata.orchestration.action.workflow.executor.IRegistrationWorkflowActionExecutor;
import be.wiv_isp.healthdata.orchestration.api.mapper.MappingResponseMapper;
import be.wiv_isp.healthdata.orchestration.domain.DataSource;
import be.wiv_isp.healthdata.orchestration.domain.HDServiceUser;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import be.wiv_isp.healthdata.orchestration.dto.ValidationResponse;
import be.wiv_isp.healthdata.orchestration.factory.IActionFactory;
import be.wiv_isp.healthdata.orchestration.mapping.IMappingContext;
import be.wiv_isp.healthdata.orchestration.mapping.MappingResponse;
import be.wiv_isp.healthdata.orchestration.service.*;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import com.sun.org.apache.xerces.internal.dom.NodeImpl;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import javax.xml.XMLConstants;
import javax.xml.bind.*;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


@Service
public class RegistrationApiService implements IRegistrationApiService {

    private static final Logger LOG = LoggerFactory.getLogger(RegistrationApiService.class);

    private static final String VERSION = "v1";
    private static final String XML_SCHEMA_FILE = "hd4dp-registration-api-v1.xsd";

    @Autowired
    private IRegistrationWorkflowService registrationWorkflowService;
    @Autowired
    private IActionFactory actionFactory;
    @Autowired
    private IRegistrationWorkflowActionExecutor workflowActionExecutor;
    @Autowired
    private IMappingService mappingService;
    @Autowired
    private IMappingContextFactory mappingContextFactory;
    @Autowired
    private IDataCollectionDefinitionForwardService catalogueService;
    @Autowired
    private IDataCollectionDefinitionForwardService dataCollectionDefinitionService;
    @Autowired
    private IDataCollectionService dataCollectionService;
    @Autowired
    private IConfigurationService configurationService;
    @Autowired
    private INoteService noteService;

    private static Schema schema;

    @Override
    public String getVersion() {
        return VERSION;
    }

    @Override
    public Response get(Long id, Boolean validation) {
        LOG.info("Retrieving registration with id [{}].", id);

        RegistrationWorkflow workflow = registrationWorkflowService.get(id);
        if (workflow == null || workflow.getStatus() == WorkflowStatus.DELETED) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "registration", id);
            throw exception;
        }

        final HDUserDetails principal = AuthenticationService.getUserDetails();
        if (!dataCollectionService.isAccessGranted(principal.getOrganization(), workflow.getDataCollectionName())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_FORBIDDEN, MessageFormat.format("Access forbidden for registration with id [{0}] (no access to data collection).", id));
            throw exception;
        }

        ValidationResponse validationResponse = null;
        if (Boolean.TRUE.equals(validation)) { // TODO: get validation from elasticsearch?
            final DataCollectionDefinitionDtoV7 dataCollectionDefinition = catalogueService.get(workflow.getDataCollectionDefinitionId(), workflow.getOrganization());
            validationResponse = mappingService.getValidationResponse(workflow, null, dataCollectionDefinition.getContent());
        }

        return Response.ok(mapOutputApiDto(workflow, validationResponse, Response.Status.OK.getStatusCode())).status(Response.Status.OK.getStatusCode()).build();
    }

    @Override
    public Response getAll(RegistrationWorkflowSearch search, PaginationRange paginationRange, Boolean validation) {
        final HDUserDetails principal = AuthenticationService.getUserDetails();
        Organization organization = principal.getOrganization();
        search.setOrganization(organization);
        search.setPaginationRange(paginationRange);

        final QueryResult<RegistrationWorkflow> queryResult = registrationWorkflowService.getAllAsQueryResult(search);
        List<RegistrationWorkflow> workflows = queryResult.getResult();

        final String catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST).getValue();
        RegistrationApiDto.DataCollectionDto.setCatalogueHost(catalogueHost);
        RegistrationApiDto dto = new RegistrationApiDto();
        for (RegistrationWorkflow workflow : workflows) {
            RegistrationApiDto.RegistrationDto registrationDto = new RegistrationApiDto.RegistrationDto(workflow);
            if (Boolean.TRUE.equals(validation)) { // TODO: get validation from elasticsearch?
                final DataCollectionDefinitionDtoV7 dataCollectionDefinition = catalogueService.get(workflow.getDataCollectionDefinitionId(), workflow.getOrganization());
                ValidationResponse validationResponse = mappingService.getValidationResponse(workflow, null, dataCollectionDefinition.getContent());
                try {
                    registrationDto.setValidationErrors(mapValidationErrors(validationResponse.getResult()));
                } catch (JSONException je) {
                    HealthDataException exception = new HealthDataException(je);
                    exception.setExceptionType(ExceptionType.JSON_EXCEPTION);
                    throw exception;
                }
            }
            dto.addRegistration(registrationDto);
        }
        dto.setRegistrationList(workflows);
        dto.setHttpStatusCode(Response.Status.OK.getStatusCode());

        if(search.isPaginated()) {
            final String responseRange = PaginationHelper.buildResponseRangeString(queryResult.getRange());
            return Response.ok(dto).header(HttpHeaders.RANGE.getValue(), responseRange).build();
        } else {
            return Response.ok(dto).build();
        }
    }

    @Override
    public Response create(String xml) {
        LOG.info("Creating new registration.");

        // map xml to dto
        RegistrationApiDto inputDto = mapInputApiDto(xml);
        ApiOptionsDto options = inputDto.getOptions();
        Long dcdId = inputDto.getRegistration().getDataCollection().getId();

        // check dcd access and date range
        final HDUserDetails principal = AuthenticationService.getUserDetails();
        final Organization organization = principal.getOrganization();

        final DataCollectionDefinitionDtoV7 dataCollectionDefinition = catalogueService.get(dcdId, organization);
        if (dataCollectionDefinition == null) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_FORBIDDEN, MessageFormat.format("Organization [{0}] does not have access to data collection with id [{1}].", organization.getName(), dcdId));
            throw exception;
        }

        // map payload
        MappingResponse mappingResponse;
        IMappingContext organizationContext = mappingContextFactory.create(organization);
        final String nodeName = ((NodeImpl)inputDto.getRegistration().getPayload()).getFirstChild().getNodeName();
        if ("csv".equalsIgnoreCase(nodeName)) { // temporary, for testing only
            String payloadXml = ((NodeImpl)inputDto.getRegistration().getPayload()).getFirstChild().getTextContent();
            mappingResponse = mappingService.mapFromCsvToJson(dataCollectionDefinition.getContent(), payloadXml, organizationContext);
        }
        else {
            String payloadXml = xml.substring(xml.indexOf("<payload>") + "<payload>".length(), xml.lastIndexOf("</payload>")); // TODO: get XML as string from DOM tree
            mappingResponse = mappingService.mapFromXmlToJson(dataCollectionDefinition.getContent(), payloadXml, organizationContext);
        }

        final String uniqueID = mappingResponse.getUniqueID();
        List<RegistrationWorkflow> workflowsInDb = registrationWorkflowService.getUnique(dcdId, uniqueID, organization);
        final RegistrationWorkflow workflowInDb = workflowsInDb.isEmpty() ? null : workflowsInDb.get(0);

        // new registration
        if (StringUtils.isBlank(uniqueID) || workflowInDb==null) {
            if (!dataCollectionDefinition.getDataCollectionGroup().isValidForCreation(new Date())) {
                RegistrationApiDto errorResponse = new RegistrationApiDto();
                errorResponse.setError("Cannot create a registration for this data collection due to date range constraints.");
                RegistrationApiDto.DataCollectionDto dcdDto = new RegistrationApiDto.DataCollectionDto(dataCollectionDefinition);
                RegistrationApiDto.RegistrationDto registrationDto = new RegistrationApiDto.RegistrationDto();
                registrationDto.setDataCollection(dcdDto);
                errorResponse.setRegistration(registrationDto);
                errorResponse.setHttpStatusCode(422);
                return Response.ok(errorResponse).status(422).build();
            }
            ValidationResponse validationResponse = validate(mappingResponse, dataCollectionDefinition);
            RegistrationWorkflow resultWorkflow = performActions(null, mappingResponse, validationResponse, dataCollectionDefinition, organization, options);

            return Response.ok(mapOutputApiDto(resultWorkflow, validationResponse, Response.Status.CREATED.getStatusCode())).status(Response.Status.CREATED.getStatusCode()).build();
        }

        // existing registration
        if (ApiOptionsDto.MODE.OVERWRITE.equalsIgnoreCase(options.getMode())) {
            if (ApiOptionsDto.MASTER.API.equalsIgnoreCase(options.getMaster())) {
                ValidationResponse validationResponse = validate(mappingResponse, dataCollectionDefinition);
                RegistrationWorkflow resultWorkflow = performActions(workflowInDb.getId(), mappingResponse, validationResponse, dataCollectionDefinition, organization, options);
                noteService.deleteAllNotes(WorkflowType.REGISTRATION, resultWorkflow.getDocument().getId());
                return Response.ok(mapOutputApiDto(resultWorkflow, validationResponse, Response.Status.OK.getStatusCode())).status(Response.Status.OK.getStatusCode()).build();
            }
            else { // MASTER.DATABASE
                // TODO: get validation from elasticsearch
                ValidationResponse validationResponse = mappingService.getValidationResponse(workflowInDb, null, dataCollectionDefinition.getContent());
                return Response.ok(mapOutputApiDto(workflowInDb, validationResponse, Response.Status.OK.getStatusCode())).status(Response.Status.OK.getStatusCode()).build();
            }
        }
        else { // MODE.MERGE
            RegistrationDocument document = new RegistrationDocument();
            document.setDocumentContent(mappingResponse.getDocumentContent().toString().getBytes(StandardCharsets.UTF_8));
            RegistrationWorkflow workflowInXml = new RegistrationWorkflow();
            workflowInXml.setDocument(document);
            workflowInXml.setMetaData(mappingResponse.getMetaData());
            workflowInXml.setDataCollectionDefinitionId(workflowInDb.getDataCollectionDefinitionId());

            MappingResponse merge;
            IMappingContext workflowContext = mappingContextFactory.create(workflowInDb);
            if (ApiOptionsDto.MASTER.DATABASE.equalsIgnoreCase(options.getMaster()))
                merge = mappingService.merge(workflowInDb, workflowInXml, workflowContext, dataCollectionDefinition.getContent());
            else // MASTER.API
                merge = mappingService.merge(workflowInXml, workflowInDb, workflowContext, dataCollectionDefinition.getContent());

            ValidationResponse validationResponse = validate(merge, dataCollectionDefinition);
            RegistrationWorkflow resultWorkflow = performActions(workflowInDb.getId(), merge, validationResponse, dataCollectionDefinition, organization, options);

            return Response.ok(mapOutputApiDto(resultWorkflow, validationResponse, Response.Status.OK.getStatusCode())).status(Response.Status.OK.getStatusCode()).build();
        }
    }

    private RegistrationApiDto mapInputApiDto(String xml) {
        final String catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST).getValue();
        RegistrationApiDto.DataCollectionDto.setCatalogueHost(catalogueHost);

        RegistrationApiDto inputDto;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(RegistrationApiDto.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            jaxbUnmarshaller.setSchema(getSchema());
            StringReader stringReader = new StringReader(xml);
            inputDto = (RegistrationApiDto) jaxbUnmarshaller.unmarshal(stringReader);
        }
        catch (UnmarshalException | MarshalException me) {
            HealthDataException exception = new HealthDataException(me);
            exception.setExceptionType(ExceptionType.INVALID_INPUT, MessageFormat.format("Validation failed, cause: {0} {1}", me.getClass(), me.getCause()));
            throw exception;
        }
        catch (JAXBException jaxbe) {
            HealthDataException exception = new HealthDataException(jaxbe);
            exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, MessageFormat.format("Could not create registration, cause: {0} {1}", jaxbe.getClass(), jaxbe.getMessage()));
            throw exception;
        }

        if (inputDto.getRegistration() == null) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_INPUT, "Request does not contain a registration.");
            throw exception;
        }
        if (inputDto.getOptions() == null)
            inputDto.setOptions(new ApiOptionsDto());
        if (!ApiOptionsDto.MODE.OVERWRITE.equalsIgnoreCase(inputDto.getOptions().getMode()) && !ApiOptionsDto.MODE.MERGE.equalsIgnoreCase(inputDto.getOptions().getMode())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_INPUT, "Incorrect value for mode parameter, allowed values are overwrite and merge.");
            throw exception;
        }
        if (!ApiOptionsDto.MASTER.DATABASE.equalsIgnoreCase(inputDto.getOptions().getMaster()) && !ApiOptionsDto.MASTER.API.equalsIgnoreCase(inputDto.getOptions().getMaster())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_INPUT, "Incorrect value for master parameter, allowed values database and api.");
            throw exception;
        }

        RegistrationApiDto.DataCollectionDto dataCollectionDto = inputDto.getRegistration().getDataCollection();
        Long dcdId = inputDto.getRegistration().getDataCollection().getId();
        if (dcdId == null) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_INPUT, "Provided data collection URI is not correct, check the host or ID.");
            throw exception;
        }
        String dcdNameFromId = dataCollectionDefinitionService.getDataCollectionName(dcdId);
        if (StringUtils.isBlank(dcdNameFromId)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "data collection", dataCollectionDto.getUri());
            throw exception;
        }
        else {
            String dcdName = dataCollectionDto.getName();
            if (dcdName != null && !dcdName.equalsIgnoreCase(dcdNameFromId)) {
                HealthDataException exception = new HealthDataException();
                exception.setExceptionType(ExceptionType.GENERAL_CONFLICT, "Provided data collection URI and name do not match (expected name for given ID is " + dcdNameFromId + ").");
                throw exception;
            }
        }

        return inputDto;
    }

    private ValidationResponse validate(MappingResponse mappingResponse, DataCollectionDefinitionDtoV7 dataCollectionDefinition) {
        RegistrationWorkflow workflow = new RegistrationWorkflow();
        RegistrationDocument document = new RegistrationDocument();
        document.setDocumentContent(mappingResponse.getDocumentContent().toString().getBytes(StandardCharsets.UTF_8));
        document.setCodedContent(mappingResponse.getToBeCoded());
        if (mappingResponse.getPrivateData() != null)
            document.setPrivateContent(mappingResponse.getPrivateData().toString().getBytes(StandardCharsets.UTF_8));
        workflow.setDocument(document);
        workflow.setMetaData(mappingResponse.getMetaData()); // <v1.8.0
        // TODO: retrieve validation from Elasticsearch after save action, in order to avoid double validation (or add target to mapFromXmlToJson())
        return mappingService.getValidationResponse(workflow, null, dataCollectionDefinition.getContent());
    }

    private RegistrationWorkflow performActions(Long workflowId, MappingResponse mappingResponse, ValidationResponse validationResponse, DataCollectionDefinitionDtoV7 dataCollectionDefinition, Organization organization, ApiOptionsDto options) {
        final HDServiceUser apiServiceUser = HDServiceUser.create(HDServiceUser.Username.API, organization);
        RegistrationWorkflow result;
        if (options.isDryRun() || (!options.isSaveOnValidationError() && !validationResponse.isSuccess())) {
            result = mockSaveActionResult(dataCollectionDefinition);
        } else {
            SaveRegistrationAction saveAction = createSaveAction(dataCollectionDefinition.getId(), mappingResponse);
            saveAction.setWorkflowId(workflowId);
            result = (RegistrationWorkflow) workflowActionExecutor.execute(saveAction, apiServiceUser);
        }
        if (options.isAutoSubmit() && validationResponse.isSuccess()) {
            if (options.isDryRun()) {
                result.setStatus(WorkflowStatus.OUTBOX);
            }
            else {
                SubmitRegistrationAction submitAction = (SubmitRegistrationAction) actionFactory.getActionByActionType(WorkflowActionType.SUBMIT, WorkflowType.REGISTRATION);
                submitAction.setWorkflowId(result.getId());
                result = (RegistrationWorkflow) workflowActionExecutor.execute(submitAction, apiServiceUser);
            }
        }
        return result;
    }

    private RegistrationWorkflow mockSaveActionResult(DataCollectionDefinitionDtoV7 dcdDto) {
        RegistrationWorkflow workflow = new RegistrationWorkflow();
        workflow.setId(0L);
        workflow.setStatus(WorkflowStatus.IN_PROGRESS);
        workflow.setDataCollectionDefinitionId(dcdDto.getId());
        workflow.setDataCollectionName(dcdDto.getDataCollectionName());
        workflow.setCreatedOn(new Timestamp(new Date().getTime()));
        workflow.setUpdatedOn(new Timestamp(new Date().getTime()));

        return workflow;
    }

    private SaveRegistrationAction createSaveAction(Long dcdId, MappingResponse mappingResponse) {
        byte[] documentContent = mappingResponse.getDocumentContent().toString().getBytes(StandardCharsets.UTF_8);

        SaveRegistrationAction saveAction = (SaveRegistrationAction) actionFactory.getActionByActionType(WorkflowActionType.SAVE, WorkflowType.REGISTRATION);
        saveAction.setDataCollectionDefinitionId(dcdId);
        saveAction.setDocumentContent(documentContent); // <v1.8.0
        saveAction.setMetaData(mappingResponse.getMetaData()); // <v1.8.0

        DocumentData documentData = new DocumentData();
        documentData.setContent(documentContent);
        documentData.setCoded(mappingResponse.getToBeCoded());
        if (mappingResponse.getPrivateData() != null)
            documentData.setPrivate(mappingResponse.getPrivateData().toString().getBytes(StandardCharsets.UTF_8));
        else
            documentData.setPatientID(mappingResponse.getMetaData());
        saveAction.setDocumentData(documentData);
        saveAction.setToBeCoded(mappingResponse.getToBeCoded());

        saveAction.setUniqueID(mappingResponse.getUniqueID());
        saveAction.setSource(DataSource.API);
        saveAction.setAffectedFields(MappingResponseMapper.mapDataSources(mappingResponse.getAffectedFields(), DataSource.API));
        saveAction.setProgress(mappingResponse.getProgress());
        saveAction.setAttachments(mappingResponse.getPendingAttachments());

        return saveAction;
    }

    private RegistrationApiDto mapOutputApiDto(RegistrationWorkflow workflow, ValidationResponse validationResponse, int statusCode) {
        final String catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST).getValue();
        RegistrationApiDto.DataCollectionDto.setCatalogueHost(catalogueHost); // not very nice

        RegistrationApiDto.RegistrationDto responseRegistration = new RegistrationApiDto.RegistrationDto(workflow);

        if (validationResponse!=null && !validationResponse.isSuccess()) {
            try {
                responseRegistration.setValidationErrors(mapValidationErrors(validationResponse.getResult()));
            } catch (JSONException je) {
                HealthDataException exception = new HealthDataException(je);
                exception.setExceptionType(ExceptionType.JSON_EXCEPTION);
                throw exception;
            }
        }

        RegistrationApiDto responseDto = new RegistrationApiDto();
        responseDto.setRegistration(responseRegistration);
        responseDto.setHttpStatusCode(statusCode);

        return responseDto;
    }

    @Override
    public Response update(String xml) {
        LOG.warn("Update not implemented in this version.");
        return null;
    }

    @Override
    public Response delete(Long id) {
        LOG.info("Deleting registration with id [{}].", id);

        final HDUserDetails principal = AuthenticationService.getUserDetails();
        RegistrationWorkflow workflow = registrationWorkflowService.get(id);
        if (workflow == null || workflow.getStatus() == WorkflowStatus.DELETED) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.DELETE_NON_EXISTING, "registration", id);
            throw exception;
        }
        else if (!dataCollectionService.isAccessGranted(principal.getOrganization(), workflow.getDataCollectionName())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_FORBIDDEN, MessageFormat.format("Deletion forbidden for registration with id [{0}] (no access to data collection).", id));
            throw exception;
        }
        else if (!RegistrationWorkflowService.isDeletable(workflow)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_CONFLICT, MessageFormat.format("Registration with id [{0}] and status [{1}] cannot be deleted.", id, workflow.getStatus()));
            throw exception;
        }
        final HDServiceUser apiServiceUser = HDServiceUser.create(HDServiceUser.Username.API, principal.getOrganization());
        registrationWorkflowService.delete(Collections.singletonList(workflow), apiServiceUser);
        return Response.noContent().build();
    }

    private static List<ValidationErrorDto> mapValidationErrors(JSONObject validationObject) throws JSONException {
        JSONArray validationErrors = validationObject.getJSONArray("validation_errors");
        List<ValidationErrorDto> resultList = new ArrayList<>();
        for (int i=0 ; i<validationErrors.length(); i++) {
            ValidationErrorDto validationErrorDto = new ValidationErrorDto();
            validationErrorDto.setMessage(validationErrors.getJSONObject(i).getString("message"));
            resultList.add(validationErrorDto);
        }
        return resultList;
    }

    private static Schema getSchema() {
        if (schema == null) {
            try {
                SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
                schema = schemaFactory.newSchema(new File(RegistrationApiService.class.getClassLoader().getResource(XML_SCHEMA_FILE).getFile()));
            }
            catch (Exception e) {
                LOG.warn("Could not load XML Schema, no validation will be performed. [{} - {}]", e.getClass(), e.getMessage());
                schema = null;
            }
        }
        return schema;
    }
}
