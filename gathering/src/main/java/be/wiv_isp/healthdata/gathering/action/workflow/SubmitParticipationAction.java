/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.action.workflow;

import be.wiv_isp.healthdata.gathering.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import org.springframework.stereotype.Component;

public class SubmitParticipationAction extends AbstractParticipationWorkflowAction {

    @Override
    public WorkflowActionType getAction() {
        return WorkflowActionType.SUBMIT;
    }

    @Override
    public ParticipationWorkflow doExecute(ParticipationWorkflow workflow, HDUserDetails userDetails) {
        // no heavy process should be implemented here as the submit action must be kept as light as possible to support bulk submit actions
        return workflow;
    }

    @Override
    public WorkflowStatus getEndStatus() {
        return WorkflowStatus.OUTBOX;
    }
}
