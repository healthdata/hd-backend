/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.gathering.dao.impl.WorkflowsToMigrateCommentsDao;
import be.wiv_isp.healthdata.gathering.domain.WorkflowsToMigrateComments;
import be.wiv_isp.healthdata.gathering.service.IWorkflowsToMigrateCommentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class WorkflowsToMigrateCommentsService implements IWorkflowsToMigrateCommentsService {

    @Autowired
    private WorkflowsToMigrateCommentsDao dao;

    @Transactional
    public WorkflowsToMigrateComments create(Long workflowId) {
        WorkflowsToMigrateComments workflowsToMigrateComments = new WorkflowsToMigrateComments();
        workflowsToMigrateComments.setWorkflowId(workflowId);
        return dao.create(workflowsToMigrateComments);
    }

    public WorkflowsToMigrateComments get(Long workflowId) {
        return dao.get(workflowId);
    }

    @Transactional
    public void delete(Long workflowId) {
        WorkflowsToMigrateComments workflowsToMigrateComments = dao.get(workflowId);
        if (workflowsToMigrateComments != null)
            dao.delete(workflowsToMigrateComments);
    }

}
