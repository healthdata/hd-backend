/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.action.workflow;

import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.action.workflow.ReportableAction;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.Progress;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.service.INoteService;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.beans.factory.annotation.Autowired;

public class CreateForReviewRegistrationAction extends AbstractRegistrationWorkflowAction implements ReportableAction {

	@Autowired
	protected INoteService noteService;

	@Override
	public WorkflowActionType getAction() {
		return WorkflowActionType.CREATE_FOR_REVIEW;
	}

	@Override
	public RegistrationWorkflow doExecute(RegistrationWorkflow workflow, HDUserDetails userDetails) {
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.ACTION_NOT_ALLOWED_ON_PLATFORM, this.getAction(), Platform.HD4DP);
		throw exception;
	}

	@Override
	@JsonIgnore
	public WorkflowStatus getEndStatus() {
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.ACTION_NOT_ALLOWED_ON_PLATFORM, this.getAction(), Platform.HD4DP);
		throw exception;
	}


	@Override
	public WorkflowType getWorkflowType() {
		return WorkflowType.REGISTRATION;
	}

	@Override
	public String getSource() {
		return source;
	}

	@Override
	public String getFile() {
		return file;
	}

	@Override
	public String getMessageType() {
		return messageType;
	}

	@Override
	public Progress getProgress() {
		return null;
	}
}
