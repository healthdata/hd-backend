/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.mapper.ExistingDirectoryConfigurationMapper;
import be.wiv_isp.healthdata.orchestration.dto.EncryptionModuleMessage;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractEncryptionModuleService;
import be.wiv_isp.healthdata.orchestration.util.FileSystemUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class FileSystemEncryptionModuleService extends AbstractEncryptionModuleService {

	private static final Logger LOG = LoggerFactory.getLogger(FileSystemEncryptionModuleService.class);

	private static final String BASENAME_PATTERN = "healthdata_{0}.{1}";
	private static final String CSV_EXTENSION = "csv";

	@Override
	public void send(EncryptionModuleMessage message, Organization organization) {
		final File outgoingDirectory = ExistingDirectoryConfigurationMapper.map(configurationService.get(ConfigurationKey.OUTGOING_DIRECTORY, organization));
		final String timestamp = new SimpleDateFormat(DateFormat.TIMESTAMP.getPattern()).format(new Date());
		final String csvFilename = MessageFormat.format(BASENAME_PATTERN, timestamp, CSV_EXTENSION);
		final File csvFile = new File(outgoingDirectory, csvFilename);

		LOG.debug(MessageFormat.format("Writing csv file to {0}", csvFile.getAbsolutePath()));

		FileSystemUtils.writeStringToFile(csvFile, message.getContent());
	}

	@Override
	public List<EncryptionModuleMessage> receive(Organization organization) {
		final File incomingDirectory = ExistingDirectoryConfigurationMapper.map(configurationService.get(ConfigurationKey.INCOMING_DIRECTORY, organization));
		final File archivedDirectory = ExistingDirectoryConfigurationMapper.map(configurationService.get(ConfigurationKey.ARCHIVED_DIRECTORY, organization));
		final File errorDirectory = ExistingDirectoryConfigurationMapper.map(configurationService.get(ConfigurationKey.ERROR_DIRECTORY, organization));

		final File[] files = incomingDirectory.listFiles((FilenameFilter) new SuffixFileFilter(CSV_EXTENSION));

		if (files == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, MessageFormat.format("Error while reading files from directory [{0}]", incomingDirectory));
			throw exception;
		}

		if (files.length == 0) {
			LOG.debug(MessageFormat.format("No messages found in [{0}]", incomingDirectory));
			return new ArrayList<>();
		}

		LOG.debug(MessageFormat.format("{0} message(s) received from Encryption Module", files.length));

		final List<EncryptionModuleMessage> messages = new ArrayList<>();
		for (File file : files) {

			String content;
			try {
				LOG.debug(MessageFormat.format("File[{0}]: reading file.", file.getName()));
				content = FileUtils.readFileToString(file);
			} catch (IOException e) {
				LOG.error(MessageFormat.format("File[{0}]: error while reading file, skipping file.", file.getName()), e);
				FileSystemUtils.moveFilesToDirectory(errorDirectory, true, file);
				continue;
			}

			int lineCount = 0;
			for (String csvRecord : content.split("\n")) {
				if (csvRecord.trim().isEmpty())
					continue;

				final Map<String, String> metadata = new HashMap<>();
				metadata.put(Message.Metadata.MESSAGE_ID, file.getName() + "_" + lineCount);

				final EncryptionModuleMessage message = new EncryptionModuleMessage();
				message.setContent(csvRecord.trim());
				message.setMetadata(metadata);

				messages.add(message);
				++lineCount;
			}

			LOG.debug(MessageFormat.format("File[{0}]: moving file to archived directory", file.getName()));
			FileSystemUtils.moveFilesToDirectory(archivedDirectory, true, file);

			if(messages.size() >= 100) {
				return messages; // return maximum 100 + MESSAGING_QUEUE_SIZE - 1 messages at a time
			}
		}

		return messages;
	}
}
