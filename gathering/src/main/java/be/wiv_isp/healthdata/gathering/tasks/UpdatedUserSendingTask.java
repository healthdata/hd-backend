/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.tasks;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.dto.HealthDataIdentificationDto;
import be.wiv_isp.healthdata.gathering.action.standalone.UserUpdateAction;
import be.wiv_isp.healthdata.gathering.domain.UpdatedUser;
import be.wiv_isp.healthdata.gathering.factory.IMessageFactory;
import be.wiv_isp.healthdata.gathering.service.IUpdatedUserService;
import be.wiv_isp.healthdata.orchestration.action.dto.UserUpdateHd4resActionDto;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.service.IUserService;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import be.wiv_isp.healthdata.orchestration.tasks.HealthDataTask;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;

@Component
public class UpdatedUserSendingTask extends HealthDataTask {

	private static final Logger LOG = LoggerFactory.getLogger(UpdatedUserSendingTask.class);

	@Autowired
	private IUpdatedUserService updatedUserService;
	@Autowired
	private IUserService userService;
	@Autowired
	private IOrganizationService organizationService;
	@Autowired
	private IMessageFactory messageFactory;
	@Autowired
	private IMessageService messageService;

	@Autowired
	@Qualifier("transactionManager")
	protected PlatformTransactionManager txManager;

	@Override
	public String getName() {
		return "UPDATED_USER";
	}

	@Override
	public void execute() {
		TransactionTemplate tmpl = new TransactionTemplate(txManager);
		tmpl.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
				doExecute();
			}
		});
	}

	private void doExecute() {
		final List<UpdatedUser> all = updatedUserService.getAll();

		if(CollectionUtils.isEmpty(all)) {
			LOG.debug("No user updates found.");
			return;
		}

		LOG.info("Found {} user updates", all.size());

		final UserUpdateHd4resActionDto actionDto = new UserUpdateHd4resActionDto();

		for (final UpdatedUser updatedUser : all) {
			final User user = userService.get(updatedUser.getUserID());

			final UserUpdateHd4resActionDto.UserDto userDto = new UserUpdateHd4resActionDto.UserDto();
			userDto.setHd4dpId(updatedUser.getUserID());
			userDto.setHealthDataIdentification(createHealthDataIdentificationDto(updatedUser.getOrganization()));

			if(user == null) {
				LOG.debug("User [{}] has been deleted.");
				userDto.setDeleted(true);
			} else{
				LOG.debug("User [{}] has been created or updated.");
				userDto.setUsername(user.getUsername());
				userDto.setLastName(user.getLastName());
				userDto.setFirstName(user.getFirstName());
				userDto.setEmail(user.getEmail());
				userDto.setDataCollectionNames(user.getDataCollectionNames());
				userDto.setAuthorities(user.getAuthorities());
				userDto.setEnabled(user.isEnabled());
				userDto.setLdapUser(user.isLdapUser());
			}

			actionDto.getUsers().add(userDto);
			updatedUserService.delete(updatedUser);
		}

		LOG.debug("Creating and sending UserUpdate action");

		final UserUpdateAction action = new UserUpdateAction();
		action.setUserUpdateDto(actionDto);

		final Message outboxMessage = messageFactory.createOutgoingMessage(action, organizationService.getMain());
		messageService.create(outboxMessage);
	}

	private HealthDataIdentificationDto createHealthDataIdentificationDto(Organization organization) {
		final HealthDataIdentificationDto healthDataIdentificationDto = new HealthDataIdentificationDto();
		healthDataIdentificationDto.setType(organization.getHealthDataIDType());
		healthDataIdentificationDto.setValue(organization.getHealthDataIDValue());
		healthDataIdentificationDto.setName(organization.getName());
		healthDataIdentificationDto.setHd4prc(organization.isHd4prc());
		return healthDataIdentificationDto;
	}
}
