/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.action.workflow;

import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.service.IRegistrationWorkflowService;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.domain.mapper.NoteMapper;
import be.wiv_isp.healthdata.orchestration.service.IAttachmentService;
import be.wiv_isp.healthdata.orchestration.service.INoteService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

public class ReOpenRegistrationAction extends AbstractRegistrationWorkflowAction {

    @Autowired
    private INoteService noteService;
    @Autowired
    private IAttachmentService attachmentService;
    @Autowired
    private IRegistrationWorkflowService workflowService;

    @Override
    public WorkflowActionType getAction() {
        return WorkflowActionType.REOPEN;
    }

    @Override
    public RegistrationWorkflow doExecute(RegistrationWorkflow workflow, HDUserDetails userDetails) {
        List<Note> oldNotes = noteService.getByDocumentId(WorkflowType.REGISTRATION, workflow.getDocument().getId());
        final List<Attachment> attachments = workflow.getDocument().getAttachments();

        workflowService.createNewDocumentVersion(workflow);

        final RegistrationWorkflowHistory history = getLastHistory(workflow, Arrays.asList(WorkflowStatus.IN_PROGRESS, WorkflowStatus.ACTION_NEEDED));
        setEndStatus(history.getNewStatus());
        workflow.setFlags(new WorkflowFlags(history.getFlags()));

        noteService.migrateNotes(WorkflowType.REGISTRATION, workflow.getDocument(), NoteMapper.mapToNoteDtos(oldNotes));
        attachmentService.linkAttachments(workflow.getDocument(), attachments);

        return workflow;
    }

    private RegistrationWorkflowHistory getLastHistory(RegistrationWorkflow workflow, List<WorkflowStatus> statuses) {
        RegistrationWorkflowHistory result = null;
        for (RegistrationWorkflowHistory h : workflow.getHistory()) {
            if(statuses.contains(h.getNewStatus())) {
                if(result == null || h.getExecutedOn().compareTo(result.getExecutedOn()) > 0) {
                    result = h;
                }
            }
        }
        return result;
    }
}
