/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.domain;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.AbstractUserRequest;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "USER_REQUESTS")
public class UserRequest extends AbstractUserRequest {

    @Column(name = "PASSWORD")
    private String password;

    @Fetch(FetchMode.JOIN)
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "ORGANIZATION_ID")
    private Organization organization;

    /**
     * can't be in subclass because of bug in hibernate
     * https://hibernate.atlassian.net/browse/HHH-6562
     */
    @Fetch(FetchMode.JOIN)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "USER_REQUEST_DATA_COLLECTIONS", joinColumns = @JoinColumn(name = "USER_REQUEST_ID"))
    @Column(name = "DATA_COLLECTION_NAME")
    protected Set<String> dataCollectionNames;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Set<String> getDataCollectionNames() {
        if (dataCollectionNames == null) {
            dataCollectionNames = new HashSet<>();
        }
        return dataCollectionNames;
    }

    public void setDataCollectionNames(Set<String> dataCollectionNames) {
        this.dataCollectionNames = dataCollectionNames;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof UserRequest) {
            UserRequest other = (UserRequest) o;

            return Objects.equals(id, other.id)
                    && Objects.equals(firstName, other.firstName)
                    && Objects.equals(lastName, other.lastName)
                    && Objects.equals(email, other.email)
                    && Objects.equals(dataCollectionNames, other.dataCollectionNames)
                    && Objects.equals(approved, other.approved)
                    && Objects.equals(username, other.username)
                    && Objects.equals(password, other.password)
                    && Objects.equals(emailRequester, other.emailRequester)
                    && Objects.equals(organization.getId(), other.organization.getId())
                    && Objects.equals(createdOn, other.createdOn)
                    && Objects.equals(updatedOn, other.updatedOn);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                this.id,
                this.firstName,
                this.lastName,
                this.email,
                this.dataCollectionNames,
                this.approved,
                this.username,
                this.password,
                this.emailRequester,
                this.organization.getId(),
                this.createdOn,
                this.updatedOn);
    }

    @Override
    public String toString() {
        return "UserRequest {" +
                "id = " + Objects.toString(this.id) + ", " +
                "firstName = " + Objects.toString(this.firstName) + ", " +
                "lastName = " + Objects.toString(this.lastName) + ", " +
                "email = " + Objects.toString(this.email) + ", " +
                "dataCollectionNames = " + Objects.toString(this.dataCollectionNames) + ", " +
                "approved = " + Objects.toString(this.approved) + ", " +
                "username = " + Objects.toString(this.username) + ", " +
                "emailRequester = " + Objects.toString(this.emailRequester) + ", " +
                "organization = " + Objects.toString(this.organization) + ", " +
                "createdOn = " + Objects.toString(this.createdOn) + ", " +
                "updatedOn = " + Objects.toString(this.updatedOn) + "}";
    }
}
