/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.AuthenticationType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.UserManagementType;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.gathering.action.standalone.UserRequestAction;
import be.wiv_isp.healthdata.gathering.api.mapper.IUserRequestMapper;
import be.wiv_isp.healthdata.gathering.domain.UserRequest;
import be.wiv_isp.healthdata.gathering.domain.search.UserRequestSearch;
import be.wiv_isp.healthdata.gathering.domain.validation.IUserRequestValidator;
import be.wiv_isp.healthdata.gathering.factory.IMessageFactory;
import be.wiv_isp.healthdata.gathering.service.IUserRequestService;
import be.wiv_isp.healthdata.orchestration.action.dto.UserRequestHd4resActionDto;
import be.wiv_isp.healthdata.orchestration.dao.IUserRequestDao;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.security.ldap.service.ILdapService;
import be.wiv_isp.healthdata.orchestration.service.*;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.PersistenceException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


@Service
public class UserRequestService extends AbstractService<UserRequest, Long, UserRequestSearch, IUserRequestDao<UserRequest, UserRequestSearch>> implements IUserRequestService {

    private static final Logger LOG = LoggerFactory.getLogger(UserRequestService.class);

    @Autowired
    protected IUserRequestDao<UserRequest, UserRequestSearch> dao;
    @Autowired
    private IUserRequestMapper userRequestMapper;
    @Autowired
    private IUserRequestValidator userRequestValidator;
    @Autowired
    private IUserService userService;
    @Autowired
    private ILdapService ldapService;
    @Autowired
    private IMessageFactory messageFactory;
    @Autowired
    private IMessageService messageService;
    @Autowired
    private IUserManagementService userManagementService;
    @Autowired
    private IMailService mailService;
    @Autowired
    private IUserRequestMailService userRequestMailService;
    @Autowired
    private IConfigurationService configurationService;

    public UserRequestService() {
        super(UserRequest.class);
    }

    @Override
    public IUserRequestDao<UserRequest, UserRequestSearch> getDao() {
        return dao;
    }

    @Override
    public UserRequest create(JSONObject json) {
        LOG.info("POST Request: Create new userRequest");
        final UserRequest userRequest = unmarshal(json);
        try {
            boolean defaultPassword = false;
            final UserManagementType userManagementType = userManagementService.getUserManagementType(userRequest.getOrganization());
            if (AuthenticationType.DATABASE.equals(userManagementType.getAuthenticationType())) {
                userRequest.setPassword(RandomStringUtils.randomAlphanumeric(32));
                defaultPassword = true;
            }
            userRequestValidator.validate(userRequest);
            final UserRequest createdUserRequest = create(userRequest);
            final UserRequestHd4resActionDto dto = buildDto(userRequest, UserRequestHd4resActionDto.Action.NEW, defaultPassword);
            sendMails(dto);
            sendToHd4res(dto, createdUserRequest.getOrganization());
            return createdUserRequest;
        } catch (PersistenceException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.CREATE_DUPLICATE_KEY, userRequest.getUsername());
            throw exception;
        } catch (HealthDataException e){
            throw e;
        } catch (Exception e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.CREATE_GENERAL);
            throw exception;
        }
    }

    @Override
    public UserRequest update(Long id, JSONObject json) {
        final UserRequest userRequestFromDb = get(id);
        if(userRequestFromDb == null) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "USER_REQUEST", id);
            throw exception;
        }

        verifyCurrentUserCanModify(userRequestFromDb);

        boolean defaultPassword = false;
        final UserRequest userRequest = unmarshal(json);
        userRequest.setCreatedOn(new Timestamp(new Date().getTime()));
        if (userRequest.getOrganization() == null) {
            userRequest.setOrganization(userRequestFromDb.getOrganization());
        }
        if (StringUtils.isBlank(userRequest.getPassword())) {
            final UserManagementType userManagementType = userManagementService.getUserManagementType(userRequest.getOrganization());
            if (AuthenticationType.DATABASE.equals(userManagementType.getAuthenticationType())) {
                userRequest.setPassword(RandomStringUtils.randomAlphanumeric(32));
                defaultPassword = true;
            }
        }
        userRequestValidator.validate(userRequest);
        if (userRequestFromDb.getApproved() != null) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.UPDATE_FINAL_STATE, "USER_REQUEST", userRequestFromDb.getId());
            throw exception;
        }
        userRequestFromDb.setFirstName(userRequest.getFirstName());
        userRequestFromDb.setLastName(userRequest.getLastName());
        userRequestFromDb.setEmail(userRequest.getEmail());
        userRequestFromDb.setEmailRequester(userRequest.getEmailRequester());
        userRequestFromDb.setDataCollectionNames(userRequest.getDataCollectionNames());
        userRequestFromDb.setUsername(userRequest.getUsername());
        userRequestFromDb.setPassword(userRequest.getPassword());
        UserRequest updatedUserRequest = update(userRequestFromDb);
        final UserRequestHd4resActionDto dto = buildDto(userRequest, UserRequestHd4resActionDto.Action.UPDATE, defaultPassword);
        sendMails(dto);
        sendToHd4res(dto, updatedUserRequest.getOrganization());
        return updatedUserRequest;
    }

    @Override
    public void accept(final UserRequest userRequest) {
        verifyCurrentUserCanModify(userRequest);

        final User user = getUserFromRequest(userRequest);

        final UserManagementType userManagementType = userManagementService.getUserManagementType(userRequest.getOrganization());
        boolean ldapUser = AuthenticationType.LDAP.equals(userManagementType.getAuthenticationType());

        if(ldapUser) {
            if(!ldapService.isValidUserName(userRequest.getUsername(), userRequest.getOrganization())) {
                StringBuilder sb = new StringBuilder();
                sb.append("The user [");
                sb.append(StringUtils.isNotBlank(userRequest.getUsername())?userRequest.getUsername():"");
                sb.append("] isn't found in the location configured for HD4DP");
                HealthDataException exception = new HealthDataException();
                exception.setExceptionType(ExceptionType.LDAP_USER_REQUEST_EXCEPTION, sb.toString());
                throw exception;
            }
            user.setPassword(null);
        }

        user.setLdapUser(ldapUser);
        userService.create(user, false);

        userRequest.setApproved(true);
        UserRequest updatedUserRequest = dao.update(userRequest);
        final UserRequestHd4resActionDto dto = buildDto(userRequest, UserRequestHd4resActionDto.Action.ACCEPT, !ldapUser);
        sendMails(dto);
        sendToHd4res(dto, updatedUserRequest.getOrganization());
    }

    @Override
    public void reject(final UserRequest userRequest) {
        verifyCurrentUserCanModify(userRequest);

        userRequest.setApproved(false);
        UserRequest updatedUserRequest = dao.update(userRequest);
        final UserRequestHd4resActionDto dto = buildDto(userRequest, UserRequestHd4resActionDto.Action.REJECT, false);
        sendMails(dto);
        sendToHd4res(dto, updatedUserRequest.getOrganization());
    }

    private void sendMails(final UserRequestHd4resActionDto dto) {
        final boolean mailServerConfigured = mailService.status();
        if(mailServerConfigured) {
            LOG.info("Mail server is correctly configured. Sending notifications using HD4DP mail server.");
            userRequestMailService.sendMails(dto);
            dto.setEmailSent(true);
        } else {
            LOG.warn("Mail server not configured. Notifications will be sent using HD4RES mail server");
            dto.setEmailSent(false);
        }
    }

    private User getUserFromRequest(UserRequest userRequest) {
        User user = new User();
        user.setUsername(userRequest.getUsername());
        user.setPassword(userRequest.getPassword());
        user.setFirstName(userRequest.getFirstName());
        user.setLastName(userRequest.getLastName());
        user.setEmail(userRequest.getEmail());
        user.setOrganization(userRequest.getOrganization());
        Set<Authority> authorities = new HashSet<>();
        Authority authority = new Authority();
        authority.setAuthority("ROLE_USER");
        authorities.add(authority);
        user.setAuthorities(authorities);
        user.setDataCollectionNames(new HashSet<>(userRequest.getDataCollectionNames()));
        user.setEnabled(true);
        return user;
    }

    private void sendToHd4res(final UserRequestHd4resActionDto dto, final Organization organization) {
        final UserRequestAction userRequestAction = new UserRequestAction();
        userRequestAction.setUserRequestDto(dto);
        userRequestAction.setHealthDataIdentification(new HealthDataIdentification(organization));

        final Message outboxMessage = messageFactory.createOutgoingMessage(userRequestAction, organization);
        messageService.create(outboxMessage);
    }

    private UserRequestHd4resActionDto buildDto(UserRequest userRequest, UserRequestHd4resActionDto.Action action, boolean defaultPassword) {
        final UserRequestHd4resActionDto dto = new UserRequestHd4resActionDto();
        dto.setUsername(userRequest.getUsername());
        if(defaultPassword) {
            dto.setDefaultPassword(userRequest.getPassword());
        }
        dto.setFirstName(userRequest.getFirstName());
        dto.setLastName(userRequest.getLastName());
        dto.setEmail(userRequest.getEmail());
        dto.setEmailRequester(userRequest.getEmailRequester());
        dto.setHd4dpId(userRequest.getId());
        dto.setHd4dpUrl(configurationService.get(ConfigurationKey.GUI_HOST).getValue());
        dto.setRequestedOn(userRequest.getCreatedOn());
        dto.setApproved(userRequest.getApproved());
        dto.setDataCollectionNames(userRequest.getDataCollectionNames());
        dto.setEmailAdmins(mailService.getEmailAdmins(userRequest.getOrganization()));
        dto.setAction(action);
        return dto;
    }

    private UserRequest unmarshal(JSONObject json) {
        try {
            return userRequestMapper.convert(json);
        } catch (JSONException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.JSON_PARSE_EXCEPTION);
            throw exception;
        }
    }


    private void verifyCurrentUserCanModify(UserRequest userRequest) {
        final Organization adminOrganization = AuthenticationService.getAuthenticatedOrganization();
        if(!adminOrganization.equals(userRequest.getOrganization())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_UNAUTHORIZED);
            throw exception;
        }
    }
}
