/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.domain;

import be.wiv_isp.healthdata.orchestration.domain.AbstractParticipationWorkflow;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "PARTICIPATION_WORKFLOWS")
public class ParticipationWorkflow extends AbstractParticipationWorkflow<ParticipationDocument> {


	@Column(name = "SUBMITTED_ON")
	private Timestamp submittedOn;

	public Timestamp getSubmittedOn() {
		return submittedOn;
	}

	public void setSubmittedOn(Timestamp submittedOn) {
		this.submittedOn = submittedOn;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof ParticipationWorkflow) {
			ParticipationWorkflow other = (ParticipationWorkflow) o;

			return Objects.equals(id, other.id)
					&& Objects.equals(status, other.status)
					&& Objects.equals(documentHistory, other.documentHistory)
					&& Objects.equals(dataCollectionName, other.dataCollectionName)
					&& Objects.equals(dataCollectionDefinitionId, other.dataCollectionDefinitionId)
					&& Objects.equals(createdOn, other.createdOn)
					&& Objects.equals(updatedOn, other.updatedOn)
					&& Objects.equals(history, other.history)
					&& Objects.equals(submittedOn, other.submittedOn)
					&& Objects.equals(organization, other.organization);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				this.id,
				this.status,
				this.documentHistory,
				this.dataCollectionName,
				this.dataCollectionDefinitionId,
				this.createdOn,
				this.updatedOn,
				this.history,
				this.submittedOn,
				this.organization);
	}

	@Override
	public String toString() {
		return "ParticipationWorkflow {" +
				"id = " + Objects.toString(this.id) + ", " +
				"status = " + Objects.toString(this.status) + ", " +
				"document = " + Objects.toString(this.documentHistory) + ", " +
				"dataCollectionName = " + Objects.toString(this.dataCollectionName) + ", " +
				"dataCollectionDefinitionId = " + Objects.toString(this.dataCollectionDefinitionId) + ", " +
				"createdOn = " + Objects.toString(this.createdOn) + ", " +
				"updatedOn = " + Objects.toString(this.updatedOn) + ", " +
				"history = " + Objects.toString(this.history) + ", " +
				"submittedOn = " + Objects.toString(this.submittedOn) + ", " +
				"organization = " + Objects.toString(this.organization) + "}";
	}
}