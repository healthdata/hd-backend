/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.action.workflow;

import be.wiv_isp.healthdata.common.dto.DataCollectionGroupDto;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.factory.IAbstractMessageFactory;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionGroupForwardService;
import be.wiv_isp.healthdata.orchestration.service.IIdentificationService;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.Date;

public class StopParticipationAction extends AbstractParticipationWorkflowAction {

	private static final Logger LOG = LoggerFactory.getLogger(StopParticipationAction.class);

	@Autowired
	private IDataCollectionGroupForwardService catalogueService;
	@Autowired
	private IIdentificationService identificationService;
	@Autowired
	private IMessageService messageService;
	@Autowired
	private IAbstractMessageFactory<RegistrationWorkflow, ParticipationWorkflow> messageFactory;

	@Override
	public WorkflowActionType getAction() {
		return WorkflowActionType.STOP;
	}

	@Override
	public ParticipationWorkflow doExecute(ParticipationWorkflow workflow, HDUserDetails userDetails) {
		workflow.setCompleted(true);
		Timestamp completedOnTimestamp = new Timestamp(new Date().getTime());
		workflow.setCompletedOn(completedOnTimestamp);
		setEndStatus(workflow.getStatus());
		sendWorkflow(workflow);
		return workflow;
	}
	
	private void sendWorkflow(ParticipationWorkflow workflow) {
		try {
			Long dataCollectionDefinitionId = workflow.getDataCollectionDefinitionId();
			DataCollectionGroupDto group = catalogueService.get(dataCollectionDefinitionId);
			if (!group.isValidForSubmission(new Date())) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.INVALID_DATE_FOR_DATA_COLLECTION_DEFINITION, dataCollectionDefinitionId, "submit");
				throw exception;
			}

			final StopParticipationAction action = new StopParticipationAction();
			action.setDataCollectionDefinitionId(workflow.getDataCollectionDefinitionId());
			action.setIdentificationWorkflow(identificationService.getHealthDataIdentification(workflow.getOrganization()));
			action.setCompletedOn(workflow.getCompletedOn());

			final Message message = messageFactory.createOutgoingMessage(action, workflow);
			messageService.create(message);
		} catch (Exception e) {
			LOG.error(MessageFormat.format("error while stopping participation: {0}", e.getMessage()), e);
		}
	}
}
