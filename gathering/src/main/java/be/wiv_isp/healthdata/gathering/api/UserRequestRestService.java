/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.api;

import be.wiv_isp.healthdata.orchestration.annotation.Auditable;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ApiType;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.gathering.domain.UserRequest;
import be.wiv_isp.healthdata.gathering.domain.search.UserRequestSearch;
import be.wiv_isp.healthdata.gathering.dto.UserRequestDto;
import be.wiv_isp.healthdata.gathering.service.IUserRequestService;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;

@Component
@Path("/userrequests")
public class UserRequestRestService {

	@Autowired
	private IUserRequestService userRequestService;

	@Transactional
	@POST
	@Auditable(apiType = ApiType.USER_REQUEST)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(String json) throws JSONException {
		final UserRequest created = userRequestService.create(new JSONObject(json));
		return Response.ok(new UserRequestDto(created)).build();
	}

	@Transactional
	@PUT
	@Path("/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.USER_REQUEST)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@Context UriInfo info, String json) throws JSONException {
		final UserRequest updated = userRequestService.update(getId(info), new JSONObject(json));
		return Response.ok(new UserRequestDto(updated)).build();
	}

	@Transactional
	@POST
	@Path("/{id}/accept")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.USER_REQUEST)
	@Produces(MediaType.APPLICATION_JSON)
	public Response accept(@Context UriInfo info) throws Exception {
		final UserRequest request = getUserRequest(getId(info));
		userRequestService.accept(request);
		return Response.noContent().build();
	}

	@Transactional
	@POST
	@Path("/{id}/reject")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.USER_REQUEST)
	@Produces(MediaType.APPLICATION_JSON)
	public Response reject(@Context UriInfo info) throws Exception {
		final UserRequest request = getUserRequest(getId(info));
		userRequestService.reject(request);
		return Response.noContent().build();
	}

	@GET
	@Path("/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context UriInfo info) throws JSONException {
		final UserRequest userRequest = getUserRequest(getId(info));
		return Response.ok(new UserRequestDto(userRequest)).build();
	}

	@GET
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(@Context UriInfo info) {
		final HDUserDetails principal = AuthenticationService.getUserDetails();
		final Organization organization = principal.getOrganization();

		final UserRequestSearch search = new UserRequestSearch(info, organization);
		final List<UserRequest> userRequests = userRequestService.getAll(search);

		return Response.ok(convert(userRequests)).build();
	}

	private List<UserRequestDto> convert(List<UserRequest> userRequests) {
		final List<UserRequestDto> converted = new ArrayList<>();
		for (UserRequest userRequest: userRequests) {
			converted.add(new UserRequestDto(userRequest));
		}
		return converted;
	}

	private UserRequest getUserRequest(Long id) {
		final UserRequest userRequest = userRequestService.get(id);
		if(userRequest == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "USER_REQUEST", id);
			throw exception;
		}
		return userRequest;
	}

	private Long getId(UriInfo info) {
		return RestUtils.getPathLong(info, "id");
	}
}
