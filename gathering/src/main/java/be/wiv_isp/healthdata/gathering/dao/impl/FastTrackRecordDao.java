/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.gathering.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.gathering.dao.IFastTrackRecordDao;
import be.wiv_isp.healthdata.gathering.domain.FastTrackRecord;
import be.wiv_isp.healthdata.gathering.domain.enumeration.FastTrackStatus;
import be.wiv_isp.healthdata.gathering.domain.search.FastTrackRecordSearch;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class FastTrackRecordDao extends CrudDaoV2<FastTrackRecord, Long, FastTrackRecordSearch> implements IFastTrackRecordDao{

    public FastTrackRecordDao() {
        super(FastTrackRecord.class);
    }

    @Override
    protected List<Predicate> getPredicates(FastTrackRecordSearch search, CriteriaBuilder cb, Root<FastTrackRecord> rootEntry) {
        final List<Predicate> predicates = new ArrayList<>();

        if(search.getUploadId() !=  null) {
            predicates.add(cb.equal(rootEntry.get("uploadId"), search.getUploadId()));
        }
        if(StringUtils.isNoneBlank(search.getHash())) {
            predicates.add(cb.equal(rootEntry.get("hash"), search.getHash()));
        }
        if(search.getExcludeErrors() != null && search.getExcludeErrors()) {
            predicates.add(cb.notEqual(rootEntry.get("status"), FastTrackStatus.ERROR));
        }
        return predicates;
    }

    @Override
    public List<FastTrackRecord> getForHash(Long dataCollectionDefinitionId, String hash) {
        TypedQuery<FastTrackRecord> query = em.createNamedQuery("getForHash", FastTrackRecord.class);
        query.setParameter("dataCollectionDefinitionId", dataCollectionDefinitionId);
        query.setParameter("hash", hash);
        return query.getResultList();
    }

    @Override
    @Transactional
    public void create(FastTrackRecord record, Long uploadId) {
        record.setUploadId(uploadId);
        create(record);
    }
}
