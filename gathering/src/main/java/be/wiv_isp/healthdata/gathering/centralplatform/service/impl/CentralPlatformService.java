/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.gathering.centralplatform.service.impl;

import be.wiv_isp.healthdata.common.caching.CacheManagementService;
import be.wiv_isp.healthdata.common.domain.RegistryDependentIdGenerationDefinition;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.gathering.api.uri.*;
import be.wiv_isp.healthdata.gathering.centralplatform.dto.*;
import be.wiv_isp.healthdata.gathering.centralplatform.service.ICentralPlatformService;
import be.wiv_isp.healthdata.gathering.domain.Upgrade;
import be.wiv_isp.healthdata.gathering.dto.InstallationDto;
import be.wiv_isp.healthdata.gathering.dto.UpgradeDto;
import be.wiv_isp.healthdata.gathering.dto.UserRequestDto;
import be.wiv_isp.healthdata.gathering.service.IInstallationDetailsService;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.dto.OrganizationDto;
import be.wiv_isp.healthdata.orchestration.service.ICatalogueClient;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IWebServiceClientService;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.List;


@Service
public class CentralPlatformService implements ICentralPlatformService {

    private static final Logger LOG = LoggerFactory.getLogger(CentralPlatformService.class);

    private String accessToken;

    @Autowired
    private IWebServiceClientService webServiceClientService;

    @Autowired
    private IConfigurationService configurationService;

    @Autowired
    private IInstallationDetailsService installationDetailsService;

    @Autowired
    private ICatalogueClient catalogueClient;

    @Override
    public InstallationDto registerInstallation() {
        final InstallationRegistrationUri uri = new InstallationRegistrationUri();
        uri.setHost(configurationService.get(ConfigurationKey.CATALOGUE_HOST).getValue());

        final WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.setUrl(uri.toString());
        wsb.setPost(true);
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.setLogResponseBody(false);
        wsb.setReturnType(new GenericType<ClientResponse>() {});
        wsb.addHeader(HttpHeaders.REGISTRATION_TOKEN, configurationService.get(ConfigurationKey.CATALOGUE_REGISTRATION_TOKEN).getValue());
        final ClientResponse response = (ClientResponse) webServiceClientService.callWebService(wsb);

        if(!isValid(response)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_CENTRAL_PLATFORM_STATUS_CODE, response.getStatus());
            throw exception;
        }

        final JSONObject jsonObject = response.getEntity(JSONObject.class);
        final InstallationDto installation = new InstallationDto();
        installation.setId(JsonUtils.getLong(jsonObject, "id"));
        installation.setUsername(JsonUtils.getString(jsonObject, "username"));
        installation.setPassword(JsonUtils.getString(jsonObject, "password"));

        return installation;
    }

    @Override
    public void registerUpgrade(final Upgrade upgrade) {
        LOG.info("Registering HD4DP upgrade to the catalogue (version: {})", upgrade.getVersion());

        final UpgradeRegistrationUri uri = new UpgradeRegistrationUri();
        uri.setHost(configurationService.get(ConfigurationKey.CATALOGUE_HOST).getValue());
        uri.setInstallationId(installationDetailsService.get().getInstallationId());

        final WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.setUrl(uri.toString());
        wsb.setPost(true);
        wsb.setType(MediaType.APPLICATION_JSON_TYPE);
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.setJson(new UpgradeDto(upgrade));
        final ClientResponse response = catalogueClient.addAuthorizationHeaderAndCallWebService(wsb);

        if(!isValid(response)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_CENTRAL_PLATFORM_STATUS_CODE, response.getStatus());
            throw exception;
        }

        LOG.info("Upgrade successfully registered to the catalogue (version: {})", upgrade.getVersion());
    }

    @Override
    public void updateOrganizations(final List<OrganizationDto> organizations) {
        LOG.info("Updating organizations to the catalogue");

        final OrganizationUpdateUri uri = new OrganizationUpdateUri();
        uri.setHost(configurationService.get(ConfigurationKey.CATALOGUE_HOST).getValue());
        uri.setInstallationId(installationDetailsService.get().getInstallationId());

        final WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.setUrl(uri.toString());
        wsb.setPost(true);
        wsb.setType(MediaType.APPLICATION_JSON_TYPE);
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.setJson(organizations);
        final ClientResponse response = catalogueClient.addAuthorizationHeaderAndCallWebService(wsb);

        if(!isValid(response)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_CENTRAL_PLATFORM_STATUS_CODE, response.getStatus());
            throw exception;
        }

        LOG.info("Organizations successfully updated to the catalogue");
    }

    @Override
    public void updateRegistrationStatistics(List<RegistrationStatisticDto> counts) {
        LOG.info("Updating registration statistics to the catalogue");

        final RegistrationStatisticsUri uri = new RegistrationStatisticsUri();
        uri.setHost(configurationService.get(ConfigurationKey.CATALOGUE_HOST).getValue());
        uri.setInstallationId(installationDetailsService.get().getInstallationId());

        final WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.setUrl(uri.toString());
        wsb.setPost(true);
        wsb.setType(MediaType.APPLICATION_JSON_TYPE);
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.setJson(counts);
        final ClientResponse response = catalogueClient.addAuthorizationHeaderAndCallWebService(wsb);

        if(!isValid(response)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_CENTRAL_PLATFORM_STATUS_CODE, response.getStatus());
            throw exception;
        }

        LOG.info("Registration statistics successfully updated to the catalogue");
    }

    @Override
    public void updateStableDataStatistics(List<StableDataStatisticDto> counts) {
        LOG.info("Updating stable data statistics to the catalogue");

        final StableDataStatisticsUri uri = new StableDataStatisticsUri();
        uri.setHost(configurationService.get(ConfigurationKey.CATALOGUE_HOST).getValue());
        uri.setInstallationId(installationDetailsService.get().getInstallationId());

        final WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.setUrl(uri.toString());
        wsb.setPost(true);
        wsb.setType(MediaType.APPLICATION_JSON_TYPE);
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.setJson(counts);
        final ClientResponse response = catalogueClient.addAuthorizationHeaderAndCallWebService(wsb);

        if(!isValid(response)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_CENTRAL_PLATFORM_STATUS_CODE, response.getStatus());
            throw exception;
        }

        LOG.info("Stable data statistics successfully updated to the catalogue");
    }

    @Override
    public void updateUsers(List<UserDto> users) {
        LOG.info("Updating users to the catalogue");

        final UsersUri uri = new UsersUri();
        uri.setHost(configurationService.get(ConfigurationKey.CATALOGUE_HOST).getValue());
        uri.setInstallationId(installationDetailsService.get().getInstallationId());

        final WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.setUrl(uri.toString());
        wsb.setPost(true);
        wsb.setType(MediaType.APPLICATION_JSON_TYPE);
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.setJson(users);
        final ClientResponse response = catalogueClient.addAuthorizationHeaderAndCallWebService(wsb);

        if(!isValid(response)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_CENTRAL_PLATFORM_STATUS_CODE, response.getStatus());
            throw exception;
        }

        LOG.info("Users successfully updated to the catalogue");
    }

    @Override
    public void updateUserRequests(List<UserRequestDto> userRequestDtos) {
        LOG.info("Updating user requests to the catalogue");

        final UserRequestsUri uri = new UserRequestsUri();
        uri.setHost(configurationService.get(ConfigurationKey.CATALOGUE_HOST).getValue());
        uri.setInstallationId(installationDetailsService.get().getInstallationId());

        final WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.setUrl(uri.toString());
        wsb.setPost(true);
        wsb.setType(MediaType.APPLICATION_JSON_TYPE);
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.setJson(userRequestDtos);
        final ClientResponse response = catalogueClient.addAuthorizationHeaderAndCallWebService(wsb);

        if(!isValid(response)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_CENTRAL_PLATFORM_STATUS_CODE, response.getStatus());
            throw exception;
        }

        LOG.info("User requests successfully updated to the catalogue");
    }

    @Override
    public void updateConfigurations(List<ConfigurationDto> configurations) {
        LOG.info("Updating configurations to the catalogue");

        final ConfigurationsUri uri = new ConfigurationsUri();
        uri.setHost(configurationService.get(ConfigurationKey.CATALOGUE_HOST).getValue());
        uri.setInstallationId(installationDetailsService.get().getInstallationId());

        final WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.setUrl(uri.toString());
        wsb.setPost(true);
        wsb.setType(MediaType.APPLICATION_JSON_TYPE);
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.setJson(configurations);
        final ClientResponse response = catalogueClient.addAuthorizationHeaderAndCallWebService(wsb);

        if(!isValid(response)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_CENTRAL_PLATFORM_STATUS_CODE, response.getStatus());
            throw exception;
        }

        LOG.info("Configurations successfully updated to the catalogue");
    }

    @Override
    public void updateMessageStatistics(List<MessageStatisticDto> messages) {
        LOG.info("Updating message statistics to catalogue");

        final MessageStatisticUri uri = new MessageStatisticUri();
        uri.setHost(configurationService.get(ConfigurationKey.CATALOGUE_HOST).getValue());
        uri.setInstallationId(installationDetailsService.get().getInstallationId());

        final WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.setUrl(uri.toString());
        wsb.setPost(true);
        wsb.setType(MediaType.APPLICATION_JSON_TYPE);
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.setJson(messages);
        final ClientResponse response = catalogueClient.addAuthorizationHeaderAndCallWebService(wsb);

        if(!isValid(response)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_CENTRAL_PLATFORM_STATUS_CODE, response.getStatus());
            throw exception;
        }

        LOG.info("Message statistics successfully updated to the catalogue");
    }

    @Override
    public void updateStatuses(JSONObject statuses) {
        LOG.info("Updating statuses to catalogue");

        final StatusesUri uri = new StatusesUri();
        uri.setHost(configurationService.get(ConfigurationKey.CATALOGUE_HOST).getValue());
        uri.setInstallationId(installationDetailsService.get().getInstallationId());

        final WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.setUrl(uri.toString());
        wsb.setPost(true);
        wsb.setType(MediaType.APPLICATION_JSON_TYPE);
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.setJson(statuses);
        final ClientResponse response = catalogueClient.addAuthorizationHeaderAndCallWebService(wsb);

        if(!isValid(response)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_CENTRAL_PLATFORM_STATUS_CODE, response.getStatus());
            throw exception;
        }

        LOG.info("Statuses successfully updated to the catalogue");
    }

    @Override
    @Cacheable(value = CacheManagementService.registryDependentIdCache)
    public RegistryDependentIdGenerationDefinition getRegistryDependentIdGenerationDefinition(String dataCollectionName) {
        LOG.debug("Retrieving registry dependent identifier definition for data collection [{}]", dataCollectionName);

        final RegistryDependentIdGenerationDefinitionUri uri = new RegistryDependentIdGenerationDefinitionUri();
        uri.setHost(configurationService.get(ConfigurationKey.CATALOGUE_HOST).getValue());
        uri.setDataCollectionName(dataCollectionName);

        final WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.setUrl(uri.toString());
        wsb.setGet(true);
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        final ClientResponse response = catalogueClient.addAuthorizationHeaderAndCallWebService(wsb);

        if(!isValid(response)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_CENTRAL_PLATFORM_STATUS_CODE, response.getStatus());
            throw exception;
        }

        return response.getEntity(RegistryDependentIdGenerationDefinition.class);
    }

    @Override
    public String getRegistryDependentId(Organization organization, String dataCollectionName, RegistryDependentIdGenerationDefinition.Phase phase) {
        LOG.debug("Retrieving registry dependent identifier for data collection [{}] and phase [{}]", dataCollectionName, phase);

        final RegistryDependentIdGenerationUri uri = new RegistryDependentIdGenerationUri();
        uri.setHost(configurationService.get(ConfigurationKey.CATALOGUE_HOST).getValue());
        uri.setDataCollectionName(dataCollectionName);
        uri.setPhase(phase.toString());

        final WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.setUrl(uri.toString());
        wsb.setGet(true);
        wsb.setAccept(MediaType.TEXT_PLAIN_TYPE);
        wsb.addParameter("submissionDate", DateFormat.DATE_AND_TIME.format(new Date()));
        wsb.addParameter("organizationIdentificationValue", organization.getHealthDataIDValue());
        final ClientResponse response = catalogueClient.addAuthorizationHeaderAndCallWebService(wsb);

        if(!isValid(response)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_CENTRAL_PLATFORM_STATUS_CODE, response.getStatus());
            throw exception;
        }

        if(ClientResponse.Status.NO_CONTENT.getStatusCode() == response.getStatus()) {
            LOG.debug("No registry dependent identifier defined for data collection [{}] and phase [{}]", dataCollectionName, phase);
            return null;
        }

        final String registryDependentId = response.getEntity(String.class);
        LOG.debug("Registry dependent identifier generated for data collection [{}] and phase [{}]: [{}]", dataCollectionName, phase, registryDependentId);
        return registryDependentId;
    }

    private boolean isValid(ClientResponse response) {
        return response.getStatus() >= 200 && response.getStatus() < 300;
    }
}
