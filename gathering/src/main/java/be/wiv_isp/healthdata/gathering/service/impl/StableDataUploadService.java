/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.search.OrganizationSearch;
import be.wiv_isp.healthdata.gathering.action.standalone.StableDataUploadAction;
import be.wiv_isp.healthdata.gathering.dao.IStableDataUploadDao;
import be.wiv_isp.healthdata.gathering.domain.StableDataUpload;
import be.wiv_isp.healthdata.gathering.factory.IMessageFactory;
import be.wiv_isp.healthdata.gathering.service.IStableDataUploadService;
import be.wiv_isp.healthdata.orchestration.domain.DataCollection;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import be.wiv_isp.healthdata.orchestration.service.IUserDataCollectionService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.*;

@Service
public class StableDataUploadService implements IStableDataUploadService {

    private static final Logger LOG = LoggerFactory.getLogger(StableDataUploadService.class);

    @Autowired
    private IStableDataUploadDao dao;

    @Autowired
    private IMessageFactory messageFactory;

    @Autowired
    private IMessageService messageService;

    @Autowired
    private IOrganizationService organizationService;

    @Autowired
    private IUserDataCollectionService userDataCollectionService;

    @Override
    @Transactional
    public StableDataUpload create(StableDataUpload stableDataUpload) {
        return dao.create(stableDataUpload);
    }

    @Override
    @Transactional
    public void send() {

        final List<StableDataUpload> all = dao.getAll();

        if(CollectionUtils.isEmpty(all)) {
            return;
        }

        final Organization mainOrganization = organizationService.getMain();

        final Map<HealthDataIdentification, List<StableDataUpload>> stableDataPerHealthDataIdentification = new HashMap<>();
        for (StableDataUpload stableDataUpload : all) {
            final HealthDataIdentification healthDataIdentification = stableDataUpload.getHealthDataIdentification();
            if(stableDataPerHealthDataIdentification.get(healthDataIdentification) == null) {
                stableDataPerHealthDataIdentification.put(healthDataIdentification, new ArrayList<StableDataUpload>());
            }
            stableDataPerHealthDataIdentification.get(healthDataIdentification).add(stableDataUpload);
        }

        for (Map.Entry<HealthDataIdentification, List<StableDataUpload>> entry : stableDataPerHealthDataIdentification.entrySet()) {
            final HealthDataIdentification healthDataIdentification = entry.getKey();
            final List<StableDataUpload> stableDataUploadsPerIdentification = entry.getValue();

            final Map<String, List<StableDataUpload>> stableDataPerDataCollection = new HashMap<>();
            for (StableDataUpload stableDataUpload : stableDataUploadsPerIdentification) {
                final String dataCollectionName = stableDataUpload.getDataCollectionName();

                if(stableDataPerDataCollection.get(dataCollectionName) == null) {
                    stableDataPerDataCollection.put(dataCollectionName, new ArrayList<StableDataUpload>());
                }
                stableDataPerDataCollection.get(dataCollectionName).add(stableDataUpload);
            }

            for (Map.Entry<String, List<StableDataUpload>> entry1 : stableDataPerDataCollection.entrySet()) {
                final String dataCollectionName = entry1.getKey();
                final List<StableDataUpload> stableDataUploads = entry1.getValue();

                LOG.info("Sending uploaded stable data report for identification {} and data collection {}", healthDataIdentification, dataCollectionName);

                final StableDataUploadAction action = new StableDataUploadAction();
                action.setDataCollectionName(dataCollectionName);
                action.setHealthDataIdentification(healthDataIdentification);
                action.setEmails(getEmails(healthDataIdentification, dataCollectionName));

                for (StableDataUpload stableDataUpload : stableDataUploads) {
                    if(stableDataUpload.isSuccess()) {
                        action.addSuccess(stableDataUpload.getStableDataId());
                    } else {
                        action.addFailure(stableDataUpload.getStableDataId());
                    }
                    dao.delete(stableDataUpload);
                }

                final Message message = messageFactory.createOutgoingMessage(action, mainOrganization);
                messageService.create(message);
            }
        }
    }

    private Set<String> getEmails(final HealthDataIdentification healthDataIdentification, final String dataCollectionName) {
        final OrganizationSearch search = new OrganizationSearch();
        search.setHealthDataIDType(healthDataIdentification.getType());
        search.setHealthDataIDValue(healthDataIdentification.getValue());

        try {
            final Organization organization = organizationService.getUnique(search);
            return userDataCollectionService.getEmailAddresses(new DataCollection(dataCollectionName), organization);
        } catch (Exception e) {
            LOG.error(MessageFormat.format("Error while retrieving email addresses for data collection {0} and identification {1}", dataCollectionName, healthDataIdentification), e);
            return new HashSet<>();
        }
    }
}
