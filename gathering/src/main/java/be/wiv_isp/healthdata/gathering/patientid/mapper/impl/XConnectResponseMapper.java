/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.patientid.mapper.impl;

import be.fgov.ehealth.consultrn._1_0.core.PersonDataType;
import be.fgov.ehealth.consultrn._1_0.core.PersonType;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.gathering.dto.PersonDto;
import be.wiv_isp.healthdata.gathering.patientid.mapper.IPersonDtoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import xconnect.RNSearchBySSINResponse;

@Service
public class XConnectResponseMapper implements IPersonDtoMapper<RNSearchBySSINResponse> {

    private static final Logger LOG = LoggerFactory.getLogger(XConnectResponseMapper.class);

    @Override
    public PersonDto map(RNSearchBySSINResponse rnSearchBySSINResponse) {
        final PersonType personType = rnSearchBySSINResponse.getOfficialResponse().getPerson();
        final PersonDataType personData = personType.getPersonData();

        final PersonDto person = new PersonDto();
        person.setSsin(personType.getSSIN());
        person.setLastName(personData.getName().getLast());
        person.setFirstName(personData.getName().getFirst());
        person.setDateOfBirth(DateFormat.DATE.parse(personData.getBirth().getDate()));
        try {
            person.setDateOfDeath(DateFormat.DATE.parse(personData.getDecease().getDate()));
        } catch (NullPointerException e) {
            // nothing to do
        }
        try {
            person.setDistrict(personData.getAddress().getStandardAddress().getMunicipality().getPostalCode());
        } catch (NullPointerException e) {
            // nothing to do
        }
        switch (personData.getGender().getValue()) {
            case MALE: person.setGender(PersonDto.Gender.MALE); break;
            case FEMALE: person.setGender(PersonDto.Gender.FEMALE); break;
            case UNKNOWN: person.setGender(PersonDto.Gender.UNKNOWN); break;
        }
        return person;
    }
}
