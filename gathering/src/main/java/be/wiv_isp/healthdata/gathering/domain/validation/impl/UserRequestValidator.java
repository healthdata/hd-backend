/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.domain.validation.impl;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.domain.UserRequest;
import be.wiv_isp.healthdata.gathering.domain.validation.IUserRequestValidator;
import be.wiv_isp.healthdata.orchestration.dao.IUserDao;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.AuthenticationType;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import be.wiv_isp.healthdata.orchestration.domain.validator.impl.ValidatorUtils;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionService;
import be.wiv_isp.healthdata.orchestration.service.IUserManagementService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;

@Service
public class UserRequestValidator implements IUserRequestValidator {

    private static final Logger LOG = LoggerFactory.getLogger(UserRequestValidator.class);

    @Autowired
    private IUserDao userDao;
    @Autowired
    private IDataCollectionService dataCollectionService;
    @Autowired
    private IUserManagementService userManagementService;

    @Override
    public void validate(UserRequest userRequest) {
        validateOrganization(userRequest);
        validateFirstName(userRequest);
        validateLastName(userRequest);
        validateEmail(userRequest);
        validateEmailRequester(userRequest);
        validateUsername(userRequest);
        validateDataCollectionNames(userRequest);

        if (AuthenticationType.DATABASE.equals(userManagementService.getUserManagementType(userRequest.getOrganization()).getAuthenticationType())) {
            verifyUsernameAvailable(userRequest);
            verifyEmailAvailable(userRequest);
            validatePassword(userRequest);
        }
    }

    private void validatePassword(UserRequest userRequest) {
        ValidatorUtils.verifyNotNull(userRequest.getPassword(), "password");
    }

    private void validateOrganization(UserRequest userRequest) {
        ValidatorUtils.verifyNotNull(userRequest.getOrganization(), "organization");
    }

    private void validateFirstName(UserRequest userRequest) {
        ValidatorUtils.verifyNotBlank(userRequest.getFirstName(), "firstname");
    }

    private void validateLastName(UserRequest userRequest) {
        ValidatorUtils.verifyNotBlank(userRequest.getLastName(), "lastname");
    }

    private void validateEmail(UserRequest userRequest) {
        ValidatorUtils.validateEmail(userRequest.getEmail());
    }

    private void validateEmailRequester(UserRequest userRequest) {
        if(StringUtils.isNotBlank(userRequest.getEmailRequester())) {
            ValidatorUtils.validateEmail(userRequest.getEmailRequester());
        }
    }

    private void validateUsername(UserRequest userRequest) {
        ValidatorUtils.verifyNotBlank(userRequest.getUsername(), "username");
    }

    private void verifyUsernameAvailable(UserRequest userRequest) {
        final UserSearch userSearch = new UserSearch();
        userSearch.setUsername(userRequest.getUsername());
        userSearch.setOrganization(userRequest.getOrganization());
        final User user = userDao.getUnique(userSearch);
        if(user != null) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_CONFLICT, "username already exists");
            throw exception;
        }
    }

    private void verifyEmailAvailable(UserRequest userRequest) {
        final UserSearch userSearch = new UserSearch();
        userSearch.setEmail(userRequest.getEmail());
        userSearch.setOrganization(userRequest.getOrganization());
        final User user = userDao.getUnique(userSearch);
        if(user != null) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_CONFLICT, "email already exists");
            throw exception;
        }
    }

    private void validateDataCollectionNames(UserRequest userRequest) {
        ValidatorUtils.verifyNotNull(userRequest.getDataCollectionNames(), "data collections");
        final Set<String> dataCollectionsForOrganization = dataCollectionService.getDataCollectionsFlat(userRequest.getOrganization()); // TODO change Flat to Groups
        if(!dataCollectionsForOrganization.containsAll(userRequest.getDataCollectionNames())) {
            final Collection<String> toBeRemovedDataCollections = CollectionUtils.removeAll(userRequest.getDataCollectionNames(), dataCollectionsForOrganization);
            LOG.warn("The following set of data collection is removed from the user request with id [{}] since his organization does not have access to it anymore: {}", userRequest.getId(), toBeRemovedDataCollections);
            userRequest.getDataCollectionNames().retainAll(dataCollectionsForOrganization);
        }
    }
}
