/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.tasks;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.gathering.service.IFollowUpService;
import be.wiv_isp.healthdata.orchestration.service.IAbstractRegistrationWorkflowService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionDefinitionForwardService;
import be.wiv_isp.healthdata.orchestration.service.IMappingService;
import be.wiv_isp.healthdata.orchestration.tasks.HealthDataTask;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FollowUpCreationTask extends HealthDataTask {

    private static final Logger LOG = LoggerFactory.getLogger(FollowUpCreationTask.class);

    @Autowired
    private IFollowUpService followUpService;

    @Autowired
    private IAbstractRegistrationWorkflowService<RegistrationWorkflow, RegistrationWorkflowSearch> workflowService;

    @Autowired
    private IDataCollectionDefinitionForwardService catalogueService;

    @Autowired
    private IMappingService mappingService;

    @Override
    public String getName() {
        return "FOLLOW_UP_CREATION";
    }

    @Override
    protected void execute() {
        LOG.info("Retrieving all data collection definition ids");
        final List<Long> dataCollectionDefinitionIds = workflowService.getDataCollectionDefinitionIds();
        LOG.info("Data collection definition ids: [{}]", dataCollectionDefinitionIds);

        for (final Long dcdId : dataCollectionDefinitionIds) {
            final DataCollectionDefinitionDtoV7 dcd = catalogueService.get(dcdId);
            if(CollectionUtils.isEmpty(dcd.getFollowUpDefinitions())) {
                LOG.info("Data collection definition with id [{}] and name [{}] has no follow up definitions", dcd.getId(), dcd.getDataCollectionName());
                continue;
            }

            LOG.info("Data collection definition with id [{}] and name [{}] has follow up definitions", dcd.getId(), dcd.getDataCollectionName());

            final RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
            search.setDataCollectionDefinitionId(dcdId);
            LOG.info("Retrieving workflows with data collection definition id [{}]", dcd.getId());
            final List<Long> workflowIds = workflowService.getWorkflowIds(search);
            LOG.info("{} workflow(s) found", workflowIds.size());
            for (final Long workflowId : workflowIds) {
                final RegistrationWorkflow workflow = workflowService.get(workflowId);
                if(CollectionUtils.isNotEmpty(workflow.getFollowUps())) {
                    LOG.debug("workflow already contains follow ups. Skipping workflow");
                    continue;
                }

                workflow.setFollowUps(followUpService.createFollowUps(dcd.getFollowUpDefinitions()));

                if(workflow.getSubmittedOn() != null) {
                    followUpService.updateScheduling(workflow.getFollowUps(), mappingService.getComputedExpressions(workflow));
                }

                workflowService.update(workflow);
            }
        }
    }
}
