/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.gathering.domain.search;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;

public class RegistrationCountGroupByOrganizationDcdStatus {

    private String organizationIdentificationValue;
    private Long dataCollectionDefinitionId;
    private WorkflowStatus status;
    private Long count;

    public RegistrationCountGroupByOrganizationDcdStatus(String organizationIdentificationValue, Long dataCollectionDefinitionId, WorkflowStatus status, Long count) {
        this.organizationIdentificationValue = organizationIdentificationValue;
        this.dataCollectionDefinitionId = dataCollectionDefinitionId;
        this.status = status;
        this.count = count;
    }

    public String getOrganizationIdentificationValue() {
        return organizationIdentificationValue;
    }

    public void setOrganizationIdentificationValue(String organizationIdentificationValue) {
        this.organizationIdentificationValue = organizationIdentificationValue;
    }

    public Long getDataCollectionDefinitionId() {
        return dataCollectionDefinitionId;
    }

    public void setDataCollectionDefinitionId(Long dataCollectionDefinitionId) {
        this.dataCollectionDefinitionId = dataCollectionDefinitionId;
    }

    public WorkflowStatus getStatus() {
        return status;
    }

    public void setStatus(WorkflowStatus status) {
        this.status = status;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof RegistrationCountGroupByOrganizationDcdStatus))
            return false;

        RegistrationCountGroupByOrganizationDcdStatus that = (RegistrationCountGroupByOrganizationDcdStatus) o;

        if (organizationIdentificationValue != null ? !organizationIdentificationValue.equals(that.organizationIdentificationValue) : that.organizationIdentificationValue != null)
            return false;
        if (dataCollectionDefinitionId != null ? !dataCollectionDefinitionId.equals(that.dataCollectionDefinitionId) : that.dataCollectionDefinitionId != null)
            return false;
        if (status != that.status)
            return false;
        return !(count != null ? !count.equals(that.count) : that.count != null);

    }

    @Override
    public int hashCode() {
        int result = organizationIdentificationValue != null ? organizationIdentificationValue.hashCode() : 0;
        result = 31 * result + (dataCollectionDefinitionId != null ? dataCollectionDefinitionId.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (count != null ? count.hashCode() : 0);
        return result;
    }
}
