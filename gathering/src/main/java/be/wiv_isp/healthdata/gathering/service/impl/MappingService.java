/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.common.json.SerializableJsonObject;
import be.wiv_isp.healthdata.gathering.domain.MappingContext;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.service.IMappingService;
import be.wiv_isp.healthdata.orchestration.api.mapper.MappingResponseMapper;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import be.wiv_isp.healthdata.orchestration.mapping.IMappingContext;
import be.wiv_isp.healthdata.orchestration.mapping.MappingRequest;
import be.wiv_isp.healthdata.orchestration.mapping.MappingResponse;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractMappingService;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class MappingService extends AbstractMappingService<RegistrationWorkflow> implements IMappingService {

    @Override
    public DocumentData refresh(MappingContext context, byte[] dcdContent, RegistrationWorkflow workflow) {
        return refresh(context, dcdContent, workflow.getDocument().getDocumentContent(), workflow.getDocument().getPrivateContent(), workflow.getMetaData(), new DocumentData());
    }

    @Override
    public DocumentData refresh(MappingContext context, byte[] dcdContent, DocumentData documentData, Map<String, String> metaData) {
        return refresh(context, dcdContent, documentData.getContent(), documentData.getPrivate(), metaData, documentData);
    }

    @Override
    protected void putAllDocumentContent(JSONObject documentObject, RegistrationWorkflow workflow) {
        putAllDocumentContent(documentObject, workflow.getDocument().getDocumentContent(), workflow.getDocument().getPrivateContent(), workflow.getMetaData());
    }

    private DocumentData refresh(IMappingContext context, byte[] dcdContent, byte[] documentContent, byte[] privateDocumentContent, Map<String, String> metadata, DocumentData documentData) {
        final MappingRequest request = new MappingRequest();
        request.getInput().put(MappingRequest.Input.DATA_COLLECTION_DEFINITION, JsonUtils.createSerializableJsonObject(dcdContent));
        request.getTargets().add(MappingRequest.Target.DOCUMENT);
        request.getTargets().add(MappingRequest.Target.TO_BE_CODED);
        final JSONObject documentObject = new SerializableJsonObject();
        putAllDocumentContent(documentObject, documentContent, privateDocumentContent, metadata);
        request.getInput().put(MappingRequest.Input.DOCUMENT, documentObject);
        request.getInput().put(MappingRequest.Input.CONTEXT, context);

        final JSONObject response = get(request, Endpoint.REFRESH);
        final MappingResponse mappingResponse = MappingResponseMapper.convert(response);

        documentData.setContent(mappingResponse.getDocumentContent());
        documentData.setPrivate(mappingResponse.getPrivateData());
        documentData.setCoded(mappingResponse.getToBeCoded());
        return documentData;
    }

    private void putAllDocumentContent(JSONObject documentObject, byte[] documentContent, byte[] privateDocumentContent, Map<String, String> metadata) {
        JsonUtils.put(documentObject, MappingRequest.Fields.CONTENT, JsonUtils.createJsonObject(documentContent));
        if (privateDocumentContent != null)
            JsonUtils.put(documentObject, MappingRequest.Fields.PRIVATE, JsonUtils.createJsonObject(privateDocumentContent));
        else
            JsonUtils.put(documentObject, MappingRequest.Fields.PATIENT_ID, new JSONObject(metadata));
    }

}
