/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.gathering.tasks;

import be.wiv_isp.healthdata.orchestration.service.impl.EncryptionService;
import be.wiv_isp.healthdata.gathering.centralplatform.service.ICentralPlatformService;
import be.wiv_isp.healthdata.gathering.domain.InstallationDetails;
import be.wiv_isp.healthdata.gathering.dto.InstallationDto;
import be.wiv_isp.healthdata.gathering.service.IInstallationDetailsService;
import be.wiv_isp.healthdata.orchestration.tasks.HealthDataTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InstallationRegistrationTask extends HealthDataTask {

    private static final Logger LOG = LoggerFactory.getLogger(InstallationRegistrationTask.class);

    @Autowired
    private IInstallationDetailsService installationDetailsService;

    @Autowired
    private ICentralPlatformService centralPlatformService;

    @Override
    public String getName() {
        return "INSTALLATION_REGISTRATION";
    }

    @Override
    protected void execute() {
        if(!isInstallationRegistered()) {
            LOG.info("Registering HD4DP installation to the catalogue");

            final InstallationDto installation = centralPlatformService.registerInstallation();

            final InstallationDetails installationDetails = new InstallationDetails();
            installationDetails.setInstallationId(installation.getId());
            installationDetails.setCatalogueUsername(installation.getUsername());
            installationDetails.setCataloguePassword(new EncryptionService().encrypt(installation.getPassword()));
            installationDetailsService.create(installationDetails);

            LOG.info("Installation successfully registered to the catalogue (installation id: {})", installation.getId());
        }
    }

    private boolean isInstallationRegistered() {
        return installationDetailsService.get() != null;
    }

}
