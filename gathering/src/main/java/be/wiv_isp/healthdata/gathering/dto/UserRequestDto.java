/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.dto;

import be.wiv_isp.healthdata.gathering.domain.UserRequest;
import be.wiv_isp.healthdata.orchestration.dto.AbstractUserRequestDto;

public class UserRequestDto extends AbstractUserRequestDto<UserRequest> {

    private String username;
    private String organizationIdentificationValue; // used by central platform

    public UserRequestDto(UserRequest userRequest) {
        super(userRequest);
        username = userRequest.getUsername();
        dataCollectionNames = userRequest.getDataCollectionNames();
        organizationIdentificationValue = userRequest.getOrganization().getHealthDataIDValue();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOrganizationIdentificationValue() {
        return organizationIdentificationValue;
    }

    public void setOrganizationIdentificationValue(String organizationIdentificationValue) {
        this.organizationIdentificationValue = organizationIdentificationValue;
    }
}
