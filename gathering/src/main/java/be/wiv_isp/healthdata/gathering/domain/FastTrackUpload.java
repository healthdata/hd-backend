/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.gathering.domain;

import be.wiv_isp.healthdata.common.util.StringUtils;
import be.wiv_isp.healthdata.gathering.domain.enumeration.FastTrackStatus;

import javax.persistence.*;
import java.io.File;
import java.sql.Timestamp;
import java.util.*;

@Entity
@Table(name = "FAST_TRACK_UPLOAD")
public class FastTrackUpload {

    public enum Status {
        NOT_UPLOADED, UPLOAD_FAILED, UPLOAD_SUCCESS
    }
    public enum Mode {
        FAST_TRACK, MERGE , OVERWRITE
    }

    public enum Master {
        CSV , DATABASE
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "UPLOAD_ID")
    private Long id;
    @Column(name = "DCD_ID", nullable = false)
    private Long dataCollectionDefinitionId;
    @Column(name = "FILE_NAME", nullable = false)
    private String fileName;
    @Column(name = "PATH")
    private String path;
    @Lob
    @Column(name = "CONTENT", nullable = false)
    private String content;
    @Enumerated(EnumType.STRING)
    @Column(name = "\"MODE\"", nullable = false)  // MODE is a reserved keyword in Oracle, hence it must be quoted
    private Mode mode;
    @Enumerated(EnumType.STRING)
    @Column(name = "MASTER")
    private Master master;
    @Column(name = "USERNAME")
    private String username;
    @Column(name = "ORGANIZATION_ID", nullable = false)
    private Long organizationId;
    @Column(name = "TOTAL_COUNT")
    private Long totalCount;
    @Column(name = "SUCCESS_COUNT")
    private long successCount;
    @Column(name = "IGNORE_COUNT")
    private long ignoreCount;
    @Column(name = "ERROR_COUNT")
    private long errorCount;
    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS", nullable = false)
    private Status status;
    @Lob
    @Column(name = "ERRORS")
    private String errors;
    @Transient
    private Map<Long, FastTrackRecord> records;
    @Column(name = "CREATED_ON", nullable = false)
    private Timestamp createdOn;
    @Column(name = "STARTED_ON")
    private Timestamp startedOn;
    @Column(name = "STOPPED_ON")
    private Timestamp stoppedOn;

    private transient List<File> attachmentFiles;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDataCollectionDefinitionId() {
        return dataCollectionDefinitionId;
    }

    public void setDataCollectionDefinitionId(Long dataCollectionDefinitionId) {
        this.dataCollectionDefinitionId = dataCollectionDefinitionId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public Master getMaster() {
        return master;
    }

    public void setMaster(Master master) {
        this.master = master;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    public long getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(long successCount) {
        this.successCount = successCount;
    }

    public long getIgnoreCount() {
        return ignoreCount;
    }

    public void setIgnoreCount(long ignoreCount) {
        this.ignoreCount = ignoreCount;
    }

    public long getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(long errorCount) {
        this.errorCount = errorCount;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getStartedOn() {
        return startedOn;
    }

    public void setStartedOn(Timestamp startedOn) {
        this.startedOn = startedOn;
    }

    public Timestamp getStoppedOn() {
        return stoppedOn;
    }

    public void setStoppedOn(Timestamp stoppedOn) {
        this.stoppedOn = stoppedOn;
    }

    public Map<Long, FastTrackRecord> getRecords() {
        if(records == null) {
            records = new HashMap<>();
        }
        return records;
    }

    public void setRecords(Map<Long, FastTrackRecord> records) {
        this.records = records;
        Long errorCount = 0L;
        Long successCount = 0L;
        Long ignoreCount = 0L;
        for (FastTrackRecord fastTrackRecord : records.values()) {
            if(FastTrackStatus.SUCCESS.equals(fastTrackRecord.getStatus())) {
                successCount++;
            } else if(FastTrackStatus.ERROR.equals(fastTrackRecord.getStatus())) {
                errorCount++;
            } else if(FastTrackStatus.IGNORE.equals(fastTrackRecord.getStatus())) {
                ignoreCount++;
            }
        }
        setSuccessCount(successCount);
        setErrorCount(errorCount);
        setIgnoreCount(ignoreCount);
    }

    @PrePersist
    public void onCreate() {
        setCreatedOn(new Timestamp(new Date().getTime()));
        if(status == null) {
            status = Status.NOT_UPLOADED;
        }
    }

    public boolean hasErrors() {
        return hasStatus(FastTrackStatus.ERROR);
    }

    public boolean hasIgnore() {
        return hasStatus(FastTrackStatus.IGNORE);
    }

    public boolean hasSuccess() {
        return hasStatus(FastTrackStatus.SUCCESS);
    }

    private boolean hasStatus(FastTrackStatus status ) {
        for (FastTrackRecord fastTrackRecord : records.values()) {
            if(status.equals(fastTrackRecord.getStatus())) {
                return true;
            }
        }
        return false;
    }

    public List<File> getAttachmentFiles() {
        return attachmentFiles;
    }

    public void setAttachmentFiles(List<File> attachmentFiles) {
        this.attachmentFiles = attachmentFiles;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof FastTrackUpload) {
            FastTrackUpload other = (FastTrackUpload) o;

            return Objects.equals(id, other.id)
                    && Objects.equals(fileName, other.fileName)
                    && Objects.equals(content, other.content)
                    && Objects.equals(records, other.records);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                this.id,
                this.fileName,
                this.content,
                this.records);
    }

    @Override
    public String toString() {
        return StringUtils.toString(this);
    }
}
