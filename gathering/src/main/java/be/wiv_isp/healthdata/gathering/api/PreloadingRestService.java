/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.api;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.api.mapper.FastTrackUploadMapper;
import be.wiv_isp.healthdata.gathering.domain.FastTrackUpload;
import be.wiv_isp.healthdata.gathering.service.IFastTrackService;
import be.wiv_isp.healthdata.gathering.tasks.FastTrackUploadTask;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.service.IAutowireService;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.QuoteMode;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Date;

@Component
@Path("/preloading")
public class PreloadingRestService {

	private static final Logger LOG = LoggerFactory.getLogger(PreloadingRestService.class);

	@Autowired
	private IFastTrackService fastTrackService;
	@Autowired
	private TaskScheduler taskScheduler;
	@Autowired
	private IAutowireService autowireService;

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@PreAuthorize("hasRole('ROLE_USER')")
	public Response preload(String json) throws Exception {
		FastTrackUpload upload = FastTrackUploadMapper.convert(new JSONObject(json));
		LOG.info("Preloading data for dataCollectionDefinition with id [{}].", upload.getDataCollectionDefinitionId());
		final HDUserDetails principal = AuthenticationService.getUserDetails();
		upload.setUsername(principal.getUsername());
		upload.setOrganizationId(principal.getOrganization().getId());
		upload.setFileName("frontendUpload.csv");



		final Reader reader = new StringReader(upload.getContent());
		final CSVFormat csvFormat = CSVFormat.DEFAULT.withDelimiter(';') //
				.withEscape('\\') //
				.withQuoteMode(QuoteMode.MINIMAL) //
				.withHeader();

		CSVParser parser;
		try {
			parser = new CSVParser(reader, csvFormat);
		} catch (Exception e) {
			upload.setErrors(e.getMessage());
			upload.setStatus(FastTrackUpload.Status.UPLOAD_FAILED);
			try {
				reader.close();
			} catch (IOException e1) {
			}
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.ERROR_WHILE_READING_FILE, upload.getFileName());
			throw exception;
		}
		upload.setTotalCount(Long.valueOf(parser.getRecords().size()));


		FastTrackUploadTask task = new FastTrackUploadTask();
		task.setUpload(upload);

		autowireService.autowire(task);

		taskScheduler.schedule(task, new Date());

		return Response.noContent().build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@PreAuthorize("hasRole('ROLE_USER')")
	public Response status() {
		return fastTrackService.status();
	}
}
