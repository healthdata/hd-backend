/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.action.workflow;

import be.wiv_isp.healthdata.common.domain.RegistryDependentIdGenerationDefinition;
import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.common.exception.HealthDataAssert;
import be.wiv_isp.healthdata.gathering.centralplatform.service.ICentralPlatformService;
import be.wiv_isp.healthdata.gathering.domain.MappingContext;
import be.wiv_isp.healthdata.gathering.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.StableData;
import be.wiv_isp.healthdata.gathering.factory.IMappingContextFactory;
import be.wiv_isp.healthdata.gathering.service.IFollowUpService;
import be.wiv_isp.healthdata.gathering.service.IMappingService;
import be.wiv_isp.healthdata.gathering.service.IStableDataService;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.domain.mapper.AttachmentMapper;
import be.wiv_isp.healthdata.orchestration.domain.mapper.NoteMessageDtoMapper;
import be.wiv_isp.healthdata.orchestration.factory.IAbstractMessageFactory;
import be.wiv_isp.healthdata.orchestration.service.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.core.Response;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.*;

public class SendRegistrationAction extends AbstractRegistrationWorkflowAction {

	private static final Logger LOG = LoggerFactory.getLogger(SendRegistrationAction.class);

	@Autowired
	private IIdentificationService identificationService;
	@Autowired
	private IDataCollectionDefinitionForwardService catalogueService;
	@Autowired
	private IMessageService messageService;
	@Autowired
	private IAbstractMessageFactory<RegistrationWorkflow, ParticipationWorkflow> messageFactory;
	@Autowired
	private IFollowUpService followUpService;
	@Autowired
	private IMappingService mappingService;
	@Autowired
	private IStableDataService stableDataService;
	@Autowired
	private IMappingContextFactory mappingContextFactory;
	@Autowired
	private be.wiv_isp.healthdata.gathering.service.IDataCollectionService dataCollectionService;
	@Autowired
	private INoteMigrationService<RegistrationWorkflow> noteMigrationService;
	@Autowired
	private ICentralPlatformService centralPlatformService;
	@Autowired
	protected INoteService noteService;

	@Override
	public WorkflowActionType getAction() {
		return WorkflowActionType.SEND;
	}

	@Override
	public String getSendStatus() {
		return SendStatus.PENDING;
	}

	@Override
	public RegistrationWorkflow preExecute(RegistrationWorkflow workflow, HDUserDetails userDetails) {
		super.preExecute(workflow, userDetails);

		final Long dcdId = workflow.getDataCollectionDefinitionId();
		final Organization organization = workflow.getOrganization();
		dataCollectionDefinition = catalogueService.get(dcdId, organization);

		if(workflow.getSubmittedOn() == null) {
			LOG.debug("Checking whether a registry dependent id must be generated");
			final RegistryDependentIdGenerationDefinition def = centralPlatformService.getRegistryDependentIdGenerationDefinition(dataCollectionDefinition.getDataCollectionName());
			final boolean generateRegistryDependentId = def.isGenerationEnabled() && RegistryDependentIdGenerationDefinition.Phase.SUBMISSION.equals(def.getPhase());
			LOG.debug("Registry dependent id must be generated: {}", generateRegistryDependentId);
			if(generateRegistryDependentId) {
				HealthDataAssert.assertNull(workflow.getRegistryDependentId(), Response.Status.INTERNAL_SERVER_ERROR , "registry dependent identifier has already been generated");
				registryDependentId = centralPlatformService.getRegistryDependentId(workflow.getOrganization(), dataCollectionDefinition.getDataCollectionName(), RegistryDependentIdGenerationDefinition.Phase.SUBMISSION);
				HealthDataAssert.assertNotNull(registryDependentId, Response.Status.INTERNAL_SERVER_ERROR, "generated registry dependent identifier is empty");

				LOG.debug("Injecting registry dependent identifier in the document data");
				final MappingContext context = mappingContextFactory.create(workflow.getOrganization(), workflow.getFollowUps(), registryDependentId);
				documentData = mappingService.refresh(context, dataCollectionDefinition.getContent(), workflow);
			}
		}

		return workflow;
	}

	@Override
	public RegistrationWorkflow doExecute(RegistrationWorkflow workflow, HDUserDetails userDetails) {
		if (!noteMigrationService.isMigrated(workflow)) {
			noteMigrationService.migrateCommentsToNotes(workflow, dataCollectionDefinition.getContent());
		}

		if(this.documentData != null) {
			workflowService.setDocumentData(workflow, documentData, null, false);
		}

		saveStableData(workflow, dataCollectionDefinition);

		final CreateForReviewRegistrationAction action = new CreateForReviewRegistrationAction();
		action.setDataCollectionDefinitionId(workflow.getDataCollectionDefinitionId());
		action.setDocumentContent(workflow.getDocument().getDocumentContent());
		action.setIdentificationWorkflow(identificationService.getHealthDataIdentification(workflow.getOrganization())); // TODO - send this information in all messages
		action.setDocumentVersion(workflow.getDocument().getVersion());
		action.setFlags(workflow.getFlags());

		final List<Attachment> attachments = workflow.getDocument().getAttachments();
		action.setAttachmentDtos(AttachmentMapper.mapToAttachmentDtos(attachments, true));

		action.setNoteMaxOrder(workflow.getDocument().getNoteMaxOrder());
		List<Note> notes = noteService.getByDocumentId(WorkflowType.REGISTRATION, workflow.getDocument().getId());
		action.setNotesMessage(NoteMessageDtoMapper.mapToNoteDtos(notes));

		List<RegistrationWorkflowHistory> history = workflow.getHistory();
		for (RegistrationWorkflowHistory workflowHistory : history) {
			if (StringUtils.isNotBlank(workflowHistory.getSource())) {
				action.setSource(workflowHistory.getSource());
				action.setFile(workflowHistory.getFile());
				action.setMessageType(workflowHistory.getMessageType());
				break;
			}
		}

		if(CollectionUtils.isNotEmpty(workflow.getFollowUps())) {
			followUpService.updateScheduling(workflow.getFollowUps(), mappingService.getComputedExpressions(workflow));
			followUpService.markActiveFollowUpsSubmitted(workflow.getFollowUps());
		}
		action.setFollowUps(workflow.getFollowUps());

		final Map<String,String> dataSources = new HashMap<>();
		for (DataSource dataSource : workflow.getDocument().getDataSources())
			dataSources.put(dataSource.getFieldPath(), dataSource.getSource());
		action.setDataSources(dataSources);

		if(registryDependentId != null) {
			HealthDataAssert.assertNull(workflow.getRegistryDependentId(), Response.Status.INTERNAL_SERVER_ERROR , "registry dependent identifier was previously set");
			workflow.setRegistryDependentId(registryDependentId);
		}

		if(workflow.getSubmittedOn() == null) {
			// Important to not update the submission date everytime a registration is sent. Reason is that follow-ups are scheduled according to the initial submission date.
			workflow.setSubmittedOn(new Timestamp(new Date().getTime()));
		}

		final Message message = messageFactory.createOutgoingMessage(action, workflow);
		messageService.create(message);

		workflow.getFlags().setFollowUp(false);
		workflow.getFlags().setCorrections(false);

		setEndStatus(WorkflowStatus.SUBMITTED);
		return workflow;
	}

	private void saveStableData(RegistrationWorkflow workflow, DataCollectionDefinitionDtoV7 dcd) {
		Set<String> stableDataCollections = dataCollectionService.getStableData();
		if(stableDataCollections.contains(workflow.getDataCollectionName())) {
			String csv = mappingService.mapFromJsonToCsv(workflow, mappingContextFactory.create(workflow), dcd.getContent(), true);
			StableData stableData = new StableData();

            stableData.setPatientId(workflow.getPatientId());
            stableData.setDataCollectionName(workflow.getDataCollectionName());
            stableData.setCsv(csv.getBytes(StandardCharsets.UTF_8));
            stableData.setValidFrom(new Timestamp(new Date().getTime()));
            stableData.setOrganization(workflow.getOrganization());
            stableDataService.create(stableData);
        }
	}

	@Override
	public WorkflowType getWorkflowType() {
		return WorkflowType.REGISTRATION;
	}
}
