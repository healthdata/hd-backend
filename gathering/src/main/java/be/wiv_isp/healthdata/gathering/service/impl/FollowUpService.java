/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.common.domain.Baseline;
import be.wiv_isp.healthdata.common.dto.FollowUpDefinitionDto;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataAssert;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.service.IFollowUpService;
import be.wiv_isp.healthdata.gathering.service.IRegistrationWorkflowService;
import be.wiv_isp.healthdata.orchestration.dao.IFollowUpDao;
import be.wiv_isp.healthdata.orchestration.domain.FollowUp;
import be.wiv_isp.healthdata.orchestration.domain.PeriodExpression;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractFollowUpService;
import be.wiv_isp.healthdata.orchestration.util.DateUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class FollowUpService extends AbstractFollowUpService implements IFollowUpService {

    private static final Logger LOG = LoggerFactory.getLogger(FollowUpService.class);

    @Autowired
    protected IFollowUpDao dao;

    @Autowired
    protected IRegistrationWorkflowService registrationWorkflowService;

    public FollowUpService() {
        super(FollowUp.class);
    }

    @Override
    protected IFollowUpDao<FollowUp> getDao() {
        return dao;
    }

    @Override
    public List<FollowUp> createFollowUps(final List<FollowUpDefinitionDto> followUpDefinitionDtos) {
        LOG.info("Creating follow up(s)");

        final List<FollowUp> followUps = new ArrayList<>();
        if(CollectionUtils.isNotEmpty(followUpDefinitionDtos)) {
            for (final FollowUpDefinitionDto definition : followUpDefinitionDtos) {
                LOG.debug("FollowUp[{}]: creating follow up", definition.getLabel());

                final FollowUp followUp = new FollowUp();
                followUp.setName(definition.getName());
                followUp.setLabel(definition.getLabel());
                followUp.setDescription(definition.getDescription());
                followUp.setTiming(definition.getTiming());
                followUp.setBaseline(definition.getBaseline());
                followUp.setConditions(definition.getConditions());
                followUp.setActive(false);
                followUp.setActivationDate(null);
                followUp.setSubmittedOn(null);

                followUps.add(followUp);
            }
        }

        LOG.info("{} follow up(s) created", followUps.size());
        return followUps;
    }

    @Override
    public void updateScheduling(final List<FollowUp> followUps, final JSONObject computedExpressions) {
        if(CollectionUtils.isNotEmpty(followUps)) {
            for(final FollowUp followUp : followUps) {
                updateScheduling(followUp, computedExpressions);
            }
        }
    }

    private void updateScheduling(FollowUp followUp, JSONObject computedExpressions) {
        LOG.debug("FollowUp[{}]: updating follow up scheduling", followUp.getId());

        if(followUp.isActive()) {
            LOG.debug("FollowUp[{}]: follow up is already active, doing nothing", followUp);
            return;
        }

        if(isAllConditionsValid(followUp.getConditions(), computedExpressions)) {
            if(!followUp.isScheduled()) {
                LOG.debug("FollowUp[{}]: follow up conditions are validated. Scheduling follow up.", followUp.getId());
                schedule(followUp, computedExpressions);
            } else {
                LOG.debug("FollowUp[{}]: follow up conditions are validated, but follow up is already scheduled, doing nothing.", followUp.getId());
            }
        } else {
            if(followUp.isScheduled()) {
                LOG.debug("FollowUp[{}]: follow up conditions are not validated. Unscheduling follow up.", followUp.getId());
                unschedule(followUp);
            } else {
                LOG.debug("FollowUp[{}]: follow up conditions are not validated, but follow up is not scheduled, doing nothing.", followUp.getId());
            }
        }
    }

    @Override
    public boolean activateIfValid(final List<FollowUp> followUps, final JSONObject computedExpressions) {
        boolean newActiveFollowUp = false;
        if(CollectionUtils.isNotEmpty(followUps)) {
            for (final FollowUp followUp : followUps) {
                newActiveFollowUp = newActiveFollowUp || activateIfValid(followUp, computedExpressions);
            }
        }
        return newActiveFollowUp;
    }

    private boolean activateIfValid(final FollowUp followUp, final JSONObject computedExpressions) {
        LOG.debug("FollowUp[{}]: activating follow up if all conditions are met", followUp.getLabel());

        if(followUp.isActive()) {
            LOG.debug("FollowUp[{}]: follow up is already active, doing nothing", followUp.getLabel());
            return false;
        }

        if(!followUp.isActivationDateOver()) {
            LOG.debug("FollowUp[{}]: activation date is not over, doing nothing", followUp.getLabel());
            return false;
        }

        if(isAllConditionsValid(followUp.getConditions(), computedExpressions)) {
            LOG.info("FollowUp[{}]: activating follow up", followUp.getLabel());
            followUp.setActive(true);
            return true;
        }

        return false;
    }

    @Override
    public void markActiveFollowUpsSubmitted(final List<FollowUp> followUps) {
        if(CollectionUtils.isNotEmpty(followUps)) {
            for (final FollowUp followUp : followUps) {
                if (followUp.isActive() && !followUp.isSubmitted()) {
                    LOG.debug("FollowUp[{}]: marking follow up as submitted", followUp.getLabel());
                    followUp.setSubmittedOn(DateUtils.currentTimestamp());
                }
            }
        }
    }

    private boolean isAllConditionsValid(final List<String> conditions, final JSONObject computedExpressions) {
        for (final String condition : conditions) {
            try {
                if(computedExpressions.getBoolean(condition)) {
                    LOG.debug("[{}] computed expression was evaluated to true", condition);
                } else {
                    LOG.debug("[{}] computed expression was evaluated to false", condition);
                    return false;
                }
            } catch (JSONException e) {
                LOG.error("Unable to retrieve the result of the [{}] computed expression. Evaluated as false. Computed expressions: {}", condition, computedExpressions);
                return false;
            }
        }
        return true;
    }

    private void schedule(final FollowUp followUp, final JSONObject computedExpressions) {
        LOG.debug("FollowUp[{}]: scheduling follow up", followUp.getLabel());

        HealthDataAssert.assertFalse(followUp.isActive(), Response.Status.INTERNAL_SERVER_ERROR, "can not schedule an already active follow up");

        final Date activationDate = getActivationDate(followUp, computedExpressions);
        if(activationDate != null) {
            final String activationDateString = DateFormat.DATE_AND_TIME.format(activationDate);
            LOG.info("FollowUp[{}]: setting activation date to {}", followUp.getLabel(), activationDateString);
            followUp.setActivationDate(new Timestamp(activationDate.getTime()));
        } else {
            LOG.warn("FollowUp[{}]: not scheduling follow up because the activation date could not be computed", followUp.getLabel());
        }
    }

    private Date getActivationDate(final FollowUp followUp, final JSONObject computedExpressions) {
        LOG.debug("FollowUp[{}]: computing activation date", followUp.getLabel());

        final Date baselineDate = getBaselineDate(followUp, computedExpressions);
        LOG.debug("FollowUp[{}]: baseline date: {}", followUp.getLabel(), DateFormat.DATE_AND_TIME.format(baselineDate));
        if(baselineDate == null) {
            return null;
        }
        final String timing = followUp.getTiming();
        LOG.debug("FollowUp[{}]: timing value: {}", followUp.getLabel(), timing);
        final Date activationDate = DateUtils.add(baselineDate, new PeriodExpression(timing));
        LOG.debug("FollowUp[{}]: activation date: {}", followUp.getLabel(), DateFormat.DATE_AND_TIME.format(activationDate));
        return activationDate;
    }

    private Date getBaselineDate(final FollowUp followUp, final JSONObject computedExpressions) {
        LOG.debug("FollowUp[{}]: retrieving baseline date", followUp.getLabel());
        final Baseline baseline = followUp.getBaseline();
        LOG.debug("FollowUp[{}]: baseline type: {}", followUp.getLabel(), baseline.getType());

        switch (baseline.getType()) {
            case REGISTRATION_SUBMISSION_DATE:
                final RegistrationWorkflow workflow = registrationWorkflowService.getByFollowUpId(followUp.getId());
                final Date submissionDate = workflow.getSubmittedOn();
                LOG.debug("Registration[{}]: submission date: {}", workflow.getId(), DateFormat.DATE_AND_TIME.format(submissionDate));
                Date baselineDate = submissionDate;
                if(baselineDate == null) {
                    LOG.debug("FollowUp[{}]: submission date is not specified. Setting base line date to the current date.", followUp.getLabel());
                    baselineDate = new Date();
                }
                return baselineDate;
            case COMPUTED_EXPRESSION:
                final String baselineValue = baseline.getValue();
                LOG.debug("FollowUp[{}]: baseline value: {}", followUp.getLabel(), baselineValue);
                if(computedExpressions.isNull(baselineValue)) {
                    /*
                        If the required computed expression is not returned by the mapping API, it can be due to one of the following reasons:
                            1. the document value referenced by the computed expression is not filled in. This is not an error situation, the document can be submitted.
                            2. the expression can not be computed because the data collection definition is inconsistent (ex: computed expression references an unexisting field)
                        It is currently not possible to distinguish these two situations, as they both return the same result. To not block the
                        sending process in situation 1, we do not throw an error when the computed expression is not returned by the mapping API.
                    */
                    LOG.debug("Computed expression [{}] not found. The corresponding document value is not filled in or the data collection definition is inconsistent.", baselineValue);
                    return null;
                }
                final String dateAsString = JsonUtils.getString(computedExpressions, baselineValue);
                LOG.debug("Result of the [{}] computed expression: [{}]", baselineValue, dateAsString);
                return DateFormat.DATE.parse(dateAsString);
            default:
                HealthDataException exception = new HealthDataException();
                exception.setExceptionType(ExceptionType.METHOD_NOT_ALLOWED, MessageFormat.format("unsupported baseline type : {0}", baseline.getType()));
                throw exception;
        }
    }
}
