/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.action.standalone;

import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.action.standalone.AbstractUserUpdateAction;
import org.springframework.stereotype.Component;

@Component
public class UserUpdateAction extends AbstractUserUpdateAction {

	@Override
	public void execute() {
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.ACTION_NOT_ALLOWED_ON_PLATFORM, this.getAction(), Platform.HD4DP);
		throw exception;
	}
}
