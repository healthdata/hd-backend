/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.gathering.factory.IMappingContextFactory;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.dao.IFastTrackRecordDao;
import be.wiv_isp.healthdata.gathering.dao.IFastTrackUploadDao;
import be.wiv_isp.healthdata.gathering.domain.FastTrackRecord;
import be.wiv_isp.healthdata.gathering.domain.FastTrackUpload;
import be.wiv_isp.healthdata.gathering.domain.search.FastTrackRecordSearch;
import be.wiv_isp.healthdata.gathering.dto.FastTrackUploadStatusDto;
import be.wiv_isp.healthdata.gathering.preloading.AppConfig;
import be.wiv_isp.healthdata.gathering.preloading.FastTrackWorkerThread;
import be.wiv_isp.healthdata.gathering.service.IFastTrackService;
import be.wiv_isp.healthdata.gathering.service.IRegistrationWorkflowService;
import be.wiv_isp.healthdata.gathering.tasks.FastTrackUploadTask;
import be.wiv_isp.healthdata.orchestration.action.workflow.executor.IRegistrationWorkflowActionExecutor;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.factory.IActionFactory;
import be.wiv_isp.healthdata.orchestration.mapping.IMappingContext;
import be.wiv_isp.healthdata.orchestration.service.*;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

@Service
public class FastTrackService implements IFastTrackService {

    private static final Logger LOG = LoggerFactory.getLogger(FastTrackService.class);

    @Autowired
    private IActionFactory actionFactory;
    @Autowired
    private IRegistrationWorkflowService workflowService;
    @Autowired
    private INoteMigrationService noteMigrationService;
    @Autowired
    private INoteService noteService;
    @Autowired
    private IDataCollectionDefinitionForwardService catalogueService;
    @Autowired
    private IRegistrationWorkflowActionExecutor workflowActionExecutor;
    @Autowired
    private IMappingContextFactory mappingContextFactory;
    @Autowired
    private IMappingService mappingService;
    @Autowired
    private IOrganizationService organizationService;
    @Autowired
    private IFastTrackUploadDao fastTrackUploadDao;
    @Autowired
    private IFastTrackRecordDao fastTrackRecordDao;
    @Autowired
    private TaskScheduler taskScheduler;
    @Autowired
    private IAutowireService autowireService;


    @Override
    public Response status() {
        final HDUserDetails principal = AuthenticationService.getUserDetails();
        FastTrackUpload upload = fastTrackUploadDao.getLast(principal.getUsername(), principal.getOrganization().getId());
        if(upload == null) {
            return Response.noContent().build();
        }
        FastTrackUploadStatusDto dto = new FastTrackUploadStatusDto(upload);

        FastTrackRecordSearch search = new FastTrackRecordSearch();
        search.setUploadId(upload.getId());
        List<FastTrackRecord> records = fastTrackRecordDao.getAll(search);

        dto.setRecords(records);

        return Response.ok(dto).build();
    }

    public Response scheduleUpload(FastTrackUpload upload) {
        FastTrackUpload lastRequest = fastTrackUploadDao.getLast(upload.getUsername(), upload.getOrganizationId());
        if(lastRequest.getStoppedOn() == null) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_FORBIDDEN, "upload in progress");
            throw exception;
        }

        FastTrackUploadTask task = new FastTrackUploadTask();
        task.setUpload(upload);
        autowireService.autowire(task);
        taskScheduler.schedule(task, new Date());

        FastTrackUploadStatusDto dto = new FastTrackUploadStatusDto(upload);

        return Response.ok(dto).build();
    }

    @Override
    public void upload(FastTrackUpload upload) {
        try {
            Organization organization = organizationService.get(upload.getOrganizationId());
            DataCollectionDefinitionDtoV7 dataCollectionDefinition = catalogueService.get(upload.getDataCollectionDefinitionId(), organization);
            if (dataCollectionDefinition == null) {
                HealthDataException exception = new HealthDataException();
                exception.setExceptionType(ExceptionType.INVALID_DATE_FOR_DATA_COLLECTION_DEFINITION, upload.getDataCollectionDefinitionId(), "creation");
                throw exception;
            }
            final byte[] dcdContent = dataCollectionDefinition.getContent();

            final Reader reader = new StringReader(upload.getContent());
            final CSVFormat csvFormat = CSVFormat.DEFAULT.withDelimiter(';') //
                    .withEscape('\\') //
                    .withQuoteMode(QuoteMode.MINIMAL) //
                    .withHeader();

            CSVParser parser;
            try {
                parser = new CSVParser(reader, csvFormat);
            } catch (Exception e) {
                LOG.error("Could not parse csv content", e);
                upload.setErrors(e.getMessage());
                upload.setStatus(FastTrackUpload.Status.UPLOAD_FAILED);
                try {
                    reader.close();
                } catch (IOException e1) {
                    LOG.error("Couldn't close reader", e);
                }
                return;
            }

            final List<String> keys = new ArrayList<>(parser.getHeaderMap().keySet());
            final String[] keysAsArray = keys.toArray(new String[keys.size()]);

            ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
            ThreadPoolTaskExecutor taskExecutor = (ThreadPoolTaskExecutor) context.getBean("taskExecutor");
            IMappingContext mappingContext = mappingContextFactory.create(organization);

            final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();


            //create a list to hold the Future object associated with Callable
            List<Future<FastTrackRecord>> list = new ArrayList<>();
            LOG.debug("Start FastTrack upload @ [{}]", getCurrentTimeStamp());
            List<CSVRecord> csvrRecords = parser.getRecords();
            upload.setTotalCount((long) csvrRecords.size());
            for (CSVRecord record : csvrRecords) {
                Callable<FastTrackRecord> worker = new FastTrackWorkerThread(
                        mappingService,
                        actionFactory,
                        workflowActionExecutor,
                        workflowService,
                        noteMigrationService,
                        noteService,
                        fastTrackRecordDao,
                        dataCollectionDefinition,
                        upload,
                        csvFormat,
                        keysAsArray,
                        record,
                        dcdContent,
                        organization,
                        mappingContext,
                        authentication);
                Future<FastTrackRecord> future = taskExecutor.submit(worker);
                list.add(future);
            }

            Map<Long, FastTrackRecord> records = new HashMap<>();
            for (Future<FastTrackRecord> fut : list) {
                final FastTrackRecord result = fut.get();
                records.put(result.getLineNumber(), result);
            }
            upload.setRecords(records);
            //shut down the executor service now
            taskExecutor.shutdown();
            parser.close();
            reader.close();
            LOG.debug("Stop CSV upload @ [{}]", getCurrentTimeStamp());
            upload.setStatus(FastTrackUpload.Status.UPLOAD_SUCCESS);
        } catch (Exception e) {
            LOG.error("Could not upload csv content", e);
            upload.setErrors(e.getMessage());
            upload.setStatus(FastTrackUpload.Status.UPLOAD_FAILED);
        }
    }

    @Override
    @Transactional
    public void createUpload(FastTrackUpload upload) {
        fastTrackUploadDao.create(upload);
    }

    @Override
    @Transactional
    public void updateUpload(FastTrackUpload upload) {
        fastTrackUploadDao.update(upload);
    }


    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        return sdfDate.format(now);
    }
}
