/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.factory.impl;

import be.wiv_isp.healthdata.gathering.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.factory.IMessageFactory;
import be.wiv_isp.healthdata.orchestration.action.standalone.StandAloneAction;
import be.wiv_isp.healthdata.orchestration.action.workflow.WorkflowAction;
import be.wiv_isp.healthdata.orchestration.util.constant.EHealthCodageConstants;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.StandAloneActionType;
import be.wiv_isp.healthdata.orchestration.dto.EncryptionModuleMessage;
import be.wiv_isp.healthdata.orchestration.factory.impl.AbstractMessageFactory;
import be.wiv_isp.healthdata.orchestration.jaxb.json.JsonMarshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class MessageFactory extends AbstractMessageFactory<RegistrationWorkflow, ParticipationWorkflow> implements IMessageFactory {

	private static final Logger LOG = LoggerFactory.getLogger(MessageFactory.class);

	public static final String NO_PATIENT_ID_DEFINED = "NO_PATIENT_ID_DEFINED";

	@Override
	public Message createOutgoingMessage(WorkflowAction action, RegistrationWorkflow workflow) {
		LOG.debug("Registration[{}]: creating outbox message for action {}", workflow.getId(), action);

		CSVContent csvContent = new CSVContent();
		if (workflow.getDocument().getPrivateContent() == null) { // old way
			final String patientId = workflow.getMetaData().get(RegistrationWorkflow.MetaData.PATIENT_IDENTIFIER);
			csvContent.putPatientId( (patientId != null) ? patientId : NO_PATIENT_ID_DEFINED );
			csvContent.putWorkflowId(String.valueOf(workflow.getId()));
		}
		else {
			csvContent.putWorkflowId(String.valueOf(workflow.getId()));
			for (Map.Entry<String, TypedValue> codedEntry : workflow.getDocument().getCodedContent().entrySet()) {
				csvContent.putField(codedEntry.getKey(), codedEntry.getValue().getValue(), codedEntry.getValue().getType());
			}
			if (workflow.getDocument().getCodedContent().isEmpty()) {
				csvContent.addDummyField(); // csv must contain at least 3 fields
			}
		}
		csvContent.setLastField(JsonMarshaller.marshalToByteArray(action));

		return createMessage(csvContent, workflow.getOrganization(), MessageType.WORKFLOW_ACTION, workflow.getId());
	}

	@Override
	public Message createOutgoingMessage(WorkflowAction action, ParticipationWorkflow workflow) {
		LOG.debug("Participation[{}]: creating outbox message for action {}", workflow.getId(), action);
		final CSVContent csvContent = new CSVContent();
		csvContent.putPatientId(NO_PATIENT_ID_DEFINED);
		csvContent.putWorkflowId(String.valueOf(workflow.getId()));
		csvContent.setLastField(JsonMarshaller.marshalToByteArray(action));

		return createMessage(csvContent, workflow.getOrganization(), MessageType.PARTICIPATION_ACTION, workflow.getId());
	}

	@Override
	public Message createOutgoingMessage(StandAloneAction action, Organization organization) {
		LOG.debug("Creating outbox message for action {}", action);
		final CSVContent csvContent = new CSVContent();
		csvContent.addDummyField(action.getAction().toString());
		csvContent.addDummyField(action.getAction().toString());
		csvContent.setLastField(JsonMarshaller.marshalToByteArray(action));

		if (StandAloneActionType.STATUS_MESSAGE_ACTION.equals(action.getAction()))
			return createMessage(csvContent, organization, MessageType.ACTION, null, 1);
		else
			return createMessage(csvContent, organization, MessageType.ACTION, null);
	}

	@Override
	protected EncryptionModuleMessage createEncryptionModuleMessage(Message message, CSVContent csvContent) {
		final EncryptionModuleMessage emMessage = new EncryptionModuleMessage();
		emMessage.setSubject(EHealthCodageConstants.ENCODE_SUBJECT);
		emMessage.setAddressee(identificationService.getEHealthCodageIdentification());
		emMessage.setEncryptionAddressee(identificationService.getHd4ResIdentification());
		emMessage.setContent(csvContent.toString());
		emMessage.addMetadata(Message.Metadata.TTP_PROJECT, configurationService.get(ConfigurationKey.EHEALTH_CODAGE_TTP_PROJECT).getValue());
		return emMessage;
	}

	private Message createMessage(CSVContent csvContent, Organization organization, MessageType messageType, Long workflowId) {
		return createMessage(csvContent, organization, messageType, workflowId, MAX_TRIALS);
	}

	private Message createMessage(CSVContent csvContent, Organization organization, MessageType messageType, Long workflowId, Integer sendingTrials) {
		final Identification encryptionAddressee = identificationService.getHd4ResIdentification();
		final MessageCorrespondent correspondent = createMessageCorrespondent(encryptionAddressee);

		final Message message = new Message();
		message.setStatus(MessageStatus.OUTBOX);
		message.setCorrespondent(correspondent);
		message.setContent(csvContent.getBytes());
		message.addMetadata(Message.Metadata.TTP_PROJECT, configurationService.get(ConfigurationKey.EHEALTH_CODAGE_TTP_PROJECT).getValue());
		message.setType(messageType);
		message.setOrganization(organization);
		message.setWorkflowId(workflowId);
		message.setRemainingSendingTrials(sendingTrials);

		LOG.debug("Message created: {}", message);
		return message;
	}
}
