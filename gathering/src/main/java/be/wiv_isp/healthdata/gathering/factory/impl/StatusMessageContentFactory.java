/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.factory.impl;

import be.wiv_isp.healthdata.gathering.factory.IStatusMessageContentFactory;
import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.gathering.domain.StatusMessage;
import be.wiv_isp.healthdata.orchestration.action.StatusMessageContent;
import be.wiv_isp.healthdata.orchestration.domain.search.StatusMessageSearch;
import be.wiv_isp.healthdata.orchestration.service.*;
import org.apache.commons.collections.CollectionUtils;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.List;

@Component
public class StatusMessageContentFactory implements IStatusMessageContentFactory {

    private static final Logger LOG = LoggerFactory.getLogger(StatusMessageContentFactory.class);

    @Autowired
    private IOrganizationService organizationService;

    @Autowired
    private IStatusMessageService<StatusMessage> statusMessageService;


    @Override
    public StatusMessageContent createContent(Organization organization) {
        LOG.debug("Creating status message content");

        final StatusMessageContent content = new StatusMessageContent();

        // organization independent
        content.set(StatusMessageContent.IS_MAIN_ORGANIZATION, organization.isMain());
        content.set(StatusMessageContent.MAIN_ORGANIZATION, organization.isMain() ? null : createMainOrganizationJsonObject());
        // organization dependent
        content.set(StatusMessageContent.LAST_HD4RES_STATUS_MESSAGE_RECEIVED, createLastHd4resStatusMessageReceived(organization));

        LOG.debug(MessageFormat.format("Status message content: {0}", content));

        return content;
    }

    private JSONObject createMainOrganizationJsonObject() {
        final Organization mainOrganization = organizationService.getMain();

        final StatusMessageContent content = new StatusMessageContent();
        content.set(StatusMessageContent.HD_IDENTIFICATION_NAME, mainOrganization.getName());
        content.set(StatusMessageContent.HD_IDENTIFICATION_TYPE, mainOrganization.getHealthDataIDType());
        content.set(StatusMessageContent.HD_IDENTIFICATION_VALUE, mainOrganization.getHealthDataIDValue());

        return content;
    }

    private String createLastHd4resStatusMessageReceived(Organization organization) {
        final StatusMessageSearch search = new StatusMessageSearch();
        search.setLastOnly(true);
        search.setHealthDataIdentification(new HealthDataIdentification(organization));

        final List<StatusMessage> statusMessages = statusMessageService.getAll(search);

        if(CollectionUtils.isEmpty(statusMessages)) {
            return null;
        }

        return new SimpleDateFormat(DateFormat.DATE_AND_TIME.getPattern()).format(statusMessages.get(0).getCreatedOn());
    }
}
