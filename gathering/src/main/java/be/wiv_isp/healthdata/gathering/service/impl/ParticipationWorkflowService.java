/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupDto;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.action.workflow.StartParticipationAction;
import be.wiv_isp.healthdata.gathering.domain.ParticipationDocument;
import be.wiv_isp.healthdata.gathering.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.search.ParticipationWorkflowSearch;
import be.wiv_isp.healthdata.gathering.service.IParticipationWorkflowService;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.factory.IAbstractMessageFactory;
import be.wiv_isp.healthdata.orchestration.service.IIdentificationService;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractParticipationWorkflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ParticipationWorkflowService extends AbstractParticipationWorkflowService<ParticipationWorkflow, ParticipationWorkflowSearch, ParticipationDocument> implements IParticipationWorkflowService {

	@Autowired
	private IIdentificationService identificationService;
	@Autowired
	private IMessageService messageService;
	@Autowired
	private IAbstractMessageFactory<RegistrationWorkflow, ParticipationWorkflow> messageFactory;

	public ParticipationWorkflowService() {
		super(ParticipationWorkflow.class);
	}

	@Override
	public ParticipationWorkflow getEntityInstance() {
		return new ParticipationWorkflow();
	}

	@Override
	public ParticipationWorkflowSearch getSearchInstance() {
		return new ParticipationWorkflowSearch();
	}

	@Override
	protected Platform getPlatform() {
		return Platform.HD4DP;
	}

	@Override
	public void sendStart(ParticipationWorkflow workflow, DataCollectionGroupDto dataCollectionGroup) {
		if (!dataCollectionGroup.isValidForSubmission(new Date())) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_DATE_FOR_DATA_COLLECTION_DEFINITION, dataCollectionGroup.getId(), "submit");
			throw exception;
		}
		final StartParticipationAction action = new StartParticipationAction();
		action.setDataCollectionDefinitionId(workflow.getDataCollectionDefinitionId());
		action.setIdentificationWorkflow(identificationService.getHealthDataIdentification(workflow.getOrganization()));
		action.setStartedOn(workflow.getCreatedOn());

		final Message message = messageFactory.createOutgoingMessage(action, workflow);
		messageService.create(message);
	}
}