/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.common.domain.enumeration.TemplateKey;
import be.wiv_isp.healthdata.gathering.domain.UpdatedUser;
import be.wiv_isp.healthdata.gathering.service.IUpdatedUserService;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.mail.UserAccountContext;
import be.wiv_isp.healthdata.orchestration.service.IMailService;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractUserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service(value = "userService")
public class UserService extends AbstractUserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private IMailService mailService;
    @Autowired
    private IUpdatedUserService updatedUserService;

    @Override
    @Transactional
    public User create(User user) {
        return create(user, true);
    }

    @Override
    @Transactional
    public User create(User user, boolean notifyUser) {
        String password = user.getPassword();
        final User created = super.create(user, notifyUser);

        if(notifyUser) {
            notifyUser(user, password);
        }

        // can be removed in Q2
        final UpdatedUser updatedUser = new UpdatedUser();
        updatedUser.setUserID(created.getId());
        updatedUser.setOrganization(user.getOrganization());
        updatedUserService.add(updatedUser);

        return created;
    }

    private void notifyUser(User user, String password) {
        LOG.debug("Notifying user of new account creation");
        if(StringUtils.isBlank(user.getEmail())) {
            LOG.warn("Could not notify user because it has no email address");
        }
        try {
            final UserAccountContext context = new UserAccountContext();
            context.setInstallationUrl(configurationService.get(ConfigurationKey.GUI_HOST).getValue());
            context.setUsername(user.getUsername());
            context.setPassword(password);
            context.setDataCollections(user.getDataCollectionNames());
            if(StringUtils.isNotBlank(user.getLastName())) {
                context.setLastName(user.getLastName());
            }
            if(StringUtils.isNotBlank(user.getFirstName())) {
                context.setFirstName(user.getFirstName());
            }

            mailService.createAndSendMail(TemplateKey.NEW_USER_ACCOUNT, context, user.getEmail());
        } catch (Exception e) {
            LOG.error("Could not notify user", e);
        }
    }

    @Override
    @Transactional
    public User update(User user, boolean passwordEncodingNeeded) {
        final User updated = super.update(user, passwordEncodingNeeded);

        // can be removed in Q2
        final UpdatedUser updatedUser = new UpdatedUser();
        updatedUser.setUserID(updated.getId());
        updatedUser.setOrganization(user.getOrganization());
        updatedUserService.add(updatedUser);

        return updated;
    }

    @Override
    @Transactional
    public void delete(User user) {
        final Long userId = user.getId();
        super.delete(user);

        // can be removed in Q2
        final UpdatedUser updatedUser = new UpdatedUser();
        updatedUser.setUserID(userId);
        updatedUser.setOrganization(user.getOrganization());
        updatedUserService.add(updatedUser);
    }
}
