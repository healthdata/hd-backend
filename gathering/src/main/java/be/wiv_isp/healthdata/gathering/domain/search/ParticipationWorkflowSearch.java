/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.domain.search;

import be.wiv_isp.healthdata.orchestration.domain.search.AbstractParticipationWorkflowSearch;

import java.util.Objects;

public class ParticipationWorkflowSearch extends AbstractParticipationWorkflowSearch{

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof ParticipationWorkflowSearch) {
			ParticipationWorkflowSearch other = (ParticipationWorkflowSearch) o;

			return Objects.equals(statuses, other.statuses)
					&& Objects.equals(dataCollectionName, other.dataCollectionName)
					&& Objects.equals(returnDeleted, other.returnDeleted)
					&& Objects.equals(notUpdatedAfterDate, other.notUpdatedAfterDate)
					&& Objects.equals(withDocument, other.withDocument)
					&& Objects.equals(organization, other.organization)
					&& Objects.equals(workflowIds, other.workflowIds)
					&& Objects.equals(notWorkflowId, other.dataCollectionDefinitionId)
					&& Objects.equals(dataCollectionDefinitionId, other.dataCollectionDefinitionId);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				this.statuses,
				this.dataCollectionName,
				this.returnDeleted,
				this.notUpdatedAfterDate,
				this.withDocument,
				this.organization,
				this.workflowIds,
				this.notWorkflowId,
				this.dataCollectionDefinitionId);
	}

	@Override
	public String toString() {
		return "ParticipationWorkflowSearch {" +
				"statuses = " + Objects.toString(this.statuses) + ", " +
				"dataCollectionName = " + Objects.toString(this.dataCollectionName) + ", " +
				"returnDeleted = " + Objects.toString(this.returnDeleted) + ", " +
				"notUpdatedAfterDate = " + Objects.toString(this.notUpdatedAfterDate) + ", " +
				"withDocument = " + Objects.toString(this.withDocument) + ", " +
				"organization = " + Objects.toString(this.organization) + ", " +
				"workflowIds = " + Objects.toString(this.workflowIds) + ", " +
				"notWorkflowId = " + Objects.toString(this.notWorkflowId) + ", " +
				"dataCollectionDefinitionId = " + Objects.toString(this.dataCollectionDefinitionId) + "}";
	}
}
