/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package be.wiv_isp.healthdata.gathering.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="options")
public class ApiOptionsDto {
    @XmlElement
    private Boolean saveOnValidationError;
    @XmlElement
    private Boolean autoSubmit;
    @XmlElement
    private Boolean dryRun;
    @XmlElement
    private String mode;
    @XmlElement
    private String master;

    public ApiOptionsDto() {
        this.saveOnValidationError = true;
        this.autoSubmit = true;
        this.dryRun = false;
        this.mode = MODE.MERGE;
        this.master = MASTER.DATABASE;
    }

    public Boolean isSaveOnValidationError() {
        return saveOnValidationError;
    }

    public void setSaveOnValidationError(Boolean saveOnValidationError) {
        this.saveOnValidationError = saveOnValidationError;
    }

    public Boolean isAutoSubmit() {
        return autoSubmit;
    }

    public void setAutoSubmit(Boolean autoSubmit) {
        this.autoSubmit = autoSubmit;
    }

    public Boolean isDryRun() {
        return dryRun;
    }

    public void setDryRun(Boolean dryRun) {
        this.dryRun = dryRun;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getMaster() {
        return master;
    }

    public void setMaster(String master) {
        this.master = master;
    }

    public class MODE {
        public static final String MERGE = "merge";
        public static final String OVERWRITE = "overwrite";
    }

    public class MASTER {
        public static final String API = "api";
        public static final String DATABASE = "database";
    }
}
