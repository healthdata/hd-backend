/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.gathering.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.gathering.dao.IExceptionReportDao;
import be.wiv_isp.healthdata.gathering.domain.ExceptionReport;
import be.wiv_isp.healthdata.gathering.domain.search.ExceptionReportSearch;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.SetJoin;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ExceptionReportDao extends CrudDaoV2<ExceptionReport, Long, ExceptionReportSearch> implements IExceptionReportDao {

    public ExceptionReportDao() {
        super(ExceptionReport.class);
    }

    @Override
    public List<ExceptionReport> getAll() {
        return getAll(null);
    }

    @Override
    protected List<Predicate> getPredicates(ExceptionReportSearch search, CriteriaBuilder cb, Root<ExceptionReport> rootEntry) {
        final List<Predicate> predicates = new ArrayList<>();

        if(StringUtils.isNotBlank(search.getHash())) {
            predicates.add(cb.equal(rootEntry.get("hash"), search.getHash()));
        }
        if(StringUtils.isNotBlank(search.getUuid())) {
            final SetJoin<Object, Object> join = rootEntry.joinSet("uuids");
            predicates.add(cb.equal(join, search.getUuid()));
        }
        if(search.getReported() != null) {
             if(search.getReported()) {
                 predicates.add(cb.isNotNull(rootEntry.get("reportedOn")));
             } else {
                 predicates.add(cb.isNull(rootEntry.get("reportedOn")));
             }
        }
        return predicates;
    }
}
