/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.gathering.tasks;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationWorkflowSearch;
import be.wiv_isp.healthdata.gathering.service.IWorkflowsToMigrateCommentsService;
import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionDefinitionForwardService;
import be.wiv_isp.healthdata.orchestration.tasks.AbstractNoteMigrationTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


@Component
public class NoteMigrationTask extends AbstractNoteMigrationTask {

    private static final Logger LOG = LoggerFactory.getLogger(NoteMigrationTask.class);

    @Autowired
    private IWorkflowsToMigrateCommentsService workflowsToMigrateCommentsService;
    @Autowired
    private IDataCollectionDefinitionForwardService catalogueService;

    @Override
    protected void execute() {
        LOG.debug("Creating list of workflows with non-migrated comments.");
        // lazy migration
        RegistrationWorkflowSearch search = new RegistrationWorkflowSearch();
        search.setReturnDeleted(false);
        List<AbstractRegistrationWorkflow> workflows = workflowService.getAll(search);
        for (AbstractRegistrationWorkflow workflow : workflows) {
            workflowsToMigrateCommentsService.create(workflow.getId());
        }

        // active migration for open registries
        List<Long> dcdIds = workflowService.getDataCollectionDefinitionIds();
        for (Long dcdId : dcdIds) {
            DataCollectionDefinitionDtoV7 dcd = catalogueService.get(dcdId);
            search.setDataCollectionDefinitionId(dcdId);
            workflows = workflowService.getAll(search);
            if (dcd.getDataCollectionGroup().isValidForComments(new Date())) {
                for (AbstractRegistrationWorkflow workflow : workflows)
                    noteMigrationService.migrateCommentsToNotes(workflow, dcd.getContent());
            }
        }
    }

}
