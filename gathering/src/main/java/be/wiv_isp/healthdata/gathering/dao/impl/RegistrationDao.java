/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDao;
import be.wiv_isp.healthdata.gathering.dao.IRegistrationDao;
import be.wiv_isp.healthdata.gathering.domain.Registration;
import be.wiv_isp.healthdata.gathering.domain.search.RegistrationSearch;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RegistrationDao extends CrudDao<Registration, Long> implements IRegistrationDao {

	public RegistrationDao() {
		super(Registration.class);
	}

	@Override
	public List<Registration> getAll(RegistrationSearch search) {
		// TODO Auto-generated method stub
		return null;
	}
}
