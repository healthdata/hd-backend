/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.gathering.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.gathering.dao.IFastTrackUploadDao;
import be.wiv_isp.healthdata.gathering.domain.FastTrackUpload;
import be.wiv_isp.healthdata.gathering.domain.search.FastTrackUploadSearch;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class FastTrackUploadDao extends CrudDaoV2<FastTrackUpload, Long, FastTrackUploadSearch> implements IFastTrackUploadDao{

    public FastTrackUploadDao() {
        super(FastTrackUpload.class);
    }

    @Override
    protected List<Predicate> getPredicates(FastTrackUploadSearch search, CriteriaBuilder cb, Root<FastTrackUpload> rootEntry) {
        final List<Predicate> predicates = new ArrayList<>();

        if(StringUtils.isNotBlank(search.getFileName())) {
            predicates.add(cb.equal(rootEntry.get("fileName"), search.getFileName()));
        }

        return predicates;
    }

    @Override
    public FastTrackUpload getLast(String username, Long organizationId) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<FastTrackUpload> cq = cb.createQuery(FastTrackUpload.class);
        Root<FastTrackUpload> rootEntry = cq.from(FastTrackUpload.class);
        CriteriaQuery<FastTrackUpload> all = cq.select(rootEntry);

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(rootEntry.get("username"), username));
        predicates.add(cb.equal(rootEntry.get("organizationId"), organizationId));
        cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));

        cq.orderBy(cb.desc(rootEntry.get("createdOn")));

        TypedQuery<FastTrackUpload> query = em.createQuery(all);
        query.setMaxResults(1);
        List<FastTrackUpload> resultList = query.getResultList();
        if (resultList.isEmpty()) {
            return null;
        } else {
            return resultList.get(0);
        }
    }
}
