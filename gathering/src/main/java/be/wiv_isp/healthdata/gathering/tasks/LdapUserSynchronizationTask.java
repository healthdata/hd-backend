/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.gathering.tasks;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.UserManagementType;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import be.wiv_isp.healthdata.orchestration.security.ldap.service.ILdapService;
import be.wiv_isp.healthdata.orchestration.service.IUserService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import be.wiv_isp.healthdata.orchestration.service.IUserManagementService;
import be.wiv_isp.healthdata.orchestration.tasks.HealthDataTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.List;

@Component
public class LdapUserSynchronizationTask extends HealthDataTask {

    private static final Logger LOG = LoggerFactory.getLogger(LdapUserSynchronizationTask.class);

    @Autowired
    private IOrganizationService organizationService;

    @Autowired
    private IUserService userService;

    @Autowired
    private ILdapService ldapService;

    @Autowired
    private IUserManagementService userManagementService;

    @Override
    public String getName() {
        return "LDAP_USER_SYNCHRONIZATION";
    }

    @Override
    protected void execute() {
        final List<Organization> organizations = organizationService.getAll();
        for (final Organization organization : organizations) {
            final UserManagementType userManagementType = userManagementService.getUserManagementType(organization);
            if(UserManagementType.DATABASE_LDAP.equals(userManagementType)) {
                LOG.info("Synchronizing users for organization {}", organization);
                final UserSearch search = new UserSearch();
                search.setLdapUser(true);
                search.setOrganization(organization);
                search.setIncompleteUserInfo(true);

                int count = 0;
                final List<User> users = userService.getAll(search);
                LOG.info("{} users found with missing info", users.size());
                for (final User user : users) {
                    LOG.info("synchronizing user [{}]", user.getUsername());
                    try {
                        final boolean modified = ldapService.fillUserInfo(user);
                        if (modified) {
                            count++;
                            userService.update(user);
                        } else {
                            LOG.warn("No extra user info could be retrieved from the LDAP. This might be due to an incomplete or wrong LDAP configuration. Skipping user synchronization for organization {}", organization);
                            break;
                        }
                    } catch (Exception e) {
                        LOG.error(MessageFormat.format("Error while synchronizing user [{0}]", user.getUsername()), e);
                    }
                }
                LOG.info("{} users synchronized for organization {}", count, organization);
            }
        }
    }
}
