/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.tasks;

import be.wiv_isp.healthdata.gathering.service.IExceptionService;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.mapper.BooleanConfigurationMapper;
import be.wiv_isp.healthdata.orchestration.tasks.AbstractDailyTask;
import be.wiv_isp.healthdata.orchestration.tasks.DataCollectionGroupReplicationTask;
import be.wiv_isp.healthdata.orchestration.tasks.HealthDataTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DailyTask extends AbstractDailyTask {

    @Autowired
    private UpgradeRegistrationTask upgradeRegistrationTask;
    @Autowired
    private DataCollectionGroupReplicationTask dataCollectionGroupReplicationTask;
    @Autowired
    private FollowUpTask followUpTask;
    @Autowired
    private UpdatedUserSendingTask updatedUserSendingTask;
    @Autowired
    private LdapUserSynchronizationTask ldapUserSynchronizationTask;
    @Autowired
    private CentralPlatformTask centralPlatformTask;
    @Autowired
    private ExceptionReportTask exceptionReportTask;
    @Autowired
    private IExceptionService exceptionService;
    @Autowired
    private OrganizationSetupTask organizationSetupTask;
    @Autowired
    private PendingRegistrationsNotificationTask pendingRegistrationsNotificationTask;

    private List<HealthDataTask> tasks;

    @Override
    protected List<HealthDataTask> getTasks() {
        if(tasks == null) {
            tasks = new ArrayList<>();
            if(statusMessagesEnabled()) {
                tasks.add(statusMessageTask);
            }
            tasks.add(exceptionReportTask);
            tasks.add(upgradeRegistrationTask);
            tasks.add(followUpTask);
            tasks.add(dataCollectionGroupReplicationTask);
            tasks.add(updatedUserSendingTask);
            tasks.add(pendingRegistrationsNotificationTask);
            tasks.add(dataCollectionGroupNotificationTask);
            tasks.add(ldapUserSynchronizationTask);
            tasks.add(centralPlatformTask);
            tasks.add(passwordResetTokenCleanUpTask);
            tasks.add(organizationSetupTask);
        }
        return tasks;
    }

    @Override
    protected void handleException(Exception e) {
        exceptionService.report(e.getMessage(), e);
    }

    private Boolean statusMessagesEnabled() {
        return BooleanConfigurationMapper.map(configurationService.get(ConfigurationKey.SCHEDULED_TASK_STATUS_MESSAGE_ENABLED));
    }
}
