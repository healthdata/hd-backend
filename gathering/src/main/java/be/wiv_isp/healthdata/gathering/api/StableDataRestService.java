/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.api;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.annotation.Auditable;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ApiType;
import be.wiv_isp.healthdata.gathering.service.IStableDataService;
import be.wiv_isp.healthdata.orchestration.service.IMappingSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Component
@Path("/stabledata")
public class StableDataRestService {

	@Autowired
	private IStableDataService stableDataService;
	@Autowired
	private IMappingSecurityService securityService;

	@GET
	@PreAuthorize("hasRole('ROLE_USER')")
	@Path("/{patientId}/{dataCollectionName}")
	@Auditable(apiType = ApiType.STABLE_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context UriInfo info) {
		return stableDataService.get(info);
	}

	@GET
	@Path("mapping/{patientId}/{dataCollectionName}")
	@Auditable(apiType = ApiType.STABLE_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getForMapping(@Context UriInfo info, @HeaderParam("Authorization") String bearerToken) {
		if(!securityService.hasAccess(bearerToken)) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GENERAL_UNAUTHORIZED);
			throw exception;
		}
		return stableDataService.get(info);
	}
}
