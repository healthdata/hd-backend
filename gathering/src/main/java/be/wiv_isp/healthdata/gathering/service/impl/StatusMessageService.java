/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.gathering.action.standalone.StatusMessageAction;
import be.wiv_isp.healthdata.gathering.domain.StatusMessage;
import be.wiv_isp.healthdata.gathering.factory.IMessageFactory;
import be.wiv_isp.healthdata.gathering.factory.IStatusMessageContentFactory;
import be.wiv_isp.healthdata.orchestration.dao.IStatusMessageDao;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.search.StatusMessageSearch;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import be.wiv_isp.healthdata.orchestration.service.IStatusMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.List;

@Service
public class StatusMessageService extends AbstractService<StatusMessage, Long, StatusMessageSearch, IStatusMessageDao<StatusMessage>> implements IStatusMessageService<StatusMessage> {

    private static final Logger LOG = LoggerFactory.getLogger(StatusMessageService.class);

    @Autowired
    protected IStatusMessageDao<StatusMessage> dao;

    @Autowired
    private IOrganizationService organizationService;
    @Autowired
    private IMessageService messageService;
    @Autowired
    private IMessageFactory messageFactory;
    @Autowired
    private IStatusMessageContentFactory statusMessageContentFactory;

    public StatusMessageService() {
        super(StatusMessage.class);
    }

    @Override
    public IStatusMessageDao<StatusMessage> getDao() {
        return dao;
    }

    @Override
    public void send() {
        LOG.info("Sending status messages");
        final List<Organization> organizations = organizationService.getAll();

        for(Organization organization : organizations) {
            LOG.debug(MessageFormat.format("Creating status message for organization [{0}]", organization));

            final StatusMessageAction action = new StatusMessageAction();
            action.setContent(statusMessageContentFactory.createContent(organization));
            action.setHealthDataIdentification(new HealthDataIdentification(organization));

            final Message outboxMessage = messageFactory.createOutgoingMessage(action, organization);
            messageService.create(outboxMessage);
        }
    }
}
