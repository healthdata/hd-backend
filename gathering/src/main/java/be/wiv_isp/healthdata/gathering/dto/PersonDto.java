/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.dto;

import be.wiv_isp.healthdata.common.json.deserializer.DateDeserializerWithoutTimestamp;
import be.wiv_isp.healthdata.common.json.serializer.DateSerializerWithoutTimestamp;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.IOException;
import java.util.Date;
import java.util.Objects;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PersonDto {

	public class Gender {
		public static final String MALE = "MALE";
		public static final String FEMALE = "FEMALE";
		public static final String UNKNOWN = "UNKNOWN";
	}

	@JsonProperty(value = "inss")
	private String ssin;

	@JsonProperty(value = "internalId")
	private String internalId;

	@JsonProperty(value = "lastName")
	private String lastName;

	@JsonProperty(value = "firstName")
	private String firstName;

	@JsonSerialize(using = DateSerializerWithoutTimestamp.class, include = JsonSerialize.Inclusion.NON_NULL)
	@JsonDeserialize(using = DateDeserializerWithoutTimestamp.class)
	@JsonProperty(value = "dateOfBirth")
	private Date dateOfBirth;

	@JsonProperty(value = "gender")
	private String gender;

	@JsonProperty(value = "district")
	private String district;

	@JsonProperty(value = "deceased")
	private Boolean deceased;

	@JsonSerialize(using = DateSerializerWithoutTimestamp.class, include = JsonSerialize.Inclusion.NON_NULL)
	@JsonDeserialize(using = DateDeserializerWithoutTimestamp.class)
	@JsonProperty(value = "dateOfDeath")
	private Date dateOfDeath;

	public PersonDto() {

	}

	public String getSsin() {
		return ssin;
	}

	public void setSsin(String ssin) {
		this.ssin = ssin;
	}

	public String getInternalId() {
		return internalId;
	}

	public void setInternalId(String internalId) {
		this.internalId = internalId;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public Boolean isDeceased() {
		return deceased;
	}

	public void setDeceased(Boolean deceased) {
		this.deceased = deceased;
	}

	public Date getDateOfDeath() {
		return dateOfDeath;
	}

	public void setDateOfDeath(Date dateOfDeath) {
		this.dateOfDeath = dateOfDeath;
	}
	
	@Override
	public String toString() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (IOException e) {
			return super.toString();
		}
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof PersonDto) {
			PersonDto other = (PersonDto) o;

			return Objects.equals(ssin, other.ssin) //
					&& Objects.equals(internalId, other.internalId) //
					&& Objects.equals(lastName, other.lastName) //
					&& Objects.equals(firstName, other.firstName) //
					&& Objects.equals(dateOfBirth, other.dateOfBirth) //
					&& Objects.equals(gender, other.gender) //
					&& Objects.equals(district, other.district) //
					&& Objects.equals(deceased, other.deceased) //
					&& Objects.equals(dateOfDeath, other.dateOfDeath);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(//
				this.ssin, //
				this.internalId, //
				this.lastName, //
				this.firstName, //
				this.dateOfBirth, //
				this.gender, //
				this.district, //
				this.deceased, //
				this.dateOfDeath);
	}
}
