/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.domain;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "STABLE_DATA_UPLOADS")
public class StableDataUpload {

    @Id
    @Column(name = "HD4RES_STABLE_DATA_ID")
    private Long stableDataId;

    @Column(name = "SUCCESS", nullable = false)
    private boolean success;

    @Column(name = "DATA_COLLECTION_NAME", nullable = false)
    private String dataCollectionName;

    @Column(name = "IDENTIFICATION_TYPE", nullable = false)
    private String identificationType;

    @Column(name = "IDENTIFICATION_VALUE", nullable = false)
    private String identificationValue;

    public Long getStableDataId() {
        return stableDataId;
    }

    public void setStableDataId(Long stableDataId) {
        this.stableDataId = stableDataId;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getDataCollectionName() {
        return dataCollectionName;
    }

    public void setDataCollectionName(String dataCollectionName) {
        this.dataCollectionName = dataCollectionName;
    }

    public String getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(String identificationType) {
        this.identificationType = identificationType;
    }

    public String getIdentificationValue() {
        return identificationValue;
    }

    public void setIdentificationValue(String identificationValue) {
        this.identificationValue = identificationValue;
    }

    public HealthDataIdentification getHealthDataIdentification() {
        final HealthDataIdentification identification = new HealthDataIdentification();
        identification.setType(identificationType);
        identification.setValue(identificationValue);
        return identification;
    }
}
