/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.gathering.tasks;

import be.wiv_isp.healthdata.orchestration.util.DateUtils;
import be.wiv_isp.healthdata.common.domain.search.Field;
import be.wiv_isp.healthdata.common.domain.search.Ordering;
import be.wiv_isp.healthdata.gathering.centralplatform.service.ICentralPlatformService;
import be.wiv_isp.healthdata.gathering.domain.Upgrade;
import be.wiv_isp.healthdata.gathering.domain.search.UpgradeSearch;
import be.wiv_isp.healthdata.gathering.service.IUpgradeService;
import be.wiv_isp.healthdata.orchestration.dto.VersionDetailsDto;
import be.wiv_isp.healthdata.orchestration.service.IVersionService;
import be.wiv_isp.healthdata.orchestration.tasks.HealthDataTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UpgradeRegistrationTask extends HealthDataTask {

    private static final Logger LOG = LoggerFactory.getLogger(UpgradeRegistrationTask.class);

    @Autowired
    private IVersionService versionService;

    @Autowired
    private IUpgradeService upgradeService;

    @Autowired
    private ICentralPlatformService centralPlatformService;

    @Override
    public String getName() {
        return "UPGRADE_REGISTRATION";
    }

    @Override
    protected void execute() {
        LOG.debug("Checking whether a new version has been installed");

        final VersionDetailsDto lastRegisteredVersion = getLastRegisteredVersion();
        LOG.debug("Last registered version: {}", lastRegisteredVersion);

        final VersionDetailsDto installedVersion = versionService.getVersionDetails();
        LOG.debug("Installed version: {}", installedVersion);

        if(installedVersion.equals(lastRegisteredVersion)) {
            LOG.debug("No new version detected");
        } else {
            LOG.info("New version detected: {}", installedVersion);
            final Upgrade upgrade = new Upgrade();
            upgrade.setExecutedOn(DateUtils.currentTimestamp());
            upgrade.setVersion(installedVersion.getVersion());
            upgrade.setFrontendCommit(installedVersion.getFrontendCommit());
            upgrade.setBackendCommit(installedVersion.getBackendCommit());
            upgrade.setMappingCommit(installedVersion.getMappingCommit());
            centralPlatformService.registerUpgrade(upgrade);
            upgradeService.create(upgrade);
            LOG.info("New version registered: {}", installedVersion);
        }
    }

    private VersionDetailsDto getLastRegisteredVersion() {
        final UpgradeSearch search = new UpgradeSearch();
        search.setMaxResults(1);
        search.setOrdering(new Ordering(Field.EXECUTED_ON, Ordering.Type.DESC));
        final Upgrade lastRegisteredUpgrade = upgradeService.getUnique(search);

        if(lastRegisteredUpgrade == null) {
            return null;
        }

        final VersionDetailsDto dto = new VersionDetailsDto();
        dto.setVersion(lastRegisteredUpgrade.getVersion());
        dto.setFrontendCommit(lastRegisteredUpgrade.getFrontendCommit());
        dto.setBackendCommit(lastRegisteredUpgrade.getBackendCommit());
        dto.setMappingCommit(lastRegisteredUpgrade.getMappingCommit());
        return dto;
    }
}
