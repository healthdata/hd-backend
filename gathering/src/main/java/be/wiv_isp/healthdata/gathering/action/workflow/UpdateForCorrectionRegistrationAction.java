/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.action.workflow;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.CSVContent;
import be.wiv_isp.healthdata.orchestration.domain.DataSource;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import be.wiv_isp.healthdata.orchestration.service.IAbstractRegistrationWorkflowService;
import be.wiv_isp.healthdata.orchestration.service.INoteService;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UpdateForCorrectionRegistrationAction extends AbstractRegistrationWorkflowAction {

	@Autowired
	protected INoteService noteService;

	@Override
	public RegistrationWorkflow doExecute(RegistrationWorkflow workflow, HDUserDetails userDetails) {
		if(!workflow.getDocument().getVersion().equals(documentVersion)) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.IGNORE_ACTION, MessageFormat.format("executing {0} action on old document version ({1})", getAction(), documentVersion));
			throw exception;
		}
		DocumentData documentData = new DocumentData();
		documentData.setContent(workflow.getDocument().getDocumentContent());
		documentData.setCoded(new HashMap<>(workflow.getDocument().getCodedContent()));
		documentData.setPrivate(workflow.getDocument().getPrivateContent());
		List<DataSource> dataSources = new ArrayList<>(workflow.getDocument().getDataSources());
		((IAbstractRegistrationWorkflowService) getWorkflowService()).setDocumentData(workflow, documentData, dataSources, true);

		workflow.getDocument().setNoteMaxOrder(noteMaxOrder);
		noteService.updateNotesFromMessage(WorkflowType.REGISTRATION, workflow.getDocument().getId(), notesMessage);

		workflow.getFlags().setCorrections(true);
		return workflow;
	}

	@Override
	public void extractInfo(Message message) {
		final CSVContent csvContent = new CSVContent(new String(message.getContent(), StandardCharsets.UTF_8));
		setWorkflowId(Long.valueOf(csvContent.getWorkflowId()));
	}

	@Override
	public WorkflowType getWorkflowType() {
		return WorkflowType.REGISTRATION;
	}

	@Override
	public WorkflowActionType getAction() {
		return WorkflowActionType.UPDATE_FOR_CORRECTION;
	}

	@Override
	@JsonIgnore
	public WorkflowStatus getEndStatus() {
		return WorkflowStatus.ACTION_NEEDED;
	}
}
