/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.api.mapper;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.domain.UserRequest;
import be.wiv_isp.healthdata.orchestration.api.mapper.AbstractUserRequestMapper;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

@Service
public class UserRequestMapper extends AbstractUserRequestMapper<UserRequest> implements IUserRequestMapper{

	@Autowired
	private IOrganizationService organizationService;

	public UserRequest convert(JSONObject json) throws JSONException {
		UserRequest userRequest = super.convert(json);
		for (Iterator<String> iterator = json.keys(); iterator.hasNext();) {
			String key = iterator.next();
			if (!"null".equalsIgnoreCase(json.getString(key))) {
				if ("userName".equalsIgnoreCase(key)) {
						userRequest.setUsername(json.getString(key));
				} else if ("password".equalsIgnoreCase(key)) {
					userRequest.setPassword(json.getString(key));
				} else if ("organizationId".equalsIgnoreCase(key)) {
					long organizationId = json.getLong(key);
					Organization organization = organizationService.get(organizationId);
					if(organization == null) {
						HealthDataException exception = new HealthDataException();
						exception.setExceptionType(ExceptionType.INVALID_USER_REQUEST_PROPERTY, "organizationId");
						throw exception;
					}
					userRequest.setOrganization(organization);
				} else if ("dataCollectionNames".equalsIgnoreCase(key)) {
					JSONArray dataCollectionNamesJSON = json.getJSONArray(key);
					Set<String> dataCollectionNames = new HashSet<>();
					for (int i = 0; i < dataCollectionNamesJSON.length(); i++) {
						dataCollectionNames.add(dataCollectionNamesJSON.getString(i));
					}
					userRequest.setDataCollectionNames(dataCollectionNames);
				}
			}
		}
		return userRequest;
	}

	@Override
	protected UserRequest getInstance() {
		return new UserRequest();
	}
}
