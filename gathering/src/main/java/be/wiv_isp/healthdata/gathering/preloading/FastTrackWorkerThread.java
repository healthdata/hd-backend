/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.gathering.preloading;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.action.workflow.SaveRegistrationAction;
import be.wiv_isp.healthdata.gathering.action.workflow.SubmitRegistrationAction;
import be.wiv_isp.healthdata.gathering.dao.IFastTrackRecordDao;
import be.wiv_isp.healthdata.gathering.domain.FastTrackRecord;
import be.wiv_isp.healthdata.gathering.domain.FastTrackUpload;
import be.wiv_isp.healthdata.gathering.domain.RegistrationDocument;
import be.wiv_isp.healthdata.gathering.domain.RegistrationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.enumeration.FastTrackStatus;
import be.wiv_isp.healthdata.gathering.service.IRegistrationWorkflowService;
import be.wiv_isp.healthdata.orchestration.action.workflow.executor.IRegistrationWorkflowActionExecutor;
import be.wiv_isp.healthdata.orchestration.api.mapper.MappingResponseMapper;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import be.wiv_isp.healthdata.orchestration.dto.ValidationResponse;
import be.wiv_isp.healthdata.orchestration.factory.IActionFactory;
import be.wiv_isp.healthdata.orchestration.mapping.IMappingContext;
import be.wiv_isp.healthdata.orchestration.mapping.MappingResponse;
import be.wiv_isp.healthdata.orchestration.service.IMappingService;
import be.wiv_isp.healthdata.orchestration.service.INoteMigrationService;
import be.wiv_isp.healthdata.orchestration.service.INoteService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;

public class FastTrackWorkerThread implements Callable<FastTrackRecord> {

    private static final Logger LOG = LoggerFactory.getLogger(FastTrackWorkerThread.class);

    private CSVRecord csvRecord;
    private IMappingService mappingService;
    private IActionFactory actionFactory;
    private IRegistrationWorkflowActionExecutor workflowActionExecutor;
    private IRegistrationWorkflowService workflowService;
    private INoteMigrationService noteMigrationService;
    private INoteService noteService;
    private IFastTrackRecordDao fastTrackRecordDao;
    private DataCollectionDefinitionDtoV7 dataCollectionDefinition;
    private CSVFormat csvFormat;
    private byte[] dcdContent;
    private FastTrackUpload upload;
    private String[] keysAsArray;
    private Organization organization;
    private IMappingContext mappingContext;
    private Authentication authentication;

    public FastTrackWorkerThread(IMappingService mappingService,
                                 IActionFactory actionFactory,
                                 IRegistrationWorkflowActionExecutor workflowActionExecutor,
                                 IRegistrationWorkflowService workflowService,
                                 INoteMigrationService noteMigrationService,
                                 INoteService noteService,
                                 IFastTrackRecordDao fastTrackRecordDao,
                                 DataCollectionDefinitionDtoV7 dataCollectionDefinition,
                                 FastTrackUpload upload,
                                 CSVFormat csvFormat,
                                 String[] keysAsArray,
                                 CSVRecord csvRecord,
                                 byte[] dcdContent,
                                 Organization organization,
                                 IMappingContext mappingContext,
                                 Authentication authentication) {
        this.mappingService = mappingService;
        this.actionFactory = actionFactory;
        this.workflowActionExecutor = workflowActionExecutor;
        this.workflowService = workflowService;
        this.noteMigrationService = noteMigrationService;
        this.noteService = noteService;
        this.fastTrackRecordDao = fastTrackRecordDao;
        this.dataCollectionDefinition = dataCollectionDefinition;
        this.upload = upload;
        this.csvFormat = csvFormat;
        this.csvRecord = csvRecord;
        this.dcdContent = dcdContent;
        this.keysAsArray = keysAsArray;
        this.organization = organization;
        this.mappingContext = mappingContext;
        this.authentication = authentication;
    }


    private static AtomicLong idCounter = new AtomicLong();

    public static String createID() {
        return String.valueOf(idCounter.getAndIncrement());
    }

    @Override
    @Transactional
    public FastTrackRecord call() throws Exception {
        MDC.put("logWSReqResp", "false");
        SecurityContextHolder.getContext().setAuthentication(authentication);

        final StringBuilder sb = new StringBuilder();
        final CSVPrinter printer = csvFormat.withHeader(keysAsArray).print(sb);
        printer.printRecord(csvRecord);
        printer.close();

        String recordWithHeaders = sb.toString();

        HDServiceUser user;
        if(StringUtils.isBlank(upload.getUsername())) {
            user = HDServiceUser.create(HDServiceUser.Username.FAST_TRACK, organization);
        } else {
            user = HDServiceUser.create(upload.getUsername(), organization);

        }

        FastTrackRecord record = new FastTrackRecord();
        record.setLineNumber(csvRecord.getRecordNumber());
        record.setHash(DigestUtils.md5Hex(recordWithHeaders.getBytes(StandardCharsets.UTF_8)));

        if(FastTrackUpload.Mode.FAST_TRACK.equals(upload.getMode())) {
            fastTrack(user, recordWithHeaders, record);
        } else {
            upload(user, recordWithHeaders, record);
        }
        fastTrackRecordDao.create(record, upload.getId());
        return record;
    }

    private FastTrackRecord fastTrack(HDServiceUser user, String recordWithHeaders, FastTrackRecord record) {
        try {
            List<FastTrackRecord> hashSearchResult = fastTrackRecordDao.getForHash(dataCollectionDefinition.getId(), record.getHash());
            MappingResponse mappingResponse = mappingService.mapFromCsvToJson(dcdContent, recordWithHeaders, mappingContext);
            final List<Attachment> pendingAttachments = mappingResponse.getPendingAttachments();
            loadAttachments(upload.getPath(), pendingAttachments);
            upload.setAttachmentFiles(getAttachmentFiles(upload.getPath(), pendingAttachments));
            if (CollectionUtils.isNotEmpty(hashSearchResult)) {
                record.setStatus(FastTrackStatus.IGNORE);
                return record;
            }

            final String uniqueID = mappingResponse.getUniqueID();
            if (StringUtils.isNotBlank(uniqueID)) {
                if (!workflowService.isUnique(upload.getDataCollectionDefinitionId(), uniqueID, null, organization)) {
                    record.setStatus(FastTrackStatus.IGNORE);
                    record.setErrors("the uniqueID[" + uniqueID + "] already exists");
                    return record;
                }
            }

            final byte[] documentContent = mappingResponse.getDocumentContent().toString().getBytes(StandardCharsets.UTF_8);
            ValidationResponse validationResponse;
            RegistrationWorkflow tempWorkflow = new RegistrationWorkflow();
            RegistrationDocument tempRegistrationDocument = new RegistrationDocument();
            tempRegistrationDocument.setDocumentContent(documentContent);
            tempRegistrationDocument.setCodedContent(mappingResponse.getToBeCoded());
            if (mappingResponse.getPrivateData() != null) {
                tempRegistrationDocument.setPrivateContent(mappingResponse.getPrivateData().toString().getBytes(StandardCharsets.UTF_8));
            }
            tempWorkflow.setDocument(tempRegistrationDocument);
            tempWorkflow.setMetaData(mappingResponse.getMetaData());

            validationResponse = mappingService.getValidationResponse(tempWorkflow, null, dataCollectionDefinition.getContent());

            if (!validationResponse.isSuccess()) {
                record.setErrors(validationResponse.getResult().toString());
                record.setStatus(FastTrackStatus.ERROR);
                return record;
            }
            SaveRegistrationAction saveAction = (SaveRegistrationAction) actionFactory.getActionByActionType(WorkflowActionType.SAVE, WorkflowType.REGISTRATION);
            saveAction.setDataCollectionDefinitionId(dataCollectionDefinition.getId());
            saveAction.setDocumentContent(documentContent);
            saveAction.setMetaData(mappingResponse.getMetaData());
            DocumentData documentData = new DocumentData();
            documentData.setContent(documentContent);
            documentData.setCoded(mappingResponse.getToBeCoded());
            if (mappingResponse.getPrivateData() != null)
                documentData.setPrivate(mappingResponse.getPrivateData().toString().getBytes(StandardCharsets.UTF_8));
            else
                documentData.setPatientID(mappingResponse.getMetaData());
            saveAction.setDocumentData(documentData);
            saveAction.setToBeCoded(mappingResponse.getToBeCoded());

            saveAction.setUniqueID(mappingResponse.getUniqueID());
            saveAction.setSource(DataSource.FAST_TRACK);
            saveAction.setAffectedFields(MappingResponseMapper.mapDataSources(mappingResponse.getAffectedFields(), DataSource.FAST_TRACK));
            saveAction.setProgress(mappingResponse.getProgress());
            saveAction.setAttachments(mappingResponse.getPendingAttachments());

            AbstractWorkflow actionResult = workflowActionExecutor.execute(saveAction, user);
            SubmitRegistrationAction submitAction = (SubmitRegistrationAction) actionFactory.getActionByActionType(WorkflowActionType.SUBMIT, WorkflowType.REGISTRATION);
            submitAction.setWorkflowId(actionResult.getId());
            actionResult = workflowActionExecutor.execute(submitAction, user);

            record.setWorkflowId(actionResult.getId());
            record.setStatus(FastTrackStatus.SUCCESS);
        } catch (Exception e) {
            record.setStatus(FastTrackStatus.ERROR);
            record.setErrors(e.toString());
        }
        return record;
    }

    private FastTrackRecord upload(HDServiceUser user, String recordWithHeaders, FastTrackRecord record) {
        try {
            MappingResponse mappingResponse = mappingService.mapFromCsvToJson(dcdContent, recordWithHeaders, mappingContext);
            final List<Attachment> pendingAttachments = mappingResponse.getPendingAttachments();
            loadAttachments(upload.getPath(), pendingAttachments);
            upload.setAttachmentFiles(getAttachmentFiles(upload.getPath(), pendingAttachments));

            final String uniqueID = mappingResponse.getUniqueID();
            AbstractWorkflow workflow;
            if (StringUtils.isBlank(uniqueID)) {
                LOG.debug("No unique ID available. Creating new registration.");
                workflow = createWorkflow(dataCollectionDefinition, upload.getFileName(), user, mappingResponse);
            } else {
                LOG.debug("Unique ID is available ({}), checking whether a registration already exists with this unique ID.", mappingResponse);
                if (workflowService.isUnique(upload.getDataCollectionDefinitionId(), uniqueID, null, organization)) {
                    LOG.debug("No existing registration with unique ID [{}]. Creating new registration.", uniqueID);
                    workflow = createWorkflow(dataCollectionDefinition, upload.getFileName(), user, mappingResponse);
                } else {
                    final List<RegistrationWorkflow> workflowsInDB = workflowService.getUnique(upload.getDataCollectionDefinitionId(), mappingResponse.getUniqueID(), organization);
                    if (workflowsInDB.size() != 1) {
                        HealthDataException exception = new HealthDataException();
                        exception.setExceptionType(ExceptionType.NON_UNIQUE_SEARCH_RESULT, "WORKFLOWS", workflowsInDB, "uniqueId");
                        throw exception;
                    }
                    LOG.debug("An existing registration was found with unique ID [{}]. Registration will be merged or overwritten.", uniqueID);

                    final AbstractRegistrationWorkflow workflowInDB = workflowsInDB.get(0);
                    workflow = workflowInDB;
                    if (!WorkflowStatus.IN_PROGRESS.equals(workflowInDB.getStatus())) {
                        HealthDataException exception = new HealthDataException();
                        exception.setExceptionType(ExceptionType.ACTION_NOT_ALLOWED_ON_STATUS, WorkflowActionType.SAVE, workflowInDB.getStatus());
                        throw exception;
                    }
                    if (!noteMigrationService.isMigrated(workflowInDB)) {
                        noteMigrationService.migrateCommentsToNotes(workflowInDB, dcdContent);
                    }
                    // query the mapping api again with the context
                    mappingResponse = mappingService.mapFromCsvToJson(dcdContent, recordWithHeaders, mappingContext);
                    if(!uniqueID.equals(mappingResponse.getUniqueID())) {
                        LOG.warn("Second call to mapping API generated an different unique ID. First call: [{}], second call: [{}]", uniqueID, mappingResponse.getUniqueID());
                    }

                    if (FastTrackUpload.Mode.OVERWRITE.equals(upload.getMode())) {
                        LOG.debug("Overwriting registration with {} version as master", upload.getMaster());
                        if (FastTrackUpload.Master.CSV.equals(upload.getMaster())) {
                            SaveRegistrationAction saveAction = (SaveRegistrationAction) actionFactory.getActionByActionType(WorkflowActionType.SAVE, WorkflowType.REGISTRATION);
                            DocumentData documentData = new DocumentData();
                            documentData.setContent(mappingResponse.getDocumentContent().toString().getBytes(StandardCharsets.UTF_8));
                            documentData.setCoded(mappingResponse.getToBeCoded());
                            documentData.setPatientID(mappingResponse.getPatientID());
                            if (mappingResponse.getPrivateData() != null)
                                documentData.setPrivate(mappingResponse.getPrivateData().toString().getBytes(StandardCharsets.UTF_8));
                            saveAction.setDocumentData(documentData);
                            saveAction.setToBeCoded(mappingResponse.getToBeCoded());
                            saveAction.setMetaData(mappingResponse.getMetaData());
                            saveAction.setUniqueID(mappingResponse.getUniqueID());
                            saveAction.setSource(DataSource.CSV);
                            saveAction.setAffectedFields(MappingResponseMapper.mapDataSources(mappingResponse.getAffectedFields(), DataSource.CSV));
                            saveAction.setFile(upload.getFileName());
                            saveAction.setProgress(mappingResponse.getProgress());
                            saveAction.setAttachments(mappingResponse.getPendingAttachments());
                            saveAction.setDataCollectionDefinitionId(dataCollectionDefinition.getId());
                            AbstractWorkflow result = workflowActionExecutor.execute(saveAction, user, workflowInDB);
                            noteService.deleteAllNotes(WorkflowType.REGISTRATION, result.getDocument().getId());
                        }
                    } else {
                        RegistrationDocument document = new RegistrationDocument();
                        document.setDocumentContent(mappingResponse.getDocumentContent().toString().getBytes(StandardCharsets.UTF_8));
                        RegistrationWorkflow workflowInCsv = new RegistrationWorkflow();
                        workflowInCsv.setDocument(document);
                        workflowInCsv.setMetaData(mappingResponse.getMetaData());
                        workflowInCsv.setDataCollectionDefinitionId(workflowInDB.getDataCollectionDefinitionId());
                        MappingResponse merge;
                        LOG.debug("Merging registrations with {} version as master", upload.getMaster());
                        if (FastTrackUpload.Master.CSV.equals(upload.getMaster())) {
                            merge = mappingService.merge(workflowInCsv, workflowInDB, mappingContext, dataCollectionDefinition.getContent());
                        } else {
                            merge = mappingService.merge(workflowInDB, workflowInCsv, mappingContext, dataCollectionDefinition.getContent());
                        }
                        SaveRegistrationAction saveAction = (SaveRegistrationAction) actionFactory.getActionByActionType(WorkflowActionType.SAVE, WorkflowType.REGISTRATION);
                        saveAction.setMetaData(merge.getMetaData());
                        DocumentData documentData = new DocumentData();
                        documentData.setContent(merge.getDocumentContent().toString().getBytes(StandardCharsets.UTF_8));
                        documentData.setCoded(merge.getToBeCoded());
                        documentData.setPatientID(merge.getPatientID());
                        if (merge.getPrivateData() != null)
                            documentData.setPrivate(merge.getPrivateData().toString().getBytes(StandardCharsets.UTF_8));
                        saveAction.setDocumentData(documentData);
                        saveAction.setToBeCoded(mappingResponse.getToBeCoded());
                        saveAction.setUniqueID(merge.getUniqueID());
                        saveAction.setSource(DataSource.CSV);
                        saveAction.setAffectedFields(MappingResponseMapper.mapDataSources(mappingResponse.getAffectedFields(), DataSource.CSV));
                        saveAction.setFile(upload.getFileName());
                        saveAction.setProgress(mappingResponse.getProgress());
                        saveAction.setAttachments(mappingResponse.getPendingAttachments());
                        saveAction.setDataCollectionDefinitionId(dataCollectionDefinition.getId());

                        workflowActionExecutor.execute(saveAction, user, workflowInDB);
                    }
                }
            }
            record.setWorkflowId(workflow.getId());
            record.setStatus(FastTrackStatus.SUCCESS);
        } catch (Exception e) {
            record.setStatus(FastTrackStatus.ERROR);
            record.setErrors(e.toString());
            LOG.error(e.getMessage(), e);
        }
        return record;

    }

    private AbstractWorkflow createWorkflow(DataCollectionDefinitionDtoV7 dataCollectionDefinition, String fileName, HDUserDetails principal, MappingResponse mappingResponse) {
        SaveRegistrationAction saveAction = (SaveRegistrationAction) actionFactory.getActionByActionType(WorkflowActionType.SAVE, WorkflowType.REGISTRATION);
        saveAction.setDataCollectionDefinitionId(dataCollectionDefinition.getId());
        saveAction.setMetaData(mappingResponse.getMetaData());
        DocumentData documentData = new DocumentData();
        documentData.setContent(mappingResponse.getDocumentContent().toString().getBytes(StandardCharsets.UTF_8));
        documentData.setCoded(mappingResponse.getToBeCoded());
        documentData.setPatientID(mappingResponse.getPatientID());
        if (mappingResponse.getPrivateData() != null)
            documentData.setPrivate(mappingResponse.getPrivateData().toString().getBytes(StandardCharsets.UTF_8));
        saveAction.setDocumentData(documentData);
        saveAction.setToBeCoded(mappingResponse.getToBeCoded());
        saveAction.setUniqueID(mappingResponse.getUniqueID());
        saveAction.setSource(DataSource.CSV);
        saveAction.setAffectedFields(MappingResponseMapper.mapDataSources(mappingResponse.getAffectedFields(),DataSource.CSV));
        saveAction.setFile(fileName);
        saveAction.setProgress(mappingResponse.getProgress());
        saveAction.setAttachments(mappingResponse.getPendingAttachments());

        return workflowActionExecutor.execute(saveAction, principal);
    }

    private void loadAttachments(String path, List<Attachment> attachments) {
        if(StringUtils.isBlank(path) || CollectionUtils.isEmpty(attachments)) {
            return;
        }
        final File parentFile = new File(path);
        for (final Attachment attachment : attachments) {
            final File file = new File(parentFile, attachment.getFilename());
            try {
                final AttachmentContent content = new AttachmentContent();
                content.setContent(FileUtils.readFileToByteArray(file));
                attachment.setContent(content);
            } catch (IOException e) {
                HealthDataException exception = new HealthDataException(e);
                exception.setExceptionType(ExceptionType.ERROR_WHILE_READING_FILE, e.getMessage());
                throw exception;
            }
        }
    }

    private List<File> getAttachmentFiles(String path, List<Attachment> attachments) {
        if(StringUtils.isBlank(path) || CollectionUtils.isEmpty(attachments)) {
            return null;
        }
        final List<File> files = new ArrayList<>();
        final File parentFile = new File(path);
        for (final Attachment attachment : attachments) {
            files.add(new File(parentFile, attachment.getFilename()));
        }
        return files;
    }
}
