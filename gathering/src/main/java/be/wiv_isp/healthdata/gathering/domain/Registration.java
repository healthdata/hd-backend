/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.domain;

import be.wiv_isp.healthdata.gathering.domain.enumeration.ExecutionOption;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "REGISTRATIONS")
public class Registration {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "REGISTRATION_ID")
	private Long id;

	@Column(name = "DATA_COLLECTION_NAME", nullable = false)
	private String dataCollectionName;

	@Lob
	@Column(name = "CONTENT", nullable = false)
	private byte[] content;

	@ElementCollection(targetClass = ExecutionOption.class)
	@Enumerated(EnumType.STRING)
	@CollectionTable(name = "REGISTRATION_OPTIONS", joinColumns = @JoinColumn(name = "REGISTRATION_ID"))
	@Column(name = "EXEC_OPTION")
	private List<ExecutionOption> executionOptions;

	@Fetch(FetchMode.JOIN)
	@OneToOne(optional = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "WORKFLOW_ID")
	private RegistrationWorkflow workflow;

	@Column(name = "CREATED_ON", nullable = false)
	private Timestamp createdOn;

	@Column(name = "UPDATED_ON", nullable = false)
	private Timestamp updatedOn;

	@Column(name = "DELETED")
	private boolean deleted;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDataCollectionName() {
		return dataCollectionName;
	}

	public void setDataCollectionName(String dataCollectionName) {
		this.dataCollectionName = dataCollectionName;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public List<ExecutionOption> getExecutionOptions() {
		return executionOptions;
	}

	public void setExecutionOptions(List<ExecutionOption> executionOptions) {
		this.executionOptions = executionOptions;
	}

	public RegistrationWorkflow getWorkflow() {
		return workflow;
	}

	public void setWorkflow(RegistrationWorkflow workflow) {
		this.workflow = workflow;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@PrePersist
	public void onCreate() {
		Timestamp createdOnTimestamp = new Timestamp(new Date().getTime());
		setCreatedOn(createdOnTimestamp);
		setUpdatedOn(createdOnTimestamp);
	}

	@PreUpdate
	public void onUpdate() {
		setUpdatedOn(new Timestamp(new Date().getTime()));
	}
}
