/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.api;

import be.wiv_isp.healthdata.gathering.domain.ParticipationDocument;
import be.wiv_isp.healthdata.gathering.domain.ParticipationWorkflow;
import be.wiv_isp.healthdata.gathering.domain.search.ParticipationWorkflowSearch;
import be.wiv_isp.healthdata.gathering.dto.FullSearchParticipationWorkflowDto;
import be.wiv_isp.healthdata.gathering.dto.ParticipationWorkflowDto;
import be.wiv_isp.healthdata.orchestration.api.AbstractParticipationWorkflowRestService;
import be.wiv_isp.healthdata.orchestration.service.IAbstractParticipationWorkflowService;
import be.wiv_isp.healthdata.orchestration.service.IAbstractWorkflowService;
import be.wiv_isp.healthdata.orchestration.action.workflow.executor.IParticipationWorkflowActionExecutor;
import be.wiv_isp.healthdata.orchestration.action.workflow.executor.IWorkflowActionExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Path;

@Component
@Path("/participations")
public class ParticipationWorkflowRestService extends AbstractParticipationWorkflowRestService<ParticipationWorkflow, ParticipationWorkflowSearch, FullSearchParticipationWorkflowDto, ParticipationWorkflowDto, ParticipationDocument> {

	@Autowired
	private IAbstractParticipationWorkflowService workflowService;

	@Autowired
	private IParticipationWorkflowActionExecutor workflowActionExecutor;

	@Override
	protected IAbstractWorkflowService<ParticipationWorkflow, ParticipationWorkflowSearch> getWorkflowService() {
		return workflowService;
	}

	@Override
	protected FullSearchParticipationWorkflowDto getFullDtoInstance(ParticipationWorkflowDto dto) {
		return new FullSearchParticipationWorkflowDto(dto);
	}

	@Override
	protected IWorkflowActionExecutor<ParticipationWorkflow> getWorkflowActionExecutor() {
		return workflowActionExecutor;
	}

	@Override
	protected boolean ignoreDeletedWorkflows() {
		return true;
	}
}
