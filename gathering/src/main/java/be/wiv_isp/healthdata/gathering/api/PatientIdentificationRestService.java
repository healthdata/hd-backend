/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.gathering.api;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.gathering.dto.PersonDto;
import be.wiv_isp.healthdata.gathering.service.IPatientIdentificationService;
import be.wiv_isp.healthdata.orchestration.annotation.Auditable;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ApiType;
import be.wiv_isp.healthdata.orchestration.service.IMappingSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("/patient")
public class PatientIdentificationRestService {

	@Autowired
	private IPatientIdentificationService patientIdentificationService;
	@Autowired
	private IMappingSecurityService securityService;

	@GET
	@PreAuthorize("hasRole('ROLE_USER')")
	@Path("/{patientId}")
	@Auditable(apiType = ApiType.PATIENT)
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("patientId") String patientId) {
		final PersonDto person = patientIdentificationService.get(patientId);
		return Response.ok(person).build();
	}


	@GET
	@Path("mapping/{patientId}")
	@Auditable(apiType = ApiType.PATIENT)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getForMapping(@HeaderParam("Authorization") String bearerToken, @PathParam("patientId") String patientId) {
		if(!securityService.hasAccess(bearerToken)) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GENERAL_UNAUTHORIZED);
			throw exception;
		}
		final PersonDto person = patientIdentificationService.get(patientId);
		return Response.ok(person).build();
	}
}
