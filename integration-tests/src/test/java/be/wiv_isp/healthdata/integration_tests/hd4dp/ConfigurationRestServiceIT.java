/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.integration_tests.hd4dp;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.dto.ConfigurationDto;
import be.wiv_isp.healthdata.integration_tests.utils.ConfigurationUtils;
import be.wiv_isp.healthdata.integration_tests.utils.TestUtils;
import be.wiv_isp.healthdata.integration_tests.utils.enums.HealthDataRole;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.util.StringUtils;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class ConfigurationRestServiceIT {

	private static TestUtils TEST_UTILS = new TestUtils();
	private static ConfigurationUtils CONFIGURATION_UTILS;

	private final ConfigurationKey CONFIGURATION_KEY = ConfigurationKey.NATIONAL_REGISTER_CONNECTOR;

	@BeforeClass
	public static void init() throws Exception {
		TEST_UTILS.initialize(UserConfigurationRestServiceIT.class.getResource("/test.it.properties"));
		CONFIGURATION_UTILS = new ConfigurationUtils(TEST_UTILS);
	}

	@Test
	public void testGetRoleUser() throws Exception {
		final ConfigurationDto configuration = CONFIGURATION_UTILS.getConfigurationEntity(CONFIGURATION_KEY, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER));
		Assert.assertNotNull(configuration);
	}

	@Test
	public void testGetRoleAdmin() throws Exception {
		final ConfigurationDto configuration = CONFIGURATION_UTILS.getConfigurationEntity(CONFIGURATION_KEY, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN));
		Assert.assertNotNull(configuration);
	}

	@Test
	public void testGetNonExistingEntity() {
		final WebResource wr = TEST_UTILS.createWebResource("{0}/configurations/nonexisting_key", Host.HD4DP);
		Assert.assertTrue(wr.head().getStatus() == Status.NOT_FOUND.getStatusCode());
	}

	@Test
	public void testUpdateRoleAdmin() throws Exception {
		ConfigurationDto configuration = CONFIGURATION_UTILS.getConfigurationEntity(CONFIGURATION_KEY, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN));
		String oldValue = configuration.getValue();
		configuration.setValue("new value");

		final ConfigurationDto updatedConfiguration = CONFIGURATION_UTILS.updateConfigurationEntity(configuration.getId(), configuration, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN));

		final ConfigurationDto retrievedConfiguration = CONFIGURATION_UTILS.getConfigurationEntity(updatedConfiguration.getId(), TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN));

		compareConfigurations(updatedConfiguration, configuration);
		compareConfigurations(retrievedConfiguration, configuration);

		configuration.setValue(oldValue);
		CONFIGURATION_UTILS.updateConfigurationEntity(configuration.getId(), configuration, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN));
	}

	@Test
	public void testUpdateRoleUser() throws Exception {
		ConfigurationDto configuration = CONFIGURATION_UTILS.getConfigurationEntity(CONFIGURATION_KEY, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN));
		configuration.setValue("new value");

		final ClientResponse response = CONFIGURATION_UTILS.updateConfiguration(configuration.getId(), configuration, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER));
		Assert.assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
	}

	@Test
	public void testUpdateWithNonMatchingKeys() throws Exception {
		ConfigurationDto configuration = CONFIGURATION_UTILS.getConfigurationEntity(CONFIGURATION_KEY, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN));
		final ClientResponse response = CONFIGURATION_UTILS.updateConfiguration(-1L, configuration, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN));
		Assert.assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCORSHeadersAvailability() throws Exception {
		final ClientResponse response = CONFIGURATION_UTILS.getConfigurations(CONFIGURATION_KEY, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN));
		final MultivaluedMap<String, String> headerMap = response.getHeaders();
		Assert.assertFalse(headerMap.isEmpty());
		Assert.assertEquals("*", headerMap.getFirst("Access-Control-Allow-Origin"));
		Assert.assertEquals("GET, POST, PUT, DELETE, OPTIONS", headerMap.getFirst("Access-Control-Allow-Methods"));
		Assert.assertEquals("10", headerMap.getFirst("Access-Control-Max-Age"));
		Assert.assertTrue(StringUtils.isEmpty(headerMap.getFirst("Access-Control-Request-Headers")));
	}

	@Test
	public void testCORSHeadersAvailabilityForbidden() throws Exception {
		ConfigurationDto configuration = new ConfigurationDto();
		configuration.setId(1L);
		configuration.setKey(CONFIGURATION_KEY);
		configuration.setValue("newValue");

		final ClientResponse response = CONFIGURATION_UTILS.updateConfiguration(1L, configuration, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER));
		Assert.assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
		final MultivaluedMap<String, String> headerMap = response.getHeaders();
		Assert.assertFalse(headerMap.isEmpty());
		Assert.assertEquals("*", headerMap.getFirst("Access-Control-Allow-Origin"));
		Assert.assertEquals("GET, POST, PUT, DELETE, OPTIONS", headerMap.getFirst("Access-Control-Allow-Methods"));
		Assert.assertEquals("10", headerMap.getFirst("Access-Control-Max-Age"));
		Assert.assertTrue(StringUtils.isEmpty(headerMap.getFirst("Access-Control-Request-Headers")));
	}

	@Test
	public void testCORSHeadersAvailabilityOptions() throws Exception {
		final ClientResponse response = CONFIGURATION_UTILS.getConfigurations(TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER));
		final MultivaluedMap<String, String> headerMap = response.getHeaders();
		Assert.assertFalse(headerMap.isEmpty());
		Assert.assertEquals("*", headerMap.getFirst("Access-Control-Allow-Origin"));
		Assert.assertEquals("GET, POST, PUT, DELETE, OPTIONS", headerMap.getFirst("Access-Control-Allow-Methods"));
		Assert.assertEquals("10", headerMap.getFirst("Access-Control-Max-Age"));
		Assert.assertEquals("Content-Range", headerMap.getFirst("Access-Control-Expose-Headers"));
		Assert.assertTrue(StringUtils.isEmpty(headerMap.getFirst("Access-Control-Request-Headers")));
	}

	@Test
	public void testHttpHeadersSecurity() throws Exception {
		final ClientResponse response = CONFIGURATION_UTILS.getConfigurations(TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER));
		final MultivaluedMap<String, String> headerMap = response.getHeaders();
		Assert.assertFalse(headerMap.isEmpty());
		Assert.assertEquals("-", headerMap.getFirst("Server"));
		Assert.assertEquals("SAMEORIGIN", headerMap.getFirst("X-Frame-Options"));
	}

	private void compareConfigurations(ConfigurationDto postConfiguration, ConfigurationDto configuration) {
		Assert.assertEquals(postConfiguration.getKey(), configuration.getKey());
		Assert.assertEquals(postConfiguration.getValue(), configuration.getValue());
		Assert.assertEquals(postConfiguration.getDescription(), configuration.getDescription());
		Assert.assertEquals(postConfiguration.isReadable(), configuration.isReadable());
		Assert.assertNotNull(configuration.getUpdatedOn());
	}
}