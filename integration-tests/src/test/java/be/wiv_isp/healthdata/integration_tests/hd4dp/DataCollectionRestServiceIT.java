/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.integration_tests.hd4dp;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.common.api.ClientVersion;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.integration_tests.utils.TestUtils;
import be.wiv_isp.healthdata.integration_tests.utils.UserUtils;
import be.wiv_isp.healthdata.integration_tests.utils.enums.HealthDataRole;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.dto.UserDto;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DataCollectionRestServiceIT {

	private static TestUtils TEST_UTILS;
	private static UserUtils USER_UTILS;

	@BeforeClass
	public static void init() throws Exception {
		TEST_UTILS = new TestUtils();
		TEST_UTILS.initialize(UserRestServiceIT.class.getResource("/test.it.properties"));
		USER_UTILS = TEST_UTILS.getUserUtils();
	}

	@Test
	public void testGetAllRoleUser() throws Exception {
		UserDto createdUser = null;
		try {
			final UserDto user = USER_UTILS.buildUser();
			user.setAuthorities(Collections.singleton(Authority.USER_AUTHORITY));

			final Organization mainOrganization = TEST_UTILS.getOrganizationUtils().getMainOrganization(Host.HD4DP);
			createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);

			addDataCollection(createdUser, "TEST");
			final Set<String> dataCollectionList = getDataCollections(createdUser);

			final WebResource wr = TEST_UTILS.createWebResource("{0}/datacollections?publicOnly=true", Host.CATALOGUE);
			final List<String> publicDataCollections = wr.header("clientVersion", ClientVersion.DataCollection._20151103).get(new GenericType<List<String>>() {});
			final Set<String> dataCollections = new HashSet<>();
			dataCollections.addAll(user.getDataCollectionNames());
			dataCollections.addAll(publicDataCollections);

			Assert.assertEquals(dataCollections, dataCollectionList);
		} finally {
			if(createdUser != null) {
				USER_UTILS.deleteUser(Host.HD4DP, createdUser);
			}
		}
	}

	@Test
	public void testGetAllRoleAdmin() throws Exception {
		UserDto createdAdminUser = null;
		try {
			final UserDto user = USER_UTILS.buildUser();
			user.setAuthorities(Collections.singleton(Authority.ADMIN_AUTHORITY));

			final Organization mainOrganization = TEST_UTILS.getOrganizationUtils().getMainOrganization(Host.HD4DP);
			createdAdminUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);

			addDataCollection(createdAdminUser, "TEST");
			final Set<String> dataCollectionList = getDataCollections(createdAdminUser);
			Assert.assertTrue(dataCollectionList.size() >= 1);
		} finally {
			if(createdAdminUser != null) {
				USER_UTILS.deleteUser(Host.HD4DP, createdAdminUser);
			}
		}
	}

	@Test
	public void testGetAllRoleAdminAndUser() throws Exception {
		UserDto createdUser = null;
		try {
			final UserDto user = USER_UTILS.buildUser();
			final Set<Authority> authorities = new HashSet<>();
			authorities.add(Authority.USER_AUTHORITY);
			authorities.add(Authority.ADMIN_AUTHORITY);
			user.setAuthorities(authorities);

			final Organization mainOrganization = TEST_UTILS.getOrganizationUtils().getMainOrganization(Host.HD4DP);
			createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);

			addDataCollection(createdUser, "TEST");
			final Set<String> dataCollectionList = getDataCollections(createdUser);

			final WebResource wr = TEST_UTILS.createWebResource("{0}/datacollections?publicOnly=true", Host.CATALOGUE);
			final List<String> publicDataCollections = wr.header("clientVersion", ClientVersion.DataCollection._20151103).get(new GenericType<List<String>>() {});
			final Set<String> dataCollections = new HashSet<>();
			dataCollections.addAll(user.getDataCollectionNames());
			dataCollections.addAll(publicDataCollections);

			Assert.assertEquals(dataCollections, dataCollectionList);
		} finally {
			if(createdUser != null) {
				USER_UTILS.deleteUser(Host.HD4DP, createdUser);
			}
		}
	}

	private Set<String> getDataCollections(UserDto user) {
		final String token = TEST_UTILS.getAccessToken(Host.HD4DP, user.getOrganization().getId(), user.getUsername(), user.getPassword());

		final WebResource wr = TEST_UTILS.createWebResource("{0}/datacollections", Host.HD4DP);
		final ClientResponse response = wr
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token)
                .get(ClientResponse.class);

		Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		return response.getEntity(new GenericType<Set<String>>() {});
	}

	private void addDataCollection(final UserDto user, final String dataCollectionName) {
		user.getDataCollectionNames().add(dataCollectionName);

		final String token = TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN);

		final WebResource wr = TEST_UTILS.createWebResource("{0}/users/{1}", Host.HD4DP, String.valueOf(user.getId()));
		final ClientResponse response = wr.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token) //
				.type(MediaType.APPLICATION_JSON) //
				.accept(MediaType.APPLICATION_JSON_TYPE) //
				.put(ClientResponse.class, user);

		Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	}
}
