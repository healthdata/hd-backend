/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.integration_tests.endtoend;

import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.gathering.dto.RegistrationWorkflowDto;
import be.wiv_isp.healthdata.integration_tests.endtoend.categories.*;
import be.wiv_isp.healthdata.integration_tests.utils.ConfigurationUtils;
import be.wiv_isp.healthdata.integration_tests.utils.TestUtils;
import be.wiv_isp.healthdata.integration_tests.utils.WorkflowUtils;
import be.wiv_isp.healthdata.integration_tests.utils.enums.ConnectionStatus;
import be.wiv_isp.healthdata.integration_tests.utils.enums.DataCollection;
import be.wiv_isp.healthdata.integration_tests.utils.enums.HealthDataRole;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.RegistrationWorkflowHistory;
import be.wiv_isp.healthdata.orchestration.domain.SendStatus;
import be.wiv_isp.healthdata.orchestration.domain.WorkflowFlags;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.EMInterfaceType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.dto.CommentDto;
import be.wiv_isp.healthdata.orchestration.dto.DocumentDto;
import be.wiv_isp.healthdata.orchestration.dto.NoteDto;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.Thread.State;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.*;

public class EndToEndScenario {

	private static final Logger LOG = LoggerFactory.getLogger(EndToEndScenario.class);

	private static TestUtils TEST_UTILS = new TestUtils();
	private static WorkflowUtils WORKFLOW_UTILS;
	private static ConfigurationUtils CONFIGURATION_UTILS;

	/* datawarehouse actions */
	private static final String ACCEPT = "accept";
	private static final String REJECT = "reject";

	private static JSONObject ACCEPTABLE_DOCUMENT;
	private static JSONObject DOCUMENT_WITH_FOLLOW_UP;
	private static JSONObject NON_ACCEPTABLE_DOCUMENT;
	private static JSONObject CORRECTED_DOCUMENT;

	private static String COMMENT = "Is this correct?";

	private int nbWorkflows;
	private DataCollection dataCollection;
	private Map<Host, List<RegistrationWorkflowDto>> workflows;

	public static final String FOLLOW_UP_LABEL = "followup1";

	private enum Flags {
		CORRECTIONS, FOLLOW_UP
	}

	@BeforeClass
	public static void init() throws Exception {
		TEST_UTILS.initialize(EndToEndScenario.class.getResource("/test.it.properties"));
		WORKFLOW_UTILS = new WorkflowUtils(TEST_UTILS);
		CONFIGURATION_UTILS = new ConfigurationUtils(TEST_UTILS);

		ACCEPTABLE_DOCUMENT = JsonUtils.createJsonObject(IOUtils.toByteArray(EndToEndScenario.class.getClassLoader().getResourceAsStream("endToEndScenario/acceptable_document.json")));
		DOCUMENT_WITH_FOLLOW_UP = JsonUtils.createJsonObject(IOUtils.toByteArray(EndToEndScenario.class.getClassLoader().getResourceAsStream("endToEndScenario/document_with_follow_up.json")));
		NON_ACCEPTABLE_DOCUMENT = JsonUtils.createJsonObject(IOUtils.toByteArray(EndToEndScenario.class.getClassLoader().getResourceAsStream("endToEndScenario/non_acceptable_document.json")));
		CORRECTED_DOCUMENT = JsonUtils.createJsonObject(IOUtils.toByteArray(EndToEndScenario.class.getClassLoader().getResourceAsStream("endToEndScenario/corrected_document.json")));
	}

	@Before
	public void before() throws Exception {
		// default values
		nbWorkflows = 1;
		dataCollection = DataCollection.TEST;

		TEST_UTILS.setConnectionBetweenHd4dpAndHd4res(ConnectionStatus.UP);
		TEST_UTILS.setConnectionBetweenHd4resAndHd4dp(ConnectionStatus.UP);

		workflows = new HashMap<>();
		workflows.put(Host.HD4DP, new ArrayList<RegistrationWorkflowDto>());
		workflows.put(Host.HD4RES, new ArrayList<RegistrationWorkflowDto>());
	}

	@After
	public void after() throws Exception {
		TEST_UTILS.setConnectionBetweenHd4dpAndHd4res(ConnectionStatus.UP);
		TEST_UTILS.setConnectionBetweenHd4resAndHd4dp(ConnectionStatus.UP);
		TEST_UTILS.restoreAllConfigurations();
	}

	@Test
	@Category(SimpleFlow.class)
	public void testApprove() throws Exception {
		save(ACCEPTABLE_DOCUMENT);
		submit();
		verifyDocumentContent(Host.HD4RES, ACCEPTABLE_DOCUMENT);
		approve();
	}

	@Test
	@Category(FeedbackLoop.class)
	public void testRejectCorrectAndApproveWithRest() throws Exception {
		TEST_UTILS.setConfiguration(Host.HD4DP, ConfigurationKey.EM_INTERFACE_IN, EMInterfaceType.REST.toString());
		TEST_UTILS.setConfiguration(Host.HD4DP, ConfigurationKey.EM_INTERFACE_OUT, EMInterfaceType.REST.toString());
		testRejectCorrectAndApprove();
	}

	@Test
	@Category(FeedbackLoop.class)
	public void testRejectCorrectAndApproveWithFileSystem() throws Exception {
		TEST_UTILS.setConfiguration(Host.HD4DP, ConfigurationKey.EM_INTERFACE_IN, EMInterfaceType.FILE_SYSTEM.toString());
		TEST_UTILS.setConfiguration(Host.HD4DP, ConfigurationKey.EM_INTERFACE_OUT, EMInterfaceType.FILE_SYSTEM.toString());
		testRejectCorrectAndApprove();
	}

	private void testRejectCorrectAndApprove() throws Exception {
		save(NON_ACCEPTABLE_DOCUMENT);
		submit();
		verifyDocumentContent(Host.HD4RES, NON_ACCEPTABLE_DOCUMENT);

		simulateDataWarehouseProcess(REJECT);
		waitForStatusBeingUpdated(Host.HD4RES, WorkflowStatus.ANNOTATIONS_NEEDED);
		addNote(COMMENT);
		submitAnnotations();
		waitForStatusBeingUpdated(Host.HD4DP, WorkflowStatus.ACTION_NEEDED);
		verifyNoteOnHd4dp();

		save(CORRECTED_DOCUMENT);
		submit();
		verifyDocumentContent(Host.HD4RES, CORRECTED_DOCUMENT);
		approve();
	}

	@Test
	@Category(DeleteAction.class)
	public void testDelete() throws Exception {
		save(ACCEPTABLE_DOCUMENT);
		delete();
	}

	@Test
	@Category(DeleteAction.class)
	public void testDeleteAfterSubmitAndReject() throws Exception {
		save(NON_ACCEPTABLE_DOCUMENT);
		submit();
		verifyDocumentContent(Host.HD4RES, NON_ACCEPTABLE_DOCUMENT);

		simulateDataWarehouseProcess(REJECT);
		waitForStatusBeingUpdated(Host.HD4RES, WorkflowStatus.ANNOTATIONS_NEEDED);
		addNote(COMMENT);
		submitAnnotations();
		waitForStatusBeingUpdated(Host.HD4DP, WorkflowStatus.ACTION_NEEDED);
		waitForWorkflowFlags(Host.HD4DP, createFlags(Flags.CORRECTIONS));
		verifyNoteOnHd4dp();

		delete();
		waitForWorkflowsBeingDeleted(Host.HD4RES);
	}

	@Test
	@Category(DeleteAction.class)
	public void testDeleteAfterFollowUpIsActive() throws Exception {
		dataCollection = DataCollection.TEST_FOLLOW_UP;
		save(DOCUMENT_WITH_FOLLOW_UP, createComputedExpressions(true));
		submit();
		verifyDocumentContent(Host.HD4RES, DOCUMENT_WITH_FOLLOW_UP);

		verifyFollowUpActivation(Host.HD4DP, false);
		verifyFollowUpActivation(Host.HD4RES, false);
		verifyFollowUpIsScheduled(Host.HD4DP, true);
		verifyFollowUpIsScheduled(Host.HD4RES, true);
		waitForFollowUpBeingActivated(Host.HD4DP);
		waitForFollowUpBeingActivated(Host.HD4RES);
		waitForStatusBeingUpdated(Host.HD4DP, WorkflowStatus.ACTION_NEEDED);
		waitForWorkflowFlags(Host.HD4DP, createFlags(Flags.FOLLOW_UP));

		delete();
		waitForWorkflowsBeingDeleted(Host.HD4RES);
	}

	@Test
	@Category(DeleteAction.class)
	public void testDeleteAfterFollowUpIsSubmittedNotPossible() throws Exception {
		dataCollection = DataCollection.TEST_FOLLOW_UP;
		save(DOCUMENT_WITH_FOLLOW_UP, createComputedExpressions(true));
		submit();
		verifyDocumentContent(Host.HD4RES, DOCUMENT_WITH_FOLLOW_UP);

		verifyFollowUpActivation(Host.HD4DP, false);
		verifyFollowUpActivation(Host.HD4RES, false);
		verifyFollowUpIsScheduled(Host.HD4DP, true);
		verifyFollowUpIsScheduled(Host.HD4RES, true);
		waitForFollowUpBeingActivated(Host.HD4DP);
		waitForFollowUpBeingActivated(Host.HD4RES);
		waitForStatusBeingUpdated(Host.HD4DP, WorkflowStatus.ACTION_NEEDED);
		waitForWorkflowFlags(Host.HD4DP, createFlags(Flags.FOLLOW_UP));

		submit();
		verifyDocumentContent(Host.HD4RES, DOCUMENT_WITH_FOLLOW_UP);
		verifyFollowUpIsSubmitted(Host.HD4DP, true);

		for (RegistrationWorkflowDto workflow : workflows.get(Host.HD4DP)) {
			WORKFLOW_UTILS.deleteWorkflow(workflow.getIdAsLong(), ClientResponse.Status.BAD_REQUEST);
		}
	}

	@Test
	@Category(ResubmitToDWH.class)
	public void testRejectResubmitToDwhAndApprove() throws Exception {
		save(NON_ACCEPTABLE_DOCUMENT);
		submit();
		verifyDocumentContent(Host.HD4RES, NON_ACCEPTABLE_DOCUMENT);
		simulateDataWarehouseProcess(REJECT);
		waitForStatusBeingUpdated(Host.HD4RES, WorkflowStatus.ANNOTATIONS_NEEDED);
		resubmitWorkflowToDwh();
		approve();
	}

	@Test
	@Category(Reopen.class)
	public void testReOpenBeforeRejectAndSubmit() throws Exception {
		save(NON_ACCEPTABLE_DOCUMENT);
		submit();
		verifyDocumentContent(Host.HD4RES, NON_ACCEPTABLE_DOCUMENT);
		simulateDataWarehouseProcess(REJECT);
		waitForStatusBeingUpdated(Host.HD4RES, WorkflowStatus.ANNOTATIONS_NEEDED);
		addNote(COMMENT);
		reOpen();
		save(CORRECTED_DOCUMENT);
		submitAnnotations();
		submit();
		verifyDocumentContent(Host.HD4RES, CORRECTED_DOCUMENT);
		approve();
		// check that the update for correction action has been ignored
		verifyHistoryDoesNotContain(Host.HD4DP, WorkflowStatus.ACTION_NEEDED);
	}

	@Test
	@Category(Reopen.class)
	public void testReOpenBeforeApproveAndSubmit() throws Exception {
		save(ACCEPTABLE_DOCUMENT);
		submit();
		verifyDocumentContent(Host.HD4RES, ACCEPTABLE_DOCUMENT);
		reOpen();

		save(CORRECTED_DOCUMENT);
		submit();
		simulateDataWarehouseProcess(ACCEPT);
		waitForStatusBeingUpdated(Host.HD4RES, WorkflowStatus.NEW);
		verifyDocumentContent(Host.HD4RES, CORRECTED_DOCUMENT);
		approve();
		// check that the first APPROVE update status action has been ignored
		verifyHistoryContainsExactly(Host.HD4DP, WorkflowStatus.APPROVED, 1);
	}

	@Test
	@Category(LostMessages.class)
	public void testSendResendMessagesAction() throws Exception {
		int msgServiceTimeoutHd4dp = getAndVerifyIntegerConfigurationValue(Host.HD4DP, ConfigurationKey.SCHEDULED_TASK_MESSAGING_SERVICE_INTERVAL, 5, 60);
		int msgServiceTimeoutHd4res = getAndVerifyIntegerConfigurationValue(Host.HD4RES, ConfigurationKey.SCHEDULED_TASK_MESSAGING_SERVICE_INTERVAL, 5, 60);
		int acknowledgmentTimeLimitHd4dp = getAndVerifyIntegerConfigurationValue(Host.HD4DP, ConfigurationKey.ACKNOWLEDGMENT_TIME_LIMIT, 30, 300);

		save(ACCEPTABLE_DOCUMENT);
		TEST_UTILS.setConnectionBetweenHd4dpAndHd4res(ConnectionStatus.DOWN); // communication channel broken
		submit();
		waitFor("messaging service to send messages", msgServiceTimeoutHd4dp);
		waitFor("workflows to have send status equal to NOK", acknowledgmentTimeLimitHd4dp);
		waitWorkflowsHaveSendStatus(Host.HD4DP, SendStatus.NOK);
		TEST_UTILS.setConnectionBetweenHd4dpAndHd4res(ConnectionStatus.UP); // communication channel repaired
		sendResendMessagesAction();
		waitFor("messaging service to send messages", msgServiceTimeoutHd4res);
		waitWorkflowsHaveSendStatus(Host.HD4DP, SendStatus.OK);
		waitForStatusBeingUpdated(Host.HD4RES, WorkflowStatus.NEW);
		waitForWorkflowFlags(Host.HD4RES, createFlags());
		verifyDocumentContent(Host.HD4RES, ACCEPTABLE_DOCUMENT);
		approve();
	}

	@Test
	@Category(LostMessages.class)
	public void testResendMessages() throws Exception {
		int msgServiceTimeoutHd4res = getAndVerifyIntegerConfigurationValue(Host.HD4RES, ConfigurationKey.SCHEDULED_TASK_MESSAGING_SERVICE_INTERVAL, 5, 60);
		int acknowledgmentTimeLimitHd4res = getAndVerifyIntegerConfigurationValue(Host.HD4RES, ConfigurationKey.ACKNOWLEDGMENT_TIME_LIMIT, 30, 300);
		int dwhServiceTimeout = getAndVerifyIntegerConfigurationValue(Host.HD4RES, ConfigurationKey.SCHEDULED_TASK_DATAWAREHOUSE_SERVICE_INTERVAL, 5, 300);

		save(ACCEPTABLE_DOCUMENT);
		submit();
		verifyDocumentContent(Host.HD4RES, ACCEPTABLE_DOCUMENT);
		TEST_UTILS.setConnectionBetweenHd4resAndHd4dp(ConnectionStatus.DOWN); // communication channel broken
		simulateDataWarehouseProcess(ACCEPT);
		waitForStatusBeingUpdated(Host.HD4RES, WorkflowStatus.APPROVED);
		waitFor("wait for dwh service to generate APPROVED messages", dwhServiceTimeout);
		waitFor("messaging service to send messages", msgServiceTimeoutHd4res);
		waitFor("workflows to have send status equal to NOK", acknowledgmentTimeLimitHd4res);
		waitWorkflowsHaveSendStatus(Host.HD4RES, SendStatus.NOK);
		TEST_UTILS.setConnectionBetweenHd4resAndHd4dp(ConnectionStatus.UP); // communication channel repaired
		resendMessages(Host.HD4RES);
		waitFor("messaging service to send messages", msgServiceTimeoutHd4res);
		waitWorkflowsHaveSendStatus(Host.HD4RES, SendStatus.OK);
		waitForStatusBeingUpdated(Host.HD4DP, WorkflowStatus.APPROVED);
	}

	@Test
	@Category(BulkSubmit.class)
	public void testBulkSubmit() throws Exception {
		TEST_UTILS.setConnectionBetweenHd4dpAndHd4res(ConnectionStatus.ASYNCHRONOUS);

		nbWorkflows = 250;
		save(ACCEPTABLE_DOCUMENT);
		bulkSubmit();
		waitForStatusBeingUpdated(Host.HD4DP, WorkflowStatus.SUBMITTED);
		TEST_UTILS.transferAsyncMessages(Platform.HD4DP, Platform.HD4RES);
		waitForWorkflowsToBeCreatedOnHd4res();
		waitForStatusBeingUpdated(Host.HD4RES, WorkflowStatus.NEW);
		waitForWorkflowFlags(Host.HD4RES, createFlags());
		verifyDocumentContent(Host.HD4RES, ACCEPTABLE_DOCUMENT);
		approve();
	}

	@Test
	@Category(SendingInBatch.class)
	public void testRejectCorrectAndApproveWithMultipleWorkflowAndSendingInBatch() throws Exception {
		TEST_UTILS.setConnectionBetweenHd4dpAndHd4res(ConnectionStatus.ASYNCHRONOUS);
		TEST_UTILS.setConnectionBetweenHd4resAndHd4dp(ConnectionStatus.ASYNCHRONOUS);

		int msgServiceTimeoutHd4dp = getAndVerifyIntegerConfigurationValue(Host.HD4DP, ConfigurationKey.SCHEDULED_TASK_MESSAGING_SERVICE_INTERVAL, 5, 60);
		int msgServiceTimeoutHd4res = getAndVerifyIntegerConfigurationValue(Host.HD4RES, ConfigurationKey.SCHEDULED_TASK_MESSAGING_SERVICE_INTERVAL, 5, 60);
		int dwhServiceTimeout = getAndVerifyIntegerConfigurationValue(Host.HD4RES, ConfigurationKey.SCHEDULED_TASK_DATAWAREHOUSE_SERVICE_INTERVAL, 5, 300);

		nbWorkflows = 25;
		save(NON_ACCEPTABLE_DOCUMENT);
		submit();
		waitFor("messaging service to send messages", msgServiceTimeoutHd4dp);
		TEST_UTILS.transferAsyncMessages(Platform.HD4DP, Platform.HD4RES);
		waitForWorkflowsToBeCreatedOnHd4res();
		waitForStatusBeingUpdated(Host.HD4RES, WorkflowStatus.NEW, true);
		waitForWorkflowFlags(Host.HD4RES, createFlags());
		verifyDocumentContent(Host.HD4RES, NON_ACCEPTABLE_DOCUMENT);
		simulateDataWarehouseProcess(REJECT);
		waitForStatusBeingUpdated(Host.HD4RES, WorkflowStatus.ANNOTATIONS_NEEDED);
		addNote(COMMENT);
		submitAnnotations();
		waitFor("messaging service to send messages", msgServiceTimeoutHd4res);
		TEST_UTILS.transferAsyncMessages(Platform.HD4RES, Platform.HD4DP);
		waitForStatusBeingUpdated(Host.HD4DP, WorkflowStatus.ACTION_NEEDED, true);
		verifyNoteOnHd4dp();
		save(CORRECTED_DOCUMENT);
		submit();
		waitFor("messaging service to send messages", msgServiceTimeoutHd4dp);
		TEST_UTILS.transferAsyncMessages(Platform.HD4DP, Platform.HD4RES);
		waitForStatusBeingUpdated(Host.HD4RES, WorkflowStatus.NEW, true);
		waitForWorkflowFlags(Host.HD4RES, createFlags(Flags.CORRECTIONS));
		verifyDocumentContent(Host.HD4RES, CORRECTED_DOCUMENT);
		simulateDataWarehouseProcess(ACCEPT);
		waitForStatusBeingUpdated(Host.HD4RES, WorkflowStatus.APPROVED);
		waitFor("wait for dwh service to generate APPROVED messages", dwhServiceTimeout);
		waitFor("messaging service to send messages", msgServiceTimeoutHd4res);
		TEST_UTILS.transferAsyncMessages(Platform.HD4RES, Platform.HD4DP);
		waitForStatusBeingUpdated(Host.HD4DP, WorkflowStatus.APPROVED, true);
	}

	@Test
	@Category(ReprocessMessage.class)
	public void testReprocessMessageAtHd4res() throws Exception {
		save(ACCEPTABLE_DOCUMENT);
		TEST_UTILS.setConfiguration(Host.HD4RES, ConfigurationKey.CATALOGUE_HOST, "http://wrong-host");
		int nbErrorMessages = getMessages(Host.HD4RES, MessageStatus.ERROR_PROCESSING).size();
		submit(false);
		waitForMessageToBeProcessedWithError(Host.HD4RES, nbErrorMessages);
		TEST_UTILS.restoreConfiguration(Host.HD4RES, ConfigurationKey.CATALOGUE_HOST);
		final Long messageId = getLastMessageProcessedWithError(Host.HD4RES);
		reprocessMessage(Host.HD4RES, messageId);
		waitForWorkflowsToBeCreatedOnHd4res();
		waitForStatusBeingUpdated(Host.HD4RES, WorkflowStatus.NEW);
		waitForWorkflowFlags(Host.HD4RES, createFlags());
		verifyDocumentContent(Host.HD4RES, ACCEPTABLE_DOCUMENT);
		approve();
	}

	@Test
	@Category(FollowUp.class)
	public void testFollowUpBecomesActiveAfterTimingConditionIsExpired() throws Exception {
		dataCollection = DataCollection.TEST_FOLLOW_UP;
		testFollowUpBecomesActiveAfterTimingConditionIsExpired(false);
	}

	@Test
	@Category(FollowUp.class)
	public void testFollowUpBecomesActiveAfterApprovedAndAfterTimingConditionIsExpired() throws Exception {
		dataCollection = DataCollection.TEST_FOLLOW_UP;
		testFollowUpBecomesActiveAfterTimingConditionIsExpired(true);
	}

	private void testFollowUpBecomesActiveAfterTimingConditionIsExpired(boolean approvedBeforeFollowUpActivation) throws Exception {
		save(DOCUMENT_WITH_FOLLOW_UP, createComputedExpressions(true));
		submit();
		verifyDocumentContent(Host.HD4RES, DOCUMENT_WITH_FOLLOW_UP);

		if(approvedBeforeFollowUpActivation) {
			approve();
		}

		verifyFollowUpActivation(Host.HD4DP, false);
		verifyFollowUpActivation(Host.HD4RES, false);
		verifyFollowUpIsScheduled(Host.HD4DP, true);
		verifyFollowUpIsScheduled(Host.HD4RES, true);
		waitForFollowUpBeingActivated(Host.HD4DP);
		waitForFollowUpBeingActivated(Host.HD4RES);
		waitForStatusBeingUpdated(Host.HD4DP, WorkflowStatus.ACTION_NEEDED);
		waitForWorkflowFlags(Host.HD4DP, createFlags(Flags.FOLLOW_UP));
		submit();
		verifyDocumentContent(Host.HD4RES, DOCUMENT_WITH_FOLLOW_UP);
		verifyFollowUpIsSubmitted(Host.HD4DP, true);
		approve();
	}

	@Test
	@Category(FollowUp.class)
	public void testFollowUpBecomesActiveAfterSaveAction() throws Exception {
		dataCollection = DataCollection.TEST_FOLLOW_UP;
		save(NON_ACCEPTABLE_DOCUMENT, createComputedExpressions(false));
		submit();
		verifyDocumentContent(Host.HD4RES, NON_ACCEPTABLE_DOCUMENT);
		verifyFollowUpActivation(Host.HD4DP, false);
		verifyFollowUpActivation(Host.HD4RES, false);
		verifyFollowUpIsScheduled(Host.HD4DP, false);
		verifyFollowUpIsScheduled(Host.HD4RES, false);
		simulateDataWarehouseProcess(REJECT);
		waitForStatusBeingUpdated(Host.HD4RES, WorkflowStatus.ANNOTATIONS_NEEDED);
		addNote(COMMENT);
		submitAnnotations();
		waitForStatusBeingUpdated(Host.HD4DP, WorkflowStatus.ACTION_NEEDED);
		verifyNoteOnHd4dp();
		waitFor("Follow up timing condition to be expired", 120);
		verifyFollowUpActivation(Host.HD4DP, false);
		verifyFollowUpIsScheduled(Host.HD4DP, false);
		save(DOCUMENT_WITH_FOLLOW_UP, createComputedExpressions(true));
		verifyFollowUpActivation(Host.HD4DP, true);
		verifyFollowUpIsScheduled(Host.HD4DP, true);
		submit();
		verifyFollowUpActivation(Host.HD4RES, true);
		verifyFollowUpIsScheduled(Host.HD4RES, true);
		verifyDocumentContent(Host.HD4RES, DOCUMENT_WITH_FOLLOW_UP);
		approve();
	}

	private JSONObject createComputedExpressions(Boolean value) throws JSONException {
		final JSONObject computedExpressions = new JSONObject();
		computedExpressions.put("number1biggerthan5", value); // true/false
		computedExpressions.put("number2biggerthan5", value.toString()); // "true"/"false" (both must work)
		return computedExpressions;
	}

	private void verifyFollowUpIsScheduled(Host host, boolean isScheduled) throws Exception {
		if(isScheduled) {
			LOG.info("verifying follow up [{}] is scheduled on {}", FOLLOW_UP_LABEL, host);
		} else {
			LOG.info("verifying follow up [{}] is not scheduled on {}", FOLLOW_UP_LABEL, host);
		}

		for (RegistrationWorkflowDto workflow : workflows.get(host)) {
			final RegistrationWorkflowDto workflowDto = WORKFLOW_UTILS.getWorkflow(host, workflow.getIdAsLong());
			Assert.assertEquals(isScheduled, getFollowUp(workflowDto.getContext().getFollowUps(), FOLLOW_UP_LABEL).isScheduled());
		}
	}

	private void verifyFollowUpIsSubmitted(Host host, boolean isSubmitted) throws Exception {
		if(isSubmitted) {
			LOG.info("verifying follow up [{}] is submitted on {}", FOLLOW_UP_LABEL, host);
		} else {
			LOG.info("verifying follow up [{}] is not submitted on {}", FOLLOW_UP_LABEL, host);
		}

		for (RegistrationWorkflowDto workflow : workflows.get(host)) {
			final RegistrationWorkflowDto workflowDto = WORKFLOW_UTILS.getWorkflow(host, workflow.getIdAsLong());
			final Timestamp submittedOn = getFollowUp(workflowDto.getContext().getFollowUps(), FOLLOW_UP_LABEL).getSubmittedOn();
			if(isSubmitted) {
				Assert.assertNotNull(submittedOn);
			} else {
				Assert.assertNull(submittedOn);
			}
		}
	}

	private void waitForWorkflowFlags(Host host, WorkflowFlags flags) throws Exception {
		LOG.info("waiting for workflows flags being updated to {} on {}", flags, host);
		for (RegistrationWorkflowDto workflow : workflows.get(host)) {
			boolean flagsOk = false;
			RegistrationWorkflowDto workflowDto = null;
			for(int i=0; i< TestUtils.MAX_TRIALS && !flagsOk; ++i) {
				workflowDto = WORKFLOW_UTILS.getWorkflow(host, workflow.getIdAsLong());
				flagsOk = flags.equals(workflowDto.getFlags());
				if(!flagsOk) {
					TEST_UTILS.waitBetweenTrials();
				}
			}
			if(!flagsOk) {
				throw new Exception(MessageFormat.format("expected workflow flags {0} for workflow {1} on {2} but was {3}", flags, workflowDto.getId(), host, workflowDto.getFlags()));
			}
		}
	}

	private WorkflowFlags createFlags(Flags... flags) {
		final WorkflowFlags wfFlags = new WorkflowFlags();
		if(flags == null) {
			return wfFlags;
		}

		for (Flags flag : flags) {
			switch(flag) {
				case CORRECTIONS: wfFlags.setCorrections(true);	break;
				case FOLLOW_UP: wfFlags.setFollowUp(true); break;
			}
		}
		return wfFlags;
	}

	private void waitForFollowUpBeingActivated(Host host) throws Exception {
		LOG.info("waiting for follow up [{}] being activated on {}", FOLLOW_UP_LABEL, host);

		for (RegistrationWorkflowDto workflow : workflows.get(host)) {
			boolean isFollowUpActive = false;
			RegistrationWorkflowDto workflowDto;
			for(int i=0; i< TestUtils.MAX_TRIALS && !isFollowUpActive; ++i) {
				workflowDto = WORKFLOW_UTILS.getWorkflow(host, workflow.getIdAsLong());
				isFollowUpActive = getFollowUp(workflowDto.getContext().getFollowUps(), FOLLOW_UP_LABEL).isActive();
				if(!isFollowUpActive) {
					TEST_UTILS.waitBetweenTrials();
				}
			}
			if(!isFollowUpActive) {
				throw new Exception(MessageFormat.format("expected follow up [{0}] to become active on {1}, but it was not", FOLLOW_UP_LABEL, host));
			}
		}
	}

	private void verifyFollowUpActivation(Host host, boolean active) throws Exception {
		LOG.info("verifying follow up [{}] has activation attribute equal to [{}] on {}", FOLLOW_UP_LABEL, active, host);

		for (RegistrationWorkflowDto workflow : workflows.get(host)) {
			final RegistrationWorkflowDto workflowDto = WORKFLOW_UTILS.getWorkflow(host, workflow.getIdAsLong());
			Assert.assertEquals(active, getFollowUp(workflowDto.getContext().getFollowUps(), FOLLOW_UP_LABEL).isActive());
		}
	}

	private void verifyDocumentContent(final Host host, final RegistrationWorkflowDto workflow, final byte[] expectedDocumentContent) throws Exception {
		verifyDocumentContent(host, workflow, JsonUtils.createJsonObject(expectedDocumentContent));
	}

	private void verifyDocumentContent(final Host host, final RegistrationWorkflowDto workflow, final JSONObject expectedDocumentContent) throws Exception {
		LOG.info("verifying document content of workflow {} on platform {}", workflow.getId(), host);
		final RegistrationWorkflowDto workflowWithDocument = WORKFLOW_UTILS.getWorkflow(host, workflow.getIdAsLong(), true, false);
		final DocumentDto document = workflowWithDocument.getDocument();
		Assert.assertEquals(expectedDocumentContent.toString(), JsonUtils.createJsonObject(document.getDocumentData().getContent()).toString());
	}

	private void simulateDataWarehouseProcess(String status) throws Exception {
		LOG.info("simulating data warehouse process with status [{}]", status);
		final WebResource wr = TEST_UTILS.createWebResource("{0}/rest/datawarehouse/{1}", Host.MOCK, status);
		final ClientResponse response = wr.type(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);
		Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	}

	private void verifyDocumentContent(Host host, JSONObject content) throws Exception {
		for (RegistrationWorkflowDto workflow : workflows.get(host)) {
			verifyDocumentContent(host, workflow, content);
		}
	}

	private void submit() throws Exception {
		submit(true);
	}

	private void submit(boolean waitForHd4resCreationAndStatusUpdate) throws Exception {
		WorkflowFlags flags = null;
		for (RegistrationWorkflowDto workflow : workflows.get(Host.HD4DP)) {
			if(flags == null) {
				final RegistrationWorkflowDto w = WORKFLOW_UTILS.getWorkflow(Host.HD4DP, workflow.getIdAsLong());
				flags = w.getFlags();
			}
			WORKFLOW_UTILS.submit(workflow);
			WORKFLOW_UTILS.verifyStatus(workflow, Host.HD4DP, WorkflowStatus.OUTBOX, WorkflowStatus.SUBMITTED);
		}
		waitForStatusBeingUpdated(Host.HD4DP, WorkflowStatus.SUBMITTED);

		if(waitForHd4resCreationAndStatusUpdate) {
			if(workflows.get(Host.HD4RES).isEmpty()) {
				waitForWorkflowsToBeCreatedOnHd4res();
			}

			waitForStatusBeingUpdated(Host.HD4RES, WorkflowStatus.NEW);
			waitForWorkflowFlags(Host.HD4RES, flags);
		}

	}

	private void reOpen() throws Exception {
		LOG.info("Re opening documents");
		for (RegistrationWorkflowDto workflow : workflows.get(Host.HD4DP)) {
			WORKFLOW_UTILS.reOpen(workflow);
		}
	}

	private void save(JSONObject content) throws Exception {
		save(content, null);
	}

	private void save(JSONObject content, JSONObject computedExpressions) throws Exception {
		final JSONObject requestBody = new JSONObject();
		requestBody.put("action", "SAVE");
		requestBody.put("documentContent", content);
		final Map<String, String> metadata = new HashMap<>();
		metadata.put("PATIENT_IDENTIFIER", "12345678910");
		requestBody.put("metaData", metadata);

		if (computedExpressions == null) {
			computedExpressions = new JSONObject();
		}
		requestBody.put("computedExpressions", computedExpressions);

		if(workflows.get(Host.HD4DP).isEmpty()) {
			LOG.info("Creating workflows for data collection {}", dataCollection);
			for (int i = 0; i < nbWorkflows; ++i) {
				requestBody.put("dataCollectionDefinitionId", TEST_UTILS.getDataCollectionDefinitionId(dataCollection));

				final ClientResponse response = WORKFLOW_UTILS.executeAction(requestBody);
				final RegistrationWorkflowDto workflow = response.getEntity(RegistrationWorkflowDto.class);
				LOG.info(MessageFormat.format("Workflow with id [{0}] created: {1}", workflow.getId(), new ObjectMapper().writeValueAsString(workflow)));

				WORKFLOW_UTILS.verifyStatus(workflow, Host.HD4DP, WorkflowStatus.IN_PROGRESS);
				workflows.get(Host.HD4DP).add(workflow);
			}
		} else {
			for (RegistrationWorkflowDto workflow : workflows.get(Host.HD4DP)) {
				requestBody.put("workflowId", workflow.getId().toString());

				final RegistrationWorkflowDto w = WORKFLOW_UTILS.getWorkflow(Host.HD4DP, workflow.getIdAsLong());
				final WorkflowStatus expectedEndStatus = w.getStatus(); // can be IN_PROGRESS on ACTION_NEEDED

				WORKFLOW_UTILS.executeAction(requestBody);
				WORKFLOW_UTILS.verifyStatus(workflow, Host.HD4DP, expectedEndStatus);
				verifyDocumentContent(Host.HD4DP, workflow, content.toString().getBytes(StandardCharsets.UTF_8));
			}
		}
	}

	private void delete() throws Exception {
		for (RegistrationWorkflowDto workflow : workflows.get(Host.HD4DP)) {
			WORKFLOW_UTILS.deleteWorkflow(workflow.getIdAsLong());
		}
		waitForWorkflowsBeingDeleted(Host.HD4DP);
	}

	private void submitAnnotations() throws Exception {
		for (RegistrationWorkflowDto workflow : workflows.get(Host.HD4RES)) {

			final JSONObject requestBody = new JSONObject();
			requestBody.put("action", "SUBMIT_ANNOTATIONS");
			requestBody.put("workflowId", workflow.getId().toString());

			WORKFLOW_UTILS.executeAction(requestBody);

			WORKFLOW_UTILS.verifyStatus(workflow, Host.HD4RES, WorkflowStatus.OUTBOX, WorkflowStatus.CORRECTIONS_REQUESTED);
		}
		waitForStatusBeingUpdated(Host.HD4RES, WorkflowStatus.CORRECTIONS_REQUESTED);
	}

	private void addNote(String content) throws Exception {
		final CommentDto comment = new CommentDto();
		comment.setContent(content);
		final SortedSet<CommentDto> comments = new TreeSet<>();
		comments.add(comment);

		final NoteDto note = new NoteDto();
		note.setFieldPath("favourite_color");
		note.setComments(comments);

		for (RegistrationWorkflowDto workflow : workflows.get(Host.HD4RES)) {
			final WebResource wr = TEST_UTILS.createWebResource("{0}/notes?workflowType=registration&workflowId={1}", Host.HD4RES, workflow.getId());
			final ClientResponse response = wr
				.header("Authorization", "Bearer " + TEST_UTILS.getAccessToken(Host.HD4RES, HealthDataRole.USER))
				.type(MediaType.APPLICATION_JSON)
				.post(ClientResponse.class, note);
			Assert.assertEquals(ClientResponse.Status.OK.getStatusCode(), response.getStatus());
		}
	}

	private void verifyNoteOnHd4dp() {
		final Host host = Host.HD4DP;

		for (RegistrationWorkflowDto workflow : workflows.get(host)) {
			final WebResource wr = TEST_UTILS.createWebResource("{0}/notes?workflowType=registration&workflowId={1}", host, workflow.getId());
			final ClientResponse response = wr
					.header("Authorization", "Bearer " + TEST_UTILS.getAccessToken(host, HealthDataRole.USER))
					.type(MediaType.APPLICATION_JSON)
					.get(ClientResponse.class);
			Assert.assertEquals(ClientResponse.Status.OK.getStatusCode(), response.getStatus());
			final List<NoteDto> notes = response.getEntity(new GenericType<List<NoteDto>>() {});

			Assert.assertEquals(1, notes.size());
			final NoteDto note = notes.get(0);

			Assert.assertEquals(1, note.getComments().size());
			Assert.assertEquals(COMMENT, note.getComments().first().getContent());
		}
	}

	private void resubmitWorkflowToDwh() throws Exception {
		LOG.info("resubmitting workflows to DWH");
		for (RegistrationWorkflowDto workflow : workflows.get(Host.HD4RES)) {

			final JSONObject requestBody = new JSONObject();
			requestBody.put("action", "RESUBMIT");
			requestBody.put("workflowId", workflow.getId().toString());

			WORKFLOW_UTILS.executeAction(requestBody);

			WORKFLOW_UTILS.verifyStatus(workflow, Host.HD4RES, WorkflowStatus.NEW);
		}
	}

	private void bulkSubmit() throws InterruptedException {
		LOG.info("submitting {} workflows in bulk", workflows.size());
		final List<Thread> threads = new ArrayList<>();

		for (int index = 0; index < workflows.get(Host.HD4DP).size(); ++index) {
			final int workflowIndex = index;
			final Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						WORKFLOW_UTILS.submit(workflows.get(Host.HD4DP).get(workflowIndex));
					} catch (Exception e) {
						LOG.error(e.getMessage(), e);
					}
				}
			});
			threads.add(t);
		}

		LOG.info("starting threads");
		for (final Thread t : threads) {
			t.start();
		}

		LOG.info("waiting for all threads to terminate");
		while (!allTerminated(threads)) {
			TEST_UTILS.waitBetweenTrials();
		}
	}

	private boolean allTerminated(List<Thread> threads) {
		for (final Thread t : threads) {
			if (!State.TERMINATED.equals(t.getState())) {
				return false;
			}
		}
		return true;
	}

	private void sendResendMessagesAction() throws Exception {
		LOG.info("sending ResendMessageAction from HD4RES to HD4DP");
		final String identificationValue = CONFIGURATION_UTILS.getConfiguration(Host.HD4DP, ConfigurationKey.IDENTIFICATION_VALUE).getValue();
		final WebResource wr = TEST_UTILS.createWebResource("{0}/monitoring/messages/resend/{1}", Host.HD4RES, identificationValue);
		final ClientResponse response = wr.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer" + TEST_UTILS.getAccessToken(Host.HD4RES, HealthDataRole.ADMIN)).post(ClientResponse.class);
		Assert.assertEquals(ClientResponse.Status.OK.getStatusCode(), response.getStatus());
	}

	private void resendMessages(Host host) {
		LOG.info("resending messages on {}", host);
		final WebResource wr = TEST_UTILS.createWebResource("{0}/messages/resend", host);
		for(RegistrationWorkflowDto workflow : workflows.get(host)) {
			wr.queryParam("status", MessageStatus.UNACKNOWLEDGED.toString());
			wr.queryParam("workflowId", workflow.getId().toString());
			final ClientResponse response = wr.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer" + TEST_UTILS.getAccessToken(host, HealthDataRole.ADMIN)).post(ClientResponse.class);
			Assert.assertEquals(ClientResponse.Status.OK.getStatusCode(), response.getStatus());
		}
	}

	private void reprocessMessage(Host host, Long messageId) {
		LOG.info("reprocessing message [{}] on {}", messageId.toString(), host);
		final WebResource wr = TEST_UTILS.createWebResource("{0}/messages/reprocess?id={1}", host, messageId.toString());
		final ClientResponse response = wr.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer" + TEST_UTILS.getAccessToken(host, HealthDataRole.ADMIN)).accept(MediaType.APPLICATION_JSON_TYPE).post(ClientResponse.class);
		Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	}

	private Long getLastMessageProcessedWithError(Host host) {
		LOG.info("retrieving last message processed with status {}", MessageStatus.ERROR_PROCESSING);
		final List<Message> messages = getMessages(host, MessageStatus.ERROR_PROCESSING);
		Collections.sort(messages, new Comparator<Message>() {
			@Override
			public int compare(Message m1, Message m2) {
				return m2.getSentOn().compareTo(m1.getSentOn());
			}
		});
		return messages.get(0).getId();
	}

	private List<Message> getMessages(Host host, MessageStatus status) {
		LOG.info("retrieving messages with status {} on {}", status, host);
		final WebResource wr = TEST_UTILS.createWebResource("{0}/messages?status={1}", host, status.toString());
		return wr.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer" + TEST_UTILS.getAccessToken(host, HealthDataRole.ADMIN)).accept(MediaType.APPLICATION_JSON_TYPE).get(new GenericType<List<Message>>() {});
	}

	private void waitForWorkflowsBeingDeleted(Host host) throws Exception {
		for (RegistrationWorkflowDto workflow : workflows.get(host)) {
			LOG.info(MessageFormat.format("waiting for workflow {0} being deleted on {1}", workflow.getId(), host));
			boolean deleted = false;
			for(int i=0; i< TestUtils.MAX_TRIALS && !deleted; ++i) {
				try {
					WORKFLOW_UTILS.getWorkflow(host, workflow.getIdAsLong(), false, false, ClientResponse.Status.NOT_FOUND.getStatusCode());
					deleted = true;
				} catch (AssertionError e) {
					TEST_UTILS.waitBetweenTrials();
				}
			}
			if(!deleted) {
				throw new Exception(MessageFormat.format("expected workflow {0} being deleted on {1} but it was not.", workflow.getId(), host));
			}
		}
	}

	private void waitForMessageToBeProcessedWithError(Host host, int nbErrorMessages) throws Exception {
		LOG.info(MessageFormat.format("waiting for message to be processed (with failure) on {0}", host));

		for (int i=1; i <= TestUtils.MAX_TRIALS; ++i) {
			final int currentNbErrorMessages = getMessages(host, MessageStatus.ERROR_PROCESSING).size();
			if (currentNbErrorMessages >= nbErrorMessages + 1) {
				return;
			}
			TEST_UTILS.waitBetweenTrials();
		}
		throw new Exception("no additional error message detected");
	}

	private void waitForWorkflowsToBeCreatedOnHd4res() throws Exception {
		for (RegistrationWorkflowDto hd4dpWorkflow : workflows.get(Host.HD4DP)) {
			RegistrationWorkflowDto workflow = TEST_UTILS.waitForWorkflowToBeCreatedOnHd4res(hd4dpWorkflow);
			workflows.get(Host.HD4RES).add(workflow);
		}
	}

	private void waitForStatusBeingUpdated(Host host, WorkflowStatus expectedStatus) throws Exception {
		waitForStatusBeingUpdated(host, expectedStatus, false);
	}

	private void waitForStatusBeingUpdated(Host host, WorkflowStatus expectedStatus, boolean async) throws Exception {
		for(RegistrationWorkflowDto workflow : workflows.get(host)) {
			LOG.info(MessageFormat.format("waiting for workflow (id: {0}) status on {1} to become {2}", workflow.getId(), host, expectedStatus));

			RegistrationWorkflowDto retrievedWorkflow = null;
			boolean statusOk = false;
			for (int i=1; i <= TestUtils.MAX_TRIALS && !statusOk; ++i) {
				retrievedWorkflow = WORKFLOW_UTILS.getWorkflow(host, workflow.getIdAsLong());
				// LOG.info(MessageFormat.format("workflow (id: {0}) status = {1}", workflow.getId(), retrievedWorkflow.getStatus()));

				statusOk = expectedStatus.equals(retrievedWorkflow.getStatus());
				if (!statusOk) {
					TEST_UTILS.waitBetweenTrials();

					if(async && i%5 == 0) {
						LOG.info("Connection is ASYNCHRONOUS, and status seems to getConfiguration not updated. Retransfering messages.");
						final Platform src = (Host.HD4DP.equals(host)) ? Platform.HD4RES : Platform.HD4DP;
						final Platform dst = (Host.HD4DP.equals(host)) ? Platform.HD4DP : Platform.HD4RES;
						TEST_UTILS.transferAsyncMessages(src, dst);
					}
				}

			}

			if(!statusOk) {
				throw new Exception(MessageFormat.format("expected workflow status {0} for workflow {1} on {2} but was {3}", expectedStatus, retrievedWorkflow.getId(), host, retrievedWorkflow.getStatus()));
			}
		}
	}

	private void waitWorkflowsHaveSendStatus(Host host, String expectedStatus) throws Exception {
		LOG.info("waiting for workflows to have send status equal to {}", expectedStatus);
		for (RegistrationWorkflowDto workflow : workflows.get(host)) {
			boolean sendStatusOk = false;
			RegistrationWorkflowDto workflowDto = null;
			for(int i=0; i< TestUtils.MAX_TRIALS && !sendStatusOk; ++i) {
				workflowDto = WORKFLOW_UTILS.getWorkflow(host, workflow.getIdAsLong());
				sendStatusOk = (expectedStatus.equals(workflowDto.getSendStatus()));
				if(!sendStatusOk) {
					TEST_UTILS.waitBetweenTrials();
				}
			}
			if(!sendStatusOk) {
				throw new Exception(MessageFormat.format("expected workflow send status {0} for workflow {1} on {2} but was {3}", expectedStatus, workflowDto.getId(), host, workflowDto.getSendStatus()));
			}
		}
	}

	private void waitFor(String message, int seconds) throws InterruptedException {
		LOG.info(MessageFormat.format("waiting for {0}: {1} seconds", message, seconds));
		Thread.sleep(seconds * 1000);
	}

	private int getAndVerifyIntegerConfigurationValue(Host host, ConfigurationKey key, int minValue, int maxValue) throws Exception {
		final int value = Integer.valueOf(CONFIGURATION_UTILS.getConfiguration(host, key).getValue());
		if(value < minValue) {
			throw new Exception(MessageFormat.format("{0} configuration value on {1} is too small ({2}). A larger value (>= {3}) should be setConfiguration.", key, host, value, minValue));
		}
		if(value > maxValue) {
			throw new Exception(MessageFormat.format("{0} configuration value on {1} is too large ({2}). A smaller value (<= {3}) should be setConfiguration to run the tests in a reasonable amount of time.", key, host, value, maxValue));
		}
		return value;
	}

	private void verifyHistoryContainsExactly(Host host, WorkflowStatus status, int expectedCount) throws Exception {
		int count = 0;
		for (RegistrationWorkflowDto workflow : workflows.get(host)) {
			final RegistrationWorkflowDto retrievedWorkflow = WORKFLOW_UTILS.getWorkflow(host, workflow.getIdAsLong());
			final SortedSet<RegistrationWorkflowHistory> history = retrievedWorkflow.getHistory();
			for (RegistrationWorkflowHistory workflowHistory : history) {
				if(status.equals(workflowHistory.getNewStatus())) {
					count++;
				}
			}
		}
		Assert.assertEquals(expectedCount, count);
	}

	private void verifyHistoryDoesNotContain(Host host, WorkflowStatus status) throws Exception {
		for (RegistrationWorkflowDto workflow : workflows.get(host)) {
			final RegistrationWorkflowDto retrievedWorkflow = WORKFLOW_UTILS.getWorkflow(host, workflow.getIdAsLong());
			final SortedSet<RegistrationWorkflowHistory> history = retrievedWorkflow.getHistory();
			for (RegistrationWorkflowHistory workflowHistory : history) {
				if(status.equals(workflowHistory.getNewStatus())) {
					Assert.assertTrue(false);
				}
			}
		}
	}

	private void approve() throws Exception {
		simulateDataWarehouseProcess(ACCEPT);
		waitForStatusBeingUpdated(Host.HD4RES, WorkflowStatus.APPROVED);
		waitForStatusBeingUpdated(Host.HD4DP, WorkflowStatus.APPROVED);
	}

	private be.wiv_isp.healthdata.orchestration.domain.FollowUp getFollowUp(List<be.wiv_isp.healthdata.orchestration.domain.FollowUp> followUps, String label) {
		for (be.wiv_isp.healthdata.orchestration.domain.FollowUp followUp : followUps) {
			if(followUp.getName().equals(label)) {
				return followUp;
			}
		}

		return null;
	}
}