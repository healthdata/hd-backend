/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.integration_tests.endtoend;

import be.wiv_isp.healthdata.integration_tests.domain.StatusResponse;
import be.wiv_isp.healthdata.integration_tests.endtoend.categories.Status;
import be.wiv_isp.healthdata.integration_tests.hd4dp.UserConfigurationRestServiceIT;
import be.wiv_isp.healthdata.integration_tests.utils.TestUtils;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.ws.rs.core.MediaType;

public class StatusScenario {

	private static TestUtils TEST_UTILS = new TestUtils();

	@BeforeClass
	public static void init() throws Exception {
		TEST_UTILS.initialize(UserConfigurationRestServiceIT.class.getResource("/test.it.properties"));
	}

	@Test
	@Category(Status.class)
	public void testStatusHd4dp() throws Exception {
		checkStatus(Host.HD4DP);
	}

	@Test
	@Category(Status.class)
	public void testStatusHd4res() throws Exception {
		checkStatus(Host.HD4RES);
	}

	private void checkStatus(Host host) {
		final WebResource wr = TEST_UTILS.createWebResource("{0}/status", host);
		final ClientResponse response = wr
				.accept(MediaType.APPLICATION_JSON_TYPE)
				.get(ClientResponse.class);

		Assert.assertEquals(ClientResponse.Status.OK.getStatusCode(), response.getStatus());

		final StatusResponse statusResponse = response.getEntity(StatusResponse.class);
		Assert.assertEquals(StatusResponse.UP, statusResponse.getDatabase());
		Assert.assertEquals(StatusResponse.UP, statusResponse.getCatalogue());
		Assert.assertEquals(StatusResponse.UP, statusResponse.getMapping());
		Assert.assertEquals(StatusResponse.UP, statusResponse.getWorkflow());
		Assert.assertEquals(StatusResponse.UP, statusResponse.getElasticsearch());
		Assert.assertEquals(StatusResponse.UP, statusResponse.getMailServer());
	}
}
