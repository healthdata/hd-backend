/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.integration_tests.hd4dp;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.integration_tests.domain.SerializableUserDto;
import be.wiv_isp.healthdata.integration_tests.utils.OrganizationUtils;
import be.wiv_isp.healthdata.integration_tests.utils.TestUtils;
import be.wiv_isp.healthdata.integration_tests.utils.UserUtils;
import be.wiv_isp.healthdata.integration_tests.utils.enums.HealthDataRole;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.dto.UserDto;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.util.*;

public class UserRestServiceIT {

	private static final Logger LOG = LoggerFactory.getLogger(UserRestServiceIT.class);

	private static final String METADATA_KEY = "METADATA_KEY";

	private static TestUtils TEST_UTILS;
	private static UserUtils USER_UTILS;
	private static OrganizationUtils ORGANIZATION_UTILS;

	@BeforeClass
	public static void init() throws Exception {
		TEST_UTILS = new TestUtils();
		TEST_UTILS.initialize(UserRestServiceIT.class.getResource("/test.it.properties"));
		USER_UTILS = TEST_UTILS.getUserUtils();
		ORGANIZATION_UTILS = TEST_UTILS.getOrganizationUtils();
	}

	@Test
	public void testCreateRoleAdmin() throws Exception {
		UserDto user = createUser();
		final Organization mainOrganization = ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);
		Assert.assertNotNull(createdUser.getId());
		compareUsersWithoutIdAndPassword(user, createdUser);
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	@Test
	public void testCreateRoleUser() throws Exception {
		final UserDto user = createUser();
		final ClientResponse response = USER_UTILS.createUserReturnResponse(user, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER));
		Assert.assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreateDuplicateUserReturnsConflict() throws Exception {
		UserDto user = createUser();
		final Organization mainOrganization = ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);
		Assert.assertNotNull(createdUser.getId());
		ClientResponse response = USER_UTILS.createUserReturnResponse(user, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN));
		Assert.assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	@Test
	public void testGetUserRoleAdmin() throws Exception {
		final UserDto user = createUser();
		final Organization mainOrganization = ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);
		final Long userId = createdUser.getId();
		Assert.assertNotNull(createdUser);
		createdUser.setPassword(null);
		final UserDto retrievedUser = USER_UTILS.getUser(mainOrganization.getId(), userId);
		Assert.assertEquals(createdUser, retrievedUser);
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	@Test
	public void testGetUserRoleUserIsForbidden() throws Exception {
		final UserDto user = createUser();
		final Organization mainOrganization = ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);
		USER_UTILS.getUser(mainOrganization.getId(), createdUser.getId(), TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER), Response.Status.FORBIDDEN.getStatusCode());
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	@Test
	public void testGetAllRoleUserIsForbidden() throws Exception {
		final WebResource wr = TEST_UTILS.createWebResource("{0}/users", Host.HD4DP);
		ClientResponse response = wr.accept(MediaType.APPLICATION_JSON_TYPE).header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER)).get(new GenericType<ClientResponse>() {});
		Assert.assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
	}

	@Test
	public void testGetNonExistingUserRoleAdminReturnsNotFound() throws Exception {
		final WebResource wr = TEST_UTILS.createWebResource("{0}/users/{1}", Host.HD4DP, "-1");
		Assert.assertEquals(Response.Status.NOT_FOUND.getStatusCode(), wr.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN)).head().getStatus());
	}

	@Test
	public void testGetNonExistingUserRoleUserIsForbidden() throws Exception {
		final WebResource wr = TEST_UTILS.createWebResource("{0}/users/{1}", Host.HD4DP, "-1");
		Assert.assertEquals(Response.Status.FORBIDDEN.getStatusCode(), wr.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER)).head().getStatus());
	}

	@Test
	public void testResetPasswordRoleAdmin() throws Exception {
		final String new_password = "new_password";

		final UserDto user = createUser();
		final Organization mainOrganization = ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);
		createdUser.setPassword(new_password);
		USER_UTILS.updateUser(createdUser.getId(), createdUser, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN));

		TEST_UTILS.getAccessToken(Host.HD4DP, mainOrganization.getId(), user.getUsername(), new_password);
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	@Test
	public void testUpdateUserWithPasswordNullHasNoEffect() throws Exception {
		final UserDto user = createUser();
		final Organization mainOrganization = ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);
		createdUser.setPassword(null);
		USER_UTILS.updateUser(createdUser.getId(), createdUser, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN));

		TEST_UTILS.getAccessToken(Host.HD4DP, mainOrganization.getId(), user.getUsername(), user.getPassword());
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	@Test
	public void testUpdateNonExistingUserRoleAdminReturnsNotFound() throws Exception {
		final UserDto user = createUser();
		user.setPassword("new_password");
		final ClientResponse response = USER_UTILS.updateUserReturnResponse(-1L, user, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN));
		Assert.assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testUpdateUserRoleUserIsForbidden() throws Exception {
		final String updatedPassword = "new_password";
		final UserDto user = createUser();
		final Organization mainOrganization = ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);
		final Long userId = createdUser.getId();
		user.setPassword(updatedPassword);

		final ClientResponse response = USER_UTILS.updateUserReturnResponse(userId, user, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER));
		Assert.assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	@Test
	public void testPasswordUpdateRoleUserNonExistingUser() throws Exception {
		final String newPassword = "new_password";
		UserDto user = createUser();
		final WebResource wr = TEST_UTILS.createWebResource("{0}/users/password/{1}", Host.HD4DP, "-1");
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		queryParams.add("oldPassword", user.getPassword());
		queryParams.add("newPassword", newPassword);
		ClientResponse response = wr.queryParams(queryParams) //
				.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER)) //
				.type(MediaType.APPLICATION_JSON_TYPE) //
				.accept(MediaType.APPLICATION_JSON_TYPE) //
				.put(ClientResponse.class, user);
		Assert.assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testPasswordUpdateRoleUserNonMatchingPassword() throws Exception {
		final String newPassword = "new_password";
		final UserDto user = createUser();
		final Organization mainOrganization = ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);
		final Long userId = createdUser.getId();
		final WebResource wr = TEST_UTILS.createWebResource("{0}/users/{1}", Host.HD4DP, String.valueOf(userId));
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		queryParams.add("oldPassword", "non_matching_password");
		final String accessTokenUser = TEST_UTILS.getAccessToken(Host.HD4DP, mainOrganization.getId(), user.getUsername(), user.getPassword());
		user.setPassword(newPassword);
		final ClientResponse response = wr
				.queryParams(queryParams)
				.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + accessTokenUser)
				.type(MediaType.APPLICATION_JSON_TYPE)
				.accept(MediaType.APPLICATION_JSON_TYPE)
				.put(ClientResponse.class, user);
		Assert.assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	@Test
	public void testPasswordUpdateRoleUserNonMatchingUsername() throws Exception {
		final String newPassword = "new_password";
		final UserDto user = createUser();
		final Organization mainOrganization = ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);
		final Long userId = createdUser.getId();
		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		queryParams.add("oldPassword", user.getPassword());
		final String accessTokenUser = TEST_UTILS.getAccessToken(Host.HD4DP, mainOrganization.getId(), user.getUsername(), user.getPassword());
		user.setPassword(newPassword);
		final ClientResponse response = USER_UTILS.updateUserReturnResponse(userId, user, accessTokenUser);

		Assert.assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	@Test
	public void testResetPasswordRoleUser() throws Exception {
		final String newPassword = "new_password";

		final UserDto user = createUser();
		final Organization mainOrganization = ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);
		final Long userId = createdUser.getId();
		final String accessToken = TEST_UTILS.getAccessToken(Host.HD4DP, mainOrganization.getId(), user.getUsername(), user.getPassword());
		USER_UTILS.updatePassword(userId, user.getPassword(), newPassword, accessToken);

		TEST_UTILS.getAccessToken(Host.HD4DP, mainOrganization.getId(), user.getUsername(), newPassword);
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	@Test
	public void testUpdateMetadataRoleUser() throws Exception {
		final UserDto user = createUser();
		final Organization mainOrganization = ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);
		final Long userId = createdUser.getId();

		final Map<String, String> metaData = new HashMap<>();
		metaData.put(METADATA_KEY, Boolean.TRUE.toString());
		metaData.put("ANOTHER_META_DATA_KEY", "ANOTHER_META_DATA_VALUE");

		final String accessTokenUser = TEST_UTILS.getAccessToken(Host.HD4DP, mainOrganization.getId(), user.getUsername(), user.getPassword());
		final UserDto updatedUser = USER_UTILS.updateMetadata(userId, metaData, accessTokenUser);
		Assert.assertEquals(metaData, updatedUser.getMetaData());
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	@Test
	public void testUpdateMetadataRoleUserMultiThreads() throws Exception {
		final UserDto user = createUser();
		final Organization mainOrganization = ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);
		final Long userId = createdUser.getId();

		final Map<String, String> metaData = new HashMap<>();
		metaData.put(METADATA_KEY, Boolean.TRUE.toString());
		metaData.put("ANOTHER_META_DATA_KEY", "ANOTHER_META_DATA_VALUE");

		final String accessTokenUser = TEST_UTILS.getAccessToken(Host.HD4DP, mainOrganization.getId(), user.getUsername(), user.getPassword());


		final List<CustomThread> threads = new ArrayList<>();

		for (int i = 0; i < 10; i++) {
			threads.add(new CustomThread(userId, metaData, accessTokenUser));
		}

		for (CustomThread thread : threads) {
			thread.start();
		}

		LOG.info("waiting for all threads to terminate");
		while (!allTerminated(threads)) {
			Thread.sleep(1000);
		}

		for (CustomThread thread : threads) {
			Assert.assertTrue(thread.isSuccess());
			Assert.assertEquals(metaData, thread.getUpdatedUser().getMetaData());
		}

		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	public class CustomThread extends Thread {

		private boolean success;
		private UserDto updatedUser;

		private Long userId;
		private Map<String, String> metaData;
		private String accessTokenUser;

		public CustomThread(Long userId, Map<String, String> metaData, String accessTokenUser) {
			this.userId = userId;
			this.metaData = metaData;
			this.accessTokenUser = accessTokenUser;
		}

		@Override
		public void run() {
			try {
				updatedUser = USER_UTILS.updateMetadata(userId, metaData, accessTokenUser);
				success = true;
			} catch (Exception e) {
				success = false;
			}
		}

		public boolean isSuccess() {
			return success;
		}

		public UserDto getUpdatedUser() {
			return updatedUser;
		}
	}

	private boolean allTerminated(List<CustomThread> threads) {
		for (final Thread t : threads) {
			if (!Thread.State.TERMINATED.equals(t.getState())) {
				return false;
			}
		}
		return true;
	}

	@Test
	public void testUpdateMetadataForNonExisitingUserReturnsNotFound() throws Exception {
		final Map<String, String> metaData = new HashMap<>();
		metaData.put(METADATA_KEY, Boolean.TRUE.toString());
		metaData.put("ANOTHER_META_DATA_KEY", "ANOTHER_META_DATA_VALUE");

		final ClientResponse response = USER_UTILS.updateMetadataReturnResponse(-1L, metaData, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER));
		Assert.assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testUpdateMetadataForUserWithNonMatchingUsernameReturnsUnauthorized() throws Exception {
		final UserDto user = createUser();
		final Organization mainOrganization = ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);

		final Map<String, String> metaData = new HashMap<>();
		metaData.put(METADATA_KEY, Boolean.TRUE.toString());
		metaData.put("ANOTHER_META_DATA_KEY", "ANOTHER_META_DATA_VALUE");

		final ClientResponse response = USER_UTILS.updateMetadataReturnResponse(createdUser.getId(), metaData, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER));
		Assert.assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(), response.getStatus());
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	@Test
	public void testUpdateMetadataRoleAdmin() throws Exception {
		final UserDto admin = createAdmin();
		final Organization mainOrganization = ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP);
		final UserDto createdAdmin = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), admin);

		Map<String, String> metaData = new HashMap<>();
		metaData.put(METADATA_KEY, Boolean.TRUE.toString());
		metaData.put("ANOTHER_META_DATA_KEY", "ANOTHER_META_DATA_VALUE");
		admin.setMetaData(metaData);

		final String accessTokenNewAdmin = TEST_UTILS.getAccessToken(Host.HD4DP, mainOrganization.getId(), admin.getUsername(), admin.getPassword());
		final UserDto updatedAdmin = USER_UTILS.updateMetadata(createdAdmin.getId(), metaData, accessTokenNewAdmin);
		Assert.assertEquals(metaData, updatedAdmin.getMetaData());
		USER_UTILS.deleteUser(Host.HD4DP, createdAdmin);
	}

	@Test
	public void testDeleteRoleAdmin() throws Exception {
		final UserDto user = createUser();
		final Organization mainOrganization = ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);
		createdUser.setPassword(null);
		final UserDto retrievedUser = USER_UTILS.getUser(mainOrganization.getId(), createdUser.getId());
		Assert.assertEquals(retrievedUser, createdUser);
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
		USER_UTILS.getUser(mainOrganization.getId(), createdUser.getId(), TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN), Response.Status.NOT_FOUND.getStatusCode());
	}

	@Test
	public void testDeleteRoleUserIsForbidden() throws Exception {
		final UserDto user = createUser();
		final Organization mainOrganization = ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);
		USER_UTILS.deleteUser(Host.HD4DP, createdUser, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER), Response.Status.FORBIDDEN.getStatusCode());
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	@Test
	public void testDeleteNonExistingUserRoleAdminReturnsNotFound() throws Exception {
		final UserDto user = new UserDto();
		user.setId(-1L);
		user.setOrganization(ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP));
		USER_UTILS.deleteUser(Host.HD4DP, user, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN), Response.Status.NOT_FOUND.getStatusCode());
	}

	@Test
	public void testDeleteNonExistingUserRoleUserIsForbidden() throws Exception {
		final UserDto user = new UserDto();
		user.setId(-1L);
		user.setOrganization(ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP));
		USER_UTILS.deleteUser(Host.HD4DP, user, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER), Response.Status.FORBIDDEN.getStatusCode());
	}

	@Test
	public void testGetAllWithPagination() throws Exception {
		final int injectedUserCount = 10;

		final WebResource wr = TEST_UTILS.createWebResource("{0}/users", Host.HD4DP);
		final List<SerializableUserDto> usersBefore = wr
				.type(MediaType.APPLICATION_JSON_TYPE)
				.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN))
				.get(new GenericType<List<SerializableUserDto>>() {});
		final int nbUsersTotal = usersBefore.size() + injectedUserCount;

		final Set<UserDto> users = new HashSet<>();
		for (int i = 1; i <= injectedUserCount; i++) {
			final UserDto user = createUser();
			final Organization mainOrganization = ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP);
			final UserDto created = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);
			users.add(created);
		}

		try {
			// first page (items 1 to 5)
			ClientResponse response = wr.accept(MediaType.APPLICATION_JSON_TYPE).header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN)).header(HttpHeaders.RANGE.getValue(), "items=1-5").get(new GenericType<ClientResponse>() {});
			Assert.assertEquals(206, response.getStatus());
			String contentRangeHeader = response.getHeaders().get(HttpHeaders.CONTENT_RANGE.getValue()).iterator().next();
			Assert.assertEquals(String.format("items %d-%d/%d", 1, 5, nbUsersTotal), contentRangeHeader);
			List<SerializableUserDto> returnedUsers = response.getEntity(new GenericType<List<SerializableUserDto>>() {});
			Assert.assertNotNull(returnedUsers);
			Assert.assertEquals(5, returnedUsers.size());

			// first page (items 6 to 10)
			response = wr.accept(MediaType.APPLICATION_JSON_TYPE).header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN)).header(HttpHeaders.RANGE.getValue(), "items=6-10").get(new GenericType<ClientResponse>() {});
			Assert.assertEquals(206, response.getStatus());
			contentRangeHeader = response.getHeaders().get(HttpHeaders.CONTENT_RANGE.getValue()).iterator().next();
			Assert.assertEquals(String.format("items %d-%d/%d", 6, 10, nbUsersTotal), contentRangeHeader);
			returnedUsers = response.getEntity(new GenericType<List<SerializableUserDto>>() {});
			Assert.assertNotNull(returnedUsers);
			Assert.assertEquals(5, returnedUsers.size());
		} finally {
			for (UserDto user : users) {
				USER_UTILS.deleteUser(Host.HD4DP, user);
			}
		}
	}

	@Test
	public void testGetMe() throws Exception {
		final UserDto user = createUser();
		final Organization mainOrganization = ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);
		final String accessTokenUser = TEST_UTILS.getAccessToken(Host.HD4DP, mainOrganization.getId(), user.getUsername(), user.getPassword());
		final UserDto returnedUser = USER_UTILS.getMe(Host.HD4DP, accessTokenUser);
		compareUsersWithoutIdAndPassword(createdUser, returnedUser);
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	@Test
	public void testCORSHeadersAvailability() throws Exception {
		final UserDto user = createUser();
		final ClientResponse response = USER_UTILS.createUserReturnResponse(user, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN));
		final UserDto createdUser = response.getEntity(new GenericType<UserDto>() {});
		final MultivaluedMap<String, String> headerMap = response.getHeaders();
		Assert.assertFalse(headerMap.isEmpty());
		Assert.assertEquals("*", headerMap.getFirst("Access-Control-Allow-Origin"));
		Assert.assertEquals("GET, POST, PUT, DELETE, OPTIONS", headerMap.getFirst("Access-Control-Allow-Methods"));
		Assert.assertEquals("10", headerMap.getFirst("Access-Control-Max-Age"));
		Assert.assertTrue(StringUtils.isEmpty(headerMap.getFirst("Access-Control-Request-Headers")));
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	@Test
	public void testCORSHeadersUnauthorized() throws Exception {
		final UserDto user = createUser();
		final ClientResponse response = USER_UTILS.createUserReturnResponse(user, null);

		Assert.assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(), response.getStatus());
		final MultivaluedMap<String, String> headerMap = response.getHeaders();
		Assert.assertFalse(headerMap.isEmpty());
		Assert.assertEquals("*", headerMap.getFirst("Access-Control-Allow-Origin"));
		Assert.assertEquals("GET, POST, PUT, DELETE, OPTIONS", headerMap.getFirst("Access-Control-Allow-Methods"));
		Assert.assertEquals("10", headerMap.getFirst("Access-Control-Max-Age"));
		Assert.assertTrue(StringUtils.isEmpty(headerMap.getFirst("Access-Control-Request-Headers")));
	}

	@Test
	public void testCORSHeadersForbidden() throws Exception {
		final UserDto user = createUser();
		final ClientResponse response = USER_UTILS.createUserReturnResponse(user, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER));
		Assert.assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
		final MultivaluedMap<String, String> headerMap = response.getHeaders();
		Assert.assertFalse(headerMap.isEmpty());
		Assert.assertEquals("*", headerMap.getFirst("Access-Control-Allow-Origin"));
		Assert.assertEquals("GET, POST, PUT, DELETE, OPTIONS", headerMap.getFirst("Access-Control-Allow-Methods"));
		Assert.assertEquals("10", headerMap.getFirst("Access-Control-Max-Age"));
		Assert.assertTrue(StringUtils.isEmpty(headerMap.getFirst("Access-Control-Request-Headers")));
	}

	@Test
	public void testCORSHeadersOptions() throws Exception {
		final ClientResponse response = getUserOptions(1L, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER));
		final MultivaluedMap<String, String> headerMap = response.getHeaders();
		Assert.assertFalse(headerMap.isEmpty());
		Assert.assertEquals("*", headerMap.getFirst("Access-Control-Allow-Origin"));
		Assert.assertEquals("GET, POST, PUT, DELETE, OPTIONS", headerMap.getFirst("Access-Control-Allow-Methods"));
		Assert.assertEquals("10", headerMap.getFirst("Access-Control-Max-Age"));
		Assert.assertTrue(StringUtils.isEmpty(headerMap.getFirst("Access-Control-Request-Headers")));
	}

	@Test
	public void testModifyingUserRoleIsNotAllowed() throws Exception {
		final UserDto user = createUser();
		final Organization mainOrganization = ORGANIZATION_UTILS.getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);

		createdUser.getAuthorities().clear();
		createdUser.getAuthorities().add(new Authority(Authority.ADMIN));

		final ClientResponse response = USER_UTILS.updateUserReturnResponse(createdUser.getId(), createdUser, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN));
		Assert.assertEquals(ClientResponse.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	private ClientResponse getUserOptions(final Long id, String token) {
		final WebResource wr = TEST_UTILS.createWebResource("{0}/users/{1}", Host.HD4DP, String.valueOf(id));
		return wr.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token).accept(MediaType.APPLICATION_JSON).options(ClientResponse.class);
	}

	private void compareUsersWithoutIdAndPassword(UserDto user, UserDto createdUser) {
		user.setId(createdUser.getId());
		user.setOrganization(createdUser.getOrganization());
		user.setPassword(null);
		createdUser.setPassword(null);
		Assert.assertEquals(user, createdUser);
	}

	private UserDto createUser() {
		final UserDto user = USER_UTILS.buildUser();
		user.setAuthorities(new HashSet<>(Collections.singletonList(Authority.USER_AUTHORITY)));
		return user;
	}

	private UserDto createAdmin() {
		final UserDto user = USER_UTILS.buildUser();
		user.setAuthorities(new HashSet<>(Collections.singletonList(Authority.ADMIN_AUTHORITY)));
		return user;
	}
}
