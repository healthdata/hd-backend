/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.integration_tests.hd4dp;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.integration_tests.utils.DataCollectionAccessRequestUtils;
import be.wiv_isp.healthdata.integration_tests.utils.TestUtils;
import be.wiv_isp.healthdata.integration_tests.utils.UserUtils;
import be.wiv_isp.healthdata.integration_tests.utils.enums.DataCollection;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import be.wiv_isp.healthdata.orchestration.dto.UserDto;
import org.codehaus.jettison.json.JSONException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataCollectionAccessRequestRestServiceIT {

	private static TestUtils TEST_UTILS;
	private static UserUtils USER_UTILS;
	private static DataCollectionAccessRequestUtils DATA_COLLECTION_ACCESS_REQUEST_UTILS;

	@BeforeClass
	public static void init() throws Exception {
		TEST_UTILS = new TestUtils();
		TEST_UTILS.initialize(UserRestServiceIT.class.getResource("/test.it.properties"));
		USER_UTILS = TEST_UTILS.getUserUtils();
		DATA_COLLECTION_ACCESS_REQUEST_UTILS = TEST_UTILS.getDataCollectionAccessRequestUtils();
	}

	@Test
	public void testRequestDataCollectionAccess() throws Exception {
		final UserDto user = USER_UTILS.buildUser();
		final Organization mainOrganization = TEST_UTILS.getOrganizationUtils().getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);
		Assert.assertFalse(createdUser.getDataCollectionNames().contains(DataCollection.TEST_A.toString()));
		final Long requestId = DATA_COLLECTION_ACCESS_REQUEST_UTILS.requestDataCollectionAccess(createdUser, Collections.singletonList(DataCollection.TEST_A));
		final Map<String, Boolean> decisions = new HashMap<>();
		decisions.put(DataCollection.TEST_A.toString(), true);
		DATA_COLLECTION_ACCESS_REQUEST_UTILS.postDataCollectionAccessRequestDecision(mainOrganization.getId(), requestId, decisions);
		final UserDto retrievedUser = USER_UTILS.getUser(mainOrganization.getId(), createdUser.getId());
		Assert.assertTrue(retrievedUser.getDataCollectionNames().contains(DataCollection.TEST_A.toString()));
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	@Test
	public void testAdminCanNotProcessRequestForOtherOrgnanization() throws JSONException {
		final UserDto user = USER_UTILS.buildUser();
		final Organization mainOrganization = TEST_UTILS.getOrganizationUtils().getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);
		Assert.assertFalse(createdUser.getDataCollectionNames().contains(DataCollection.TEST_A.toString()));
		final Long requestId = DATA_COLLECTION_ACCESS_REQUEST_UTILS.requestDataCollectionAccess(createdUser, Collections.singletonList(DataCollection.TEST_A));
		final Map<String, Boolean> decisions = new HashMap<>();
		decisions.put(DataCollection.TEST_A.toString(), true);

		final List<Organization> organizations = TEST_UTILS.getOrganizationUtils().getOrganizations(Host.HD4DP);
		Organization otherOrganization = null;
		for (Organization o : organizations) {
			if(!o.equals(mainOrganization)){
				otherOrganization = o;
				break;
			}
		}

		Assert.assertNotNull(otherOrganization);
		final Long organizationId = otherOrganization.getId();
		final String username = "admin";
		final String password = otherOrganization.getHealthDataIDValue();

		String accessToken = TEST_UTILS.getAccessToken(Host.HD4DP, organizationId, username, password);
		DATA_COLLECTION_ACCESS_REQUEST_UTILS.postDataCollectionAccessRequestDecision(mainOrganization.getId(), requestId, decisions, accessToken, Response.Status.UNAUTHORIZED.getStatusCode());
		final UserDto retrievedUser = USER_UTILS.getUser(mainOrganization.getId(), createdUser.getId());
		Assert.assertFalse(retrievedUser.getDataCollectionNames().contains(DataCollection.TEST_A.toString()));
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}
}