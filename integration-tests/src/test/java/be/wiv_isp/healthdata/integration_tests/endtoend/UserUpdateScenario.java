/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.integration_tests.endtoend;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.integration_tests.utils.enums.HealthDataRole;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.hd4res.domain.Hd4dpAuthority;
import be.wiv_isp.healthdata.hd4res.dto.Hd4dpUserDto;
import be.wiv_isp.healthdata.integration_tests.utils.TestUtils;
import be.wiv_isp.healthdata.integration_tests.utils.UserUtils;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.dto.UserDto;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserUpdateScenario {

	private static final Logger LOG = LoggerFactory.getLogger(UserUpdateScenario.class);

	private static TestUtils TEST_UTILS = new TestUtils();
	private static UserUtils USER_UTILS;

	final int NB_TRIALS = 20;
	final int WAITING_TIME_SECONDS = 10;

	@BeforeClass
	public static void init() throws Exception {
		TEST_UTILS.initialize(UserUpdateScenario.class.getResource("/test.it.properties"));
		USER_UTILS = new UserUtils(TEST_UTILS);
	}

	@Test
	public void test() throws Exception {
		final UserDto user = USER_UTILS.buildUser();
		user.setDataCollectionNames(new HashSet<>(Arrays.asList("TEST_A", "TEST_B")));
		final Organization mainOrganization = TEST_UTILS.getOrganizationUtils().getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);

		LOG.info("Verifying user is created on HD4RES");
		Hd4dpUserDto userOnHd4res = null;
		for (int i = 0; userOnHd4res == null && i < NB_TRIALS; i++) {
			sleep();
			userOnHd4res = getUserOnHd4res(createdUser);
		}
		Assert.assertTrue(compare(createdUser, userOnHd4res, true));

		LOG.info("Updating user on HD4DP");
		final long timestamp = System.currentTimeMillis();
		createdUser.setEmail("test_" + timestamp + "@test.com");
		createdUser.setFirstName("firstname_" + timestamp);
		createdUser.setLastName("lastname_" + timestamp);
		createdUser.getDataCollectionNames().clear();
		createdUser.getDataCollectionNames().add("TEST_A");
		createdUser.getDataCollectionNames().add("TEST_C");
		USER_UTILS.updateUser(createdUser.getId(), createdUser, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER));

		LOG.info("Verifying user is updated on HD4RES");
		userOnHd4res = null;
		for (int i = 0; !compare(user, userOnHd4res, false) && i < NB_TRIALS; i++) {
			sleep();
			userOnHd4res = getUserOnHd4res(user);
		}
		Assert.assertTrue(compare(user, userOnHd4res, true));

		LOG.info("Deleting user on HD4DP");
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);

		LOG.info("Verifying user is deleted on HD4RES");
		userOnHd4res = getUserOnHd4res(user);
		for (int i = 0; userOnHd4res != null && i < NB_TRIALS; i++) {
			sleep();
			userOnHd4res = getUserOnHd4res(user);
		}
		Assert.assertNull(userOnHd4res);
	}

	private Hd4dpUserDto getUserOnHd4res(UserDto user) {
		final WebResource wr = TEST_UTILS.createWebResource("{0}/hd4dpusers", Host.HD4RES);
		final ClientResponse response = wr
				.queryParam(RestUtils.ParameterName.HD4DP_ID, String.valueOf(user.getId()))
				.queryParam(RestUtils.ParameterName.IDENTIFICATION_TYPE, user.getOrganization().getHealthDataIDType())
				.queryParam(RestUtils.ParameterName.IDENTIFICATION_VALUE, user.getOrganization().getHealthDataIDValue())
				.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + TEST_UTILS.getAccessToken(Host.HD4RES, HealthDataRole.ADMIN)) //
				.accept(MediaType.APPLICATION_JSON) //
				.get(ClientResponse.class);


		Assert.assertEquals(ClientResponse.Status.OK.getStatusCode(), response.getStatus());

		final List<Hd4dpUserDto> users = response.getEntity(new GenericType<List<Hd4dpUserDto>>() {});

		if(users.isEmpty()) {
			return null;
		}

		Assert.assertEquals(1, users.size());
		return users.get(0);
	}

	private void sleep() throws InterruptedException {
		LOG.info("Sleeping for {} seconds", WAITING_TIME_SECONDS);
		Thread.sleep(WAITING_TIME_SECONDS * 1000);
	}

	private boolean compare(UserDto userOnHd4dp, Hd4dpUserDto userOnHd4res, boolean loggingEnabled) {
		if(loggingEnabled) {
			LOG.info("Comparing user on HD4DP and HD4RES");
		}
		if(userOnHd4dp == null) {
			if(loggingEnabled) {
				LOG.error("userOnHd4dp is null");
			}
			return false;
		}
		if(userOnHd4res == null) {
			if(loggingEnabled) {
				LOG.error("userOnHd4res is null");
			}
			return false;
		}
		try {
			Assert.assertEquals(userOnHd4dp.getUsername(), userOnHd4res.getUsername());
			Assert.assertEquals(userOnHd4dp.getLastName(), userOnHd4res.getLastName());
			Assert.assertEquals(userOnHd4dp.getFirstName(), userOnHd4res.getFirstName());
			Assert.assertEquals(userOnHd4dp.getEmail(), userOnHd4res.getEmail());
			Assert.assertEquals(userOnHd4dp.getDataCollectionNames(), userOnHd4res.getDataCollectionNames());
			Assert.assertEquals(userOnHd4dp.getAuthorities(), convert(userOnHd4res.getAuthorities()));
			Assert.assertEquals(userOnHd4dp.getOrganization().getHealthDataIDType(), userOnHd4res.getHealthDataIdentification().getType());
			Assert.assertEquals(userOnHd4dp.getOrganization().getHealthDataIDValue(), userOnHd4res.getHealthDataIdentification().getValue());
			if(loggingEnabled) {
				LOG.info("User on HD4DP and HD4RES are equal");
			}
			return true;
		} catch (AssertionError e) {
			if(loggingEnabled) {
				LOG.error(e.getMessage());
			}
			return false;
		}
	}

	private Set<Authority> convert(Set<Hd4dpAuthority> authorities) {
		final Set<Authority> result = new HashSet<>();
		for (final Hd4dpAuthority authority : authorities) {
			result.add(new Authority(authority.getAuthority()));
		}
		return result;
	}
}
