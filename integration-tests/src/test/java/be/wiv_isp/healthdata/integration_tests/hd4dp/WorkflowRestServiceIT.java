/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.integration_tests.hd4dp;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.integration_tests.utils.enums.HealthDataRole;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.gathering.domain.RegistrationDocument;
import be.wiv_isp.healthdata.gathering.dto.RegistrationWorkflowDto;
import be.wiv_isp.healthdata.integration_tests.utils.TestUtils;
import be.wiv_isp.healthdata.integration_tests.utils.WorkflowUtils;
import be.wiv_isp.healthdata.integration_tests.utils.enums.DataCollection;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationDocument;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.dto.AbstractWorkflowDto;
import be.wiv_isp.healthdata.orchestration.dto.DocumentDto;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.api.client.WebResource;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.util.StringUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class WorkflowRestServiceIT {

	private static TestUtils TEST_UTILS = new TestUtils();
	private static WorkflowUtils WORKFLOW_UTILS;

	private static DataCollection DATA_COLLECTION = DataCollection.TEST;

	@BeforeClass
	public static void init() throws Exception {
		TEST_UTILS.initialize(WorkflowRestServiceIT.class.getResource("/test.it.properties"));
		WORKFLOW_UTILS = TEST_UTILS.getWorkflowUtils();
	}

	@Test
	public void testCreateRoleUser() throws Exception {
		final RegistrationWorkflowDto expected = new RegistrationWorkflowDto();
		expected.setStatus(WorkflowStatus.IN_PROGRESS);
		expected.setDataCollectionName(DATA_COLLECTION.toString());

		long countWorkflowsBeforeCreate = WORKFLOW_UTILS.getWorkflowCount(Host.HD4DP, DATA_COLLECTION);
		AbstractWorkflowDto workflow = WORKFLOW_UTILS.createWorkflow(DATA_COLLECTION);
		Thread.sleep(5000);
		long countWorkflowsAfterCreate = WORKFLOW_UTILS.getWorkflowCount(Host.HD4DP, DATA_COLLECTION);

		Assert.assertEquals(countWorkflowsBeforeCreate + 1, countWorkflowsAfterCreate);
		compareWorkflows(expected, workflow);

		WORKFLOW_UTILS.deleteWorkflow(workflow.getIdAsLong());
	}

	@Test
	public void testGetRoleUser() throws Exception {
		final RegistrationWorkflowDto expected = new RegistrationWorkflowDto();
		expected.setStatus(WorkflowStatus.IN_PROGRESS);
		expected.setDataCollectionName(DATA_COLLECTION.toString());

		AbstractWorkflowDto createdWorkflow = WORKFLOW_UTILS.createWorkflow(DATA_COLLECTION);
		AbstractWorkflowDto workflow = WORKFLOW_UTILS.getWorkflow(Host.HD4DP, createdWorkflow.getIdAsLong());
		compareWorkflows(expected, workflow);

		WORKFLOW_UTILS.deleteWorkflow(workflow.getIdAsLong());
	}

	@Test
	public void testGetHistoryCount() throws Exception {
		AbstractWorkflowDto createdWorkflow = WORKFLOW_UTILS.createWorkflow(DATA_COLLECTION);
		AbstractWorkflowDto workflow = WORKFLOW_UTILS.getWorkflow(Host.HD4DP, createdWorkflow.getIdAsLong());

		AbstractWorkflowDto workflowBeforeSave = WORKFLOW_UTILS.getWorkflow(Host.HD4DP, createdWorkflow.getIdAsLong());
		Assert.assertEquals(1, workflowBeforeSave.getHistory().size());

		Thread.sleep(1000); // ensure SAVE actions are executed at different timestamps
		WORKFLOW_UTILS.executeAction(getSaveJson(workflow.getIdAsLong()));

		AbstractWorkflowDto workflowAfterSave = WORKFLOW_UTILS.getWorkflow(Host.HD4DP, createdWorkflow.getIdAsLong());
		Assert.assertEquals(2, workflowAfterSave.getHistory().size());

		WORKFLOW_UTILS.deleteWorkflow(workflow.getIdAsLong());
	}

	@Test
	public void testGetNonExistingEntity() throws Exception {
		WORKFLOW_UTILS.getWorkflow(Host.HD4DP, -1L, Status.NOT_FOUND.getStatusCode());
	}

	@Test
	public void testExecuteSaveAction() throws Exception {
		AbstractWorkflowDto newWorkflow = WORKFLOW_UTILS.createWorkflow(DATA_COLLECTION);
		WORKFLOW_UTILS.executeAction(getSaveJson(newWorkflow.getIdAsLong()));

		AbstractWorkflowDto workflow = WORKFLOW_UTILS.getWorkflow(Host.HD4DP, newWorkflow.getIdAsLong());

		RegistrationDocument document = new RegistrationDocument();
		document.setDocumentContent("{ \"key\" : \"some JSON value\" }".getBytes());

		RegistrationWorkflowDto expected = new RegistrationWorkflowDto();
		expected.setId(newWorkflow.getId());
		expected.setDataCollectionName(DATA_COLLECTION.toString());
		expected.setStatus(WorkflowStatus.IN_PROGRESS);
		expected.setDocument(new DocumentDto(document));

		compareWorkflows(expected, workflow);

		WORKFLOW_UTILS.deleteWorkflow(workflow.getIdAsLong());
	}

	@Test
	public void testExecuteActionUpdatedOnMismatch() throws Exception {
		final AbstractWorkflowDto newWorkflow = WORKFLOW_UTILS.createWorkflow(DATA_COLLECTION);

		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, -1);
		final Date oneMinuteAgo = cal.getTime();
		final String updatedOnAsString = new SimpleDateFormat(DateFormat.DATE_AND_TIME.getPattern()).format(oneMinuteAgo);

		final JSONObject saveJson = getSaveJson(newWorkflow.getIdAsLong());
		final JSONObject metaData = saveJson.getJSONObject("metaData");
		metaData.put("UPDATED_ON", updatedOnAsString);

		WORKFLOW_UTILS.executeAction(saveJson, Status.CONFLICT);
		WORKFLOW_UTILS.deleteWorkflow(newWorkflow.getIdAsLong());
	}

	@Test
	public void testExecuteActionUpdatedOnCorrect() throws Exception {
		final AbstractWorkflowDto newWorkflow = WORKFLOW_UTILS.createWorkflow(DATA_COLLECTION);

		final Timestamp updatedOn = newWorkflow.getUpdatedOn();
		final String updatedOnAsString = new SimpleDateFormat(DateFormat.DATE_AND_TIME.getPattern()).format(new Date(updatedOn.getTime()));

		final JSONObject saveJson = getSaveJson(newWorkflow.getIdAsLong());
		final JSONObject metaData = saveJson.getJSONObject("metaData");
		metaData.put("UPDATED_ON", updatedOnAsString);

		WORKFLOW_UTILS.executeAction(saveJson);
		WORKFLOW_UTILS.deleteWorkflow(newWorkflow.getIdAsLong());
	}

	@Test
	public void testGetDocument() throws Exception {
		AbstractWorkflowDto newWorkflow = WORKFLOW_UTILS.createWorkflow(DATA_COLLECTION);
		WORKFLOW_UTILS.executeAction(getSaveJson(newWorkflow.getIdAsLong()));
		AbstractRegistrationDocument document = WORKFLOW_UTILS.getDocument(newWorkflow.getIdAsLong(), TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER));

		Assert.assertNotNull(document);
		Assert.assertArrayEquals("{\"key\":\"some JSON value\"}".getBytes(), document.getDocumentContent());

		WORKFLOW_UTILS.deleteWorkflow(newWorkflow.getIdAsLong());
	}

	@Test
	public void testGetDocumentNoWorkflow() throws Exception {
		WORKFLOW_UTILS.getDocument(-999L, TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER), Response.Status.NOT_FOUND.getStatusCode());
	}

	@Test
	public void testCORSHeadersAvailability() throws Exception {
		final ClientResponse response = getWorkflows(TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN));
		final MultivaluedMap<String, String> headerMap = response.getHeaders();
		Assert.assertFalse(headerMap.isEmpty());
		Assert.assertEquals("*", headerMap.getFirst("Access-Control-Allow-Origin"));
		Assert.assertEquals("GET, POST, PUT, DELETE, OPTIONS", headerMap.getFirst("Access-Control-Allow-Methods"));
		Assert.assertEquals("10", headerMap.getFirst("Access-Control-Max-Age"));
		Assert.assertTrue(StringUtils.isEmpty(headerMap.getFirst("Access-Control-Request-Headers")));
	}

	@Test
	public void testCORSHeadersAvailabilityUnauthorized() throws Exception {
		final WebResource wr = TEST_UTILS.createWebResource("{0}/workflows", Host.HD4DP);
		ClientResponse response = wr.type(MediaType.APPLICATION_JSON_TYPE).header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + null).get(ClientResponse.class);
		Assert.assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(), response.getStatus());
		final MultivaluedMap<String, String> headerMap = response.getHeaders();
		Assert.assertFalse(headerMap.isEmpty());
		Assert.assertEquals("*", headerMap.getFirst("Access-Control-Allow-Origin"));
		Assert.assertEquals("GET, POST, PUT, DELETE, OPTIONS", headerMap.getFirst("Access-Control-Allow-Methods"));
		Assert.assertEquals("10", headerMap.getFirst("Access-Control-Max-Age"));
		Assert.assertTrue(StringUtils.isEmpty(headerMap.getFirst("Access-Control-Request-Headers")));
	}

	@Test
	public void testCORSHeadersAvailabilityForbidden() throws Exception {
		final ClientResponse response = getWorkflows(TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN));
		Assert.assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
		final MultivaluedMap<String, String> headerMap = response.getHeaders();
		Assert.assertFalse(headerMap.isEmpty());
		Assert.assertEquals("*", headerMap.getFirst("Access-Control-Allow-Origin"));
		Assert.assertEquals("GET, POST, PUT, DELETE, OPTIONS", headerMap.getFirst("Access-Control-Allow-Methods"));
		Assert.assertEquals("10", headerMap.getFirst("Access-Control-Max-Age"));
		Assert.assertTrue(StringUtils.isEmpty(headerMap.getFirst("Access-Control-Request-Headers")));
	}

	@Test
	public void testCORSHeadersAvailabilityOptions() throws Exception {
		final ClientResponse response = getWorkflowsOptions(TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN));
		final MultivaluedMap<String, String> headerMap = response.getHeaders();
		Assert.assertFalse(headerMap.isEmpty());
		Assert.assertEquals("*", headerMap.getFirst("Access-Control-Allow-Origin"));
		Assert.assertEquals("GET, POST, PUT, DELETE, OPTIONS", headerMap.getFirst("Access-Control-Allow-Methods"));
		Assert.assertEquals("10", headerMap.getFirst("Access-Control-Max-Age"));
		Assert.assertEquals("Content-Range", headerMap.getFirst("Access-Control-Expose-Headers"));
		Assert.assertTrue(StringUtils.isEmpty(headerMap.getFirst("Access-Control-Request-Headers")));

	}

	@Test
	public void testHttpHeadersSecurity() throws Exception {
		final ClientResponse response = getWorkflowsOptions(TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN));
		final MultivaluedMap<String, String> headerMap = response.getHeaders();
		Assert.assertFalse(headerMap.isEmpty());
		Assert.assertEquals("-", headerMap.getFirst("Server"));
		Assert.assertEquals("SAMEORIGIN", headerMap.getFirst("X-Frame-Options"));
	}

	@Test
	public void testElasticSearchQuery() throws Exception {
		final AbstractWorkflowDto workflow = WORKFLOW_UTILS.createWorkflow(DATA_COLLECTION);
		final String elasticSearchQuery = MessageFormat.format("'{'\"query\" : '{' \"match\" : '{' \"id\" : {0} '}}}'", workflow.getId());
		int trials = 0;
		while (true) {
			// elasticsearch might not be instantly synchronized. Therefore, we do 10 trials, with 5 seconds interval between each.
			try {
				Assert.assertEquals(new Long(1L), WORKFLOW_UTILS.getWorkflowCount(Host.HD4DP, DATA_COLLECTION, elasticSearchQuery));
				break;
			} catch (AssertionError e) {
				if (++trials >= 10) {
					throw e;
				}
				Thread.sleep(5000);
			}
		}
		WORKFLOW_UTILS.deleteWorkflow(workflow.getIdAsLong());
	}

	private ClientResponse getWorkflows(String token) {
		final String query = "{\"query\" : { \"match_all\" : {}}}";
		final WebResource wr = TEST_UTILS.createWebResource("{0}/workflows/search/{1}", Host.HD4DP, TEST_UTILS.getDataCollectionDefinitionId(DATA_COLLECTION).toString());
		return wr.type(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token).post(ClientResponse.class, query);
	}

	private ClientResponse getWorkflowsOptions(String token) {
		final WebResource wr = TEST_UTILS.createWebResource("{0}/workflows", Host.HD4DP);
		return wr.type(MediaType.APPLICATION_JSON_TYPE).header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token).get(ClientResponse.class);
	}

	private void compareWorkflows(AbstractWorkflowDto expected, AbstractWorkflowDto actual) {
		Assert.assertNotNull(actual.getId());
		Assert.assertEquals(expected.getStatus(), actual.getStatus());
		Assert.assertEquals(expected.getDataCollectionName(), actual.getDataCollectionName());

		if (expected.getDocument() != null && actual.getDocument() != null) {
			Assert.assertArrayEquals(expected.getDocument().getDocumentData().getContent(), actual.getDocument().getDocumentData().getContent());
		}

		Assert.assertNotNull(actual.getCreatedOn());
		Assert.assertNotNull(actual.getUpdatedOn());
	}

	private JSONObject getSaveJson(Long workflowId) throws JSONException {
		final JSONObject saveJson = new JSONObject();
		saveJson.put("action", "SAVE");
		saveJson.put("workflowId", workflowId);
		final JSONObject documentContent = new JSONObject();
		documentContent.put("key", "some JSON value");
		saveJson.put("documentContent", documentContent);
		saveJson.put("metaData", new HashMap<>());
		return saveJson;
	}
}