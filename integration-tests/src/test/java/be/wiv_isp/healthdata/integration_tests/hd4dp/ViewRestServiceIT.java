/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.integration_tests.hd4dp;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.integration_tests.utils.TestUtils;
import be.wiv_isp.healthdata.integration_tests.utils.UserUtils;
import be.wiv_isp.healthdata.integration_tests.utils.ViewUtils;
import be.wiv_isp.healthdata.integration_tests.utils.enums.DataCollection;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import be.wiv_isp.healthdata.orchestration.dto.UserDto;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class ViewRestServiceIT {

	private static TestUtils TEST_UTILS;
	private static UserUtils USER_UTILS;
	private static ViewUtils VIEW_UTILS;

	@BeforeClass
	public static void init() throws Exception {
		TEST_UTILS = new TestUtils();
		TEST_UTILS.initialize(UserRestServiceIT.class.getResource("/test.it.properties"));
		USER_UTILS = TEST_UTILS.getUserUtils();
		VIEW_UTILS = TEST_UTILS.getViewUtils();
	}

	@Test
	public void testCreateUpdateAndDeleteView() throws Exception {
		final Host host = Host.HD4DP;
		final Long dcdId = TEST_UTILS.getDataCollectionDefinitionId(DataCollection.TEST);

		final UserDto user = USER_UTILS.buildUser();
		final Organization mainOrganization = TEST_UTILS.getOrganizationUtils().getMainOrganization(host);
		final UserDto createdUser = USER_UTILS.createUser(host, mainOrganization.getId(), user);

		Assert.assertEquals(0, VIEW_UTILS.getViews(host, mainOrganization.getId(), createdUser, dcdId).size());
		VIEW_UTILS.createView(host, mainOrganization.getId(), createdUser, dcdId, "my-view", new JSONObject());
		Assert.assertEquals(1, VIEW_UTILS.getViews(host, mainOrganization.getId(), createdUser, dcdId).size());

		Assert.assertEquals(1, VIEW_UTILS.getViews(host, mainOrganization.getId(), createdUser, dcdId).size());
		VIEW_UTILS.createView(host, mainOrganization.getId(), createdUser, dcdId, "my-view-2", new JSONObject());
		Assert.assertEquals(2, VIEW_UTILS.getViews(host, mainOrganization.getId(), createdUser, dcdId).size());

		final JSONObject viewContent = new JSONObject();
		viewContent.put("key", "value");
		VIEW_UTILS.updateView(host, mainOrganization.getId(), createdUser, dcdId, "my-view", viewContent);

		VIEW_UTILS.createViewDuplicateView(host, mainOrganization.getId(), createdUser, dcdId, "my-view", viewContent);

		VIEW_UTILS.deleteView(host, mainOrganization.getId(), createdUser, dcdId, "my-view");
		Assert.assertEquals(1, VIEW_UTILS.getViews(host, mainOrganization.getId(), createdUser, dcdId).size());
		USER_UTILS.deleteUser(host, createdUser);
	}
}
