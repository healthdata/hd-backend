/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.integration_tests.hd4dp;

import be.wiv_isp.healthdata.integration_tests.utils.enums.HealthDataRole;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.integration_tests.utils.TestUtils;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class StatusMessageRestServiceIT {

	private static final TestUtils TEST_UTILS = new TestUtils();
	private static final String STATUS_MESSAGE_SEND_URI_PATTERN = "{0}/statusmessages/send";

	@BeforeClass
	public static void init() throws Exception {
		TEST_UTILS.initialize(StatusMessageRestServiceIT.class.getResource("/test.it.properties"));
	}

	@Test
	public void testSendHd4dp() {
		send(Host.HD4DP);
	}

	@Test
	public void testSendHd4res() {
		send(Host.HD4RES);
	}

	private void send(Host host) {
		final String token = TEST_UTILS.getAccessToken(host, HealthDataRole.ADMIN);
		final WebResource wr = TEST_UTILS.createWebResource(STATUS_MESSAGE_SEND_URI_PATTERN, host);
		final ClientResponse response = wr.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token).post(new GenericType<ClientResponse>() {});

		Assert.assertEquals(ClientResponse.Status.OK.getStatusCode(), response.getStatus());
	}
}
