/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.integration_tests.utils;

import be.wiv_isp.healthdata.integration_tests.utils.enums.HealthDataRole;
import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.gathering.domain.RegistrationDocument;
import be.wiv_isp.healthdata.gathering.dto.FullSearchRegistrationWorkflowDto;
import be.wiv_isp.healthdata.gathering.dto.RegistrationWorkflowDto;
import be.wiv_isp.healthdata.integration_tests.utils.enums.DataCollection;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.lang.ArrayUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WorkflowUtils {

    private static final Logger LOG = LoggerFactory.getLogger(WorkflowUtils.class);

    private TestUtils testUtils;

    public WorkflowUtils(TestUtils testUtils) {
        this.testUtils = testUtils;
    }

    public RegistrationWorkflowDto getWorkflow(Host host, Long workflowId) throws Exception {
        return getWorkflow(host, workflowId, false, true, Response.Status.OK.getStatusCode());
    }

    public RegistrationWorkflowDto getWorkflow(Host host, Long workflowId, int expectedResponse) throws Exception {
        return getWorkflow(host, workflowId, false, true, expectedResponse);
    }

    public RegistrationWorkflowDto getWorkflow(Host host, Long workflowId, Boolean fullSearch, Boolean returnDeleted) throws Exception {
        return getWorkflow(host, workflowId, fullSearch, returnDeleted, Response.Status.OK.getStatusCode());
    }

    public RegistrationWorkflowDto getWorkflow(Host host, Long workflowId, Boolean fullSearch, Boolean returnDeleted, int expectedResponse) throws Exception {
        LOG.debug(MessageFormat.format("Retrieving workflow on host {0} with id {1}", host, workflowId));

        final String token = testUtils.getAccessToken(host, HealthDataRole.USER);
        final WebResource wr = testUtils.createWebResource("{0}/workflows/{1}?fullSearch={2}&returnDeleted={3}", host, String.valueOf(workflowId), fullSearch.toString(), returnDeleted.toString());
        final ClientResponse clientResponse = wr.type(MediaType.APPLICATION_JSON_TYPE).header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token).get(ClientResponse.class);

        Assert.assertEquals(expectedResponse, clientResponse.getStatus());

        if(clientResponse.getStatus() == ClientResponse.Status.OK.getStatusCode()) {
            if (fullSearch) {
                return clientResponse.getEntity(FullSearchRegistrationWorkflowDto.class);
            }
            return clientResponse.getEntity(RegistrationWorkflowDto.class);
        }
        return null;
    }

    public void deleteWorkflow(Long workflowId) throws Exception {
        deleteWorkflow(workflowId, ClientResponse.Status.OK);
    }

    public void deleteWorkflow(Long workflowId, ClientResponse.Status expectedStatus) throws Exception {
        if(ClientResponse.Status.OK.equals(expectedStatus)) {
            LOG.info(MessageFormat.format("Deleting workflow with id {0}", workflowId));
        } else {
            LOG.info(MessageFormat.format("Deleting workflow with id {0} and expect response status {1}", workflowId, expectedStatus));
        }

        final JSONObject requestBody = new JSONObject();
        requestBody.put("action", "DELETE");
        requestBody.put("workflowId", workflowId.toString());

        executeAction(requestBody, expectedStatus);
    }

    public ClientResponse executeAction(JSONObject requestBody) throws Exception {
        return executeAction(requestBody, ClientResponse.Status.OK, ClientResponse.Status.ACCEPTED);
    }

    public ClientResponse executeAction(JSONObject requestBody, ClientResponse.Status... expectedStatus) throws Exception {
        // extract info from json string
        final Long workflowId = requestBody.isNull("workflowId") ? null : requestBody.getLong("workflowId");
        final WorkflowActionType action = WorkflowActionType.valueOf(requestBody.getString("action"));
        final Host host = mapActionTypeToHost(action);

        if(workflowId == null) {
            LOG.info(MessageFormat.format("Executing action {0} on host {2}", action, workflowId, host));
        } else {
            LOG.info(MessageFormat.format("Executing action {0} on workflow {1} on host {2}", action, workflowId, host));
        }

        final String token = testUtils.getAccessToken(host, HealthDataRole.USER);

        final ClientResponse response = testUtils
                .createWebResource("{0}/workflows/actions", host)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token)
                .post(ClientResponse.class, requestBody);

        boolean ok = false;
        for (ClientResponse.Status status : expectedStatus) {
            if(status.getStatusCode() == response.getStatus()) {
                ok = true;
                break;
            }
        }
        if(!ok) {
            LOG.error("response status: " + response.getStatus());
            Assert.assertTrue(false);
        }

        return response;
    }

    private Host mapActionTypeToHost(WorkflowActionType action) throws Exception {
        if (WorkflowActionType.DELETE.equals(action)) {
            return Host.HD4DP;
        }
        if (action.appliesTo(Platform.HD4DP)) {
            return Host.HD4DP;
        }
        if (action.appliesTo(Platform.HD4RES)) {
            return Host.HD4RES;
        }
        throw new Exception(MessageFormat.format("Could not map action [{0}] to a host.", action));
    }

    public RegistrationWorkflowDto createWorkflow(DataCollection dataCollection) throws Exception {
        return createWorkflow(new JSONObject(), dataCollection);
    }

    public RegistrationWorkflowDto createWorkflow(JSONObject documentContent, DataCollection dataCollection) throws Exception {
        LOG.info("Creating new worklfow for data collection {}", dataCollection);

        final JSONObject saveJson = new JSONObject();
        saveJson.put("action", "SAVE");
        saveJson.put("dataCollectionDefinitionId", testUtils.getDataCollectionDefinitionId(dataCollection));
        saveJson.put("documentContent", documentContent);

        final String token = testUtils.getAccessToken(Host.HD4DP, HealthDataRole.USER);
        final ClientResponse response = testUtils.createWebResource("{0}/workflows/actions", Host.HD4DP)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token)
                .post(ClientResponse.class, saveJson);

        Assert.assertTrue(Response.Status.OK.getStatusCode() == response.getStatus() || Response.Status.ACCEPTED.getStatusCode() == response.getStatus());

        final RegistrationWorkflowDto workflow = response.getEntity(RegistrationWorkflowDto.class);
        LOG.info(MessageFormat.format("Workflow with id [{0}] created: {1}", workflow.getId(), new ObjectMapper().writeValueAsString(workflow)));

        return workflow;
    }

    public Long getWorkflowCount(Host host, DataCollection dataCollection) throws JSONException {
        final String elasticSearchQuery = "{\"query\" : { \"match_all\" : {}}}";
        return getWorkflowCount(host, dataCollection, elasticSearchQuery);
    }

    public Long getWorkflowCount(Host host, DataCollection dataCollection, String elasticSearchQuery) throws JSONException {
        final String token = testUtils.getAccessToken(host, HealthDataRole.USER);
        final ClientResponse response = testUtils.createWebResource("{0}/workflows/search/{1}", host, testUtils.getDataCollectionDefinitionId(dataCollection).toString()).type(MediaType.APPLICATION_JSON_TYPE).header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token).post(ClientResponse.class, elasticSearchQuery);
        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        final JSONObject jsonObject = new JSONObject(response.getEntity(String.class));
        LOG.info("jsonObject: {}", jsonObject);
        return jsonObject.getJSONObject("hits").getLong("total");
    }

    public List<RegistrationWorkflowDto> getWorkflows(Host host, DataCollection dataCollection, String elasticSearchQuery) throws Exception {
        final String token = testUtils.getAccessToken(host, HealthDataRole.USER);
        final ClientResponse response = testUtils.createWebResource("{0}/workflows/search/{1}", host, testUtils.getDataCollectionDefinitionId(dataCollection).toString()).type(MediaType.APPLICATION_JSON_TYPE).header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token).post(ClientResponse.class, elasticSearchQuery);
        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        final List<RegistrationWorkflowDto> results = new ArrayList<>();
        final JSONArray hits = new JSONObject(response.getEntity(String.class)).getJSONObject("hits").getJSONArray("hits");
        for(int i=0; i<hits.length(); ++i) {
            final JSONObject hit = hits.getJSONObject(i);
            final Long workflowId = hit.getJSONObject("_source").getLong("id");
            results.add(getWorkflow(host, workflowId));
        }
        return results;
    }

    public RegistrationWorkflowDto getWorkflow(Host host, DataCollection dataCollection, String elasticSearchQuery) throws Exception {
        final List<RegistrationWorkflowDto> workflows = getWorkflows(host, dataCollection, elasticSearchQuery);
        if(workflows.isEmpty()) {
            return null;
        }
        if(workflows.size() == 1) {
            return workflows.get(0);
        }
        throw new Exception("More than one workflow found");
    }

    public long getWorkflowCountByDataProviderId(DataCollection dataCollection, final String identificationValue) throws ClientHandlerException, UniformInterfaceException, JSONException {
        final String query = MessageFormat.format("'{'\"query\" : '{' \"match\" : '{' \"identificationValue\" : \"{0}\" '}}}'", identificationValue);
        return getWorkflowCount(Host.HD4RES, dataCollection, query);
    }

    public void submit(RegistrationWorkflowDto workflow) throws Exception {
        LOG.info("Submitting workflow with id {}", workflow.getId());

        final JSONObject requestBody = new JSONObject();
        requestBody.put("action", "SUBMIT");
        requestBody.put("workflowId", workflow.getId().toString());

        executeAction(requestBody);
    }

    public void reOpen(RegistrationWorkflowDto workflow) throws Exception {
        LOG.info("Re opening workflow with id {}", workflow.getId());
        final JSONObject requestBody = new JSONObject();
        requestBody.put("action", "REOPEN");
        requestBody.put("workflowId", workflow.getId().toString());

        executeAction(requestBody);
        verifyStatus(workflow, Host.HD4DP, WorkflowStatus.IN_PROGRESS);
    }

    public void verifyStatus(RegistrationWorkflowDto workflow, Host host, WorkflowStatus... statuses) throws Exception {
        LOG.info("verifying status of worklow {} on {} is one of {}", workflow.getId(), host, ArrayUtils.toString(statuses));
        Assert.assertTrue(Arrays.asList(statuses).contains(getWorkflow(host, workflow.getIdAsLong()).getStatus()));
    }

    public RegistrationWorkflowDto retrieveHd4resWorkflow(final RegistrationWorkflowDto hd4dpWorkflow) throws Exception {
        final String encodedWorkflowId = new String(com.sun.jersey.core.util.Base64.encode(hd4dpWorkflow.getId().toString()));
        final String query = MessageFormat.format("'{'\"query\" : '{' \"match\" : '{' \"hd4dpWorkflowId\" : \"{0}\" '}}}'", encodedWorkflowId);
        return getWorkflow(Host.HD4RES, DataCollection.valueOf(hd4dpWorkflow.getDataCollectionName()), query);
    }

    public RegistrationDocument getDocument(Long id, String token) {
        return getDocument(id, token, ClientResponse.Status.OK.getStatusCode());
    }

    public RegistrationDocument getDocument(Long id, String token, int expectedResponse) {
        final ClientResponse response = testUtils.createWebResource("{0}/workflows/{1}/documents", Host.HD4DP, String.valueOf(id))
                .type(MediaType.APPLICATION_JSON_TYPE)
                .header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token)
                .get(ClientResponse.class);

        Assert.assertEquals(expectedResponse, response.getStatus());

        if(response.getStatus() == ClientResponse.Status.OK.getStatusCode()) {
            return response.getEntity(RegistrationDocument.class);
        }
        return null;
    }
}
