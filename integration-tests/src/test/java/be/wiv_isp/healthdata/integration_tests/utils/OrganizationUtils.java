/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.integration_tests.utils;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import com.sun.jersey.api.client.GenericType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrganizationUtils {

    private static final Logger LOG = LoggerFactory.getLogger(OrganizationUtils.class);

    private TestUtils testUtils;

    private Map<Host, Organization> mainOrganizations = new HashMap();

    public OrganizationUtils(TestUtils testUtils) {
        this.testUtils = testUtils;
    }

    public Organization getMainOrganization(Host host) {
        if (mainOrganizations.get(host) == null) {
            mainOrganizations.put(host, doGetMainOrganization(host));
        }
        return mainOrganizations.get(host);
    }

    private Organization doGetMainOrganization(Host host) {
        LOG.info("Retrieving main organization for host {}", host);

        for (final Organization organization : getOrganizations(host)) {
            if (organization.isMain()) {
                return organization;
            }
        }

        throw new RuntimeException("No main organization found");
    }

    public List<Organization> getOrganizations(Host host) {
        LOG.info("Retrieving all organizations for host {}", host);
        final List<Organization> organizations = testUtils.createWebResource("{0}/organizations", host)
                .type(MediaType.APPLICATION_JSON_TYPE)
            .get(new GenericType<List<Organization>>() {});
        LOG.info("{} organization(s) found", organizations.size());
        return organizations;
    }

}
