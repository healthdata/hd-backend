/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.integration_tests.utils;

import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import com.sun.jersey.api.client.ClientResponse;
import org.codehaus.jettison.json.JSONObject;

import java.text.MessageFormat;

public class TokenUtils {

	private TestUtils testUtils;

	public TokenUtils(TestUtils testUtils) {
		this.testUtils = testUtils;
	}

	public String getAccessToken(Host host, String username, String password, Long organizationId) {
		final ClientResponse response = testUtils.createWebResource("{0}/oauth/token", host)
			.queryParam("grant_type", "organization_password")
			.queryParam("client_id", "healthdata-client")
			.queryParam("username", username)
			.queryParam("password", password)
			.queryParam("organization_id", organizationId.toString())
			.post(ClientResponse.class);

		if(ClientResponse.Status.OK.getStatusCode() == response.getStatus()) {
			return JsonUtils.getString(response.getEntity(JSONObject.class), "access_token");
		}

		throw new RuntimeException(MessageFormat.format("Received status {0} from server. Expected {1}", response.getStatus(), ClientResponse.Status.OK.getStatusCode()));
	}
}
