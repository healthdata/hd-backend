/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.integration_tests.utils;

import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.integration_tests.utils.enums.DataCollection;
import be.wiv_isp.healthdata.integration_tests.utils.enums.HealthDataRole;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import be.wiv_isp.healthdata.orchestration.dto.UserDto;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

public class DataCollectionAccessRequestUtils {

    private static final Logger LOG = LoggerFactory.getLogger(DataCollectionAccessRequestUtils.class);

    private TestUtils testUtils;

    public DataCollectionAccessRequestUtils(TestUtils testUtils) {
        this.testUtils = testUtils;
    }

    public Long requestDataCollectionAccess(UserDto user, List<DataCollection> dataCollections) throws JSONException {
        LOG.info("Requesting data collection access for data collections: {}", dataCollections);

        Assert.assertNotNull(user.getUsername());
        Assert.assertNotNull(user.getPassword());
        Assert.assertNotNull(user.getOrganization());

        final String accessToken = testUtils.getAccessToken(Host.HD4DP, user.getOrganization().getId(), user.getUsername(), user.getPassword());

        final WebResource wr = testUtils.createWebResource("{0}/organizations/{1}/dataCollectionAccessRequests", Host.HD4DP, user.getOrganization().getId().toString());
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("dataCollections", dataCollections);
        final ClientResponse response = wr
                .type(MediaType.APPLICATION_JSON_TYPE)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + accessToken)
                .post(ClientResponse.class, jsonObject);
        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        final JSONObject entity = response.getEntity(JSONObject.class);
        return entity.getLong("id");
    }

    public void postDataCollectionAccessRequestDecision(Long organizationId, Long requestId, Map<String, Boolean> decisions) {
        final String accessToken = testUtils.getAccessToken(Host.HD4DP, organizationId, HealthDataRole.ADMIN);
        postDataCollectionAccessRequestDecision(organizationId, requestId, decisions, accessToken, Response.Status.OK.getStatusCode());
    }

    public void postDataCollectionAccessRequestDecision(Long organizationId, Long requestId, Map<String, Boolean> decisions, String accessToken, int expectedResponse) {
        LOG.info("Posting data collection access decisions");

        final WebResource wr = testUtils.createWebResource("{0}/organizations/{1}/dataCollectionAccessRequests/{2}/decision", Host.HD4DP, organizationId.toString(), requestId.toString());
        final ClientResponse response = wr
                .type(MediaType.APPLICATION_JSON_TYPE)
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + accessToken)
                .post(ClientResponse.class, decisions);
        Assert.assertEquals(expectedResponse, response.getStatus());
    }
}
