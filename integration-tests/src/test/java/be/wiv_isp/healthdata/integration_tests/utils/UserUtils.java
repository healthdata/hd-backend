/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.integration_tests.utils;

import be.wiv_isp.healthdata.integration_tests.utils.enums.HealthDataRole;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.integration_tests.domain.SerializableUserDto;
import be.wiv_isp.healthdata.integration_tests.utils.enums.DataCollection;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.dto.UserDto;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;

public class UserUtils {

    private static final Logger LOG = LoggerFactory.getLogger(UserUtils.class);

    private TestUtils testUtils;

    public UserUtils(TestUtils testUtils) {
        this.testUtils = testUtils;
    }

    public UserDto buildUser() {
        LOG.info("Building user");
        final long timestamp = System.currentTimeMillis();

        final UserDto user = new SerializableUserDto();
        user.setUsername("IT_" + timestamp);
        user.setPassword("pazz");
        user.setFirstName("firstname_" + timestamp);
        user.setLastName("lastname_" + timestamp);
        user.setEmail("test_" + timestamp + "@test.com");
        user.setAuthorities(new HashSet<>(Collections.singletonList(Authority.USER_AUTHORITY)));
        user.setDataCollectionNames(new HashSet<>(Collections.singletonList(DataCollection.TEST.toString())));
        user.setEnabled(true);

        return user;
    }

    public UserDto createUser(Host host, Long organizationId, UserDto user) {
        LOG.info("Creating user");

        final String accessToken = testUtils.getAccessToken(host, organizationId, HealthDataRole.ADMIN);

        final WebResource wr = testUtils.createWebResource("{0}/users", Host.HD4DP);
        final ClientResponse response = wr
                .header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + accessToken)
                .type(MediaType.APPLICATION_JSON)
                .post(ClientResponse.class, user);

        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        final UserDto createdUser = response.getEntity(SerializableUserDto.class);
        createdUser.setPassword(user.getPassword());
        return createdUser;
    }

    public ClientResponse createUserReturnResponse(final UserDto user, final String token) {
        final WebResource wr = testUtils.createWebResource("{0}/users", Host.HD4DP);
        return wr.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, user);
    }

    public UserDto updateUser(Long id, UserDto user, String accessToken) throws Exception {
        final ClientResponse response = updateUserReturnResponse(id, user, accessToken);
        if (Response.Status.OK.getStatusCode() == response.getStatus()) {
            return response.getEntity(new GenericType<SerializableUserDto>() {});
        }
        throw new Exception("Could not update user. Response status [" + response.getStatus() + "] returned from the server");
    }

    public ClientResponse updateUserReturnResponse(Long id, UserDto user, String accessToken) {
        final WebResource wr = testUtils.createWebResource("{0}/users/{1}", Host.HD4DP, String.valueOf(id));
        return wr.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + accessToken) //
                .type(MediaType.APPLICATION_JSON_TYPE) //
                .accept(MediaType.APPLICATION_JSON_TYPE) //
                .put(ClientResponse.class, user);
    }

    public UserDto getMe(Host host, String accessToken) {
        final WebResource wr = testUtils.createWebResource("{0}/users/me", host);
        final ClientResponse response = wr
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + accessToken)
                .get(ClientResponse.class);

        Assert.assertEquals(ClientResponse.Status.OK.getStatusCode(), response.getStatus());
        return response.getEntity(SerializableUserDto.class);
    }

    public UserDto getUser(Long organizationId, Long userId) {
        final String accessToken = testUtils.getAccessToken(Host.HD4DP, organizationId, HealthDataRole.ADMIN);
        final ClientResponse response = getUser(organizationId, userId, accessToken, Response.Status.OK.getStatusCode());
        return response.getEntity(SerializableUserDto.class);
    }

    public ClientResponse getUser(Long organizationId, Long userId, String accessToken, int expectedResponse) {
        LOG.info("Retrieving user with organization id {} and user id {}", organizationId, userId);
        final WebResource wr = testUtils.createWebResource("{0}/users/{1}", Host.HD4DP, String.valueOf(userId));
        final ClientResponse response = wr
                .header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + accessToken)
                .accept(MediaType.APPLICATION_JSON)
                .get(ClientResponse.class);

        Assert.assertEquals(expectedResponse, response.getStatus());
        return response;
    }

    public ClientResponse updatePassword(Long userId, String password, String new_password, String accessToken) {
        final WebResource wr = testUtils.createWebResource("{0}/users/password/{1}", Host.HD4DP, String.valueOf(userId));
        final MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
        queryParams.add("oldPassword", password);
        queryParams.add("newPassword", new_password);
        final ClientResponse response = wr.queryParams(queryParams) //
                .header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + accessToken) //
                .type(MediaType.APPLICATION_JSON_TYPE) //
                .accept(MediaType.APPLICATION_JSON_TYPE) //
                .put(ClientResponse.class);

        Assert.assertEquals(ClientResponse.Status.OK.getStatusCode(), response.getStatus());
        return response;
    }

    public ClientResponse updateMetadataReturnResponse(Long userId, Map<String, String> metaData, String accessToken) {
        final WebResource wr = testUtils.createWebResource("{0}/users/metadata/{1}", Host.HD4DP, String.valueOf(userId));
        return wr.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + accessToken) //
                .type(MediaType.APPLICATION_JSON_TYPE) //
                .accept(MediaType.APPLICATION_JSON_TYPE) //
                .put(ClientResponse.class, metaData);
    }

    public UserDto updateMetadata(Long userId, Map<String, String> metaData, String accessToken) {
        final ClientResponse response = updateMetadataReturnResponse(userId, metaData, accessToken);
        Assert.assertEquals(ClientResponse.Status.OK.getStatusCode(), response.getStatus());
        return response.getEntity(SerializableUserDto.class);
    }

    public void deleteUser(Host host, UserDto user) {
        final String accessToken = testUtils.getAccessToken(host, user.getOrganization().getId(), HealthDataRole.ADMIN);
        deleteUser(host, user, accessToken, Response.Status.NO_CONTENT.getStatusCode());
    }

    public void deleteUser(Host host, UserDto user, String accessToken, int expectedResponse) {
        LOG.info("Deleting user {}", user.getId());
        final WebResource wr = testUtils.createWebResource("{0}/users/{1}", host, String.valueOf(user.getId()));
        final ClientResponse response = wr
                .header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + accessToken)
                .delete(ClientResponse.class);
        Assert.assertEquals(expectedResponse, response.getStatus());
    }
}
