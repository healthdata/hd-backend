/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.integration_tests.hd4dp;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.NationalRegisterConnector;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.integration_tests.utils.enums.HealthDataRole;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.gathering.dto.PersonDto;
import be.wiv_isp.healthdata.integration_tests.utils.TestUtils;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class PatientIdentificationRestServiceIT {

	private static TestUtils TEST_UTILS = new TestUtils();

	@BeforeClass
	public static void init() throws Exception {
		TEST_UTILS.initialize(PatientIdentificationRestServiceIT.class.getResource("/test.it.properties"));
	}

	@After
	public void cleanUp() throws Exception {
		TEST_UTILS.restoreAllConfigurations();
	}

	@Test
	public void testCustomRestNrcConnectorWithExistingSsin() throws Exception {
		TEST_UTILS.setConfiguration(Host.HD4DP, ConfigurationKey.NATIONAL_REGISTER_CONNECTOR, NationalRegisterConnector.CUSTOM_REST_NRC.toString());

		final String ssin = "53080282590";

		final PersonDto personDto = getPerson(ssin);
		final PersonDto expectedPersonDto = getExpectedPersonBySsin(ssin);

		Assert.assertEquals(expectedPersonDto, personDto);
	}

	@Test
	public void testCustomRestNrcConnectorWithUnexistingValidSsinReturnsEmptyResponse() throws Exception {
		TEST_UTILS.setConfiguration(Host.HD4DP, ConfigurationKey.NATIONAL_REGISTER_CONNECTOR, NationalRegisterConnector.CUSTOM_REST_NRC.toString());

		final String ssin = "90092050968";

		final PersonDto personDto = getPerson(ssin);

		Assert.assertEquals(new PersonDto(), personDto);
	}

	@Test
	public void testCustomRestNrcConnectorWithUnexistingNonValidSsinReturnsEmptyResponse() throws Exception {
		TEST_UTILS.setConfiguration(Host.HD4DP, ConfigurationKey.NATIONAL_REGISTER_CONNECTOR, NationalRegisterConnector.CUSTOM_REST_NRC.toString());

		final String internalId = "1";

		final PersonDto personDto = getPerson(internalId);

		Assert.assertEquals(new PersonDto(), personDto);
	}

	@Test
	public void testAdtConnectorWithExistingSsin() throws Exception {
		TEST_UTILS.setConfiguration(Host.HD4DP, ConfigurationKey.NATIONAL_REGISTER_CONNECTOR, NationalRegisterConnector.ADT.toString());

		final String ssin = "53080282590";

		final PersonDto personDto = getPerson(ssin);
		final PersonDto expectedPersonDto = getExpectedPersonBySsin(ssin);

		Assert.assertEquals(expectedPersonDto, personDto);
	}

	@Test
	public void testAdtConnectorWithExistingInternalId() throws Exception {
		TEST_UTILS.setConfiguration(Host.HD4DP, ConfigurationKey.NATIONAL_REGISTER_CONNECTOR, NationalRegisterConnector.ADT.toString());

		final String internalId = "103";

		final PersonDto personDto = getPerson(internalId);
		final PersonDto expectedPersonDto = getExpectedPersonByInternalId(internalId);

		Assert.assertEquals(expectedPersonDto, personDto);
	}

	@Test
	public void testAdtConnectorWithUnexistingValidSsinReturnsEmptyResponse() throws Exception {
		TEST_UTILS.setConfiguration(Host.HD4DP, ConfigurationKey.NATIONAL_REGISTER_CONNECTOR, NationalRegisterConnector.ADT.toString());

		final String ssin = "90092050968";

		final PersonDto personDto = getPerson(ssin);

		Assert.assertEquals(new PersonDto(), personDto);
	}

	@Test
	public void testAdtConnectorWithUnexistingNonValidSsinReturnsEmptyResponse() throws Exception {
		TEST_UTILS.setConfiguration(Host.HD4DP, ConfigurationKey.NATIONAL_REGISTER_CONNECTOR, NationalRegisterConnector.ADT.toString());

		final String internalId = "1";

		final PersonDto personDto = getPerson(internalId);

		Assert.assertEquals(new PersonDto(), personDto);
	}

	@Test
	public void testNoConnectorReturnsEmptyResponse() throws Exception {
		TEST_UTILS.setConfiguration(Host.HD4DP, ConfigurationKey.NATIONAL_REGISTER_CONNECTOR, NationalRegisterConnector.NONE.toString());

		final String ssin = "53080282590";

		final PersonDto personDto = getPerson(ssin);

		Assert.assertEquals(new PersonDto(), personDto);
	}

	private PersonDto getExpectedPersonBySsin(String ssin) throws ParseException {
		switch (ssin) {
		case "53080282590":
			return getSofiaPieterman();

		default:
			return null;
		}
	}

	private PersonDto getExpectedPersonByInternalId(String internalId) throws ParseException {
		switch (internalId) {
		case "103":
			return getSofiaPieterman();

		default:
			return null;
		}
	}

	private PersonDto getSofiaPieterman() throws ParseException {
		final PersonDto expectedPersonDto = new PersonDto();
		expectedPersonDto.setSsin("53080282590");
		expectedPersonDto.setInternalId("103");
		expectedPersonDto.setLastName("Pieterman");
		expectedPersonDto.setFirstName("Sofia");
		expectedPersonDto.setDateOfBirth(new SimpleDateFormat(DateFormat.DDMMYYYY.getPattern()).parse("2/08/1953"));
		expectedPersonDto.setDistrict("8850");
		expectedPersonDto.setGender(PersonDto.Gender.FEMALE);
		expectedPersonDto.setDeceased(true);
		expectedPersonDto.setDateOfDeath(new SimpleDateFormat(DateFormat.DDMMYYYY.getPattern()).parse("30/10/2002"));
		return expectedPersonDto;
	}

	private PersonDto getPerson(String patientId) {
		final String accessToken = TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER);
		final WebResource wr = TEST_UTILS.createWebResource("{0}/patient/{1}", Host.HD4DP, patientId);
		final ClientResponse response = wr.accept(MediaType.APPLICATION_JSON_TYPE).header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + accessToken).get(ClientResponse.class);

		Assert.assertEquals(ClientResponse.Status.OK.getStatusCode(), response.getStatus());

		return response.getEntity(PersonDto.class);
	}
}
