/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.integration_tests.utils;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.dto.ConfigurationDto;
import be.wiv_isp.healthdata.integration_tests.utils.enums.HealthDataRole;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import be.wiv_isp.healthdata.orchestration.dto.UserDto;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

public class ConfigurationUtils {

    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationUtils.class);

    private TestUtils testUtils;

    public ConfigurationUtils(TestUtils testUtils) {
        this.testUtils = testUtils;
    }

    public ConfigurationDto getConfiguration(Host host, final ConfigurationKey key) throws Exception {
        final String token = testUtils.getAccessToken(host, HealthDataRole.ADMIN);
        final UserDto user = testUtils.getUserUtils().getMe(host, token);
        final WebResource wr = testUtils.createWebResource("{0}/configurations", host);

        final ClientResponse response = wr.queryParam("key", key.toString()).header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token) //
                .accept(MediaType.APPLICATION_JSON_TYPE) //
                .get(ClientResponse.class);
        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        final List<ConfigurationDto> configurations = response.getEntity(new GenericType<List<ConfigurationDto>>() {});

        if (configurations.size() == 1) {
            return configurations.get(0);
        }

        for (ConfigurationDto c : configurations) {
            if (c.getOrganization() == null) {
                continue;
            }

            if (user.getOrganization().getId().equals(c.getOrganization().getId())) {
                return c;
            }
        }

        throw new Exception("no configuration found for key: " + key);
    }

    public ConfigurationDto updateConfiguration(Host host, final Long id, final ConfigurationDto configuration, final String access) {
        final WebResource wr = testUtils.createWebResource("{0}/configurations/{1}", host, id.toString());

        ClientResponse response = wr.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + access) //
                .type(MediaType.APPLICATION_JSON_TYPE) //
                .accept(MediaType.APPLICATION_JSON_TYPE) //
                .put(ClientResponse.class, configuration);

        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        return response.getEntity(ConfigurationDto.class);
    }

    public ConfigurationDto getConfigurationEntity(final Long id, final String token) {
        final WebResource wr = testUtils.createWebResource("{0}/configurations/{1}", Host.HD4DP, String.valueOf(id));

        final ClientResponse response = wr.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token) //
                .accept(MediaType.APPLICATION_JSON_TYPE) //
                .get(ClientResponse.class);

        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        return response.getEntity(ConfigurationDto.class);
    }

    public ConfigurationDto getConfigurationEntity(final ConfigurationKey key, final String token) {
        final ClientResponse response = getConfigurations(key, token);
        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        List<ConfigurationDto> entity = response.getEntity(new GenericType<List<ConfigurationDto>>() {});
        return entity.get(0);
    }

    public ClientResponse getConfigurations(final ConfigurationKey key, final String token) {
        final WebResource wr = testUtils.createWebResource("{0}/configurations", Host.HD4DP);
        return wr.queryParam("key", key.toString()) //
                .header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token) //
                .accept(MediaType.APPLICATION_JSON_TYPE) //
                .get(ClientResponse.class);
    }

    public ClientResponse getConfigurations(final String token) {
        final WebResource wr = testUtils.createWebResource("{0}/configurations", Host.HD4DP);
        return wr.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token) //
                .accept(MediaType.APPLICATION_JSON_TYPE) //
                .get(ClientResponse.class);
    }

    public ConfigurationDto updateConfigurationEntity(final Long id, final ConfigurationDto configuration, final String token) {
        return updateConfiguration(Host.HD4DP, id, configuration, token);
    }

    public ClientResponse updateConfiguration(final Long id, final ConfigurationDto configuration, final String token) {
        final WebResource wr = testUtils.createWebResource("{0}/configurations/{1}", Host.HD4DP, String.valueOf(id));

        return wr.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token) //
                .type(MediaType.APPLICATION_JSON_TYPE) //
                .accept(MediaType.APPLICATION_JSON_TYPE) //
                .put(ClientResponse.class, configuration);
    }
}
