/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.integration_tests.endtoend;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.integration_tests.utils.enums.HealthDataRole;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.gathering.dto.StableDataDto;
import be.wiv_isp.healthdata.integration_tests.endtoend.categories.StableData;
import be.wiv_isp.healthdata.integration_tests.utils.TestUtils;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class StableDataScenario {

    private static final Logger LOG = LoggerFactory.getLogger(StableDataScenario.class);

    private static TestUtils TEST_UTILS = new TestUtils();

    private final Integer MAX_TRIALS = 100;
    private final Integer WAITING_TIME_BETWEEN_TRIALS = 5;

    private static String BASE64_STABLE_DATA;

    @BeforeClass
    public static void init() throws Exception {
        TEST_UTILS.initialize(EndToEndScenario.class.getResource("/test.it.properties"));

        final byte[] bytes = IOUtils.toByteArray(StableDataScenario.class.getClassLoader().getResourceAsStream("stable_data.csv"));
        BASE64_STABLE_DATA = new String(Base64.encodeBase64(bytes), StandardCharsets.UTF_8);
    }

    @Test
    @Category(StableData.class)
    public void testSendStableData() throws Exception {
        final List<Organization> organizations = getOrganizations(Host.HD4DP);
        Organization mainOrg = null;
        for (Organization organization : organizations) {
            if(organization.isMain()) {
                mainOrg = organization;
            }
        }

        LOG.info("main organization: {}", mainOrg);

        final List<JSONObject> hd4resStatusMessages = getHd4resStatusMessages();

        boolean found = false;
        for (JSONObject statusMessage : hd4resStatusMessages) {
            final String type = statusMessage.getJSONObject("healthDataIdentification").getString("type");
            final String value = statusMessage.getJSONObject("healthDataIdentification").getString("value");
            if(type.equals(mainOrg.getHealthDataIDType()) && value.equals(mainOrg.getHealthDataIDValue())) {
                found = true;
                break;
            }
        }

        if(!found) {
            throw new Exception(MessageFormat.format("No status message found for organization {0}", mainOrg));
        }

        final String dataCollectionName = "TEST";

        // 1. upload stable data in HD4RES
        final Date uploadedTime = new Date();
        List<Long> ids = uploadStableData(dataCollectionName, mainOrg.getHealthDataIDType(), mainOrg.getHealthDataIDValue());

        // 2. verify stable data have status NEW OR SENT
        verifyStableDataStatus(ids, "NEW", "SENT");

        // 3. verify stable data status is updated to UPLOADED in HD4RES
        waitForStableDataStatus(ids, "UPLOADED");

        // 4. verify stable data is uploaded in HD4DP
        String patientId = "19800610DJM";
        StableDataDto stableData = getStableData(Host.HD4DP, patientId, dataCollectionName);
        Assert.assertEquals(patientId, stableData.getPatientId());
        Assert.assertEquals(dataCollectionName, stableData.getDataCollectionName());
        Assert.assertTrue(stableData.getValidFrom().compareTo(uploadedTime) > 0); // verify new stable data has been uploaded

        patientId = "20001220JJM";
        stableData = getStableData(Host.HD4DP, patientId, dataCollectionName);
        Assert.assertEquals(patientId, stableData.getPatientId());
        Assert.assertEquals(dataCollectionName, stableData.getDataCollectionName());
        Assert.assertTrue(stableData.getValidFrom().compareTo(uploadedTime) > 0); // verify new stable data has been uploaded
    }

    private List<Organization> getOrganizations(Host host) {
        final WebResource webResource = TEST_UTILS.createWebResource("{0}/organizations", host);
        return webResource.get(new GenericType<List<Organization>>() {});
    }

    private List<JSONObject> getHd4resStatusMessages() {
        final WebResource webResource = TEST_UTILS.createWebResource("{0}/statusmessages", Host.HD4RES);
        final String responseAsString = webResource.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer" + TEST_UTILS.getAccessToken(Host.HD4RES, HealthDataRole.ADMIN)).get(String.class);
        final JSONArray array = JsonUtils.createJsonArray(responseAsString);
        final List<JSONObject> result = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            result.add(JsonUtils.getJSONObject(array, i));
        }
        return result;
    }

    private List<Long> uploadStableData(String dataCollectionName, String hdType, String hdValue) throws JSONException {
        LOG.info("uploading stable data for organization [{} - {}] and data collection name {}", hdType, hdValue, dataCollectionName);

        final JSONObject healthDataIdentification = new JSONObject();
        healthDataIdentification.put("type", hdType);
        healthDataIdentification.put("value", hdValue);

        final JSONObject stableDataRequest = new JSONObject();
        stableDataRequest.put("receiver", healthDataIdentification);
        stableDataRequest.put("dataCollectionName", dataCollectionName);
        stableDataRequest.put("patientIdFormat", "batch");
        stableDataRequest.put("base64Csv", BASE64_STABLE_DATA);

        final WebResource webResource = TEST_UTILS.createWebResource("{0}/stabledata/upload", Host.HD4RES);
        final ClientResponse response = webResource.type(MediaType.APPLICATION_JSON_TYPE)
                .header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer" + TEST_UTILS.getAccessToken(Host.HD4RES, HealthDataRole.ADMIN))
                .post(ClientResponse.class, stableDataRequest);

        Assert.assertEquals(200, response.getStatus());

        final JSONArray result = JsonUtils.createJsonArray(response.getEntity(String.class));
        final List<Long> ids = new ArrayList<>();
        for (int i = 0; i < result.length(); i++) {
            final JSONObject jsonObject = result.getJSONObject(i);
            ids.add(jsonObject.getLong("id"));
        }
        return ids;
    }

    private void waitForStableDataStatus(List<Long> ids, String... possibleStatuses) throws JSONException, InterruptedException {
        LOG.info("waiting for stable data status to become one of {}", Arrays.toString(possibleStatuses));
        boolean statusesOk = false;
        for (int i = 0; i < MAX_TRIALS && !statusesOk; i++) {
            try {
                verifyStableDataStatus(ids, possibleStatuses);
                statusesOk = true;
            } catch (AssertionError e) {
                Thread.sleep(WAITING_TIME_BETWEEN_TRIALS * 1000);
            }
        }
    }

    private void verifyStableDataStatus(List<Long> ids, String... possibleStatuses) throws JSONException {
        LOG.debug("verifying stable data status is one of {}", Arrays.toString(possibleStatuses));
        final List<String> possibleStatusesList = Arrays.asList(possibleStatuses);
        for (final Long id : ids) {
            final WebResource webResource = TEST_UTILS.createWebResource("{0}/stabledata/{1}", Host.HD4RES, id.toString());
            final ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_TYPE)
                    .header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer" + TEST_UTILS.getAccessToken(Host.HD4RES, HealthDataRole.ADMIN))
                    .get(ClientResponse.class);

            Assert.assertEquals(200, response.getStatus());
            final JSONObject jsonObject = response.getEntity(JSONObject.class);
            if(!possibleStatusesList.contains(jsonObject.getString("status"))) {
                LOG.error("Expected status is one of {}, but was {}", possibleStatusesList, jsonObject.getString("status"));
                Assert.assertTrue(false);
            }
        }
    }

    private StableDataDto getStableData(Host host, String patientId, String dataCollectionName) {
        final WebResource webResource = TEST_UTILS.createWebResource("{0}/stabledata/{1}/{2}", host, patientId, dataCollectionName);
        final ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_TYPE)
                .header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer" + TEST_UTILS.getAccessToken(host, HealthDataRole.USER))
                .get(ClientResponse.class);

        Assert.assertEquals(200, response.getStatus());

        final StableDataDto stableData = response.getEntity(StableDataDto.class);
        return stableData;
    }
}
