/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.integration_tests.hd4dp;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.integration_tests.utils.UserUtils;
import be.wiv_isp.healthdata.integration_tests.utils.enums.HealthDataRole;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.gathering.dto.RegistrationWorkflowDto;
import be.wiv_isp.healthdata.integration_tests.utils.TestUtils;
import be.wiv_isp.healthdata.integration_tests.utils.WorkflowUtils;
import be.wiv_isp.healthdata.integration_tests.utils.enums.DataCollection;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import be.wiv_isp.healthdata.orchestration.dto.AttachmentDto;
import be.wiv_isp.healthdata.orchestration.dto.UserDto;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.MultiPart;
import com.sun.jersey.multipart.file.FileDataBodyPart;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class AttachmentRestServiceIT {

	private static final Logger LOG = LoggerFactory.getLogger(AttachmentRestServiceIT.class);

	private static TestUtils TEST_UTILS = new TestUtils();
	private static WorkflowUtils WORKFLOW_UTILS;
	private static UserUtils USER_UTILS;

	@BeforeClass
	public static void init() throws Exception {
		TEST_UTILS.initialize(AttachmentRestServiceIT.class.getResource("/test.it.properties"));
		WORKFLOW_UTILS = new WorkflowUtils(TEST_UTILS);
		USER_UTILS = new UserUtils(TEST_UTILS);
	}

	@Test
	public void testUploadGetAndDelete() throws Exception {
		final RegistrationWorkflowDto workflow = WORKFLOW_UTILS.createWorkflow(DataCollection.TEST_FOLLOW_UP);

		final AttachmentDto attachment = buildAttachment(workflow.getIdAsLong());
		final File file = new File(this.getClass().getResource("/attachments/attachment1.txt").toURI());
		final byte[] fileContent = FileUtils.readFileToByteArray(file);
		final AttachmentDto created = uploadAttachment(attachment, file, workflow.getIdAsLong());

		final AttachmentDto retrieved = getAttachment(Host.HD4DP, attachment.getUuid());
		Assert.assertNotNull(retrieved);
		Assert.assertEquals(attachment.getUuid(), created.getUuid());
		Assert.assertEquals("attachment1.txt", retrieved.getFilename());
		Assert.assertEquals(DigestUtils.md5Hex(fileContent), retrieved.getHash());
		Assert.assertEquals("text/plain", retrieved.getMimeType());

		byte[] downloadedContent = downloadAttachment(Host.HD4DP, HealthDataRole.USER, attachment.getUuid(), 200);
		Assert.assertArrayEquals(fileContent, downloadedContent);

		downloadedContent = downloadAttachment(Host.HD4DP, HealthDataRole.USER, attachment.getUuid(), 200);
		Assert.assertArrayEquals(fileContent, downloadedContent);

		deleteAttachment(attachment);
		getAttachment(Host.HD4DP, HealthDataRole.USER, attachment.getUuid(), ClientResponse.Status.NOT_FOUND.getStatusCode());

		WORKFLOW_UTILS.deleteWorkflow(workflow.getIdAsLong());
	}

	@Test
	public void testUploadAndGetWithUnauthorizedUser() throws Exception {
		final RegistrationWorkflowDto workflow = WORKFLOW_UTILS.createWorkflow(DataCollection.TEST_FOLLOW_UP); // TEST_A is used as it is a non-public data collection

		final AttachmentDto attachment = buildAttachment(workflow.getIdAsLong());
		final File file = new File(this.getClass().getResource("/attachments/attachment1.txt").toURI());
		uploadAttachment(attachment, file, workflow.getIdAsLong());

		final UserDto user = USER_UTILS.buildUser();
		final Organization mainOrganization = TEST_UTILS.getOrganizationUtils().getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);
		final String accessToken = TEST_UTILS.getAccessToken(Host.HD4DP, mainOrganization.getId(), createdUser.getUsername(), createdUser.getPassword());

		getAttachment(Host.HD4DP, attachment.getUuid(), accessToken, ClientResponse.Status.UNAUTHORIZED.getStatusCode());

		WORKFLOW_UTILS.deleteWorkflow(workflow.getIdAsLong());
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	@Test
	public void testAttachmentCanNotBeRetrievedIfWorkflowIsDeleted() throws Exception {
		final RegistrationWorkflowDto workflow = WORKFLOW_UTILS.createWorkflow(DataCollection.TEST_FOLLOW_UP);

		final AttachmentDto attachment = buildAttachment(workflow.getIdAsLong());
		final File file = new File(this.getClass().getResource("/attachments/attachment1.txt").toURI());
		uploadAttachment(attachment, file, workflow.getIdAsLong());

		WORKFLOW_UTILS.deleteWorkflow(workflow.getIdAsLong());

		getAttachment(Host.HD4DP, HealthDataRole.USER, attachment.getUuid(), ClientResponse.Status.NOT_FOUND.getStatusCode());
	}

	@Test
	public void testSendAttachmentToHd4res() throws Exception {
		final RegistrationWorkflowDto workflow = WORKFLOW_UTILS.createWorkflow(DataCollection.TEST_FOLLOW_UP);
		final byte[] bytes = IOUtils.toByteArray(this.getClass().getResourceAsStream("/endToEndScenario/acceptable_document.json"));
		final JSONObject jsonContent = JsonUtils.createJsonObject(bytes);
		save(workflow, jsonContent);

		final File file1 = new File(this.getClass().getResource("/attachments/attachment1.txt").toURI());
		final File file2 = new File(this.getClass().getResource("/attachments/attachment2.txt").toURI());

		final AttachmentDto attachment1 = buildAttachment(workflow.getIdAsLong());
		final AttachmentDto attachment2 = buildAttachment(workflow.getIdAsLong());
		final AttachmentDto attachment3 = buildAttachment(workflow.getIdAsLong());

		final AttachmentDto hd4dpAttachment1 = uploadAttachment(attachment1, file1, workflow.getIdAsLong());
		WORKFLOW_UTILS.submit(workflow);

		RegistrationWorkflowDto hd4resWorkflow = TEST_UTILS.waitForWorkflowToBeCreatedOnHd4res(workflow);
		Assert.assertEquals(1, hd4resWorkflow.getAttachments().size());
		final AttachmentDto hd4resAttachment1 = hd4resWorkflow.getAttachments().get(0);
		compareAttachments(hd4dpAttachment1, getAttachment(Host.HD4RES, hd4resAttachment1.getUuid()));

		// add second attachment and submit
		WORKFLOW_UTILS.reOpen(workflow);
		final AttachmentDto hd4dpAttachment2 = uploadAttachment(attachment2, file1, workflow.getIdAsLong());
		final AttachmentDto hd4dpAttachment3 = uploadAttachment(attachment3, file2, workflow.getIdAsLong());
		WORKFLOW_UTILS.submit(workflow);

		byte[] downloadedContent = downloadAttachment(Host.HD4DP, HealthDataRole.USER, hd4resAttachment1.getUuid(), 200);
		Assert.assertArrayEquals(FileUtils.readFileToByteArray(file1), downloadedContent);

		hd4resWorkflow = waitForAttachmentsBeingUpdated(Host.HD4RES, hd4resWorkflow, 3);
		for (final AttachmentDto attachmentDto : hd4resWorkflow.getAttachments()) {
			if(attachmentDto.getUuid().equals(hd4dpAttachment1)) {
				compareAttachments(hd4dpAttachment1, getAttachment(Host.HD4RES, attachmentDto.getUuid()));
			}
			if(attachmentDto.getUuid().equals(hd4dpAttachment2)) {
				compareAttachments(hd4dpAttachment2, getAttachment(Host.HD4RES,  attachmentDto.getUuid()));
			}
			if(attachmentDto.getUuid().equals(hd4dpAttachment3)) {
				compareAttachments(hd4dpAttachment3, getAttachment(Host.HD4RES, attachmentDto.getUuid()));
			}
		}

		WORKFLOW_UTILS.reOpen(workflow);
		deleteAttachment(attachment1);
		deleteAttachment(attachment3);
		WORKFLOW_UTILS.submit(workflow);

		hd4resWorkflow = waitForAttachmentsBeingUpdated(Host.HD4RES, hd4resWorkflow, 1);
		compareAttachments(hd4dpAttachment2, getAttachment(Host.HD4RES, hd4resWorkflow.getAttachments().get(0).getUuid()));
	}

	private RegistrationWorkflowDto waitForAttachmentsBeingUpdated(Host host, RegistrationWorkflowDto workflow, int expectedNumberOfAttachment) throws Exception {
		LOG.info("Waiting for attachments being updated");
		boolean updated = false;
		for (int i = 0; i < TestUtils.MAX_TRIALS && !updated; i++) {
			workflow = WORKFLOW_UTILS.getWorkflow(host, workflow.getIdAsLong());

			if(workflow.getAttachments().size() == expectedNumberOfAttachment) {
				updated = true;
			} else {
				TEST_UTILS.waitBetweenTrials();
			}
		}
		Assert.assertEquals(expectedNumberOfAttachment, workflow.getAttachments().size());
		return workflow;
	}

	private void compareAttachments(AttachmentDto attachment1, AttachmentDto attachment2) {
		Assert.assertEquals(attachment1.getUuid(), attachment2.getUuid());
		Assert.assertEquals(attachment1.getHash(), attachment2.getHash());
		Assert.assertEquals(attachment1.getFilename(), attachment2.getFilename());
		Assert.assertArrayEquals(attachment1.getZippedContent(), attachment2.getZippedContent());
		Assert.assertEquals(attachment1.getSize(), attachment2.getSize());
	}

	private AttachmentDto uploadAttachment(AttachmentDto attachment, File file, Long workflowId) {
		LOG.info("Uploading attachment {}", attachment);

		final FileDataBodyPart filePart = new FileDataBodyPart("file", file);

		final MultiPart multipartEntity = new FormDataMultiPart()
			.field("workflowId", workflowId.toString())
			.bodyPart(filePart);

		final WebResource wr = TEST_UTILS.createWebResource("{0}/attachments/upload/{1}", Host.HD4DP, attachment.getUuid());
		return wr.header("Authorization", "Bearer " + TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER))
				.type(MediaType.MULTIPART_FORM_DATA)
				.post(AttachmentDto.class, multipartEntity);
	}

	private AttachmentDto getAttachment(Host host, String uuid) {
		return getAttachment(host, HealthDataRole.USER, uuid, ClientResponse.Status.OK.getStatusCode());
	}

	private AttachmentDto getAttachment(Host host, HealthDataRole role, String uuid, int expectedCode) {
		return getAttachment(host, uuid, TEST_UTILS.getAccessToken(host, role), expectedCode);
	}

	private AttachmentDto getAttachment(Host host, String uuid, String accessToken, int expectedCode) {
		LOG.info("Retrieving attachment with uuid {}", uuid);
		final WebResource wr = TEST_UTILS.createWebResource("{0}/attachments/info/{1}", host, uuid);
		final ClientResponse response = wr
				.header("Authorization", "Bearer " + accessToken)
				.type(MediaType.APPLICATION_JSON_TYPE)
				.get(ClientResponse.class);
		Assert.assertEquals(expectedCode, response.getStatus());
		if(response.getStatus() == 200) {
			return response.getEntity(AttachmentDto.class);
		}
		return null;
	}

	private byte[] downloadAttachment(Host host, HealthDataRole role, String uuid, int expectedCode) {
		LOG.info("Retrieving attachment with uuid {}", uuid);
		final WebResource wr = TEST_UTILS.createWebResource("{0}/attachments/download/{1}", host, uuid);
		final ClientResponse response = wr.header("Authorization", "Bearer " + TEST_UTILS.getAccessToken(host, role)).type(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
		Assert.assertEquals(expectedCode, response.getStatus());
		if(response.getStatus() == 200) {
			return response.getEntity(byte[].class);
		}
		return null;
	}

	private void deleteAttachment(AttachmentDto attachment) {
		LOG.info("Deleting attachment with uuid {}", attachment.getUuid());
		final WebResource wr = TEST_UTILS.createWebResource("{0}/attachments/delete/{1}", Host.HD4DP, attachment.getUuid());
		wr.header("Authorization", "Bearer " + TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.USER)).type(MediaType.APPLICATION_JSON_TYPE).delete();
	}

	private String generateUuid() {
		return UUID.randomUUID().toString();
	}

	private void save(RegistrationWorkflowDto workflow, JSONObject content) throws Exception {
		final JSONObject requestBody = new JSONObject();
		requestBody.put("action", "SAVE");
		requestBody.put("documentContent", content);
		final Map<String, String> metadata = new HashMap<>();
		metadata.put("PATIENT_IDENTIFIER", "12345678910");
		requestBody.put("metaData", metadata);
		requestBody.put("computedExpressions", new JSONObject());
		requestBody.put("workflowId", workflow.getId().toString());

		WORKFLOW_UTILS.executeAction(requestBody);
	}

	private AttachmentDto buildAttachment(Long workflowId) {
		final AttachmentDto attachment = new AttachmentDto();
		attachment.setUuid(generateUuid());
		//attachment.setWorkflowId(workflowId);
		return attachment;
	}
}