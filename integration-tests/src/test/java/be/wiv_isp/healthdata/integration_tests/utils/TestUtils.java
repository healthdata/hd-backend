/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.integration_tests.utils;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.dto.ConfigurationDto;
import be.wiv_isp.healthdata.common.api.ClientVersion;
import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.integration_tests.utils.enums.HealthDataRole;
import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.gathering.dto.RegistrationWorkflowDto;
import be.wiv_isp.healthdata.integration_tests.utils.enums.ConnectionStatus;
import be.wiv_isp.healthdata.integration_tests.utils.enums.DataCollection;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONException;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.*;

public class TestUtils {

	public static Logger LOG = LoggerFactory.getLogger(TestUtils.class);

	public static final Integer MAX_TRIALS = 100;
	public static final Integer WAITING_TIME_BETWEEN_TRIALS = 5;

	private Map<Host, String> hostUrls = new HashMap<>();
    private Map<Host, Map<ConfigurationKey, ConfigurationDto>> configurations = new HashMap();
	private Map<UserIdentification, String> accessTokens = new HashMap<>();
	private Map<DataCollection, Long> dataCollectionDefinitionIds = new HashMap<>();
	private Map<Host, ConnectionStatus> connectionStatusFrom = new HashMap<>();

	private Client client;
	private Properties properties = new Properties();
	private Properties userProperties = new Properties();

	private UserUtils userUtils;
	private ViewUtils viewUtils;
	private ConfigurationUtils configurationUtils;
	private DataCollectionAccessRequestUtils dataCollectionAccessRequestUtils;
	private WorkflowUtils workflowUtils;
	private OrganizationUtils organizationUtils;
	private TokenUtils tokenUtils;

	public void initialize(URL propertiesUrl) throws IOException, JSONException {
		LOG.info("Initializing Test Utils");

		final ClientConfig config = new DefaultClientConfig();
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		client = Client.create(config);

		properties.load(propertiesUrl.openStream());
		userProperties.load(TestUtils.class.getResourceAsStream("/users.properties"));

		hostUrls.put(Host.HD4DP, getProperty("server.host"));
		hostUrls.put(Host.HD4RES, getProperty("hd4res.host"));
		hostUrls.put(Host.CATALOGUE, getProperty("catalogue.host"));
		hostUrls.put(Host.MOCK, getProperty("mock.host"));

		for(final Host host : Host.values()) {
			LOG.info(host + ": " + hostUrls.get(host));
			configurations.put(host, new HashMap<ConfigurationKey, ConfigurationDto>());
			connectionStatusFrom.put(host, ConnectionStatus.UP);
		}

		for (final DataCollection dataCollection : DataCollection.values()) {
			try {
				dataCollectionDefinitionIds.put(dataCollection, doGetDataCollectionDefinitionId(dataCollection));
			} catch (Exception e) {
				LOG.warn(e.getMessage(), e);
				LOG.warn("No data collection definition found for data collection [{}]", dataCollection);
			}
		}

		LOG.info("Data collection definition ids: {}", dataCollectionDefinitionIds);

		userUtils = new UserUtils(this);
		viewUtils = new ViewUtils(this);
		dataCollectionAccessRequestUtils = new DataCollectionAccessRequestUtils(this);
		configurationUtils = new ConfigurationUtils(this);
		workflowUtils = new WorkflowUtils(this);
		organizationUtils = new OrganizationUtils(this);
		tokenUtils = new TokenUtils(this);

		LOG.info("Test Utils initialized");
	}

	public UserUtils getUserUtils() {
		return userUtils;
	}

	public ViewUtils getViewUtils() {
		return viewUtils;
	}

	public DataCollectionAccessRequestUtils getDataCollectionAccessRequestUtils() {
		return dataCollectionAccessRequestUtils;
	}

	public WorkflowUtils getWorkflowUtils() {
		return workflowUtils;
	}

	public ConfigurationUtils getConfigurationUtils() {
		return configurationUtils;
	}

	public OrganizationUtils getOrganizationUtils() {
		return organizationUtils;
	}

	public TokenUtils getTokenUtils() {
		return tokenUtils;
	}

	private String getProperty(String key) {
		final String value = System.getProperty(key);
		return StringUtils.isNotEmpty(value) ? value : properties.getProperty(key);
	}

	private String getUsersProperty(String key) {
		final String value = System.getProperty(key);
		return StringUtils.isNotEmpty(value) ? value : userProperties.getProperty(key);
	}

	public String getHostUrl(Host host) {
		return hostUrls.get(host);
	}

	public Long getDataCollectionDefinitionId(final DataCollection dataCollection) {
		return dataCollectionDefinitionIds.get(dataCollection);
	}

	public void saveConfiguration(Host host, ConfigurationKey key) throws Exception {
		LOG.info(MessageFormat.format("Saving configuration for host [{0}] and key [{1}]", host, key));

		final ConfigurationDto configuration = configurationUtils.getConfiguration(host, key);

		if(configurations.get(host).get(key) == null) {
			configurations.get(host).put(key, configuration);
		}
	}

    public void setConfiguration(Host host, ConfigurationKey key, String value) throws Exception {
		LOG.info(MessageFormat.format("Setting configuration for host [{0}] and key [{1}] to [{2}]", host, key, value));

		saveConfiguration(host, key);
		final String token = getAccessToken(host, HealthDataRole.ADMIN);

		ConfigurationDto configuration = configurationUtils.getConfiguration(host, key);
		configuration.setValue(value);
		configurationUtils.updateConfiguration(host, configuration.getId(), configuration, token);
	}

	public String getAccessToken(Host host, Long organizationId, String username, String password) {
		final UserIdentification userIdentification = new UserIdentification(host, organizationId, username);
		if(accessTokens.get(userIdentification) == null) {
			accessTokens.put(userIdentification, tokenUtils.getAccessToken(host, username, password, organizationId));
		}
		return accessTokens.get(userIdentification);
	}

	public String getAccessToken(Host host, Long organizationId, HealthDataRole role) {
		return getAccessToken(host, organizationId, getUsername(role), getPassword(role));
	}

	public String getAccessToken(Host host, HealthDataRole role) {
		final Organization mainOrganization = organizationUtils.getMainOrganization(host);
		return getAccessToken(host, mainOrganization.getId(), role);
	}

	private String getUsername(HealthDataRole role) {
		switch (role) {
			case USER:
				return getUsersProperty("usernameUser");
			case ADMIN:
				return getUsersProperty("usernameAdmin");
		}
		throw new RuntimeException("Invalid role");
	}

	private String getPassword(HealthDataRole role) {
		switch (role) {
			case USER:
				return getUsersProperty("passwordUser");
			case ADMIN:
				return getUsersProperty("passwordAdmin");
		}
		throw new RuntimeException("Invalid role");
	}

    public void restoreConfiguration(Host host, ConfigurationKey key) throws Exception {
		LOG.info(MessageFormat.format("Resetting configuration for host [{0}] and key [{1}].", host, key));
		final ConfigurationDto configuration = configurations.get(host).get(key);

		if (configuration == null) {
			LOG.warn(MessageFormat.format("No configuration was saved for host [{0}] and key [{1}]. Doing nothing.", host, key));
			return;
		}

		setConfiguration(host, key, configuration.getValue());
	}

	public void restoreAllConfigurations() throws Exception {
		LOG.info("Resetting all configurations");

        for(Map.Entry<Host, Map<ConfigurationKey, ConfigurationDto>> configurationsByHost : configurations.entrySet()) {
			final Host host = configurationsByHost.getKey();
            final Map<ConfigurationKey, ConfigurationDto> configurationsMap = configurationsByHost.getValue();

			LOG.info(MessageFormat.format("Resetting configurations for host [{0}]", host));
            for(Map.Entry<ConfigurationKey, ConfigurationDto> configurationsEntry : configurationsMap.entrySet()) {
				final ConfigurationDto configuration = configurationsEntry.getValue();

				LOG.info(MessageFormat.format("Resetting [{0}] configuration to [{1}]", configuration.getKey(), configuration.getValue()));

				final WebResource wr = createWebResource("{0}/configurations/{1}", host, String.valueOf(configuration.getId()));

				String accessToken = getAccessToken(host, HealthDataRole.ADMIN);

				ClientResponse response = wr.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + accessToken) //
						.type(MediaType.APPLICATION_JSON_TYPE) //
						.accept(MediaType.APPLICATION_JSON_TYPE) //
						.put(ClientResponse.class, configuration);

				if (Response.Status.UNAUTHORIZED.getStatusCode() == response.getStatus()) {
					LOG.warn("Access token is expired, retrieving new access token");
					accessToken = refreshToken(host, HealthDataRole.ADMIN);

					response = wr.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + accessToken) //
							.type(MediaType.APPLICATION_JSON_TYPE) //
							.accept(MediaType.APPLICATION_JSON_TYPE) //
							.put(ClientResponse.class, configuration);
				}

				if (Response.Status.OK.getStatusCode() != response.getStatus()) {
					LOG.error(MessageFormat.format("Updating configuration returned a HTTP code {0}", response.getStatus()));
				}
			}

			configurationsMap.clear();
		}
	}

	private String refreshToken(Host host, HealthDataRole role) {
		final Organization mainOrganization = organizationUtils.getMainOrganization(host);
		final UserIdentification userIdentification = new UserIdentification(host, mainOrganization.getId(), getUsername(role));
		accessTokens.put(userIdentification, null);
		return getAccessToken(host, role);
	}

	public WebResource createWebResource(final String urlPattern, final Host host, String... parameters) {
		final List<Object> params = new ArrayList<>();
		params.add(getHostUrl(host));
		params.addAll(Arrays.asList(parameters));
		final String url = MessageFormat.format(urlPattern, params.toArray());

		return client.resource(encodeUrl(url));
	}

	public void setConnectionBetweenHd4dpAndHd4res(ConnectionStatus status) {
		LOG.info(MessageFormat.format("setting connection status between HD4DP and HD4RES to [{0}]", status));
		final WebResource wr = createWebResource("{0}/rest/em/connection/hd4dptohd4res/{1}", Host.MOCK, status.toString());
		final ClientResponse response = wr.accept(MediaType.APPLICATION_JSON_TYPE).post(new GenericType<ClientResponse>() {});
		Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		connectionStatusFrom.put(Host.HD4DP, status);
	}

	public void setConnectionBetweenHd4resAndHd4dp(ConnectionStatus status) {
		LOG.info(MessageFormat.format("setting connection status between HD4RES and HD4DP to [{0}]", status));
		final WebResource wr = createWebResource("{0}/rest/em/connection/hd4restohd4dp/{1}", Host.MOCK, status.toString());
		final ClientResponse response = wr.accept(MediaType.APPLICATION_JSON_TYPE).post(new GenericType<ClientResponse>() {});
		Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		connectionStatusFrom.put(Host.HD4RES, status);
	}

	private Long doGetDataCollectionDefinitionId(DataCollection dataCollection) {
		final WebResource wr = createWebResource("{0}/datacollectiondefinitions/latestmajors/{1}", Host.CATALOGUE, dataCollection.toString());

		final List<DataCollectionDefinitionDtoV7> list = wr
			.type(MediaType.APPLICATION_JSON_TYPE)
			.header(HttpHeaders.CLIENT_VERSION.getValue(), ClientVersion.DataCollectionDefinition.getDefault())
			.get(new GenericType<List<DataCollectionDefinitionDtoV7>>() {});

		Collections.sort(list, new Comparator<DataCollectionDefinitionDtoV7>() {
			@Override
			public int compare(DataCollectionDefinitionDtoV7 dcd1, DataCollectionDefinitionDtoV7 dcd2) {
				return dcd2.getVersion().compareTo(dcd1.getVersion());
			}
		});

		for (final DataCollectionDefinitionDtoV7 dcd : list) {
			if(!isStartDateValid(dcd.getDataCollectionGroup().getStartDate())) {
				continue;
			}
			if(!isEndDateValid(dcd.getDataCollectionGroup().getEndDateCreation())) {
				continue;
			}
			if(!isEndDateValid(dcd.getDataCollectionGroup().getEndDateSubmission())) {
				continue;
			}
			if(!isEndDateValid(dcd.getDataCollectionGroup().getEndDateComments())) {
				continue;
			}

			return dcd.getId();
		}
		return 0L;
	}

	private boolean isStartDateValid(Timestamp startDate) {
		return startDate == null || startDate.getTime() < new Date().getTime();
	}

	private boolean isEndDateValid(Timestamp endDate) {
		return endDate == null || endDate.getTime() > new Date().getTime();
	}

	private URI encodeUrl(String url) {
		try {
			return new URI(null, url, null);
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}

	public RegistrationWorkflowDto waitForWorkflowToBeCreatedOnHd4res(RegistrationWorkflowDto hd4dpWorkflow) throws Exception {
		LOG.info(MessageFormat.format("waiting for workflow (hd4dp id: {0}) to be created on hd4res", hd4dpWorkflow.getId()));

		for (int i=1; i <= MAX_TRIALS; ++i) {
			final RegistrationWorkflowDto workflow = workflowUtils.retrieveHd4resWorkflow(hd4dpWorkflow);
			if(workflow != null) {
				return workflow;
			}

			waitBetweenTrials();

			if(ConnectionStatus.ASYNCHRONOUS.equals(connectionStatusFrom.get(Host.HD4DP))) {
				LOG.info("Connection is ASYNCHRONOUS, and registrations seem to be not transferred to HD4RES. Transferring messages again.");
				transferAsyncMessages(Platform.HD4DP, Platform.HD4RES);
			}
		}
		throw new Exception(MessageFormat.format("hd4res workflow (hd4dp id: {0}) not found", hd4dpWorkflow.getId()));
	}

	public void waitBetweenTrials() throws InterruptedException {
		Thread.sleep(WAITING_TIME_BETWEEN_TRIALS * 1000);
	}

	public void transferAsyncMessages(final Platform src, final Platform dest) {
		LOG.info("transferring asynchronous messages from {} to {}", src, dest);
		final WebResource wr = createWebResource("{0}/rest/em/{1}/async/send", Host.MOCK, src.toString().toLowerCase());
		final ClientResponse response = wr.accept(MediaType.TEXT_PLAIN_TYPE).post(ClientResponse.class);
		Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
	}

	private class UserIdentification {
		private Host host;
		private Long organizationId;
		private String username;

		public UserIdentification(Host host, Long organizationId, String username) {
			this.host = host;
			this.organizationId = organizationId;
			this.username = username;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (!(o instanceof UserIdentification)) return false;

			UserIdentification that = (UserIdentification) o;

			if (host != that.host) return false;
			if (organizationId != null ? !organizationId.equals(that.organizationId) : that.organizationId != null)
				return false;
			return !(username != null ? !username.equals(that.username) : that.username != null);

		}

		@Override
		public int hashCode() {
			int result = host != null ? host.hashCode() : 0;
			result = 31 * result + (organizationId != null ? organizationId.hashCode() : 0);
			result = 31 * result + (username != null ? username.hashCode() : 0);
			return result;
		}
	}
}
