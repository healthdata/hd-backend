/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.integration_tests.endtoend;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.UserManagementType;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.integration_tests.domain.SerializableUserDto;
import be.wiv_isp.healthdata.integration_tests.hd4dp.UserRestServiceIT;
import be.wiv_isp.healthdata.integration_tests.utils.TestUtils;
import be.wiv_isp.healthdata.integration_tests.utils.UserUtils;
import be.wiv_isp.healthdata.integration_tests.utils.enums.HealthDataRole;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.dto.UserDto;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.SimpleDateFormat;
import java.util.*;

public class LdapAuthenticationIT {
	public static final String LDAP_USER = "ldap_user_it";
	private static final String PASSWORD = "pazz1";

	private static TestUtils TEST_UTILS;
	private static UserUtils USER_UTILS;

	@BeforeClass
	public static void init() throws Exception {
		TEST_UTILS = new TestUtils();
		TEST_UTILS.initialize(UserRestServiceIT.class.getResource("/test.it.properties"));
		USER_UTILS = TEST_UTILS.getUserUtils();
		TEST_UTILS.setConfiguration(Host.HD4DP, ConfigurationKey.USER_MANAGEMENT_TYPE, UserManagementType.DATABASE_LDAP.toString());
	}

	@AfterClass
	public static void tearDown() throws Exception {
		TEST_UTILS.restoreAllConfigurations();
	}

	@Test
	public void testCreateUserAndUpdateMetadata() throws Exception {
		final List<SerializableUserDto> users = getLdapUsers(LDAP_USER);
		System.out.println("users = " + users);

		final SerializableUserDto userDto = users.get(0);
		Assert.assertNull(userDto.getId()); // verify the user does not exist in the database

		userDto.setAuthorities(new HashSet<>(Collections.singletonList(new Authority(Authority.USER))));
		userDto.setDataCollectionNames(new HashSet<>(Collections.singletonList("TEST")));

		final Organization mainOrganization = TEST_UTILS.getOrganizationUtils().getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), userDto);
		Assert.assertNotNull(createdUser.getId());

		final String accessToken = TEST_UTILS.getAccessToken(Host.HD4DP, mainOrganization.getId(), createdUser.getUsername(), PASSWORD);
		USER_UTILS.getMe(Host.HD4DP, accessToken);

		final Map<String, String> metaData = new HashMap<>();
		metaData.put("timestamp", new SimpleDateFormat(DateFormat.TIMESTAMP.toString()).format(new Date()));
		final WebResource wr = TEST_UTILS.createWebResource("{0}/users/metadata/{1}", Host.HD4DP, createdUser.getId().toString());
		final ClientResponse response = wr.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + accessToken).type(MediaType.APPLICATION_JSON_TYPE).accept(MediaType.APPLICATION_JSON_TYPE).put(ClientResponse.class, metaData);
		Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		final UserDto updatedUser = USER_UTILS.getMe(Host.HD4DP, accessToken);
		Assert.assertEquals(metaData, updatedUser.getMetaData());

		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	@Test
	public void testAdminCanAlwaysLogin() {
		final String accessToken = TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN);
		Assert.assertNotNull(accessToken);
		USER_UTILS.getMe(Host.HD4DP, accessToken);
	}

	private List<SerializableUserDto> getLdapUsers(String searchTerm) {
		final WebResource wr = TEST_UTILS.createWebResource("{0}/ldap/users", Host.HD4DP);
		return wr
				.queryParam("search", searchTerm)
				.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + TEST_UTILS.getAccessToken(Host.HD4DP, HealthDataRole.ADMIN))
				.get(new GenericType<List<SerializableUserDto>>(){});
	}
}
