/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.integration_tests.hd4dp;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.integration_tests.utils.TestUtils;
import be.wiv_isp.healthdata.integration_tests.utils.UserUtils;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import be.wiv_isp.healthdata.orchestration.dto.UserConfigurationDto;
import be.wiv_isp.healthdata.orchestration.dto.UserDto;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import java.util.List;

public class UserConfigurationRestServiceIT {

	private static TestUtils TEST_UTILS;
	private static UserUtils USER_UTILS;

	@BeforeClass
	public static void init() throws Exception {
		TEST_UTILS = new TestUtils();
		TEST_UTILS.initialize(UserRestServiceIT.class.getResource("/test.it.properties"));
		USER_UTILS = TEST_UTILS.getUserUtils();
	}

	@Test
	public void testGetUpdateAndDelete() throws Exception {
		final UserDto user = USER_UTILS.buildUser();
		final Organization mainOrganization = TEST_UTILS.getOrganizationUtils().getMainOrganization(Host.HD4DP);
		final UserDto createdUser = USER_UTILS.createUser(Host.HD4DP, mainOrganization.getId(), user);

		final String token = TEST_UTILS.getAccessToken(Host.HD4DP, mainOrganization.getId(), user.getUsername(), user.getPassword());

		final List<UserConfigurationDto> dtos = getUserConfigurations(Host.HD4DP, token);
		Assert.assertNotNull(dtos);

		final UserConfigurationDto conf = dtos.get(0);
		conf.setValue("NEW_VALUE");
		final UserConfigurationDto updatedConfiguration = updateUserConfiguration(Host.HD4DP, token, conf);
		Assert.assertEquals("NEW_VALUE", updatedConfiguration.getValue());
		USER_UTILS.deleteUser(Host.HD4DP, createdUser);
	}

	private List<UserConfigurationDto> getUserConfigurations(final Host host, final String token) {
		final WebResource wr = TEST_UTILS.createWebResource("{0}/users/configurations", host);
		final ClientResponse response = wr
				.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token)
				.type(MediaType.APPLICATION_JSON).get(ClientResponse.class);

		Assert.assertEquals(ClientResponse.Status.OK.getStatusCode(), response.getStatus());
		return response.getEntity(new GenericType<List<UserConfigurationDto>>() {});
	}

	private UserConfigurationDto updateUserConfiguration(final Host host, final String token, final UserConfigurationDto configurationDto) {
		final WebResource wr = TEST_UTILS.createWebResource("{0}/users/configurations/{1}", host, String.valueOf(configurationDto.getId()));
		final ClientResponse response = wr
				.header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token) //
				.type(MediaType.APPLICATION_JSON) //
				.put(ClientResponse.class, configurationDto);

		Assert.assertEquals(ClientResponse.Status.OK.getStatusCode(), response.getStatus());
		return response.getEntity(UserConfigurationDto.class);
	}
}
