/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.integration_tests.utils;

import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.integration_tests.utils.enums.Host;
import be.wiv_isp.healthdata.orchestration.domain.View;
import be.wiv_isp.healthdata.orchestration.dto.UserDto;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

public class ViewUtils {

    private static final Logger LOG = LoggerFactory.getLogger(ViewUtils.class);

    private TestUtils testUtils;

    public ViewUtils(TestUtils testUtils) {
        this.testUtils = testUtils;
    }

    public List<View> getViews(Host host, Long organizationId, UserDto user, Long dcdId) {
        LOG.info("Retrieving views on {} for user {} and data collection definition id {}", host, user.getId(), dcdId);
        final String token = testUtils.getAccessToken(host, organizationId, user.getUsername(), user.getPassword());

        final WebResource wr = testUtils.createWebResource("{0}/users/views/{1}", host, dcdId.toString());
        final ClientResponse response = wr
                .header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token)
                .type(MediaType.APPLICATION_JSON)
                .get(ClientResponse.class);

        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        return response.getEntity(new GenericType<List<View>>() {});
    }

    public View createView(Host host, Long organizationId, UserDto user, Long dcdId, String viewName, JSONObject viewContent) {
        LOG.info("Creating view [{}] on {} for user {} and data collection definition id {}", viewName, host, user.getId(), dcdId);
        final String token = testUtils.getAccessToken(host, organizationId, user.getUsername(), user.getPassword());

        final WebResource wr = testUtils.createWebResource("{0}/users/views/{1}/{2}", host, dcdId.toString(), viewName);
        final ClientResponse response = wr
                .header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token) //
                .type(MediaType.APPLICATION_JSON) //
                .post(ClientResponse.class, viewContent.toString());

        Assert.assertEquals(ClientResponse.Status.OK.getStatusCode(), response.getStatus());
        final View createdView = response.getEntity(View.class);
        Assert.assertEquals(viewName, createdView.getName());
        Assert.assertEquals(dcdId, createdView.getDataCollectionDefinitionId());
        Assert.assertArrayEquals(viewContent.toString().getBytes(), createdView.getContent());
        return createdView;
    }

    public View updateView(Host host, Long organizationId, UserDto user, Long dcdId, String viewName, JSONObject viewContent) {
        LOG.info("Updating view [{}] on {} for user {} and data collection definition id {}", viewName, host, user.getId(), dcdId);
        final String token = testUtils.getAccessToken(host, organizationId, user.getUsername(), user.getPassword());

        final WebResource wr = testUtils.createWebResource("{0}/users/views/{1}/{2}", host, dcdId.toString(), viewName);
        final ClientResponse response = wr
                .header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token) //
                .type(MediaType.APPLICATION_JSON) //
                .put(ClientResponse.class, viewContent.toString());

        Assert.assertEquals(ClientResponse.Status.OK.getStatusCode(), response.getStatus());

        final View updatedView = response.getEntity(View.class);
        Assert.assertEquals(viewName, updatedView.getName());
        Assert.assertEquals(dcdId, updatedView.getDataCollectionDefinitionId());
        Assert.assertArrayEquals(viewContent.toString().getBytes(), updatedView.getContent());

        return updatedView;
    }

    public void deleteView(Host host, Long organizationId, UserDto user, Long dcdId, String viewName) {
        LOG.info("Deleting view [{}] on {} for user {} and data collection definition id {}", viewName, host, user.getId(), dcdId);
        final String token = testUtils.getAccessToken(host, organizationId, user.getUsername(), user.getPassword());

        final WebResource wr = testUtils.createWebResource("{0}/users/views/{1}/{2}", Host.HD4DP, dcdId.toString(), viewName);
        final ClientResponse response = wr
                .header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token) //
                .type(MediaType.APPLICATION_JSON) //
                .delete(ClientResponse.class);

        Assert.assertEquals(ClientResponse.Status.NO_CONTENT.getStatusCode(), response.getStatus());
    }

    public void createViewDuplicateView(Host host, Long organizationId, UserDto user, Long dcdId, String viewName, JSONObject viewContent) {
        LOG.info("Creating (duplicate) view [{}] on {} for user {} and data collection definition id {}", viewName, host, user.getId(), dcdId);
        final String token = testUtils.getAccessToken(host, organizationId, user.getUsername(), user.getPassword());

        final WebResource wr = testUtils.createWebResource("{0}/users/views/{1}/{2}", Host.HD4DP, dcdId.toString(), viewName);
        final ClientResponse response = wr
                .header(HttpHeaders.AUTHORIZATION.getValue(), "Bearer " + token) //
                .type(MediaType.APPLICATION_JSON) //
                .post(ClientResponse.class, viewContent.toString());

        Assert.assertEquals(ClientResponse.Status.CONFLICT.getStatusCode(), response.getStatus());
    }
}
