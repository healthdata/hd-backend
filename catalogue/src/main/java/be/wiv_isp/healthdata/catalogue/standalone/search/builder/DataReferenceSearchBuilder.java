/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.search.builder;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import be.wiv_isp.healthdata.catalogue.standalone.enumeration.SearchFilterType;
import be.wiv_isp.healthdata.catalogue.standalone.enumeration.SortingType;
import be.wiv_isp.healthdata.catalogue.standalone.search.DataReferenceSearch;

public class DataReferenceSearchBuilder {

	private String type;

	private SearchFilterType filter = SearchFilterType.ALL;

	private SortingType sorting = SortingType.SEARCH_FIRST;

	private String code;

	private String value;

	private String language;

	private Timestamp validOn = new Timestamp(new Date().getTime());

	private String parentCode;

	private int limit = 200;

	public DataReferenceSearchBuilder withType(String type) {
		this.type = type;
		return this;
	}

	public DataReferenceSearchBuilder withFilter(SearchFilterType filter) {
		this.filter = filter;
		return this;
	}

	public DataReferenceSearchBuilder withSorting(SortingType sorting) {
		this.sorting = sorting;
		return this;
	}

	public DataReferenceSearchBuilder withCode(String code) {
		this.code = code;
		return this;
	}

	public DataReferenceSearchBuilder withParentCode(String parentCode) {
		this.parentCode = parentCode;
		return this;
	}

	public DataReferenceSearchBuilder withValue(String value) {
		this.value = value;
		return this;
	}

	public DataReferenceSearchBuilder withLanguage(String language) {
		this.language = language;
		return this;
	}

	public DataReferenceSearchBuilder withValidOn(Timestamp validOn) {
		this.validOn = validOn;
		return this;
	}

	public DataReferenceSearchBuilder withValidOn(int year, int month, int day) {
		this.validOn = createTimestamp(year, month, day);
		return this;
	}

	public DataReferenceSearchBuilder withLimit(int limit) {
		this.limit = limit;
		return this;
	}

	public DataReferenceSearch build() {
		DataReferenceSearch dataReferenceSearch = new DataReferenceSearch();
		dataReferenceSearch.setType(type);
		dataReferenceSearch.setFilter(filter);
		dataReferenceSearch.setSorting(sorting);
		dataReferenceSearch.setCode(code);
		dataReferenceSearch.setParentCode(parentCode);
		dataReferenceSearch.setValue(value);
		dataReferenceSearch.setLanguage(language);
		dataReferenceSearch.setValidOn(validOn);
		dataReferenceSearch.setLimit(limit);
		return dataReferenceSearch;
	}

	private Timestamp createTimestamp(int year, int month, int day) {
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month - 1);
		cal.set(Calendar.DATE, day);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return new Timestamp(cal.getTimeInMillis());
	}

	public Timestamp getValidOn() {
		return validOn;
	}
}
