/*
 *  Copyright 2014-2017 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.catalogue.standalone.service.impl;

import be.wiv_isp.healthdata.catalogue.standalone.dao.IDataCollectionGroupDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionGroup;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Description;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Label;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Period;
import be.wiv_isp.healthdata.catalogue.standalone.dto.DataCollectionGroupDto;
import be.wiv_isp.healthdata.catalogue.standalone.mapper.DataCollectionGroupDtoMapper;
import be.wiv_isp.healthdata.catalogue.standalone.search.DataCollectionGroupSearch;
import be.wiv_isp.healthdata.catalogue.standalone.service.IDataCollectionGroupService;
import be.wiv_isp.healthdata.common.caching.CacheManagementService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DataCollectionGroupService extends AbstractService<DataCollectionGroup, Long, DataCollectionGroupSearch, IDataCollectionGroupDao> implements IDataCollectionGroupService {

    @Autowired
    private IDataCollectionGroupDao dao;

    public DataCollectionGroupService() {
        super(DataCollectionGroup.class);
    }

    @Override
    protected IDataCollectionGroupDao getDao() {
        return dao;
    }

    @Override
    @Cacheable(value = CacheManagementService.dataCollectionGroupCache)
    public List<DataCollectionGroup> get(String dataCollectionGroupName) {
        DataCollectionGroupSearch search = new DataCollectionGroupSearch();
        search.setName(dataCollectionGroupName);
        return getAll(search);
    }

    @Override
    @CacheEvict(value = CacheManagementService.dataCollectionGroupCache, allEntries = true)
    public DataCollectionGroup create(JSONObject json) {
        try {
            DataCollectionGroupDto dto = DataCollectionGroupDtoMapper.map(json);
            DataCollectionGroupSearch search = new DataCollectionGroupSearch();
            search.setName(dto.getName());
            search.setMajorVersion(dto.getMajorVersion());
            if (getAll(search).size() > 0) {
                HealthDataException exception = new HealthDataException();
                exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, "DataCollectionGroup with the same name and version already exists");
                throw exception;
            }
            DataCollectionGroup dataCollectionGroup = new DataCollectionGroup();
            dataCollectionGroup.setId(dto.getId());
            dataCollectionGroup.setName(dto.getName());
            dataCollectionGroup.setMajorVersion(dto.getMajorVersion());
            dataCollectionGroup.setLabel(new Label(dto.getLabel().getEn(), dto.getLabel().getFr(), dto.getLabel().getNl()));
            dataCollectionGroup.setDescription(new Description(dto.getDescription().getEn(), dto.getDescription().getFr(), dto.getDescription().getNl()));
            dataCollectionGroup.setStartDate(dto.getStartDate());
            dataCollectionGroup.setEndDateCreation(dto.getEndDateCreation());
            dataCollectionGroup.setEndDateSubmission(dto.getEndDateSubmission());
            dataCollectionGroup.setEndDateComments(dto.getEndDateComments());
            dataCollectionGroup.setParticipationContent(dto.getParticipationContent());
            if (dto.getPeriod() != null) {
                Period period = new Period(dto.getPeriod().getStart(), dto.getPeriod().getEnd());
                dataCollectionGroup.setPeriod(period);
            }
            return dao.create(dataCollectionGroup);
        }
        catch (JSONException e) {
            return null;
        }
    }

    @Override
    @CacheEvict(value = CacheManagementService.dataCollectionGroupCache, allEntries = true)
    public DataCollectionGroup update(JSONObject json) {
        try {
            DataCollectionGroupDto dto = DataCollectionGroupDtoMapper.map(json);
            DataCollectionGroupSearch search = new DataCollectionGroupSearch();
            search.setId(dto.getId());
            search.setName(dto.getName());
            DataCollectionGroup dataCollectionGroup = getUnique(search);
            dataCollectionGroup.setMajorVersion(dto.getMajorVersion());
            dataCollectionGroup.setDescription(new Description(dto.getDescription().getEn(), dto.getDescription().getFr(), dto.getDescription().getNl()));
            dataCollectionGroup.setLabel(new Label(dto.getLabel().getEn(), dto.getLabel().getFr(), dto.getLabel().getNl()));
            dataCollectionGroup.setStartDate(dto.getStartDate());
            dataCollectionGroup.setEndDateCreation(dto.getEndDateCreation());
            dataCollectionGroup.setEndDateSubmission(dto.getEndDateSubmission());
            dataCollectionGroup.setEndDateComments(dto.getEndDateComments());
            dataCollectionGroup.setParticipationContent(dto.getParticipationContent());
            if (dto.getPeriod() != null) {
                Period period = new Period(dto.getPeriod().getStart(), dto.getPeriod().getEnd());
                dataCollectionGroup.setPeriod(period);
            }
            return dao.update(dataCollectionGroup);
        }
        catch (JSONException e) {
            return null;
        }
    }

    @Override
    @CacheEvict(value = CacheManagementService.dataCollectionGroupCache, allEntries = true)
    public void delete(DataCollectionGroup dataCollectionGroup) {
        dao.delete(dataCollectionGroup);
    }

    @Override
    public DataCollectionGroup latest(DataCollectionGroupSearch search) {
        return dao.latest(search);
    }

    @Override
    public List<DataCollectionGroup> latestMajors(String dataCollectionName) {
        DataCollectionGroupSearch search = new DataCollectionGroupSearch();
        search.setName(dataCollectionName);
        return dao.getAll(search);
    }
}
