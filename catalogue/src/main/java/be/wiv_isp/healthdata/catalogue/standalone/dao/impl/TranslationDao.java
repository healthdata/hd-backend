/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import be.wiv_isp.healthdata.catalogue.standalone.dao.ITranslationDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Translation;
import be.wiv_isp.healthdata.common.dao.impl.CrudDao;

@Repository
public class TranslationDao extends CrudDao<Translation, Long> implements ITranslationDao {

	public TranslationDao() {
		super(Translation.class);
	}

	@Override
	public List<Translation> getAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Translation> cq = cb.createQuery(Translation.class);
		Root<Translation> rootEntry = cq.from(Translation.class);

		CriteriaQuery<Translation> all = cq.select(rootEntry);
		TypedQuery<Translation> allQuery = em.createQuery(all);
		return allQuery.getResultList();
	}
}
