/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.domain;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "UPGRADES")
public class Upgrade {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "UPGRADE_ID")
    private Long id;

    @Column(name = "VERSION", nullable = false)
    private String version;

    @Column(name = "FRONTEND_COMMIT", nullable = false)
    private String frontendCommit;

    @Column(name = "BACKEND_COMMIT", nullable = false)
    private String backendCommit;

    @Column(name = "MAPPING_COMMIT", nullable = false)
    private String mappingCommit;

    @Column(name = "EXECUTED_ON", nullable = false)
    private Timestamp executedOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getFrontendCommit() {
        return frontendCommit;
    }

    public void setFrontendCommit(String frontendCommit) {
        this.frontendCommit = frontendCommit;
    }

    public String getBackendCommit() {
        return backendCommit;
    }

    public void setBackendCommit(String backendCommit) {
        this.backendCommit = backendCommit;
    }

    public String getMappingCommit() {
        return mappingCommit;
    }

    public void setMappingCommit(String mappingCommit) {
        this.mappingCommit = mappingCommit;
    }

    public Timestamp getExecutedOn() {
        return executedOn;
    }

    public void setExecutedOn(Timestamp executedOn) {
        this.executedOn = executedOn;
    }
}
