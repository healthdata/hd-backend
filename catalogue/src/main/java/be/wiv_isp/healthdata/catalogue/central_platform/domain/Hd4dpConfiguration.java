/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.catalogue.central_platform.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "HD4DP_CONFIGURATIONS")
@IdClass(Hd4dpConfiguration.Hd4dpConfigurationId.class)
public class Hd4dpConfiguration {

    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name="ORGANIZATION_ID", nullable = false)
    private Organization organization;

    @Id
    @Column(name="LOOKUP_KEY", nullable = false)
    private String key;

    @Column(name="VALUE")
    private String value;

    @Column(name="UPDATED_ON", nullable = false)
    private Timestamp updatedOn;

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @PreUpdate
    @PrePersist
    public void onUpdate() {
        setUpdatedOn(new Timestamp(new Date().getTime()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hd4dpConfiguration that = (Hd4dpConfiguration) o;

        if (!organization.equals(that.organization)) return false;
        if (!key.equals(that.key)) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        return updatedOn != null ? updatedOn.equals(that.updatedOn) : that.updatedOn == null;
    }

    @Override
    public int hashCode() {
        int result = organization.hashCode();
        result = 31 * result + key.hashCode();
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (updatedOn != null ? updatedOn.hashCode() : 0);
        return result;
    }

    public static class Hd4dpConfigurationId implements Serializable {
        private Organization organization;
        private String key;

        public Hd4dpConfigurationId() {
        }

        public Hd4dpConfigurationId(Organization organization, String key) {
            this.organization = organization;
            this.key = key;
        }

        public Organization getOrganization() {
            return organization;
        }

        public void setOrganization(Organization organization) {
            this.organization = organization;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }
    }


}
