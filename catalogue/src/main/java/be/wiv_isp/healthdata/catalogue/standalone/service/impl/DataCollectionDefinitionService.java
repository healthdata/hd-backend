/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.impl;

import be.wiv_isp.healthdata.catalogue.standalone.dao.IDataCollectionDefinitionDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionDefinition;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionGroup;
import be.wiv_isp.healthdata.catalogue.standalone.domain.FollowUpDefinition;
import be.wiv_isp.healthdata.catalogue.standalone.domain.search.FollowUpDefinitionSearch;
import be.wiv_isp.healthdata.catalogue.standalone.dto.DataCollection;
import be.wiv_isp.healthdata.catalogue.standalone.search.DataCollectionDefinitionSearch;
import be.wiv_isp.healthdata.catalogue.standalone.service.IDataCollectionDefinitionService;
import be.wiv_isp.healthdata.catalogue.standalone.service.IDataCollectionGroupService;
import be.wiv_isp.healthdata.catalogue.standalone.service.IFollowUpDefinitionService;
import be.wiv_isp.healthdata.common.caching.CacheManagementService;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DataCollectionDefinitionService extends AbstractService<DataCollectionDefinition, Long, DataCollectionDefinitionSearch, IDataCollectionDefinitionDao> implements IDataCollectionDefinitionService {

	@Autowired
	private IDataCollectionDefinitionDao dao;
	@Autowired
	private IFollowUpDefinitionService followUpDefinitionService;
	@Autowired
	private IDataCollectionGroupService dataCollectionGroupService;

	public DataCollectionDefinitionService() {
		super(DataCollectionDefinition.class);
	}

	@Override
	protected IDataCollectionDefinitionDao getDao() {
		return dao;
	}

	@Override
	@CacheEvict(value = CacheManagementService.dataCollectionDefinitionLatestCache, allEntries = true)
	public DataCollectionDefinition create(DataCollectionDefinition dataCollectionDefinition) {
		updateFollowUpsBeforeSave(dataCollectionDefinition);
		return dao.create(dataCollectionDefinition);
	}

	@Override
	@CacheEvict(value = CacheManagementService.dataCollectionDefinitionLatestCache, allEntries = true)
	public DataCollectionDefinition update(DataCollectionDefinition dataCollectionDefinition) {
		updateFollowUpsBeforeSave(dataCollectionDefinition);
		return dao.update(dataCollectionDefinition);
	}

	@Cacheable(value = CacheManagementService.dataCollectionDefinitionLatestCache)
	public List<DataCollection> get(DataCollectionGroup group) {
		DataCollectionDefinitionSearch search = new DataCollectionDefinitionSearch();
		search.setDataCollectionGroupId(group.getId());
		List<DataCollectionDefinition> dataCollectionDefinitions = dao.getAll(search);

		List<DataCollection> dataCollections = new ArrayList<>();
		for (DataCollectionDefinition dataCollectionDefinition : dataCollectionDefinitions) {
			DataCollection dataCollection = new DataCollection(group, dataCollectionDefinition);
			dataCollections.add(dataCollection);
		}
		return dataCollections;
	}

	@Override
	public List<DataCollectionDefinition> getAll(DataCollectionDefinitionSearch search) {
		return filterValidForCreation(search, super.getAll(search));
	}

	@Override
	@Cacheable(value = CacheManagementService.dataCollectionDefinitionLatestCache)
	public List<DataCollectionDefinition> getForGroup(Long groupId) {
		DataCollectionDefinitionSearch search = new DataCollectionDefinitionSearch();
		search.setDataCollectionGroupId(groupId);
		return filterValidForCreation(search, getAll(search));
	}

	private List<DataCollectionDefinition> filterValidForCreation(DataCollectionDefinitionSearch search, List<DataCollectionDefinition> dataCollectionDefinitions) {
		if (search.getValidForCreation() == null) {
			return dataCollectionDefinitions;
		}
		List<DataCollectionDefinition> filtered = new ArrayList<>();
		for (DataCollectionDefinition dataCollectionDefinition : dataCollectionDefinitions) {
			DataCollectionGroup dataCollectionGroup = dataCollectionDefinition.getDataCollectionGroup();
			if (dataCollectionGroup.isValidForCreation(search.getValidForCreation())) {
				filtered.add(dataCollectionDefinition);
			}
		}
		return filtered;
	}

	@Override
	@Cacheable(value = CacheManagementService.dataCollectionDefinitionLatestCache)
	public DataCollectionDefinition latest(DataCollectionDefinitionSearch search) {
		return dao.latest(search);
	}

	@Override
	@Cacheable(value = CacheManagementService.dataCollectionDefinitionLatestCache)
	public List<DataCollectionDefinition> latestMajors(String dataCollectionName) {
		DataCollectionDefinitionSearch search = new DataCollectionDefinitionSearch();
		search.setDataCollectionName(dataCollectionName);
		return dao.getAll(search);
	}

	@Override
	@CacheEvict(value = CacheManagementService.dataCollectionDefinitionLatestCache, allEntries = true)
	public void delete(DataCollectionDefinition dataCollectionDefinition) {
		for (FollowUpDefinition followUpDefinition : dataCollectionDefinition.getFollowUpDefinitions()) {
			followUpDefinitionService.delete(followUpDefinition);
		}
		dao.delete(dataCollectionDefinition);
	}

	@Override
	public void loadDataCollectionGroup(DataCollectionDefinition dataCollectionDefinition) {
		if(dataCollectionDefinition != null) {
			final Long groupId = dataCollectionDefinition.getDataCollectionGroupId();
			if (groupId != null) {
				final DataCollectionGroup dataCollectionGroup = dataCollectionGroupService.get(groupId);
				dataCollectionDefinition.setDataCollectionGroup(dataCollectionGroup);
			}
		}
	}

	private void updateFollowUpsBeforeSave(DataCollectionDefinition dataCollectionDefinition) {
		for (FollowUpDefinition followUpDefinition : dataCollectionDefinition.getFollowUpDefinitions()) {
			followUpDefinition.setDataCollectionDefinition(dataCollectionDefinition);
			if(dataCollectionDefinition.getId() != null) {
				final FollowUpDefinitionSearch search = new FollowUpDefinitionSearch();
				search.setDataCollectionDefinitionId(dataCollectionDefinition.getId());
				search.setName(followUpDefinition.getName());
				final FollowUpDefinition existing = followUpDefinitionService.getUnique(search);
				if (existing != null) {
					followUpDefinition.setId(existing.getId());
				}
			}
		}
	}
}
