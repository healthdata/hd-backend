/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service;

import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionDefinition;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionGroup;
import be.wiv_isp.healthdata.catalogue.standalone.dto.DataCollection;
import be.wiv_isp.healthdata.catalogue.standalone.search.DataCollectionDefinitionSearch;
import be.wiv_isp.healthdata.common.service.IAbstractService;

import java.util.List;


public interface IDataCollectionDefinitionService extends IAbstractService<DataCollectionDefinition, Long, DataCollectionDefinitionSearch> {

	List<DataCollection> get(DataCollectionGroup group);

	List<DataCollectionDefinition> getForGroup(Long groupId);

	DataCollectionDefinition latest(DataCollectionDefinitionSearch search);

	List<DataCollectionDefinition> latestMajors(String dataCollectionName);

	void loadDataCollectionGroup(DataCollectionDefinition dataCollectionDefinition);
}
