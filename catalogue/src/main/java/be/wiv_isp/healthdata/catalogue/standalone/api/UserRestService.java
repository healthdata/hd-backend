/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.api;

import be.wiv_isp.healthdata.catalogue.standalone.annotation.Auditable;
import be.wiv_isp.healthdata.catalogue.standalone.domain.User;
import be.wiv_isp.healthdata.catalogue.standalone.service.IConfigurationService;
import be.wiv_isp.healthdata.catalogue.standalone.service.IUserService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("/users")
public class UserRestService {

	private static final Logger LOG = LoggerFactory.getLogger(UserRestService.class);

	@Autowired
	private IConfigurationService configurationService;

	@Autowired
	private IUserService userService;

	@Resource(name = "passwordEncoder")
	private PasswordEncoder passwordEncoder;

	@Transactional
	@PUT
	@Path("/password/{id}")
	@Auditable(apiType = ApiType.USERS)
	@PreAuthorize("hasRole('ROLE_USER')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") Long id, @QueryParam("oldPassword") String oldPassword, @QueryParam("newPassword") String newPassword) {
		LOG.info("PUT Request: Update password for id [{}]", id);

		User user = userService.get(id);
		if (user == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "USER", id);
			throw exception;
		}
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		UserDetails principal = (UserDetails) authentication.getPrincipal();
		final String username = user.getUsername();
		if (!username.equals(principal.getUsername())) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.UNAUTHORIZED_USER_UPDATE, principal.getUsername(), username);
			throw exception;
		}
		if (!passwordEncoder.matches(oldPassword, user.getPassword())) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_USER_PASSWORD, "PASSWORD");
			throw exception;
		}
		user.setPassword(newPassword);
		User updatedUser = userService.update(user, true);
		LOG.info("New password has been set for user with id [{}]", id);

		return Response.ok(updatedUser).build();
	}

}
