/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.search;

import be.wiv_isp.healthdata.common.domain.search.EntitySearch;

import java.sql.Timestamp;
import java.util.List;

public class DataCollectionDefinitionSearch extends EntitySearch {

	private Long id;
	private String dataCollectionName;
	private Long dataCollectionGroupId;
	private Integer minorVersion;
	// keep for older service versions
	private Timestamp validForCreation; //filtered on service layer
	private List<String> salesForceIds;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDataCollectionName() {
		return dataCollectionName;
	}

	public void setDataCollectionName(String dataCollectionName) {
		this.dataCollectionName = dataCollectionName;
	}

	public Long getDataCollectionGroupId() {
		return dataCollectionGroupId;
	}

	public void setDataCollectionGroupId(Long dataCollectionGroupId) {
		this.dataCollectionGroupId = dataCollectionGroupId;
	}

	public Integer getMinorVersion() {
		return minorVersion;
	}

	public void setMinorVersion(Integer minorVersion) {
		this.minorVersion = minorVersion;
	}

	public Timestamp getValidForCreation() {
		return validForCreation;
	}

	public void setValidForCreation(Timestamp validForCreation) {
		this.validForCreation = validForCreation;
	}

	public List<String> getSalesForceIds() {
		return salesForceIds;
	}

	public void setSalesForceIds(List<String> salesForceIds) {
		this.salesForceIds = salesForceIds;
	}
}
