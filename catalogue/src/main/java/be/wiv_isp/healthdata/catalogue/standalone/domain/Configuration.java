/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "CONFIGURATIONS")
public class Configuration implements Serializable {

	private static final long serialVersionUID = 7767507069644394827L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CONFIGURATION_ID")
	private Long id;

	@Column(name = "LOOKUP_KEY", unique = false, nullable = false)
	private String key;

	@Column(name = "VALUE", nullable = true)
	private String value;

	@Column(name = "READABLE")
	private boolean readable;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isReadable() {
		return readable;
	}

	public void setReadable(boolean readable) {
		this.readable = readable;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof Configuration) {
			Configuration other = (Configuration) o;

			return Objects.equals(key, other.key)
					&& Objects.equals(value, other.value)
					&& Objects.equals(readable, other.readable);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				this.key,
				this.value,
				this.readable);
	}

	@Override
	public String toString() {
		return "Configuration {" +
				"key = " + Objects.toString(this.key) + ", " +
				"value = " + Objects.toString(this.value) + ", " +
				"readable = " + Objects.toString(this.readable) + "}";
	}

	public static final String INSTALLATION_REGISTRATION_TOKEN = "INSTALLATION_REGISTRATION_TOKEN";

	public class SalesForce {

		private SalesForce() {

		}
		public static final String SALESFORCE_IS_TEST = "SALESFORCE_IS_TEST";
		public static final String LOG_SALESFORCE_CALLS = "LOG_SALESFORCE_CALLS";
		public static final String SALESFORCE_URL = "SALESFORCE_URL";
		public static final String SALESFORCE_CLIENT_ID = "SALESFORCE_CLIENT_ID";
		public static final String SALESFORCE_CLIENT_SECRET = "SALESFORCE_CLIENT_SECRET";
		public static final String SALESFORCE_USERNAME = "SALESFORCE_USERNAME";
		public static final String SALESFORCE_PASSWORD = "SALESFORCE_PASSWORD";

	}

}
