/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.mapper;

import be.wiv_isp.healthdata.catalogue.standalone.domain.*;
import be.wiv_isp.healthdata.common.domain.Baseline;
import be.wiv_isp.healthdata.common.domain.BaselineType;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.domain.enumeration.TimeUnit;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.*;

public class DataCollectionDefinitionRequestMapper {

	static public final String DATA_COLLECTION_DEFINITION = "dataCollectionDefinition";
	static public final String DATA_COLLECTION_GROUP = "dataCollectionGroup";

	static public DataCollectionDefinition extractDataCollectionDefinition(JSONObject json) throws Exception {
		DataCollectionDefinition dataCollectionDefinition = new DataCollectionDefinition();
		Map<String, Object> result = new HashMap<>();
		for (Iterator<String> iterator = json.keys(); iterator.hasNext();) {
			String key = iterator.next();
			if (!"null".equalsIgnoreCase(json.getString(key))) {
				if ("id".equalsIgnoreCase(key)) {
					try {
						dataCollectionDefinition.setId(Long.valueOf(json.getString(key)));
					} catch (NumberFormatException e) {
						HealthDataException exception = new HealthDataException(e);
						exception.setExceptionType(ExceptionType.INVALID_FORMAT, json.getString(key), "long");
						throw exception;
					}
				} else if ("dataCollectionName".equalsIgnoreCase(key)) {
					dataCollectionDefinition.setDataCollectionName(json.getString(key));
				} else if ("dataCollectionGroupId".equalsIgnoreCase(key)) {
					dataCollectionDefinition.setDataCollectionGroupId(json.getLong(key));
				} else if ("label".equalsIgnoreCase(key)) {
					final Label label = new Label();
					final JSONObject labelJSON = json.getJSONObject(key);
					if (labelJSON.names() != null) {
						for (int j = 0; j < labelJSON.names().length(); ++j) {
							final String languagueKey = labelJSON.names().getString(j);
							if ("en".equalsIgnoreCase(languagueKey)) {
								label.setEn(labelJSON.getString(languagueKey));
							} else if ("fr".equalsIgnoreCase(languagueKey)) {
								label.setFr(labelJSON.getString(languagueKey));
							} else if ("nl".equalsIgnoreCase(languagueKey)) {
								label.setNl(labelJSON.getString(languagueKey));
							}
						}
					}
					dataCollectionDefinition.setLabel(label);
				} else if ("description".equalsIgnoreCase(key)) {
					final Description description = new Description();
					final JSONObject descriptionJSON = json.getJSONObject(key);
					if (descriptionJSON.names() != null) {
						for (int j = 0; j < descriptionJSON.names().length(); ++j) {
							final String languagueKey = descriptionJSON.names().getString(j);
							if ("en".equalsIgnoreCase(languagueKey)) {
								description.setEn(descriptionJSON.getString(languagueKey));
							} else if ("fr".equalsIgnoreCase(languagueKey)) {
								description.setFr(descriptionJSON.getString(languagueKey));
							} else if ("nl".equalsIgnoreCase(languagueKey)) {
								description.setNl(descriptionJSON.getString(languagueKey));
							}
						}
					}
					dataCollectionDefinition.setDescription(description);
				} else if ("content".equalsIgnoreCase(key)) {
					final JSONObject contentJSON = json.getJSONObject(key);
					dataCollectionDefinition.setDefinitionContent(contentJSON.toString().getBytes(StandardCharsets.UTF_8));
				} else if ("minorVersion".equalsIgnoreCase(key)) {
					dataCollectionDefinition.setMinorVersion(json.getInt(key));
				} else if ("createdOn".equalsIgnoreCase(key)) {
					try {
						final Date asDate = DateUtils.parseDate(json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
						dataCollectionDefinition.setCreatedOn(new Timestamp(asDate.getTime()));
					} catch (ParseException e) {
						HealthDataException exception = new HealthDataException(e);
						exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
						throw exception;
					}
				} else if ("updatedOn".equalsIgnoreCase(key)) {
					try {
						final Date asDate = DateUtils.parseDate(json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
						dataCollectionDefinition.setUpdatedOn(new Timestamp(asDate.getTime()));
					} catch (ParseException e) {
						HealthDataException exception = new HealthDataException(e);
						exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
						throw exception;
					}
				} else if ("followUpDefinitions".equalsIgnoreCase(key)) {
					try {
						final List<FollowUpDefinition> followUpDefinitions = new ArrayList<>();
						final JSONArray jsonArray = json.getJSONArray(key);
						for(int i=0; i<jsonArray.length(); ++i) {
							followUpDefinitions.add(createFollowUpDefinition(jsonArray.getJSONObject(i)));
						}
						dataCollectionDefinition.getFollowUpDefinitions().clear();
						dataCollectionDefinition.getFollowUpDefinitions().addAll(followUpDefinitions);

					} catch (JSONException e) {
						HealthDataException exception = new HealthDataException(e);
						exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, "Could not parse follow up definitions");
						throw exception;
					}
				}
			}
		}
		return dataCollectionDefinition;
	}

	static private FollowUpDefinition createFollowUpDefinition(JSONObject fuDefinitionJsonObject) throws JSONException {
		final FollowUpDefinition def = new FollowUpDefinition();
		def.setLabel(fuDefinitionJsonObject.getString("label"));
		def.setName(fuDefinitionJsonObject.getString("name"));
		def.setDescription(fuDefinitionJsonObject.getString("description"));
		final String timingCondition = fuDefinitionJsonObject.getString("timing");
		validateTimingCondition(timingCondition);
		def.setTiming(timingCondition);

		final JSONObject baselineJsonObject = fuDefinitionJsonObject.getJSONObject("baseline");
		final String baselineType = baselineJsonObject.getString("type");
		final String baselineValue = baselineJsonObject.isNull("value") ? null : baselineJsonObject.getString("value");
		def.setBaseline(new Baseline(BaselineType.valueOf(baselineType), baselineValue));

		final JSONArray conditions = fuDefinitionJsonObject.getJSONArray("conditions");
		for(int j=0; j<conditions.length(); ++j) {
            def.addCondition(conditions.getString(j));
        }
		return def;
	}

	static private void validateTimingCondition(String value) {
		if(value == null || value.length() < 2) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_TIMING_CONDITION, value, ArrayUtils.toString(TimeUnit.values()));
			throw exception;
		}

		try {
			TimeUnit.getByLabel(value.substring(value.length() - 1));
		} catch (HealthDataException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.INVALID_TIMING_CONDITION, value, ArrayUtils.toString(TimeUnit.values()));
			throw exception;
		}

		final String amountAsString = value.substring(0, value.length() - 1);
		try {
			Integer.valueOf(amountAsString);
		} catch (NumberFormatException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.INVALID_TIMING_CONDITION, value, ArrayUtils.toString(TimeUnit.values()));
			throw exception;
		}
	}

}
