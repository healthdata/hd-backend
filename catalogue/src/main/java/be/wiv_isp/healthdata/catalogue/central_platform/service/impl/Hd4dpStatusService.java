/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.catalogue.central_platform.service.impl;


import be.wiv_isp.healthdata.catalogue.central_platform.dao.IHd4dpStatusDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.*;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.Hd4dpStatusSearch;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IHd4dpStatusService;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IInstallationService;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;

@Service
public class Hd4dpStatusService extends AbstractService<Hd4dpStatus, Hd4dpStatus.Hd4dpStatusId, Hd4dpStatusSearch, IHd4dpStatusDao> implements IHd4dpStatusService {

    private static final Logger LOG = LoggerFactory.getLogger(Hd4dpStatusService.class);

    @Autowired
    private IInstallationService installationService;

    @Autowired
    private IHd4dpStatusDao hd4dpStatusDao;


    public Hd4dpStatusService() {
        super(Hd4dpStatus.class);
    }

    @Override
    public IHd4dpStatusDao getDao() {
        return hd4dpStatusDao;
    }

    @Override
    @Transactional
    public void update(final Long installationId, final JSONObject statuses) {
        LOG.info("Updating HD4DP status for installation {}", installationId);

        Installation installation = installationService.get(installationId);
        installation.getStatuses().clear();
        Iterator iterator = statuses.keys();
        while (iterator.hasNext()) {
            try {
                String component = (String) iterator.next();
                String status = statuses.getString(component);
                Hd4dpStatus hd4dpStatus = new Hd4dpStatus();
                hd4dpStatus.setInstallation(installation);
                hd4dpStatus.setComponent(component);
                hd4dpStatus.setStatus(status);
                installation.getStatuses().add(hd4dpStatus);
                installationService.update(installation);
            }
            catch (JSONException e) {
            }
        }

        LOG.info("HD4DP status successfully updated for installation {}", installationId);
    }

}
