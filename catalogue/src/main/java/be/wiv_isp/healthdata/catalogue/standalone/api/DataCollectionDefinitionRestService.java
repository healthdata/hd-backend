/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.api;

import be.wiv_isp.healthdata.catalogue.standalone.annotation.Auditable;
import be.wiv_isp.healthdata.catalogue.standalone.service.v5.IDataCollectionDefinitionServiceV5;
import be.wiv_isp.healthdata.catalogue.standalone.service.v6.IDataCollectionDefinitionServiceV6;
import be.wiv_isp.healthdata.catalogue.standalone.service.v7.IDataCollectionDefinitionServiceV7;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Component
@Path("/datacollectiondefinitions")
public class DataCollectionDefinitionRestService {

	@Autowired
	private IDataCollectionDefinitionServiceV5 v5;
	@Autowired
	private IDataCollectionDefinitionServiceV6 v6;
	@Autowired
	private IDataCollectionDefinitionServiceV7 v7;

	@Transactional
	@POST
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(@HeaderParam("ClientVersion") String clientVersion, @Context UriInfo info, String json) throws Exception {
		final String apiVersion = ApiVersion.get(clientVersion, info, ApiType.DATA_COLLECTION_DEFINITION);
		switch (apiVersion) {
		case ApiVersion.DataCollectionDefinition.V7:
			return v7.create(new JSONObject(json));
		default:
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_CLIENT_VERSION, clientVersion);
			throw exception;
		}
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@HeaderParam("ClientVersion") String clientVersion, @Context UriInfo info) {
		final String apiVersion = ApiVersion.get(clientVersion, info, ApiType.DATA_COLLECTION_DEFINITION);
		switch (apiVersion) {
		case ApiVersion.DataCollectionDefinition.V6:
			return v6.get(info);
		case ApiVersion.DataCollectionDefinition.V7:
			return v7.get(info);
		default:
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_CLIENT_VERSION, clientVersion);
			throw exception;
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(@HeaderParam("ClientVersion") String clientVersion, @Context UriInfo info, @HeaderParam("Range") String range) {
		final String apiVersion = ApiVersion.get(clientVersion, info, ApiType.DATA_COLLECTION_DEFINITION);
		switch (apiVersion) {
		case ApiVersion.DataCollectionDefinition.V6:
			return v6.getAll(info, range);
		case ApiVersion.DataCollectionDefinition.V7:
			return v7.getAll(info, range);
		default:
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_CLIENT_VERSION, clientVersion);
			throw exception;
		}
	}

	@GET
	@Path("/latestmajors/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response latestMajors(@HeaderParam("ClientVersion") String clientVersion, @Context UriInfo info) {
		final String apiVersion = ApiVersion.get(clientVersion, info, ApiType.DATA_COLLECTION_DEFINITION);
		switch (apiVersion) {
		case ApiVersion.DataCollectionDefinition.V5:
			return v5.latestMajors(info);
		case ApiVersion.DataCollectionDefinition.V6:
			return v6.latestMajors(info);
		default:
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_CLIENT_VERSION, clientVersion);
			throw exception;
		}
	}

	@PUT
	@Path("/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION)
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public Response update(@HeaderParam("ClientVersion") String clientVersion, @Context UriInfo info, String json) throws Exception {
		final String apiVersion = ApiVersion.get(clientVersion, info, ApiType.DATA_COLLECTION_DEFINITION);
		switch (apiVersion) {
		case ApiVersion.DataCollectionDefinition.V7:
			return v7.update(info, new JSONObject(json));
		default:
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_CLIENT_VERSION, clientVersion);
			throw exception;
		}
	}

	@DELETE
	@Path("/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION)
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public Response delete(@HeaderParam("ClientVersion") String clientVersion, @Context UriInfo info, @PathParam("id") Long id) {
		final String apiVersion = ApiVersion.get(clientVersion, info, ApiType.DATA_COLLECTION_DEFINITION);
		switch (apiVersion) {
			case ApiVersion.DataCollectionDefinition.V7:
				v7.delete(id);
				return Response.noContent().build();
			default:
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.INVALID_CLIENT_VERSION, clientVersion);
				throw exception;
		}
	}
}
