/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.v6.impl;

import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionDefinition;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionGroup;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Version;
import be.wiv_isp.healthdata.catalogue.standalone.dto.DataCollection;
import be.wiv_isp.healthdata.catalogue.standalone.dto.converters.FollowUpDefinitionDtoConverter;
import be.wiv_isp.healthdata.catalogue.standalone.dto.converters.ParticipationDefinitionDtoConverter;
import be.wiv_isp.healthdata.catalogue.standalone.mapper.VersionMapper;
import be.wiv_isp.healthdata.catalogue.standalone.search.DataCollectionDefinitionSearch;
import be.wiv_isp.healthdata.catalogue.standalone.search.DataCollectionGroupSearch;
import be.wiv_isp.healthdata.catalogue.standalone.service.IDataCollectionDefinitionService;
import be.wiv_isp.healthdata.catalogue.standalone.service.IDataCollectionGroupService;
import be.wiv_isp.healthdata.catalogue.standalone.service.ISalesForceService;
import be.wiv_isp.healthdata.catalogue.standalone.service.v6.IDataCollectionDefinitionServiceV6;
import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV6;
import be.wiv_isp.healthdata.common.dto.TranslatableStringDto;
import be.wiv_isp.healthdata.common.dto.VersionDto;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.*;

@Service
public class DataCollectionDefinitionServiceV6 implements IDataCollectionDefinitionServiceV6 {

	private static final Logger LOG = LoggerFactory.getLogger(DataCollectionDefinitionServiceV6.class);

	private static String[] ALLOWED_DATE_FORMATS = new String[] { DateFormat.DATE.getPattern()};

	@Autowired
	private IDataCollectionDefinitionService dataCollectionDefinitionService;
	@Autowired
	private IDataCollectionGroupService dataCollectionGroupService;
	@Autowired
	private ISalesForceService salesForceService;

	@Override
	public Response get(UriInfo info) {
		Long id = getId(info);
		DataCollectionDefinition dataCollectionDefinition = dataCollectionDefinitionService.get(id);
		if (dataCollectionDefinition == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		DataCollectionGroup dataCollectionGroup = dataCollectionDefinition.getDataCollectionGroup();
		return Response.ok(convert(dataCollectionDefinition, dataCollectionGroup)).build();
	}

	@Override
	public Response getAll(UriInfo info, String range) {
		String identificationType = null;
		String identificationValue = null;
		String dataCollectionName = null;
		Version version = null;
		String validForCreationAsString = null;

		MultivaluedMap<String, String> queryParameters = info.getQueryParameters();
		for (String queryParameterkey : queryParameters.keySet()) {
			if ("identificationType".equalsIgnoreCase(queryParameterkey)) {
				identificationType = queryParameters.getFirst(queryParameterkey);
			} else if ("identificationValue".equalsIgnoreCase(queryParameterkey)) {
				identificationValue = queryParameters.getFirst(queryParameterkey);
			} else if ("dataCollectionName".equalsIgnoreCase(queryParameterkey)) {
				dataCollectionName = queryParameters.getFirst(queryParameterkey);
			} else if ("version".equalsIgnoreCase(queryParameterkey)) {
				version = VersionMapper.getVersion(queryParameters.getFirst(queryParameterkey));
			} else if ("validForCreation".equalsIgnoreCase(queryParameterkey)) {
				validForCreationAsString = queryParameters.getFirst(queryParameterkey);
			}
		}

		LOG.trace("GET Request: Get all data collection definitions");
		if (StringUtils.isBlank(identificationType) ^ StringUtils.isBlank(identificationValue)) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_IDENTIFICATION);
			throw exception;
		}
		Timestamp validForCreation = null;

		try {
			if (StringUtils.isNoneBlank(validForCreationAsString)) {
				final Date validForCreationAsDate = DateUtils.parseDate(validForCreationAsString, ALLOWED_DATE_FORMATS);
				validForCreation = new Timestamp(validForCreationAsDate.getTime());
			}
		} catch (ParseException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, validForCreationAsString, Arrays.toString(ALLOWED_DATE_FORMATS));
			throw exception;

		} catch (IllegalArgumentException e) {
			LOG.info("IllegalArgumentException on validForCreationAsString: nothing to do, dates are optional query parameters");
		}

		DataCollectionGroupSearch search = new DataCollectionGroupSearch();
		search.setName(dataCollectionName);
		if (version != null) {
			search.setMajorVersion(version.getMajor());
		}
		search.setValidForCreation(validForCreation);
		List<DataCollectionGroup> groups = dataCollectionGroupService.getAll(search);

		final List<DataCollection> dateCollections = new ArrayList();
		for (DataCollectionGroup group : groups) {
			final DataCollectionDefinitionSearch dcdSearch = new DataCollectionDefinitionSearch();
			dcdSearch.setDataCollectionGroupId(group.getId());
			dcdSearch.setDataCollectionName(dataCollectionName);
			if (version != null) {
				dcdSearch.setMinorVersion(version.getMinor());
			}
			List<DataCollectionDefinition> dataCollectionDefinitions = dataCollectionDefinitionService.getAll(dcdSearch);
			for (DataCollectionDefinition dataCollectionDefinition : dataCollectionDefinitions) {
				dateCollections.add(new DataCollection(group, dataCollectionDefinition));
			}
		}

		List<DataCollectionDefinitionDtoV6> result = new ArrayList<>();
		if (identificationType == null) {
			for (DataCollection dataCollection : dateCollections) {
				result.add(convert(dataCollection.getDefinition(), dataCollection.getGroup()));
			}
		} else {
			Set<String> dataCollectionNames = new HashSet<>();
			dataCollectionNames.addAll(salesForceService.getDataCollections(identificationType, identificationValue));
			dataCollectionNames.addAll(salesForceService.getDataCollections(true));
			for (DataCollection dataCollection : dateCollections) {
				if (dataCollectionNames.contains(dataCollection.getDefinition().getDataCollectionName())) {
					result.add(convert(dataCollection.getDefinition(), dataCollection.getGroup()));
				}
			}
		}
		return Response.ok(result).build();
	}

	@Override
	public Response latestMajors(UriInfo info) {
		final String name = getNameParameter(info);
		LOG.trace("GET Request: Get latest data collection definitions with name [{}] for the different major versions.", name);
		final List<DataCollectionDefinition> dataCollectionDefinitions = dataCollectionDefinitionService.latestMajors(name);
		if (dataCollectionDefinitions == null || dataCollectionDefinitions.isEmpty()) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}

		String identificationType = getIdentificationType(info);
		String identificationValue = getIdentificationValue(info);
		if (StringUtils.isNoneBlank(identificationType) && StringUtils.isNoneBlank(identificationValue)) {
			List<DataCollectionDefinition> salesForceDataCollectionDefinitions = new ArrayList<>();
			Set<String> dataCollectionNames = new HashSet<>();
			dataCollectionNames.addAll(salesForceService.getDataCollections(identificationType, identificationValue));
			dataCollectionNames.addAll(salesForceService.getDataCollections(true));
			for (DataCollectionDefinition dataCollectionDefinition : dataCollectionDefinitions) {
				if (dataCollectionNames.contains(dataCollectionDefinition.getDataCollectionName())) {
					salesForceDataCollectionDefinitions.add(dataCollectionDefinition);
				}
			}
			return Response.ok(convert(salesForceDataCollectionDefinitions)).build();
		}
		return Response.ok(convert(dataCollectionDefinitions)).build();
	}

	private Long getId(UriInfo info) {
		return RestUtils.getPathLong(info, RestUtils.ParameterName.ID);
	}

	private String getIdentificationType(UriInfo info) {
		return RestUtils.getParameterString(info, RestUtils.ParameterName.IDENTIFICATION_TYPE);
	}

	private String getIdentificationValue(UriInfo info) {
		return RestUtils.getParameterString(info, RestUtils.ParameterName.IDENTIFICATION_VALUE);
	}

	private String getNameParameter(UriInfo info) {
		return RestUtils.getPathString(info, RestUtils.ParameterName.NAME);
	}

	private List<DataCollectionDefinitionDtoV6> convert(List<DataCollectionDefinition> dataCollectionDefinitions) {
		if (dataCollectionDefinitions == null) {
			return null;
		}
		List<DataCollectionDefinitionDtoV6> converted = new ArrayList<>();
		for (DataCollectionDefinition dataCollectionDefinition : dataCollectionDefinitions) {
			DataCollectionGroup dataCollectionGroup = dataCollectionDefinition.getDataCollectionGroup();
			converted.add(convert(dataCollectionDefinition, dataCollectionGroup));
		}
		return converted;
	}

	public static DataCollectionDefinitionDtoV6 convert(DataCollectionDefinition dataCollectionDefinition, DataCollectionGroup dataCollectionGroup) {
		if (dataCollectionDefinition == null) {
			return null;
		}
		final DataCollectionDefinitionDtoV6 dataCollectionDefinitionDtoV6 = new DataCollectionDefinitionDtoV6();
		dataCollectionDefinitionDtoV6.setId(dataCollectionDefinition.getId());
		dataCollectionDefinitionDtoV6.setDataCollectionName(dataCollectionDefinition.getDataCollectionName());

		TranslatableStringDto labelDto = new TranslatableStringDto();
		labelDto.setEn(dataCollectionDefinition.getLabel().getEn());
		labelDto.setFr(dataCollectionDefinition.getLabel().getFr());
		labelDto.setNl(dataCollectionDefinition.getLabel().getNl());
		dataCollectionDefinitionDtoV6.setLabel(labelDto);

		TranslatableStringDto descriptionDto = new TranslatableStringDto();
		descriptionDto.setEn(dataCollectionDefinition.getDescription().getEn());
		descriptionDto.setFr(dataCollectionDefinition.getDescription().getFr());
		descriptionDto.setNl(dataCollectionDefinition.getDescription().getNl());
		dataCollectionDefinitionDtoV6.setDescription(descriptionDto);

		dataCollectionDefinitionDtoV6.setContent(dataCollectionDefinition.getDefinitionContent() == null ? null : dataCollectionDefinition.getDefinitionContent());
		dataCollectionDefinitionDtoV6.setVersion(convert(dataCollectionGroup.getMajorVersion(), dataCollectionDefinition.getMinorVersion()));
		dataCollectionDefinitionDtoV6.setCreatedOn(dataCollectionDefinition.getCreatedOn());
		dataCollectionDefinitionDtoV6.setStartDate(dataCollectionGroup.getStartDate());
		dataCollectionDefinitionDtoV6.setEndDateCreation(dataCollectionGroup.getEndDateCreation());
		dataCollectionDefinitionDtoV6.setEndDateSubmission(dataCollectionGroup.getEndDateSubmission());
		dataCollectionDefinitionDtoV6.setEndDateComments(dataCollectionGroup.getEndDateComments());
		dataCollectionDefinitionDtoV6.setFollowUpDefinitions(FollowUpDefinitionDtoConverter.convert(dataCollectionDefinition.getFollowUpDefinitions()));
		dataCollectionDefinitionDtoV6.setParticipationDefinition(ParticipationDefinitionDtoConverter.convert(dataCollectionGroup.getParticipationContent()));

		return dataCollectionDefinitionDtoV6;
	}

	private static VersionDto convert(Integer majorVersion, Integer minorVersion) {
		final VersionDto versionDto = new VersionDto();
		versionDto.setMajor(majorVersion);
		versionDto.setMinor(minorVersion);
		return versionDto;
	}
}
