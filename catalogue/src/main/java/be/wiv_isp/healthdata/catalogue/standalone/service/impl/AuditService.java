/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import be.wiv_isp.healthdata.catalogue.standalone.dao.IAuditDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Audit;
import be.wiv_isp.healthdata.catalogue.standalone.service.IAuditService;

@Component
public class AuditService implements IAuditService {

	@Autowired
	private IAuditDao auditDao;

	@Override
	public Audit get(Long id) {
		return auditDao.get(id);
	}

	@Override
	@Transactional
	public Audit create(Audit audit) {
		return auditDao.create(audit);
	}

	@Override
	@Transactional
	public Audit update(Audit audit) {
		return auditDao.update(audit);
	}

	@Override
	public void delete(Audit audit) {
		auditDao.delete(audit);
	}
}
