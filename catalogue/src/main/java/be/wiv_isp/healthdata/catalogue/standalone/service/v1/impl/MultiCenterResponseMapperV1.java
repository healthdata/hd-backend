/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.v1.impl;

import be.wiv_isp.healthdata.common.dto.SalesForceOrganizationDto;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class MultiCenterResponseMapperV1 {
	static public String convert(Set<SalesForceOrganizationDto> linkedOrganizations) {
		JSONArray organisations = new JSONArray();

		for (SalesForceOrganizationDto salesForceOrganization : linkedOrganizations) {
			organisations.put(getJSONOrganisation(salesForceOrganization));
		}

		return organisations.toString();
	}

	static public String convert(SalesForceOrganizationDto salesForceOrganization) {
		return getJSONOrganisation(salesForceOrganization).toString();
	}

	private static JSONObject getJSONOrganisation(SalesForceOrganizationDto salesForceOrganization) {
		Map<String, String> organizationMap = new LinkedHashMap<>();
		if (salesForceOrganization != null) {
			organizationMap.put("identificationType", salesForceOrganization.getIdentificationType());
			organizationMap.put("identificationValue", salesForceOrganization.getIdentificationValue());
			organizationMap.put("name", salesForceOrganization.getName());
		}
		return new JSONObject(organizationMap);
	}
}
