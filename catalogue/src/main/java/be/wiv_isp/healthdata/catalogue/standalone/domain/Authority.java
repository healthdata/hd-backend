/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.domain;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "AUTHORITIES")
public class Authority implements GrantedAuthority {
	private static final long serialVersionUID = 1L;

	public static final String ADMIN = "ROLE_ADMIN";
	public static final String INSTALLATION = "ROLE_INSTALLATION";

	public static final Authority ADMIN_AUTHORITY = new Authority(Authority.ADMIN);
	public static final Authority INSTALLATION_AUTHORITY = new Authority(Authority.INSTALLATION);

	public Authority() {

	}

	public Authority(String authority) {
		this.authority = authority;
	}

	@Id
	@GeneratedValue
	@Column(name = "authority_id")
	private Long authorityId;

	@Column(name = "authority")
	private String authority;

	@Override
	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof Authority) {
			Authority other = (Authority) o;

			return Objects.equals(authority, other.authority);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.authority);
	}

	@Override
	public String toString() {
		return "Authority {" + "authority = " + authority + "}";
	}
}
