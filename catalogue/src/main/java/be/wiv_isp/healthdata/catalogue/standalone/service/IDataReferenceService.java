/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service;

import java.sql.Timestamp;
import java.util.List;

import be.wiv_isp.healthdata.catalogue.standalone.dao.IDataReferenceDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataReference;
import be.wiv_isp.healthdata.catalogue.standalone.dto.FullDataReferenceDto;
import be.wiv_isp.healthdata.catalogue.standalone.search.DataReferenceSearch;

public interface IDataReferenceService {

	DataReference create(DataReference dataReference);

	List<DataReference> find(DataReferenceSearch search);

	DataReference update(DataReference dataReference);

	void delete(DataReference dataReference);

	FullDataReferenceDto getFullDataReferenceDto(DataReference dataReference, Timestamp validOn);

	DataReference getParent(DataReference dataReference, Timestamp validOn);

	List<DataReference> getChilder(DataReference dataReference, Timestamp validOn);

	void setDataReferenceDao(IDataReferenceDao dataReferenceDao);

}
