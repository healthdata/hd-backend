/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.domain.mapper;

import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.GroupByRegistrationStatistic;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.json.SerializableJsonObject;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.util.List;

public class RegistrationStatisticMapper {

    public static SerializableJsonObject map(List<GroupByRegistrationStatistic> all) {
        final SerializableJsonObject json = new SerializableJsonObject();
        try {
            for (final GroupByRegistrationStatistic item : all) {
                put(json, item);
            }
        } catch (JSONException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.JSON_PARSE_EXCEPTION, e.getMessage());
            throw exception;
        }
        return json;
    }

    private static void put(JSONObject json, GroupByRegistrationStatistic item) throws JSONException {
        final String dataCollectionName = item.getDataCollectionName();
        final Integer majorVersion = item.getMajorVersion();
        final String healthDataIdentificationValue = item.getHealthDataIdentificationValue();
        final String healthDataIdentificationName = item.getHealthDataIdentificationName();
        final String status = item.getStatus();
        final Long count = item.getCount();

        final JSONArray dataCollectionArray = getOrCreateDataCollectionArray(json, dataCollectionName);
        final JSONObject majorVersionObject = getOrCreateMajorVersionObject(dataCollectionArray, majorVersion);
        final JSONArray dataProviders = majorVersionObject.getJSONArray("dataProviders");
        final JSONObject dataProviderObject = getOrCreateDataProviderObject(dataProviders, healthDataIdentificationValue, healthDataIdentificationName);

        if(dataProviderObject.isNull("createdOn") || item.getMinUpdatedOn().before(DateFormat.DATE_AND_TIME.parse(dataProviderObject.getString("createdOn")))) {
            dataProviderObject.put("createdOn", DateFormat.DATE_AND_TIME.format(item.getMinUpdatedOn()));
        }

        final JSONObject registrations = dataProviderObject.getJSONObject("registrations");
        registrations.put(status, count);
    }

    private static JSONArray getOrCreateDataCollectionArray(JSONObject json, String dataCollectionName) throws JSONException {
        if(json.isNull(dataCollectionName)) {
            json.put(dataCollectionName, new JSONArray());
        }
        return json.getJSONArray(dataCollectionName);
    }

    private static JSONObject getOrCreateMajorVersionObject(JSONArray dataCollections, int majorVersion) throws JSONException {
        for (int i = 0; i < dataCollections.length(); i++) {
            final JSONObject jsonObject = dataCollections.getJSONObject(i);
            if(jsonObject.getJSONObject("version").getInt("major") == majorVersion) {
                return jsonObject;
            }
        }
        final JSONObject versionObject = new SerializableJsonObject();
        versionObject.put("major", majorVersion);

        final JSONObject jsonObject = new SerializableJsonObject();
        jsonObject.put("version", versionObject);
        jsonObject.put("dataProviders", new JSONArray());
        dataCollections.put(jsonObject);
        return jsonObject;
    }

    private static JSONObject getOrCreateDataProviderObject(JSONArray dataProviders, String healthDataIdentificationValue, String healthDataIdentificationName) throws JSONException {
        for (int i = 0; i < dataProviders.length(); i++) {
            final JSONObject jsonObject = dataProviders.getJSONObject(i);
            if(healthDataIdentificationValue.equals(jsonObject.getJSONObject("identification").getString("value"))) {
                return jsonObject;
            }
        }
        final JSONObject identificationObject = new SerializableJsonObject();
        identificationObject.put("value", healthDataIdentificationValue);
        identificationObject.put("name", healthDataIdentificationName);

        final JSONObject jsonObject = new SerializableJsonObject();
        jsonObject.put("identification", identificationObject);
        jsonObject.put("registrations", new SerializableJsonObject());
        dataProviders.put(jsonObject);
        return jsonObject;
    }
}
