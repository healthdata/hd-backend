/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.dto;

import be.wiv_isp.healthdata.catalogue.central_platform.domain.Installation;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Organization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Upgrade;
import be.wiv_isp.healthdata.common.json.serializer.DateSerializer;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class InstallationDto {

    public InstallationDto(Installation installation) {
        this.id = installation.getId();
        this.username = installation.getUser().getUsername();
        this.password = installation.getUser().getPassword();
        this.createdOn = installation.getCreatedOn();
        for (final Upgrade upgrade : installation.getUpgrades()) {
            this.upgrades.add(new UpgradeDto(upgrade));
        }
        for (final Organization organization : installation.getOrganizations()) {
            this.organizations.add(new OrganizationDto(organization));
        }
    }

    private Long id;
    private String username;
    private String password;
    private Timestamp createdOn;
    private List<UpgradeDto> upgrades = new ArrayList<>();
    private List<OrganizationDto> organizations = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonSerialize(using = DateSerializer.class)
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public List<UpgradeDto> getUpgrades() {
        return upgrades;
    }

    public void setUpgrades(List<UpgradeDto> upgrades) {
        this.upgrades = upgrades;
    }

    public List<OrganizationDto> getOrganizations() {
        return organizations;
    }

    public void setOrganizations(List<OrganizationDto> organizations) {
        this.organizations = organizations;
    }
}
