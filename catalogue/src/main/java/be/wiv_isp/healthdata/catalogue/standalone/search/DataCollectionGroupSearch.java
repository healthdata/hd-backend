/*
 *  Copyright 2014-2017 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package be.wiv_isp.healthdata.catalogue.standalone.search;


import be.wiv_isp.healthdata.common.domain.search.EntitySearch;

import java.sql.Timestamp;

public class DataCollectionGroupSearch extends EntitySearch{

	private Long id;
	private String name;
	private Integer majorVersion;
	private Timestamp validForCreation;
	private Timestamp validForSubmission;
	private Timestamp validForComments;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getMajorVersion() {
		return majorVersion;
	}

	public void setMajorVersion(Integer majorVersion) {
		this.majorVersion = majorVersion;
	}

	public Timestamp getValidForCreation() {
		return validForCreation;
	}

	public void setValidForCreation(Timestamp validForCreation) {
		this.validForCreation = validForCreation;
	}

	public Timestamp getValidForSubmission() {
		return validForSubmission;
	}

	public void setValidForSubmission(Timestamp validForSubmission) {
		this.validForSubmission = validForSubmission;
	}

	public Timestamp getValidForComments() {
		return validForComments;
	}

	public void setValidForComments(Timestamp validForComments) {
		this.validForComments = validForComments;
	}


}
