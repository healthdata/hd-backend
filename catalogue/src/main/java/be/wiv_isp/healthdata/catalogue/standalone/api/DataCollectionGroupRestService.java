/*
 *  Copyright 2014-2017 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package be.wiv_isp.healthdata.catalogue.standalone.api;

import be.wiv_isp.healthdata.catalogue.standalone.annotation.Auditable;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionDefinition;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionGroup;
import be.wiv_isp.healthdata.catalogue.standalone.dto.DataCollectionGroupDto;
import be.wiv_isp.healthdata.catalogue.standalone.mapper.DataCollectionGroupListDtoMapper;
import be.wiv_isp.healthdata.catalogue.standalone.search.DataCollectionGroupSearch;
import be.wiv_isp.healthdata.catalogue.standalone.service.IDataCollectionDefinitionService;
import be.wiv_isp.healthdata.catalogue.standalone.service.IDataCollectionGroupService;
import be.wiv_isp.healthdata.catalogue.standalone.service.v3.IDataCollectionServiceV3;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupListDto;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupListMap;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.*;


@Component
@Path("/datacollectiongroups")
public class DataCollectionGroupRestService {


	@Autowired
	private IDataCollectionGroupService dataCollectionGroupService;
	@Autowired
	private IDataCollectionDefinitionService dataCollectionDefinitionService;
	@Autowired
	private IDataCollectionServiceV3 v3;

	@Transactional
	@POST
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(String json) throws Exception {
		DataCollectionGroup dataCollectionGroup = dataCollectionGroupService.create(new JSONObject(json));

		return Response.status(Response.Status.CREATED).entity(new DataCollectionGroupDto(dataCollectionGroup)).build();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("id") Long id) {
		DataCollectionGroup dataCollectionGroup = dataCollectionGroupService.get(id);

		return Response.ok(new DataCollectionGroupDto(dataCollectionGroup)).build();
	}

	@GET
	@Path("/{groupName}/{majorVersion}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("groupName") String groupName, @PathParam("majorVersion") Integer majorVersion) {
		final DataCollectionGroupSearch search = new DataCollectionGroupSearch();
		search.setName(groupName);
		search.setMajorVersion(majorVersion);
		final DataCollectionGroup dataCollectionGroup = dataCollectionGroupService.getUnique(search);

		return Response.ok(new DataCollectionGroupDto(dataCollectionGroup)).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {
		final List<DataCollectionGroup> dcgs = dataCollectionGroupService.getAll();
		List<DataCollectionGroupDto> dcgDtos = new ArrayList<>();
		for (DataCollectionGroup dcg :dcgs)
			dcgDtos.add(new DataCollectionGroupDto(dcg));

		return Response.ok(dcgDtos).build();
	}

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response list(@Context UriInfo info) {
		String identificationType = getIdentificationType(info);
		String identificationValue = getIdentificationValue(info);
		Boolean withContent = getWithContent(info);
		Boolean latestVersion = getLatestVersion(info);
		if (withContent == null)
			withContent = Boolean.FALSE;
		if (latestVersion == null)
			latestVersion = Boolean.FALSE;

		Map<String, Set<String>> all;
		if(StringUtils.isNoneBlank(identificationType, identificationValue)) {
			all = v3.getAll(identificationType, identificationValue);
		} else {
			all = v3.getAll();
		}
		DataCollectionGroupListMap result = new DataCollectionGroupListMap();
		try {
			for (String dataCollectionGroupName : all.keySet()) {
				List<DataCollectionGroup> dataCollectionGroups = dataCollectionGroupService.get(dataCollectionGroupName);
				List<DataCollectionGroupListDto> convertedGroups = new ArrayList<>();
				if (Boolean.TRUE.equals(latestVersion)) {
					Integer latest = -1;
					DataCollectionGroup latestGroup = null;
					for (DataCollectionGroup dataCollectionGroup : dataCollectionGroups) {
						if (dataCollectionGroup.getMajorVersion() > latest) {
							latest = dataCollectionGroup.getMajorVersion();
							latestGroup = dataCollectionGroup;
						}
					}
					if (latestGroup != null) {
						List<DataCollectionDefinition> dataCollectionDefinitions = dataCollectionDefinitionService.getForGroup(latestGroup.getId());
						DataCollectionGroupListDto convertedGroup = DataCollectionGroupListDtoMapper.convert(latestGroup, dataCollectionDefinitions, withContent);
						convertedGroups.add(convertedGroup);
					}
				}
				else {
					for (DataCollectionGroup dataCollectionGroup : dataCollectionGroups) {
						List<DataCollectionDefinition> dataCollectionDefinitions = dataCollectionDefinitionService.getForGroup(dataCollectionGroup.getId());
						DataCollectionGroupListDto convertedGroup = DataCollectionGroupListDtoMapper.convert(dataCollectionGroup, dataCollectionDefinitions, withContent);
						convertedGroups.add(convertedGroup);
					}
				}
				if(!convertedGroups.isEmpty()) {
					result.put(dataCollectionGroupName, convertedGroups);
				}
			}
		} catch (JSONException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.JSON_EXCEPTION);
			throw exception;
		}
		return Response.ok(result).build();
	}


	private String getIdentificationType(UriInfo info) {
		return RestUtils.getParameterString(info, RestUtils.ParameterName.IDENTIFICATION_TYPE);
	}

	private String getIdentificationValue(UriInfo info) {
		return RestUtils.getParameterString(info, RestUtils.ParameterName.IDENTIFICATION_VALUE);
	}

	private Boolean getWithContent(UriInfo info) {
		return RestUtils.getParameterBoolean(info, "withContent");
	}

	private Boolean getLatestVersion(UriInfo info) {
		return RestUtils.getParameterBoolean(info, "latestVersion");
	}

	@PUT
	@Path("/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION)
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public Response update(@PathParam("id") Long id, String json) throws Exception {
		if (!id.equals(new JSONObject(json).getLong("id"))) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.UPDATE_CONFLICTING, "DataCollectionGroup", id);
			throw exception;
		}
		DataCollectionGroup dataCollectionGroup = dataCollectionGroupService.update(new JSONObject(json));

		return Response.ok(new DataCollectionGroupDto(dataCollectionGroup)).build();
	}

	@DELETE
	@Path("/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION_DEFINITION)
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public Response delete(@PathParam("id") Long id) {
		final DataCollectionGroup dataCollectionGroup = dataCollectionGroupService.get(id);
		if (dataCollectionGroup == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.DELETE_NON_EXISTING, "data collection group", id);
			throw exception;
		}
		if (!dataCollectionDefinitionService.getForGroup(dataCollectionGroup.getId()).isEmpty()) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.METHOD_NOT_ALLOWED, "delete non-empty data collection group");
			throw exception;
		}

		dataCollectionGroupService.delete(dataCollectionGroup);

		return Response.noContent().build();
	}

}
