/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.catalogue.central_platform.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "HD4DP_STATUSES")
@IdClass(Hd4dpStatus.Hd4dpStatusId.class)
public class Hd4dpStatus {
    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "INSTALLATION_ID", nullable = false)
    private Installation installation;

    @Id
    @Column(name = "COMPONENT", nullable = false)
    private String component;

    @Column(name = "STATUS", nullable = false)
    private String status;

    @Column(name = "UPDATED_ON", nullable = false)
    private Timestamp updatedOn;


    public Installation getInstallation() {
        return installation;
    }

    public void setInstallation(Installation installation) {
        this.installation = installation;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @PreUpdate
    @PrePersist
    public void onUpdate() {
        setUpdatedOn(new Timestamp(new Date().getTime()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hd4dpStatus that = (Hd4dpStatus) o;

        if (!installation.equals(that.installation)) return false;
        if (!component.equals(that.component)) return false;
        return status.equals(that.status);
    }

    @Override
    public int hashCode() {
        int result = installation.hashCode();
        result = 31 * result + component.hashCode();
        result = 31 * result + status.hashCode();
        return result;
    }

    public static class Hd4dpStatusId implements Serializable {
        private Installation installation;
        private String component;

        public Hd4dpStatusId() {
        }

        public Hd4dpStatusId(Installation installation, String component) {
            this.installation = installation;
            this.component = component;
        }

        public Installation getInstallation() {
            return installation;
        }

        public void setInstallation(Installation installation) {
            this.installation = installation;
        }

        public String getComponent() {
            return component;
        }

        public void setComponent(String component) {
            this.component = component;
        }
    }

}
