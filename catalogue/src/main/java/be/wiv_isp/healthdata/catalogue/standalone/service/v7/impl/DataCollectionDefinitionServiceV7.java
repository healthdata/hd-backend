/*
 *  Copyright 2014-2017 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.v7.impl;

import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionDefinition;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionGroup;
import be.wiv_isp.healthdata.catalogue.standalone.dto.converters.FollowUpDefinitionDtoConverter;
import be.wiv_isp.healthdata.catalogue.standalone.mapper.DataCollectionDefinitionRequestMapper;
import be.wiv_isp.healthdata.catalogue.standalone.search.DataCollectionDefinitionSearch;
import be.wiv_isp.healthdata.catalogue.standalone.service.IDataCollectionDefinitionService;
import be.wiv_isp.healthdata.catalogue.standalone.service.IDataCollectionGroupService;
import be.wiv_isp.healthdata.catalogue.standalone.service.ISalesForceService;
import be.wiv_isp.healthdata.catalogue.standalone.service.v7.IDataCollectionDefinitionServiceV7;
import be.wiv_isp.healthdata.common.api.exception.NotFoundException;
import be.wiv_isp.healthdata.common.dto.*;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class DataCollectionDefinitionServiceV7 implements IDataCollectionDefinitionServiceV7 {

	private static final Logger LOG = LoggerFactory.getLogger(DataCollectionDefinitionServiceV7.class);

	@Autowired
	private IDataCollectionDefinitionService dataCollectionDefinitionService;
	@Autowired
	private IDataCollectionGroupService dataCollectionGroupService;
	@Autowired
	private ISalesForceService salesForceService;

	@Override
	public Response create(JSONObject json) throws Exception {
		LOG.info("POST Request: Create new dataCollectionDefinition");
		DataCollectionDefinition dataCollectionDefinition = extractDataCollectionDefinition(json);

		DataCollectionDefinition createdDataCollectionDefinition;
		try {
			dataCollectionDefinition.seteFormsVersion(0);
			createdDataCollectionDefinition = dataCollectionDefinitionService.create(dataCollectionDefinition);
		} catch (Exception e) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.CREATE_GENERAL);
			throw exception;
		}
		DataCollectionGroup dataCollectionGroup = dataCollectionDefinition.getDataCollectionGroup();
		return Response.ok(convert(dataCollectionGroup, createdDataCollectionDefinition)).build();
	}

	@Override
	public Response get(UriInfo info) {
		Long id = getId(info);
		DataCollectionDefinition dataCollectionDefinition = dataCollectionDefinitionService.get(id);
		if (dataCollectionDefinition == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		DataCollectionGroup dataCollectionGroup = dataCollectionDefinition.getDataCollectionGroup();
		return Response.ok(convert(dataCollectionGroup, dataCollectionDefinition)).build();
	}

	@Override
	public Response getAll(UriInfo info, String range) {
		String identificationType = null;
		String identificationValue = null;
		String dataCollectionName = null;

		MultivaluedMap<String, String> queryParameters = info.getQueryParameters();
		for (String queryParameterkey : queryParameters.keySet()) {
			if ("identificationType".equalsIgnoreCase(queryParameterkey)) {
				identificationType = queryParameters.getFirst(queryParameterkey);
			} else if ("identificationValue".equalsIgnoreCase(queryParameterkey)) {
				identificationValue = queryParameters.getFirst(queryParameterkey);
			} else if ("dataCollectionName".equalsIgnoreCase(queryParameterkey)) {
				dataCollectionName = queryParameters.getFirst(queryParameterkey);
			}
		}

		LOG.trace("GET Request: Get all data collection definitions");
		if (StringUtils.isBlank(identificationType) ^ StringUtils.isBlank(identificationValue)) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_IDENTIFICATION);
			throw exception;
		}

		final DataCollectionDefinitionSearch dcdSearch = new DataCollectionDefinitionSearch();
		dcdSearch.setDataCollectionName(dataCollectionName);
		List<DataCollectionDefinition> definitions = dataCollectionDefinitionService.getAll(dcdSearch);

		List<DataCollectionDefinitionDtoV7> result = new ArrayList<>();
		if (identificationType == null) {
			for (DataCollectionDefinition definition : definitions) {
				definition.setDataCollectionGroupId(definition.getDataCollectionGroup().getId());
				DataCollectionGroup group = dataCollectionGroupService.get(definition.getDataCollectionGroupId());
				result.add(convert(group, definition));
			}
		} else {
			Set<String> dataCollectionNames = new HashSet<>();
			dataCollectionNames.addAll(salesForceService.getDataCollections(identificationType, identificationValue));
			dataCollectionNames.addAll(salesForceService.getDataCollections(true));
			for (DataCollectionDefinition definition : definitions) {
				if (dataCollectionNames.contains(definition.getDataCollectionName())) {
					definition.setDataCollectionGroupId(definition.getDataCollectionGroup().getId());
					DataCollectionGroup group = dataCollectionGroupService.get(definition.getDataCollectionGroupId());
					result.add(convert(group, definition));
				}
			}
		}
		return Response.ok(result).build();
	}

	@Override
	public Response update(UriInfo info, JSONObject json) throws Exception {
		Long id = getId(info);
		LOG.info("PUT Request: Update data collection definition with id [{}].", id);
		DataCollectionDefinition dataCollectionDefinition = extractDataCollectionDefinition(json);
		if (!id.equals(dataCollectionDefinition.getId())) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.UPDATE_CONFLICTING, id, dataCollectionDefinition.getId());
			throw exception;
		}
		final DataCollectionDefinition dcdInDb = dataCollectionDefinitionService.get(id);
		if (dcdInDb == null) {
			LOG.info("No data collection definition with id [{}] found in the database.", id);
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.UPDATE_NON_EXISTING, "DATA COLLECTION DEFINITION", id);
			throw exception;
		}
		dataCollectionDefinition.seteFormsVersion(1 + dcdInDb.geteFormsVersion());
		final DataCollectionDefinition updatedDataCollectionDefinition = dataCollectionDefinitionService.update(dataCollectionDefinition);
		DataCollectionGroup dataCollectionGroup = dataCollectionDefinition.getDataCollectionGroup();
		return Response.ok(convert(dataCollectionGroup, updatedDataCollectionDefinition)).build();
	}

	@Override
	public void delete(Long id) {
		LOG.info("DELETE Request: delete data collection definition with id [{}].", id);
		final DataCollectionDefinition dcdInDb = dataCollectionDefinitionService.get(id);
		if(dcdInDb == null) {
			throw new NotFoundException("DCD not found");
		}
		dataCollectionDefinitionService.delete(dcdInDb);
	}

	private DataCollectionDefinition extractDataCollectionDefinition(JSONObject json) throws Exception {
		final DataCollectionDefinition dataCollectionDefinition = DataCollectionDefinitionRequestMapper.extractDataCollectionDefinition(json);
		dataCollectionDefinitionService.loadDataCollectionGroup(dataCollectionDefinition);
		return dataCollectionDefinition;
	}

	private Long getId(UriInfo info) {
		return RestUtils.getPathLong(info, RestUtils.ParameterName.ID);
	}


	public static DataCollectionDefinitionDtoV7 convert(DataCollectionGroup group, DataCollectionDefinition definition) {
		if (definition == null) {
			return null;
		}
		DataCollectionGroupDtoV7 dataCollectionGroupDtoV7 = new DataCollectionGroupDtoV7();
		dataCollectionGroupDtoV7.setId(group.getId());
		dataCollectionGroupDtoV7.setName(group.getName());

		TranslatableStringDto labelDtoGroup = new TranslatableStringDto();
		labelDtoGroup.setEn(group.getLabel().getEn());
		labelDtoGroup.setFr(group.getLabel().getFr());
		labelDtoGroup.setNl(group.getLabel().getNl());
		dataCollectionGroupDtoV7.setLabel(labelDtoGroup);

		TranslatableStringDto descriptionDtoGroup = new TranslatableStringDto();
		descriptionDtoGroup.setEn(group.getDescription().getEn());
		descriptionDtoGroup.setFr(group.getDescription().getFr());
		descriptionDtoGroup.setNl(group.getDescription().getNl());
		dataCollectionGroupDtoV7.setDescription(descriptionDtoGroup);

		if(group.getPeriod() != null) {
			PeriodDto periodDto = new PeriodDto();
			periodDto.setStart(group.getPeriod().getStart());
			periodDto.setEnd(group.getPeriod().getEnd());
			dataCollectionGroupDtoV7.setPeriod(periodDto);
		}
		dataCollectionGroupDtoV7.setMajorVersion(group.getMajorVersion());
		dataCollectionGroupDtoV7.setCreatedOn(group.getCreatedOn());
		dataCollectionGroupDtoV7.setUpdatedOn(group.getUpdatedOn());
		dataCollectionGroupDtoV7.setStartDate(group.getStartDate());
		dataCollectionGroupDtoV7.setEndDateCreation(group.getEndDateCreation());
		dataCollectionGroupDtoV7.setEndDateSubmission(group.getEndDateSubmission());
		dataCollectionGroupDtoV7.setEndDateComments(group.getEndDateComments());

		ParticipationDefinitionDto participationDefinitionDto = new ParticipationDefinitionDto();
		participationDefinitionDto.setContent(group.getParticipationContent());
		dataCollectionGroupDtoV7.setParticipationDefinition(participationDefinitionDto);



		final DataCollectionDefinitionDtoV7 dataCollectionDefinitionDtoV7 = new DataCollectionDefinitionDtoV7();
		dataCollectionDefinitionDtoV7.setDataCollectionGroup(dataCollectionGroupDtoV7);
		dataCollectionDefinitionDtoV7.setId(definition.getId());
		dataCollectionDefinitionDtoV7.setDataCollectionName(definition.getDataCollectionName());

		TranslatableStringDto labelDto = new TranslatableStringDto();
		labelDto.setEn(definition.getLabel().getEn());
		labelDto.setFr(definition.getLabel().getFr());
		labelDto.setNl(definition.getLabel().getNl());
		dataCollectionDefinitionDtoV7.setLabel(labelDto);

		TranslatableStringDto descriptionDto = new TranslatableStringDto();
		descriptionDto.setEn(definition.getDescription().getEn());
		descriptionDto.setFr(definition.getDescription().getFr());
		descriptionDto.setNl(definition.getDescription().getNl());
		dataCollectionDefinitionDtoV7.setDescription(descriptionDto);

		dataCollectionDefinitionDtoV7.setContent(definition.getDefinitionContent());
		dataCollectionDefinitionDtoV7.setMinorVersion(definition.getMinorVersion());
		dataCollectionDefinitionDtoV7.setFollowUpDefinitions(FollowUpDefinitionDtoConverter.convert(definition.getFollowUpDefinitions()));

		return dataCollectionDefinitionDtoV7;
	}
}
