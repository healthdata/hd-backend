/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.domain;

import be.wiv_isp.healthdata.common.util.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;

@Entity
@Table(name = "DATA_COLLECTION_DEFINITIONS")
public class DataCollectionDefinition implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "DATA_COLLECTION_DEFINITIONS_ID")
	private Long id;

	@Column(name = "DATA_COLLECTION_NAME", nullable = false)
	private String dataCollectionName;

	@Embedded
	private Label label;

	@Embedded
	private Description description;

	@Embedded
	private DescriptionLong descriptionLong;

	@Lob
	@Column(name = "CONTENT", nullable = false)
	private byte[] definitionContent;

	@Column(name = "MINOR_VERSION", nullable = false)
	private Integer minorVersion;

	@Column(name = "CREATED_ON", nullable = false)
	private Timestamp createdOn;

	@Column(name = "UPDATED_ON", nullable = false)
	private Timestamp updatedOn;

	@Fetch(FetchMode.SUBSELECT)
	@OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL }, orphanRemoval = true, mappedBy = "dataCollectionDefinition")
	private List<FollowUpDefinition> followUpDefinitions;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "DATA_COLLECTION_GROUPS_ID")
	private DataCollectionGroup dataCollectionGroup;

	@Column(name = "E_FORMS_VERSION")
	private Integer eFormsVersion;

	@Transient
	private Long dataCollectionGroupId;

	public DataCollectionDefinition() {
	}

	@PrePersist
	public void onCreate() {
		long currentTimeWithoutMilliseconds = new Date().getTime();
		setCreatedOn(new Timestamp(currentTimeWithoutMilliseconds));
		setUpdatedOn(new Timestamp(currentTimeWithoutMilliseconds));
	}

	@PreUpdate
	public void onUpdate() {
		long currentTimeWithoutMilliseconds = new Date().getTime();
		setUpdatedOn(new Timestamp(currentTimeWithoutMilliseconds));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDataCollectionName() {
		return dataCollectionName;
	}

	public void setDataCollectionName(String dataCollectionName) {
		this.dataCollectionName = dataCollectionName;
	}

	public Label getLabel() {
		return label;
	}

	public void setLabel(Label label) {
		this.label = label;
	}

	public Description getDescription() {
		return description;
	}

	public void setDescription(Description description) {
		this.description = description;
	}

	public DescriptionLong getDescriptionLong() {
		return descriptionLong;
	}

	public void setDescriptionLong(DescriptionLong descriptionLong) {
		this.descriptionLong = descriptionLong;
	}

	public byte[] getDefinitionContent() {
		return definitionContent;
	}

	public void setDefinitionContent(byte[] content) {
		this.definitionContent = content;
	}

	public Integer getMinorVersion() {
		return minorVersion;
	}

	public void setMinorVersion(Integer minorVersion) {
		this.minorVersion = minorVersion;
	}

	public Integer geteFormsVersion() {
		return eFormsVersion;
	}

	public void seteFormsVersion(Integer eFormsVersion) {
		this.eFormsVersion = eFormsVersion;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public List<FollowUpDefinition> getFollowUpDefinitions() {
		if(followUpDefinitions == null) {
			followUpDefinitions = new ArrayList<>();
		}
		return followUpDefinitions;
	}

	public void setFollowUpDefinitions(List<FollowUpDefinition> followUpDefinitions) {
		this.followUpDefinitions = followUpDefinitions;
	}

	public DataCollectionGroup getDataCollectionGroup() {
		return dataCollectionGroup;
	}

	public void setDataCollectionGroup(DataCollectionGroup dataCollectionGroup) {
		this.dataCollectionGroup = dataCollectionGroup;
	}

	public Long getDataCollectionGroupId() {
		return dataCollectionGroupId;
	}

	public void setDataCollectionGroupId(Long dataCollectionGroupId) {
		this.dataCollectionGroupId = dataCollectionGroupId;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof DataCollectionDefinition) {
			DataCollectionDefinition other = (DataCollectionDefinition) o;

			return Objects.equals(id, other.id)
					&& Objects.equals(dataCollectionName, other.dataCollectionName)
					&& Objects.equals(label, other.label)
					&& Objects.equals(description, other.description)
					&& Arrays.equals(definitionContent, other.definitionContent)
					&& Objects.equals(minorVersion, other.minorVersion)
					&& Objects.equals(createdOn, other.createdOn)
					&& Objects.equals(updatedOn, other.updatedOn);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				this.id,
				this.dataCollectionName,
				this.label,
				this.description,
				Arrays.hashCode(this.definitionContent),
				this.minorVersion,
				this.createdOn,
				this.updatedOn);
	}

	@Override
	public String toString() {
		return StringUtils.toString(this);
	}
}
