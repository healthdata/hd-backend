/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.v1.impl;

import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionDefinition;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionGroup;
import be.wiv_isp.healthdata.catalogue.standalone.dto.DataCollection;
import be.wiv_isp.healthdata.catalogue.standalone.search.DataCollectionDefinitionSearch;
import be.wiv_isp.healthdata.catalogue.standalone.search.DataCollectionGroupSearch;
import be.wiv_isp.healthdata.catalogue.standalone.service.IDataCollectionDefinitionService;
import be.wiv_isp.healthdata.catalogue.standalone.service.IDataCollectionGroupService;
import be.wiv_isp.healthdata.catalogue.standalone.service.ISalesForceService;
import be.wiv_isp.healthdata.catalogue.standalone.service.v1.IHd4prcServiceV1;
import be.wiv_isp.healthdata.catalogue.standalone.service.v6.impl.DataCollectionDefinitionServiceV6;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.sql.Timestamp;
import java.util.*;

@Service
public class Hd4prcServiceV1 implements IHd4prcServiceV1 {

	private static final Logger LOG = LoggerFactory.getLogger(Hd4prcServiceV1.class);

	@Autowired
	private IDataCollectionDefinitionService dataCollectionDefinitionService;
	@Autowired
	private IDataCollectionGroupService dataCollectionGroupService;
	@Autowired
	private ISalesForceService salesForceService;

	@Override
	public Response getAll(UriInfo info) throws JSONException {
		Timestamp validForCreation = new Timestamp(new Date().getTime());
		DataCollectionGroupSearch search = new DataCollectionGroupSearch();
		search.setValidForCreation(validForCreation);
		List<DataCollectionGroup> dataCollectionGroups = dataCollectionGroupService.getAll(search);
		List<DataCollectionGroup> hd4prcFilter = filterHd4prc(dataCollectionGroups);
		Map<String, Set<String>> taxonomies = new HashMap<>();
		List<DataCollection> dataCollections = new ArrayList<>();
		for (DataCollectionGroup group : hd4prcFilter) {
			taxonomies.put(group.getName(), salesForceService.getTaxonomy(group.getName()));
			dataCollections.addAll(dataCollectionDefinitionService.get(group));
		}
		return Response.ok(Hd4prcResponseMapperV1.convert(dataCollections, taxonomies)).build();
	}

	@Override
	public Response getDataCollectionDefinition(UriInfo info) {
		MultivaluedMap<String, String> queryParameters = info.getQueryParameters();
		String dataCollectionName = null;
		Integer majorVersion = null;
		Integer minorVersion = null;
		for (String queryParameterkey : queryParameters.keySet()) {
			if ("dataCollectionName".equalsIgnoreCase(queryParameterkey)) {
				dataCollectionName = queryParameters.getFirst(queryParameterkey).toUpperCase();
			} else if("majorVersion".equalsIgnoreCase(queryParameterkey)) {
				majorVersion = Integer.valueOf(queryParameters.getFirst(queryParameterkey));
			} else if("minorVersion".equalsIgnoreCase(queryParameterkey)) {
				minorVersion = Integer.valueOf(queryParameters.getFirst(queryParameterkey));
			}
		}
		if(StringUtils.isBlank(dataCollectionName) || !isHd4prc(dataCollectionName)) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.NON_UNIQUE_SEARCH_RESULT, 0 ,"dataCollectionDefinition", dataCollectionName);
			throw exception;
		}
		DataCollectionDefinitionSearch dcdSearch = new DataCollectionDefinitionSearch();
		dcdSearch.setMinorVersion(minorVersion);
		dcdSearch.setDataCollectionName(dataCollectionName);
		List<DataCollectionDefinition> definitions = dataCollectionDefinitionService.getAll(dcdSearch);
		if(definitions.isEmpty()) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "dataCollectionDefinition", dcdSearch);
			throw exception;
		}
		DataCollection collection = getGroup(majorVersion, definitions);
		if(collection == null) {
			final DataCollectionGroupSearch search = new DataCollectionGroupSearch();
			search.setMajorVersion(majorVersion);
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "dataCollectionGroup", search);
			throw exception;
		}
		return Response.ok(DataCollectionDefinitionServiceV6.convert(collection.getDefinition(), collection.getGroup())).build();
	}

	private DataCollection getGroup(Integer majorVersion, List<DataCollectionDefinition> definitions) {
		for (DataCollectionDefinition definition : definitions) {
			DataCollectionGroup group = definition.getDataCollectionGroup();
			if(group.getMajorVersion().equals(majorVersion)) {
				return new DataCollection(group, definition);
			}
		}
		return null;
	}

	private boolean isHd4prc(String groupName) {
		Set<String> hd4prc = salesForceService.getHd4prcDataCollections();
		for (String hd4prcName : hd4prc) {
			if (groupName.equalsIgnoreCase(hd4prcName)) {
				return true;
			}
		}
		return false;
	}

	private List<DataCollectionGroup> filterHd4prc(List<DataCollectionGroup> dataCollectionGroups) {
		Set<String> dataCollectionNames = salesForceService.getHd4prcDataCollections();
		return filter(dataCollectionGroups, dataCollectionNames);
	}

	private List<DataCollectionGroup> filter(List<DataCollectionGroup> dataCollectionGroups, Set<String> dataCollectionNames) {
		List<DataCollectionGroup> result = new ArrayList<>();
		for (DataCollectionGroup dataCollectionGroup : dataCollectionGroups) {
			if (dataCollectionNames.contains(dataCollectionGroup.getName())) {
				result.add(dataCollectionGroup);
			}
		}
		return result;
	}
}
