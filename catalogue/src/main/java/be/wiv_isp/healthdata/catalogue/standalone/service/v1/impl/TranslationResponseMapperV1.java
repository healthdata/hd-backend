/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.v1.impl;

import be.wiv_isp.healthdata.catalogue.standalone.domain.Translation;
import be.wiv_isp.healthdata.catalogue.standalone.enumeration.Language;
import org.codehaus.jettison.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class TranslationResponseMapperV1 {

	public String convert(List<Translation> translations, Language language) {
		Map<String, String> singleTranslations = new TreeMap<>();
		for (Translation translation : translations) {
			if (Language.NL.equals(language)) {
				singleTranslations.put(translation.getKey(), translation.getNl());
			} else if (Language.FR.equals(language)) {
				singleTranslations.put(translation.getKey(), translation.getFr());
			}
		}
		return new JSONObject(singleTranslations).toString();
	}
}
