/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.comparator;

import org.apache.commons.lang3.math.NumberUtils;

import java.util.Comparator;

public class NumericStringComparator implements Comparator<String> {

	@Override
	public int compare(String value1, String value2) {
		if (value1 == null) {
			if (value2 == null) {
				return 0;
			} else {
				return -1;
			}
		}
		if (value2 == null) {
			return 1;
		}
		if (value1.equals(value2)) {
			return 0;
		}
		if (value1.isEmpty()) {
			return -1;
		}
		if (value2.isEmpty()) {
			return 1;
		}
		String numericStartValue1 = getNumericStart(value1);
		String numericStartValue2 = getNumericStart(value2);
		if (numericStartValue1 != null && numericStartValue1.isEmpty()) {
			if (numericStartValue2 != null && numericStartValue2.isEmpty()) {
				return value1.compareTo(value2);
			} else {
				return 1;
			}
		} else {
			if (numericStartValue2 != null && numericStartValue2.isEmpty()) {
				return -1;
			} else {
				if (numericStartValue1.equals(numericStartValue2)) {
					return value1.compareTo(value2);
				} else {
					return NumberUtils.createBigInteger(numericStartValue1).compareTo(NumberUtils.createBigInteger(numericStartValue2));
				}
			}
		}
	}

	static public String getNumericStart(String value) {
		if (value == null || value.length() == 0) {
			return "";
		}
		StringBuilder numericStart = new StringBuilder();
		boolean firstCharacter = true;
		for (char c : value.toCharArray()) {
			if (Character.isDigit(c)) {
				if (firstCharacter && '0' == c) {
					break;
				}
				firstCharacter = false;
				numericStart.append(c);
			} else {
				break;
			}
		}
		String numericStartString = numericStart.toString();
		try {
			NumberUtils.createBigInteger(numericStartString);
			return numericStartString;
		} catch (Exception e) {
			return "";
		}
	}
}
