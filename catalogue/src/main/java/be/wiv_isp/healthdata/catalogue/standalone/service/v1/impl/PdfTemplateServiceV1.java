/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.v1.impl;

import be.wiv_isp.healthdata.catalogue.standalone.domain.PdfTemplate;
import be.wiv_isp.healthdata.catalogue.standalone.service.IPdfTemplateService;
import be.wiv_isp.healthdata.catalogue.standalone.service.v1.IPdfTemplateServiceV1;
import be.wiv_isp.healthdata.common.api.exception.NotFoundException;
import be.wiv_isp.healthdata.common.dto.PdfTemplateDto;
import be.wiv_isp.healthdata.common.domain.enumeration.Language;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import com.sun.jersey.multipart.MultiPart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Service
public class PdfTemplateServiceV1 implements IPdfTemplateServiceV1 {

	@Autowired
	private IPdfTemplateService pdfTemplateService;

	@Override
	public Response get(UriInfo info, String languageHeader) {
		Long dataCollectionDefinitionId = RestUtils.getPathLong(info, RestUtils.ParameterName.DATA_COLLECTION_DEFINITION_ID);
		Language language = Language.getFromHeader(languageHeader);
		try {
			PdfTemplate pdfTemplate = pdfTemplateService.get(dataCollectionDefinitionId);
			return Response.ok(convert(pdfTemplate, language)).build();
		} catch (NoResultException e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

	@Override
	@Transactional
	public Response create(UriInfo info, MultiPart multiPart) throws Exception {
		Long dataCollectionDefinitionId = RestUtils.getPathLong(info, RestUtils.ParameterName.DATA_COLLECTION_DEFINITION_ID);
		PdfTemplateDto pdfTemplateDto = PdfTemplateRequestMapperV1.convert(multiPart);
		PdfTemplate pdfTemplate = new PdfTemplate();
		pdfTemplate.setId(dataCollectionDefinitionId);
		pdfTemplate.setTemplateEn(pdfTemplateDto.getEn());
		pdfTemplate.setTemplateFr(pdfTemplateDto.getFr());
		pdfTemplate.setTemplateNl(pdfTemplateDto.getNl());
		pdfTemplateService.create(pdfTemplate);
		return Response.noContent().build();
	}

	@Override
	@Transactional
	public Response update(UriInfo info, MultiPart multiPart) throws Exception {
		Long dataCollectionDefinitionId = RestUtils.getPathLong(info, RestUtils.ParameterName.DATA_COLLECTION_DEFINITION_ID);
		PdfTemplateDto pdfTemplateDto = PdfTemplateRequestMapperV1.convert(multiPart);
		PdfTemplate pdfTemplate = pdfTemplateService.get(dataCollectionDefinitionId);
		if(pdfTemplate == null) {
			throw new NotFoundException("Pdf template not found");
		}
		pdfTemplate.setTemplateEn(pdfTemplateDto.getEn());
		pdfTemplate.setTemplateFr(pdfTemplateDto.getFr());
		pdfTemplate.setTemplateNl(pdfTemplateDto.getNl());
		pdfTemplateService.update(pdfTemplate);
		return Response.noContent().build();
	}


	@Override
	@Transactional
	public Response delete(UriInfo info) throws Exception {
		Long dataCollectionDefinitionId = RestUtils.getPathLong(info, RestUtils.ParameterName.DATA_COLLECTION_DEFINITION_ID);
		PdfTemplate pdfTemplate = pdfTemplateService.get(dataCollectionDefinitionId);
		if(pdfTemplate == null) {
			throw new NotFoundException("Pdf template not found");
		}
		pdfTemplateService.delete(pdfTemplate);
		return Response.noContent().build();
	}

	private byte[] convert(PdfTemplate pdfTemplate, Language language) {
		if (pdfTemplate == null) {
			return null;
		}
		switch (language) {
			case EN:
				return pdfTemplate.getTemplateEn();
			case FR:
				return pdfTemplate.getTemplateFr();
			case NL:
				return pdfTemplate.getTemplateNl();
		}
		return null;
	}
}
