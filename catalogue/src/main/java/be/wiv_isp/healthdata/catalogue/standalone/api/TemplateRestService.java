/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.api;

import be.wiv_isp.healthdata.catalogue.standalone.annotation.Auditable;
import be.wiv_isp.healthdata.catalogue.standalone.service.v1.ITemplateServiceV1;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Component
@Path("/templates")
public class TemplateRestService {

	@Autowired
	private ITemplateServiceV1 v1;

	@GET
	@Path("/{key}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@HeaderParam("ClientVersion") String clientVersion, @Context UriInfo info) {
		String apiVersion = ApiVersion.get(clientVersion, info, ApiType.TEMPLATES);
		if (ApiVersion.Template.V1.equals(apiVersion)) {
			return v1.get(info);
		}
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.INVALID_CLIENT_VERSION, clientVersion);
		throw exception;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(@HeaderParam("ClientVersion") String clientVersion, @Context UriInfo info) {
		String apiVersion = ApiVersion.get(clientVersion, info, ApiType.TEMPLATES);
		if (ApiVersion.Template.V1.equals(apiVersion)) {
			return v1.getAll();
		}
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.INVALID_CLIENT_VERSION, clientVersion);
		throw exception;
	}

	@POST
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.TEMPLATES)
	@Path("/{key}")
	@Transactional
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(@HeaderParam("ClientVersion") String clientVersion, @Context UriInfo info, String json) throws Exception {
		String apiVersion = ApiVersion.get(clientVersion, info, ApiType.TEMPLATES);
		if (ApiVersion.Template.V1.equals(apiVersion)) {
			return v1.create(info, new JSONObject(json));
		}
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.INVALID_CLIENT_VERSION, clientVersion);
		throw exception;
	}

	@PUT
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.TEMPLATES)
	@Path("/{key}")
	@Transactional
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@HeaderParam("ClientVersion") String clientVersion, @Context UriInfo info, String json) throws Exception {
		String apiVersion = ApiVersion.get(clientVersion, info, ApiType.TEMPLATES);
		if (ApiVersion.Template.V1.equals(apiVersion)) {
			return v1.update(info, new JSONObject(json));
		}
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.INVALID_CLIENT_VERSION, clientVersion);
		throw exception;
	}
}
