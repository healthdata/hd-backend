/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.domain;

import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionDefinition;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "REGISTRATION_STATISTICS")
@IdClass(RegistrationStatistic.RegistrationStatisticId.class)
public class RegistrationStatistic {

    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "ORGANIZATION_ID")
    private Organization organization;

    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "DATA_COLLECTION_DEFINITION_ID")
    private DataCollectionDefinition dataCollectionDefinition;

    @Id
    @Column(name = "STATUS", nullable = false)
    private String status;

    @Column(name = "COUNT", nullable = false)
    private Long count;

    @Column(name = "UPDATED_ON")
    private Timestamp updatedOn;

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public DataCollectionDefinition getDataCollectionDefinition() {
        return dataCollectionDefinition;
    }

    public void setDataCollectionDefinition(DataCollectionDefinition dataCollectionDefinition) {
        this.dataCollectionDefinition = dataCollectionDefinition;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @PreUpdate
    @PrePersist
    public void onUpdate() {
        updatedOn = new Timestamp(new Date().getTime());
    }

    public static class RegistrationStatisticId implements Serializable {

        private Organization organization;
        private DataCollectionDefinition dataCollectionDefinition;
        private String status;

        public RegistrationStatisticId() {

        }

        public RegistrationStatisticId(Organization organization, DataCollectionDefinition dataCollectionDefinition, String status) {
            this.organization = organization;
            this.dataCollectionDefinition = dataCollectionDefinition;
            this.status = status;
        }

        public Organization getOrganization() {
            return organization;
        }

        public void setOrganization(Organization organization) {
            this.organization = organization;
        }

        public DataCollectionDefinition getDataCollectionDefinition() {
            return dataCollectionDefinition;
        }

        public void setDataCollectionDefinition(DataCollectionDefinition dataCollectionDefinition) {
            this.dataCollectionDefinition = dataCollectionDefinition;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }

            if (o instanceof RegistrationStatisticId) {
                RegistrationStatisticId other = (RegistrationStatisticId) o;

                return Objects.equals(organization, other.organization)
                        && Objects.equals(dataCollectionDefinition, other.dataCollectionDefinition)
                        && Objects.equals(status, other.status);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return Objects.hash(
                    this.organization,
                    this.dataCollectionDefinition,
                    this.status);
        }
    }
}
