/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.catalogue.standalone.domain;


import be.wiv_isp.healthdata.common.domain.DateRange;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "DATA_COLLECTION_GROUPS")
public class DataCollectionGroup implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="DATA_COLLECTION_GROUPS_ID")
    private Long id;
    @Column(name = "NAME", nullable = false)
    private String name;
    @Column(name = "MAJOR_VERSION", nullable = false)
    private Integer majorVersion;
    @Embedded
    private Label label;
    @Embedded
    private Description description;
    @Embedded
    private Period period;
    @Column(name = "CREATED_ON", nullable = false)
    private Timestamp createdOn;
    @Column(name = "UPDATED_ON", nullable = false)
    private Timestamp updatedOn;
    @Column(name = "START_DATE")
    private Timestamp startDate;
    @Column(name = "END_DATE_CREATION")
    private Timestamp endDateCreation;
    @Column(name = "END_DATE_SUBMISSION")
    private Timestamp endDateSubmission;
    @Column(name = "END_DATE_COMMENTS")
    private Timestamp endDateComments;
    @Lob
    @Column(name = "PARTICIPATION_CONTENT")
    private byte[] participationContent;

    public DataCollectionGroup() {
    }

    @PrePersist
    public void onCreate() {
        long currentTimeWithoutMilliseconds = new Date().getTime();
        setCreatedOn(new Timestamp(currentTimeWithoutMilliseconds));
        setUpdatedOn(new Timestamp(currentTimeWithoutMilliseconds));
    }

    @PreUpdate
    public void onUpdate() {
        long currentTimeWithoutMilliseconds = new Date().getTime();
        setUpdatedOn(new Timestamp(currentTimeWithoutMilliseconds));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMajorVersion() {
        return majorVersion;
    }

    public void setMajorVersion(Integer majorVersion) {
        this.majorVersion = majorVersion;
    }

    public Label getLabel() {
        return label;
    }

    public void setLabel(Label label) {
        this.label = label;
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDateCreation() {
        return endDateCreation;
    }

    public void setEndDateCreation(Timestamp endDateCreation) {
        this.endDateCreation = endDateCreation;
    }

    public Timestamp getEndDateSubmission() {
        return endDateSubmission;
    }

    public void setEndDateSubmission(Timestamp endDateSubmission) {
        this.endDateSubmission = endDateSubmission;
    }

    public Timestamp getEndDateComments() {
        return endDateComments;
    }

    public void setEndDateComments(Timestamp endDateComments) {
        this.endDateComments = endDateComments;
    }

    public byte[] getParticipationContent() {
        return participationContent;
    }

    public void setParticipationContent(byte[] participationContent) {
        this.participationContent = participationContent;
    }

    public boolean isValidForCreation(Date date) {
        return DateRange.isBetween(startDate, date, endDateCreation);
    }
}
