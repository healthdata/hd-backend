/*
 *  Copyright 2014-2017 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package be.wiv_isp.healthdata.catalogue.standalone.service;

import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionGroup;
import be.wiv_isp.healthdata.catalogue.standalone.search.DataCollectionGroupSearch;
import be.wiv_isp.healthdata.common.service.IAbstractService;
import org.codehaus.jettison.json.JSONObject;

import java.util.List;

public interface IDataCollectionGroupService extends IAbstractService<DataCollectionGroup, Long, DataCollectionGroupSearch> {

	List<DataCollectionGroup> get(String dataCollectionGroupName);

	DataCollectionGroup create(JSONObject json);

	DataCollectionGroup update(JSONObject json);

	void delete(DataCollectionGroup dataCollectionGroup);

	DataCollectionGroup latest(DataCollectionGroupSearch search);

	List<DataCollectionGroup> latestMajors(String dataCollectionName);
}
