/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.v5.impl;

import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionDefinition;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionGroup;
import be.wiv_isp.healthdata.catalogue.standalone.dto.converters.FollowUpDefinitionDtoConverter;
import be.wiv_isp.healthdata.catalogue.standalone.dto.converters.ParticipationDefinitionDtoConverter;
import be.wiv_isp.healthdata.catalogue.standalone.service.IDataCollectionDefinitionService;
import be.wiv_isp.healthdata.catalogue.standalone.service.ISalesForceService;
import be.wiv_isp.healthdata.catalogue.standalone.service.v5.IDataCollectionDefinitionServiceV5;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV5;
import be.wiv_isp.healthdata.common.dto.VersionDto;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class DataCollectionDefinitionServiceV5 implements IDataCollectionDefinitionServiceV5 {

	private static final Logger LOG = LoggerFactory.getLogger(DataCollectionDefinitionServiceV5.class);

	private static String[] ALLOWED_DATE_FORMATS = new String[] { DateFormat.DATE.getPattern()};

	@Autowired
	private IDataCollectionDefinitionService dataCollectionDefinitionService;
	@Autowired
	private ISalesForceService salesForceService;

	@Override
	public Response latestMajors(UriInfo info) {
		final String name = getNameParameter(info);
		LOG.trace("GET Request: Get latest data collection definitions with name [{}] for the different major versions.", name);
		final List<DataCollectionDefinition> dataCollectionDefinitions = dataCollectionDefinitionService.latestMajors(name);
		if (dataCollectionDefinitions == null || dataCollectionDefinitions.isEmpty()) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}

		String identificationType = getIdentificationType(info);
		String identificationValue = getIdentificationValue(info);
		if (StringUtils.isNoneBlank(identificationType) && StringUtils.isNoneBlank(identificationValue)) {
			List<DataCollectionDefinition> salesForceDataCollectionDefinitions = new ArrayList<>();
			Set<String> dataCollectionNames = new HashSet<>();
			dataCollectionNames.addAll(salesForceService.getDataCollections(identificationType, identificationValue));
			dataCollectionNames.addAll(salesForceService.getDataCollections(true));
			for (DataCollectionDefinition dataCollectionDefinition : dataCollectionDefinitions) {
				if (dataCollectionNames.contains(dataCollectionDefinition.getDataCollectionName())) {
					salesForceDataCollectionDefinitions.add(dataCollectionDefinition);
				}
			}
			return Response.ok(convert(salesForceDataCollectionDefinitions)).build();
		}
		return Response.ok(convert(dataCollectionDefinitions)).build();
	}

	private String getIdentificationType(UriInfo info) {
		return RestUtils.getParameterString(info, RestUtils.ParameterName.IDENTIFICATION_TYPE);
	}

	private String getIdentificationValue(UriInfo info) {
		return RestUtils.getParameterString(info, RestUtils.ParameterName.IDENTIFICATION_VALUE);
	}

	private String getNameParameter(UriInfo info) {
		return RestUtils.getPathString(info, RestUtils.ParameterName.NAME);
	}

	private List<DataCollectionDefinitionDtoV5> convert(List<DataCollectionDefinition> dataCollectionDefinitions) {
		if (dataCollectionDefinitions == null) {
			return null;
		}
		List<DataCollectionDefinitionDtoV5> converted = new ArrayList<>();
		for (DataCollectionDefinition dataCollectionDefinition : dataCollectionDefinitions) {
			DataCollectionGroup dataCollectionGroup = dataCollectionDefinition.getDataCollectionGroup();
			converted.add(convert(dataCollectionDefinition, dataCollectionGroup));
		}
		return converted;
	}

	public static DataCollectionDefinitionDtoV5 convert(DataCollectionDefinition dataCollectionDefinition, DataCollectionGroup dataCollectionGroup) {
		if (dataCollectionDefinition == null) {
			return null;
		}
		final DataCollectionDefinitionDtoV5 dataCollectionDefinitionDtoV5 = new DataCollectionDefinitionDtoV5();
		dataCollectionDefinitionDtoV5.setId(dataCollectionDefinition.getId());
		dataCollectionDefinitionDtoV5.setDataCollectionName(dataCollectionDefinition.getDataCollectionName());
		dataCollectionDefinitionDtoV5.setDescription(dataCollectionDefinition.getDescription().getEn());
		dataCollectionDefinitionDtoV5.setContent(dataCollectionDefinition.getDefinitionContent() == null ? null : dataCollectionDefinition.getDefinitionContent());
		dataCollectionDefinitionDtoV5.setVersion(convert(dataCollectionGroup.getMajorVersion(), dataCollectionDefinition.getMinorVersion()));
		dataCollectionDefinitionDtoV5.setCreatedOn(dataCollectionDefinition.getCreatedOn());
		dataCollectionDefinitionDtoV5.setStartDate(dataCollectionGroup.getStartDate());
		dataCollectionDefinitionDtoV5.setEndDateCreation(dataCollectionGroup.getEndDateCreation());
		dataCollectionDefinitionDtoV5.setEndDateSubmission(dataCollectionGroup.getEndDateSubmission());
		dataCollectionDefinitionDtoV5.setEndDateComments(dataCollectionGroup.getEndDateComments());
		dataCollectionDefinitionDtoV5.setFollowUpDefinitions(FollowUpDefinitionDtoConverter.convert(dataCollectionDefinition.getFollowUpDefinitions()));
		dataCollectionDefinitionDtoV5.setParticipationDefinition(ParticipationDefinitionDtoConverter.convert(dataCollectionGroup.getParticipationContent()));
		return dataCollectionDefinitionDtoV5;
	}

	private static VersionDto convert(Integer majorVersion, Integer minorVersion) {
		final VersionDto versionDto = new VersionDto();
		versionDto.setMajor(majorVersion);
		versionDto.setMinor(minorVersion);
		return versionDto;
	}
}
