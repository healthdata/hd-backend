/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.dao.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IRegistrationStatisticDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Installation;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Organization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.RegistrationStatistic;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.GroupByRegistrationStatistic;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.RegistrationStatisticSearch;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionDefinition;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionGroup;
import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Repository
public class RegistrationStatisticDao extends CrudDaoV2<RegistrationStatistic, RegistrationStatistic.RegistrationStatisticId, RegistrationStatisticSearch> implements IRegistrationStatisticDao {

    public RegistrationStatisticDao() {
        super(RegistrationStatistic.class);
    }

    @Override
    protected List<Predicate> getPredicates(RegistrationStatisticSearch search, CriteriaBuilder cb, Root<RegistrationStatistic> rootEntry) {
        List<Predicate> predicates = new ArrayList<>();
        if (search.getOrganizationId() != null) {
            predicates.add(cb.equal(rootEntry.<Installation>get("organization").get("id"), search.getOrganizationId()));
        }
        if (search.getDataCollectionDefinitionId() != null) {
            predicates.add(cb.equal(rootEntry.<DataCollectionDefinition>get("dataCollectionDefinition").get("id"), search.getDataCollectionDefinitionId()));
        }
        if (search.getStatus() != null) {
            predicates.add(cb.equal(rootEntry.get("status"), search.getStatus()));
        }
        return predicates;
    }

    @Override
    public List<GroupByRegistrationStatistic> getAllGroupBy() {
        final CriteriaBuilder cb = em.getCriteriaBuilder();
        final CriteriaQuery<GroupByRegistrationStatistic> cq = cb.createQuery(GroupByRegistrationStatistic.class);
        final Root<RegistrationStatistic> rootEntry = cq.from(RegistrationStatistic.class);

        cq.multiselect(
                rootEntry.<DataCollectionDefinition>get("dataCollectionDefinition").get("dataCollectionName"),
                rootEntry.<DataCollectionDefinition>get("dataCollectionDefinition").<DataCollectionGroup>get("dataCollectionGroup").<Integer>get("majorVersion"),
                rootEntry.<Organization>get("organization").get("identificationValue"),
                rootEntry.<Organization>get("organization").get("name"),
                rootEntry.get("status"),
                cb.sum(rootEntry.<Long>get("count")),
                cb.least(rootEntry.<Timestamp>get("updatedOn"))
        );

        cq.groupBy(
                rootEntry.<DataCollectionDefinition>get("dataCollectionDefinition").get("dataCollectionName"),
                rootEntry.<DataCollectionDefinition>get("dataCollectionDefinition").<DataCollectionGroup>get("dataCollectionGroup").<Integer>get("majorVersion"),
                rootEntry.<Organization>get("organization").get("identificationValue"),
                rootEntry.get("status")
        );

        // TODO-WDC-2948: filter out deleted organization?

        final TypedQuery<GroupByRegistrationStatistic> allQuery = em.createQuery(cq);
        return allQuery.getResultList();
    }
}
