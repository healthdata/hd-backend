/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.v1.impl;

import be.wiv_isp.healthdata.common.dto.PdfTemplateDto;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.MultiPart;
import org.apache.commons.io.IOUtils;

public class PdfTemplateRequestMapperV1 {

	static public PdfTemplateDto convert(MultiPart multiPart) throws Exception {
		PdfTemplateDto pdfTemplate = new PdfTemplateDto();

		BodyPartEntity enBpe = (BodyPartEntity) multiPart.getBodyParts().get(0).getEntity();
		pdfTemplate.setEn(IOUtils.toByteArray(enBpe.getInputStream()));
		BodyPartEntity frBpe = (BodyPartEntity) multiPart.getBodyParts().get(1).getEntity();
		pdfTemplate.setFr(IOUtils.toByteArray(frBpe.getInputStream()));
		BodyPartEntity nlBpe = (BodyPartEntity) multiPart.getBodyParts().get(2).getEntity();
		pdfTemplate.setNl(IOUtils.toByteArray(nlBpe.getInputStream()));

		return pdfTemplate;
	}
}
