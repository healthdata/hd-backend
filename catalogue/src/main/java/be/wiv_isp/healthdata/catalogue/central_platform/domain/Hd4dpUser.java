/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.central_platform.domain;


import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "HD4DP_USERS")
@IdClass(Hd4dpUser.Hd4dpUserId.class)
public class Hd4dpUser {

    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "ORGANIZATION_ID")
    private Organization organization;

    @Id
    @Column(name = "USERNAME")
    private String username;

    @Column(name = "FIRST_NAME")
    protected String firstName;

    @Column(name = "LAST_NAME")
    protected String lastName;

    @Column(name = "EMAIL")
    protected String email;

    @Column(name = "ENABLED")
    private boolean enabled;

    @Column(name = "LDAP_USER")
    private boolean ldapUser;

    @Fetch(FetchMode.JOIN)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "HD4DP_USER_AUTHORITIES", joinColumns = { @JoinColumn(name = "ORGANIZATION_ID"), @JoinColumn(name = "USERNAME") })
    @Column(name = "AUTHORITY")
    private Set<String> authorities;

    @Fetch(FetchMode.JOIN)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "HD4DP_USER_DATA_COLLECTIONS", joinColumns = { @JoinColumn(name = "ORGANIZATION_ID"), @JoinColumn(name = "USERNAME") })
    @Column(name = "DATA_COLLECTION_NAME")
    private Set<String> dataCollectionNames;

    @Column(name = "UPDATED_ON", nullable = false)
    protected Timestamp updatedOn;

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isLdapUser() {
        return ldapUser;
    }

    public void setLdapUser(boolean ldapUser) {
        this.ldapUser = ldapUser;
    }

    public Set<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<String> authorities) {
        this.authorities = authorities;
    }

    public Set<String> getDataCollectionNames() {
        return dataCollectionNames;
    }

    public void setDataCollectionNames(Set<String> dataCollectionNames) {
        this.dataCollectionNames = dataCollectionNames;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @PreUpdate
    @PrePersist
    public void onUpdate() {
        setUpdatedOn(new Timestamp(new Date().getTime()));
    }

    public static class Hd4dpUserId implements Serializable {

        private Organization organization;
        private String username;

        public Hd4dpUserId() {

        }

        public Hd4dpUserId(Organization organization, String username) {
            this.organization = organization;
            this.username = username;
        }

        public Organization getOrganization() {
            return organization;
        }

        public void setOrganization(Organization organization) {
            this.organization = organization;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Hd4dpUser))
            return false;

        Hd4dpUser hd4dpUser = (Hd4dpUser) o;

        if (enabled != hd4dpUser.enabled)
            return false;
        if (ldapUser != hd4dpUser.ldapUser)
            return false;
        if (organization != null ? !organization.equals(hd4dpUser.organization) : hd4dpUser.organization != null)
            return false;
        if (username != null ? !username.equals(hd4dpUser.username) : hd4dpUser.username != null)
            return false;
        if (firstName != null ? !firstName.equals(hd4dpUser.firstName) : hd4dpUser.firstName != null)
            return false;
        if (lastName != null ? !lastName.equals(hd4dpUser.lastName) : hd4dpUser.lastName != null)
            return false;
        if (email != null ? !email.equals(hd4dpUser.email) : hd4dpUser.email != null)
            return false;
        if (authorities != null ? !authorities.equals(hd4dpUser.authorities) : hd4dpUser.authorities != null)
            return false;
        return !(dataCollectionNames != null ? !dataCollectionNames.equals(hd4dpUser.dataCollectionNames) : hd4dpUser.dataCollectionNames != null);

    }

    @Override
    public int hashCode() {
        int result = organization != null ? organization.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (enabled ? 1 : 0);
        result = 31 * result + (ldapUser ? 1 : 0);
        result = 31 * result + (authorities != null ? authorities.hashCode() : 0);
        result = 31 * result + (dataCollectionNames != null ? dataCollectionNames.hashCode() : 0);
        return result;
    }
}
