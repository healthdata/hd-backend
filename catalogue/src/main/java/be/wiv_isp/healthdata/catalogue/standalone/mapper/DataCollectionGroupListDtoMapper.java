/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.catalogue.standalone.mapper;


import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionDefinition;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionGroup;
import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionListDto;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupListDto;
import be.wiv_isp.healthdata.common.dto.PeriodDto;
import be.wiv_isp.healthdata.common.dto.TranslatableStringDto;
import org.codehaus.jettison.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class DataCollectionGroupListDtoMapper {

    static public DataCollectionGroupListDto convert(DataCollectionGroup dataCollectionGroup, List<DataCollectionDefinition> dataCollectionDefinitions, boolean withContent) throws JSONException{
        DataCollectionGroupListDto dto = new DataCollectionGroupListDto();

        dto.setDataCollectionGroupName(dataCollectionGroup.getName());
        dto.setDataCollectionGroupId(dataCollectionGroup.getId());
        dto.setMajorVersion(dataCollectionGroup.getMajorVersion());

        TranslatableStringDto label = new TranslatableStringDto();
        label.setEn(dataCollectionGroup.getLabel().getEn());
        label.setFr(dataCollectionGroup.getLabel().getFr());
        label.setNl(dataCollectionGroup.getLabel().getNl());
        dto.setLabel(label);

        TranslatableStringDto description = new TranslatableStringDto();
        description.setEn(dataCollectionGroup.getDescription().getEn());
        description.setFr(dataCollectionGroup.getDescription().getFr());
        description.setNl(dataCollectionGroup.getDescription().getNl());
        dto.setDescription(description);

        PeriodDto period = new PeriodDto();
        period.setStart(dataCollectionGroup.getPeriod() != null ? dataCollectionGroup.getPeriod().getStart(): null);
        period.setEnd(dataCollectionGroup.getPeriod() != null ? dataCollectionGroup.getPeriod().getEnd(): null);
        dto.setPeriod(period);

        dto.setStartDate(dataCollectionGroup.getStartDate());
        dto.setEndDateCreation(dataCollectionGroup.getEndDateCreation());
        dto.setEndDateSubmission(dataCollectionGroup.getEndDateSubmission());
        dto.setEndDateComments(dataCollectionGroup.getEndDateComments());

        List<DataCollectionDefinitionListDto> convertedDefinitions = new ArrayList<>();
        for (DataCollectionDefinition dataCollectionDefinition : dataCollectionDefinitions) {
            DataCollectionDefinitionListDto convertedDefinition = new DataCollectionDefinitionListDto();
            convertedDefinition.setDataCollectionName(dataCollectionDefinition.getDataCollectionName());
            convertedDefinition.setDataCollectionDefinitionId(dataCollectionDefinition.getId());
            convertedDefinition.setMinorVersion(dataCollectionDefinition.getMinorVersion());

            TranslatableStringDto labelDefinition = new TranslatableStringDto();
            labelDefinition.setEn(dataCollectionDefinition.getLabel().getEn());
            labelDefinition.setFr(dataCollectionDefinition.getLabel().getFr());
            labelDefinition.setNl(dataCollectionDefinition.getLabel().getNl());
            convertedDefinition.setLabel(labelDefinition);

            TranslatableStringDto descriptionDefinition = new TranslatableStringDto();
            descriptionDefinition.setEn(dataCollectionDefinition.getDescription().getEn());
            descriptionDefinition.setFr(dataCollectionDefinition.getDescription().getFr());
            descriptionDefinition.setNl(dataCollectionDefinition.getDescription().getNl());
            convertedDefinition.setDescription(descriptionDefinition);

            if (withContent)
                convertedDefinition.setContent(dataCollectionDefinition.getDefinitionContent());
            else
                convertedDefinition.setContent(null);

            convertedDefinitions.add(convertedDefinition);
        }
        dto.setDataCollectionDefinitions(convertedDefinitions);
        return dto;
    }
}
