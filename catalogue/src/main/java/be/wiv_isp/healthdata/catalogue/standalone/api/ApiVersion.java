/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.api;

import be.wiv_isp.healthdata.catalogue.standalone.mapper.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

public class ApiVersion {

    private static final Logger LOG = LoggerFactory.getLogger(ApiVersion.class);

	public class DataCollectionDefinition {

		public static final String V5 = "V5";
		// Q3 - 2016
		// multi language description
		// pdf template
		public static final String V6 = "V6";
		// multi registry
		public static final String V7 = "V7";
	}

	public class DataCollectionDefinitionManager {
		// Physical release Q1 - 2016
		// Modified version and history added
		public static final String V2 = "V2";
	}

	public class DataCollection {

		// identificationType is now RIZIV and not NIHII
		public static final String V2 = "V2";
		// multi-registry
		public static final String V3 = "V3";

	}

	public class Translation {
		public static final String V1 = "V1";
	}

	public class MultiCenter {
		public static final String V1 = "V1";
	}

	public class Template {
		//Physical release Q2 - 2016
		public static final String V1 = "V1";
	}


	public class PdfTemplate {
		//Physical release Q3 - 2016
		public static final String V1 = "V1";
	}

    public static String get(String clientVersion, UriInfo info, ApiType apiType) {
        if (StringUtils.isBlank(clientVersion)) {
            LOG.warn("The [ClientVersion] header is not set.");
            MultivaluedMap<String, String> queryParameters = info.getQueryParameters();
            for (String queryParameterKey : queryParameters.keySet()) {
                if ("clientVersion".equalsIgnoreCase(queryParameterKey)) {
                    clientVersion = queryParameters.getFirst(queryParameterKey);
                }
            }
        }
        if (StringUtils.isNotBlank(clientVersion)) {
            switch (apiType) {
				case DATA_COLLECTION_DEFINITION:
					return DataCollectionDefinitionVersionMapper.getApiVersion(clientVersion);
				case MULTI_CENTER:
					return MultiCenterVersionMapper.getApiVersion(clientVersion);
				case DATA_COLLECTIONS:
					return DataCollectionVersionMapper.getApiVersion(clientVersion);
				case TRANSLATIONS:
					return TranslationVersionMapper.getApiVersion(clientVersion);
				case TEMPLATES:
					return TemplateVersionMapper.getApiVersion(clientVersion);
				case PDF_TEMPLATES:
					return PdfTemplateVersionMapper.getApiVersion(clientVersion);
			}

        }
        LOG.warn("The [clientVersion] query parameter is not set.");
		switch (apiType) {
			case DATA_COLLECTION_DEFINITION:
				return DataCollectionDefinitionVersionMapper.getLatestApiVersion();
			case MULTI_CENTER:
				return MultiCenterVersionMapper.getLatestApiVersion();
			case DATA_COLLECTIONS:
				return DataCollectionVersionMapper.getLatestApiVersion();
			case TRANSLATIONS:
				return TranslationVersionMapper.getLatestApiVersion();
			case TEMPLATES:
				return TemplateVersionMapper.getLatestApiVersion();
			case PDF_TEMPLATES:
				return PdfTemplateVersionMapper.getLatestApiVersion();
		}
		LOG.error("The default api version isn't set.");
		return "";
    }
}
