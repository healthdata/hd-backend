/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.impl;

import be.wiv_isp.healthdata.catalogue.standalone.service.IEncryptionService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import com.sun.jersey.core.util.Base64;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.text.MessageFormat;

@Component
public class EncryptionService implements IEncryptionService {

	private static final String ALGORITHM = "AES";
	private static final String KEY_STORE_TYPE = "JCEKS";
	private static final String KEY_STORE_FILE = "keystore.jceks";
	private static final String KEY_STORE_PASSWORD = "TlRxk8tN6g";
	private static final String HEALTHDATA_KEY_PASSWORD = "m4lcrRUrZM";
	private static final String HEALTHDATA_KEY_ALIAS = "hd-key";

	private Cipher encryptionCipher;
	private Cipher decryptionCipher;

	@PostConstruct
	public void init() {
		final KeyStore keyStore = loadKeyStore();

		final Key key = getKey(keyStore);

		encryptionCipher = createCipher(key, Cipher.ENCRYPT_MODE);
		decryptionCipher = createCipher(key, Cipher.DECRYPT_MODE);
	}

	private Cipher createCipher(Key key, int encryptMode) {
		try {
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			cipher.init(encryptMode, key);
			return cipher;
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, "Error while creating encryption cipher");
			throw exception;
		}
	}

	private KeyStore loadKeyStore() {
		try {
			final KeyStore ks = KeyStore.getInstance(KEY_STORE_TYPE);
			ks.load(getClass().getClassLoader().getResourceAsStream(KEY_STORE_FILE), KEY_STORE_PASSWORD.toCharArray());
			return ks;
		} catch (NoSuchAlgorithmException | CertificateException | IOException | KeyStoreException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.ERROR_WHILE_LOADING_KEYSTORE);
			throw exception;

		}
	}

	private Key getKey(KeyStore keyStore) {
		try {
			return keyStore.getKey(HEALTHDATA_KEY_ALIAS, HEALTHDATA_KEY_PASSWORD.toCharArray());
		} catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.ERROR_WHILE_LOADING_KEYSTORE);
			throw exception;
		}
	}

	@Override
	public byte[] encrypt(byte[] bytes) {
		try {
			return encryptionCipher.doFinal(bytes);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, "Error while encrypting data");
			throw exception;

		}
	}

	@Override
	public byte[] decrypt(byte[] bytes) {
		try {
			return decryptionCipher.doFinal(bytes);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, MessageFormat.format("Error while decrypting data: {0}", new String(Base64.encode(bytes))));
			throw exception;
		}
	}
}
