/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.impl;

import be.wiv_isp.healthdata.catalogue.standalone.dao.IPdfTemplateDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.PdfTemplate;
import be.wiv_isp.healthdata.catalogue.standalone.service.IPdfTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PdfTemplateService implements IPdfTemplateService {

	@Autowired
	private IPdfTemplateDao dao;

	@Override
	public PdfTemplate get(Long dataCollectionDefinitionId) {
		return dao.get(dataCollectionDefinitionId);
	}

	@Override
	public PdfTemplate create(PdfTemplate pdfTemplate) {
		return dao.create(pdfTemplate);
	}

	@Override
	public PdfTemplate update(PdfTemplate pdfTemplate) {
		return dao.update(pdfTemplate);
	}

	@Override
	public void delete(PdfTemplate pdfTemplate) {
		dao.delete(pdfTemplate);
	}
}
