/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.dao.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IHd4dpUserRequestDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Hd4dpUserRequest;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.Hd4dpUserRequestSearch;
import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class Hd4dpUserRequestDao extends CrudDaoV2<Hd4dpUserRequest, Hd4dpUserRequest.Hd4dpUserRequestId, Hd4dpUserRequestSearch> implements IHd4dpUserRequestDao {

    public Hd4dpUserRequestDao() {
        super(Hd4dpUserRequest.class);
    }

    @Override
    protected List<Predicate> getPredicates(Hd4dpUserRequestSearch search, CriteriaBuilder cb, Root<Hd4dpUserRequest> rootEntry) {
        return null;
    }

}
