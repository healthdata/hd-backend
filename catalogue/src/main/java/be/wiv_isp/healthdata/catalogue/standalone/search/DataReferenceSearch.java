/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.search;

import java.sql.Timestamp;

import be.wiv_isp.healthdata.catalogue.standalone.enumeration.SearchFilterType;
import be.wiv_isp.healthdata.catalogue.standalone.enumeration.SortingType;

public class DataReferenceSearch {

	private String type;

	private SearchFilterType filter = SearchFilterType.ALL;

	private SortingType sorting = SortingType.SEARCH_FIRST;

	private String code;

	private String parentCode;

	private String value;

	private String language;

	private Timestamp validOn;

	private int limit;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public SearchFilterType getFilter() {
		return filter;
	}

	public void setFilter(SearchFilterType filter) {
		this.filter = filter;
	}

	public SortingType getSorting() {
		return sorting;
	}

	public void setSorting(SortingType sorting) {
		this.sorting = sorting;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Timestamp getValidOn() {
		return validOn;
	}

	public void setValidOn(Timestamp validOn) {
		this.validOn = validOn;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getLimit() {
		return limit;
	}
}
