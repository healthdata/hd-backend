/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.v1.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import be.wiv_isp.healthdata.catalogue.standalone.domain.Translation;

public class TranslationRequestMapperV1 {

	@SuppressWarnings("unchecked")
	public List<Translation> convert(JSONArray json) throws Exception {
		List<Translation> translations = new ArrayList<>();
		for (int i = 0; i < json.length(); i++) {
			JSONObject jsonObject = json.getJSONObject(i);
			Translation translation = new Translation();
			for (Iterator<String> iterator = jsonObject.keys(); iterator.hasNext();) {
				String key = iterator.next();
				if ("key".equalsIgnoreCase(key)) {
					translation.setKey(jsonObject.getString(key));
				} else if ("nl".equalsIgnoreCase(key)) {
					translation.setNl(jsonObject.getString(key));
				} else if ("fr".equalsIgnoreCase(key)) {
					translation.setFr(jsonObject.getString(key));
				}
			}
			translations.add(translation);
		}
		return translations;
	}
}
