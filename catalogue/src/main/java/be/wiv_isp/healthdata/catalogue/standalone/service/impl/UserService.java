/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.impl;

import be.wiv_isp.healthdata.catalogue.standalone.dao.IUserDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.User;
import be.wiv_isp.healthdata.catalogue.standalone.security.ILoginAttemptService;
import be.wiv_isp.healthdata.catalogue.standalone.service.IUserService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.NoResultException;

@Component(value = "userService")
public class UserService implements IUserService, UserDetailsService {

	@Autowired
	private IUserDao userDao;

	@Autowired
	private ILoginAttemptService loginAttemptService;

	@Resource(name = "passwordEncoder")
	private PasswordEncoder passwordEncoder;

	@Override
	@Transactional
	public User create(User user) {
		String encPassword = encodePassword(user.getPassword());
		user.setPassword(encPassword);
		return userDao.create(user);
	}

	@Override
	public User get(Long id) {
		return userDao.get(id);
	}

	@Override
	public User get(String userName) {
		return userDao.get(userName);

	}

	@Override
	@Transactional
	public User update(User user) {
		return update(user, false);
	}

	@Override
	@Transactional
	public User update(User user, boolean passwordEncodingNeeded) {
		if (passwordEncodingNeeded) {
			String encPassword = encodePassword(user.getPassword());
			user.setPassword(encPassword);
		}
		return userDao.update(user);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		User user = get(id);
		userDao.delete(user);
	}

	private String encodePassword(String password) {
		if (password == null) {
			return null;
		}

		return passwordEncoder.encode(password);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		String blockedIp = loginAttemptService.isBlocked();
		if (StringUtils.isNotBlank(blockedIp)) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.LOGIN_BLOCKED, blockedIp);
			throw exception;
		}
		try {
			return userDao.get(username);
		} catch (NoResultException e) {
			throw new UsernameNotFoundException("User Not Found", e);
		}
	}
}
