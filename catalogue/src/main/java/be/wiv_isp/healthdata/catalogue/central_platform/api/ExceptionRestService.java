/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.catalogue.central_platform.api;

import be.wiv_isp.healthdata.catalogue.central_platform.domain.ExceptionReport;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Installation;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.mapper.ExceptionReportMapper;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.ExceptionReportSearch;
import be.wiv_isp.healthdata.catalogue.central_platform.dto.ExceptionReportDto;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IExceptionReportService;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IInstallationService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;

@Component
@Path("/exceptions")
public class ExceptionRestService {

	private static final Logger LOG = LoggerFactory.getLogger(ExceptionRestService.class);

	@Autowired
	private IInstallationService installationService;
	@Autowired
	private IExceptionReportService exceptionReportService;

	@POST
	@Path("/installations/{installationId}")
	@PreAuthorize("hasRole('ROLE_INSTALLATION')")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response exceptionForInstallation(@PathParam("installationId") final Long installationId, final String json) {
		try {
			installationService.verifyAccess(installationId);
			ExceptionReport exceptionReport = ExceptionReportMapper.convert(new JSONObject(json));
			exceptionReport.setInstallationId(installationId);
			exceptionReportService.create(exceptionReport);
			return Response.noContent().build();
		} catch (JSONException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.JSON_PARSE_EXCEPTION);
			throw exception;
		}
	}

	@GET
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getExceptionForInstallation(@Context UriInfo info) {
		ExceptionReportSearch exceptionReportSearch = ExceptionReportMapper.convertSearch(info);
		List<ExceptionReport> all = exceptionReportService.getAll(exceptionReportSearch);
		List<ExceptionReportDto> result = new ArrayList<>();
		for (ExceptionReport exceptionReport : all) {
			Installation installation = installationService.get(exceptionReport.getInstallationId());
			ExceptionReportDto convert = ExceptionReportMapper.convert(exceptionReport, installation.getOrganizations());
			result.add(convert);
		}
		return Response.ok(result).build();
	}
}
