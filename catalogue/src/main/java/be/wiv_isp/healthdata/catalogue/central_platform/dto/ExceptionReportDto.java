/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.catalogue.central_platform.dto;

import be.wiv_isp.healthdata.catalogue.central_platform.domain.Organization;
import be.wiv_isp.healthdata.common.json.serializer.DateSerializer;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ExceptionReportDto {

    private InstallationDto installation = new InstallationDto();
    private String message;
    private String stackTrace;
    private Set<String> uuids;
    @JsonSerialize(using = DateSerializer.class)
    private Timestamp startTime;
    @JsonSerialize(using = DateSerializer.class)
    private Timestamp endTime;

    public InstallationDto getInstallation() {
        return installation;
    }

    public void setInstallation(InstallationDto installation) {
        this.installation = installation;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    public Set<String> getUuids() {
        return uuids;
    }

    public void setUuids(Set<String> uuids) {
        this.uuids = uuids;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public class InstallationDto {

        private List<OrganizationDto> organizations = new ArrayList<>();

        public void addOrganization(Organization organization) {
            OrganizationDto dto = new OrganizationDto();
            dto.setIdentificationValue(organization.getIdentificationValue());
            dto.setName(organization.getName());
            dto.setMain(organization.isMain());
            dto.setDeleted(organization.isDeleted());
            if(organization.isMain()) {
                organizations.add(0, dto);
            } else {
                organizations.add(dto);
            }
        }

        public List<OrganizationDto> getOrganizations() {
            return organizations;
        }

        public void setOrganizations(List<OrganizationDto> organizations) {
            this.organizations = organizations;
        }

        public class OrganizationDto {
            private String identificationValue;
            private String name;
            private boolean main;
            private boolean deleted;

            public String getIdentificationValue() {
                return identificationValue;
            }

            public void setIdentificationValue(String identificationValue) {
                this.identificationValue = identificationValue;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public boolean isMain() {
                return main;
            }

            public void setMain(boolean main) {
                this.main = main;
            }

            public boolean isDeleted() {
                return deleted;
            }

            public void setDeleted(boolean deleted) {
                this.deleted = deleted;
            }
        }
    }
}
