/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.catalogue.central_platform.dao.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IExceptionReportDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.ExceptionReport;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.ExceptionReportSearch;
import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Repository
public class ExceptionReportDao extends CrudDaoV2<ExceptionReport, Long, ExceptionReportSearch> implements IExceptionReportDao {

    public ExceptionReportDao() {
        super(ExceptionReport.class);
    }

    @Override
    protected List<Predicate> getPredicates(ExceptionReportSearch search, CriteriaBuilder cb, Root<ExceptionReport> rootEntry) {
        List<Predicate> predicates = new ArrayList<>();
        if (search.getInstallationId() != null) {
            predicates.add(cb.equal(rootEntry.get("installationId"), search.getInstallationId()));
        }
        if(StringUtils.isNotBlank(search.getReportDate())) {
            SimpleDateFormat sdf = new SimpleDateFormat(DateFormat.DATE.getPattern());
            Date minDate = null;
            try {
                minDate = sdf.parse(search.getReportDate());
            } catch (ParseException e) {
                HealthDataException exception = new HealthDataException(search);
                exception.setResponseStatus(Response.Status.BAD_REQUEST);
                exception.setResponseMessage(ExceptionType.INVALID_DATE_FORMAT.getMessage(), search.getReportDate(), DateFormat.DATE.getPattern());
                throw exception;
            }
            Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1) - TimeUnit.SECONDS.toMillis(1));

            Path<Timestamp> from = rootEntry.get("startTime");
            Predicate startPredicate = cb.lessThanOrEqualTo(from, maxDate);
            Path<Timestamp> till = rootEntry.get("endTime");
            Predicate endPredicate = cb.greaterThanOrEqualTo(till, minDate);
            Predicate finalCondition = cb.and(startPredicate, endPredicate);

            predicates.add(finalCondition);
        }
        return predicates;
    }
}
