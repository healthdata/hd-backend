/*
 *  Copyright 2014-2017 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.catalogue.standalone.dto;


import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionGroup;
import be.wiv_isp.healthdata.common.dto.PeriodDto;
import be.wiv_isp.healthdata.common.dto.TranslatableStringDto;
import be.wiv_isp.healthdata.common.json.deserializer.JsonToByteArrayDeserializer;
import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.ByteArrayToJsonSerializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.Serializable;
import java.sql.Timestamp;

public class DataCollectionGroupDto implements Serializable {

    private static final Long serialVersionUID = 1L;

    private Long id;
    private String name;
    private TranslatableStringDto label;
    private TranslatableStringDto description;
    private Integer majorVersion;
    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp createdOn;
    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp updatedOn;
    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp startDate;
    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp endDateCreation;
    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp endDateSubmission;
    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp endDateComments;
    @JsonSerialize(using = ByteArrayToJsonSerializer.class)
    @JsonDeserialize(using = JsonToByteArrayDeserializer.class)
    private byte[] participationContent;
    private PeriodDto period;

    public DataCollectionGroupDto() {
    }

    public DataCollectionGroupDto(DataCollectionGroup dataCollectionGroup) {
        id = dataCollectionGroup.getId();
        name = dataCollectionGroup.getName();
        label = new TranslatableStringDto(dataCollectionGroup.getLabel().getEn(), dataCollectionGroup.getLabel().getFr(), dataCollectionGroup.getLabel().getNl());
        description = new TranslatableStringDto(dataCollectionGroup.getDescription().getEn(), dataCollectionGroup.getDescription().getFr(), dataCollectionGroup.getDescription().getNl());
        majorVersion = dataCollectionGroup.getMajorVersion();
        createdOn = dataCollectionGroup.getCreatedOn();
        updatedOn = dataCollectionGroup.getUpdatedOn();
        startDate = dataCollectionGroup.getStartDate();
        endDateCreation = dataCollectionGroup.getEndDateCreation();
        endDateSubmission = dataCollectionGroup.getEndDateSubmission();
        endDateComments = dataCollectionGroup.getEndDateComments();
        participationContent = dataCollectionGroup.getParticipationContent();
        if (dataCollectionGroup.getPeriod() != null) {
            period = new PeriodDto(dataCollectionGroup.getPeriod().getStart(), dataCollectionGroup.getPeriod().getEnd());
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TranslatableStringDto getLabel() {
        return label;
    }

    public void setLabel(TranslatableStringDto label) {
        this.label = label;
    }

    public TranslatableStringDto getDescription() {
        return description;
    }

    public void setDescription(TranslatableStringDto description) {
        this.description = description;
    }

    public Integer getMajorVersion() {
        return majorVersion;
    }

    public void setMajorVersion(Integer majorVersion) {
        this.majorVersion = majorVersion;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDateCreation() {
        return endDateCreation;
    }

    public void setEndDateCreation(Timestamp endDateCreation) {
        this.endDateCreation = endDateCreation;
    }

    public Timestamp getEndDateSubmission() {
        return endDateSubmission;
    }

    public void setEndDateSubmission(Timestamp endDateSubmission) {
        this.endDateSubmission = endDateSubmission;
    }

    public Timestamp getEndDateComments() {
        return endDateComments;
    }

    public void setEndDateComments(Timestamp endDateComments) {
        this.endDateComments = endDateComments;
    }

    public byte[] getParticipationContent() {
        return participationContent;
    }

    public void setParticipationContent(byte[] participationContent) {
        this.participationContent = participationContent;
    }

    public PeriodDto getPeriod() {
        return period;
    }

    public void setPeriod(PeriodDto period) {
        this.period = period;
    }
}
