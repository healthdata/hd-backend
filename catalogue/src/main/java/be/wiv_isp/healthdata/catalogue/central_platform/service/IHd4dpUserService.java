/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.service;

import be.wiv_isp.healthdata.catalogue.central_platform.domain.Hd4dpUser;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.JsonObjectsByOrganization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.Hd4dpUserSearch;
import be.wiv_isp.healthdata.common.service.IAbstractService;

public interface IHd4dpUserService extends IAbstractService<Hd4dpUser, Hd4dpUser.Hd4dpUserId, Hd4dpUserSearch> {

    void update(Long installationId, JsonObjectsByOrganization hd4dpUsers);

}
