/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.aspect;

import be.wiv_isp.healthdata.catalogue.standalone.annotation.Auditable;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Audit;
import be.wiv_isp.healthdata.catalogue.standalone.service.IAuditService;
import be.wiv_isp.healthdata.common.domain.enumeration.StatusResult;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import com.sun.jersey.core.spi.factory.ResponseImpl;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.core.UriInfo;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Aspect
@Component
public class AuditingAspect {

	private static final Logger LOG = LoggerFactory.getLogger(AuditingAspect.class);

	@Autowired
	private IAuditService auditService;

	@Around("@annotation(auditable)")
	@Transactional
	public Object createAudit(ProceedingJoinPoint pjp, Auditable auditable) throws Throwable {
		final String apiType = auditable.apiType().getDescription();
		final String methodName = pjp.getSignature().getName();

		MethodSignature signature = (MethodSignature) pjp.getSignature();
		List<String> parameterNames = Arrays.asList(signature.getParameterNames());
		List<Object> parameterValues = Arrays.asList(pjp.getArgs());

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < parameterNames.size(); i++) {
			sb.append(parameterNames.get(i));
			sb.append(" = ");
			sb.append(limitLength(parameterToString(parameterValues, i), 200));
			if (i != parameterNames.size() - 1) {
				sb.append(" & ");
			}
		}
		final String subject = sb.toString();
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = null;
		if (authentication != null) {
			if ("anonymousUser".equals(authentication.getPrincipal().toString())) {
				username = "anonymousUser";
			} else {
				final UserDetails principal = (UserDetails) authentication.getPrincipal();
				username = principal.getUsername();
			}
		}

		LOG.debug("Creating Auditing entry: apiType=" + apiType + ", methodName=" + methodName + ", subject=" + subject + ", username=" + username);
		Audit audit = new Audit();
		audit.setApi(apiType);
		audit.setMethod(methodName);
		audit.setSubject(subject);
		audit.setUserName(username);
		audit = auditService.create(audit);

		String status;
		String exceptionMessage = null;
		try {
			Object output = pjp.proceed();
			status = getStatus(output);
			audit.setStatus(status);
			LOG.debug("Updating Auditing entry: status=" + status + ", exceptionMessage=" + exceptionMessage);
			auditService.update(audit);
			return output;
		} catch (HealthDataException e) {
			status = String.valueOf(e.getResponseStatus().getStatusCode());
			exceptionMessage = e.getMessage();
			audit.setStatus(status);
			audit.setExceptionMessage(exceptionMessage);
			LOG.debug("Updating Auditing entry: status=" + status + ", exceptionMessage=" + exceptionMessage);
			auditService.update(audit);
			throw e;
		}
	}

	private String parameterToString(List<Object> parameterValues, int i) {
		Object parameter = parameterValues.get(i);
		if (parameter instanceof UriInfo) {
			StringBuilder sb = new StringBuilder();
			UriInfo uriInfo = (UriInfo) parameter;
			if (!uriInfo.getPathParameters().isEmpty()) {
				sb.append("path: ");
				sb.append(uriInfo.getPathParameters());
				sb.append(" ");
			}
			if (!uriInfo.getQueryParameters().isEmpty()) {
				sb.append("query: ");
				sb.append(uriInfo.getQueryParameters());
				sb.append(" ");
			}
			return sb.toString();
		}
		return String.valueOf(parameter);
	}

	private String getStatus(Object output) {
		StringBuilder sb;
		String status;
		if (output instanceof HashMap) {
			Map<String, StatusResult> map = (HashMap<String, StatusResult>) output;
			sb = new StringBuilder();
			int i = 1;
			for (Map.Entry<String, StatusResult> entry : map.entrySet()) {
				sb.append(entry.getKey());
				sb.append("=");
				sb.append(entry.getValue().name());
				if (i != map.size()) {
					sb.append(" & ");
				}
				i++;
			}
			status = sb.toString();
		} else {
			status = String.valueOf(((ResponseImpl) output).getStatusType().getStatusCode());
		}
		return status;
	}

	private String limitLength(String value, int length) {
		if (value.length() <= length) {
			return value;
		} else {
			return value.substring(0, length - 3).concat("...");
		}
	}

	public IAuditService getAuditService() {
		return auditService;
	}

	public void setAuditService(IAuditService auditService) {
		this.auditService = auditService;
	}
}
