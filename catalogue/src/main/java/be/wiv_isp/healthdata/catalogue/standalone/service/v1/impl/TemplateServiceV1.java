/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.v1.impl;

import be.wiv_isp.healthdata.catalogue.standalone.domain.Template;
import be.wiv_isp.healthdata.catalogue.standalone.service.ITemplateService;
import be.wiv_isp.healthdata.catalogue.standalone.service.v1.ITemplateServiceV1;
import be.wiv_isp.healthdata.common.dto.TemplateDtoV1;
import be.wiv_isp.healthdata.common.domain.enumeration.TemplateKey;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class TemplateServiceV1 implements ITemplateServiceV1 {

	@Autowired
	private ITemplateService templateService;

	@Override
	public Response get(UriInfo info) {
		TemplateKey templateKey = TemplateKey.getKey(info);
		try {
			Template template = templateService.get(templateKey);
			return Response.ok(convert(template)).build();
		} catch (NoResultException e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}

	@Override
	public Response getAll() {
		List<Template> templates = templateService.getAll();
		return Response.ok(convert(templates)).build();
	}

	@Override
	public Response update(UriInfo info, JSONObject json) {
		try {
			Template template = TemplateRequestMapperV1.convert(json);
			TemplateKey templateKey = TemplateKey.getKey(info);
			if (templateKey == null || !templateKey.toString().equalsIgnoreCase(template.getKey())) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.INVALID_INPUT, MessageFormat.format("The id provided in the url ({0}) does not match with the id provided in the request body ({1})", templateKey, template.getId()));
				throw exception;
			}

			Template templateFromDB = templateService.get(templateKey);
			templateFromDB.setSubject(template.getSubject());
			templateFromDB.setTemplateText(template.getTemplateText());

			Template updatedTemplate = templateService.update(templateFromDB);
			return Response.ok(convert(updatedTemplate)).build();
		} catch (JSONException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.JSON_PARSE_EXCEPTION);
			throw exception;

		}
	}

	@Override
	public Response create(UriInfo info, JSONObject json) {
		try {
			Template template = TemplateRequestMapperV1.convert(json);
			TemplateKey templateKey = TemplateKey.getKey(info);
			if (templateKey == null || !templateKey.toString().equalsIgnoreCase(template.getKey())) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.INVALID_INPUT, MessageFormat.format("The id provided in the url ({0}) does not match with the id provided in the request body ({1})", templateKey, template.getId()));
				throw exception;
			}

			Template createdTemplate = templateService.create(template);
			return Response.ok(convert(createdTemplate)).build();
		} catch (JSONException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.JSON_PARSE_EXCEPTION);
			throw exception;
		}
	}

	private List<TemplateDtoV1> convert(List<Template> templates) {
		List<TemplateDtoV1> converted = new ArrayList<>();
		for (Template template : templates) {
			converted.add(convert(template));
		}
		return converted;
	}


	private TemplateDtoV1 convert(Template template) {
		if (template == null) {
			return null;
		}
		final TemplateDtoV1 templateDtoV1 = new TemplateDtoV1();
		templateDtoV1.setId(template.getId());
		templateDtoV1.setKey(template.getKey());
		templateDtoV1.setSubject(template.getSubject());
		templateDtoV1.setText(new String(template.getTemplateText(), StandardCharsets.UTF_8));
		TemplateKey templateKey = TemplateKey.valueOf(template.getKey());
		return templateDtoV1;
	}
}
