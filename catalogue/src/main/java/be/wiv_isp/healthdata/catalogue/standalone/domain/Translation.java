/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "TRANSLATIONS")
public class Translation implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "TRANSLATION_ID")
	private Long id;

	@Column(name = "LOOKUP_KEY")
	private String key;

	@Column(name = "NL")
	private String nl;

	@Column(name = "FR")
	private String fr;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getNl() {
		return nl;
	}

	public void setNl(String nl) {
		this.nl = nl;
	}

	public String getFr() {
		return fr;
	}

	public void setFr(String fr) {
		this.fr = fr;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof Translation) {
			Translation other = (Translation) o;

			return Objects.equals(key, other.key);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(//
				this.key);
	}

	@Override
	public String toString() {
		return "Translation {" + //
				"key = " + Objects.toString(this.key) + ", " + //
				"nl = " + Objects.toString(this.nl) + ", " + //
				"fr = " + Objects.toString(this.fr) + "}";
	}
}
