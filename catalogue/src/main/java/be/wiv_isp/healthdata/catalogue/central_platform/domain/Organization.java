/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.domain;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ORGANIZATIONS")
public class Organization {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ORGANIZATION_ID")
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "INSTALLATION_ID", nullable = false)
    private Installation installation;

    @Column(name = "IDENTIFICATION_VALUE", nullable = false)
    private String identificationValue;

    @Column(name = "NAME")
    private String name;

    @Column(name = "MAIN")
    private boolean main;

    @Column(name = "DELETED")
    private boolean deleted;

    @Fetch(FetchMode.SUBSELECT)
    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL }, orphanRemoval = true, mappedBy = "organization")
    private List<RegistrationStatistic> registrationStatistics = new ArrayList<>();

    @Fetch(FetchMode.SUBSELECT)
    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL }, orphanRemoval = true, mappedBy = "organization")
    private List<StableDataStatistic> stableDataStatistics = new ArrayList<>();

    @Fetch(FetchMode.SUBSELECT)
    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL }, orphanRemoval = true, mappedBy = "organization")
    private List<Hd4dpUser> users = new ArrayList<>();

    @Fetch(FetchMode.SUBSELECT)
    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL }, orphanRemoval = true, mappedBy = "organization")
    private List<Hd4dpUserRequest> userRequests = new ArrayList<>();

    @Fetch(FetchMode.SUBSELECT)
    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL}, orphanRemoval = true, mappedBy = "organization")
    private List<Hd4dpConfiguration> configurations = new ArrayList<>();

    @Fetch(FetchMode.SUBSELECT)
    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL}, orphanRemoval = true, mappedBy = "organization")
    private List<MessageStatistic> messageStatistics = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Installation getInstallation() {
        return installation;
    }

    public void setInstallation(Installation installation) {
        this.installation = installation;
    }

    public String getIdentificationValue() {
        return identificationValue;
    }

    public void setIdentificationValue(String identificationValue) {
        this.identificationValue = identificationValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isMain() {
        return main;
    }

    public void setMain(boolean main) {
        this.main = main;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public List<RegistrationStatistic> getRegistrationStatistics() {
        return registrationStatistics;
    }

    public void setRegistrationStatistics(List<RegistrationStatistic> registrationStatistics) {
        this.registrationStatistics = registrationStatistics;
    }

    public List<StableDataStatistic> getStableDataStatistics() {
        return stableDataStatistics;
    }

    public void setStableDataStatistics(List<StableDataStatistic> stableDataStatistics) {
        this.stableDataStatistics = stableDataStatistics;
    }

    public List<Hd4dpUser> getUsers() {
        return users;
    }

    public void setUsers(List<Hd4dpUser> users) {
        this.users = users;
    }

    public List<Hd4dpUserRequest> getUserRequests() {
        return userRequests;
    }

    public void setUserRequests(List<Hd4dpUserRequest> userRequests) {
        this.userRequests = userRequests;
    }

    public List<Hd4dpConfiguration> getConfigurations() {
        return configurations;
    }

    public void setConfigurations(List<Hd4dpConfiguration> configurations) {
        this.configurations = configurations;
    }

    public List<MessageStatistic> getMessageStatistics() {
        return messageStatistics;
    }

    public void setMessageStatistics(List<MessageStatistic> messageStatistics) {
        this.messageStatistics = messageStatistics;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Organization))
            return false;

        Organization that = (Organization) o;

        if (main != that.main)
            return false;
        if (deleted != that.deleted)
            return false;
        if (id != null ? !id.equals(that.id) : that.id != null)
            return false;
        if (identificationValue != null ? !identificationValue.equals(that.identificationValue) : that.identificationValue != null)
            return false;
        return name != null ? name.equals(that.name) : that.name == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (identificationValue != null ? identificationValue.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (main ? 1 : 0);
        result = 31 * result + (deleted ? 1 : 0);
        return result;
    }
}
