/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.impl;

import be.wiv_isp.healthdata.catalogue.standalone.dao.IFollowUpDefinitionDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.FollowUpDefinition;
import be.wiv_isp.healthdata.catalogue.standalone.domain.search.FollowUpDefinitionSearch;
import be.wiv_isp.healthdata.catalogue.standalone.service.IFollowUpDefinitionService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FollowUpDefinitionService extends AbstractService<FollowUpDefinition, Long, FollowUpDefinitionSearch, IFollowUpDefinitionDao> implements IFollowUpDefinitionService {

    @Autowired
    private IFollowUpDefinitionDao dao;

    public FollowUpDefinitionService() {
        super(FollowUpDefinition.class);
    }

    @Override
    public FollowUpDefinition getUnique(FollowUpDefinitionSearch search) {
        final List<FollowUpDefinition> all = dao.getAll(search);

        if(CollectionUtils.isEmpty(all)) {
            return null;
        }
        if(all.size() > 1) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.NON_UNIQUE_SEARCH_RESULT, all.size(), "FOLLOW_UP_DEFINITION", search);
            throw exception;
        }
        return all.get(0);
    }

    @Override
    protected IFollowUpDefinitionDao getDao() {
        return dao;
    }
}
