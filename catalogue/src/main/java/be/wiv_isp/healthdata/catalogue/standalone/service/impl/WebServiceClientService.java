/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.impl;

import be.wiv_isp.healthdata.catalogue.standalone.service.IWebServiceClientService;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.common.util.HttpComponentStringBuilder;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.Response;
import java.text.MessageFormat;
import java.util.Map;
import java.util.Map.Entry;

@Component
public class WebServiceClientService implements IWebServiceClientService {

	private static final Logger LOG = LoggerFactory.getLogger("ws-logger");

	private static long REQUEST_ID = 0;

	private Client client;

	@PostConstruct
	public void createClient() {
		final ClientConfig config = new DefaultClientConfig();
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		client = Client.create(config);
	}

	@Override
	public Object callWebService(WebServiceBuilder builder) {
		Long requestId = getNewRequestId();
		LOG.info("Request (id: {}) \n\n{}", requestId, HttpComponentStringBuilder.webServiceBuilderToString(builder));

		if (client == null) {
			createClient();
		}
		WebResource wr = client.resource(builder.getUrl());
		Map<String, String> parameters = builder.getParameters();
		for (Entry<String, String> parameterEntry : parameters.entrySet()) {
			wr = wr.queryParam(parameterEntry.getKey(), parameterEntry.getValue());
		}
		Builder wrb = wr.getRequestBuilder();
		if (builder.getType() != null) {
			wrb = wrb.type(builder.getType());
		}
		if (builder.getAccept() != null) {
			wrb = wrb.accept(builder.getAccept());
		}
		Map<HttpHeaders, String> header = builder.getHeader();
		for (Entry<HttpHeaders, String> headerEntry : header.entrySet()) {
			wrb = wrb.header(headerEntry.getKey().getValue(), headerEntry.getValue());
		}
		if (builder.isPost()) {
			ClientResponse response;
			if (builder.getJson() != null) {
				response = wrb.post(ClientResponse.class, builder.getJson());
			} else {
				response = wrb.post(ClientResponse.class);
			}

			if (builder.logResponseBody()) {
				logHttpResponse(requestId, response, builder.logResponseBody());
			}

			if (builder.getReturnType().getRawClass().equals(ClientResponse.class)) {
				return response;
			} else {
				if (response.getStatus() >= 200 && response.getStatus() < 300) {
					return response.getEntity(builder.getReturnType());
				}
			}
		}
		if (builder.isGet()) {
			final ClientResponse response = wrb.get(ClientResponse.class);

			if (builder.logResponseBody()) {
				logHttpResponse(requestId, response, builder.logResponseBody());
			}

			if (builder.getReturnType().getRawClass().equals(ClientResponse.class)) {
				return response;
			} else {
				if (Response.Status.NOT_FOUND.getStatusCode() == response.getStatus()) {
					return null;
				} else if (Response.Status.NO_CONTENT.getStatusCode() == response.getStatus()) {
					return null;
				} else if (Response.Status.OK.getStatusCode() == response.getStatus()) {
					return response.getEntity(builder.getReturnType());
				}
			}
		}
		if (builder.isPut()) {
			ClientResponse response;
			if (builder.getJson() != null) {
				response = wrb.put(ClientResponse.class, builder.getJson());
			} else {
				response = wrb.put(ClientResponse.class);
			}

			if (builder.logResponseBody()) {
				logHttpResponse(requestId, response, builder.logResponseBody());
			}

			if (builder.getReturnType().getRawClass().equals(ClientResponse.class)) {
				return response;
			} else {
				if (response.getStatus() >= 200 && response.getStatus() < 300) {
					return response.getEntity(builder.getReturnType());
				}
			}
		}
		if (builder.isDelete()) {
			final ClientResponse response = wrb.delete(ClientResponse.class);

			if (builder.logResponseBody()) {
				logHttpResponse(requestId, response, builder.logResponseBody());
			}

			if (builder.getReturnType().getRawClass().equals(ClientResponse.class)) {
				return response;
			} else {
				if (response.getStatus() >= 200 && response.getStatus() < 300) {
					return response.getEntity(builder.getReturnType());
				}
			}
		}
		return null;
	}

	private synchronized long getNewRequestId() {
		return ++REQUEST_ID;
	}

	private void logHttpResponse(Long requestId, ClientResponse response, boolean withResponseBody) {
		if (response.getStatus() >= 200 && response.getStatus() < 300) {
			final String logMessage = MessageFormat.format("Response (request id: {0}) \n\n{1}", requestId.toString(), HttpComponentStringBuilder.clientResponseToString(response, withResponseBody));
			LOG.info(logMessage);
		} else {
			final String logMessage = MessageFormat.format("Response (request id: {0}) \n\n{1}", requestId.toString(), HttpComponentStringBuilder.clientResponseToString(response, true));
			LOG.error(logMessage);
		}
	}
}