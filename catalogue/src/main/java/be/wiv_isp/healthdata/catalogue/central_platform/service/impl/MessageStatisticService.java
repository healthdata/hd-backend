/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.catalogue.central_platform.service.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IMessageStatisticDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.JsonObjectsByOrganization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.MessageStatistic;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Organization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.MessageStatisticSearch;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IMessageStatisticService;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IOrganizationService;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MessageStatisticService extends AbstractService<MessageStatistic, MessageStatistic.MessageStatisticId, MessageStatisticSearch, IMessageStatisticDao> implements IMessageStatisticService {

    private static final Logger LOG = LoggerFactory.getLogger(MessageStatisticService.class);

    @Autowired
    private IOrganizationService organizationService;

    @Autowired
    private IMessageStatisticDao messageStatisticDao;


    public MessageStatisticService() {
        super(MessageStatistic.class);
    }

    @Override
    public IMessageStatisticDao getDao() {
        return messageStatisticDao;
    }

    @Override
    @Transactional
    public void update(final Long installationId, final JsonObjectsByOrganization data) {
        LOG.info("Updating message statistics for installation {}", installationId);

        final List<Organization> organizations = organizationService.getAllByInstallationId(installationId);

        for (final Organization organization : organizations) {
            updateOrganization(organization, data);
        }

        LOG.info("Message statistics successfully updated for installation {}", installationId);
    }

    private void updateOrganization(Organization organization, JsonObjectsByOrganization data) {
        LOG.debug("Updating message statistics for organization {} (id value = {})", organization.getId(), organization.getIdentificationValue());

        organization.getMessageStatistics().clear();
        final List<JSONObject> jsonObjects = data.getJsonObjects(organization.getIdentificationValue());
        for (final JSONObject jsonObject : jsonObjects) {
            final MessageStatistic messageStatistic = unmarshal(jsonObject);
            messageStatistic.setOrganization(organization);
            organization.getMessageStatistics().add(messageStatistic);
        }
        organizationService.update(organization);
        LOG.debug("Message statistics successfully updated for organization {} (id value = {})", organization.getId(), organization.getIdentificationValue());
    }

    private MessageStatistic unmarshal(final JSONObject json) {
        final MessageStatistic messageStatistic = new MessageStatistic();
        messageStatistic.setStatus(JsonUtils.getString(json, "status"));
        messageStatistic.setCount(JsonUtils.getLong(json, "count"));
        return messageStatistic;
    }

}
