/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.domain;

import be.wiv_isp.healthdata.common.domain.Baseline;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "FOLLOW_UP_DEFINITIONS", uniqueConstraints = @UniqueConstraint(columnNames = {"LABEL", "DATA_COLLECTION_DEFINITION_ID"}))
public class FollowUpDefinition implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "FOLLOW_UP_DEFINITION_ID")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "DATA_COLLECTION_DEFINITION_ID", nullable = false)
    private DataCollectionDefinition dataCollectionDefinition;

    @Column(name = "LABEL", nullable = false)
    private String label;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @Column(name = "TIMING", nullable = false)
    private String timing;

    @Embedded
    private Baseline baseline;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "FOLLOW_UP_DEFINITION_CONDITIONS", joinColumns=@JoinColumn(name="FOLLOW_UP_DEFINITION_ID", nullable = false))
    @Column(name = "CONDITION_LABEL")
    private List<String> conditions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public DataCollectionDefinition getDataCollectionDefinition() {
        return dataCollectionDefinition;
    }

    public void setDataCollectionDefinition(DataCollectionDefinition dataCollectionDefinition) {
        this.dataCollectionDefinition = dataCollectionDefinition;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTiming() {
        return timing;
    }

    public void setTiming(String timing) {
        this.timing = timing;
    }

    public Baseline getBaseline() {
        return baseline;
    }

    public void setBaseline(Baseline baseline) {
        this.baseline = baseline;
    }

    public List<String> getConditions() {
        return conditions;
    }

    public void setConditions(List<String> conditions) {
        this.conditions = conditions;
    }

    public void addCondition(String condition) {
        if(conditions == null) {
            conditions = new ArrayList<>();
        }
        conditions.add(condition);
    }
}