/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.catalogue.central_platform.domain.mapper;

import be.wiv_isp.healthdata.catalogue.central_platform.domain.ExceptionReport;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Organization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.ExceptionReportSearch;
import be.wiv_isp.healthdata.catalogue.central_platform.dto.ExceptionReportDto;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import javax.ws.rs.core.UriInfo;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ExceptionReportMapper {

    static public ExceptionReport convert(JSONObject json) throws JSONException {
        ExceptionReport report = new ExceptionReport();

        for (Iterator<String> iterator = json.keys(); iterator.hasNext(); ) {
            String key = iterator.next();
            if (!"null".equalsIgnoreCase(json.getString(key))) {
                if ("message".equalsIgnoreCase(key)) {
                    report.setMessage(json.getString(key));
                } else if ("stackTrace".equalsIgnoreCase(key)) {
                    report.setStackTrace(json.getString(key));
                } else if ("uuids".equalsIgnoreCase(key)) {
                    if (!"null".equalsIgnoreCase(json.getString(key))) {
                        final JSONArray uuidsJson = json.getJSONArray(key);
                        Set<String> uuids = new HashSet<>();
                        for (int k = 0; k < uuidsJson.length(); ++k) {
                            uuids.add(uuidsJson.getString(k));
                        }
                        report.setUuids(uuids);
                    }
                } else if ("startTime".equalsIgnoreCase(key)) {
                    report.setStartTime(JsonUtils.getTimestamp(json, key));
                } else if ("endTime".equalsIgnoreCase(key)) {
                    report.setEndTime(JsonUtils.getTimestamp(json, key));
                }
            }
        }
        return report;
    }

    static public ExceptionReportSearch convertSearch(UriInfo info) {
        ExceptionReportSearch search = new ExceptionReportSearch();
        Long installationId = RestUtils.getParameterLong(info, RestUtils.ParameterName.INSTALLATION_ID);
        search.setInstallationId(installationId);
        String reportDate = RestUtils.getParameterString(info, RestUtils.ParameterName.REPORT_DATE);
        search.setReportDate(reportDate);
        return search;
    }

    static public ExceptionReportDto convert(ExceptionReport exceptionReport, List<Organization> organizations) {
        ExceptionReportDto dto = new ExceptionReportDto();

        dto.setMessage(exceptionReport.getMessage());
        dto.setStackTrace(exceptionReport.getStackTrace());
        dto.setUuids(exceptionReport.getUuids());
        dto.setStartTime(exceptionReport.getStartTime());
        dto.setEndTime(exceptionReport.getEndTime());
        for (Organization organization : organizations) {
            dto.getInstallation().addOrganization(organization);
        }
        return dto;
    }
}