/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.v1.impl;

import be.wiv_isp.healthdata.catalogue.standalone.domain.Template;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.Iterator;

public class TemplateRequestMapperV1 {

	static public Template convert(JSONObject json) throws JSONException {
		Template template = new Template();
		for (Iterator<String> iterator = json.keys(); iterator.hasNext();) {
			String key = iterator.next();
			if ("key".equalsIgnoreCase(key)) {
				template.setKey(json.getString(key));
			} else if ("subject".equalsIgnoreCase(key)) {
				template.setSubject(json.getString(key));
			} else if ("text".equalsIgnoreCase(key)) {
				template.setTemplateText(json.getString(key).getBytes(StandardCharsets.UTF_8));
			}
		}
		return template;
	}
}
