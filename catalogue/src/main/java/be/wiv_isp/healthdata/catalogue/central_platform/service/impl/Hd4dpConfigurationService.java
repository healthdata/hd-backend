/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.catalogue.central_platform.service.impl;


import be.wiv_isp.healthdata.catalogue.central_platform.dao.IHd4dpConfigurationDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Hd4dpConfiguration;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.JsonObjectsByOrganization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Organization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.Hd4dpConfigurationSearch;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IHd4dpConfigurationService;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IOrganizationService;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class Hd4dpConfigurationService extends AbstractService<Hd4dpConfiguration, Hd4dpConfiguration.Hd4dpConfigurationId, Hd4dpConfigurationSearch, IHd4dpConfigurationDao> implements IHd4dpConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(Hd4dpConfigurationService.class);

    @Autowired
    private IOrganizationService organizationService;

    @Autowired
    private IHd4dpConfigurationDao hd4dpConfigurationDao;


    public Hd4dpConfigurationService() {
        super(Hd4dpConfiguration.class);
    }

    @Override
    public IHd4dpConfigurationDao getDao() {
        return hd4dpConfigurationDao;
    }

    @Override
    @Transactional
    public void update(final Long installationId, final JsonObjectsByOrganization data) {
        LOG.info("Updating HD4DP configurations for installation {}", installationId);

        final List<Organization> organizations = organizationService.getAllByInstallationId(installationId);

        for (final Organization organization : organizations) {
            updateOrganization(organization, data);
        }

        LOG.info("HD4DP configurations successfully updated for installation {}", installationId);
    }

    private void updateOrganization(final Organization organization, final JsonObjectsByOrganization statistics) {
        LOG.debug("HD4DP configuration for organization {} (id value = {})", organization.getId(), organization.getIdentificationValue());

        organization.getConfigurations().clear();
        final List<JSONObject> jsonObjects = statistics.getJsonObjects(organization.getIdentificationValue());
        for (final JSONObject jsonObject : jsonObjects) {
            final Hd4dpConfiguration hd4dpConfiguration= unmarshal(jsonObject);
            hd4dpConfiguration.setOrganization(organization);
            organization.getConfigurations().add(hd4dpConfiguration);
        }
        organizationService.update(organization);
        LOG.debug("HD4DP configuration successfully updated for organization {} (id value = {})", organization.getId(), organization.getIdentificationValue());
    }

    private Hd4dpConfiguration unmarshal(JSONObject json) {
        final Hd4dpConfiguration hd4dpConfiguration = new Hd4dpConfiguration();
        hd4dpConfiguration.setKey(JsonUtils.getString(json, "key"));
        hd4dpConfiguration.setValue(JsonUtils.getString(json, "value"));
        return hd4dpConfiguration;
    }
}
