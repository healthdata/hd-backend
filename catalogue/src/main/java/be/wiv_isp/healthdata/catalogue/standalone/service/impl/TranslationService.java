/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import be.wiv_isp.healthdata.common.caching.CacheManagementService;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import be.wiv_isp.healthdata.catalogue.standalone.dao.ITranslationDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Translation;
import be.wiv_isp.healthdata.catalogue.standalone.service.ITranslationService;

@Service
public class TranslationService implements ITranslationService {

	@Autowired
	private ITranslationDao dao;

	@Override
	@Cacheable(CacheManagementService.translationCache)
	public List<Translation> getAll() {
		return dao.getAll();
	}

	@Transactional
	@Override
	@CacheEvict(value = CacheManagementService.translationCache, allEntries = true)
	public void update(List<Translation> translations) {
		List<Translation> all = dao.getAll();
		Map<String, Translation> translationMap = new HashedMap<>();
		for (Translation translation : all) {
			translationMap.put(translation.getKey(), translation);
		}
		for (Translation translation : translations) {
			Translation currentTranslation = translationMap.get(translation.getKey());
			if (currentTranslation == null) {
				dao.create(translation);
			} else {
				if (StringUtils.isNoneBlank(translation.getNl())) {
					currentTranslation.setNl(translation.getNl());
				}
				if (StringUtils.isNoneBlank(translation.getFr())) {
					currentTranslation.setFr(translation.getFr());
				}
				dao.update(currentTranslation);
			}

		}
	}

	@Transactional
	@Override
	@CacheEvict(value = CacheManagementService.translationCache, allEntries = true)
	public void delete(List<Translation> translations) {
		List<Translation> all = dao.getAll();
		Map<String, Translation> translationMap = new HashedMap<>();
		for (Translation translation : all) {
			translationMap.put(translation.getKey(), translation);
		}
		for (Translation translation : translations) {
			Translation currentTranslation = translationMap.get(translation.getKey());
			dao.delete(currentTranslation);
		}
	}

}
