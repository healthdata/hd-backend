/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.central_platform.api;

import be.wiv_isp.healthdata.catalogue.central_platform.domain.Installation;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.JsonObjectsByOrganization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Organization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Upgrade;
import be.wiv_isp.healthdata.catalogue.central_platform.dto.InstallationDto;
import be.wiv_isp.healthdata.catalogue.central_platform.dto.OrganizationDto;
import be.wiv_isp.healthdata.catalogue.central_platform.dto.UpgradeDto;
import be.wiv_isp.healthdata.catalogue.central_platform.service.*;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Authority;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Configuration;
import be.wiv_isp.healthdata.catalogue.standalone.domain.User;
import be.wiv_isp.healthdata.catalogue.standalone.service.IConfigurationService;
import be.wiv_isp.healthdata.catalogue.standalone.service.IUserService;
import be.wiv_isp.healthdata.common.api.exception.UnauthorizedException;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.MessageFormat;
import java.util.*;

@Component
@Path("/centralplatform/installations")
public class CpInstallationRestService {

	private static final Logger LOG = LoggerFactory.getLogger(CpInstallationRestService.class);

	public static final int PASSWORD_LENGTH = 30;
	public static final String USERNAME_PATTERN = "INSTALLATION_{0}";

	@Autowired
	private IUserService userService;
	@Autowired
	private IInstallationService installationService;
	@Autowired
	private IConfigurationService configurationService;
	@Autowired
	private IRegistrationStatisticService registrationStatisticService;
	@Autowired
	private IStableDataStatisticService stableDataStatisticService;
    @Autowired
    private IHd4dpUserService hd4dpUserService;
	@Autowired
	private IHd4dpUserRequestService hd4dpUserRequestService;
    @Autowired
	private IHd4dpConfigurationService hd4dpConfigurationService;
    @Autowired
	private IMessageStatisticService messageStatisticService;
    @Autowired
	private IHd4dpStatusService hd4dpStatusService;

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(@HeaderParam("Registration-Token") final String registrationToken) {
		LOG.info("Registering new HD4DP installation");

		verifyInstallationRegistrationToken(registrationToken);

		LOG.debug("Creating new installation user");
		final User user = new User();
		user.setUsername(MessageFormat.format(USERNAME_PATTERN, DateFormat.TIMESTAMP.format(new Date())));
		final String password = RandomStringUtils.randomAlphanumeric(PASSWORD_LENGTH);
		user.setPassword(password);
		user.setAuthorities(new HashSet<>(Collections.singletonList(new Authority(Authority.INSTALLATION))));
		final User createdUser = userService.create(user);
		LOG.info("User [{}] created", createdUser.getUsername());

		LOG.debug("Creating new installation");
		final Installation installation = new Installation();
		installation.setUser(createdUser);
		final Installation createdInstallation = installationService.create(installation);
		LOG.info("Installation with id [{}] created", installation.getId());

		final InstallationDto installationDto = new InstallationDto(createdInstallation);
		installationDto.setPassword(password); // set password in clear text instead of encrypted
		return Response.ok(installationDto).build();
	}

	private void verifyInstallationRegistrationToken(final String token) {
		LOG.info("Verifying installation registration token");
		final String registrationToken = configurationService.get(Configuration.INSTALLATION_REGISTRATION_TOKEN).getValue();
		if(!registrationToken.equals(token)) {
			LOG.warn("Invalid registration token: {}", token);
			throw new UnauthorizedException();
		}
	}

	@GET
	@Path("/{installationId}")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_INSTALLATION')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("installationId") final Long installationId) {
		LOG.info("Retrieving installation with id {}", installationId);

		final Installation installation = installationService.verifyAccess(installationId);
		return Response.ok(new InstallationDto(installation)).build();
	}

	@POST
	@Path("/{installationId}/organizations")
	@PreAuthorize("hasRole('ROLE_INSTALLATION')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateOrganizations(@PathParam("installationId") final Long installationId, final List<OrganizationDto> organizationDtos) {
		LOG.info("Updating organizations for installation with id {}", installationId);

		final Installation installation = installationService.verifyAccess(installationId);

		final List<Organization> organizations = map(organizationDtos);
		final Installation updatedInstallation = installationService.updateOrganizations(installation, organizations);

		return Response.ok(new InstallationDto(updatedInstallation)).build();
	}

	private List<Organization> map(final List<OrganizationDto> dtos) {
		final List<Organization> organizations = new ArrayList<>();
		for (final OrganizationDto dto : dtos) {
			final Organization organization = new Organization();
			organization.setIdentificationValue(dto.getIdentificationValue());
			organization.setName(dto.getName());
			organization.setMain(dto.isMain());
			organization.setDeleted(dto.isDeleted());
			organizations.add(organization);
		}
		return organizations;
	}

	@POST
	@Path("/{installationId}/upgrade")
	@PreAuthorize("hasRole('ROLE_INSTALLATION')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response upgrade(@PathParam("installationId") final Long installationId, final UpgradeDto dto) {
		LOG.info("Registering upgrade for installation with id {}", installationId);

		final Installation installation = installationService.verifyAccess(installationId);

		final Upgrade upgrade = new Upgrade();
		upgrade.setVersion(dto.getVersion());
		upgrade.setFrontendCommit(dto.getFrontendCommit());
		upgrade.setBackendCommit(dto.getBackendCommit());
		upgrade.setMappingCommit(dto.getMappingCommit());
		upgrade.setExecutedOn(dto.getExecutedOn());
		installation.getUpgrades().add(upgrade);
		final Installation updatedInstallation = installationService.update(installation);

		return Response.ok(new InstallationDto(updatedInstallation)).build();
	}

	@POST
	@Path("/{installationId}/statistics/registrations")
	@PreAuthorize("hasRole('ROLE_INSTALLATION')")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response registrationStatistics(@PathParam("installationId") final Long installationId, final String json) {
		installationService.verifyAccess(installationId);
		registrationStatisticService.update(installationId, new JsonObjectsByOrganization(json));
		return Response.ok().build();
	}

	@POST
	@Path("/{installationId}/statistics/stabledata")
	@PreAuthorize("hasRole('ROLE_INSTALLATION')")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response stableDataStatistics(@PathParam("installationId") final Long installationId, final String json) {
		installationService.verifyAccess(installationId);
		stableDataStatisticService.update(installationId, new JsonObjectsByOrganization(json));
		return Response.ok().build();
	}

    @POST
    @Path("/{installationId}/users")
    @PreAuthorize("hasRole('ROLE_INSTALLATION')")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response users(@PathParam("installationId") final Long installationId, final String json) {
		installationService.verifyAccess(installationId);
        hd4dpUserService.update(installationId, new JsonObjectsByOrganization(json));
        return Response.ok().build();
    }

	@POST
	@Path("/{installationId}/userRequests")
	@PreAuthorize("hasRole('ROLE_INSTALLATION')")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response userRequests(@PathParam("installationId") final Long installationId, final String json) {
		installationService.verifyAccess(installationId);
		hd4dpUserRequestService.update(installationId, new JsonObjectsByOrganization(json));
		return Response.ok().build();
	}

	@POST
	@Path("/{installationId}/configurations")
	@PreAuthorize("hasRole('ROLE_INSTALLATION')")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response configurations(@PathParam("installationId") final Long installationId, final String json) {
		installationService.verifyAccess(installationId);
		hd4dpConfigurationService.update(installationId, new JsonObjectsByOrganization(json));
		return Response.ok().build();
	}

	@POST
	@Path("/{installationId}/statistics/messages")
	@PreAuthorize("hasRole('ROLE_INSTALLATION')")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response messageStatistics(@PathParam("installationId") final Long installationId, final String json) {
		installationService.verifyAccess(installationId);
		messageStatisticService.update(installationId, new JsonObjectsByOrganization(json));
		return Response.ok().build();
	}

	@POST
	@Path("/{installationId}/statuses")
	@PreAuthorize("hasRole('ROLE_INSTALLATION')")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response statuses(@PathParam("installationId") final Long installationId, final String json) {
		installationService.verifyAccess(installationId);
		hd4dpStatusService.update(installationId, JsonUtils.createJsonObject(json));
		return Response.ok().build();
	}

}
