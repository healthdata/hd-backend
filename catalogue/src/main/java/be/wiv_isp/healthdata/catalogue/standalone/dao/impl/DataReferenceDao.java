/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.dao.impl;

import be.wiv_isp.healthdata.catalogue.standalone.dao.IDataReferenceDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataReference;
import be.wiv_isp.healthdata.catalogue.standalone.enumeration.SearchFilterType;
import be.wiv_isp.healthdata.catalogue.standalone.search.DataReferenceSearch;
import be.wiv_isp.healthdata.common.dao.impl.CrudDao;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DataReferenceDao extends CrudDao<DataReference, Long> implements IDataReferenceDao {

	public DataReferenceDao() {
		super(DataReference.class);
	}

	@Override
	public List<DataReference> getAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DataReference> cq = cb.createQuery(DataReference.class);
		Root<DataReference> rootEntry = cq.from(DataReference.class);
		CriteriaQuery<DataReference> all = cq.select(rootEntry);
		TypedQuery<DataReference> allQuery = em.createQuery(all);
		return allQuery.getResultList();
	}

	@Override
	public List<DataReference> getAll(DataReferenceSearch search) {
		if (search == null) {
			return getAll();
		}
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DataReference> cq = cb.createQuery(DataReference.class);
		Root<DataReference> rootEntry = cq.from(DataReference.class);
		CriteriaQuery<DataReference> all = cq.select(rootEntry);

		List<Predicate> predicates = new ArrayList<>();
		if (search.getType() != null) {
			predicates.add(cb.equal(rootEntry.get("type"), search.getType()));
		}
		if (search.getCode() != null) {
			predicates.add(cb.equal(rootEntry.get("code"), search.getCode()));
		}
		if (search.getParentCode() != null) {
			if ("NULL".equals(search.getParentCode())) {
				predicates.add(cb.isNull(rootEntry.<String> get("parentCode")));
			} else {
				predicates.add(cb.equal(rootEntry.get("parentCode"), search.getParentCode()));
			}
		}
		if (search.getValue() != null) {
			String pattern;
			if (SearchFilterType.ALL.equals(search.getFilter())) {
				pattern = "%" + search.getValue().toLowerCase() + "%";
			} else if (SearchFilterType.END.equals(search.getFilter())) {
				pattern = "%" + search.getValue().toLowerCase();
			} else if (SearchFilterType.START.equals(search.getFilter())) {
				pattern = search.getValue().toLowerCase() + "%";
			} else {
				pattern = search.getValue().toLowerCase();
			}
			predicates.add(cb.like( //
					cb.lower(rootEntry.<String> get("value")), pattern) //
					);
		}
		if (search.getLanguage() != null) {
			predicates.add(cb.equal(rootEntry.get("language"), search.getLanguage()));
		}
		if (search.getValidOn() != null) {
			ParameterExpression<Timestamp> validOn = cb.parameter(Timestamp.class, "validOn");
			Path<Timestamp> from = rootEntry.get("validFrom");
			Predicate startPredicate = cb.or(cb.isNull(from), cb.lessThanOrEqualTo(from, validOn));
			Path<Timestamp> till = rootEntry.get("validTill");
			Predicate endPredicate = cb.or(cb.isNull(till), cb.greaterThanOrEqualTo(till, validOn));
			Predicate finalCondition = cb.and(startPredicate, endPredicate);
			predicates.add(finalCondition);
		}
		if (!predicates.isEmpty()) {
			cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		}
		cq.orderBy(cb.asc(rootEntry.get("value")));
		TypedQuery<DataReference> allQuery = em.createQuery(all);
		if (search.getLimit() != 0) {
			allQuery.setFirstResult(0);
			allQuery.setMaxResults(search.getLimit());
		}
		if (search.getValidOn() != null) {
			allQuery.setParameter("validOn", search.getValidOn());
		}
		return allQuery.getResultList();
	}
}
