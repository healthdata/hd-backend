/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.domain;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

@Entity
@Table(name = "AUDIT_LOGS")
public class Audit {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "AUDIT_LOG_ID")
	private Long id;

	@Column(name = "API", unique = false, nullable = false)
	private String apiType;

	@Column(name = "METHOD", unique = false, nullable = false)
	private String method;

	@Column(name = "USERNAME", unique = false, nullable = false)
	private String userName;

	@Column(name = "SUBJECT", unique = false, nullable = true)
	private String subject;

	@Column(name = "STATUS", unique = false, nullable = true)
	private String status;

	@Column(name = "TIME_IN", unique = false, nullable = true)
	private Timestamp timeIn;

	@Column(name = "TIME_OUT", unique = false, nullable = true)
	private Timestamp timeOut;

	@Column(name = "EXCEPTION_MESSAGE", unique = false, nullable = true)
	private String exceptionMessage;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getApi() {
		return apiType;
	}

	public void setApi(String apiType) {
		this.apiType = apiType;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getTimeIn() {
		return timeIn;
	}

	public void setTimeIn(Timestamp timeIn) {
		this.timeIn = timeIn;
	}

	public Timestamp getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(Timestamp timeOut) {
		this.timeOut = timeOut;
	}

	public String getExceptionMessage() {
		return exceptionMessage;
	}

	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	@PrePersist
	public void onCreate() {
		long currentTime = new Date().getTime();
		setTimeIn(new Timestamp(currentTime));
	}

	@PreUpdate
	public void onUpdate() {
		long currentTime = new Date().getTime();
		setTimeOut(new Timestamp(currentTime));
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof Audit) {
			Audit other = (Audit) o;

			return Objects.equals(id, other.id) //
					&& Objects.equals(apiType, other.apiType) //
					&& Objects.equals(method, other.method) //
					&& Objects.equals(userName, other.userName) //
					&& Objects.equals(subject, other.subject) //
					&& Objects.equals(status, other.status) //
					&& Objects.equals(timeIn, other.timeIn) //
					&& Objects.equals(timeOut, other.timeOut) //
					&& Objects.equals(exceptionMessage, other.exceptionMessage);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash( //
				this.id, //
				this.apiType, //
				this.method, //
				this.userName, //
				this.subject, //
				this.status, //
				this.timeIn, //
				this.timeOut, //
				this.exceptionMessage);
	}

	@Override
	public String toString() {
		return "Audit {" + //
				"id = " + Objects.toString(this.id) + ", " + //
				"api = " + Objects.toString(this.apiType) + ", " + //
				"method = " + Objects.toString(this.method) + ", " + //
				"userName = " + Objects.toString(this.userName) + ", " + //
				"subject = " + Objects.toString(this.subject) + ", " + //
				"status = " + Objects.toString(this.status) + ", " + //
				"timeIn = " + Objects.toString(timeIn) + ", " + //
				"timeOut = " + Objects.toString(timeOut) + ", " + //
				"exceptionMessage = " + Objects.toString(this.exceptionMessage) + "}";
	}
}
