/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.dao.impl;

import be.wiv_isp.healthdata.catalogue.standalone.dao.ITemplateDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Template;
import be.wiv_isp.healthdata.common.dao.impl.CrudDao;
import be.wiv_isp.healthdata.common.domain.enumeration.TemplateKey;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TemplateDao extends CrudDao<Template, Long> implements ITemplateDao {

	public TemplateDao() {
		super(Template.class);
	}

	@Override
	public Template get(TemplateKey templateKey) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Template> cq = cb.createQuery(Template.class);
		Root<Template> rootEntry = cq.from(Template.class);
		CriteriaQuery<Template> all = cq.select(rootEntry);

		List<Predicate> predicates = new ArrayList<>();
		predicates.add(cb.equal(cb.lower(rootEntry.<String> get("key")), templateKey.toString().toLowerCase()));
		if (!predicates.isEmpty()) {
			cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		}
		TypedQuery<Template> allQuery = em.createQuery(all);
		return allQuery.getSingleResult();
	}


	@Override
	public List<Template> getAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Template> cq = cb.createQuery(Template.class);
		Root<Template> rootEntry = cq.from(Template.class);
		CriteriaQuery<Template> all = cq.select(rootEntry);
		TypedQuery<Template> allQuery = em.createQuery(all);
		return allQuery.getResultList();
	}
}
