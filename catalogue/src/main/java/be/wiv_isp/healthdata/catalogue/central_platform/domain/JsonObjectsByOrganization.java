/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.domain;

import be.wiv_isp.healthdata.common.json.JsonUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonObjectsByOrganization {

    private static final Logger LOG = LoggerFactory.getLogger(JsonObjectsByOrganization.class);

    private Map<String, List<JSONObject>> jsonObjects = new HashMap<>();

    public JsonObjectsByOrganization(final String json) {
        LOG.debug("Parsing json content");
        final JSONArray array = JsonUtils.createJsonArray(json);

        for (int i = 0; i < array.length(); i++) {
            final JSONObject jsonObject = JsonUtils.getJSONObject(array, i);
            final String organizationIdValue = JsonUtils.getString(jsonObject, "organizationIdentificationValue");

            if (jsonObjects.get(organizationIdValue) == null) {
                jsonObjects.put(organizationIdValue, new ArrayList<JSONObject>());
            }

            jsonObjects.get(organizationIdValue).add(jsonObject);
        }
    }

    public List<JSONObject> getJsonObjects(final String organizationIdValue) {
        final List<JSONObject> jsonObject = jsonObjects.get(organizationIdValue);
        return jsonObject == null ? new ArrayList<JSONObject>() : jsonObject;
    }

}
