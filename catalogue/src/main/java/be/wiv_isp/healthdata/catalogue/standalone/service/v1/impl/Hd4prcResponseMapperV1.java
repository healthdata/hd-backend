/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.v1.impl;

import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionDefinition;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionGroup;
import be.wiv_isp.healthdata.catalogue.standalone.dto.DataCollection;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.util.*;

public class Hd4prcResponseMapperV1 {

	static public String convert(List<DataCollection> dataCollectionDefinitions, Map<String, Set<String>> taxonomies) throws JSONException {
		List<JSONObject> response = new ArrayList<>();
		for (DataCollection dataCollection : dataCollectionDefinitions) {
			DataCollectionGroup group = dataCollection.getGroup();
			DataCollectionDefinition definition = dataCollection.getDefinition();

			JSONObject dataCollectionDefinitionJSON = new JSONObject();
			JSONObject id = new JSONObject();
				id.put("name", definition.getDataCollectionName());
				StringBuilder version = new StringBuilder();
				version.append(group.getMajorVersion());
				version.append(".");
				version.append(definition.getMinorVersion());
				version.append(".");
				version.append(definition.geteFormsVersion());
			id.put("version", version);
			dataCollectionDefinitionJSON.put("id",id);

			JSONObject labels = new JSONObject();
				labels.put("en", definition.getDescription().getEn());
				labels.put("fr", definition.getDescription().getFr());
				labels.put("nl", definition.getDescription().getNl());
			dataCollectionDefinitionJSON.put("labels",labels);

			JSONObject descriptions = new JSONObject();
				if(definition.getDescriptionLong() != null) {
					descriptions.put("en", StringUtils.isNotBlank(definition.getDescriptionLong().getEn()) ? definition.getDescriptionLong().getEn() : definition.getDescription().getEn());
					descriptions.put("fr", StringUtils.isNotBlank(definition.getDescriptionLong().getFr()) ? definition.getDescriptionLong().getFr() : definition.getDescription().getFr());
					descriptions.put("nl", StringUtils.isNotBlank(definition.getDescriptionLong().getNl()) ? definition.getDescriptionLong().getNl() : definition.getDescription().getNl());
				} else {
					descriptions.put("en", definition.getDescription().getEn());
					descriptions.put("fr", definition.getDescription().getFr());
					descriptions.put("nl", definition.getDescription().getNl());
				}
			dataCollectionDefinitionJSON.put("descriptions",descriptions);

			JSONObject functionalConfiguration = new JSONObject();
				functionalConfiguration.put("documentType", "LETTER");
				functionalConfiguration.put("lifespan", "SEND_ONE_TIME");
				functionalConfiguration.put("editable", false);
				functionalConfiguration.put("containsRecipients", true);
				functionalConfiguration.put("canModifySender", false);
				functionalConfiguration.put("canModifyRecipients", false);
				functionalConfiguration.put("canModifyAttachments", false);
				functionalConfiguration.put("supportsDataSet", true);
				functionalConfiguration.put("supportsExport", false);
			dataCollectionDefinitionJSON.put("functionalConfiguration", functionalConfiguration);

			JSONObject fixedRecipient = new JSONObject();
				fixedRecipient.put("identifier", "1990002213");
				fixedRecipient.put("quality", "INSTITUTION_EHP");
				fixedRecipient.put("name", "Healthdata");
			dataCollectionDefinitionJSON.put("fixedRecipient", fixedRecipient);

			JSONObject exportConfiguration = new JSONObject();
				JSONArray documentFormats = new JSONArray();
				documentFormats.put("A4");
				exportConfiguration.put("documentFormats", documentFormats);
				exportConfiguration.put("defaultDocumentFormat", "A4");

				JSONArray formats = new JSONArray();
				formats.put("adr");
				formats.put("pdf");
				formats.put("json");
				exportConfiguration.put("formats", formats);
				exportConfiguration.put("dataSetFormat", "json");

			dataCollectionDefinitionJSON.put("exportConfiguration", exportConfiguration);

			dataCollectionDefinitionJSON.put("taxonomy", getTaxonomyString(taxonomies, group.getName()));
			response.add(dataCollectionDefinitionJSON);
		}
		return new JSONArray(response).toString();
	}

	private static String getTaxonomyString(Map<String, Set<String>> taxonomies, String groupName) {
		Set<String> taxonomy = taxonomies.get(groupName);
		Iterator<String> it = taxonomy.iterator();
		if (! it.hasNext())
			return "";

		StringBuilder sb = new StringBuilder();
		for (;;) {
			String fullName = it.next();
			sb.append(fullName);
			if (! it.hasNext())
				return sb.toString();
			sb.append(',');
		}
	}
}
