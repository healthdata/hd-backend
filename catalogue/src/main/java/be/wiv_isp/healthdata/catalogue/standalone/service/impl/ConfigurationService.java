/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.impl;

import java.nio.charset.Charset;

import be.wiv_isp.healthdata.common.caching.CacheManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import be.wiv_isp.healthdata.catalogue.standalone.dao.IConfigurationDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Configuration;
import be.wiv_isp.healthdata.catalogue.standalone.service.IConfigurationService;
import be.wiv_isp.healthdata.catalogue.standalone.service.IEncryptionService;

import com.sun.jersey.core.util.Base64;

@Component
public class ConfigurationService implements IConfigurationService {

	private static final Charset UTF_8_CHARSET = Charset.forName("UTF-8");

	@Autowired
	private CacheManager cacheManager;

	@Autowired
	private IConfigurationDao configurationDao;

	@Autowired
	private IEncryptionService encryptionService;

	@Override
	public Configuration get(Long id) {
		Configuration configuration = configurationDao.get(id);

		if (configuration != null && !configuration.isReadable()) {
			String base64EncryptedValue = configuration.getValue();
			byte[] encryptedValue = Base64.decode(base64EncryptedValue);
			byte[] value = encryptionService.decrypt(encryptedValue);

			configuration.setValue(new String(value, UTF_8_CHARSET));

			this.detach(configuration);
		}

		return configuration;
	}

	@Override
	@Cacheable(value = CacheManagementService.configurationCache)
	public Configuration get(String key) {
		Configuration configuration = configurationDao.get(key);

		if (configuration != null && !configuration.isReadable()) {
			String base64EncryptedValue = configuration.getValue();
			byte[] encryptedValue = Base64.decode(base64EncryptedValue);
			byte[] value = encryptionService.decrypt(encryptedValue);

			configuration.setValue(new String(value, UTF_8_CHARSET));

			this.detach(configuration);
		}

		return configuration;
	}

	@Override
	@CacheEvict(value = CacheManagementService.configurationCache, allEntries = true)
	public Configuration update(Configuration configuration) {
		if (!configuration.isReadable()) {
			byte[] value = configuration.getValue().getBytes(UTF_8_CHARSET);
			byte[] encryptedValue = encryptionService.encrypt(value);
			String base64EncryptedValue = new String(Base64.encode(encryptedValue), UTF_8_CHARSET);

			configuration.setValue(base64EncryptedValue);

			final Configuration updatedEntity = configurationDao.update(configuration);

			this.detach(updatedEntity);

			return updatedEntity;
		}
		return configurationDao.update(configuration);
	}

	@Override
	public void delete(Configuration configuration) {
		configurationDao.delete(configuration);
	}

	@Override
	public void detach(Configuration configuration) {
		configurationDao.detach(configuration);
	}
}
