/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.mapper;

import be.wiv_isp.healthdata.catalogue.standalone.api.ApiVersion;
import be.wiv_isp.healthdata.common.api.ClientVersion;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;

public class DataCollectionVersionMapper {

	public static final String getLatestApiVersion() {
		return ApiVersion.DataCollection.V2;
	}

	public static final String getApiVersion(String clientVersion) {
		if (ClientVersion.DataCollection._20151103.equals(clientVersion)) {
			return ApiVersion.DataCollection.V2;
		}
		if (ClientVersion.DataCollection._20170301.equalsIgnoreCase(clientVersion)) {
			return ApiVersion.DataCollection.V3;
		}
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.INVALID_CLIENT_VERSION, clientVersion);
		throw exception;
	}
}
