/*
 *  Copyright 2014-2017 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package be.wiv_isp.healthdata.catalogue.standalone.dao.impl;

import be.wiv_isp.healthdata.catalogue.standalone.dao.IDataCollectionGroupDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionGroup;
import be.wiv_isp.healthdata.catalogue.standalone.search.DataCollectionGroupSearch;
import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DataCollectionGroupDao extends CrudDaoV2<DataCollectionGroup, Long, DataCollectionGroupSearch> implements IDataCollectionGroupDao {

	public DataCollectionGroupDao() {
		super(DataCollectionGroup.class);
	}


	@Override
	protected List<Predicate> getPredicates(DataCollectionGroupSearch search, CriteriaBuilder cb, Root<DataCollectionGroup> rootEntry) {
		final List<Predicate> predicates = new ArrayList<>();

		if (search.getId() != null) {
			predicates.add(cb.equal(rootEntry.<Long>get("id"), search.getId()));
		}
		if (search.getName() != null) {
			predicates.add(cb.equal(cb.upper(rootEntry.<String> get("name")), search.getName().toUpperCase()));
		}
		if (search.getMajorVersion() != null) {
			predicates.add(cb.equal(rootEntry.get("majorVersion"), search.getMajorVersion()));
		}
		if (search.getValidForCreation() != null) {
			final Predicate p1 = cb.lessThanOrEqualTo(rootEntry.<Timestamp> get("startDate"), search.getValidForCreation());
			final Predicate p2 = cb.greaterThanOrEqualTo(rootEntry.<Timestamp> get("endDateCreation"), search.getValidForCreation());
			Predicate or = cb.or(p2, cb.isNull(rootEntry.<Timestamp> get("endDateCreation")));

			predicates.add(cb.and(p1, or));
		}
		if (search.getValidForSubmission() != null) {
			final Predicate p1 = cb.lessThanOrEqualTo(rootEntry.<Timestamp> get("startDate"), search.getValidForSubmission());
			final Predicate p2 = cb.greaterThanOrEqualTo(rootEntry.<Timestamp> get("endDateSubmission"), search.getValidForSubmission());
			Predicate or = cb.or(p2, cb.isNull(rootEntry.<Timestamp> get("endDateSubmission")));

			predicates.add(cb.and(p1, or));
		}
		if (search.getValidForComments() != null) {
			final Predicate p1 = cb.lessThanOrEqualTo(rootEntry.<Timestamp> get("startDate"), search.getValidForComments());
			final Predicate p2 = cb.greaterThanOrEqualTo(rootEntry.<Timestamp> get("endDateComments"), search.getValidForComments());
			Predicate or = cb.or(p2, cb.isNull(rootEntry.<Timestamp> get("endDateComments")));

			predicates.add(cb.and(p1, or));
		}
		return predicates;
	}

	@Override
	public DataCollectionGroup latest(DataCollectionGroupSearch search) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DataCollectionGroup> cQuery = cb.createQuery(DataCollectionGroup.class);
		Root<DataCollectionGroup> dcg = cQuery.from(DataCollectionGroup.class);

		final List<Predicate> predicates = new ArrayList<>();

		if (search.getName() != null) {
			predicates.add(cb.equal(cb.upper(dcg.<String> get("name")), search.getName().toUpperCase()));
		}

		if (!predicates.isEmpty()) {
			cQuery.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		}

		TypedQuery<DataCollectionGroup> allQuery = em.createQuery(cQuery);
		List<DataCollectionGroup> resultList = allQuery.getResultList();

		if (resultList.isEmpty()) {
			return null;
		} else {
			DataCollectionGroup latestDataCollectionGroup = null;
			for (DataCollectionGroup dataCollectionGroup : resultList) {
				if (latestDataCollectionGroup == null || latestDataCollectionGroup.getMajorVersion() < dataCollectionGroup.getMajorVersion()) {
					latestDataCollectionGroup = dataCollectionGroup;
				}
			}
			return latestDataCollectionGroup;
		}
	}
}
