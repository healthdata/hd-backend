/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.service.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IRegistrationStatisticDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.JsonObjectsByOrganization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Organization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.RegistrationStatistic;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.GroupByRegistrationStatistic;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.RegistrationStatisticSearch;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IOrganizationService;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IRegistrationStatisticService;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionDefinition;
import be.wiv_isp.healthdata.catalogue.standalone.service.IDataCollectionDefinitionService;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class RegistrationStatisticService extends AbstractService<RegistrationStatistic, RegistrationStatistic.RegistrationStatisticId, RegistrationStatisticSearch, IRegistrationStatisticDao> implements IRegistrationStatisticService {

    private static final Logger LOG = LoggerFactory.getLogger(RegistrationStatisticService.class);

    @Autowired
    private IRegistrationStatisticDao registrationStatisticDao;

    @Autowired
    private IOrganizationService organizationService;

    @Autowired
    private IDataCollectionDefinitionService dataCollectionDefinitionService;

    public RegistrationStatisticService() {
        super(RegistrationStatistic.class);
    }

    @Override
    protected IRegistrationStatisticDao getDao() {
        return registrationStatisticDao;
    }

    @Override
    public List<GroupByRegistrationStatistic> getAllGroupBy() {
        return getDao().getAllGroupBy();
    }

    @Override
    @Transactional
    public void update(final Long installationId, final JsonObjectsByOrganization data) {
        LOG.info("Updating registration statistics for installation {}", installationId);

        final List<Organization> organizations = organizationService.getAllByInstallationId(installationId);

        for (final Organization organization : organizations) {
            udpateOrganization(organization, data);
        }

        LOG.info("Registration statistics successfully updated for installation {}", installationId);
    }

    private void udpateOrganization(final Organization organization, final JsonObjectsByOrganization statistics) {
        LOG.debug("Updating registration statistics for organization {} (id value = {})", organization.getId(), organization.getIdentificationValue());

        organization.getRegistrationStatistics().clear();
        final List<JSONObject> jsonObjects = statistics.getJsonObjects(organization.getIdentificationValue());
        for (final JSONObject jsonObject : jsonObjects) {
            final RegistrationStatistic registrationStatistic = unmarshal(jsonObject);

            if (registrationStatistic.getDataCollectionDefinition() == null) {
                final Long dataCollectionDefinitionId = JsonUtils.getLong(jsonObject, "dataCollectionDefinitionId");
                LOG.warn("data collection definition with id {} not found. Ignoring item.", dataCollectionDefinitionId);
                continue;
            }

            registrationStatistic.setOrganization(organization);
            organization.getRegistrationStatistics().add(registrationStatistic);
        }
        organizationService.update(organization);
        LOG.debug("Registration statistics successfully updated for organization {} (id value = {})", organization.getId(), organization.getIdentificationValue());
    }

    private RegistrationStatistic unmarshal(final JSONObject json) {
        final Long dataCollectionDefinitionId = JsonUtils.getLong(json, "dataCollectionDefinitionId");
        final DataCollectionDefinition dataCollectionDefinition = dataCollectionDefinitionService.get(dataCollectionDefinitionId);

        final RegistrationStatistic registrationStatistic = new RegistrationStatistic();
        registrationStatistic.setDataCollectionDefinition(dataCollectionDefinition);
        registrationStatistic.setStatus(JsonUtils.getString(json, "status"));
        registrationStatistic.setCount(JsonUtils.getLong(json, "count"));
        return registrationStatistic;
    }
}
