/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.domain;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
@Table(name = "DATA_REFERENCES", indexes = { //
@Index(name = "data_reference_type", columnList = "TYPE", unique = false),//
		@Index(name = "data_reference_code", columnList = "CODE", unique = false),//
		@Index(name = "data_reference_parentcode", columnList = "PARENT_CODE", unique = false),//
		@Index(name = "data_reference_value", columnList = "VALUE", unique = false),//
		@Index(name = "data_reference_language", columnList = "LANGUAGE", unique = false),//
		@Index(name = "data_reference_validfrom", columnList = "VALID_FROM", unique = false),//
		@Index(name = "data_reference_validtill", columnList = "VALID_TILL", unique = false) })
public class DataReference {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "DATA_REFERENCE_ID")
	private Long id;

	@Column(name = "TYPE", nullable = false)
	private String type;

	@Column(name = "CODE", nullable = false)
	private String code;

	@Column(name = "PARENT_CODE", nullable = true)
	private String parentCode;

	@Column(name = "VALUE", nullable = false)
	private String value;

	@Column(name = "LANGUAGE", nullable = false)
	private String language;

	@Column(name = "VALID_FROM", nullable = true)
	private Timestamp validFrom;

	@Column(name = "VALID_TILL", nullable = true)
	private Timestamp validTill;

	@JsonIgnore
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	@JsonIgnore
	public Timestamp getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Timestamp validFrom) {
		this.validFrom = validFrom;
	}

	@JsonIgnore
	public Timestamp getValidTill() {
		return validTill;
	}

	public void setValidTill(Timestamp validTill) {
		this.validTill = validTill;
	}

	@Override
	public String toString() {
		return "DataCollectionDefinition {" + //
				"id = " + Objects.toString(this.id) + ", " + //
				"code = " + Objects.toString(this.code) + ", " + //
				"value = " + Objects.toString(this.value) + ", " + //
				"language = " + Objects.toString(this.language) + ", " + //
				"validFrom = " + Objects.toString(this.validFrom) + ", " + //
				"validTill = " + Objects.toString(this.validTill) + "}";
	}
}
