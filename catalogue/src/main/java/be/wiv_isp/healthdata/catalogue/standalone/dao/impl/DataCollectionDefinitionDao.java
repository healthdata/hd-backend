/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.dao.impl;

import be.wiv_isp.healthdata.catalogue.standalone.dao.IDataCollectionDefinitionDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionDefinition;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionGroup;
import be.wiv_isp.healthdata.catalogue.standalone.search.DataCollectionDefinitionSearch;
import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DataCollectionDefinitionDao extends CrudDaoV2<DataCollectionDefinition, Long, DataCollectionDefinitionSearch> implements IDataCollectionDefinitionDao {

	public DataCollectionDefinitionDao() {
		super(DataCollectionDefinition.class);
	}

	@Override
	protected List<Predicate> getPredicates(DataCollectionDefinitionSearch search, CriteriaBuilder cb, Root<DataCollectionDefinition> rootEntry) {
		final List<Predicate> predicates = new ArrayList<>();
		if (search.getId() != null) {
			predicates.add(cb.equal(rootEntry.<Long>get("id"), search.getId()));
		}

		if (search.getDataCollectionName() != null) {
			predicates.add(cb.equal(cb.upper(rootEntry.<String> get("dataCollectionName")), search.getDataCollectionName().toUpperCase()));
		}
		if (search.getDataCollectionGroupId() != null) {
			predicates.add(cb.equal(rootEntry.<DataCollectionGroup>get("dataCollectionGroup").get("id"), search.getDataCollectionGroupId()));
		}
		if (search.getMinorVersion() != null) {
			predicates.add(cb.equal(rootEntry.get("minorVersion"), search.getMinorVersion()));
		}
		return predicates;
	}

	@Override
	public DataCollectionDefinition latest(final DataCollectionDefinitionSearch search) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DataCollectionDefinition> cQuery = cb.createQuery(DataCollectionDefinition.class);
		Root<DataCollectionDefinition> dcd = cQuery.from(DataCollectionDefinition.class);

		final List<Predicate> predicates = new ArrayList<>();

		if (search.getDataCollectionName() != null) {
			predicates.add(cb.equal(cb.upper(dcd.<String> get("dataCollectionName")), search.getDataCollectionName().toUpperCase()));
		}

		if (!predicates.isEmpty()) {
			cQuery.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		}

		TypedQuery<DataCollectionDefinition> allQuery = em.createQuery(cQuery);
		List<DataCollectionDefinition> resultList = allQuery.getResultList();

		if (resultList.isEmpty()) {
			return null;
		} else {
			DataCollectionDefinition latestDataCollectionDefinition = null;
			for (DataCollectionDefinition dataCollectionDefinition : resultList) {
				if (latestDataCollectionDefinition == null || latestDataCollectionDefinition.getMinorVersion() < dataCollectionDefinition.getMinorVersion()) {
					latestDataCollectionDefinition = dataCollectionDefinition;
				}
			}
			return latestDataCollectionDefinition;
		}
	}
}
