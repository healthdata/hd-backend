/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.v1.impl;

import be.wiv_isp.healthdata.catalogue.standalone.domain.Translation;
import be.wiv_isp.healthdata.catalogue.standalone.enumeration.Language;
import be.wiv_isp.healthdata.catalogue.standalone.service.ITranslationService;
import be.wiv_isp.healthdata.catalogue.standalone.service.v1.ITranslationServiceV1;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.codehaus.jettison.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Arrays;
import java.util.List;

@Service
public class TranslationServiceV1 implements ITranslationServiceV1 {

	@Autowired
	private ITranslationService translationService;

	@Override
	public Response getAll(UriInfo info) {
		MultivaluedMap<String, String> queryParameters = info.getQueryParameters();
		Language language = null;
		for (String queryParameterkey : queryParameters.keySet()) {
			if ("language".equalsIgnoreCase(queryParameterkey)) {
				try {
					language = Language.valueOf(queryParameters.getFirst(queryParameterkey).toUpperCase());
				} catch (IllegalArgumentException e) {
					HealthDataException exception = new HealthDataException(e);
					exception.setExceptionType(ExceptionType.INVALID_FORMAT, queryParameters.getFirst(queryParameterkey), Arrays.toString(Language.values()));
					throw exception;
				}
			}
		}
		if (language == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_FORMAT, "null", Arrays.toString(Language.values()));
			throw exception;
		}

		List<Translation> tanslations = translationService.getAll();

		TranslationResponseMapperV1 mapper = new TranslationResponseMapperV1();
		return Response //
				.ok(mapper.convert(tanslations, language)) //
				.build();
	}

	@Override
	public Response update(UriInfo info, JSONArray json) throws Exception {
		TranslationRequestMapperV1 mapper = new TranslationRequestMapperV1();
		translationService.update(mapper.convert(json));
		return Response.noContent().build();
	}

	@Override
	public Response delete(UriInfo info, JSONArray json) throws Exception {
		TranslationRequestMapperV1 mapper = new TranslationRequestMapperV1();
		translationService.delete(mapper.convert(json));
		return Response.noContent().build();
	}
}
