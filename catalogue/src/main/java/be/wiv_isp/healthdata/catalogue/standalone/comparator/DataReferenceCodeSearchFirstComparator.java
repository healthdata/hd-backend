/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.comparator;

import java.util.Comparator;

import org.apache.commons.lang3.StringUtils;

import be.wiv_isp.healthdata.catalogue.standalone.domain.DataReference;

public class DataReferenceCodeSearchFirstComparator implements Comparator<DataReference> {

	private final String searchString;

	public DataReferenceCodeSearchFirstComparator(String searchString) {
		this.searchString = searchString;
	}

	@Override
	public int compare(DataReference arg0, DataReference arg1) {
		Comparator<String> numericStringcomparator = new NumericStringComparator();
		if (StringUtils.startsWithIgnoreCase(arg0.getCode(), searchString)) {
			if (StringUtils.startsWithIgnoreCase(arg1.getCode(), searchString)) {
				return numericStringcomparator.compare(arg0.getCode(), arg1.getCode());
			} else {
				return -1;
			}
		} else {
			if (StringUtils.startsWithIgnoreCase(arg1.getCode(), searchString)) {
				return 1;
			} else {
				return numericStringcomparator.compare(arg0.getCode(), arg1.getCode());
			}
		}
	}
}
