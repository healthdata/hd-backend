/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.central_platform.api;

import be.wiv_isp.healthdata.catalogue.central_platform.domain.RegistryDependentIdGenerationInput;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IRegistryDependentIdService;
import be.wiv_isp.healthdata.catalogue.standalone.service.ISalesForceService;
import be.wiv_isp.healthdata.common.api.exception.BadRequestException;
import be.wiv_isp.healthdata.common.domain.RegistryDependentIdGenerationDefinition;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.text.MessageFormat;
import java.util.Date;

@Component
@Path("/centralplatform/datacollections")
public class CpDataCollectionRestService {

	@Autowired
	private IRegistryDependentIdService registryDependentIdService;

	@Autowired
	private ISalesForceService salesForceService;

	@GET
	@Path("/{dataCollectionName}/registryDependentId/{phase}/generate")
	@Produces(MediaType.TEXT_PLAIN)
	@PreAuthorize("hasRole('ROLE_INSTALLATION')")
	public Response generateRegistryDependentId(@Context UriInfo info) {
		final String dataCollectionName = getDataCollectionName(info);
		final RegistryDependentIdGenerationDefinition.Phase phase = getPhase(info);

		final RegistryDependentIdGenerationInput extraParameters = new RegistryDependentIdGenerationInput();
		extraParameters.setSubmissionDate(getSubmissionDate(info));
		extraParameters.setOrganizationIdentificationValue(getOrganizationIdentificationValue(info));

		final String registryDependentId = registryDependentIdService.getRegistryDependentId(dataCollectionName, phase, extraParameters);

		if(registryDependentId == null) {
			return Response.noContent().build();
		}

		return Response.ok(registryDependentId).build();
	}

	@GET
	@Path("/{dataCollectionName}/registryDependentId/definition")
	@Produces(MediaType.APPLICATION_JSON)
	@PreAuthorize("hasRole('ROLE_INSTALLATION')")
	public Response getRegistryDependentIdGenerationDefinition(@Context UriInfo info) {
		final String dataCollectionName = getDataCollectionName(info);
		return Response.ok(salesForceService.getRegistryDependentIdGenerationDefinition(dataCollectionName)).build();
	}

	private String getDataCollectionName(UriInfo info) {
		return RestUtils.getPathString(info, "dataCollectionName");
	}

	private RegistryDependentIdGenerationDefinition.Phase getPhase(UriInfo info) {
		final String phaseString = RestUtils.getPathString(info, "phase");
		try {
			return RegistryDependentIdGenerationDefinition.Phase.match(phaseString);
		} catch (IllegalArgumentException e) {
			throw new BadRequestException(MessageFormat.format("Invalid phase parameter. Possible values are {0}", RegistryDependentIdGenerationDefinition.Phase.values()));
		}
	}

	private Date getSubmissionDate(UriInfo info) {
		final String submissionDateString = RestUtils.getParameterString(info, "submissionDate");
		if(submissionDateString == null) {
			return null;
		}
		return DateFormat.DATE_AND_TIME.parse(submissionDateString);
	}

	private String getOrganizationIdentificationValue(UriInfo info) {
		return RestUtils.getParameterString(info, "organizationIdentificationValue");
	}

}
