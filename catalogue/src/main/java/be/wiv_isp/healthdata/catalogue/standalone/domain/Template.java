/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.domain;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "TEMPLATES")
public class Template {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "TEMPLATE_ID")
    private Long id;

    @Column(name = "LOOKUP_KEY", unique = true, nullable = false)
    private String key;

    @Column(name = "SUBJECT", nullable = false)
    private String subject;

    @Lob
    @Column(name = "TEXT", nullable = false)
    private byte[] templateText;

    @Column(name = "UPDATED_ON")
    private Timestamp updatedOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public byte[] getTemplateText() {
        return templateText;
    }

    public void setTemplateText(byte[] templateText) {
        this.templateText = templateText;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @PreUpdate
    public void onUpdate() {
        setUpdatedOn(new Timestamp(new Date().getTime()));
    }


    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof Template) {
            Template other = (Template) o;

            return Objects.equals(key, other.key)
                    && Objects.equals(subject, other.subject)
                    && Objects.equals(templateText, other.templateText)
                    && Objects.equals(updatedOn, other.updatedOn);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                this.key,
                this.subject,
                this.templateText,
                this.updatedOn);
    }

    @Override
    public String toString() {
        return "Template {" +
                "key = " + Objects.toString(this.key) + ", " +
                "subject = " + Objects.toString(this.subject) + ", " +
                "templateText = " + Objects.toString(this.templateText) + ", " +
                "updatedOn = " + Objects.toString(this.updatedOn) + "}";
    }
}
