/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.dto.converters;

import be.wiv_isp.healthdata.catalogue.standalone.domain.FollowUpDefinition;
import be.wiv_isp.healthdata.common.dto.FollowUpDefinitionDto;

import java.util.ArrayList;
import java.util.List;

public class FollowUpDefinitionDtoConverter {

    public static FollowUpDefinitionDto convert(FollowUpDefinition followUpDefinition) {
        final FollowUpDefinitionDto followUpDefinitionDto = new FollowUpDefinitionDto();
        followUpDefinitionDto.setId(followUpDefinition.getId());
        followUpDefinitionDto.setLabel(followUpDefinition.getLabel());
        followUpDefinitionDto.setName(followUpDefinition.getName());
        followUpDefinitionDto.setDescription(followUpDefinition.getDescription());
        followUpDefinitionDto.setTiming(followUpDefinition.getTiming());
        followUpDefinitionDto.setBaseline(followUpDefinition.getBaseline());
        followUpDefinitionDto.setConditions(followUpDefinition.getConditions());
        return followUpDefinitionDto;
    }

    public static List<FollowUpDefinitionDto> convert(List<FollowUpDefinition> followUpDefinitions) {
        final List<FollowUpDefinitionDto> dtos = new ArrayList<>();
        if(followUpDefinitions != null) {
            for (FollowUpDefinition followUpDefinition : followUpDefinitions) {
                dtos.add(convert(followUpDefinition));
            }
        }
        return dtos;
    }
}
