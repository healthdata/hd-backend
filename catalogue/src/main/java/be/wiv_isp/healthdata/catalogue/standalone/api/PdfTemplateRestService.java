/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.api;

import be.wiv_isp.healthdata.catalogue.standalone.service.v1.IPdfTemplateServiceV1;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import com.sun.jersey.multipart.MultiPart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Component
@Path("/pdf/templates")
public class PdfTemplateRestService {

	@Autowired
	private IPdfTemplateServiceV1 v1;

	@GET
	@Path("/{dataCollectionDefinitionId}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response get(@HeaderParam("Accept-Language") String languageHeader, @HeaderParam("ClientVersion") String clientVersion, @Context UriInfo info) {
		String apiVersion = ApiVersion.get(clientVersion, info, ApiType.PDF_TEMPLATES);
		if (ApiVersion.PdfTemplate.V1.equals(apiVersion)) {
			return v1.get(info, languageHeader);
		}
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.INVALID_CLIENT_VERSION, clientVersion);
		throw exception;
	}


	@POST
	@Path("/{dataCollectionDefinitionId}")
	@Consumes("multipart/mixed")
	public Response create(@HeaderParam("ClientVersion") String clientVersion, @Context UriInfo info, MultiPart multiPart) throws Exception {
		String apiVersion = ApiVersion.get(clientVersion, info, ApiType.PDF_TEMPLATES);
		if (ApiVersion.PdfTemplate.V1.equals(apiVersion)) {
			return v1.create(info, multiPart);
		}
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.INVALID_CLIENT_VERSION, clientVersion);
		throw exception;
	}

	@PUT
	@Path("/{dataCollectionDefinitionId}")
	@Consumes("multipart/mixed")
	public Response update(@HeaderParam("ClientVersion") String clientVersion, @Context UriInfo info, MultiPart multiPart) throws Exception {
		String apiVersion = ApiVersion.get(clientVersion, info, ApiType.PDF_TEMPLATES);
		if (ApiVersion.PdfTemplate.V1.equals(apiVersion)) {
			return v1.update(info, multiPart);
		}
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.INVALID_CLIENT_VERSION, clientVersion);
		throw exception;
	}

	@DELETE
	@Path("/{dataCollectionDefinitionId}")
	public Response delete(@HeaderParam("ClientVersion") String clientVersion, @Context UriInfo info) throws Exception {
		String apiVersion = ApiVersion.get(clientVersion, info, ApiType.PDF_TEMPLATES);
		if (ApiVersion.PdfTemplate.V1.equals(apiVersion)) {
			return v1.delete(info);
		}
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.INVALID_CLIENT_VERSION, clientVersion);
		throw exception;
	}
}
