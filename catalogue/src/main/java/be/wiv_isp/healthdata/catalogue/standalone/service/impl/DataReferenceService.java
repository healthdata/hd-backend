/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.impl;

import be.wiv_isp.healthdata.catalogue.standalone.comparator.DataReferenceCodeSearchFirstComparator;
import be.wiv_isp.healthdata.catalogue.standalone.comparator.DataReferenceValueSearchFirstComparator;
import be.wiv_isp.healthdata.catalogue.standalone.comparator.NumericStringComparator;
import be.wiv_isp.healthdata.catalogue.standalone.dao.IDataReferenceDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataReference;
import be.wiv_isp.healthdata.catalogue.standalone.dto.FullDataReferenceDto;
import be.wiv_isp.healthdata.catalogue.standalone.enumeration.SearchFilterType;
import be.wiv_isp.healthdata.catalogue.standalone.enumeration.SortingType;
import be.wiv_isp.healthdata.catalogue.standalone.search.DataReferenceSearch;
import be.wiv_isp.healthdata.catalogue.standalone.search.builder.DataReferenceSearchBuilder;
import be.wiv_isp.healthdata.catalogue.standalone.service.IDataReferenceService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class DataReferenceService implements IDataReferenceService {

	@Autowired
	IDataReferenceDao dataReferenceDao;

	@Override
	public void setDataReferenceDao(IDataReferenceDao dataReferenceDao) {
		this.dataReferenceDao = dataReferenceDao;
	}

	@Override
	public DataReference create(DataReference dataReference) {
		try {
			return dataReferenceDao.create(dataReference);
		} catch (Exception e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.CREATE_GENERAL);
			throw exception;
		}
	}

	@Override
	public List<DataReference> find(DataReferenceSearch search) {
		List<DataReference> result = dataReferenceDao.getAll(search);
		if (!StringUtils.isBlank(search.getValue())) {
			if (SortingType.SEARCH_FIRST.equals(search.getSorting())) {
				DataReferenceValueSearchFirstComparator comparator = new DataReferenceValueSearchFirstComparator(search.getValue());
				Collections.sort(result, comparator);
			} else {
				BeanComparator<DataReference> comparator = new BeanComparator<>("value", new NumericStringComparator());
				Collections.sort(result, comparator);
			}
		} else if (!StringUtils.isBlank(search.getCode())) {
			if (SortingType.SEARCH_FIRST.equals(search.getSorting())) {
				DataReferenceCodeSearchFirstComparator comparator = new DataReferenceCodeSearchFirstComparator(search.getCode());
				Collections.sort(result, comparator);
			} else {
				BeanComparator<DataReference> comparator = new BeanComparator<>("code", new NumericStringComparator());
				Collections.sort(result, comparator);
			}
		} else {
			BeanComparator<DataReference> comparator = new BeanComparator<>("value", new NumericStringComparator());
			Collections.sort(result, comparator);
		}
		return result;
	}

	@Override
	public FullDataReferenceDto getFullDataReferenceDto(DataReference dataReference, Timestamp validOn) {
		FullDataReferenceDto dto = new FullDataReferenceDto(dataReference);

		List<DataReference> childer = getChilder(dataReference, validOn);
		List<String> childrenCodes = new ArrayList<>();
		for (DataReference child : childer) {
			childrenCodes.add(child.getCode());
		}
		dto.setChildren(childrenCodes);
		List<String> parentCodes = new ArrayList<>();
		DataReference parent = getParent(dataReference, validOn);
		while (parent != null) {
			parentCodes.add(parent.getCode());
			parent = getParent(parent, validOn);
		}
		Collections.reverse(parentCodes);
		dto.setParents(parentCodes);
		return dto;
	}

	@Override
	public DataReference getParent(DataReference dataReference, Timestamp validOn) {
		if (dataReference.getParentCode() == null) {
			return null;
		}
		DataReferenceSearch search = new DataReferenceSearchBuilder() //
				.withCode(dataReference.getParentCode()) //
				.withType(dataReference.getType()) //
				.withFilter(SearchFilterType.EXACT) //
				.withLanguage(dataReference.getLanguage()) //
				.withValidOn(validOn) //
				.build();
		List<DataReference> parents = dataReferenceDao.getAll(search);
		if (parents.size() != 1) {
			return null;
		}
		return parents.get(0);
	}

	@Override
	public List<DataReference> getChilder(DataReference dataReference, Timestamp validOn) {
		DataReferenceSearch search = new DataReferenceSearchBuilder() //
				.withParentCode(dataReference.getCode()) //
				.withType(dataReference.getType()) //
				.withFilter(SearchFilterType.EXACT) //
				.withLanguage(dataReference.getLanguage()) //
				.withValidOn(validOn) //
				.build();
		return dataReferenceDao.getAll(search);
	}

	@Override
	public DataReference update(DataReference dataReference) {
		return dataReferenceDao.update(dataReference);
	}

	@Override
	public void delete(DataReference dataReference) {
		dataReferenceDao.delete(dataReference);
	}
}
