/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service;

import be.wiv_isp.healthdata.common.domain.RegistryDependentIdGenerationDefinition;
import be.wiv_isp.healthdata.common.dto.SalesForceAccountDto;
import be.wiv_isp.healthdata.common.dto.SalesForceOrganizationDto;

import java.util.Map;
import java.util.Set;

public interface ISalesForceService {

	void createDataCollection(String dataCollectionName);

	Set<String> getDataCollections(String identificationType, String identificationValue);

	Map<String, Set<String>> getDataCollectionsGrouped(String identificationType, String identificationValue);

	Set<String> getDataCollections(boolean publicOnly);

	Map<String, Set<String>> getDataCollectionsGrouped(boolean publicOnly);

	Set<String> getStableData();

	Set<String> getHd4prcDataCollections();

	Set<String> getTaxonomy(String dataCollectionName);

	Set<SalesForceOrganizationDto> getLinkedOrganizations(String identificationType, String identificationValue);

	SalesForceOrganizationDto getName(String identificationType, String identificationValue);

	void createOrganizations(String parentIdentificationType, String parentIdentificationValue, SalesForceAccountDto accountDto);

	Set<SalesForceOrganizationDto> getOrganizations(String dataCollectionName);

	Set<SalesForceOrganizationDto> getAllOrganizations();

	boolean isHd4prcOrganization(String identificationType, String identificationValue);

	String getRegistryCode(String dataCollectionName);

	RegistryDependentIdGenerationDefinition getRegistryDependentIdGenerationDefinition(String dataCollectionName);

	boolean status();

}
