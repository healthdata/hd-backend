/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.central_platform.domain;


import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "HD4DP_USER_REQUESTS")
@IdClass(Hd4dpUserRequest.Hd4dpUserRequestId.class)
public class Hd4dpUserRequest {

    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "ORGANIZATION_ID")
    private Organization organization;

    @Id
    @Column(name = "HD4DP_ID")
    private Long hd4dpId;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "FIRST_NAME")
    protected String firstName;

    @Column(name = "LAST_NAME")
    protected String lastName;

    @Column(name = "EMAIL")
    protected String email;

    @Column(name = "EMAIL_REQUESTER")
    protected String emailRequester;

    @Column(name = "APPROVED")
    protected Boolean approved;

    @Fetch(FetchMode.JOIN)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "HD4DP_USER_REQUEST_AUTHORITIES", joinColumns = { @JoinColumn(name = "ORGANIZATION_ID", referencedColumnName = "ORGANIZATION_ID"), @JoinColumn(name = "HD4DP_ID", referencedColumnName = "HD4DP_ID") })
    @Column(name = "AUTHORITY")
    private Set<String> authorities;

    @Fetch(FetchMode.JOIN)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "HD4DP_USER_REQUEST_DATA_COLLECTIONS", joinColumns = { @JoinColumn(name = "ORGANIZATION_ID", referencedColumnName = "ORGANIZATION_ID"), @JoinColumn(name = "HD4DP_ID", referencedColumnName = "HD4DP_ID") })
    @Column(name = "DATA_COLLECTION_NAME")
    private Set<String> dataCollectionNames;

    @Column(name = "UPDATED_ON", nullable = false)
    protected Timestamp updatedOn;

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Long getHd4dpId() {
        return hd4dpId;
    }

    public void setHd4dpId(Long hd4dpId) {
        this.hd4dpId = hd4dpId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailRequester() {
        return emailRequester;
    }

    public void setEmailRequester(String emailRequester) {
        this.emailRequester = emailRequester;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public Set<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<String> authorities) {
        this.authorities = authorities;
    }

    public Set<String> getDataCollectionNames() {
        return dataCollectionNames;
    }

    public void setDataCollectionNames(Set<String> dataCollectionNames) {
        this.dataCollectionNames = dataCollectionNames;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @PreUpdate
    @PrePersist
    public void onUpdate() {
        setUpdatedOn(new Timestamp(new Date().getTime()));
    }

    public static class Hd4dpUserRequestId implements Serializable {

        private Organization organization;
        private Long hd4dpId;

        public Hd4dpUserRequestId() {

        }

        public Hd4dpUserRequestId(Organization organization, Long hd4dpId) {
            this.organization = organization;
            this.hd4dpId = hd4dpId;
        }

        public Organization getOrganization() {
            return organization;
        }

        public void setOrganization(Organization organization) {
            this.organization = organization;
        }

        public Long getHd4dpId() {
            return hd4dpId;
        }

        public void setHd4dpId(Long hd4dpId) {
            this.hd4dpId = hd4dpId;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Hd4dpUserRequest)) return false;

        Hd4dpUserRequest that = (Hd4dpUserRequest) o;

        if (organization != null ? !organization.equals(that.organization) : that.organization != null) return false;
        if (hd4dpId != null ? !hd4dpId.equals(that.hd4dpId) : that.hd4dpId != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (emailRequester != null ? !emailRequester.equals(that.emailRequester) : that.emailRequester != null)
            return false;
        if (approved != null ? !approved.equals(that.approved) : that.approved != null) return false;
        if (authorities != null ? !authorities.equals(that.authorities) : that.authorities != null) return false;
        if (dataCollectionNames != null ? !dataCollectionNames.equals(that.dataCollectionNames) : that.dataCollectionNames != null)
            return false;
        return !(updatedOn != null ? !updatedOn.equals(that.updatedOn) : that.updatedOn != null);

    }

    @Override
    public int hashCode() {
        int result = organization != null ? organization.hashCode() : 0;
        result = 31 * result + (hd4dpId != null ? hd4dpId.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (emailRequester != null ? emailRequester.hashCode() : 0);
        result = 31 * result + (approved != null ? approved.hashCode() : 0);
        result = 31 * result + (authorities != null ? authorities.hashCode() : 0);
        result = 31 * result + (dataCollectionNames != null ? dataCollectionNames.hashCode() : 0);
        result = 31 * result + (updatedOn != null ? updatedOn.hashCode() : 0);
        return result;
    }
}
