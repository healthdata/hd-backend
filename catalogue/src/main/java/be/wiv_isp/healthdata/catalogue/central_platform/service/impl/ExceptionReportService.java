/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.catalogue.central_platform.service.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IExceptionReportDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.ExceptionReport;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.ExceptionReportSearch;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IExceptionReportService;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IOrganizationService;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExceptionReportService extends AbstractService<ExceptionReport, Long, ExceptionReportSearch, IExceptionReportDao> implements IExceptionReportService {

    private static final Logger LOG = LoggerFactory.getLogger(ExceptionReportService.class);

    @Autowired
    private IExceptionReportDao dao;

    @Autowired
    private IOrganizationService organizationService;

    public ExceptionReportService() {
        super(ExceptionReport.class);
    }

    @Override
    protected IExceptionReportDao getDao() {
        return dao;
    }
}
