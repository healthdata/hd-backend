/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.service.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IUniqueIdDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.UniqueId;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.UniqueIdSearch;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IUniqueIdService;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

@Service
public class UniqueIdService extends AbstractService<UniqueId, UniqueId.CompositeId, UniqueIdSearch, IUniqueIdDao> implements IUniqueIdService {

    private static final Logger LOG = LoggerFactory.getLogger(UniqueIdService.class);

    @Autowired
    private IUniqueIdDao dao;

    @Autowired
    @Qualifier("transactionManager")
    protected PlatformTransactionManager txManager;

    public UniqueIdService() {
        super(UniqueId.class);
    }

    @Override
    protected IUniqueIdDao getDao() {
        return dao;
    }

    @Override
    public synchronized UniqueId getNext(final String key) {
        LOG.debug("Retrieving next unique id for key [{}]", key);

        final TransactionTemplate transactionTemplate = new TransactionTemplate(txManager);
        transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);

        /* Important to use a transaction template and not the @Transactional annotation. */
        return transactionTemplate.execute(new TransactionCallback<UniqueId>() {
            @Override
            public UniqueId doInTransaction(TransactionStatus status) {
                final UniqueId last = dao.getLast(key);

                final UniqueId uniqueId = new UniqueId();
                uniqueId.setKey(key);
                uniqueId.setCounter(last == null ? 1L : last.getCounter() + 1L);
                final UniqueId created = create(uniqueId);
                LOG.debug("Next unique id for key [{}]: {}", key, created.getCounter());
                return created;
            }
        });
    }
}
