/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.service;

import be.wiv_isp.healthdata.catalogue.central_platform.domain.JsonObjectsByOrganization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.RegistrationStatistic;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.GroupByRegistrationStatistic;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.RegistrationStatisticSearch;
import be.wiv_isp.healthdata.common.service.IAbstractService;

import java.util.List;

public interface IRegistrationStatisticService extends IAbstractService<RegistrationStatistic, RegistrationStatistic.RegistrationStatisticId, RegistrationStatisticSearch> {

    List<GroupByRegistrationStatistic> getAllGroupBy();

    void update(Long installationId, JsonObjectsByOrganization statistics);

}
