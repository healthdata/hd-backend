/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.service.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IHd4dpUserDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Hd4dpUser;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.JsonObjectsByOrganization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Organization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.Hd4dpUserSearch;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IHd4dpUserService;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IOrganizationService;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class Hd4dpUserService extends AbstractService<Hd4dpUser, Hd4dpUser.Hd4dpUserId, Hd4dpUserSearch, IHd4dpUserDao> implements IHd4dpUserService {

    private static final Logger LOG = LoggerFactory.getLogger(Hd4dpUserService.class);

    @Autowired
    private IHd4dpUserDao dao;

    @Autowired
    private IOrganizationService organizationService;

    public Hd4dpUserService() {
        super(Hd4dpUser.class);
    }

    @Override
    protected IHd4dpUserDao getDao() {
        return dao;
    }

    @Override
    @Transactional
    public void update(Long installationId, JsonObjectsByOrganization data) {
        LOG.info("Updating users for installation {}", installationId);

        final List<Organization> organizations = organizationService.getAllByInstallationId(installationId);

        for (final Organization organization : organizations) {
            updateOrganization(organization, data);
        }

        LOG.info("Users successfully updated for installation {}", installationId);
    }

    private void updateOrganization(final Organization organization, final JsonObjectsByOrganization hd4dpUsers) {
        LOG.debug("Updating users for organization {} (id value = {})", organization.getId(), organization.getIdentificationValue());

        organization.getUsers().clear();
        final List<JSONObject> jsonObjects = hd4dpUsers.getJsonObjects(organization.getIdentificationValue());
        for (final JSONObject json : jsonObjects) {
            final Hd4dpUser hd4dpUser = unmarshal(json);
            hd4dpUser.setOrganization(organization);
            organization.getUsers().add(hd4dpUser);
        }
        organizationService.update(organization);
        LOG.debug("Users successfully updated for organization {} (id value = {})", organization.getId(), organization.getIdentificationValue());
    }

    private Hd4dpUser unmarshal(final JSONObject json) {
        final Hd4dpUser user = new Hd4dpUser();
        user.setUsername(JsonUtils.getString(json, "username"));
        user.setFirstName(JsonUtils.getString(json, "firstName"));
        user.setLastName(JsonUtils.getString(json, "lastName"));
        user.setEmail(JsonUtils.getString(json, "email"));
        user.setAuthorities(JsonUtils.getStringSet(json, "authorities"));
        user.setDataCollectionNames(JsonUtils.getStringSet(json, "dataCollectionNames"));
        user.setEnabled(JsonUtils.getBoolean(json, "enabled"));
        user.setLdapUser(JsonUtils.getBoolean(json, "ldapUser"));
        return user;
    }
}
