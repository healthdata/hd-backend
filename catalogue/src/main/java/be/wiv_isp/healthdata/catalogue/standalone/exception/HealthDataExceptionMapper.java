/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.catalogue.standalone.exception;

import be.wiv_isp.healthdata.common.exception.ExceptionResponse;
import be.wiv_isp.healthdata.common.exception.ExceptionUtils;
import be.wiv_isp.healthdata.common.exception.HealthDataException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class HealthDataExceptionMapper implements ExceptionMapper<HealthDataException> {

	@Override
	public Response toResponse(HealthDataException ex) {
		String uuid = ExceptionUtils.log(ex);

		if(ex.getResponseStatus() == null) {
			ex.setResponseStatus(Response.Status.INTERNAL_SERVER_ERROR);
		}
		ExceptionResponse response = new ExceptionResponse();
		response.setUuid(uuid);
		response.setError(ex.getErrorCode());
		response.setError_description(ex.getResponseMessage());

		return Response.status(ex.getResponseStatus())
				.entity(response)
				.type(MediaType.APPLICATION_JSON)
				.build();
	}
}
