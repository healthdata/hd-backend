/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.impl;

import be.wiv_isp.healthdata.common.domain.RegistryDependentIdGenerationDefinition;
import be.wiv_isp.healthdata.catalogue.standalone.dao.ISalesForceTestAccountDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Configuration;
import be.wiv_isp.healthdata.catalogue.standalone.domain.SalesForceTestAccount;
import be.wiv_isp.healthdata.catalogue.standalone.service.IConfigurationService;
import be.wiv_isp.healthdata.catalogue.standalone.service.ISalesForceService;
import be.wiv_isp.healthdata.catalogue.standalone.service.IWebServiceClientService;
import be.wiv_isp.healthdata.common.caching.CacheManagementService;
import be.wiv_isp.healthdata.common.caching.ICacheManagementService;
import be.wiv_isp.healthdata.common.dto.SalesForceAccountDto;
import be.wiv_isp.healthdata.common.dto.SalesForceDataCollection;
import be.wiv_isp.healthdata.common.dto.SalesForceOrganizationDto;
import be.wiv_isp.healthdata.common.dto.SalesForceParticipation;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import com.sun.jersey.api.client.GenericType;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.MediaType;
import java.util.*;

@Component
public class SalesForceService implements ISalesForceService {

	private static final Logger LOG = LoggerFactory.getLogger(SalesForceService.class);

	private String salesForceLoginUrl;
	private String clientId;
	private String clientSecret;
	private String username;
	private String password;
	private String salesForceEndPointUrl;
	private boolean isTest;
	private boolean logSalesForceCalls;
	
	private static final String SALESFORCE_QUERY_PATH = "/services/data/v31.0/query";


	@Autowired
	private IWebServiceClientService webServiceClientService;
	@Autowired
	private IConfigurationService configurationService;
	@Autowired
	private ICacheManagementService cacheManagementService;
	@Autowired
	private ISalesForceTestAccountDao salesForceTestAccountDao;

	private String getAccessToken() throws JSONException {
		WebServiceBuilder builder = new WebServiceBuilder();
		builder.setLogResponseBody(false);
		builder.setUrl(salesForceLoginUrl + "/services/oauth2/token");
		builder.addParameter("grant_type", "password");
		builder.addParameter("client_id", clientId, true);
		builder.addParameter("client_secret", clientSecret, true);
		builder.addParameter("username", username, true);
		builder.addParameter("password", password, true);
		builder.setPost(true);
		builder.setReturnType(new GenericType<JSONObject>() {
		});

		JSONObject security = (JSONObject) webServiceClientService.callWebService(builder);

		salesForceEndPointUrl = security.getString("instance_url");

		return security.getString("access_token");
	}

	private String getOrganizationId(String identificationType, String identificationValue, String accessToken) throws JSONException {
		final String query = "SELECT Id FROM Account WHERE HealthData_ID_Value__c = '" + identificationValue + "' AND HealthData_ID_Type__c = '" + identificationType + "' AND IsDeleted = false";
		final JSONArray records = getQueryResult(accessToken, query);

		if (records.length() == 0) {
			return null;
		} else {
			return records.getJSONObject(0).getString("Id");
		}
	}

	@Override
	public void createDataCollection(String dataCollectionName) {
		try {
			if(StringUtils.isBlank(dataCollectionName) || dataCollectionName.contains(" ")) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.SALESFORCE_CREATE, "register");
				throw exception;
			}
			loadConfiguration();
			String accessToken = getAccessToken();

			SalesForceDataCollection dataCollection = new SalesForceDataCollection();
			dataCollection.setName(dataCollectionName);

			WebServiceBuilder builder = new WebServiceBuilder();
			builder.setLogResponseBody(logSalesForceCalls);
			builder.setUrl(salesForceEndPointUrl + "/services/data/v31.0/sobjects/HealthData_Register__c");
			builder.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
			builder.setAccept(MediaType.APPLICATION_JSON_TYPE);
			builder.setType(MediaType.APPLICATION_JSON_TYPE);
			builder.setJson(dataCollection);
			builder.setReturnType(new GenericType<JSONObject>() {
			});
			builder.setPost(true);

			JSONObject wsbResponse = (JSONObject) webServiceClientService.callWebService(builder);
			Boolean success = wsbResponse.getBoolean("success");
			if(!success) {
				LOG.error(wsbResponse.getJSONArray("errors").toString());
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.SALESFORCE_CREATE, "register");
				throw exception;

			}
			String salesForceDataCollectionId = wsbResponse.getString("id");
			createParticipation(salesForceDataCollectionId);

			cacheManagementService.evict(CacheManagementService.salesForceCache);
		} catch (Exception e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.SALESFORCE_CREATE, "register");
			throw exception;

		}
	}

	private void createParticipation(String salesForceDataCollectionId) {
		try {
			if(StringUtils.isBlank(salesForceDataCollectionId)) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.SALESFORCE_CREATE, "participation");
				throw exception;
			}
			loadConfiguration();
			String accessToken = getAccessToken();

			List<SalesForceTestAccount> testAccounts = salesForceTestAccountDao.getAll();
			for (SalesForceTestAccount testAccount : testAccounts) {
				SalesForceParticipation participation = new SalesForceParticipation();
				participation.setOrganisatie__c(testAccount.getSalesForceId());
				participation.setHealthData_Register__c(salesForceDataCollectionId);

				WebServiceBuilder builder = new WebServiceBuilder();
				builder.setLogResponseBody(logSalesForceCalls);
				builder.setUrl(salesForceEndPointUrl + "/services/data/v31.0/sobjects/HealthData_Register_Participation__c");
				builder.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
				builder.setAccept(MediaType.APPLICATION_JSON_TYPE);
				builder.setType(MediaType.APPLICATION_JSON_TYPE);
				builder.setJson(participation);
				builder.setReturnType(new GenericType<JSONObject>() {
				});
				builder.setPost(true);

				JSONObject wsbResponse = (JSONObject) webServiceClientService.callWebService(builder);
				Boolean success = wsbResponse.getBoolean("success");
				if(!success) {
					LOG.error(wsbResponse.getJSONArray("errors").toString());
					HealthDataException exception = new HealthDataException();
					exception.setExceptionType(ExceptionType.SALESFORCE_CREATE, "participation");
					throw exception;
				}
			}
			cacheManagementService.evict(CacheManagementService.salesForceCache);
		} catch (Exception e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.SALESFORCE_CREATE, "participants");
			throw exception;
		}
	}

	@Override
	@Cacheable(value = CacheManagementService.salesForceCache)
	public Set<String> getDataCollections(String identificationType, String identificationValue) {
		Map<String, Set<String>> registries = getDataCollectionsGrouped(identificationType, identificationValue);
		Set<String> dataCollections = new HashSet<>();
		for (Set<String> dataCollectionSet : registries.values())
			dataCollections.addAll(dataCollectionSet);

		return dataCollections;
	}

	@Override
	@Cacheable(value = CacheManagementService.salesForceCache)
	public Map<String, Set<String>> getDataCollectionsGrouped(String identificationType, String identificationValue) {
		try {
			loadConfiguration();
			String accessToken = getAccessToken();
			String organizationId = getOrganizationId(identificationType, identificationValue, accessToken);
			if (organizationId == null)
				return Collections.emptyMap();

			StringBuilder query = new StringBuilder();
			query.append("SELECT HealthData_Register_Naam__c, Healthdata_Register_Master__c FROM HealthData_Register_Participation__c WHERE ");
			String where = "(Organisatie__c = '" + organizationId + "' OR PrimaryOrganization__c = '" + organizationId + "') AND IsDeleted = false AND Actief_als_data_provider__c = true AND Do_not_show_in_HD4DP__c = false";
			query.append("(").append(where).append(")");
			final JSONArray records = getQueryResult(accessToken, query.toString());

			// TODO: make more efficient by caching allRegistries
			final Map<String, Set<String>> allRegistries = getDataCollectionsGrouped(false);
			Map<String, Set<String>> registries = new HashMap<>();
			for (int i = 0; i < records.length(); i++) {
				final String master = records.getJSONObject(i).getString("Healthdata_Register_Master__c");
				if (master == null || master.equalsIgnoreCase("null")) {
					final String name = records.getJSONObject(i).getString("HealthData_Register_Naam__c");
					if (allRegistries.keySet().contains(name))
						registries.put(name, new HashSet<>(allRegistries.get(name)));
					else
						registries.put(name, new HashSet<>(Collections.singletonList(name)));
				} else {
					registries.put(master, new HashSet<String>());
				}
			}

			for (int i = 0; i < records.length(); i++) {
				final String name = records.getJSONObject(i).getString("HealthData_Register_Naam__c");
				if (registries.keySet().contains(name))
					continue;
				String masterName = records.getJSONObject(i).getString("Healthdata_Register_Master__c");
				if (masterName == null || masterName.equalsIgnoreCase("null"))
					registries.put(name, new HashSet<>(Collections.singletonList(name)));
				else
					registries.get(masterName).add(name);
			}

			return registries;
		} catch (Exception e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.SALESFORCE_GET, "register participants");
			throw exception;
		}
	}

	@Override
	@Cacheable(value = CacheManagementService.salesForceCache)
	public Set<String> getDataCollections(boolean publicOnly) {
		final Map<String, Set<String>> registries = getDataCollectionsGrouped(publicOnly);
		Set<String> dataCollections = new HashSet<>();
		for (Set<String> dataCollectionSet : registries.values())
			dataCollections.addAll(dataCollectionSet);

		return dataCollections;
	}

	@Override
	@Cacheable(value = CacheManagementService.salesForceCache)
	public Map<String, Set<String>> getDataCollectionsGrouped(boolean publicOnly) {
		try {
			loadConfiguration();
			String accessToken = getAccessToken();
			return getRegistryNamesGrouped(accessToken, publicOnly);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.SALESFORCE_GET, "register participants");
			throw exception;
		}
	}

	@Override
	@Cacheable(value = CacheManagementService.salesForceCache)
	public Set<String> getStableData() {
		try {
			loadConfiguration();
			String accessToken = getAccessToken();

			final String query = "SELECT Name FROM HealthData_Register__c WHERE IsDeleted = false and StableData__c = true AND Do_not_show_in_HD4DP__c = false";
			final JSONArray records = getQueryResult(accessToken, query);


			Set<String> registries = new TreeSet<>();
			for (int i = 0; i < records.length(); i++) {
				registries.add(records.getJSONObject(i).getString("Name"));
			}
			return registries;
		} catch (Exception e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.SALESFORCE_GET, "participants");
			throw exception;
		}
	}

	@Override
	@Cacheable(value = CacheManagementService.salesForceCache)
	public Set<String> getHd4prcDataCollections() {
		try {
			loadConfiguration();
			String accessToken = getAccessToken();

			final String query = "SELECT Name FROM HealthData_Register__c WHERE IsDeleted = false and HD4PRC__c = true";
			final JSONArray records = getQueryResult(accessToken, query);

			Set<String> registries = new TreeSet<>();
			for (int i = 0; i < records.length(); i++) {
				registries.add(records.getJSONObject(i).getString("Name"));
			}
			return registries;
		} catch (Exception e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.SALESFORCE_GET, "participants");
			throw exception;
		}
	}

	@Override
	@Cacheable(value = CacheManagementService.salesForceCache)
	public Set<String> getTaxonomy(String groupName) {
		try {
			loadConfiguration();
			String accessToken = getAccessToken();

			StringBuilder query = new StringBuilder();
			query.append("SELECT Taxonomy__c FROM HealthData_Register__c WHERE Name = '");
			query.append(groupName);
			query.append("' AND IsDeleted = false");
			final JSONArray records = getQueryResult(accessToken, query.toString());


			Set<String> taxonomy = new TreeSet<>();
			for (int i = 0; i < records.length(); i++) {
				String taxonomy__c = records.getJSONObject(i).getString("Taxonomy__c");
				if (StringUtils.isNotBlank(taxonomy__c) && !"null".equalsIgnoreCase(taxonomy__c)) {
					taxonomy.addAll(Arrays.asList(taxonomy__c.split(";")));
				}
			}
			return taxonomy;
		} catch (Exception e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.SALESFORCE_GET, "taxonomy");
			throw exception;
		}
	}

	@Override
	@Cacheable(value = CacheManagementService.salesForceCache)
	public Set<SalesForceOrganizationDto> getLinkedOrganizations(String identificationType, String identificationValue) {
		try {
			loadConfiguration();
			String accessToken = getAccessToken();
			String organizationId = getOrganizationId(identificationType, identificationValue, accessToken);
			if (organizationId == null) {
				return Collections.emptySet();
			}
			final String query = "SELECT Name, HealthData_ID_Type__c, HealthData_ID_Value__c, HD4PrC_Installation__c FROM Account WHERE MultiCenterHost__c = '" + organizationId + "' AND IsDeleted = false";
			final JSONArray records = getQueryResult(accessToken, query);

			Set<SalesForceOrganizationDto> salesForceOrganizations = new HashSet<>();
			for (int i = 0; i < records.length(); i++) {
				JSONObject registerParticipant = records.getJSONObject(i);
				SalesForceOrganizationDto salesForceOrganization = new SalesForceOrganizationDto();
				salesForceOrganization.setName(registerParticipant.getString("Name"));
				salesForceOrganization.setIdentificationType(registerParticipant.getString("HealthData_ID_Type__c"));
				salesForceOrganization.setIdentificationValue(registerParticipant.getString("HealthData_ID_Value__c"));
				salesForceOrganization.setHd4prc(registerParticipant.getBoolean("HD4PrC_Installation__c"));
				if (isValidIdentification(salesForceOrganization.getIdentificationType(), salesForceOrganization.getIdentificationValue())) {
					salesForceOrganizations.add(salesForceOrganization);
				}
				else {
					LOG.warn("Ignoring organization with invalid identification: [{}]", salesForceOrganization);
				}
			}
			return salesForceOrganizations;
		} catch (Exception e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.SALESFORCE_GET, "linked organisations");
			throw exception;
		}
	}

	@Override
	@Cacheable(value = CacheManagementService.salesForceCache)
	public SalesForceOrganizationDto getName(String identificationType, String identificationValue) {
		try {
			loadConfiguration();
			String accessToken = getAccessToken();
			final String query = "SELECT Name, HealthData_ID_Type__c, HealthData_ID_Value__c, HD4PrC_Installation__c FROM Account WHERE HealthData_ID_Value__c = '" + identificationValue + "' AND HealthData_ID_Type__c = '" + identificationType
					+ "' AND IsDeleted = false";
			final JSONArray records = getQueryResult(accessToken, query);

			if (records.length() < 1) {
				return null;
			} else {
				JSONObject registerParticipant = records.getJSONObject(0);
				SalesForceOrganizationDto salesForceOrganization = new SalesForceOrganizationDto();
				salesForceOrganization.setName(registerParticipant.getString("Name"));
				salesForceOrganization.setIdentificationType(registerParticipant.getString("HealthData_ID_Type__c"));
				salesForceOrganization.setIdentificationValue(registerParticipant.getString("HealthData_ID_Value__c"));
				salesForceOrganization.setHd4prc(registerParticipant.getBoolean("HD4PrC_Installation__c"));
				return salesForceOrganization;

			}
		} catch (Exception e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.SALESFORCE_GET, "linked organisations");
			throw exception;
		}
	}

	@Override
	@Cacheable(value = CacheManagementService.salesForceCache)
	public boolean isHd4prcOrganization(String identificationType, String identificationValue) {
		try {
			loadConfiguration();
			String accessToken = getAccessToken();
			final String query = "SELECT HD4PrC_Installation__c FROM Account WHERE HealthData_ID_Value__c = '" + identificationValue + "' AND HealthData_ID_Type__c = '" + identificationType
					+ "' AND IsDeleted = false";
			final JSONArray records = getQueryResult(accessToken, query);

			if (records.length() < 1) {
				return false;
			} else {
				JSONObject registerParticipant = records.getJSONObject(0);
				return registerParticipant.getBoolean("HD4PrC_Installation__c");
			}
		} catch (Exception e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.SALESFORCE_GET, "linked organisations");
			throw exception;
		}
	}
	@Override
	@Cacheable(value = CacheManagementService.salesForceCache)
	public String getRegistryCode(String dataCollectionName) {
		try {
			loadConfiguration();
			String accessToken = getAccessToken();
			final String query = "SELECT Code__c FROM HealthData_Register__c WHERE Name = '" + dataCollectionName + "'";
			final JSONArray records = getQueryResult(accessToken, query);

			if (records.length() < 1) {
				return null;
			} else {
				final JSONObject data = records.getJSONObject(0);
				return data.getString("Code__c");
			}
		} catch (Exception e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.SALESFORCE_GET, "registry code");
			throw exception;
		}
	}

	@Override
	@Cacheable(value = CacheManagementService.salesForceCache)
	public RegistryDependentIdGenerationDefinition getRegistryDependentIdGenerationDefinition(String dataCollectionName) {
		try {
			loadConfiguration();
			String accessToken = getAccessToken();
			final String query = "SELECT Registration_ID_generation_enabled__c, Registration_ID_generation_type__c, Registration_ID_generation_phase__c FROM HealthData_Register__c WHERE Name = '" + dataCollectionName + "'";
			final JSONArray records = getQueryResult(accessToken, query);

			if (records.length() < 1) {
				return null;
			} else {
				final JSONObject data = records.getJSONObject(0);

				final RegistryDependentIdGenerationDefinition def = new RegistryDependentIdGenerationDefinition();
				def.setGenerationEnabled(data.getBoolean("Registration_ID_generation_enabled__c"));
				if(def.isGenerationEnabled()) {
					final String typeString = data.getString("Registration_ID_generation_type__c");
					def.setType(RegistryDependentIdGenerationDefinition.Type.match(typeString));

					final String phaseString = data.getString("Registration_ID_generation_phase__c");
					def.setPhase(RegistryDependentIdGenerationDefinition.Phase.match(phaseString));
				}
				return def;
			}
		} catch (Exception e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.SALESFORCE_GET, "registration id generation definition");
			throw exception;
		}
	}

	@Override
	public void createOrganizations(String parentIdentificationType, String parentIdentificationValue, SalesForceAccountDto organization) {
		try {
			loadConfiguration();
			String accessToken = getAccessToken();

			if(!StringUtils.isAnyBlank(parentIdentificationType, parentIdentificationValue)) {
				String parentId = getOrganizationId(parentIdentificationType, parentIdentificationValue, accessToken);
				organization.setParentId(parentId);
				organization.setMultiCenterHost__c(parentId);
			}
			organization.setTest__c(isTest);

			WebServiceBuilder builder = new WebServiceBuilder();
			builder.setLogResponseBody(logSalesForceCalls);
			builder.setUrl(salesForceEndPointUrl + "/services/data/v31.0/sobjects/Account");
			builder.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
			builder.setAccept(MediaType.APPLICATION_JSON_TYPE);
			builder.setType(MediaType.APPLICATION_JSON_TYPE);
			builder.setJson(organization);
			builder.setReturnType(new GenericType<JSONObject>() {
			});
			builder.setPost(true);

			JSONObject wsbResponse = (JSONObject) webServiceClientService.callWebService(builder);
			Boolean success = wsbResponse.getBoolean("success");
			if(!success) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.SALESFORCE_CREATE, "organisation");
				throw exception;
			}
			cacheManagementService.evict(CacheManagementService.salesForceCache);
		} catch (Exception e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.SALESFORCE_CREATE, "organisation");
			throw exception;
		}

	}

	@Override
	@Cacheable(value = CacheManagementService.salesForceCache)
	public Set<SalesForceOrganizationDto> getOrganizations(String dataCollectionName) {
		LOG.info("Retrieving organizations that are linked to the [{}] data collection", dataCollectionName);

		if (StringUtils.isBlank(dataCollectionName))
			return getAllOrganizations();

		try {
			loadConfiguration();
			String accessToken = getAccessToken();
			final String query = "SELECT Organisatie_Naam__c, Organisatie_Type__c, Organisatie_Value__c FROM HealthData_Register_Participation__c WHERE HealthData_Register_Naam__c = '" + dataCollectionName + "' AND IsDeleted = false AND Actief_als_data_provider__c = true AND Do_not_show_in_HD4DP__c = false AND Organisatie__r.Test__c = " + isTest;
			final JSONArray records = getQueryResult(accessToken, query);

			Set<SalesForceOrganizationDto> salesForceOrganizations = new HashSet<>();
			for (int i = 0; i < records.length(); i++) {
				JSONObject registerParticipant = records.getJSONObject(i);
				SalesForceOrganizationDto salesForceOrganization = new SalesForceOrganizationDto();
				salesForceOrganization.setName(registerParticipant.getString("Organisatie_Naam__c"));
				salesForceOrganization.setIdentificationType(registerParticipant.getString("Organisatie_Type__c"));
				salesForceOrganization.setIdentificationValue(registerParticipant.getString("Organisatie_Value__c"));
				if (isValidIdentification(salesForceOrganization.getIdentificationType(), salesForceOrganization.getIdentificationValue())) {
					salesForceOrganizations.add(salesForceOrganization);
				}
				else {
					LOG.warn("Ignoring organization with invalid identification: [{}]", salesForceOrganization);
				}
			}
			LOG.info("{} organizations found", salesForceOrganizations.size());
			return salesForceOrganizations;
		} catch (Exception e) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.SALESFORCE_GET, "participating organisations for the " + dataCollectionName + " data collection");
			throw exception;
		}
	}

	@Override
	@Cacheable(value = CacheManagementService.salesForceCache)
	public Set<SalesForceOrganizationDto> getAllOrganizations() {
		try {
			loadConfiguration();
			String accessToken = getAccessToken();
			final String query = "SELECT Organisatie_Naam__c, Organisatie_Type__c, Organisatie_Value__c FROM HealthData_Register_Participation__c WHERE IsDeleted = false AND Actief_als_data_provider__c = true AND Do_not_show_in_HD4DP__c = false AND Organisatie__r.Test__c = " + isTest;
			final JSONArray records = getQueryResult(accessToken, query);

			Set<SalesForceOrganizationDto> salesForceOrganizations = new HashSet<>();
			for (int i = 0; i < records.length(); i++) {
				JSONObject registerParticipant = records.getJSONObject(i);
				SalesForceOrganizationDto salesForceOrganization = new SalesForceOrganizationDto();
				salesForceOrganization.setName(registerParticipant.getString("Organisatie_Naam__c"));
				salesForceOrganization.setIdentificationType(registerParticipant.getString("Organisatie_Type__c"));
				salesForceOrganization.setIdentificationValue(registerParticipant.getString("Organisatie_Value__c"));
				if (isValidIdentification(salesForceOrganization.getIdentificationType(), salesForceOrganization.getIdentificationValue())) {
					salesForceOrganizations.add(salesForceOrganization);
				}
				else {
					LOG.warn("Ignoring organization with invalid identification: [{}]", salesForceOrganization);
				}
			}
			LOG.info("{} organizations found", salesForceOrganizations.size());
			return salesForceOrganizations;
		} catch (Exception e) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.SALESFORCE_GET, "participating organisations");
			throw exception;
		}
	}

	private Map<String, Set<String>> getRegistryNamesGrouped(String accessToken, boolean publicOnly) throws JSONException {
		StringBuilder query = new StringBuilder();
		StringBuilder where1 = new StringBuilder();
		StringBuilder where2 = new StringBuilder();
		query.append("SELECT Name, Master_HealthData_Register__r.Name FROM HealthData_Register__c WHERE ");
		where1.append("IsDeleted = false AND Do_not_show_in_HD4DP__c = false");
		where2.append("Master_HealthData_Register__r.IsDeleted = false AND Master_HealthData_Register__r.Do_not_show_in_HD4DP__c = false");
		if (publicOnly) {
			where1.append(" and Public__c = true");
			where2.append(" and Master_HealthData_Register__r.Public__c = true");
		}
		query.append("(").append(where1).append(") OR (").append(where2).append(")");

		final JSONArray records = getQueryResult(accessToken, query.toString());

		Set<String> groups = new TreeSet<>();
		for (int i = 0; i < records.length(); i++) {
			if (records.getJSONObject(i).get("Master_HealthData_Register__r") instanceof JSONObject) // because null object is stored as the String "null"
				groups.add(records.getJSONObject(i).getJSONObject("Master_HealthData_Register__r").getString("Name"));
		}

		Map<String, Set<String>> registryNames = new HashMap<>();
		for (String group : groups) {
			registryNames.put(group, new HashSet<String>());
		}

		for (int i = 0; i < records.length(); i++) {
			final String name = records.getJSONObject(i).getString("Name");
			if (groups.contains(name))
				continue;
			if (records.getJSONObject(i).get("Master_HealthData_Register__r") instanceof JSONObject) {
				String masterName = records.getJSONObject(i).getJSONObject("Master_HealthData_Register__r").getString("Name");
				registryNames.get(masterName).add(name);
			}
			else {
				registryNames.put(name, new HashSet<>(Collections.singletonList(name)));
			}

		}
		return registryNames;
	}

	private void loadConfiguration() {
		salesForceLoginUrl = configurationService.get(Configuration.SalesForce.SALESFORCE_URL).getValue();
		clientId = configurationService.get(Configuration.SalesForce.SALESFORCE_CLIENT_ID).getValue();
		clientSecret = configurationService.get(Configuration.SalesForce.SALESFORCE_CLIENT_SECRET).getValue();
		username = configurationService.get(Configuration.SalesForce.SALESFORCE_USERNAME).getValue();
		password = configurationService.get(Configuration.SalesForce.SALESFORCE_PASSWORD).getValue();
		try {
			isTest = Boolean.valueOf(configurationService.get(Configuration.SalesForce.SALESFORCE_IS_TEST).getValue());
		} catch (Exception e) {
			LOG.warn("Could not retrieve environment information from the configuration. Setting [isTest] value to [false].", e);
			isTest = false;
		}
		try {
			logSalesForceCalls = Boolean.valueOf(configurationService.get(Configuration.SalesForce.LOG_SALESFORCE_CALLS).getValue());
		} catch (Exception e) {
			LOG.warn("Could not retrieve environment information from the configuration. Setting [logSalesForceCalls] value to [false].", e);
			logSalesForceCalls = false;
		}
	}

	private JSONArray getQueryResult(String accessToken, String query) throws JSONException {
		WebServiceBuilder builder = new WebServiceBuilder();
		builder.setLogResponseBody(logSalesForceCalls);
		builder.setUrl(salesForceEndPointUrl + SALESFORCE_QUERY_PATH);
		builder.addParameter("q", query);
		builder.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
		builder.setGet(true);
		builder.setReturnType(new GenericType<JSONObject>() {
		});

		JSONObject result = (JSONObject) webServiceClientService.callWebService(builder);
		return result.getJSONArray("records");
	}

	@Override
	public boolean status() {
		try {
			loadConfiguration();
			String accessToken = getAccessToken();
			return !StringUtils.isBlank(accessToken);
		} catch (JSONException e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
	}

	private boolean isValidIdentification(String identificationType, String identificationValue) {
		if (StringUtils.isAnyBlank(identificationType, identificationValue))
			return false;
		if ("NULL".equalsIgnoreCase(identificationType))
			return false;
		return !"NULL".equalsIgnoreCase(identificationValue);
	}
}
