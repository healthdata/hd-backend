/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.v1.impl;

import be.wiv_isp.healthdata.catalogue.standalone.service.ISalesForceService;
import be.wiv_isp.healthdata.catalogue.standalone.service.v1.IMultiCenterServiceV1;
import be.wiv_isp.healthdata.common.dto.SalesForceAccountDto;
import be.wiv_isp.healthdata.common.dto.SalesForceOrganizationDto;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Set;

@Service
public class MultiCenterServiceV1 implements IMultiCenterServiceV1 {

	@Autowired
	private ISalesForceService salesForceService;

	@Override
	public Response getLinkedOrganizations(UriInfo info) {
		String identificationType = RestUtils.getParameterString(info, RestUtils.ParameterName.IDENTIFICATION_TYPE);
		String identificationValue = RestUtils.getParameterString(info, RestUtils.ParameterName.IDENTIFICATION_VALUE);

		Set<SalesForceOrganizationDto> linkedOrganizations = salesForceService.getLinkedOrganizations(identificationType, identificationValue);

		return Response
				.ok(MultiCenterResponseMapperV1.convert(linkedOrganizations))
				.build();
	}

	@Override
	public Response create(UriInfo info, JSONObject json) {
		try {
			String parentIdentificationType = RestUtils.getParameterString(info, RestUtils.ParameterName.PARENT_IDENTIFICATION_TYPE);
			String parentIdentificationValue = RestUtils.getParameterString(info, RestUtils.ParameterName.PARENT_IDENTIFICATION_VALUE);

			salesForceService.createOrganizations(parentIdentificationType, parentIdentificationValue, SalesForceAccountDto.map(json));

			return Response
					.noContent()
					.build();
		} catch (JSONException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.JSON_PARSE_EXCEPTION);
			throw exception;
		}
	}


	@Override
	public Response getName(UriInfo info) {
		String identificationType = RestUtils.getParameterString(info, RestUtils.ParameterName.IDENTIFICATION_TYPE);
		String identificationValue = RestUtils.getParameterString(info, RestUtils.ParameterName.IDENTIFICATION_VALUE);

		SalesForceOrganizationDto organisation = salesForceService.getName(identificationType, identificationValue);

		return Response
				.ok(MultiCenterResponseMapperV1.convert(organisation))
				.build();
	}
}
