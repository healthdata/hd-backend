/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.api;

import be.wiv_isp.healthdata.catalogue.standalone.service.v2.IDataCollectionServiceV2;
import be.wiv_isp.healthdata.catalogue.standalone.service.v3.IDataCollectionServiceV3;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Component
@Path("/datacollections")
public class DataCollectionRestService {

	@Autowired
	private IDataCollectionServiceV2 v2;

	@Autowired
	private IDataCollectionServiceV3 v3;

	@POST
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(@HeaderParam("ClientVersion") String clientVersion, @Context UriInfo info, String json) throws Exception {
		String apiVersion = ApiVersion.get(clientVersion, info, ApiType.DATA_COLLECTIONS);
		if (ApiVersion.DataCollection.V2.equals(apiVersion)) {
			return v2.create(info, new JSONObject(json));
		}
		if (ApiVersion.DataCollection.V3.equals(apiVersion)) {
			return v3.create(info, new JSONObject(json));
		}
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.INVALID_CLIENT_VERSION, clientVersion);
		throw exception;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(@HeaderParam("ClientVersion") String clientVersion, @Context UriInfo info) {
		String apiVersion = ApiVersion.get(clientVersion, info, ApiType.DATA_COLLECTIONS);
		if (ApiVersion.DataCollection.V2.equals(apiVersion)) {
			return v2.getAll(info);
		}
		if (ApiVersion.DataCollection.V3.equals(apiVersion)) {
			return v3.getAll(info);
		}
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.INVALID_CLIENT_VERSION, clientVersion);
		throw exception;
	}

	@GET
	@Path("/stabledata")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStableData(@HeaderParam("ClientVersion") String clientVersion, @Context UriInfo info) {
		String apiVersion = ApiVersion.get(clientVersion, info, ApiType.DATA_COLLECTIONS);
		if (ApiVersion.DataCollection.V2.equals(apiVersion)) {
			return v2.getStableData(info);
		}
		if (ApiVersion.DataCollection.V3.equals(apiVersion)) {
			return v3.getStableData(info);
		}
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.INVALID_CLIENT_VERSION, clientVersion);
		throw exception;
	}


	@GET
	@Path("/status")
	public Response status(@HeaderParam("ClientVersion") String clientVersion, @Context UriInfo info) {
		String apiVersion = ApiVersion.get(clientVersion, info, ApiType.DATA_COLLECTIONS);
		if (ApiVersion.DataCollection.V2.equals(apiVersion)) {
			return v2.status();
		}
		if (ApiVersion.DataCollection.V3.equals(apiVersion)) {
			return v3.status();
		}
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.INVALID_CLIENT_VERSION, clientVersion);
		throw exception;
	}
}