/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.domain;

import be.wiv_isp.healthdata.catalogue.standalone.domain.User;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "INSTALLATIONS")
public class Installation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "INSTALLATION_ID")
    private Long id;

    @Column(name = "CREATED_ON", nullable = false)
    private Timestamp createdOn;

    @OneToOne(optional = false)
    @JoinColumn(name = "USER_ID")
    private User user;

    @Fetch(FetchMode.SUBSELECT)
    @OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
    @JoinColumn(name = "INSTALLATION_ID", nullable = false)
    private List<Upgrade> upgrades = new ArrayList<>();

    @Fetch(FetchMode.SUBSELECT)
    @OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL }, orphanRemoval = true, mappedBy = "installation")
    private List<Organization> organizations = new ArrayList<>();

    @Fetch(FetchMode.SUBSELECT)
    @OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL}, orphanRemoval = true, mappedBy = "installation")
    private List<Hd4dpStatus> statuses = new ArrayList<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Upgrade> getUpgrades() {
        return upgrades;
    }

    public void setUpgrades(List<Upgrade> upgrades) {
        this.upgrades = upgrades;
    }

    public List<Organization> getOrganizations() {
        return organizations;
    }

    public void setOrganizations(List<Organization> organizations) {
        this.organizations.clear();
        this.organizations.addAll(organizations);
    }

    public List<Hd4dpStatus> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<Hd4dpStatus> statuses) {
        this.statuses = statuses;
    }

    @PrePersist
    public void onCreate() {
        setCreatedOn(new Timestamp(new Date().getTime()));
    }
}
