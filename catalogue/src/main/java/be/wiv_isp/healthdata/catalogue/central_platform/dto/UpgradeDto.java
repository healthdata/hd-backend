/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.dto;

import be.wiv_isp.healthdata.catalogue.central_platform.domain.Upgrade;
import be.wiv_isp.healthdata.common.json.serializer.DateSerializer;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.sql.Timestamp;

public class UpgradeDto {

    private String version;
    private String frontendCommit;
    private String backendCommit;
    private String mappingCommit;
    private Timestamp executedOn;

    public UpgradeDto() {

    }

    public UpgradeDto(final Upgrade upgrade) {
        version = upgrade.getVersion();
        frontendCommit = upgrade.getFrontendCommit();
        backendCommit = upgrade.getBackendCommit();
        mappingCommit = upgrade.getMappingCommit();
        executedOn = upgrade.getExecutedOn();
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getFrontendCommit() {
        return frontendCommit;
    }

    public void setFrontendCommit(String frontendCommit) {
        this.frontendCommit = frontendCommit;
    }

    public String getBackendCommit() {
        return backendCommit;
    }

    public void setBackendCommit(String backendCommit) {
        this.backendCommit = backendCommit;
    }

    public String getMappingCommit() {
        return mappingCommit;
    }

    public void setMappingCommit(String mappingCommit) {
        this.mappingCommit = mappingCommit;
    }

    @JsonSerialize(using = DateSerializer.class)
    public Timestamp getExecutedOn() {
        return executedOn;
    }

    public void setExecutedOn(Timestamp executedOn) {
        this.executedOn = executedOn;
    }
}
