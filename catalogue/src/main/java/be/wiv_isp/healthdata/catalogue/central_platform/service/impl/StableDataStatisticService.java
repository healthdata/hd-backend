/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.service.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IStableDataStatisticDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.JsonObjectsByOrganization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Organization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.StableDataStatistic;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.StableDataStatisticSearch;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IOrganizationService;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IStableDataStatisticService;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StableDataStatisticService extends AbstractService<StableDataStatistic, StableDataStatistic.StableDataStatisticId, StableDataStatisticSearch, IStableDataStatisticDao> implements IStableDataStatisticService {

    private static final Logger LOG = LoggerFactory.getLogger(StableDataStatisticService.class);

    @Autowired
    private IStableDataStatisticDao dao;

    @Autowired
    private IOrganizationService organizationService;

    public StableDataStatisticService() {
        super(StableDataStatistic.class);
    }

    @Override
    protected IStableDataStatisticDao getDao() {
        return dao;
    }

    @Override
    @Transactional
    public void update(final Long installationId, final JsonObjectsByOrganization data) {
        LOG.info("Updating stable data statistics for installation {}", installationId);

        final List<Organization> organizations = organizationService.getAllByInstallationId(installationId);

        for (final Organization organization : organizations) {
            updateOrganization(organization, data);
        }

        LOG.info("Stable data statistics successfully updated for installation {}", installationId);
    }

    private void updateOrganization(final Organization organization, final JsonObjectsByOrganization statistics) {
        LOG.debug("Updating stable data statistics for organization {} (id value = {})", organization.getId(), organization.getIdentificationValue());

        organization.getStableDataStatistics().clear();
        final List<JSONObject> jsonObjects = statistics.getJsonObjects(organization.getIdentificationValue());
        for (final JSONObject jsonObject : jsonObjects) {
            final StableDataStatistic stableDataStatistic = unmarshal(jsonObject);
            stableDataStatistic.setOrganization(organization);
            organization.getStableDataStatistics().add(stableDataStatistic);
        }
        organizationService.update(organization);
        LOG.debug("Stable data statistics successfully updated for organization {} (id value = {})", organization.getId(), organization.getIdentificationValue());
    }

    private StableDataStatistic unmarshal(final JSONObject json) {
        final StableDataStatistic stableDataStatistic = new StableDataStatistic();
        stableDataStatistic.setDataCollectionName(JsonUtils.getString(json, "dataCollectionName"));
        stableDataStatistic.setCount(JsonUtils.getLong(json, "count"));
        stableDataStatistic.setDistinctPatientIdCount(JsonUtils.getLong(json, "distinctPatientIdCount"));
        return stableDataStatistic;
    }
}
