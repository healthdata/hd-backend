/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.service.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.service.IInstallationService;
import be.wiv_isp.healthdata.catalogue.central_platform.dao.IInstallationDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Installation;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Organization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.InstallationSearch;
import be.wiv_isp.healthdata.catalogue.standalone.domain.User;
import be.wiv_isp.healthdata.common.api.exception.ForbiddenException;
import be.wiv_isp.healthdata.common.api.exception.NotFoundException;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class InstallationService extends AbstractService<Installation, Long, InstallationSearch, IInstallationDao> implements IInstallationService {

    @Autowired
    private IInstallationDao installationDao;

    public InstallationService() {
        super(Installation.class);
    }

    @Override
    protected IInstallationDao getDao() {
        return installationDao;
    }

    @Override
    @Transactional
    public Installation updateOrganizations(final Installation installation, final List<Organization> organizations) {
        final List<Organization> updatedOrganizations = new ArrayList<>();

        for (final Organization organization : organizations) {
            final String identificationValue = organization.getIdentificationValue();
            final Organization existingOrganization = findOrganizationByIdentificationValue(installation.getOrganizations(), identificationValue);
            if(existingOrganization != null) {
                // map properties
                existingOrganization.setName(organization.getName());
                existingOrganization.setMain(organization.isMain());
                existingOrganization.setDeleted(organization.isDeleted());

                updatedOrganizations.add(existingOrganization);
            } else {
                organization.setInstallation(installation);
                updatedOrganizations.add(organization);
            }
        }
        installation.setOrganizations(updatedOrganizations);
        return update(installation);
    }

    private Organization findOrganizationByIdentificationValue(final List<Organization> organizations, final String identificationValue) {
        for (final Organization organization : organizations) {
            if(identificationValue.equals(organization.getIdentificationValue())) {
                return organization;
            }
        }
        return null;
    }

    @Override
    public Installation verifyAccess(final Long installationId) {
        final Installation installation = get(installationId);
        if(installation == null) {
            throw new NotFoundException(MessageFormat.format("Installation with id {0} not found", installationId));
        }

        final User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(!installation.getUser().equals(user)) {
            throw new ForbiddenException();
        }
        return installation;
    }

}
