/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "STABLE_DATA_STATISTICS")
@IdClass(StableDataStatistic.StableDataStatisticId.class)
public class StableDataStatistic {

    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "ORGANIZATION_ID")
    private Organization organization;

    @Id
    @Column(name = "DATA_COLLECTION_NAME", nullable = false)
    private String dataCollectionName;

    @Column(name = "COUNT", nullable = false)
    private Long count;

    @Column(name = "DISTINCT_PATIENT_ID_COUNT", nullable = false)
    private Long distinctPatientIdCount;

    @Column(name = "UPDATED_ON")
    private Timestamp updatedOn;

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public String getDataCollectionName() {
        return dataCollectionName;
    }

    public void setDataCollectionName(String dataCollectionName) {
        this.dataCollectionName = dataCollectionName;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getDistinctPatientIdCount() {
        return distinctPatientIdCount;
    }

    public void setDistinctPatientIdCount(Long distinctPatientIdCount) {
        this.distinctPatientIdCount = distinctPatientIdCount;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @PreUpdate
    @PrePersist
    public void onUpdate() {
        updatedOn = new Timestamp(new Date().getTime());
    }

    public static class StableDataStatisticId implements Serializable {

        private Organization organization;
        private String dataCollectionName;

        public StableDataStatisticId() {

        }

        public StableDataStatisticId(Organization organization, String dataCollectionName) {
            this.organization = organization;
            this.dataCollectionName = dataCollectionName;
        }

        public Organization getOrganization() {
            return organization;
        }

        public void setOrganization(Organization organization) {
            this.organization = organization;
        }

        public String getDataCollectionName() {
            return dataCollectionName;
        }

        public void setDataCollectionName(String dataCollectionName) {
            this.dataCollectionName = dataCollectionName;
        }


        @Override
        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }

            if (o instanceof StableDataStatistic.StableDataStatisticId) {
                StableDataStatistic.StableDataStatisticId other = (StableDataStatistic.StableDataStatisticId) o;

                return Objects.equals(organization, other.organization)
                        && Objects.equals(dataCollectionName, other.dataCollectionName);
            }
            return false;
        }

        @Override
        public int hashCode() {
            return Objects.hash(
                    this.organization,
                    this.dataCollectionName);
        }
    }
}
