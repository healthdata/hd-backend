/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.catalogue.central_platform.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "MESSAGE_STATISTICS")
@IdClass(MessageStatistic.MessageStatisticId.class)
public class MessageStatistic {

    @Id
    @ManyToOne(optional = false)
    @JoinColumn(name = "ORGANIZATION_ID", nullable = false)
    private Organization organization;

    @Id
    @Column(name = "STATUS", nullable = false)
    private String status;

    @Column(name = "COUNT", nullable = false)
    private Long count;

    @Column(name="UPDATED_ON", nullable = false)
    private Timestamp updatedOn;


    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @PreUpdate
    @PrePersist
    public void onUpdate() {
        setUpdatedOn(new Timestamp(new Date().getTime()));
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessageStatistic that = (MessageStatistic) o;

        if (!organization.equals(that.organization)) return false;
        if (!status.equals(that.status)) return false;
        return count.equals(that.count);
    }

    @Override
    public int hashCode() {
        int result = organization.hashCode();
        result = 31 * result + status.hashCode();
        result = 31 * result + count.hashCode();
        return result;
    }

    public static class MessageStatisticId implements Serializable {
        private Organization organization;
        private String status;

        public MessageStatisticId() {
        }

        public MessageStatisticId(Organization organization, String status) {
            this.organization = organization;
            this.status = status;
        }

        public Organization getOrganization() {
            return organization;
        }

        public void setOrganization(Organization organization) {
            this.organization = organization;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

}
