/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.domain;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Objects;

@Entity
@Table(name = "PDF_TEMPLATES")
public class PdfTemplate {

    @Id
    @Column(name = "DATA_COLLECTION_DEFINITION_ID")
    private Long id;

    @Lob
    @Column(name = "TEMPLATE_EN", nullable = false)
    private byte[] templateEn;

    @Lob
    @Column(name = "TEMPLATE_FR", nullable = false)
    private byte[] templateFr;

    @Lob
    @Column(name = "TEMPLATE_NL", nullable = false)
    private byte[] templateNl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getTemplateEn() {
        return templateEn;
    }

    public void setTemplateEn(byte[] templateEn) {
        this.templateEn = templateEn;
    }

    public byte[] getTemplateFr() {
        return templateFr;
    }

    public void setTemplateFr(byte[] templateFr) {
        this.templateFr = templateFr;
    }

    public byte[] getTemplateNl() {
        return templateNl;
    }

    public void setTemplateNl(byte[] templateNl) {
        this.templateNl = templateNl;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof PdfTemplate) {
            PdfTemplate other = (PdfTemplate) o;

            return Objects.equals(id, other.id)
                    && Arrays.equals(templateEn, other.templateEn)
                    && Arrays.equals(templateFr, other.templateFr)
                    && Arrays.equals(templateNl, other.templateNl);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                this.id,
                Arrays.hashCode(this.templateEn),
                Arrays.hashCode(this.templateFr),
                Arrays.hashCode(this.templateNl));
    }

    @Override
    public String toString() {
        return "PdfTemplate {" +
                "id = " + Objects.toString(this.id) + ", " +
                "templateFr = " + new String(this.templateEn) + ", " +
                "templateFr = " + new String(this.templateFr) + ", " +
                "templateNl = " + new String(this.templateNl) + "}";
    }
}
