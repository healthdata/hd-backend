/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.v2.impl;

import be.wiv_isp.healthdata.catalogue.standalone.service.ISalesForceService;
import be.wiv_isp.healthdata.catalogue.standalone.service.v2.IDataCollectionServiceV2;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.HashSet;
import java.util.Set;

@Service
public class DataCollectionServiceV2 implements IDataCollectionServiceV2 {

	@Autowired
	private ISalesForceService salesForceService;

	@Override
	public Response create(UriInfo info, JSONObject dataCollection) throws JSONException{
		salesForceService.createDataCollection(dataCollection.getString("Name"));
		return Response.noContent().build();
	}

	@Override
	public Response getAll(UriInfo info) {
		String identificationType = getIdentificationType(info);
		String identificationValue = getIdentificationValue(info);
		Boolean publicOnly = getPublicOnly(info);
		Set<String> dataCollections = new HashSet<>();
		if (StringUtils.isNoneBlank(identificationType, identificationValue)) {
			dataCollections.addAll(salesForceService.getDataCollections(identificationType, identificationValue));
			dataCollections.addAll(salesForceService.getDataCollections(true));
			if(salesForceService.isHd4prcOrganization(identificationType, identificationValue)) {
				dataCollections.addAll(salesForceService.getHd4prcDataCollections());
			}
		} else {
			dataCollections = salesForceService.getDataCollections(Boolean.TRUE.equals(publicOnly));
		}
		return Response.ok(dataCollections).build();
	}

	@Override
	public Response getStableData(UriInfo info) {
		Set<String> dataCollections = salesForceService.getStableData();
		return Response.ok(dataCollections).build();
	}

	@Override
	public Response status() {
		boolean status = salesForceService.status();
		if (status) {
			return Response.noContent().build();
		} else {
			return Response.serverError().build();
		}
	}

	private Boolean getPublicOnly(UriInfo info) {
		return RestUtils.getParameterBoolean(info, "publicOnly");
	}

	private String getIdentificationType(UriInfo info) {
		return RestUtils.getParameterString(info, "identificationType");
	}

	private String getIdentificationValue(UriInfo info) {
		return RestUtils.getParameterString(info, "identificationValue");
	}

}
