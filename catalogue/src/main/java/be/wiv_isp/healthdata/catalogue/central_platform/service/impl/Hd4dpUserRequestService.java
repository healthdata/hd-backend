/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.service.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IHd4dpUserRequestDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Hd4dpUserRequest;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.JsonObjectsByOrganization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Organization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.Hd4dpUserRequestSearch;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IHd4dpUserRequestService;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IOrganizationService;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class Hd4dpUserRequestService extends AbstractService<Hd4dpUserRequest, Hd4dpUserRequest.Hd4dpUserRequestId, Hd4dpUserRequestSearch, IHd4dpUserRequestDao> implements IHd4dpUserRequestService {

    private static final Logger LOG = LoggerFactory.getLogger(Hd4dpUserRequestService.class);

    @Autowired
    private IHd4dpUserRequestDao dao;

    @Autowired
    private IOrganizationService organizationService;

    public Hd4dpUserRequestService() {
        super(Hd4dpUserRequest.class);
    }

    @Override
    protected IHd4dpUserRequestDao getDao() {
        return dao;
    }

    @Override
    @Transactional
    public void update(Long installationId, JsonObjectsByOrganization userRequests) {
        LOG.info("Updating user requests for installation {}", installationId);

        final List<Organization> organizations = organizationService.getAllByInstallationId(installationId);

        for (final Organization organization : organizations) {
            updateOrganization(organization, userRequests);
        }

        LOG.info("User requests successfully updated for installation {}", installationId);
    }

    private void updateOrganization(final Organization organization, final JsonObjectsByOrganization userRequests) {
        LOG.debug("Updating user requests for organization {} (id value = {})", organization.getId(), organization.getIdentificationValue());

        organization.getUserRequests().clear();
        final List<JSONObject> jsonObjects = userRequests.getJsonObjects(organization.getIdentificationValue());
        for (final JSONObject json : jsonObjects) {
            final Hd4dpUserRequest userRequest = unmarshal(json);
            userRequest.setOrganization(organization);
            organization.getUserRequests().add(userRequest);
        }
        organizationService.update(organization);
        LOG.debug("User requests successfully updated for organization {} (id value = {})", organization.getId(), organization.getIdentificationValue());
    }

    private Hd4dpUserRequest unmarshal(final JSONObject json) {
        final Hd4dpUserRequest userRequest = new Hd4dpUserRequest();
        userRequest.setHd4dpId(JsonUtils.getLong(json, "id"));
        userRequest.setUsername(JsonUtils.getString(json, "username"));
        userRequest.setFirstName(JsonUtils.getString(json, "firstName"));
        userRequest.setLastName(JsonUtils.getString(json, "lastName"));
        userRequest.setEmail(JsonUtils.getString(json, "email"));
        userRequest.setEmailRequester(JsonUtils.getString(json, "emailRequester"));
        userRequest.setApproved(JsonUtils.getBooleanObject(json, "approved"));
        userRequest.setAuthorities(JsonUtils.getStringSet(json, "authorities"));
        userRequest.setDataCollectionNames(JsonUtils.getStringSet(json, "dataCollectionNames"));
        return userRequest;
    }
}
