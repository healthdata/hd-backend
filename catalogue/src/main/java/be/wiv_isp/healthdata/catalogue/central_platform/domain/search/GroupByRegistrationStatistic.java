/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.domain.search;

import java.util.Date;

public class GroupByRegistrationStatistic {

    private String dataCollectionName;
    private Integer majorVersion;
    private String healthDataIdentificationValue;
    private String healthDataIdentificationName;
    private String status;
    private Long count;
    private Date minUpdatedOn;

    public GroupByRegistrationStatistic() {

    }

    public GroupByRegistrationStatistic(String dataCollectionName, Integer majorVersion, String healthDataIdentificationValue, String healthDataIdentificationName, String status, Long count, Date minUpdatedOn) {
        this.dataCollectionName = dataCollectionName;
        this.majorVersion = majorVersion;
        this.healthDataIdentificationValue = healthDataIdentificationValue;
        this.healthDataIdentificationName = healthDataIdentificationName;
        this.status = status;
        this.count = count;
        this.minUpdatedOn = minUpdatedOn;
    }

    public String getDataCollectionName() {
        return dataCollectionName;
    }

    public void setDataCollectionName(String dataCollectionName) {
        this.dataCollectionName = dataCollectionName;
    }

    public Integer getMajorVersion() {
        return majorVersion;
    }

    public void setMajorVersion(Integer majorVersion) {
        this.majorVersion = majorVersion;
    }

    public String getHealthDataIdentificationValue() {
        return healthDataIdentificationValue;
    }

    public void setHealthDataIdentificationValue(String healthDataIdentificationValue) {
        this.healthDataIdentificationValue = healthDataIdentificationValue;
    }

    public String getHealthDataIdentificationName() {
        return healthDataIdentificationName;
    }

    public void setHealthDataIdentificationName(String healthDataIdentificationName) {
        this.healthDataIdentificationName = healthDataIdentificationName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Date getMinUpdatedOn() {
        return minUpdatedOn;
    }

    public void setMinUpdatedOn(Date minUpdatedOn) {
        this.minUpdatedOn = minUpdatedOn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GroupByRegistrationStatistic)) return false;

        GroupByRegistrationStatistic that = (GroupByRegistrationStatistic) o;

        if (dataCollectionName != null ? !dataCollectionName.equals(that.dataCollectionName) : that.dataCollectionName != null)
            return false;
        if (majorVersion != null ? !majorVersion.equals(that.majorVersion) : that.majorVersion != null) return false;
        if (healthDataIdentificationValue != null ? !healthDataIdentificationValue.equals(that.healthDataIdentificationValue) : that.healthDataIdentificationValue != null)
            return false;
        if (healthDataIdentificationName != null ? !healthDataIdentificationName.equals(that.healthDataIdentificationName) : that.healthDataIdentificationName != null)
            return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (count != null ? !count.equals(that.count) : that.count != null) return false;
        return !(minUpdatedOn != null ? !minUpdatedOn.equals(that.minUpdatedOn) : that.minUpdatedOn != null);

    }

    @Override
    public int hashCode() {
        int result = dataCollectionName != null ? dataCollectionName.hashCode() : 0;
        result = 31 * result + (majorVersion != null ? majorVersion.hashCode() : 0);
        result = 31 * result + (healthDataIdentificationValue != null ? healthDataIdentificationValue.hashCode() : 0);
        result = 31 * result + (healthDataIdentificationName != null ? healthDataIdentificationName.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (count != null ? count.hashCode() : 0);
        result = 31 * result + (minUpdatedOn != null ? minUpdatedOn.hashCode() : 0);
        return result;
    }
}
