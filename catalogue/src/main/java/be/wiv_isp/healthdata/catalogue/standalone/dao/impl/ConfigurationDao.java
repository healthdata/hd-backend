/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.dao.impl;

import be.wiv_isp.healthdata.catalogue.standalone.dao.IConfigurationDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Configuration;
import be.wiv_isp.healthdata.common.dao.impl.CrudDao;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ConfigurationDao extends CrudDao<Configuration, Long> implements IConfigurationDao {

	public ConfigurationDao() {
		super(Configuration.class);
	}

	@Override
	public Configuration get(String lookup) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Configuration> cq = cb.createQuery(Configuration.class);
		Root<Configuration> rootEntry = cq.from(Configuration.class);
		CriteriaQuery<Configuration> all = cq.select(rootEntry);

		List<Predicate> predicates = new ArrayList<>();
		if (lookup != null) {
			predicates.add(cb.equal(cb.lower(rootEntry.<String> get("key")), lookup.toLowerCase()));
		}
		if (!predicates.isEmpty()) {
			cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		}
		TypedQuery<Configuration> allQuery = em.createQuery(all);
		return allQuery.getSingleResult();
	}
}
