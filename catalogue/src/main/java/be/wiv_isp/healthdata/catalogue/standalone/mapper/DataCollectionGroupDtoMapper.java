/*
 *  Copyright 2014-2017 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.catalogue.standalone.mapper;


import be.wiv_isp.healthdata.catalogue.standalone.dto.DataCollectionGroupDto;
import be.wiv_isp.healthdata.common.dto.PeriodDto;
import be.wiv_isp.healthdata.common.dto.TranslatableStringDto;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.apache.commons.lang3.time.DateUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;

public class DataCollectionGroupDtoMapper {

    static public DataCollectionGroupDto map(JSONObject json) throws JSONException {
        DataCollectionGroupDto dataCollectionGroup = new DataCollectionGroupDto();

        for (Iterator<String> iterator = json.keys(); iterator.hasNext(); ) {
            String key = iterator.next();
            if (!"null".equalsIgnoreCase(json.getString(key))) {
                if ("id".equalsIgnoreCase(key)) {
                    try {
                        dataCollectionGroup.setId(Long.valueOf(json.getString(key)));
                    } catch (NumberFormatException e) {
                        HealthDataException exception = new HealthDataException();
                        exception.setExceptionType(ExceptionType.INVALID_FORMAT, json.getString(key), "long");
                        throw exception;
                    }
                } else if ("name".equalsIgnoreCase(key)) {
                    dataCollectionGroup.setName(json.getString(key));
                } else if ("label".equalsIgnoreCase(key)) {
                    final TranslatableStringDto label = new TranslatableStringDto();
                    final JSONObject labelJSON = json.getJSONObject(key);
                    if (labelJSON.names() != null) {
                        for (int j = 0; j < labelJSON.names().length(); ++j) {
                            final String languagueKey = labelJSON.names().getString(j);
                            if ("en".equalsIgnoreCase(languagueKey)) {
                                label.setEn(labelJSON.getString(languagueKey));
                            } else if ("fr".equalsIgnoreCase(languagueKey)) {
                                label.setFr(labelJSON.getString(languagueKey));
                            } else if ("nl".equalsIgnoreCase(languagueKey)) {
                                label.setNl(labelJSON.getString(languagueKey));
                            }
                        }
                    }
                    dataCollectionGroup.setLabel(label);
                } else if ("description".equalsIgnoreCase(key)) {
                    final TranslatableStringDto description = new TranslatableStringDto();
                    final JSONObject descriptionJSON = json.getJSONObject(key);
                    if (descriptionJSON.names() != null) {
                        for (int j = 0; j < descriptionJSON.names().length(); ++j) {
                            final String languagueKey = descriptionJSON.names().getString(j);
                            if ("en".equalsIgnoreCase(languagueKey)) {
                                description.setEn(descriptionJSON.getString(languagueKey));
                            } else if ("fr".equalsIgnoreCase(languagueKey)) {
                                description.setFr(descriptionJSON.getString(languagueKey));
                            } else if ("nl".equalsIgnoreCase(languagueKey)) {
                                description.setNl(descriptionJSON.getString(languagueKey));
                            }
                        }
                    }
                    dataCollectionGroup.setDescription(description);
                } else if ("startDate".equalsIgnoreCase(key)) {
                    try {
                        final Date asDate = DateUtils.parseDate(json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
                        dataCollectionGroup.setStartDate(new Timestamp(asDate.getTime()));
                    } catch (ParseException e) {
                        HealthDataException exception = new HealthDataException(e);
                        exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
                        throw exception;
                    }
                } else if ("endDateCreation".equalsIgnoreCase(key)) {
                    try {
                        final Date asDate = DateUtils.parseDate(json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
                        dataCollectionGroup.setEndDateCreation(new Timestamp(asDate.getTime()));
                    } catch (ParseException e) {
                        HealthDataException exception = new HealthDataException(e);
                        exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
                        throw exception;
                    }
                } else if ("endDateSubmission".equalsIgnoreCase(key)) {
                    try {
                        final Date asDate = DateUtils.parseDate(json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
                        dataCollectionGroup.setEndDateSubmission(new Timestamp(asDate.getTime()));
                    } catch (ParseException e) {
                        HealthDataException exception = new HealthDataException(e);
                        exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
                        throw exception;
                    }
                } else if ("endDateComments".equalsIgnoreCase(key)) {
                    try {
                        final Date asDate = DateUtils.parseDate(json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
                        dataCollectionGroup.setEndDateComments(new Timestamp(asDate.getTime()));
                    } catch (ParseException e) {
                        HealthDataException exception = new HealthDataException(e);
                        exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
                        throw exception;
                    }
                } else if ("createdOn".equalsIgnoreCase(key)) {
                    try {
                        final Date asDate = DateUtils.parseDate(json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
                        dataCollectionGroup.setCreatedOn(new Timestamp(asDate.getTime()));
                    } catch (ParseException e) {
                        HealthDataException exception = new HealthDataException(e);
                        exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
                        throw exception;
                    }
                } else if ("updatedOn".equalsIgnoreCase(key)) {
                    try {
                        final Date asDate = DateUtils.parseDate(json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
                        dataCollectionGroup.setUpdatedOn(new Timestamp(asDate.getTime()));
                    } catch (ParseException e) {
                        HealthDataException exception = new HealthDataException(e);
                        exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
                        throw exception;
                    }
                } else if ("participationContent".equalsIgnoreCase(key)) {
                    final JSONObject contentJSON = json.getJSONObject(key);
                    dataCollectionGroup.setParticipationContent(contentJSON.toString().getBytes(StandardCharsets.UTF_8));
                } else if ("majorVersion".equalsIgnoreCase(key)) {
                    dataCollectionGroup.setMajorVersion(json.getInt(key));
                } else if ("period".equalsIgnoreCase(key)) {
                    String start = json.getJSONObject(key).getString("start");
                    String end = json.getJSONObject(key).getString("end");
                    if ("null".equalsIgnoreCase(start))
                        start = null;
                    if ("null".equalsIgnoreCase(end))
                        end = null;
                    PeriodDto period = new PeriodDto(start, end);
                    dataCollectionGroup.setPeriod(period);
                }
            }

        }
        return dataCollectionGroup;
    }
}
