/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.catalogue.central_platform.dao.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IMessageStatisticDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Installation;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.MessageStatistic;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.MessageStatisticSearch;
import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MessageStatisticDao extends CrudDaoV2<MessageStatistic, MessageStatistic.MessageStatisticId, MessageStatisticSearch> implements IMessageStatisticDao {

    public MessageStatisticDao() {
        super(MessageStatistic.class);
    }

    @Override
    protected List<Predicate> getPredicates(MessageStatisticSearch search, CriteriaBuilder cb, Root<MessageStatistic> rootEntry) {
        List<Predicate> predicates = new ArrayList<>();
        if (search.getOrganizationId() != null) {
            predicates.add(cb.equal(rootEntry.<Installation>get("organization").get("id"), search.getOrganizationId()));
        }
        if (search.getStatus() != null) {
            predicates.add(cb.equal(rootEntry.get("status"), search.getStatus()));
        }
        return predicates;
    }

}
