/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.dao.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IUniqueIdDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.UniqueId;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.UniqueIdSearch;
import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.common.domain.search.Field;
import be.wiv_isp.healthdata.common.domain.search.Ordering;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UniqueIdDao extends CrudDaoV2<UniqueId, UniqueId.CompositeId, UniqueIdSearch> implements IUniqueIdDao {

    public UniqueIdDao() {
        super(UniqueId.class);
    }

    @Override
    protected List<Predicate> getPredicates(UniqueIdSearch search, CriteriaBuilder cb, Root<UniqueId> rootEntry) {
        final List<Predicate> predicates = new ArrayList<>();
        if(search.getKey() != null) {
            predicates.add(cb.equal(rootEntry.<String>get("key"), search.getKey()));
        }
        return predicates;
    }

    @Override
    public UniqueId getLast(String key) {
        final UniqueIdSearch search = new UniqueIdSearch();
        search.setKey(key);
        search.setOrdering(new Ordering(Field.COUNTER, Ordering.Type.DESC));
        search.setMaxResults(1);
        return getUnique(search);
    }

}
