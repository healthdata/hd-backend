/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.service.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.domain.RegistryDependentIdGenerationInput;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IRegistryDependentIdService;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IUniqueIdService;
import be.wiv_isp.healthdata.catalogue.standalone.service.ISalesForceService;
import be.wiv_isp.healthdata.common.domain.RegistryDependentIdGenerationDefinition;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataAssert;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.text.MessageFormat;

@Service
public class RegistryDependentIdService implements IRegistryDependentIdService {

    private static final Logger LOG = LoggerFactory.getLogger(RegistryDependentIdService.class);

    @Autowired
    private IUniqueIdService uniqueIdService;

    @Autowired
    private ISalesForceService salesForceService;

    @Override
    public String getRegistryDependentId(String dataCollectionName, RegistryDependentIdGenerationDefinition.Phase phase, RegistryDependentIdGenerationInput extraParameters) {
        LOG.debug("Retrieving registry dependent id for data collection [{}] and phase [{}]", dataCollectionName, phase);
        final RegistryDependentIdGenerationDefinition def = salesForceService.getRegistryDependentIdGenerationDefinition(dataCollectionName);

        if(!def.isGenerationEnabled()) {
            LOG.trace("No registry dependent id must be generated for this data collection");
            return null;
        }

        if(!def.getPhase().equals(phase)) {
            LOG.trace("No registry dependent id must be generated for this phase");
            return null;
        }

        switch (def.getType()) {
            case TYPE_1:
                return generateType1(dataCollectionName, extraParameters);
            case TYPE_2:
                return generateType2(dataCollectionName, extraParameters);
            default:
                HealthDataException exception = new HealthDataException();
                exception.setExceptionType(ExceptionType.INVALID_INPUT, MessageFormat.format("type [{0}] is not supported", def.getType()));
                throw exception;
        }

    }

    private String generateType1(String dataCollectionName, RegistryDependentIdGenerationInput extraParameters) {
        LOG.debug("Generating registry dependent identifier of type 1");
        HealthDataAssert.assertNotNull(extraParameters.getSubmissionDate(), Response.Status.BAD_REQUEST, "no submission date provided");
        final String registryCode = salesForceService.getRegistryCode(dataCollectionName);
        HealthDataAssert.assertTrue(registryCode.length() == 3, Response.Status.INTERNAL_SERVER_ERROR, "the registry code must be a string value of length 3");
        final String yearOfSubmission = DateFormat.YY.format(extraParameters.getSubmissionDate());
        final String key = StringUtils.joinWith("_", dataCollectionName, yearOfSubmission).toUpperCase();
        final Long counterPerYear = uniqueIdService.getNext(key).getCounter();
        final int counterUpperBound = 999999;
        HealthDataAssert.assertTrue(counterPerYear <= counterUpperBound, Response.Status.INTERNAL_SERVER_ERROR, "the yearly counter must be lower than {0}", counterUpperBound);
        final String counterPerYearString = String.format("%06d", counterPerYear);
        final String mod97 = computeMod97(registryCode, yearOfSubmission, counterPerYearString);
        return StringUtils.joinWith(".", registryCode, yearOfSubmission, counterPerYearString, mod97);
    }

    private String generateType2(String dataCollectionName, RegistryDependentIdGenerationInput extraParameters) {
        LOG.debug("Generating registry dependent identifier of type 2");
        final String organizationIdentificationValue = extraParameters.getOrganizationIdentificationValue();
        HealthDataAssert.assertNotNull(organizationIdentificationValue, Response.Status.BAD_REQUEST, "no organization identification value provided");
        HealthDataAssert.assertTrue(extraParameters.getOrganizationIdentificationValue().length() == 8, Response.Status.BAD_REQUEST, "the organization identification value must be a string value of length 8");
        final String organizationIdValueSubString = organizationIdentificationValue.substring(3, 6);
        final String key = StringUtils.joinWith("_", dataCollectionName, organizationIdValueSubString);
        final Long counter = uniqueIdService.getNext(key).getCounter();
        final int counterUpperBound= 9999;
        HealthDataAssert.assertTrue(counter <= counterUpperBound, Response.Status.INTERNAL_SERVER_ERROR, "the counter must be lower than {0}", counterUpperBound);
        final String counterString = String.format("%04d", counter);
        return StringUtils.joinWith(".", organizationIdValueSubString, counterString);
    }

    private String computeMod97(String registryCode, String yearOfSubmission, String counterPerYearString) {
        final Long value = Long.valueOf(registryCode + yearOfSubmission + counterPerYearString);
        return String.format("%02d", value % 97);
    }
}
