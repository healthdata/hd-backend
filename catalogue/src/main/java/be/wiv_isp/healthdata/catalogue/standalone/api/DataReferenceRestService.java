/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.api;

import be.wiv_isp.healthdata.catalogue.standalone.domain.DataReference;
import be.wiv_isp.healthdata.catalogue.standalone.dto.DataReferenceDto;
import be.wiv_isp.healthdata.catalogue.standalone.dto.FullDataReferenceDto;
import be.wiv_isp.healthdata.catalogue.standalone.enumeration.SearchFilterType;
import be.wiv_isp.healthdata.catalogue.standalone.enumeration.SortingType;
import be.wiv_isp.healthdata.catalogue.standalone.search.builder.DataReferenceSearchBuilder;
import be.wiv_isp.healthdata.catalogue.standalone.service.IDataReferenceService;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Path("/dataReference")
public class DataReferenceRestService {

	private static final Logger LOG = LoggerFactory.getLogger(DataReferenceRestService.class);

	@Autowired
	private IDataReferenceService dataReferenceService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(@Context UriInfo info) {
		LOG.debug("GET Request: Get all data references mapping with the UriInfo");
		MultivaluedMap<String, String> queryParameters = info.getQueryParameters();
		String type = queryParameters.getFirst("type");
		DataReferenceSearchBuilder searchBuilder = new DataReferenceSearchBuilder();
		searchBuilder.withType(type);
		String validOnAsString = queryParameters.getFirst("validOn");
		if (!StringUtils.isBlank(validOnAsString)) {
			SimpleDateFormat sdf = new SimpleDateFormat(DateFormat.DDMMYYYY.getPattern());
			Date validOn;
			try {
				validOn = sdf.parse(validOnAsString);
			} catch (ParseException e) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, validOnAsString, DateFormat.DDMMYYYY.getPattern());
				throw exception;
			}
			searchBuilder.withValidOn(new Timestamp(validOn.getTime()));
		}
		String filter = queryParameters.getFirst("filter");
		if (!StringUtils.isBlank(filter)) {
			try {
				searchBuilder.withFilter(SearchFilterType.valueOf(filter));
			} catch (IllegalArgumentException e) {
				HealthDataException exception = new HealthDataException(e);
				exception.setExceptionType(ExceptionType.INVALID_FORMAT, filter, SearchFilterType.values());
				throw exception;
			}
		}
		String sorting = queryParameters.getFirst("sorting");
		if (!StringUtils.isBlank(sorting)) {
			try {
				searchBuilder.withSorting(SortingType.valueOf(sorting));
			} catch (IllegalArgumentException e) {
				HealthDataException exception = new HealthDataException(e);
				exception.setExceptionType(ExceptionType.INVALID_FORMAT, sorting, SortingType.values());
				throw exception;
			}
		}
		String language = queryParameters.getFirst("language");
		if (!StringUtils.isBlank(language)) {
			searchBuilder.withLanguage(language);
		}
		String code = queryParameters.getFirst("code");
		if (!StringUtils.isBlank(code)) {
			searchBuilder.withCode(code);
		}
		String parentCode = queryParameters.getFirst("parentCode");
		if (StringUtils.isNoneBlank(parentCode)) {
			searchBuilder.withParentCode(parentCode);
		}
		String value = queryParameters.getFirst("value");
		if (!StringUtils.isBlank(value)) {
			searchBuilder.withValue(value);
		}
		String limit = queryParameters.getFirst("limit");
		if (!StringUtils.isBlank(limit)) {
			try {
				searchBuilder.withLimit(Integer.valueOf(limit));
			} catch (IllegalArgumentException e) {
				HealthDataException exception = new HealthDataException(e);
				exception.setExceptionType(ExceptionType.INVALID_FORMAT, limit, "integer");
				throw exception;
			}
		}
		List<DataReference> dataReferences = dataReferenceService.find(searchBuilder.build());
		String fullSearch = queryParameters.getFirst("fullSearch");
		if (StringUtils.isNoneBlank(fullSearch) && Boolean.valueOf(fullSearch)) {
			List<FullDataReferenceDto> result = new ArrayList<>();
			for (DataReference dataReference : dataReferences) {
				result.add(dataReferenceService.getFullDataReferenceDto(dataReference, searchBuilder.getValidOn()));
			}
			return Response.ok(result).build();
		} else {
			List<DataReferenceDto> result = new ArrayList<>();
			for (DataReference dataReference : dataReferences) {
				result.add(new DataReferenceDto(dataReference));
			}
			return Response.ok(result).build();
		}
	}
}