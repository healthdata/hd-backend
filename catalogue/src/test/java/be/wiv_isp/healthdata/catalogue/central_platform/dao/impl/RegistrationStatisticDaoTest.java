/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.central_platform.dao.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IInstallationDao;
import be.wiv_isp.healthdata.catalogue.central_platform.dao.IOrganizationDao;
import be.wiv_isp.healthdata.catalogue.central_platform.dao.IRegistrationStatisticDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Installation;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Organization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.RegistrationStatistic;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.GroupByRegistrationStatistic;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.RegistrationStatisticSearch;
import be.wiv_isp.healthdata.catalogue.standalone.dao.IDataCollectionDefinitionDao;
import be.wiv_isp.healthdata.catalogue.standalone.dao.IDataCollectionGroupDao;
import be.wiv_isp.healthdata.catalogue.standalone.dao.IUserDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.*;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class RegistrationStatisticDaoTest {

	@Autowired
	private IUserDao userDao;
	@Autowired
	private IInstallationDao installationDao;
	@Autowired
	private IOrganizationDao organizationDao;
	@Autowired
	private IRegistrationStatisticDao registrationStatisticDao;
	@Autowired
	private IDataCollectionDefinitionDao dataCollectionDefinitionDao;
	@Autowired
	private IDataCollectionGroupDao dataCollectionGroupDao;

	@Test
	public void testSearchByOrganizationId() {
		final DataCollectionDefinition dataCollectionDefinition = createDataCollectionDefinition(1L);

		final Installation installation = createInstallation("user", "password");
		final Organization organization1 = createOrganization(installation);
		final Organization organization2 = createOrganization(installation);
		Assert.assertNotEquals(organization1.getId(), organization2.getId());

		RegistrationStatistic registrationStatistic1 = new RegistrationStatistic();
		registrationStatistic1.setOrganization(organization1);
		registrationStatistic1.setDataCollectionDefinition(dataCollectionDefinition);
		registrationStatistic1.setStatus("NEW");
		registrationStatistic1.setCount(1L);
		registrationStatistic1 = registrationStatisticDao.create(registrationStatistic1);

		RegistrationStatistic registrationStatistic2 = new RegistrationStatistic();
		registrationStatistic2.setOrganization(organization2);
		registrationStatistic2.setDataCollectionDefinition(dataCollectionDefinition);
		registrationStatistic2.setStatus("NEW");
		registrationStatistic2.setCount(1L);
		registrationStatistic2 = registrationStatisticDao.create(registrationStatistic2);

		List<RegistrationStatistic> all = registrationStatisticDao.getAll();
		Assert.assertEquals(2, all.size());

		RegistrationStatisticSearch search = new RegistrationStatisticSearch();
		search.setOrganizationId(organization1.getId());
		all = registrationStatisticDao.getAll(search);

		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(registrationStatistic1));
	}

	@Test
	public void testSearchByDataCollectionDefinitionId() {
		final DataCollectionDefinition dataCollectionDefinition1 = createDataCollectionDefinition(1L);
		final DataCollectionDefinition dataCollectionDefinition2 = createDataCollectionDefinition(2L);

		final Installation installation = createInstallation("user", "password");
		final Organization organization = createOrganization(installation);

		RegistrationStatistic registrationStatistic1 = new RegistrationStatistic();
		registrationStatistic1.setOrganization(organization);
		registrationStatistic1.setDataCollectionDefinition(dataCollectionDefinition1);
		registrationStatistic1.setStatus("NEW");
		registrationStatistic1.setCount(1L);
		registrationStatistic1 = registrationStatisticDao.create(registrationStatistic1);

		RegistrationStatistic registrationStatistic2 = new RegistrationStatistic();
		registrationStatistic2.setOrganization(organization);
		registrationStatistic2.setDataCollectionDefinition(dataCollectionDefinition2);
		registrationStatistic2.setStatus("NEW");
		registrationStatistic2.setCount(1L);
		registrationStatistic2 = registrationStatisticDao.create(registrationStatistic2);

		List<RegistrationStatistic> all = registrationStatisticDao.getAll();
		Assert.assertEquals(2, all.size());

		RegistrationStatisticSearch search = new RegistrationStatisticSearch();
		search.setDataCollectionDefinitionId(1L);
		all = registrationStatisticDao.getAll(search);

		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(registrationStatistic1));
	}

	@Test
	public void testSearchByStatus() {
		final DataCollectionDefinition dataCollectionDefinition = createDataCollectionDefinition(1L);

		final Installation installation = createInstallation("user", "password");
		final Organization organization = createOrganization(installation);

		RegistrationStatistic registrationStatistic1 = new RegistrationStatistic();
		registrationStatistic1.setOrganization(organization);
		registrationStatistic1.setDataCollectionDefinition(dataCollectionDefinition);
		registrationStatistic1.setStatus("NEW");
		registrationStatistic1.setCount(1L);
		registrationStatistic1 = registrationStatisticDao.create(registrationStatistic1);

		RegistrationStatistic registrationStatistic2 = new RegistrationStatistic();
		registrationStatistic2.setOrganization(organization);
		registrationStatistic2.setDataCollectionDefinition(dataCollectionDefinition);
		registrationStatistic2.setStatus("APPROVED");
		registrationStatistic2.setCount(1L);
		registrationStatistic2 = registrationStatisticDao.create(registrationStatistic2);

		List<RegistrationStatistic> all = registrationStatisticDao.getAll();
		Assert.assertEquals(2, all.size());

		RegistrationStatisticSearch search = new RegistrationStatisticSearch();
		search.setStatus("NEW");
		all = registrationStatisticDao.getAll(search);

		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(registrationStatistic1));
	}

	@Test
	public void testGetAllGroupByMajorVersion() {
		DataCollectionGroup test_1_v1 = createDataCollectionGroup(1L, "TEST_1", 1);
		DataCollectionGroup test_1_v2 = createDataCollectionGroup(2L, "TEST_1", 2);
		DataCollectionGroup test_2_v1 = createDataCollectionGroup(3L, "TEST_2", 1);
		DataCollectionGroup test_2_v2 = createDataCollectionGroup(4L, "TEST_2", 2);

		final DataCollectionDefinition dcdTest1_v1_0 = createDataCollectionDefinition(1L, "TEST_1", 0, test_1_v1);
		final DataCollectionDefinition dcdTest1_v1_1 = createDataCollectionDefinition(2L, "TEST_1", 1, test_1_v1);
		final DataCollectionDefinition dcdTest1_v2_0 = createDataCollectionDefinition(3L, "TEST_1", 0, test_1_v2);
		final DataCollectionDefinition dcdTest2_v1_0 = createDataCollectionDefinition(4L, "TEST_2", 0, test_2_v1);
		final DataCollectionDefinition dcdTest2_v1_1 = createDataCollectionDefinition(5L, "TEST_2", 1, test_2_v1);
		final DataCollectionDefinition dcdTest2_v2_0 = createDataCollectionDefinition(6L, "TEST_2", 0, test_2_v2);

		final Installation installation = createInstallation("user", "password");
		final Organization organization1 = createOrganization(installation, "11111111", "MyOrganization1");
		final Organization organization2 = createOrganization(installation, "22222222", "MyOrganization2");

		final RegistrationStatistic registrationStatistic1 = registrationStatisticDao.create(buildRegistrationStatistic(organization1, dcdTest1_v1_0, "STATUS_1", 1L));
		final RegistrationStatistic registrationStatistic2 = registrationStatisticDao.create(buildRegistrationStatistic(organization1, dcdTest1_v1_1, "STATUS_1", 2L));
		final RegistrationStatistic registrationStatistic3 = registrationStatisticDao.create(buildRegistrationStatistic(organization1, dcdTest1_v2_0, "STATUS_1", 3L));
		final RegistrationStatistic registrationStatistic4 = registrationStatisticDao.create(buildRegistrationStatistic(organization1, dcdTest2_v1_0, "STATUS_1", 4L));
		final RegistrationStatistic registrationStatistic5 = registrationStatisticDao.create(buildRegistrationStatistic(organization1, dcdTest2_v1_1, "STATUS_1", 5L));
		final RegistrationStatistic registrationStatistic6 = registrationStatisticDao.create(buildRegistrationStatistic(organization1, dcdTest2_v2_0, "STATUS_1", 6L));

		final RegistrationStatistic registrationStatistic7 = registrationStatisticDao.create(buildRegistrationStatistic(organization1, dcdTest1_v1_0, "STATUS_2", 7L));
		final RegistrationStatistic registrationStatistic8 = registrationStatisticDao.create(buildRegistrationStatistic(organization1, dcdTest1_v1_1, "STATUS_2", 8L));
		final RegistrationStatistic registrationStatistic9 = registrationStatisticDao.create(buildRegistrationStatistic(organization1, dcdTest1_v2_0, "STATUS_2", 9L));
		final RegistrationStatistic registrationStatistic10 = registrationStatisticDao.create(buildRegistrationStatistic(organization1, dcdTest2_v1_0, "STATUS_2", 10L));
		final RegistrationStatistic registrationStatistic11 = registrationStatisticDao.create(buildRegistrationStatistic(organization1, dcdTest2_v1_1, "STATUS_2", 11L));
		final RegistrationStatistic registrationStatistic12 = registrationStatisticDao.create(buildRegistrationStatistic(organization1, dcdTest2_v2_0, "STATUS_2", 12L));

		final RegistrationStatistic registrationStatistic13 = registrationStatisticDao.create(buildRegistrationStatistic(organization2, dcdTest1_v1_0, "STATUS_1", 13L));
		final RegistrationStatistic registrationStatistic14 = registrationStatisticDao.create(buildRegistrationStatistic(organization2, dcdTest1_v1_1, "STATUS_1", 14L));
		final RegistrationStatistic registrationStatistic15 = registrationStatisticDao.create(buildRegistrationStatistic(organization2, dcdTest1_v2_0, "STATUS_1", 15L));
		final RegistrationStatistic registrationStatistic16 = registrationStatisticDao.create(buildRegistrationStatistic(organization2, dcdTest2_v1_0, "STATUS_1", 16L));
		final RegistrationStatistic registrationStatistic17 = registrationStatisticDao.create(buildRegistrationStatistic(organization2, dcdTest2_v1_1, "STATUS_1", 17L));
		final RegistrationStatistic registrationStatistic18 = registrationStatisticDao.create(buildRegistrationStatistic(organization2, dcdTest2_v2_0, "STATUS_1", 18L));

		final RegistrationStatistic registrationStatistic19 = registrationStatisticDao.create(buildRegistrationStatistic(organization2, dcdTest1_v1_0, "STATUS_2", 19L));
		final RegistrationStatistic registrationStatistic20 = registrationStatisticDao.create(buildRegistrationStatistic(organization2, dcdTest1_v1_1, "STATUS_2", 20L));
		final RegistrationStatistic registrationStatistic21 = registrationStatisticDao.create(buildRegistrationStatistic(organization2, dcdTest1_v2_0, "STATUS_2", 21L));
		final RegistrationStatistic registrationStatistic22 = registrationStatisticDao.create(buildRegistrationStatistic(organization2, dcdTest2_v1_0, "STATUS_2", 22L));
		final RegistrationStatistic registrationStatistic23 = registrationStatisticDao.create(buildRegistrationStatistic(organization2, dcdTest2_v1_1, "STATUS_2", 23L));
		final RegistrationStatistic registrationStatistic24 = registrationStatisticDao.create(buildRegistrationStatistic(organization2, dcdTest2_v2_0, "STATUS_2", 24L));

		final List<GroupByRegistrationStatistic> all = registrationStatisticDao.getAllGroupBy();

		Assert.assertEquals(16, all.size());

		Assert.assertTrue(all.contains(new GroupByRegistrationStatistic("TEST_1", 1, "11111111", "MyOrganization1", "STATUS_1", 3L, registrationStatistic1.getUpdatedOn())));
		Assert.assertTrue(all.contains(new GroupByRegistrationStatistic("TEST_1", 2, "11111111", "MyOrganization1", "STATUS_1", 3L, registrationStatistic3.getUpdatedOn())));
		Assert.assertTrue(all.contains(new GroupByRegistrationStatistic("TEST_2", 1, "11111111", "MyOrganization1", "STATUS_1", 9L, registrationStatistic4.getUpdatedOn())));
		Assert.assertTrue(all.contains(new GroupByRegistrationStatistic("TEST_2", 2, "11111111", "MyOrganization1", "STATUS_1", 6L, registrationStatistic6.getUpdatedOn())));

		Assert.assertTrue(all.contains(new GroupByRegistrationStatistic("TEST_1", 1, "11111111", "MyOrganization1", "STATUS_2", 15L, registrationStatistic7.getUpdatedOn())));
		Assert.assertTrue(all.contains(new GroupByRegistrationStatistic("TEST_1", 2, "11111111", "MyOrganization1", "STATUS_2", 9L, registrationStatistic9.getUpdatedOn())));
		Assert.assertTrue(all.contains(new GroupByRegistrationStatistic("TEST_2", 1, "11111111", "MyOrganization1", "STATUS_2", 21L, registrationStatistic10.getUpdatedOn())));
		Assert.assertTrue(all.contains(new GroupByRegistrationStatistic("TEST_2", 2, "11111111", "MyOrganization1", "STATUS_2", 12L, registrationStatistic12.getUpdatedOn())));

		Assert.assertTrue(all.contains(new GroupByRegistrationStatistic("TEST_1", 1, "22222222", "MyOrganization2", "STATUS_1", 27L, registrationStatistic13.getUpdatedOn())));
		Assert.assertTrue(all.contains(new GroupByRegistrationStatistic("TEST_1", 2, "22222222", "MyOrganization2", "STATUS_1", 15L, registrationStatistic15.getUpdatedOn())));
		Assert.assertTrue(all.contains(new GroupByRegistrationStatistic("TEST_2", 1, "22222222", "MyOrganization2", "STATUS_1", 33L, registrationStatistic16.getUpdatedOn())));
		Assert.assertTrue(all.contains(new GroupByRegistrationStatistic("TEST_2", 2, "22222222", "MyOrganization2", "STATUS_1", 18L, registrationStatistic18.getUpdatedOn())));

		Assert.assertTrue(all.contains(new GroupByRegistrationStatistic("TEST_1", 1, "22222222", "MyOrganization2", "STATUS_2", 39L, registrationStatistic19.getUpdatedOn())));
		Assert.assertTrue(all.contains(new GroupByRegistrationStatistic("TEST_1", 2, "22222222", "MyOrganization2", "STATUS_2", 21L, registrationStatistic21.getUpdatedOn())));
		Assert.assertTrue(all.contains(new GroupByRegistrationStatistic("TEST_2", 1, "22222222", "MyOrganization2", "STATUS_2", 45L, registrationStatistic22.getUpdatedOn())));
		Assert.assertTrue(all.contains(new GroupByRegistrationStatistic("TEST_2", 2, "22222222", "MyOrganization2", "STATUS_2", 24L, registrationStatistic24.getUpdatedOn())));

		try {
			final String s = new ObjectMapper().writeValueAsString(all);
			System.out.println("s = " + s);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private RegistrationStatistic buildRegistrationStatistic(Organization organization, DataCollectionDefinition dcd, String status, long count) {
		final RegistrationStatistic registrationStatistic = new RegistrationStatistic();
		registrationStatistic.setOrganization(organization);
		registrationStatistic.setDataCollectionDefinition(dcd);
		registrationStatistic.setStatus(status);
		registrationStatistic.setCount(count);
		return registrationStatistic;
	}

	private Organization createOrganization(Installation installation) {
		return createOrganization(installation, "11111111", "MyOrganization");
	}

	private Organization createOrganization(Installation installation, String identificationValue, String name) {
		Organization organization = new Organization();
		organization.setInstallation(installation);
		organization.setIdentificationValue(identificationValue);
		organization.setName(name);
		organization.setMain(true);
		organization.setDeleted(false);
		return organizationDao.create(organization);
	}

	private Installation createInstallation(String username, String password) {
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setAuthorities(new HashSet<>(Arrays.asList(new Authority(Authority.INSTALLATION))));
		user = userDao.create(user);

		Installation installation = new Installation();
		installation.setUser(user);
		return installationDao.create(installation);
	}

	private DataCollectionDefinition createDataCollectionDefinition(Long id) {
		DataCollectionGroup group = createDataCollectionGroup(id, "TEST", 1);
		return createDataCollectionDefinition(id, "TEST", 0, group);
	}


	private DataCollectionGroup createDataCollectionGroup(Long id, String name, int majorVersion) {
		DataCollectionGroup dataCollectionGroup = new DataCollectionGroup();
		dataCollectionGroup.setId(id);
		dataCollectionGroup.setName(name);
		dataCollectionGroup.setMajorVersion(majorVersion);
		dataCollectionGroup.setLabel(new Label(name, name, name));
		dataCollectionGroup.setDescription(new Description(name, name, name));
		return dataCollectionGroupDao.create(dataCollectionGroup);
	}

	private DataCollectionDefinition createDataCollectionDefinition(Long id, String name, int minorVersion, DataCollectionGroup dataCollectionGroup) {
		DataCollectionDefinition dataCollectionDefinition = new DataCollectionDefinition();
		dataCollectionDefinition.setId(id);
		dataCollectionDefinition.setDataCollectionName(name);
		dataCollectionDefinition.setLabel(new Label(name, name, name));
		dataCollectionDefinition.setDescription(new Description(name, name, name));
		dataCollectionDefinition.setMinorVersion(minorVersion);
		dataCollectionDefinition.setDefinitionContent("content".getBytes());
		dataCollectionDefinition.setDataCollectionGroup(dataCollectionGroup);
		return dataCollectionDefinitionDao.create(dataCollectionDefinition);
	}
}
