/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.dao.impl;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import be.wiv_isp.healthdata.catalogue.standalone.dao.ITranslationDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Translation;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "/applicationContext-test-dao.xml" })
@Transactional
public class TranslationDaoTest {

	@Autowired
	private ITranslationDao dao;

	Translation translation1;
	Translation translation2;
	Translation translation3;
	Translation translation4;

	@Before
	public void setup() {

		translation1 = getTranslation("key1", "key1nl", "key1fr");
		translation2 = getTranslation("key2", "key2nl", "key2fr");
		translation3 = getTranslation("key3", "key3nl", "key3fr");
		translation4 = getTranslation("key4", "key4nl", "key4fr");

		dao.create(translation1);
		dao.create(translation2);
		dao.create(translation3);
		dao.create(translation4);
	}

	private Translation getTranslation(String key, String nl, String fr) {
		Translation translation = new Translation();
		translation.setKey(key);
		translation.setNl(nl);
		translation.setFr(fr);
		return translation;
	}

	@Test
	public void testGetAll() {
		List<Translation> translations = dao.getAll();
		Assert.assertEquals(4, translations.size());
		Assert.assertTrue(translations.contains(translation1));
		Assert.assertTrue(translations.contains(translation2));
		Assert.assertTrue(translations.contains(translation3));
		Assert.assertTrue(translations.contains(translation4));
	}
}
