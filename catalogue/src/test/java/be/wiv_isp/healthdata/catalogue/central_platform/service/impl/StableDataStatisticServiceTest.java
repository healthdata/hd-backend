/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.service.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.domain.Installation;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Organization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.StableDataStatistic;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IInstallationService;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IOrganizationService;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IStableDataStatisticService;
import be.wiv_isp.healthdata.catalogue.central_platform.dao.IInstallationDao;
import be.wiv_isp.healthdata.catalogue.central_platform.dao.IOrganizationDao;
import be.wiv_isp.healthdata.catalogue.central_platform.dao.IStableDataStatisticDao;
import be.wiv_isp.healthdata.catalogue.standalone.dao.IUserDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Authority;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.JsonObjectsByOrganization;
import be.wiv_isp.healthdata.catalogue.standalone.domain.User;
import be.wiv_isp.healthdata.catalogue.standalone.service.IUserService;
import be.wiv_isp.healthdata.catalogue.standalone.service.impl.UserService;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class StableDataStatisticServiceTest {
    @Autowired
    private IUserDao userDao;

    @Autowired
    private IInstallationDao installationDao;

    @Autowired
    private IStableDataStatisticDao stableDataStatisticDao;

    @Autowired
    private IOrganizationDao organizationDao;

    private IUserService userService = new UserService();
    private IInstallationService installationService = new InstallationService();
    private IStableDataStatisticService stableDataStatisticService = new StableDataStatisticService();
    private IOrganizationService organizationService = new OrganizationService();

    // mocks
    private PasswordEncoder passwordEncoder = EasyMock.createNiceMock(PasswordEncoder.class);

    @Before
    public void before() {
        ReflectionTestUtils.setField(userService, "userDao", userDao);
        ReflectionTestUtils.setField(userService, "passwordEncoder", passwordEncoder);
        ReflectionTestUtils.setField(installationService, "installationDao", installationDao);
        ReflectionTestUtils.setField(stableDataStatisticService, "dao", stableDataStatisticDao);
        ReflectionTestUtils.setField(stableDataStatisticService, "organizationService", organizationService);
        ReflectionTestUtils.setField(organizationService, "organizationDao", organizationDao);
    }

    @Test
    public void testUpdateRegistrationStatistic() throws Exception {
        EasyMock.expect(passwordEncoder.encode("pazz")).andReturn("pazz");
        EasyMock.replay(passwordEncoder);

        Installation installation = new Installation();
        installation.setUser(createUser());
        installation = installationService.create(installation);
        EasyMock.verify(passwordEncoder);

        Organization organization1 = createOrganization(installation, "11111111", "MyOrganization1", true, false);
        Organization organization2 = createOrganization(installation, "22222222", "MyOrganization2", true, false);
        organization1 = organizationDao.create(organization1);
        organization2 = organizationDao.create(organization2);
        installationService.updateOrganizations(installation, Arrays.asList(organization1, organization2));

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(createJsonObject("11111111", "TEST1", 1L, 1L));
        jsonArray.put(createJsonObject("11111111", "TEST2", 1L, 1L));
        jsonArray.put(createJsonObject("11111111", "TEST3", 1L, 1L));
        jsonArray.put(createJsonObject("22222222", "TEST1", 1L, 1L));
        jsonArray.put(createJsonObject("33333333", "TEST1", 1L, 1L)); // unexisting organization is ignored

        stableDataStatisticService.update(installation.getId(), new JsonObjectsByOrganization(jsonArray.toString()));

        List<StableDataStatistic> all = stableDataStatisticService.getAll();
        Assert.assertEquals(4, all.size());

        Assert.assertTrue(contains(all, createStableDataStatistic(organization1, "TEST1", 1L, 1L)));
        Assert.assertTrue(contains(all, createStableDataStatistic(organization1, "TEST2", 1L, 1L)));
        Assert.assertTrue(contains(all, createStableDataStatistic(organization1, "TEST3", 1L, 1L)));
        Assert.assertTrue(contains(all, createStableDataStatistic(organization2, "TEST1", 1L, 1L)));

        jsonArray = new JSONArray();
        jsonArray.put(createJsonObject("11111111", "TEST1", 1L, 1L));
        jsonArray.put(createJsonObject("11111111", "TEST2", 2L, 2L));
        jsonArray.put(createJsonObject("22222222", "TEST2", 1L, 1L));

        stableDataStatisticService.update(installation.getId(), new JsonObjectsByOrganization(jsonArray.toString()));

        all = stableDataStatisticService.getAll();
        Assert.assertEquals(3, all.size());

        Assert.assertTrue(contains(all, createStableDataStatistic(organization1, "TEST1", 1L, 1L)));
        Assert.assertTrue(contains(all, createStableDataStatistic(organization1, "TEST2", 2L, 2L)));
        Assert.assertTrue(contains(all, createStableDataStatistic(organization2, "TEST2", 1L, 1L)));
    }

    private JSONObject createJsonObject(String orgIdValue, String dataCollectionName, long count, long distinctPatientIdCount) throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("organizationIdentificationValue", orgIdValue);
        jsonObject.put("dataCollectionName", dataCollectionName);
        jsonObject.put("count", count);
        jsonObject.put("distinctPatientIdCount", distinctPatientIdCount);
        return jsonObject;
    }

    private StableDataStatistic createStableDataStatistic(Organization organization1, String dataCollectionName, long count, long distinctPatientIdCount) {
        final StableDataStatistic stableDataStatistic = new StableDataStatistic();
        stableDataStatistic.setOrganization(organization1);
        stableDataStatistic.setDataCollectionName(dataCollectionName);
        stableDataStatistic.setCount(count);
        stableDataStatistic.setDistinctPatientIdCount(distinctPatientIdCount);
        return stableDataStatistic;
    }

    private boolean contains(List<StableDataStatistic> list, StableDataStatistic sds) {
        for (final StableDataStatistic stableDataStatistic : list) {
            if(!sds.getOrganization().getId().equals(stableDataStatistic.getOrganization().getId())) {
                continue;
            }
            if(!sds.getDataCollectionName().equals(stableDataStatistic.getDataCollectionName())) {
                continue;
            }
            if(!sds.getCount().equals(stableDataStatistic.getCount())) {
                continue;
            }
            if(!sds.getDistinctPatientIdCount().equals(stableDataStatistic.getDistinctPatientIdCount())) {
                continue;
            }
            return true;
        }
        return false;
    }

    private Organization createOrganization(Installation installation, String identificationValue, String name, boolean main, boolean deleted) {
        final Organization organization = new Organization();
        organization.setInstallation(installation);
        organization.setIdentificationValue(identificationValue);
        organization.setName(name);
        organization.setMain(main);
        organization.setDeleted(deleted);
        return organization;
    }

    private User createUser() {
        final User user = new User();
        user.setUsername("user");
        user.setPassword("pazz");
        user.setAuthorities(new HashSet<>(Arrays.asList(new Authority(Authority.INSTALLATION))));
        return userService.create(user);
    }
}