/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.dao.impl;

import be.wiv_isp.healthdata.catalogue.standalone.dao.IDataCollectionDefinitionDao;
import be.wiv_isp.healthdata.catalogue.standalone.dao.IDataCollectionGroupDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionDefinition;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataCollectionGroup;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Description;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Label;
import be.wiv_isp.healthdata.catalogue.standalone.search.DataCollectionDefinitionSearch;
import be.wiv_isp.healthdata.common.domain.search.QueryResult;
import be.wiv_isp.healthdata.common.pagination.PaginationRange;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "/applicationContext-test-dao.xml" })
@Transactional
public class DataCollectionDefinitionDaoTest {

	private static final long NB_INSERTS = 10;

	private static int versionCounter = 1;

	@Autowired
	private IDataCollectionDefinitionDao dao;
	@Autowired
	private IDataCollectionGroupDao dataCollectionGroupDao;


	private final Map<Long, DataCollectionDefinition> insertedEntities = new HashMap<>();

	public void initialize() throws ParseException {
		for (int i = 1; i <= NB_INSERTS; i++) {
			DataCollectionDefinition dcd = createDataCollectionDefinition(i);
			insertedEntities.put(dcd.getId(), dcd);
		}
	}

	@Test
	public void testGet() throws Exception {
		initialize();
		for (long id : insertedEntities.keySet()) {
			DataCollectionDefinition dataCollectionDefinition = dao.get(id);
			Assert.assertTrue(dataCollectionDefinition.equals(insertedEntities.get(id)));
		}
	}

	@Test
	public void testFindByNameAndVersion() throws Exception {
		initialize();
		// check that all inserted entities can be retrieved using name and
		// version
		DataCollectionDefinitionSearch search;
		for (DataCollectionDefinition insertedEntity : insertedEntities.values()) {
			Integer minorVersion = insertedEntity.getMinorVersion();
			search = new DataCollectionDefinitionSearch();
			search.setDataCollectionName(insertedEntity.getDataCollectionName());
			search.setMinorVersion(minorVersion);

			List<DataCollectionDefinition> dataCollectionDefinitions = dao.getAll(search);

			Assert.assertTrue(dataCollectionDefinitions.iterator().next().equals(insertedEntity));
		}

		// non existing entity returns null
		search = new DataCollectionDefinitionSearch();
		search.setDataCollectionName("UT_DATA_COLLECTION_NAME_1");
		search.setMinorVersion(1);
		Assert.assertTrue(dao.getAll(search).isEmpty());
		search = new DataCollectionDefinitionSearch();
		search.setDataCollectionName("UT_DATA_COLLECTION_NAME_10");
		search.setMinorVersion(1);
		Assert.assertTrue(dao.getAll(search).isEmpty());
		search = new DataCollectionDefinitionSearch();
		search.setDataCollectionName("blah");
		search.setMinorVersion(99);
		Assert.assertTrue(dao.getAll(search).isEmpty());
	}

	@Test
	public void testLatest() throws Exception {
		long id = NB_INSERTS;
		Label label = new Label();
		label.setEn("label en");
		label.setFr("label fr");
		label.setNl("label nl");
		Description description = new Description();
		description.setEn("description en");
		description.setFr("description fr");
		description.setNl("description nl");

		DataCollectionGroup dataCollectionGroup1 = new DataCollectionGroup();
		dataCollectionGroup1.setId(id);
		dataCollectionGroup1.setName("LATEST");
		dataCollectionGroup1.setDescription(description);
		dataCollectionGroup1.setLabel(label);
		dataCollectionGroup1.setMajorVersion(1);
		dataCollectionGroup1.setParticipationContent(new byte[]{});
		dataCollectionGroupDao.create(dataCollectionGroup1);

		DataCollectionDefinition dataCollectionDefinition1 = new DataCollectionDefinition();
		dataCollectionDefinition1.setId(++id);
		dataCollectionDefinition1.setDataCollectionName("LATEST");
		dataCollectionDefinition1.setDefinitionContent("{}".getBytes(StandardCharsets.UTF_8));
		dataCollectionDefinition1.setMinorVersion(1);
		dataCollectionDefinition1.setLabel(label);
		dataCollectionDefinition1.setDescription(description);
		dataCollectionDefinition1.setDataCollectionGroup(dataCollectionGroup1);
		dao.create(dataCollectionDefinition1);

		Thread.sleep(1000);

		DataCollectionDefinition dataCollectionDefinition2 = new DataCollectionDefinition();
		dataCollectionDefinition2.setId(++id);
		dataCollectionDefinition2.setDataCollectionName("LATEST");
		dataCollectionDefinition2.setDefinitionContent("{}".getBytes(StandardCharsets.UTF_8));
		dataCollectionDefinition2.setMinorVersion(2);

		dataCollectionDefinition2.setLabel(label);
		dataCollectionDefinition2.setDescription(description);

		dataCollectionDefinition2.setDataCollectionGroup(dataCollectionGroup1);
		dao.create(dataCollectionDefinition2);

		Thread.sleep(1000);

		DataCollectionDefinition dataCollectionDefinition3 = new DataCollectionDefinition();
		dataCollectionDefinition3.setId(++id);
		dataCollectionDefinition3.setDataCollectionName("LATEST");
		dataCollectionDefinition3.setDefinitionContent("{}".getBytes(StandardCharsets.UTF_8));
		dataCollectionDefinition3.setMinorVersion(3);
		dataCollectionDefinition3.setLabel(label);
		dataCollectionDefinition3.setDescription(description);

		dataCollectionDefinition3.setDataCollectionGroup(dataCollectionGroup1);
		dao.create(dataCollectionDefinition3);

		final DataCollectionDefinitionSearch search = new DataCollectionDefinitionSearch();
		search.setDataCollectionName("LATEST");
		DataCollectionDefinition latest = dao.latest(search);
		Assert.assertNotNull(latest);
		Assert.assertEquals(latest, dataCollectionDefinition3);
	}

	@Test
	public void testLatestNoResult() throws Exception {
		final DataCollectionDefinitionSearch search = new DataCollectionDefinitionSearch();
		search.setDataCollectionName("LATEST");
		DataCollectionDefinition latest = dao.latest(search);

		Assert.assertNull(latest);
	}

	@Test
	public void testFindWithPagination() throws Exception {
		initialize();
		final PaginationRange range = new PaginationRange(1, 5);

		final DataCollectionDefinitionSearch search = new DataCollectionDefinitionSearch();
		search.setPaginationRange(range);

		final QueryResult<DataCollectionDefinition> queryResult = dao.getAllAsQueryResult(search);
		final List<DataCollectionDefinition> dataCollectionDefinitions = queryResult.getResult();
		verify(dataCollectionDefinitions, search, queryResult.getRange());
	}

	@Test
	public void testFindBySearchNoResults() throws Exception {
		initialize();
		DataCollectionDefinitionSearch search;
		List<DataCollectionDefinition> dataCollectionDefinitions;

		search = new DataCollectionDefinitionSearch();
		search.setDataCollectionName("non_existing_name");

		dataCollectionDefinitions = dao.getAll(search);
		Assert.assertTrue(dataCollectionDefinitions.isEmpty());

		search = new DataCollectionDefinitionSearch();
		search.setDataCollectionName("non_existing_description");

		dataCollectionDefinitions = dao.getAll(search);
		Assert.assertTrue(dataCollectionDefinitions.isEmpty());
	}

	/**
	 * Check that all entities are returned when providing a search with no
	 * constraints
	 */
	@Test
	public void testFindBySearchAllResults() throws Exception {
		initialize();
		final DataCollectionDefinitionSearch search = new DataCollectionDefinitionSearch();
		final QueryResult<DataCollectionDefinition> queryResult = dao.getAllAsQueryResult(search);
		final List<DataCollectionDefinition> dataCollectionDefinitions = queryResult.getResult();
		verify(dataCollectionDefinitions, null, queryResult.getRange());
	}

	@Test
	public void testGetNonExistingEntity() {
		DataCollectionDefinition nonExistingEntity = dao.get(-1L);

		Assert.assertNull(nonExistingEntity);
	}

	/**
	 * Creates and inserts a data collection definition in the database.
	 */
	private DataCollectionDefinition createDataCollectionDefinition(long count) {
		return createDataCollectionDefinition(count, null, null);
	}

	/**
	 * Creates and inserts a data collection definition in the database.
	 */
	private DataCollectionDefinition createDataCollectionDefinition(long count, Timestamp startDate, Timestamp endDateCreation) {
		startDate = (startDate == null) ? new Timestamp(new Date().getTime()) : startDate;
		endDateCreation = (endDateCreation == null) ? new Timestamp(new Date().getTime()) : endDateCreation;

		Label label = new Label();
		label.setEn("label en" + count);
		label.setFr("label fr" + count);
		label.setNl("label nl" + count);

		Description description = new Description();
		description.setEn("UT_DATA_COLLECTION_DESC_" + count);
		description.setFr("UT_DATA_COLLECTION_DESC_" + count);
		description.setNl("UT_DATA_COLLECTION_DESC_" + count);

		DataCollectionGroup dcg = new DataCollectionGroup();
		dcg.setId(count);
		dcg.setName("test");
		dcg.setDescription(description);
		dcg.setLabel(label);
		dcg.setStartDate(startDate);
		dcg.setMajorVersion(1);
		dcg.setParticipationContent(new byte[]{});

		dataCollectionGroupDao.create(dcg);

		final DataCollectionDefinition dcd = new DataCollectionDefinition();
		dcd.setId(count);
		dcd.setDefinitionContent("test".getBytes(StandardCharsets.UTF_8));
		dcd.setCreatedOn(new Timestamp(new Date().getTime()));
		dcd.setDataCollectionName("UT_DATA_COLLECTION_NAME_" + count);
		dcd.setMinorVersion(versionCounter++);
		dcd.setDataCollectionGroup(dcg);
		dcd.setLabel(label);
		dcd.setDescription(description);

		dcg.setParticipationContent(new byte[]{});
		dcd.setDataCollectionGroup(dcg);
		return dao.create(dcd);
	}

	/**
	 * Verify that the list in argument contains exactly all the entities that
	 * were inserted into the database.
	 */
	private void verify(List<DataCollectionDefinition> dataCollectionDefinitions, DataCollectionDefinitionSearch search, PaginationRange paginationRange) {
		long expectedSize;
		if (search != null && search.isPaginated()) {
			expectedSize = search.getPaginationRange().getTo() - search.getPaginationRange().getFrom() + 1;
			Assert.assertEquals(NB_INSERTS, paginationRange.getTotalCount());
		} else {
			expectedSize = NB_INSERTS;
		}
		Assert.assertEquals(expectedSize, dataCollectionDefinitions.size());

		int nbOfComparisons = 0;

		for (DataCollectionDefinition insertedEntity : insertedEntities.values()) {
			for (DataCollectionDefinition entityFromDb : dataCollectionDefinitions) {
				if (insertedEntity.getId().equals(entityFromDb.getId())) {
					++nbOfComparisons;
					Assert.assertTrue(insertedEntity.equals(entityFromDb));
				}
			}
		}

		Assert.assertEquals(expectedSize, nbOfComparisons);
	}

	private Timestamp createTimestamp(int year, int month, int day) {
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month - 1);
		cal.set(Calendar.DATE, day);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return new Timestamp(cal.getTimeInMillis());
	}
}
