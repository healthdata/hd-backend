/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.service.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.domain.Installation;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Organization;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IInstallationService;
import be.wiv_isp.healthdata.catalogue.central_platform.dao.IInstallationDao;
import be.wiv_isp.healthdata.catalogue.standalone.dao.IUserDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Authority;
import be.wiv_isp.healthdata.catalogue.standalone.domain.User;
import be.wiv_isp.healthdata.catalogue.standalone.service.IUserService;
import be.wiv_isp.healthdata.catalogue.standalone.service.impl.UserService;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class OrganizationServiceTest {

    @Autowired
    private IUserDao userDao;

    @Autowired
    private IInstallationDao installationDao;

    private IUserService userService = new UserService();
    private IInstallationService installationService = new InstallationService();

    // mocks
    private PasswordEncoder passwordEncoder = EasyMock.createNiceMock(PasswordEncoder.class);

    @Before
    public void before() {
        ReflectionTestUtils.setField(userService, "userDao", userDao);
        ReflectionTestUtils.setField(userService, "passwordEncoder", passwordEncoder);
        ReflectionTestUtils.setField(installationService, "installationDao", installationDao);
    }

    @Test
    public void testUpdateOrganizations() throws Exception {
        EasyMock.expect(passwordEncoder.encode("pazz")).andReturn("pazz");
        EasyMock.replay(passwordEncoder);

        final Installation installation = new Installation();
        installation.setUser(createUser());
        final List<Organization> organizations = new ArrayList<>();
        organizations.add(createOrganization("11111111", "MyOrganization1", true, false));
        organizations.add(createOrganization("22222222", "MyOrganization2", false, false));
        organizations.add(createOrganization("33333333", "MyOrganization3", false, false));
        for (final Organization organization : organizations) {
            organization.setInstallation(installation);
        }
        installation.setOrganizations(organizations);

        final Installation createdInstallation = installationService.create(installation);

        Assert.assertEquals(3, createdInstallation.getOrganizations().size());
        Assert.assertTrue(containsOrganization(createdInstallation.getOrganizations(), createOrganization("11111111", "MyOrganization1", true, false)));
        Assert.assertTrue(containsOrganization(createdInstallation.getOrganizations(), createOrganization("22222222", "MyOrganization2", false, false)));
        Assert.assertTrue(containsOrganization(createdInstallation.getOrganizations(), createOrganization("33333333", "MyOrganization3", false, false)));

        final List<Organization> newOrganizations = new ArrayList<>(); // organization 2: removed
        newOrganizations.add(createOrganization("11111111", "MyOrganizationNew1", true, false)); // organization 1: name modified
        newOrganizations.add(createOrganization("33333333", "MyOrganization3", false, true)); // organization 3: delete flag modifed
        newOrganizations.add(createOrganization("44444444", "MyOrganization4", false, false)); // organization 4: added
        newOrganizations.add(createOrganization("55555555", "MyOrganization5", false, false)); // organization 5: added
        final Installation updateInstallation = installationService.updateOrganizations(createdInstallation, newOrganizations);

        Assert.assertEquals(4, updateInstallation.getOrganizations().size());
        Assert.assertTrue(containsOrganization(updateInstallation.getOrganizations(), createOrganization("11111111", "MyOrganizationNew1", true, false)));
        Assert.assertTrue(containsOrganization(updateInstallation.getOrganizations(), createOrganization("33333333", "MyOrganization3", false, true)));
        Assert.assertTrue(containsOrganization(updateInstallation.getOrganizations(), createOrganization("44444444", "MyOrganization4", false, false)));
        Assert.assertTrue(containsOrganization(updateInstallation.getOrganizations(), createOrganization("55555555", "MyOrganization5", false, false)));

        EasyMock.verify(passwordEncoder);
    }

    private Organization createOrganization(String identificationValue, String name, boolean main, boolean deleted) {
        final Organization organization = new Organization();
        organization.setIdentificationValue(identificationValue);
        organization.setName(name);
        organization.setMain(main);
        organization.setDeleted(deleted);
        return organization;
    }

    private User createUser() {
        final User user = new User();
        user.setUsername("user");
        user.setPassword("pazz");
        user.setAuthorities(new HashSet<>(Arrays.asList(new Authority(Authority.INSTALLATION))));
        return userService.create(user);
    }

    private boolean containsOrganization(final List<Organization> organizations, final Organization organization) {
        for (final Organization o : organizations) {
            if(equalWithoutId(o, organization)) {
                return true;
            }
        }
        return false;
    }

    private boolean equalWithoutId(final Organization o1, final Organization o2) {
        if(!Objects.equals(o1.getName(), o2.getName())) {
            return false;
        }
        if(!Objects.equals(o1.getIdentificationValue(), o2.getIdentificationValue())) {
            return false;
        }
        if(!Objects.equals(o1.isMain(), o2.isMain())) {
            return false;
        }
        return Objects.equals(o1.isDeleted(), o2.isDeleted());
    }

}