/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.service.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IHd4dpUserDao;
import be.wiv_isp.healthdata.catalogue.central_platform.dao.IInstallationDao;
import be.wiv_isp.healthdata.catalogue.central_platform.dao.IOrganizationDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Hd4dpUser;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Installation;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.JsonObjectsByOrganization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Organization;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IHd4dpUserService;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IInstallationService;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IOrganizationService;
import be.wiv_isp.healthdata.catalogue.standalone.dao.IUserDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Authority;
import be.wiv_isp.healthdata.catalogue.standalone.domain.User;
import be.wiv_isp.healthdata.catalogue.standalone.service.IUserService;
import be.wiv_isp.healthdata.catalogue.standalone.service.impl.UserService;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class Hd4dpUserServiceTest {

    @Autowired
    private IUserDao userDao;

    @Autowired
    private IInstallationDao installationDao;

    @Autowired
    private IHd4dpUserDao hd4dpUserDao;

    @Autowired
    private IOrganizationDao organizationDao;

    private IUserService userService = new UserService();
    private IInstallationService installationService = new InstallationService();
    private IHd4dpUserService hd4dpUserService = new Hd4dpUserService();
    private IOrganizationService organizationService = new OrganizationService();

    // mocks
    private PasswordEncoder passwordEncoder = EasyMock.createNiceMock(PasswordEncoder.class);

    @Before
    public void before() {
        ReflectionTestUtils.setField(userService, "userDao", userDao);
        ReflectionTestUtils.setField(userService, "passwordEncoder", passwordEncoder);
        ReflectionTestUtils.setField(installationService, "installationDao", installationDao);
        ReflectionTestUtils.setField(hd4dpUserService, "dao", hd4dpUserDao);
        ReflectionTestUtils.setField(hd4dpUserService, "organizationService", organizationService);
        ReflectionTestUtils.setField(organizationService, "organizationDao", organizationDao);
    }

    @Test
    public void testUpdateRegistrationStatistic() throws Exception {
        EasyMock.expect(passwordEncoder.encode("pazz")).andReturn("pazz");
        EasyMock.replay(passwordEncoder);

        Installation installation = new Installation();
        installation.setUser(createUser());
        installation = installationService.create(installation);
        EasyMock.verify(passwordEncoder);

        Organization organization1 = createOrganization(installation, "11111111", "MyOrganization1", true, false);
        Organization organization2 = createOrganization(installation, "22222222", "MyOrganization2", true, false);
        Organization organization3 = createOrganization(installation, "33333333", "MyOrganization3", true, false);
        organization1 = organizationDao.create(organization1);
        organization2 = organizationDao.create(organization2);
        installationService.updateOrganizations(installation, Arrays.asList(organization1, organization2));

        Hd4dpUser hd4dpUser1 = createHd4dpUser(organization1, "user1", "lastname1", "firstname1", "email1", new HashSet<>(Collections.singletonList("authority1")), new HashSet<>(Collections.singletonList("dc1")), true, false);
        Hd4dpUser hd4dpUser2 = createHd4dpUser(organization2, "user2", "lastname2", "firstname2", "email2", new HashSet<>(Collections.singletonList("authority2")), new HashSet<>(Collections.singletonList("dc2")), true, false);
        Hd4dpUser hd4dpUser3 = createHd4dpUser(organization3, "user3", "lastname3", "firstname3", "email3", new HashSet<>(Collections.singletonList("authority3")), new HashSet<>(Collections.singletonList("dc3")), true, false);

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(createJsonObject(hd4dpUser1));
        jsonArray.put(createJsonObject(hd4dpUser2));
        jsonArray.put(createJsonObject(hd4dpUser3)); // hd4dpUser3 is ignored as organization with identification value "33333333" does not exist

        System.out.println("jsonArray = " + jsonArray);

        hd4dpUserService.update(installation.getId(), new JsonObjectsByOrganization(jsonArray.toString()));

        List<Hd4dpUser> all = hd4dpUserService.getAll();
        Assert.assertEquals(2, all.size());

        Assert.assertTrue(all.contains(hd4dpUser1));
        Assert.assertTrue(all.contains(hd4dpUser2));

        hd4dpUser1 = createHd4dpUser(organization1, "user1", "lastname1", "firstname1", "email-modified-1", new HashSet<>(Collections.singletonList("authority1")), new HashSet<>(Collections.singletonList("dc1")), true, false);

        jsonArray = new JSONArray();
        jsonArray.put(createJsonObject(hd4dpUser1));

        hd4dpUserService.update(installation.getId(), new JsonObjectsByOrganization(jsonArray.toString()));

        all = hd4dpUserService.getAll();
        Assert.assertEquals(1, all.size());

        Assert.assertTrue(all.contains(hd4dpUser1));
    }

    private JSONObject createJsonObject(Hd4dpUser user) throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("organizationIdentificationValue", user.getOrganization().getIdentificationValue());
        jsonObject.put("username", user.getUsername());
        jsonObject.put("lastName", user.getLastName());
        jsonObject.put("firstName", user.getFirstName());
        jsonObject.put("email", user.getEmail());
        jsonObject.put("authorities", user.getAuthorities());
        jsonObject.put("dataCollectionNames", user.getDataCollectionNames());
        jsonObject.put("enabled", user.isEnabled());
        jsonObject.put("ldapUser", user.isLdapUser());
        return jsonObject;
    }

    private Hd4dpUser createHd4dpUser(Organization organization, String username, String lastName, String firstName, String email, Set<String> authorities, Set<String> dataCollections, boolean enabled, boolean ldapUser) throws JSONException {
        final Hd4dpUser user = new Hd4dpUser();
        user.setUsername(username);
        user.setOrganization(organization);
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setEmail(email);
        user.setAuthorities(authorities);
        user.setDataCollectionNames(dataCollections);
        user.setEnabled(enabled);
        user.setLdapUser(ldapUser);
        return user;
    }

    private Organization createOrganization(Installation installation, String identificationValue, String name, boolean main, boolean deleted) {
        final Organization organization = new Organization();
        organization.setInstallation(installation);
        organization.setIdentificationValue(identificationValue);
        organization.setName(name);
        organization.setMain(main);
        organization.setDeleted(deleted);
        return organization;
    }

    private User createUser() {
        final User user = new User();
        user.setUsername("user");
        user.setPassword("pazz");
        user.setAuthorities(new HashSet<>(Collections.singletonList(new Authority(Authority.INSTALLATION))));
        return userService.create(user);
    }
}