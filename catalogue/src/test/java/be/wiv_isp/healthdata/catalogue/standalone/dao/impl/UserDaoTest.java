/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.dao.impl;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import be.wiv_isp.healthdata.catalogue.standalone.dao.IUserDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Authority;
import be.wiv_isp.healthdata.catalogue.standalone.domain.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class UserDaoTest {

	@Autowired
	private IUserDao dao;

	@Test
	public void testGet() {
		User user = createUser("username", "password", "ROLE_USER");
		user = dao.create(user);

		User refreshedUser = dao.get(user.getId());
		Assert.assertNotNull(refreshedUser);
		Assert.assertEquals(user, refreshedUser);
	}

	@Test
	public void testGet2() {
		User user = createUser("username", "password", "ROLE_USER");
		user = dao.create(user);

		User refreshedUser = dao.get(user.getUsername());
		Assert.assertNotNull(refreshedUser);
		Assert.assertEquals(user, refreshedUser);
	}

	@Test
	public void testGetNotAvailable() {
		User user = dao.get(-1L);
		Assert.assertNull(user);
	}

	@Test
	public void testCreate() {
		User user = createUser("username", "password", "ROLE_USER");
		user = dao.create(user);
		User createdUser = dao.create(user);
		Assert.assertNotNull(createdUser);
		Assert.assertEquals(user, createdUser);
	}

	@Test
	public void testUpdate() {
		User user = createUser("username", "password", "ROLE_USER");
		user = dao.create(user);
		user.setPassword("updated_password");
		User updatedUser = dao.update(user);
		Assert.assertNotNull(updatedUser);
		Assert.assertEquals(user, updatedUser);
	}

	private User createUser(String userName, String password, String role) {
		Authority authority = new Authority();
		authority.setAuthority(role);

		Set<Authority> authorities = new HashSet<>();
		authorities.add(authority);

		User user = new User();
		user.setUsername(userName);
		user.setPassword(password);
		user.setAuthorities(authorities);
		return user;
	}
}
