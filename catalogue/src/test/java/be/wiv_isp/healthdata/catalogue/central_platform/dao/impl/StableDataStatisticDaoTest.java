/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.central_platform.dao.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.domain.Installation;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Organization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.StableDataStatistic;
import be.wiv_isp.healthdata.catalogue.central_platform.dao.IInstallationDao;
import be.wiv_isp.healthdata.catalogue.central_platform.dao.IOrganizationDao;
import be.wiv_isp.healthdata.catalogue.central_platform.dao.IStableDataStatisticDao;
import be.wiv_isp.healthdata.catalogue.standalone.dao.IUserDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.*;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.StableDataStatisticSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class StableDataStatisticDaoTest {

	@Autowired
	private IUserDao userDao;

	@Autowired
	private IInstallationDao installationDao;

	@Autowired
	private IOrganizationDao organizationDao;

	@Autowired
	private IStableDataStatisticDao stableDataStatisticDao;

	@Test
	public void testSearchByOrganizationId() {
		final Installation installation = createInstallation("user", "password");
		final Organization organization1 = createOrganization(installation);
		final Organization organization2 = createOrganization(installation);
		Assert.assertNotEquals(organization1.getId(), organization2.getId());

		StableDataStatistic stableDataStatistic1 = new StableDataStatistic();
		stableDataStatistic1.setOrganization(organization1);
		stableDataStatistic1.setDataCollectionName("TEST");
		stableDataStatistic1.setCount(1L);
		stableDataStatistic1.setDistinctPatientIdCount(1L);
		stableDataStatistic1 = stableDataStatisticDao.create(stableDataStatistic1);

		StableDataStatistic stableDataStatistic2 = new StableDataStatistic();
		stableDataStatistic2.setOrganization(organization2);
		stableDataStatistic2.setDataCollectionName("TEST");
		stableDataStatistic2.setCount(1L);
		stableDataStatistic2.setDistinctPatientIdCount(1L);
		stableDataStatistic2 = stableDataStatisticDao.create(stableDataStatistic2);

		List<StableDataStatistic> all = stableDataStatisticDao.getAll();
		Assert.assertEquals(2, all.size());

		StableDataStatisticSearch search = new StableDataStatisticSearch();
		search.setOrganizationId(organization1.getId());
		all = stableDataStatisticDao.getAll(search);

		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(stableDataStatistic1));
	}

	@Test
	public void testSearchByDataCollectionName() {
		final Installation installation = createInstallation("user", "password");
		final Organization organization = createOrganization(installation);

		StableDataStatistic stableDataStatistic1 = new StableDataStatistic();
		stableDataStatistic1.setOrganization(organization);
		stableDataStatistic1.setDataCollectionName("TEST");
		stableDataStatistic1.setCount(1L);
		stableDataStatistic1.setDistinctPatientIdCount(1L);
		stableDataStatistic1 = stableDataStatisticDao.create(stableDataStatistic1);

		StableDataStatistic stableDataStatistic2 = new StableDataStatistic();
		stableDataStatistic2.setOrganization(organization);
		stableDataStatistic2.setDataCollectionName("TEST2");
		stableDataStatistic2.setCount(1L);
		stableDataStatistic2.setDistinctPatientIdCount(1L);
		stableDataStatistic2 = stableDataStatisticDao.create(stableDataStatistic2);

		List<StableDataStatistic> all = stableDataStatisticDao.getAll();
		Assert.assertEquals(2, all.size());

		StableDataStatisticSearch search = new StableDataStatisticSearch();
		search.setDataCollectionName("TEST");
		all = stableDataStatisticDao.getAll(search);

		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(stableDataStatistic1));
	}

	private Organization createOrganization(Installation installation) {
		Organization organization = new Organization();
		organization.setInstallation(installation);
		organization.setIdentificationValue("11111111");
		organization.setName("MyOrganization");
		organization.setMain(true);
		organization.setDeleted(false);
		return organizationDao.create(organization);
	}

	private Installation createInstallation(String username, String password) {
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setAuthorities(new HashSet<>(Arrays.asList(new Authority(Authority.INSTALLATION))));
		user = userDao.create(user);

		Installation installation = new Installation();
		installation.setUser(user);
		return installationDao.create(installation);
	}
}
