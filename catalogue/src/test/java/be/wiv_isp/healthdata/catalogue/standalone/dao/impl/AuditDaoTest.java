/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.dao.impl;

import be.wiv_isp.healthdata.catalogue.standalone.api.ApiType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import be.wiv_isp.healthdata.catalogue.standalone.dao.IAuditDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Audit;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class AuditDaoTest {

	Audit audit;

	@Autowired
	private IAuditDao auditDao;

	@Before
	public void setUp() throws Exception {
		audit = buildAudit();
		audit = auditDao.create(audit);
	}

	@Test
	public void testGet() throws Exception {
		final Audit returnedAudit = auditDao.get(audit.getId());
		Assert.assertNotNull(returnedAudit);
		Assert.assertEquals(audit.getId(), returnedAudit.getId());
		Assert.assertNotNull(audit.getTimeIn());
		Assert.assertNull(audit.getTimeOut());
	}

	@Test
	public void testUpdate() throws Exception {
		audit.setStatus("OK");
		auditDao.update(audit);

		final Audit returnedAudit = auditDao.get(audit.getId());
		Assert.assertNotNull(returnedAudit);
		Assert.assertEquals(audit.getId(), returnedAudit.getId());
		Assert.assertNotNull(audit.getTimeOut());
		Assert.assertEquals("OK", audit.getStatus());
	}

	@Test
	public void testDelete() throws Exception {
		auditDao.delete(audit);

		final Audit returnedAudit = auditDao.get(audit.getId());
		Assert.assertNull(returnedAudit);
	}

	private Audit buildAudit() {
		Audit audit = new Audit();
		audit.setApi(ApiType.DATA_COLLECTIONS.getDescription());
		audit.setMethod("GET");
		audit.setSubject("1");
		audit.setUserName("USERNAME");
		audit.setExceptionMessage("EXCEPTION_MESSAGE");
		return audit;
	}
}
