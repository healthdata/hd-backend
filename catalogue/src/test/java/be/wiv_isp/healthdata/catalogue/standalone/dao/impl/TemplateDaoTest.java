/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.dao.impl;

import be.wiv_isp.healthdata.catalogue.standalone.dao.ITemplateDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Template;
import be.wiv_isp.healthdata.common.domain.enumeration.TemplateKey;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "/applicationContext-test-dao.xml" })
@Transactional
public class TemplateDaoTest {

	@Autowired
	private ITemplateDao dao;

	Template template1;
	Template template2;
	Template template3;

	@Before
	public void setup() {

		template1 = getTemplate(TemplateKey.USER_REQUEST_NEW_FOR_ADMIN, "title1", "content1");
		template2 = getTemplate(TemplateKey.USER_REQUEST_NEW_FOR_REQUESTER, "title2", "content2");
		template3 = getTemplate(TemplateKey.USER_REQUEST_NEW_FOR_USER, "title3", "content3");

		dao.create(template1);
		dao.create(template2);
		dao.create(template3);
	}

	private Template getTemplate(TemplateKey templateKey, String title, String content) {
		Template template = new Template();
		template.setKey(templateKey.toString());
		template.setSubject(title);
		template.setTemplateText(content.getBytes(StandardCharsets.UTF_8));
		return template;
	}

	@Test
	public void testGetAll() {
		List<Template> templates = dao.getAll();
		Assert.assertEquals(3, templates.size());
		Assert.assertTrue(templates.contains(template1));
		Assert.assertTrue(templates.contains(template2));
		Assert.assertTrue(templates.contains(template3));
	}
	@Test
	public void testGet() {
		Template template = dao.get(TemplateKey.USER_REQUEST_NEW_FOR_ADMIN);
		Assert.assertEquals(template1, template);
	}
}
