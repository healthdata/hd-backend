/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.catalogue.central_platform.dao.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IExceptionReportDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.ExceptionReport;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.ExceptionReportSearch;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class ExceptionReportDaoTest {

    public static final String UUID1 = "11111111-1111-1111-1111-111111111111";
    public static final String UUID2 = "22222222-2222-2222-2222-222222222222";
    public static final String UUID3 = "33333333-3333-3333-3333-333333333333";
    public static final String UUID4 = "44444444-4444-4444-4444-444444444444";

    @Autowired
    private IExceptionReportDao dao;

    private ExceptionReport exceptionReport1;
    private ExceptionReport exceptionReport2;
    private ExceptionReport exceptionReport3;

    private String start1 = "2017-01-01T12:00:00+00:00";
    private String end1 = "2017-01-02T12:00:00+00:00";

    private String start2 = "2017-01-01T12:00:00+00:00";
    private String end2 = "2017-01-03T12:00:00+00:00";

    private String start3 = "2017-01-02T12:00:00+00:00";
    private String end3 = "2017-01-03T12:00:00+00:00";


    private void setup() throws Exception{
        SimpleDateFormat sdf = new SimpleDateFormat(DateFormat.DATE_AND_TIME.getPattern());


        exceptionReport1 = new ExceptionReport();
        exceptionReport1.setInstallationId(1L);
        exceptionReport1.setMessage("error1");
        exceptionReport1.setStackTrace("stackTrace1");
        Set<String> uuids1 = new HashSet<>();
        uuids1.add(UUID1);
        uuids1.add(UUID2);
        exceptionReport1.setUuids(uuids1);
        exceptionReport1.setStartTime(new Timestamp(sdf.parse(start1).getTime()));
        exceptionReport1.setEndTime(new Timestamp(sdf.parse(end1).getTime()));

        dao.create(exceptionReport1);


        exceptionReport2 = new ExceptionReport();
        exceptionReport2.setInstallationId(1L);
        exceptionReport2.setMessage("error1");
        Set<String> uuids2 = new HashSet<>();
        uuids2.add(UUID3);
        exceptionReport2.setUuids(uuids2);
        exceptionReport2.setStartTime(new Timestamp(sdf.parse(start2).getTime()));
        exceptionReport2.setEndTime(new Timestamp(sdf.parse(end2).getTime()));

        dao.create(exceptionReport2);


        exceptionReport3 = new ExceptionReport();
        exceptionReport3.setInstallationId(2L);
        exceptionReport3.setMessage("error3");
        Set<String> uuids3 = new HashSet<>();
        uuids3.add(UUID4);
        exceptionReport3.setUuids(uuids3);
        exceptionReport3.setStartTime(new Timestamp(sdf.parse(start3).getTime()));
        exceptionReport3.setEndTime(new Timestamp(sdf.parse(end3).getTime()));

        dao.create(exceptionReport3);
    }

    @Test
    public void testSearch() throws Exception {
        setup();

        ExceptionReportSearch search = new ExceptionReportSearch();
        List<ExceptionReport> all = dao.getAll(search);
        Assert.assertEquals(3, all.size());
        Assert.assertTrue(all.contains(exceptionReport1));
        Assert.assertTrue(all.contains(exceptionReport2));
        Assert.assertTrue(all.contains(exceptionReport3));

        search = new ExceptionReportSearch();
        search.setInstallationId(1L);
        all = dao.getAll(search);
        Assert.assertEquals(2, all.size());
        Assert.assertTrue(all.contains(exceptionReport1));
        Assert.assertTrue(all.contains(exceptionReport2));

        search = new ExceptionReportSearch();
        search.setReportDate("2017-01-01");
        all = dao.getAll(search);
        Assert.assertEquals(2, all.size());
        Assert.assertTrue(all.contains(exceptionReport1));
        Assert.assertTrue(all.contains(exceptionReport2));

        search = new ExceptionReportSearch();
        search.setReportDate("2017-01-02");
        all = dao.getAll(search);
        Assert.assertTrue(all.contains(exceptionReport1));
        Assert.assertTrue(all.contains(exceptionReport2));
        Assert.assertTrue(all.contains(exceptionReport3));
        Assert.assertEquals(3, all.size());

        search = new ExceptionReportSearch();
        search.setReportDate("2017-01-03");
        all = dao.getAll(search);
        Assert.assertEquals(2, all.size());
        Assert.assertTrue(all.contains(exceptionReport2));
        Assert.assertTrue(all.contains(exceptionReport3));
    }

}
