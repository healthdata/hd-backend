/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.service.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IInstallationDao;
import be.wiv_isp.healthdata.catalogue.central_platform.dao.IOrganizationDao;
import be.wiv_isp.healthdata.catalogue.central_platform.dao.IRegistrationStatisticDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Installation;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.JsonObjectsByOrganization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Organization;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.RegistrationStatistic;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IInstallationService;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IOrganizationService;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IRegistrationStatisticService;
import be.wiv_isp.healthdata.catalogue.standalone.dao.IDataCollectionDefinitionDao;
import be.wiv_isp.healthdata.catalogue.standalone.dao.IDataCollectionGroupDao;
import be.wiv_isp.healthdata.catalogue.standalone.dao.IUserDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.*;
import be.wiv_isp.healthdata.catalogue.standalone.service.IDataCollectionDefinitionService;
import be.wiv_isp.healthdata.catalogue.standalone.service.IDataCollectionGroupService;
import be.wiv_isp.healthdata.catalogue.standalone.service.IUserService;
import be.wiv_isp.healthdata.catalogue.standalone.service.impl.DataCollectionDefinitionService;
import be.wiv_isp.healthdata.catalogue.standalone.service.impl.DataCollectionGroupService;
import be.wiv_isp.healthdata.catalogue.standalone.service.impl.UserService;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class RegistrationStatisticServiceTest {
    @Autowired
    private IUserDao userDao;
    @Autowired
    private IInstallationDao installationDao;
    @Autowired
    private IRegistrationStatisticDao registrationStatisticDao;
    @Autowired
    private IDataCollectionDefinitionDao dataCollectionDefinitionDao;
    @Autowired
    private IDataCollectionGroupDao dataCollectionGroupDao;

    @Autowired
    private IOrganizationDao organizationDao;

    private IUserService userService = new UserService();
    private IInstallationService installationService = new InstallationService();
    private IRegistrationStatisticService registrationStatisticService = new RegistrationStatisticService();
    private IDataCollectionDefinitionService dataCollectionDefinitionService = new DataCollectionDefinitionService();
    private IDataCollectionGroupService dataCollectionGroupService = new DataCollectionGroupService();
    private IOrganizationService organizationService = new OrganizationService();

    // mocks
    private PasswordEncoder passwordEncoder = EasyMock.createNiceMock(PasswordEncoder.class);

    @Before
    public void before() {
        ReflectionTestUtils.setField(userService, "userDao", userDao);
        ReflectionTestUtils.setField(userService, "passwordEncoder", passwordEncoder);
        ReflectionTestUtils.setField(installationService, "installationDao", installationDao);
        ReflectionTestUtils.setField(registrationStatisticService, "registrationStatisticDao", registrationStatisticDao);
        ReflectionTestUtils.setField(registrationStatisticService, "organizationService", organizationService);
        ReflectionTestUtils.setField(registrationStatisticService, "dataCollectionDefinitionService", dataCollectionDefinitionService);
        ReflectionTestUtils.setField(dataCollectionDefinitionService, "dao", dataCollectionDefinitionDao);
        ReflectionTestUtils.setField(dataCollectionGroupService, "dao", dataCollectionGroupDao);
        ReflectionTestUtils.setField(organizationService, "organizationDao", organizationDao);
    }

    @Test
    public void testUpdateRegistrationStatistic() throws Exception {
        EasyMock.expect(passwordEncoder.encode("pazz")).andReturn("pazz");
        EasyMock.replay(passwordEncoder);

        Installation installation = new Installation();
        installation.setUser(createUser());
        installation = installationService.create(installation);
        EasyMock.verify(passwordEncoder);

        Organization organization1 = createOrganization(installation, "11111111", "MyOrganization1", true, false);
        Organization organization2 = createOrganization(installation, "22222222", "MyOrganization2", true, false);
        organization1 = organizationDao.create(organization1);
        organization2 = organizationDao.create(organization2);
        installationService.updateOrganizations(installation, Arrays.asList(organization1, organization2));

        final DataCollectionDefinition dataCollectionDefinition1 = createDataCollectionDefinition(1L);
        final DataCollectionDefinition dataCollectionDefinition2 = createDataCollectionDefinition(2L);

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(createJsonObject("11111111", 1L, "NEW", 1L));
        jsonArray.put(createJsonObject("11111111", 1L, "ACCEPTED", 2L));
        jsonArray.put(createJsonObject("11111111", 2L, "NEW", 3L));
        jsonArray.put(createJsonObject("22222222", 1L, "NEW", 4L));
        jsonArray.put(createJsonObject("33333333", 1L, "NEW", 1L)); // unexisting organization is ignored
        jsonArray.put(createJsonObject("11111111", 3L, "NEW", 1L)); // unexisting data collection definition is ignored

        registrationStatisticService.update(installation.getId(), new JsonObjectsByOrganization(jsonArray.toString()));

        List<RegistrationStatistic> all = registrationStatisticService.getAll();
        Assert.assertEquals(4, all.size());

        Assert.assertTrue(contains(all, createRegistrationStatistic(organization1, dataCollectionDefinition1, "NEW", 1L)));
        Assert.assertTrue(contains(all, createRegistrationStatistic(organization1, dataCollectionDefinition1, "ACCEPTED", 2L)));
        Assert.assertTrue(contains(all, createRegistrationStatistic(organization1, dataCollectionDefinition2, "NEW", 3L)));
        Assert.assertTrue(contains(all, createRegistrationStatistic(organization2, dataCollectionDefinition1, "NEW", 4L)));

        jsonArray = new JSONArray();
        jsonArray.put(createJsonObject("11111111", 1L, "NEW", 2L));
        jsonArray.put(createJsonObject("11111111", 1L, "ACCEPTED", 1L));
        jsonArray.put(createJsonObject("22222222", 1L, "REJECTED", 1L));
        jsonArray.put(createJsonObject("22222222", 1L, "NEW", 4L));

        registrationStatisticService.update(installation.getId(), new JsonObjectsByOrganization(jsonArray.toString()));

        all = registrationStatisticService.getAll();
        Assert.assertEquals(4, all.size());

        Assert.assertTrue(contains(all, createRegistrationStatistic(organization1, dataCollectionDefinition1, "NEW", 2L)));
        Assert.assertTrue(contains(all, createRegistrationStatistic(organization1, dataCollectionDefinition1, "ACCEPTED", 1L)));
        Assert.assertTrue(contains(all, createRegistrationStatistic(organization2, dataCollectionDefinition1, "REJECTED", 1L)));
        Assert.assertTrue(contains(all, createRegistrationStatistic(organization2, dataCollectionDefinition1, "NEW", 4L)));
    }

    private JSONObject createJsonObject(String orgIdValue, long dcdId, String status, long count) throws JSONException {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("organizationIdentificationValue", orgIdValue);
        jsonObject.put("dataCollectionDefinitionId", dcdId);
        jsonObject.put("status", status);
        jsonObject.put("count", count);
        return jsonObject;
    }

    private RegistrationStatistic createRegistrationStatistic(Organization organization1, DataCollectionDefinition dataCollectionDefinition1, String status, long count) {
        final RegistrationStatistic expectedRegistrationStatistic = new RegistrationStatistic();
        expectedRegistrationStatistic.setOrganization(organization1);
        expectedRegistrationStatistic.setDataCollectionDefinition(dataCollectionDefinition1);
        expectedRegistrationStatistic.setStatus(status);
        expectedRegistrationStatistic.setCount(count);
        return expectedRegistrationStatistic;
    }

    private boolean contains(List<RegistrationStatistic> list, RegistrationStatistic rc) {
        for (final RegistrationStatistic registrationStatistic : list) {
            if(!rc.getOrganization().getId().equals(registrationStatistic.getOrganization().getId())) {
                continue;
            }
            if(!rc.getDataCollectionDefinition().getId().equals(registrationStatistic.getDataCollectionDefinition().getId())) {
                continue;
            }
            if(!rc.getStatus().equals(registrationStatistic.getStatus())) {
                continue;
            }
            if(!rc.getCount().equals(registrationStatistic.getCount())) {
                continue;
            }
            return true;
        }
        return false;
    }

    private DataCollectionDefinition createDataCollectionDefinition(long dataCollectionDefinitionId) {
        DataCollectionGroup dataCollectionGroup = new DataCollectionGroup();
        dataCollectionGroup.setId(dataCollectionDefinitionId);
        dataCollectionGroup.setName("TEST");
        dataCollectionGroup.setMajorVersion(1);
        dataCollectionGroup.setLabel(new Label("TEST", "TEST", "TEST"));
        dataCollectionGroup.setDescription(new Description("TEST", "TEST", "TEST"));
        dataCollectionGroupService.create(dataCollectionGroup);


        DataCollectionDefinition dataCollectionDefinition = new DataCollectionDefinition();
        dataCollectionDefinition.setId(dataCollectionDefinitionId);
        dataCollectionDefinition.setDataCollectionName("TEST");
        dataCollectionDefinition.setLabel(new Label("TEST", "TEST", "TEST"));
        dataCollectionDefinition.setDescription(new Description("TEST", "TEST", "TEST"));
        dataCollectionDefinition.setMinorVersion(0);
        dataCollectionDefinition.setDefinitionContent("content".getBytes());
        dataCollectionDefinition.setDataCollectionGroup(dataCollectionGroup);
        dataCollectionDefinition = dataCollectionDefinitionService.create(dataCollectionDefinition);
        return dataCollectionDefinition;
    }

    private Organization createOrganization(Installation installation, String identificationValue, String name, boolean main, boolean deleted) {
        final Organization organization = new Organization();
        organization.setInstallation(installation);
        organization.setIdentificationValue(identificationValue);
        organization.setName(name);
        organization.setMain(main);
        organization.setDeleted(deleted);
        return organization;
    }

    private User createUser() {
        final User user = new User();
        user.setUsername("user");
        user.setPassword("pazz");
        user.setAuthorities(new HashSet<>(Arrays.asList(new Authority(Authority.INSTALLATION))));
        return userService.create(user);
    }
}