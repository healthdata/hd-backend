/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class NumericStringComparatorTest {

	@Test
	public void numericStartTest() {

		Assert.assertEquals("", NumericStringComparator.getNumericStart(null));
		Assert.assertEquals("", NumericStringComparator.getNumericStart(""));
		Assert.assertEquals("1", NumericStringComparator.getNumericStart("1"));
		Assert.assertEquals("1", NumericStringComparator.getNumericStart("1a1"));
		Assert.assertEquals("100", NumericStringComparator.getNumericStart("100 - 100"));
		Assert.assertEquals("", NumericStringComparator.getNumericStart("a9"));
		Assert.assertEquals("", NumericStringComparator.getNumericStart("�9"));
		Assert.assertEquals("", NumericStringComparator.getNumericStart("0009"));

	}

	@Test
	public void sortTest() {
		List<String> toSort = new ArrayList<String>();
		toSort.add(null);
		toSort.add("");
		toSort.add("1");
		toSort.add("1a1");
		toSort.add("100 - 100");
		toSort.add("a9");
		toSort.add("�9");
		toSort.add("000009");

		Collections.sort(toSort, new NumericStringComparator());

		Assert.assertEquals(null, toSort.get(0));
		Assert.assertEquals("", toSort.get(1));
		Assert.assertEquals("1", toSort.get(2));
		Assert.assertEquals("1a1", toSort.get(3));
		Assert.assertEquals("100 - 100", toSort.get(4));
		Assert.assertEquals("000009", toSort.get(5));
		Assert.assertEquals("a9", toSort.get(6));
		Assert.assertEquals("�9", toSort.get(7));
	}
}
