/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.central_platform.dao.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IInstallationDao;
import be.wiv_isp.healthdata.catalogue.central_platform.dao.IOrganizationDao;
import be.wiv_isp.healthdata.catalogue.standalone.dao.IUserDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Authority;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Installation;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Organization;
import be.wiv_isp.healthdata.catalogue.standalone.domain.User;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.OrganizationSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class OrganizationDaoTest {

	@Autowired
	private IUserDao userDao;

	@Autowired
	private IInstallationDao installationDao;

	@Autowired
	private IOrganizationDao organizationDao;

	@Test
	public void testSearchByInstallationId() {
		Installation installation1 = createInstallation("user1", "password");
		Installation installation2 = createInstallation("user2", "password");
		Assert.assertNotEquals(installation1.getId(), installation2.getId());

		Organization organization1 = new Organization();
		organization1.setInstallation(installation1);
		organization1.setIdentificationValue("11111111");
		organization1.setName("MyOrganization");
		organization1.setMain(true);
		organization1.setDeleted(false);
		organization1 = organizationDao.create(organization1);

		Organization organization2 = new Organization();
		organization2.setInstallation(installation2);
		organization2.setIdentificationValue("11111111");
		organization2.setName("MyOrganization");
		organization2.setMain(true);
		organization2.setDeleted(false);
		organization2 = organizationDao.create(organization2);

		List<Organization> all = organizationDao.getAll();
		Assert.assertEquals(2, all.size());

		final OrganizationSearch search = new OrganizationSearch();
		search.setInstallationId(installation1.getId());
		all = organizationDao.getAll(search);

		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(organization1));
	}

	@Test
	public void testSearchByIdentificationValue() {
		Installation installation = createInstallation("user1", "password");

		Organization organization1 = new Organization();
		organization1.setInstallation(installation);
		organization1.setIdentificationValue("11111111");
		organization1.setName("MyOrganization");
		organization1.setMain(true);
		organization1.setDeleted(false);
		organization1 = organizationDao.create(organization1);

		Organization organization2 = new Organization();
		organization2.setInstallation(installation);
		organization2.setIdentificationValue("22222222");
		organization2.setName("MyOrganization");
		organization2.setMain(true);
		organization2.setDeleted(false);
		organization2 = organizationDao.create(organization2);

		List<Organization> all = organizationDao.getAll();
		Assert.assertEquals(2, all.size());

		final OrganizationSearch search = new OrganizationSearch();
		search.setIdentificationValue("11111111");
		all = organizationDao.getAll(search);

		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(organization1));
	}

	private Installation createInstallation(String username, String password) {
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setAuthorities(new HashSet<>(Arrays.asList(new Authority(Authority.INSTALLATION))));
		user = userDao.create(user);

		Installation installation = new Installation();
		installation.setUser(user);
		return installationDao.create(installation);
	}
}
