/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.central_platform.dao.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IInstallationDao;
import be.wiv_isp.healthdata.catalogue.standalone.dao.IUserDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Authority;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.Installation;
import be.wiv_isp.healthdata.catalogue.standalone.domain.User;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.InstallationSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class InstallationDaoTest {

	@Autowired
	private IUserDao userDao;

	@Autowired
	private IInstallationDao installationDao;

	@Test
	public void testSearchByUserId() {
		User user1 = new User();
		user1.setUsername("user1");
		user1.setPassword("password");
		user1.setAuthorities(new HashSet<>(Arrays.asList(new Authority(Authority.INSTALLATION))));
		user1 = userDao.create(user1);

		User user2 = new User();
		user2.setUsername("user2");
		user2.setPassword("password");
		user2.setAuthorities(new HashSet<>(Arrays.asList(new Authority(Authority.INSTALLATION))));
		user2 = userDao.create(user2);

		Assert.assertNotEquals(user1.getId(), user2.getId());

		Installation installation1 = new Installation();
		installation1.setUser(user1);
		installation1 = installationDao.create(installation1);

		Installation installation2 = new Installation();
		installation2.setUser(user2);
		installation2 = installationDao.create(installation2);

		List<Installation> all = installationDao.getAll();
		Assert.assertEquals(2, all.size());

		InstallationSearch search = new InstallationSearch();
		search.setUserId(user1.getId());
		all = installationDao.getAll(search);

		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(installation1));
	}
}
