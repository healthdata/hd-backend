/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.dao.impl;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import be.wiv_isp.healthdata.catalogue.standalone.dao.IDataReferenceDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.DataReference;
import be.wiv_isp.healthdata.catalogue.standalone.enumeration.SearchFilterType;
import be.wiv_isp.healthdata.catalogue.standalone.search.DataReferenceSearch;
import be.wiv_isp.healthdata.catalogue.standalone.search.builder.DataReferenceSearchBuilder;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "/applicationContext-test-dao.xml" })
@Transactional
public class DataReferenceDaoTest {

	@Autowired
	private IDataReferenceDao dao;

	DataReference dataReference1NL;
	DataReference dataReference2NL;
	DataReference dataReference3NL;
	DataReference dataReference4NL;
	DataReference dataReference5NL;
	DataReference dataReference1FR;
	DataReference dataReference2FR;
	DataReference dataReference3FR;
	DataReference dataReference4FR;
	DataReference dataReference5FR;

	@Before
	public void setup() {
		dataReference1NL = new DataReference();
		dataReference1NL.setType("ZIP");
		dataReference1NL.setCode("testCode1");
		dataReference1NL.setValue("testValue1NL");
		dataReference1NL.setLanguage("NL");

		dao.create(dataReference1NL);

		dataReference1FR = new DataReference();
		dataReference1FR.setType("ZIP");
		dataReference1FR.setCode("testCode1");
		dataReference1FR.setValue("testValue1FR");
		dataReference1FR.setLanguage("FR");

		dao.create(dataReference1FR);

		dataReference2NL = new DataReference();
		dataReference2NL.setType("ZIP");
		dataReference2NL.setCode("testCode2");
		dataReference2NL.setValue("testValue2NL");
		dataReference2NL.setLanguage("NL");
		dataReference2NL.setValidTill(createTimestamp(2014, 1, 1));

		dao.create(dataReference2NL);

		dataReference2FR = new DataReference();
		dataReference2FR.setType("ZIP");
		dataReference2FR.setCode("testCode2");
		dataReference2FR.setValue("testValue2FR");
		dataReference2FR.setLanguage("FR");
		dataReference2FR.setValidTill(createTimestamp(2014, 1, 1));

		dao.create(dataReference2FR);

		dataReference3NL = new DataReference();
		dataReference3NL.setType("ZIP");
		dataReference3NL.setCode("testCode3");
		dataReference3NL.setValue("testValue3NL");
		dataReference3NL.setLanguage("NL");
		dataReference3NL.setValidFrom(createTimestamp(2014, 1, 1));

		dao.create(dataReference3NL);

		dataReference3FR = new DataReference();
		dataReference3FR.setType("ZIP");
		dataReference3FR.setCode("testCode3");
		dataReference3FR.setValue("testValue3FR");
		dataReference3FR.setLanguage("FR");
		dataReference3FR.setValidFrom(createTimestamp(2014, 1, 1));

		dao.create(dataReference3FR);

		dataReference4NL = new DataReference();
		dataReference4NL.setType("ZIP");
		dataReference4NL.setCode("testCode4");
		dataReference4NL.setValue("testValue4NL");
		dataReference4NL.setLanguage("NL");
		dataReference4NL.setValidFrom(createTimestamp(2014, 1, 1));
		dataReference4NL.setValidTill(createTimestamp(2015, 1, 1));

		dao.create(dataReference4NL);

		dataReference4FR = new DataReference();
		dataReference4FR.setType("ZIP");
		dataReference4FR.setCode("testCode4");
		dataReference4FR.setValue("testValue4FR");
		dataReference4FR.setLanguage("FR");
		dataReference4FR.setValidFrom(createTimestamp(2014, 1, 1));
		dataReference4FR.setValidTill(createTimestamp(2015, 1, 1));

		dao.create(dataReference4FR);

		dataReference5NL = new DataReference();
		dataReference5NL.setType("ZIP");
		dataReference5NL.setCode("testCode5");
		dataReference5NL.setParentCode("testCode4");
		dataReference5NL.setValue("testValue5NL");
		dataReference5NL.setLanguage("NL");

		dao.create(dataReference5NL);

		dataReference5FR = new DataReference();
		dataReference5FR.setType("ZIP");
		dataReference5FR.setCode("testCode5");
		dataReference5FR.setParentCode("testCode4");
		dataReference5FR.setValue("testValue5FR");
		dataReference5FR.setLanguage("FR");

		dao.create(dataReference5FR);
	}

	@Test
	public void testGetAll() {
		List<DataReference> dataReferences = dao.getAll();
		Assert.assertEquals(10, dataReferences.size());
		Assert.assertTrue(dataReferences.contains(dataReference1NL));
		Assert.assertTrue(dataReferences.contains(dataReference2NL));
		Assert.assertTrue(dataReferences.contains(dataReference3NL));
		Assert.assertTrue(dataReferences.contains(dataReference4NL));
		Assert.assertTrue(dataReferences.contains(dataReference5NL));
		Assert.assertTrue(dataReferences.contains(dataReference1FR));
		Assert.assertTrue(dataReferences.contains(dataReference2FR));
		Assert.assertTrue(dataReferences.contains(dataReference3FR));
		Assert.assertTrue(dataReferences.contains(dataReference4FR));
		Assert.assertTrue(dataReferences.contains(dataReference5FR));
	}

	@Test
	public void testSearchType() {
		DataReferenceSearch search = new DataReferenceSearchBuilder() //
				.withType("ZIP") //
				.withValidOn(null) //
				.build();
		List<DataReference> dataReferences = dao.getAll(search);
		Assert.assertEquals(10, dataReferences.size());
		Assert.assertTrue(dataReferences.contains(dataReference1NL));
		Assert.assertTrue(dataReferences.contains(dataReference2NL));
		Assert.assertTrue(dataReferences.contains(dataReference3NL));
		Assert.assertTrue(dataReferences.contains(dataReference4NL));
		Assert.assertTrue(dataReferences.contains(dataReference5NL));
		Assert.assertTrue(dataReferences.contains(dataReference1FR));
		Assert.assertTrue(dataReferences.contains(dataReference2FR));
		Assert.assertTrue(dataReferences.contains(dataReference3FR));
		Assert.assertTrue(dataReferences.contains(dataReference4FR));
		Assert.assertTrue(dataReferences.contains(dataReference5FR));
	}

	@Test
	public void testSearchCode1() {
		DataReferenceSearch search = new DataReferenceSearchBuilder() //
				.withCode("testCode1") //
				.withValidOn(null) //
				.build();
		List<DataReference> dataReferences = dao.getAll(search);
		Assert.assertEquals(2, dataReferences.size());
		Assert.assertTrue(dataReferences.contains(dataReference1NL));
		Assert.assertTrue(dataReferences.contains(dataReference1FR));
	}

	@Test
	public void testSearchCode2() {
		DataReferenceSearch search = new DataReferenceSearchBuilder() //
				.withCode("1") //
				.withValidOn(null) //
				.build();
		List<DataReference> dataReferences = dao.getAll(search);
		Assert.assertEquals(0, dataReferences.size());
	}

	@Test
	public void testSearchParentCode1() {
		DataReferenceSearch search = new DataReferenceSearchBuilder() //
				.withParentCode("testCode4") //
				.withValidOn(null) //
				.build();
		List<DataReference> dataReferences = dao.getAll(search);
		Assert.assertEquals(2, dataReferences.size());
		Assert.assertTrue(dataReferences.contains(dataReference5NL));
		Assert.assertTrue(dataReferences.contains(dataReference5NL));
	}

	@Test
	public void testSearchValue1() {
		DataReferenceSearch search = new DataReferenceSearchBuilder() //
				.withValue("testValue1NL") //
				.withValidOn(null) //
				.build();
		List<DataReference> dataReferences = dao.getAll(search);
		Assert.assertEquals(1, dataReferences.size());
		Assert.assertTrue(dataReferences.contains(dataReference1NL));
	}

	@Test
	public void testSearchValue2() {
		DataReferenceSearch search = new DataReferenceSearchBuilder() //
				.withValue("1") //
				.withValidOn(null) //
				.build();
		List<DataReference> dataReferences = dao.getAll(search);
		Assert.assertEquals(2, dataReferences.size());
		Assert.assertTrue(dataReferences.contains(dataReference1NL));
		Assert.assertTrue(dataReferences.contains(dataReference1FR));
	}

	@Test
	public void testSearchValue3() {
		DataReferenceSearch search = new DataReferenceSearchBuilder() //
				.withValue("value") //
				.withValidOn(null) //
				.build();
		List<DataReference> dataReferences = dao.getAll(search);
		Assert.assertEquals(10, dataReferences.size());
		Assert.assertTrue(dataReferences.contains(dataReference1NL));
		Assert.assertTrue(dataReferences.contains(dataReference2NL));
		Assert.assertTrue(dataReferences.contains(dataReference3NL));
		Assert.assertTrue(dataReferences.contains(dataReference4NL));
		Assert.assertTrue(dataReferences.contains(dataReference5NL));
		Assert.assertTrue(dataReferences.contains(dataReference1FR));
		Assert.assertTrue(dataReferences.contains(dataReference2FR));
		Assert.assertTrue(dataReferences.contains(dataReference3FR));
		Assert.assertTrue(dataReferences.contains(dataReference4FR));
		Assert.assertTrue(dataReferences.contains(dataReference5FR));
	}

	@Test
	public void testSearchLanguage1() {
		DataReferenceSearch search = new DataReferenceSearchBuilder() //
				.withLanguage("NL") //
				.withValidOn(null) //
				.build();
		List<DataReference> dataReferences = dao.getAll(search);
		Assert.assertEquals(5, dataReferences.size());
		Assert.assertTrue(dataReferences.contains(dataReference1NL));
		Assert.assertTrue(dataReferences.contains(dataReference2NL));
		Assert.assertTrue(dataReferences.contains(dataReference3NL));
		Assert.assertTrue(dataReferences.contains(dataReference4NL));
		Assert.assertTrue(dataReferences.contains(dataReference5NL));
	}

	@Test
	public void testSearchLanguage2() {
		DataReferenceSearch search = new DataReferenceSearchBuilder() //
				.withLanguage("N") //
				.withValidOn(null) //
				.build();
		List<DataReference> dataReferences = dao.getAll(search);
		Assert.assertEquals(0, dataReferences.size());
	}

	@Test
	public void testSearchValidOn1() {
		DataReferenceSearch search = new DataReferenceSearchBuilder() //
				.withValidOn(2013, 1, 1) //
				.build();
		List<DataReference> dataReferences = dao.getAll(search);
		Assert.assertEquals(6, dataReferences.size());
		Assert.assertTrue(dataReferences.contains(dataReference1NL));
		Assert.assertTrue(dataReferences.contains(dataReference2NL));
		Assert.assertTrue(dataReferences.contains(dataReference5NL));
		Assert.assertTrue(dataReferences.contains(dataReference1FR));
		Assert.assertTrue(dataReferences.contains(dataReference2FR));
		Assert.assertTrue(dataReferences.contains(dataReference5FR));
	}

	@Test
	public void testSearchValidOn2() {
		DataReferenceSearch search = new DataReferenceSearchBuilder() //
				.withValidOn(2016, 1, 1) //
				.build();
		List<DataReference> dataReferences = dao.getAll(search);
		Assert.assertEquals(6, dataReferences.size());
		Assert.assertTrue(dataReferences.contains(dataReference1NL));
		Assert.assertTrue(dataReferences.contains(dataReference3NL));
		Assert.assertTrue(dataReferences.contains(dataReference5NL));
		Assert.assertTrue(dataReferences.contains(dataReference1FR));
		Assert.assertTrue(dataReferences.contains(dataReference3FR));
		Assert.assertTrue(dataReferences.contains(dataReference5FR));
	}

	@Test
	public void testSearchValidOn3() {
		DataReferenceSearch search = new DataReferenceSearchBuilder() //
				.withValidOn(2014, 6, 30) //
				.build();
		List<DataReference> dataReferences = dao.getAll(search);
		Assert.assertEquals(8, dataReferences.size());
		Assert.assertTrue(dataReferences.contains(dataReference1NL));
		Assert.assertTrue(dataReferences.contains(dataReference3NL));
		Assert.assertTrue(dataReferences.contains(dataReference4NL));
		Assert.assertTrue(dataReferences.contains(dataReference5NL));
		Assert.assertTrue(dataReferences.contains(dataReference1FR));
		Assert.assertTrue(dataReferences.contains(dataReference3FR));
		Assert.assertTrue(dataReferences.contains(dataReference4FR));
		Assert.assertTrue(dataReferences.contains(dataReference5FR));
	}

	@Test
	public void testSearchValidOnAndLanguage() {
		DataReferenceSearch search = new DataReferenceSearchBuilder() //
				.withLanguage("NL") //
				.withValidOn(2014, 6, 30) //
				.build();
		List<DataReference> dataReferences = dao.getAll(search);
		Assert.assertEquals(4, dataReferences.size());
		Assert.assertTrue(dataReferences.contains(dataReference1NL));
		Assert.assertTrue(dataReferences.contains(dataReference3NL));
		Assert.assertTrue(dataReferences.contains(dataReference4NL));
		Assert.assertTrue(dataReferences.contains(dataReference5NL));
	}

	@Test
	public void testSearchValueEnd1() {
		DataReferenceSearch search = new DataReferenceSearchBuilder() //
				.withFilter(SearchFilterType.END) //
				.withValue("FR") //
				.withValidOn(null) //
				.build();
		List<DataReference> dataReferences = dao.getAll(search);
		Assert.assertEquals(5, dataReferences.size());
		Assert.assertTrue(dataReferences.contains(dataReference1FR));
		Assert.assertTrue(dataReferences.contains(dataReference2FR));
		Assert.assertTrue(dataReferences.contains(dataReference3FR));
		Assert.assertTrue(dataReferences.contains(dataReference4FR));
		Assert.assertTrue(dataReferences.contains(dataReference5FR));
	}

	@Test
	public void testSearchValueEnd2() {
		DataReferenceSearch search = new DataReferenceSearchBuilder() //
				.withFilter(SearchFilterType.END) //
				.withValue("Value") //
				.withValidOn(null) //
				.build();
		List<DataReference> dataReferences = dao.getAll(search);
		Assert.assertEquals(0, dataReferences.size());
	}

	@Test
	public void testSearchValueStart1() {
		DataReferenceSearch search = new DataReferenceSearchBuilder() //
				.withFilter(SearchFilterType.START) //
				.withValue("test") //
				.withValidOn(null) //
				.build();
		List<DataReference> dataReferences = dao.getAll(search);
		Assert.assertEquals(10, dataReferences.size());
		Assert.assertTrue(dataReferences.contains(dataReference1NL));
		Assert.assertTrue(dataReferences.contains(dataReference2NL));
		Assert.assertTrue(dataReferences.contains(dataReference3NL));
		Assert.assertTrue(dataReferences.contains(dataReference4NL));
		Assert.assertTrue(dataReferences.contains(dataReference5NL));
		Assert.assertTrue(dataReferences.contains(dataReference1FR));
		Assert.assertTrue(dataReferences.contains(dataReference2FR));
		Assert.assertTrue(dataReferences.contains(dataReference3FR));
		Assert.assertTrue(dataReferences.contains(dataReference4FR));
		Assert.assertTrue(dataReferences.contains(dataReference5FR));
	}

	@Test
	public void testSearchValueStart2() {
		DataReferenceSearch search = new DataReferenceSearchBuilder() //
				.withFilter(SearchFilterType.START) //
				.withValue("Value") //
				.withValidOn(null) //
				.build();
		List<DataReference> dataReferences = dao.getAll(search);
		Assert.assertEquals(0, dataReferences.size());
	}

	private Timestamp createTimestamp(int year, int month, int day) {
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month - 1);
		cal.set(Calendar.DATE, day);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return new Timestamp(cal.getTimeInMillis());
	}
}
