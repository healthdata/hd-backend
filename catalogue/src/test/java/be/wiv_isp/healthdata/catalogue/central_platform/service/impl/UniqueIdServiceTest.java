/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.central_platform.service.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IUniqueIdDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.UniqueId;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IUniqueIdService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UniqueIdServiceTest {

	private static final Logger LOG = LoggerFactory.getLogger(UniqueIdServiceTest.class);

	@Autowired
	private IUniqueIdDao dao;

	@Autowired
	@Qualifier("transactionManager")
	protected PlatformTransactionManager txManager;

	private IUniqueIdService uniqueIdService = new UniqueIdService();

	@Before
	public void before() {
		ReflectionTestUtils.setField(uniqueIdService, "dao", dao);
		ReflectionTestUtils.setField(uniqueIdService, "txManager", txManager);
	}

	@Test
	public void testGetNext() {
		UniqueId uniqueId = uniqueIdService.getNext("key_1");
		Assert.assertEquals(new UniqueId("key_1", 1L), uniqueId);

		uniqueId = uniqueIdService.getNext("key_1");
		Assert.assertEquals(new UniqueId("key_1", 2L), uniqueId);

		uniqueId = uniqueIdService.getNext("key_2");
		Assert.assertEquals(new UniqueId("key_2", 1L), uniqueId);
	}

	@Test
	public void testGetNextWithHigherLoad() throws InterruptedException {
		final int nbInserts = 100;

		final List<Thread> threads = new ArrayList<>();

		LOG.info("initializing threads");
		for (int i = 0; i < nbInserts; i++) {
			threads.add(new Thread(new Runnable() {
				@Override
				public void run() {
					uniqueIdService.getNext("key_1");
				}
			}));

			threads.add(new Thread(new Runnable() {
				@Override
				public void run() {
					uniqueIdService.getNext("key_2");
				}
			}));
		}

		LOG.info("starting threads");
		for (Thread thread : threads) {
			thread.start();
		}

		LOG.info("waiting for all threads to terminate");
		while (!allTerminated(threads)) {
			Thread.sleep(1000);
		}

		Assert.assertEquals(2*nbInserts, dao.getAll().size());
	}

	private boolean allTerminated(List<Thread> threads) {
		for (final Thread t : threads) {
			if (!Thread.State.TERMINATED.equals(t.getState())) {
				return false;
			}
		}
		return true;
	}
}
