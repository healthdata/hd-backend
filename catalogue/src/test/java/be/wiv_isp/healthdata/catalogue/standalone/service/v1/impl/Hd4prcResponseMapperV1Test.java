/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.service.v1.impl;

import be.wiv_isp.healthdata.catalogue.standalone.domain.*;
import be.wiv_isp.healthdata.catalogue.standalone.dto.DataCollection;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class Hd4prcResponseMapperV1Test {

    @Test
    public void testConvert() throws Exception {
        List<DataCollection> toConvert = new ArrayList<>();
        toConvert.add(createDataCollectionDefinition(1L, "TEST1", 1, 3, "test", "Test register"));
        toConvert.add(createDataCollectionDefinition(2L, "TEST1", 2, 0, "test", "Test register"));

        String converted = Hd4prcResponseMapperV1.convert(toConvert, getTaxonomy());

        Assert.assertEquals("[{\"id\":{\"name\":\"TEST1\",\"version\":\"1.3.0\"},\"labels\":{\"en\":\"Test register_EN\",\"fr\":\"Test register_FR\",\"nl\":\"Test register_NL\"},\"descriptions\":{\"en\":\"Test register_EN\",\"fr\":\"Test register_FR\",\"nl\":\"Test register_NL\"},\"functionalConfiguration\":{\"documentType\":\"LETTER\",\"lifespan\":\"SEND_ONE_TIME\",\"editable\":false,\"containsRecipients\":true,\"canModifySender\":false,\"canModifyRecipients\":false,\"canModifyAttachments\":false,\"supportsDataSet\":true,\"supportsExport\":false},\"fixedRecipient\":{\"identifier\":\"1990002213\",\"quality\":\"INSTITUTION_EHP\",\"name\":\"Healthdata\"},\"exportConfiguration\":{\"documentFormats\":[\"A4\"],\"defaultDocumentFormat\":\"A4\",\"formats\":[\"adr\",\"pdf\",\"json\"],\"dataSetFormat\":\"json\"},\"taxonomy\":\"node1.node11.node111,node2.node21.node211\"},{\"id\":{\"name\":\"TEST1\",\"version\":\"2.0.0\"},\"labels\":{\"en\":\"Test register_EN\",\"fr\":\"Test register_FR\",\"nl\":\"Test register_NL\"},\"descriptions\":{\"en\":\"Test register_EN\",\"fr\":\"Test register_FR\",\"nl\":\"Test register_NL\"},\"functionalConfiguration\":{\"documentType\":\"LETTER\",\"lifespan\":\"SEND_ONE_TIME\",\"editable\":false,\"containsRecipients\":true,\"canModifySender\":false,\"canModifyRecipients\":false,\"canModifyAttachments\":false,\"supportsDataSet\":true,\"supportsExport\":false},\"fixedRecipient\":{\"identifier\":\"1990002213\",\"quality\":\"INSTITUTION_EHP\",\"name\":\"Healthdata\"},\"exportConfiguration\":{\"documentFormats\":[\"A4\"],\"defaultDocumentFormat\":\"A4\",\"formats\":[\"adr\",\"pdf\",\"json\"],\"dataSetFormat\":\"json\"},\"taxonomy\":\"node1.node11.node111,node2.node21.node211\"}]", converted);
    }

    @Test
    public void testConvertEmpty() throws Exception {
        List<DataCollection> toConvert = new ArrayList<>();

        String converted = Hd4prcResponseMapperV1.convert(toConvert, getTaxonomy());

        Assert.assertEquals("[]", converted);
    }


    private DataCollection createDataCollectionDefinition(Long id, String name, int majorVersion, int minorVersion, String labelValue, String descriptionValue) {
        DataCollectionGroup group = new DataCollectionGroup();
        group.setName(name);
        group.setMajorVersion(majorVersion);

        DataCollectionDefinition dataCollectionDefinition = new DataCollectionDefinition();
        dataCollectionDefinition.setId(id);
        dataCollectionDefinition.setDataCollectionName(name);
        dataCollectionDefinition.setMinorVersion(minorVersion);
        dataCollectionDefinition.seteFormsVersion(0);
        Label label = new Label();
        label.setEn(labelValue + "_EN");
        label.setFr(labelValue + "_FR");
        label.setNl(labelValue + "_NL");
        dataCollectionDefinition.setLabel(label);
        Description description = new Description();
        description.setEn(descriptionValue + "_EN");
        description.setFr(descriptionValue + "_FR");
        description.setNl(descriptionValue + "_NL");
        dataCollectionDefinition.setDescription(description);
        return new DataCollection(group, dataCollectionDefinition);
    }

    public static Map<String, Set<String>> getTaxonomy() {
        Map<String, Set<String>> taxonomies = new HashMap<>();



        Set<String> taxonomyTEST1 = new TreeSet<>();
        taxonomyTEST1.add("node1.node11.node111");
        taxonomyTEST1.add("node2.node21.node211");
        taxonomies.put("TEST1", taxonomyTEST1);

        Set<String> taxonomyTEST2 = new TreeSet<>();
        taxonomyTEST2.add("node1.node11.node111");
        taxonomyTEST2.add("node2.node21.node211");
        taxonomies.put("TEST2", taxonomyTEST2);

        Set<String> taxonomyTEST3 = new TreeSet<>();
        taxonomyTEST3.add("node1.node11.node112");
        taxonomies.put("TEST3", taxonomyTEST3);

        Set<String> taxonomyTEST4 = new TreeSet<>();
        taxonomyTEST4.add("node1.node12");
        taxonomies.put("TEST4", taxonomyTEST4);

        Set<String> taxonomyTEST5 = new TreeSet<>();
        taxonomyTEST5.add("node2.node21");
        taxonomies.put("TEST5", taxonomyTEST5);

        return taxonomies;
    }
}
