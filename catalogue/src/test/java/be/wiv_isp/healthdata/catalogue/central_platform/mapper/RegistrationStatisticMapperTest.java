/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.catalogue.central_platform.mapper;

import be.wiv_isp.healthdata.catalogue.central_platform.domain.mapper.RegistrationStatisticMapper;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.GroupByRegistrationStatistic;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.json.SerializableJsonObject;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class RegistrationStatisticMapperTest {

    @Test
    public void testMap() throws IOException, JSONException {
        final String input = IOUtils.toString(RegistrationStatisticMapperTest.class.getClassLoader().getResourceAsStream("registrationStatisticMapper/input.json"), StandardCharsets.UTF_8);

        final JSONArray array = new JSONArray(input);
        final List<GroupByRegistrationStatistic> all = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            final JSONObject jsonObject = array.getJSONObject(i);
            final GroupByRegistrationStatistic groupByRegistrationStatistic = new GroupByRegistrationStatistic();
            groupByRegistrationStatistic.setDataCollectionName(jsonObject.getString("dataCollectionName"));
            groupByRegistrationStatistic.setMajorVersion(jsonObject.getInt("majorVersion"));
            groupByRegistrationStatistic.setHealthDataIdentificationValue(jsonObject.getString("healthDataIdentificationValue"));
            groupByRegistrationStatistic.setHealthDataIdentificationName(jsonObject.getString("healthDataIdentificationName"));
            groupByRegistrationStatistic.setStatus(jsonObject.getString("status"));
            groupByRegistrationStatistic.setCount(jsonObject.getLong("count"));
            groupByRegistrationStatistic.setMinUpdatedOn(DateFormat.DATE_AND_TIME.parse(jsonObject.getString("minUpdatedOn")));
            all.add(groupByRegistrationStatistic);
        }

        // method to test
        final SerializableJsonObject json = RegistrationStatisticMapper.map(all);

        String expectedOutput = IOUtils.toString(RegistrationStatisticMapperTest.class.getClassLoader().getResourceAsStream("registrationStatisticMapper/expectedOutput.json"), StandardCharsets.UTF_8);
        // update date string representations in expected output to take the default timezone into account (Jenkins)
        for (int i = 1; i <= 31; i++) {
            final String toReplace = "[" + i + "_january_2017" + "]";
            final Date date = DateFormat.DATE_AND_TIME.parse(MessageFormat.format("2017-01-{0}T12:00:00+01:00", String.format("%02d", i)));
            final String replaceWith = DateFormat.DATE_AND_TIME.format(date);
            expectedOutput = expectedOutput.replace(toReplace, replaceWith);
        }
        
        final Map map1 = new ObjectMapper().readValue(json.toString(), Map.class);
        final Map map2 = new ObjectMapper().readValue(expectedOutput, Map.class);
        Assert.assertEquals(map1, map2);
    }

}
