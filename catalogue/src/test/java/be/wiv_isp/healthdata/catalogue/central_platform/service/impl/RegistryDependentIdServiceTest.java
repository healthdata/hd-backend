/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.central_platform.service.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IUniqueIdDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.RegistryDependentIdGenerationInput;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IRegistryDependentIdService;
import be.wiv_isp.healthdata.catalogue.central_platform.service.IUniqueIdService;
import be.wiv_isp.healthdata.catalogue.standalone.service.ISalesForceService;
import be.wiv_isp.healthdata.common.domain.RegistryDependentIdGenerationDefinition;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.PlatformTransactionManager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class RegistryDependentIdServiceTest {

	@Autowired
	private IUniqueIdDao uniqueIdDao;

	@Autowired
	@Qualifier("transactionManager")
	protected PlatformTransactionManager txManager;

	private IUniqueIdService uniqueIdService = new UniqueIdService();
	private IRegistryDependentIdService registryDependentIdService = new RegistryDependentIdService();
	private ISalesForceService salesForceService = EasyMock.createNiceMock(ISalesForceService.class);

	@Before
	public void before() {
		ReflectionTestUtils.setField(uniqueIdService, "dao", uniqueIdDao);
		ReflectionTestUtils.setField(uniqueIdService, "txManager", txManager);

		ReflectionTestUtils.setField(registryDependentIdService, "uniqueIdService", uniqueIdService);
		ReflectionTestUtils.setField(registryDependentIdService, "salesForceService", salesForceService);
	}

	@Test
	public void testGenerateType1() {
		final RegistryDependentIdGenerationDefinition def = new RegistryDependentIdGenerationDefinition();
		def.setGenerationEnabled(true);
		def.setPhase(RegistryDependentIdGenerationDefinition.Phase.CREATION);
		def.setType(RegistryDependentIdGenerationDefinition.Type.TYPE_1);

		EasyMock.expect(salesForceService.getRegistryDependentIdGenerationDefinition("TEST")).andReturn(def).anyTimes();
		EasyMock.expect(salesForceService.getRegistryCode("TEST")).andReturn("123").anyTimes();
		EasyMock.expect(salesForceService.getRegistryDependentIdGenerationDefinition("TEST_2")).andReturn(def).anyTimes();
		EasyMock.expect(salesForceService.getRegistryCode("TEST_2")).andReturn("456").anyTimes();
		EasyMock.replay(salesForceService);

		final RegistryDependentIdGenerationInput extraParameters = new RegistryDependentIdGenerationInput();

		extraParameters.setSubmissionDate(DateFormat.DATE.parse("2017-06-01"));
		Assert.assertEquals("123.17.000001.44", registryDependentIdService.getRegistryDependentId("TEST", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));
		Assert.assertEquals("123.17.000002.45", registryDependentIdService.getRegistryDependentId("TEST", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));
		Assert.assertEquals("456.17.000001.51", registryDependentIdService.getRegistryDependentId("TEST_2", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));
		Assert.assertEquals("456.17.000002.52", registryDependentIdService.getRegistryDependentId("TEST_2", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));
		Assert.assertEquals("123.17.000003.46", registryDependentIdService.getRegistryDependentId("TEST", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));
		Assert.assertEquals("456.17.000003.53", registryDependentIdService.getRegistryDependentId("TEST_2", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));

		extraParameters.setSubmissionDate(DateFormat.DATE.parse("2018-06-01"));
		Assert.assertEquals("123.18.000001.71", registryDependentIdService.getRegistryDependentId("TEST", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));
		Assert.assertEquals("123.18.000002.72", registryDependentIdService.getRegistryDependentId("TEST", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));
		Assert.assertEquals("456.18.000001.78", registryDependentIdService.getRegistryDependentId("TEST_2", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));
		Assert.assertEquals("456.18.000002.79", registryDependentIdService.getRegistryDependentId("TEST_2", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));
		Assert.assertEquals("123.18.000003.73", registryDependentIdService.getRegistryDependentId("TEST", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));
		Assert.assertEquals("456.18.000003.80", registryDependentIdService.getRegistryDependentId("TEST_2", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));

		EasyMock.verify(salesForceService);
	}

	@Test
	public void testGenerateType2() {
		final RegistryDependentIdGenerationDefinition def = new RegistryDependentIdGenerationDefinition();
		def.setGenerationEnabled(true);
		def.setPhase(RegistryDependentIdGenerationDefinition.Phase.CREATION);
		def.setType(RegistryDependentIdGenerationDefinition.Type.TYPE_2);

		EasyMock.expect(salesForceService.getRegistryDependentIdGenerationDefinition("TEST")).andReturn(def).anyTimes();
		EasyMock.expect(salesForceService.getRegistryDependentIdGenerationDefinition("TEST_2")).andReturn(def).anyTimes();
		EasyMock.replay(salesForceService);

		final RegistryDependentIdGenerationInput extraParameters = new RegistryDependentIdGenerationInput();

		extraParameters.setOrganizationIdentificationValue("12345678");
		Assert.assertEquals("456.0001", registryDependentIdService.getRegistryDependentId("TEST", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));
		Assert.assertEquals("456.0002", registryDependentIdService.getRegistryDependentId("TEST", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));
		Assert.assertEquals("456.0001", registryDependentIdService.getRegistryDependentId("TEST_2", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));
		Assert.assertEquals("456.0002", registryDependentIdService.getRegistryDependentId("TEST_2", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));
		Assert.assertEquals("456.0003", registryDependentIdService.getRegistryDependentId("TEST", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));
		Assert.assertEquals("456.0003", registryDependentIdService.getRegistryDependentId("TEST_2", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));

		extraParameters.setOrganizationIdentificationValue("87654321");
		Assert.assertEquals("543.0001", registryDependentIdService.getRegistryDependentId("TEST", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));
		Assert.assertEquals("543.0002", registryDependentIdService.getRegistryDependentId("TEST", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));
		Assert.assertEquals("543.0001", registryDependentIdService.getRegistryDependentId("TEST_2", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));
		Assert.assertEquals("543.0002", registryDependentIdService.getRegistryDependentId("TEST_2", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));
		Assert.assertEquals("543.0003", registryDependentIdService.getRegistryDependentId("TEST", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));
		Assert.assertEquals("543.0003", registryDependentIdService.getRegistryDependentId("TEST_2", RegistryDependentIdGenerationDefinition.Phase.CREATION, extraParameters));

		EasyMock.verify(salesForceService);
	}

}
