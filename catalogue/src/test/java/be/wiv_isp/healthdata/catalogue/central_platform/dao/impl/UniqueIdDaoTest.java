/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.central_platform.dao.impl;

import be.wiv_isp.healthdata.catalogue.central_platform.dao.IUniqueIdDao;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.UniqueId;
import be.wiv_isp.healthdata.catalogue.central_platform.domain.search.UniqueIdSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class UniqueIdDaoTest {

	@Autowired
	private IUniqueIdDao dao;

	@Test
	public void testCreate() {
		final UniqueId uniqueId = new UniqueId("key", 1L);
		final UniqueId created = dao.create(uniqueId);
		Assert.assertNotNull(created);
		Assert.assertEquals(uniqueId, created);
	}

	@Test
	public void testSearchByKey() {
		final UniqueId uniqueId1 = new UniqueId("key_1", 1L);
		final UniqueId created1 = dao.create(uniqueId1);

		final UniqueId uniqueId2 = new UniqueId("key_2", 1L);
		final UniqueId created2 = dao.create(uniqueId2);

		Assert.assertEquals(2, dao.getAll().size());

		final UniqueIdSearch search = new UniqueIdSearch();
		search.setKey("key_1");
		final List<UniqueId> all = dao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	@Test
	public void testGetLast() {
		dao.create(new UniqueId("key_1", 1L));
		dao.create(new UniqueId("key_1", 2L));
		dao.create(new UniqueId("key_1", 3L));
		dao.create(new UniqueId("key_2", 1L));

		UniqueId last = dao.getLast("key_1");
		Assert.assertNotNull(last);
		Assert.assertEquals(new UniqueId("key_1", 3L), last);

		last = dao.getLast("key_2");
		Assert.assertNotNull(last);
		Assert.assertEquals(new UniqueId("key_2", 1L), last);

		last = dao.getLast("key_3");
		Assert.assertNull(last);
	}

}
