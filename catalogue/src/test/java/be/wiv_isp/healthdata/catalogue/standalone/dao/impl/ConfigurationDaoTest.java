/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.catalogue.standalone.dao.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import be.wiv_isp.healthdata.catalogue.standalone.dao.IConfigurationDao;
import be.wiv_isp.healthdata.catalogue.standalone.domain.Configuration;

/**
 * Created with IntelliJ IDEA. User: Mathias Date: 10/07/14 Time: 18:03 To
 * change this template use File | Settings | File Templates.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class ConfigurationDaoTest {

	private static final Logger LOG = LoggerFactory.getLogger(ConfigurationDaoTest.class);

	private static final String CONFIGURATION_KEY_1 = "key1";

	private static final String CONFIGURATION_KEY_2 = "key2";

	private Configuration configuration1;

	private Configuration configuration2;

	@Autowired
	private IConfigurationDao configurationDao;

	@Before
	public void setUp() throws Exception {
		// Creation of first Configuration element
		configuration1 = new Configuration();
		configuration1.setKey(CONFIGURATION_KEY_1);
		configuration1.setValue("value1");
		configuration1.setReadable(true);

		LOG.info("creating configuration 1");
		configurationDao.create(configuration1);

		// Creation of second Configuration element
		configuration2 = new Configuration();
		configuration2.setKey(CONFIGURATION_KEY_2);
		configuration2.setValue("value2");
		configuration2.setReadable(true);

		LOG.info("creating configuration 2");
		configurationDao.create(configuration2);
	}

	@Test
	public void testGet() throws Exception {
		final Configuration returnedConfiguration = configurationDao.get(configuration1.getId());
		Assert.assertEquals(configuration1.getKey(), returnedConfiguration.getKey());
		Assert.assertEquals(configuration1.getValue(), returnedConfiguration.getValue());
		Assert.assertEquals(configuration1.isReadable(), returnedConfiguration.isReadable());
	}

	@Test
	public void testGet2() throws Exception {
		final Configuration returnedConfiguration = configurationDao.get(configuration1.getKey());
		Assert.assertEquals(configuration1.getKey(), returnedConfiguration.getKey());
		Assert.assertEquals(configuration1.getValue(), returnedConfiguration.getValue());
		Assert.assertEquals(configuration1.isReadable(), returnedConfiguration.isReadable());
	}

	@Test
	public void testUpdate() {
		final String initialValue = "value1";
		final String updatedValue = "value1_modified";

		final Configuration configuration = configurationDao.get(configuration1.getId());
		Assert.assertTrue(initialValue.equals(configuration.getValue()));
		configuration.setValue(updatedValue);
		configurationDao.update(configuration);
		final Configuration updatedConfiguration = configurationDao.get(configuration1.getId());
		Assert.assertTrue(updatedValue.equals(updatedConfiguration.getValue()));
	}

	@Test
	public void testDelete() {
		configurationDao.delete(configuration1);
		final Configuration returnedConfiguration = configurationDao.get(configuration1.getId());
		Assert.assertNull(returnedConfiguration);
	}

	@Test
	public void testDeleteNonExistingConfiguration() {
		final Long unexsitingId = Long.MIN_VALUE;
		final String unexsitingKey = "unexisting_key";
		final Configuration unexistingConfiguration = new Configuration();
		unexistingConfiguration.setKey(unexsitingKey);
		unexistingConfiguration.setValue("value");

		final Configuration returnedConfiguration = configurationDao.get(unexsitingId);
		Assert.assertNull(returnedConfiguration);
		configurationDao.delete(unexistingConfiguration);

		Assert.assertNull(returnedConfiguration);
	}
}
