/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.util;

import org.junit.Assert;
import org.junit.Test;

public class IdentificationTypeTest {

	@Test
	public void testCBE() {
		Assert.assertEquals("KBO", IdentificationType.getSalesForceValue("CBE"));
	}

	@Test
	public void testEHP() {
		Assert.assertEquals("EHP", IdentificationType.getSalesForceValue("EHP"));
	}

	@Test
	public void testNIHII() {
		Assert.assertEquals("RIZIV", IdentificationType.getSalesForceValue("NIHII"));
	}

	@Test
	public void testNIHII_LABO() {
		Assert.assertEquals("RIZIV", IdentificationType.getSalesForceValue("NIHII-LABO"));
	}

	@Test
	public void testNIHII_HOSPITAL() {
		Assert.assertEquals("RIZIV", IdentificationType.getSalesForceValue("NIHII-HOSPITAL"));
	}

	@Test
	public void testUnexisting() {
		Assert.assertEquals("TEST", IdentificationType.getSalesForceValue("TEST"));
	}
}
