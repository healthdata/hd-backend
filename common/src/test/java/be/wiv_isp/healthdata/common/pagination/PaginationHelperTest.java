/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.pagination;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.text.MessageFormat;

public class PaginationHelperTest {

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void testBuildPaginationRangeNull() {
		Assert.assertNull(PaginationHelper.buildPaginationRange(null));
	}

	@Test
	public void testBuildPaginationRangeNormalScenario() {
		final String rangeString = "Range: items=0-24";
		PaginationRange range = PaginationHelper.buildPaginationRange(rangeString);
		Assert.assertEquals(0, range.getFrom());
		Assert.assertEquals(24, range.getTo());
		Assert.assertEquals(0, range.getTotalCount());
	}

	@Test
	public void testBuildPaginationRangeFormatException() {
		final String rangeString = "Range: items=0";
		expectedException.expect(HealthDataException.class);
		expectedException.expectMessage(ExceptionType.INVALID_RANGE_FORMAT.getMessage().replace("''", "'"));
		PaginationHelper.buildPaginationRange(rangeString);
	}

	@Test
	public void testBuildPaginationRangeException() {
		final String rangeString = "Range: items=24-0";
		expectedException.expect(HealthDataException.class);
		expectedException.expectMessage(MessageFormat.format(ExceptionType.INVALID_RANGE.getMessage(), 24, 0));
		PaginationHelper.buildPaginationRange(rangeString);
	}
}
