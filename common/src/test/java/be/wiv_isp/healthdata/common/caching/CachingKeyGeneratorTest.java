/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.common.caching;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-cache.xml" })
public class CachingKeyGeneratorTest {

    @Autowired
    private Incrementor incrementor;

    @Autowired
    private Decrementor decrementor;

    @Test // WDC-3135
    public void test() {
        Assert.assertEquals(0, incrementor.getNumberOfCalls());

        Assert.assertEquals(2, incrementor.compute(1));
        Assert.assertEquals(1, incrementor.getNumberOfCalls());

        Assert.assertEquals(2, incrementor.compute(1));
        Assert.assertEquals(1, incrementor.getNumberOfCalls()); // verify result was retrieved from cache

        Assert.assertEquals(0, decrementor.getNumberOfCalls());

        Assert.assertEquals(0, decrementor.compute(1));
        Assert.assertEquals(1, decrementor.getNumberOfCalls());

        Assert.assertEquals(0, decrementor.compute(1));
        Assert.assertEquals(1, decrementor.getNumberOfCalls()); // verify result was retrieved from cache
    }

}
