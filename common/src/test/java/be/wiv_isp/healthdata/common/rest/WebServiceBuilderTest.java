/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.rest;

import com.sun.jersey.api.client.GenericType;
import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class WebServiceBuilderTest {

	@Test
	public void testHashcode() {
		WebServiceBuilder wsb1 = new WebServiceBuilder();
		wsb1.addParameter("language", "NL");
		wsb1.setUrl("http://localhost:8080");
		wsb1.setGet(true);
		wsb1.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb1.setReturnType(new GenericType<String>() {
		});

		WebServiceBuilder wsb2 = new WebServiceBuilder();
		wsb2.addParameter("language", "NL");
		wsb2.setUrl("http://localhost:8080");
		wsb2.setGet(true);
		wsb2.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb2.setReturnType(new GenericType<String>() {
		});

		Assert.assertEquals(wsb1.hashCode(), wsb2.hashCode());
	}

	/*
	Serialize tests are written to ensure the WebServiceBuilder class is serializable (used by the caching framework)
	 */
	@Test
	public void testSerialize() throws Exception {
		final WebServiceBuilder wsb = new WebServiceBuilder();
		wsb.setAccept(null);
		wsb.setType(MediaType.APPLICATION_JSON_TYPE);
		wsb.setReturnType(new GenericType<String>() {});
		wsb.setGet(true);
		wsb.setUrl("http://host:port");
		wsb.setJson("{}");
		wsb.addHeader(HttpHeaders.AUTHORIZATION, "Bearer token");
		wsb.addParameter("key", "value");

		// serialize
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		final ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(wsb);
		oos.close();

		// deserialize
		final ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(baos.toByteArray()));
		final WebServiceBuilder deserializedWsb = (WebServiceBuilder) in.readObject();
		in.close();

		// compare
		Assert.assertEquals(wsb, deserializedWsb);
	}

	@Test
	public void testSerializeWithReturnTypeNull() throws Exception {
		final WebServiceBuilder wsb = new WebServiceBuilder();
		wsb.setAccept(null);
		wsb.setType(MediaType.APPLICATION_JSON_TYPE);
		wsb.setReturnType(null);
		wsb.setGet(true);
		wsb.setUrl("http://host:port");
		wsb.setJson("{}");
		wsb.addHeader(HttpHeaders.AUTHORIZATION, "Bearer token");
		wsb.addParameter("key", "value");

		// serialize
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		final ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(wsb);
		oos.close();

		// deserialize
		final ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(baos.toByteArray()));
		final WebServiceBuilder deserializedWsb = (WebServiceBuilder) in.readObject();
		in.close();

		// compare
		Assert.assertEquals(wsb, deserializedWsb);
	}
}
