/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.util;

import java.util.Map;
import java.util.Map.Entry;

import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class WebServiceUtils {
	public static Object callWebService(WebServiceBuilder builder) {
		final ClientConfig config = new DefaultClientConfig();
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		final Client client = Client.create(config);

		WebResource wr = client.resource(builder.getUrl());
		Map<String, String> parameters = builder.getParameters();
		for (Entry<String, String> parameterEntry : parameters.entrySet()) {
			wr = wr.queryParam(parameterEntry.getKey(), parameterEntry.getValue());
		}
		Builder wrb = wr.getRequestBuilder();
		if (builder.getType() != null) {
			wrb = wrb.type(builder.getType());
		}
		if (builder.getAccept() != null) {
			wrb = wrb.accept(builder.getAccept());
		}
		Map<HttpHeaders, String> header = builder.getHeader();
		for (Entry<HttpHeaders, String> headerEntry : header.entrySet()) {
			wrb = wrb.header(headerEntry.getKey().getValue(), headerEntry.getValue());
		}
		if (builder.isPost()) {
			if (builder.getJson() != null) {
				return wrb.post(builder.getReturnType(), builder.getJson());
			} else {
				return wrb.post(builder.getReturnType());
			}
		}
		if (builder.isGet()) {
			return wrb.get(builder.getReturnType());

		}
		if (builder.isDelete()) {
			return wrb.delete(builder.getReturnType());
		}
		return null;
	}
}
