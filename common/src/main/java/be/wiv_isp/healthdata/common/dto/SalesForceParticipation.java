/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.common.dto;

public class SalesForceParticipation {

    public String Organisatie__c;
    public String HealthData_Register__c;
    public boolean Actief_als_data_provider__c = true;

    public String getOrganisatie__c() {
        return Organisatie__c;
    }

    public void setOrganisatie__c(String organisatie__c) {
        Organisatie__c = organisatie__c;
    }

    public String getHealthData_Register__c() {
        return HealthData_Register__c;
    }

    public void setHealthData_Register__c(String healthData_Register__c) {
        HealthData_Register__c = healthData_Register__c;
    }

    public boolean isActief_als_data_provider__c() {
        return Actief_als_data_provider__c;
    }

    public void setActief_als_data_provider__c(boolean actief_als_data_provider__c) {
        Actief_als_data_provider__c = actief_als_data_provider__c;
    }
}
