/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.rest;

import com.sun.jersey.api.client.GenericType;

import javax.ws.rs.core.MediaType;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class WebServiceBuilder implements Serializable {

	private static final long serialVersionUID = 2930051511013931561L;

	private boolean get;
	private boolean post;
	private boolean put;
	private boolean delete;

	private SerializableMediaType type;
	private SerializableMediaType accept;

	private String url;
	private SerializableGenericType returnType;
	private final Map<HttpHeaders, String> header = new HashMap<>();
	private final Map<String, String> publicParameters = new HashMap<>();
	private final Map<String, String> privateParameters = new HashMap<>();
	private Object json;

	private boolean logResponseBody = true; // default to true

	public boolean isGet() {
		return get;
	}

	public void setGet(boolean get) {
		this.get = get;
		if (get) {
			post = false;
			delete = false;
			put = false;
		}
	}

	public boolean isPost() {
		return post;
	}

	public void setPost(boolean post) {
		this.post = post;
		if (post) {
			get = false;
			delete = false;
			put = false;
		}
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
		if (delete) {
			get = false;
			post = false;
			put = false;
		}
	}

	public boolean isPut() {
		return put;
	}

	public void setPut(boolean put) {
		this.put = put;
		if (put) {
			get = false;
			post = false;
			delete = false;
		}
	}

	public MediaType getType() {
		return type == null ? null : type.getMediaType();
	}

	public void setType(MediaType type) {
		this.type = (type == null) ? null : new SerializableMediaType(type);
	}

	public MediaType getAccept() {
		return accept == null ? null : accept.getMediaType();
	}

	public void setAccept(MediaType accept) {
		this.accept = (accept == null) ? null : new SerializableMediaType(accept);
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = encode(url);
	}

	private String encode(String url) {
		try {
			return new URI(null, url, null).toString();
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
	}

	public GenericType<?> getReturnType() {
		return (returnType == null) ? null : returnType.getGenericType();
	}

	public void setReturnType(GenericType<?> returnType) {
		this.returnType = (returnType == null) ? null : new SerializableGenericType(returnType);
	}

	public Object getJson() {
		return json;
	}

	public void setJson(Object json) {
		this.json = json;
	}

	public Map<HttpHeaders, String> getHeader() {
		return header;
	}

	public void addHeader(HttpHeaders httpHeaders, String value) {
		header.put(httpHeaders, value);
	}

	public Map<String, String> getPrivateParameters() {
		return privateParameters;
	}

	public Map<String, String> getPublicParameters() {
		return publicParameters;
	}

	public Map<String, String> getParameters() {
		Map<String, String> allParameters = new HashMap<>();

		allParameters.putAll(publicParameters);
		allParameters.putAll(privateParameters);

		return allParameters;
	}

	public void addParameter(String key, String value) {
		addParameter(key, value, false);
	}

	public void addParameter(String key, String value, boolean privateParameter) {
		if (privateParameter) {
			privateParameters.put(key, value);
		} else {
			publicParameters.put(key, value);
		}
	}

	public boolean logResponseBody() {
		return logResponseBody;
	}

	public void setLogResponseBody(boolean logResponseBody) {
		this.logResponseBody = logResponseBody;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof WebServiceBuilder) {
			WebServiceBuilder other = (WebServiceBuilder) o;

			return Objects.equals(get, other.get) //
					&& Objects.equals(post, other.post) //
					&& Objects.equals(put, other.put) //
					&& Objects.equals(delete, other.delete) //
					&& Objects.equals(type, other.type) //
					&& Objects.equals(accept, other.accept) //
					&& Objects.equals(url, other.url) //
					&& Objects.equals(header, other.header) //
					&& Objects.equals(publicParameters, other.publicParameters) //
					&& Objects.equals(privateParameters, other.privateParameters) //
					&& Objects.equals(json, other.json);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash( //
				this.get, //
				this.post, //
				this.put, //
				this.delete, //
				this.type, //
				this.accept, //
				this.url, //
				this.header, //
				this.publicParameters, //
				this.privateParameters, //
				this.json);
	}

	@Override
	public String toString() {
		return "WebServiceBuilder {" + //
				"get = " + Objects.toString(this.get) + ", " + //
				"post = " + Objects.toString(this.post) + ", " + //
				"put = " + Objects.toString(this.put) + ", " + //
				"delete = " + Objects.toString(this.delete) + ", " + //
				"type = " + Objects.toString(this.type) + ", " + //
				"accept = " + Objects.toString(this.accept) + ", " + //
				"url = " + Objects.toString(this.url) + ", " + //
				"header = " + Objects.toString(this.header) + ", " + //
				"parameters = " + Objects.toString(this.publicParameters) + ", " + //
				"json = " + Objects.toString(this.json) + "}";
	}
}