/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.common.dto;

import be.wiv_isp.healthdata.common.json.deserializer.JsonToByteArrayDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.ByteArrayToStringSerializer;
import be.wiv_isp.healthdata.common.util.StringUtils;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.Serializable;

public class PdfTemplateDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    @JsonSerialize(using = ByteArrayToStringSerializer.class)
    @JsonDeserialize(using = JsonToByteArrayDeserializer.class)
    private byte[] en;
    @JsonSerialize(using = ByteArrayToStringSerializer.class)
    @JsonDeserialize(using = JsonToByteArrayDeserializer.class)
    private byte[] fr;
    @JsonSerialize(using = ByteArrayToStringSerializer.class)
    @JsonDeserialize(using = JsonToByteArrayDeserializer.class)
    private byte[] nl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getEn() {
        return en;
    }

    public void setEn(byte[] en) {
        this.en = en;
    }

    public byte[] getFr() {
        return fr;
    }

    public void setFr(byte[] fr) {
        this.fr = fr;
    }

    public byte[] getNl() {
        return nl;
    }

    public void setNl(byte[] nl) {
        this.nl = nl;
    }

    @Override
    public String toString() {
        return StringUtils.toStringExclude(this, "en", "fr", "nl");
    }
}
