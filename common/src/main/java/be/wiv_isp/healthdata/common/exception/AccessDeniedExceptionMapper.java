/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.UUID;

@Provider
public class AccessDeniedExceptionMapper implements ExceptionMapper<AccessDeniedException> {

	private static final Logger LOG = LoggerFactory.getLogger(AccessDeniedExceptionMapper.class);

	@Override
	public Response toResponse(AccessDeniedException ex) {
		String uuid = UUID.randomUUID().toString();

		StackTraceElement[] stackTrace = ex.getStackTrace();
		for (StackTraceElement stackTraceElement : stackTrace) {
			if (stackTraceElement.getClassName().startsWith("be.wiv_isp.healthdata")) {
				LOG.error("Exception [{}]: Access is denied for :" + stackTraceElement, uuid);
				break;
			}
		}

		ExceptionResponse response = new ExceptionResponse();
		response.setUuid(uuid);
		response.setError("access_denied");
		response.setError_description("Access is denied");

		return Response.status(Status.FORBIDDEN)
				.entity(response)
				.type(MediaType.APPLICATION_JSON)
				.build();
	}
}
