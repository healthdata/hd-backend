/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.json.deserializer;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.apache.commons.lang3.time.DateUtils;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;

public class TimestampDeserializer extends JsonDeserializer<Timestamp> {

	private static final String[] DATE_FORMATS = { DateFormat.DATE_AND_TIME.getPattern(),

	DateFormat.DATE_AND_TIME_NON_ISO.getPattern(), DateFormat.DATE_AND_TIME_WITH_MS.getPattern() };

	@Override
	public Timestamp deserialize(JsonParser jsonparser, DeserializationContext deserializationcontext) throws IOException {

		final String dateAsString = jsonparser.getText();

		try {
			final Date validForCreationAsDate = DateUtils.parseDate(dateAsString, DATE_FORMATS);
			return new Timestamp(validForCreationAsDate.getTime());
		} catch (ParseException | IllegalArgumentException e) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, dateAsString, Arrays.toString(DATE_FORMATS));
			throw exception;
		}
	}
}
