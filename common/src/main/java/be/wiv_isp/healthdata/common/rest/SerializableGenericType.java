/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.rest;

import com.sun.jersey.api.client.GenericType;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Objects;

public class SerializableGenericType implements Serializable {

    private static final long serialVersionUID = 3008874527284039522L;

    private GenericType<?> genericType;

    public SerializableGenericType(GenericType<?> genericType) {
        this.genericType = genericType;
    }

    public GenericType<?> getGenericType() {
        return genericType;
    }

    public void setGenericType(GenericType<?> genericType) {
        this.genericType = genericType;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof SerializableGenericType) {
            SerializableGenericType other = (SerializableGenericType) o;

            return Objects.equals(genericType.getType(), other.genericType.getType())
                    && Objects.equals(genericType.getRawClass(), other.genericType.getRawClass());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.genericType);
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.writeObject((genericType == null) ? null : genericType.getType());
    }

    private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
        final Type genericTypeAsType = (Type) ois.readObject();
        genericType = (genericTypeAsType == null) ? null : new GenericType<>(genericTypeAsType);
    }
}
