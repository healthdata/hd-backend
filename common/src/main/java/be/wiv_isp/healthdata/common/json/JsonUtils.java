/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.json;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.apache.commons.lang3.time.DateUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Date;
import java.util.Map;
import java.util.Set;

public class JsonUtils {

    public static JSONObject createJsonObject(byte[] b) {
        return createJsonObject(new String(b, StandardCharsets.UTF_8));
    }

    public static JSONObject createJsonObject(String s) {
        try {
            return new JSONObject(s);
        } catch (JSONException e) {
            throw getHealthDataException(e);
        }
    }

    public static JSONObject createSerializableJsonObject(byte[] b) {
        return createSerializableJsonObject(new String(b, StandardCharsets.UTF_8));
    }

    public static JSONObject createSerializableJsonObject(String s) {
        try {
            return new SerializableJsonObject(s);
        } catch (JSONException e) {
            throw getHealthDataException(e);
        }
    }

    public static JSONObject createSerializableJsonObject(Map<String, String> map) {
        return new SerializableJsonObject(map);
    }

    public static JSONArray createJsonArray(byte[] b) {
        return createJsonArray(new String(b, StandardCharsets.UTF_8));
    }

    public static JSONArray createJsonArray(String s) {
        try {
            return new JSONArray(s);
        } catch (JSONException e) {
            throw getHealthDataException(e);
        }
    }

    public static void put(JSONObject jsonObject, String key, Object value) {
        try {
            if(value != null && Collection.class.isAssignableFrom(value.getClass())) {
                jsonObject.put(key, (Collection) value);
            } else {
                jsonObject.put(key, value);
            }
        } catch (JSONException e) {
            throw getHealthDataException(e);
        }
    }

    public static Long getLong(JSONObject jsonObject, String key) {
        try {
            if(jsonObject.isNull(key)) {
                return null;
            }
            return jsonObject.getLong(key);
        } catch (JSONException e) {
            throw getHealthDataException(e);
        }
    }

    public static String getString(JSONObject jsonObject, String key) {
        try {
            if(jsonObject.isNull(key)) {
                return null;
            }
            return jsonObject.getString(key);
        } catch (JSONException e) {
            throw getHealthDataException(e);
        }
    }

    public static JSONObject getJSONObject(JSONObject jsonObject, String key) {
        try {
            if(jsonObject.isNull(key)) {
                return null;
            }
            return jsonObject.getJSONObject(key);
        } catch (JSONException e) {
            throw getHealthDataException(e);
        }
    }

    public static JSONArray getJSONArray(JSONObject jsonObject, String key) {
        try {
            if(jsonObject.isNull(key)) {
                return null;
            }
            return jsonObject.getJSONArray(key);
        } catch (JSONException e) {
            throw getHealthDataException(e);
        }
    }

    public static JSONObject getJSONObject(JSONArray array, int i) {
        try {
            return array.getJSONObject(i);
        } catch (JSONException e) {
            throw getHealthDataException(e);
        }
    }

    public static boolean getBoolean(JSONObject jsonObject, String key) {
        try {
            return jsonObject.getBoolean(key);
        } catch (JSONException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.JSON_PARSE_EXCEPTION, e.getMessage());
            throw exception;
        }
    }

    public static Boolean getBooleanObject(JSONObject jsonObject, String key) {
        try {
            if(jsonObject.isNull(key)) {
                return null;
            }
            return jsonObject.getBoolean(key);
        } catch (JSONException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.JSON_PARSE_EXCEPTION, e.getMessage());
            throw exception;
        }
    }

    public static boolean equals(JSONObject o1, JSONObject o2) {
        if(o1 == null && o2 == null) {
            return true;
        }
        if(o1 == null || o2 == null) {
            return false;
        }
        return o1.toString().equals(o2.toString());
    }

    public static long getLong(JSONArray array, int i) {
        try {
            return array.getLong(i);
        } catch (JSONException e) {
            throw getHealthDataException(e);
        }
    }

    public static String getString(JSONArray array, int i) {
        try {
            return array.getString(i);
        } catch (JSONException e) {
            throw getHealthDataException(e);
        }
    }

    static private HealthDataException getHealthDataException(JSONException e) {
        HealthDataException exception = new HealthDataException(e);
        exception.setExceptionType(ExceptionType.JSON_PARSE_EXCEPTION, e.getMessage());
        return exception;
    }

    static public Timestamp getTimestamp (JSONObject json, String key) throws JSONException{
        try {
            final Date asDate = DateUtils.parseDate(json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
            return new Timestamp(asDate.getTime());
        } catch (ParseException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, json.getString(key), DateFormat.DATE_AND_TIME.getPattern());
            throw exception;
        }
    }

    public static Set<String> getStringSet(final JSONObject json, final String key) {
        final JSONArray array = JsonUtils.getJSONArray(json, key);

        if(array == null) {
            return null;
        }

        final Set<String> res = new HashSet<>();
        for (int i = 0; i < array.length(); i++) {
            res.add(JsonUtils.getString(array, i));
        }

        return res;
    }
}
