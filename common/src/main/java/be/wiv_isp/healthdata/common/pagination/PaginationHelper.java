/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.pagination;

import org.springframework.util.StringUtils;

public class PaginationHelper {

	public static PaginationRange buildPaginationRange(String range) {
		if (StringUtils.isEmpty(range)) {
			return null;
		}

		final String[] ranges = range.split("=")[1].split("-");
		PaginationValidator.validateFormat(ranges);

		int from = Integer.parseInt(ranges[0]);
		int to = Integer.parseInt(ranges[1]);

		final PaginationRange result = new PaginationRange(from, to);
		PaginationValidator.validateRange(result);
		return result;
	}

	public static String buildResponseRangeString(PaginationRange paginationRange) {
		long total = paginationRange.getTotalCount();
		long from = Math.min(paginationRange.getFrom(), total+1);
		long to = Math.min(paginationRange.getTo(), total);
		return String.format("items %d-%d/%d", from, to, total);
	}

}
