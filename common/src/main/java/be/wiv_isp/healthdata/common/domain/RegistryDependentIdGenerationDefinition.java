/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.common.domain;

import java.io.Serializable;

public class RegistryDependentIdGenerationDefinition implements Serializable {

    private static final long serialVersionUID = -2418288540337974825L;

    private boolean generationEnabled;
    private Type type;
    private Phase phase;

    public boolean isGenerationEnabled() {
        return generationEnabled;
    }

    public void setGenerationEnabled(boolean generationEnabled) {
        this.generationEnabled = generationEnabled;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Phase getPhase() {
        return phase;
    }

    public void setPhase(Phase phase) {
        this.phase = phase;
    }

    public enum Type {
        TYPE_1,
        TYPE_2;

        public static Type match(final String value) {
            final String valueWithoutSpaces = replaceSpaceWithUnderscore(value);
            for (final Type type : Type.values()) {
                if(type.name().equalsIgnoreCase(valueWithoutSpaces)) {
                    return type;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    public enum Phase {
        CREATION,
        SUBMISSION;

        public static Phase match(final String value) {
            final String valueWithoutSpaces = replaceSpaceWithUnderscore(value);
            for (final Phase phase : Phase.values()) {
                if(phase.name().equalsIgnoreCase(valueWithoutSpaces)) {
                    return phase;
                }
            }
            throw new IllegalArgumentException();
        }

    }

    private static final String SPACE = " ";
    private static final String UNDERSCORE = "_";

    private static String replaceSpaceWithUnderscore(String value) {
        return value.replace(SPACE, UNDERSCORE);
    }

}
