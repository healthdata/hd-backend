/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.dto;

import be.wiv_isp.healthdata.common.json.deserializer.JsonToByteArrayDeserializer;
import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.ByteArrayToJsonSerializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;

public class DataCollectionDefinitionDtoV7 implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String dataCollectionName;
	private Integer minorVersion;
	private DataCollectionGroupDtoV7 dataCollectionGroup;
	private TranslatableStringDto label;
	private TranslatableStringDto description;
	@JsonSerialize(using = ByteArrayToJsonSerializer.class)
	@JsonDeserialize(using = JsonToByteArrayDeserializer.class)
	private byte[] content;
	private List<FollowUpDefinitionDto> followUpDefinitions;
	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	private Timestamp createdOn;
	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	private Timestamp updatedOn;


	public DataCollectionDefinitionDtoV7() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDataCollectionName() {
		return dataCollectionName;
	}

	public void setDataCollectionName(String dataCollectionName) {
		this.dataCollectionName = dataCollectionName;
	}

	public TranslatableStringDto getLabel() {
		return label;
	}

	public DataCollectionGroupDtoV7 getDataCollectionGroup() {
		return dataCollectionGroup;
	}

	public void setDataCollectionGroup(DataCollectionGroupDtoV7 dataCollectionGroup) {
		this.dataCollectionGroup = dataCollectionGroup;
	}

	public void setLabel(TranslatableStringDto label) {
		this.label = label;
	}

	public TranslatableStringDto getDescription() {
		return description;
	}

	public void setDescription(TranslatableStringDto description) {
		this.description = description;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public Integer getMinorVersion() {
		return minorVersion;
	}

	public void setMinorVersion(Integer minorVersion) {
		this.minorVersion = minorVersion;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public List<FollowUpDefinitionDto> getFollowUpDefinitions() {
		return followUpDefinitions;
	}

	public void setFollowUpDefinitions(List<FollowUpDefinitionDto> followUpDefinitions) {
		this.followUpDefinitions = followUpDefinitions;
	}

	@JsonIgnore
	public String getVersion() {
		return MessageFormat.format("{0}.{1}", dataCollectionGroup.getMajorVersion(), minorVersion);
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof DataCollectionDefinitionDtoV7) {
			DataCollectionDefinitionDtoV7 other = (DataCollectionDefinitionDtoV7) o;

			return Objects.equals(id, other.id)
					&& Objects.equals(dataCollectionGroup, other.dataCollectionGroup)
					&& Objects.equals(dataCollectionName, other.dataCollectionName)
					&& Objects.equals(label, other.label)
					&& Objects.equals(description, other.description)
					&& Objects.equals(content, other.content)
					&& Objects.equals(minorVersion, other.minorVersion)
					&& Objects.equals(createdOn, other.createdOn)
					&& Objects.equals(updatedOn, other.updatedOn);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				this.id,
				this.dataCollectionGroup,
				this.dataCollectionName,
				this.label,
				this.description,
				this.content,
				this.minorVersion,
				this.createdOn,
				this.updatedOn);
	}
}
