/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.common.exception;

import ch.qos.logback.classic.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class ExceptionUtils {

    private static final Logger LOG = LoggerFactory.getLogger(ExceptionUtils.class);

    static public String log(HealthDataException ex) {
        String uuid = UUID.randomUUID().toString();

        if(Level.ERROR.equals(ex.getLogLevel())) {
            logError(uuid, ex);
        } else if (Level.WARN.equals(ex.getLogLevel())) {
            logWarn(uuid, ex);
        } else if (Level.INFO.equals(ex.getLogLevel())) {
            logInfo(uuid, ex);
        } else if (Level.DEBUG.equals(ex.getLogLevel())) {
            logDebug(uuid, ex);
        }
        return uuid;
    }

    static public String log(Throwable ex) {
        String uuid = UUID.randomUUID().toString();

        LOG.error("Exception[{}]: {}", uuid, ex.getMessage());
        LOG.error(uuid, ex);

        return uuid;

    }

    static private void logError(String uuid, HealthDataException ex) {
        LOG.error("Exception[{}]: {}", uuid, ex.getResponseMessage());
        if(ex.getCause() != null) {
            LOG.error(uuid, ex.getCause());
        }
        for (Loggable loggable : ex.getLoggable()) {
            LOG.error("Exception[{}]: {}", uuid, loggable.log());
        }
    }

    static private void logWarn(String uuid, HealthDataException ex) {
        LOG.warn("Exception[{}]: {}", uuid, ex.getResponseMessage());
        if(ex.getCause() != null) {
            LOG.warn(uuid, ex.getCause());
        }
        for (Loggable loggable : ex.getLoggable()) {
            LOG.warn("Exception[{}]: {}", uuid, loggable.log());
        }
    }

    static private void logInfo(String uuid, HealthDataException ex) {
        LOG.info("Exception[{}]: {}", uuid, ex.getResponseMessage());
        if(ex.getCause() != null) {
            LOG.info(uuid, ex.getCause());
        }
        for (Loggable loggable : ex.getLoggable()) {
            LOG.info("Exception[{}]: {}", uuid, loggable.log());
        }
    }

    static private void logDebug(String uuid, HealthDataException ex) {
        LOG.debug("Exception[{}]: {}", uuid, ex.getResponseMessage());
        if(ex.getCause() != null) {
            LOG.debug(uuid, ex.getCause());
        }
        for (Loggable loggable : ex.getLoggable()) {
            LOG.debug("Exception[{}]: {}", uuid, loggable.log());
        }
    }
}
