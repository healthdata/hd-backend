/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.dto;

import java.io.Serializable;
import java.util.Objects;

public class SalesForceOrganizationDto implements Serializable {

	private static final long serialVersionUID = 8305489958526226948L;

	private String identificationType;

	private String identificationValue;

	private String name;

	private boolean hd4prc = false;

	public String getIdentificationType() {
		return identificationType;
	}

	public void setIdentificationType(String identificationType) {
		this.identificationType = identificationType;
	}

	public String getIdentificationValue() {
		return identificationValue;
	}

	public void setIdentificationValue(String identificationValue) {
		this.identificationValue = identificationValue;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isHd4prc() {
		return hd4prc;
	}

	public void setHd4prc(boolean hd4prc) {
		this.hd4prc = hd4prc;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof SalesForceOrganizationDto) {
			SalesForceOrganizationDto other = (SalesForceOrganizationDto) o;

			return Objects.equals(identificationType, other.identificationType)
					&& Objects.equals(identificationValue, other.identificationValue)
					&& Objects.equals(name, other.name);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				this.identificationType,
				this.identificationValue,
				this.name);
	}

	@Override
	public String toString() {
		return "Organization {" +
				"identificationType = " + Objects.toString(this.identificationType) + ", " +
				"identificationValue = " + Objects.toString(this.identificationValue) + ", " +
				"name = " + Objects.toString(this.name) + "}";
	}
}
