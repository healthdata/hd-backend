/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.common.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public class DataCollectionGroupListMap extends HashMap<String, List<DataCollectionGroupListDto>> implements Serializable{

    private static final long serialVersionUID = 1L;

    public DataCollectionGroupListDto getLast(String dataCollectionGroupName) {
        DataCollectionGroupListDto last = null;
        List<DataCollectionGroupListDto> groups = get(dataCollectionGroupName);
        for (DataCollectionGroupListDto group : groups) {
            if(last == null || group.getMajorVersion() > last.getMajorVersion()) {
                last = group;
            }
        }
        return last;
    }

    // (temporary) fix for retrieving a data collection definition without knowing its group name (needed for provisioning and fastrack)
    public DataCollectionGroupListDto getLastByDataCollectionName(String dataCollectionName) {
        for (final String group : keySet()) {
            final DataCollectionGroupListDto last = getLast(group);
            if(last.get(dataCollectionName) != null) {
                return last;
            }
        }
        return null;
    }
}
