/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IdentificationType {

	private static final Logger LOG = LoggerFactory.getLogger(IdentificationType.class);

	public static final String RIZIV = "RIZIV";
	public static final String KBO = "KBO";
	public static final String EHP = "EHP";
	public static final String CBE = "CBE";

	public static final String NIHII = "NIHII";

	public static String getSalesForceValue(String identificationType) {
		if (identificationType == null)
			identificationType = "";
		identificationType = identificationType.toUpperCase();
		switch (identificationType) {
		case "NIHII":
		case "NIHII-LABO":
		case "NIHII-HOSPITAL":
			return RIZIV;
		case "CBE":
			return KBO;
		case "EHP":
			return EHP;
		default:
			LOG.warn("Unknown identification type: [{}]", identificationType);
			return identificationType;
		}
	}
}
