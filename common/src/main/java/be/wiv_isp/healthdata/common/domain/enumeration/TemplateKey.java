/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.domain.enumeration;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

public enum TemplateKey {

    USER_REQUEST_NEW_FOR_USER,
    USER_REQUEST_NEW_FOR_REQUESTER,
    USER_REQUEST_NEW_FOR_ADMIN,
    USER_REQUEST_UPDATE_FOR_USER,
    USER_REQUEST_UPDATE_FOR_REQUESTER,
    USER_REQUEST_ACCEPT_FOR_USER_DB,
    USER_REQUEST_ACCEPT_FOR_USER_LDAP,
    USER_REQUEST_ACCEPT_FOR_REQUESTER,
    USER_REQUEST_REJECT_FOR_USER,
    USER_REQUEST_REJECT_FOR_REQUESTER,
    UPLOADED_STABLE_DATA,
    START_OF_DATA_COLLECTION_PERIOD,
    END_OF_DATA_COLLECTION_PERIOD,
    DATA_COLLECTION_DEFINITION_UPDATE,
    REGISTRATION_UPDATES,
    REGISTRATION_UPDATES_HD4RES,
    REGISTRATION_UPDATES_DMA_REP,
    NEW_USER_ACCOUNT,
    DATA_COLLECTION_ACCESS_REQUEST_NEW,
    DATA_COLLECTION_ACCESS_REQUEST_RESULT,
    PASSWORD_RESET,
    ;

    static public TemplateKey getKey(UriInfo info) {
        MultivaluedMap<String, String> pathParameters = info.getPathParameters();
        for (String pathParameterKey : pathParameters.keySet()) {
            if ("key".equalsIgnoreCase(pathParameterKey)) {
                TemplateKey[] templateKeys = TemplateKey.values();
                for (TemplateKey templateKey : templateKeys) {
                    if(templateKey.toString().equalsIgnoreCase(pathParameters.getFirst(pathParameterKey))) {
                        return templateKey;
                    }
                }
            }
        }
        return null;
    }
}
