/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.exception;

import ch.qos.logback.classic.Level;
import org.apache.commons.lang3.ArrayUtils;

import javax.ws.rs.core.Response;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HealthDataException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private Response.Status responseStatus;
	private String responseMessage;
	private String errorCode;
	private List<Loggable> loggable = new ArrayList<>();
	private Level logLevel = Level.ERROR;

	public HealthDataException(Loggable... loggable) {
		this(null, loggable);
	}

	public HealthDataException(Throwable cause, Loggable... loggables) {
		super(cause);
		if(ArrayUtils.isNotEmpty(loggables)) {
			this.loggable.addAll(Arrays.asList(loggables));
		}
	}

	public void setExceptionType(ExceptionType exceptionType, Object... messageArguments) {
		responseMessage = MessageFormat.format(exceptionType.getMessage(), messageArguments);
		responseStatus = exceptionType.getStatus();
		setErrorCode(exceptionType.name());
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setResponseStatus(Response.Status responseStatus) {
		this.responseStatus = responseStatus;
	}

	public Response.Status getResponseStatus() {
		return responseStatus;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String message, Object... messageArguments) {
		responseMessage = MessageFormat.format(message, messageArguments);
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public List<Loggable> getLoggable() {
		return loggable;
	}

	public void setLoggable(List<Loggable> loggable) {
		this.loggable = loggable;
	}

	public Level getLogLevel() {
		return logLevel;
	}

	public void setLogLevel(Level logLevel) {
		this.logLevel = logLevel;
	}

	@Override
	public String getMessage() {
		return responseMessage;
	}

	@Override
	public String getLocalizedMessage() {
		return responseMessage;
	}


}
