/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.rest;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class RestUtils {

    public class ParameterName {
        public static final String NAME = "name";
        public static final String ID = "id";
        public static final String UUID = "uuid";
        public static final String IDENTIFICATION_TYPE = "identificationType";
        public static final String IDENTIFICATION_VALUE = "identificationValue";
        public static final String PARENT_IDENTIFICATION_TYPE = "parentIdentificationType";
        public static final String PARENT_IDENTIFICATION_VALUE = "parentIdentificationValue";
        public static final String DATA_COLLECTION_DEFINITION_ID = "dataCollectionDefinitionId";
        public static final String DATA_COLLECTION_NAME = "dataCollectionName";
        public static final String HD4DP_ID = "hd4dpId";
        public static final String WORKFLOW_ID = "workflowId";
        public static final String AUTHORITY = "authority";
        public static final String LANGUAGE = "LANGUAGE";
        public static final String VIEW_NAME = "viewName";
        public static final String INSTALLATION_ID = "installationId";
        public static final String REPORT_DATE = "reportDate";
    }

    static public String getPathString(UriInfo info, String parameterName) {
        MultivaluedMap<String, String> pathParameters = info.getPathParameters();
        for (String pathParameterKey : pathParameters.keySet()) {
            if (parameterName.equalsIgnoreCase(pathParameterKey)) {
                try {
                    return URLDecoder.decode(pathParameters.getFirst(pathParameterKey), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return null;
    }

    static public Long getPathLong(UriInfo info, String parameterName) {
        MultivaluedMap<String, String> pathParameters = info.getPathParameters();
        for (String pathParameterKey : pathParameters.keySet()) {
            if (parameterName.equalsIgnoreCase(pathParameterKey)) {
                return Long.valueOf(pathParameters.getFirst(pathParameterKey));
            }
        }
        return null;
    }


    static public Boolean getPathBoolean(UriInfo info, String parameterName) {
        MultivaluedMap<String, String> pathParameters = info.getPathParameters();
        for (String pathParameterKey : pathParameters.keySet()) {
            if (parameterName.equalsIgnoreCase(pathParameterKey)) {
                return Boolean.valueOf(pathParameters.getFirst(pathParameterKey));
            }
        }
        return null;
    }

    static public String getParameterString(UriInfo info, String parameterName) {
        MultivaluedMap<String, String> queryParameters = info.getQueryParameters();
        for (String pathParameterKey : queryParameters.keySet()) {
            if (parameterName.equalsIgnoreCase(pathParameterKey)) {
                return queryParameters.getFirst(pathParameterKey);
            }
        }
        return null;
    }

    static public Long getParameterLong(UriInfo info, String parameterName) {
        MultivaluedMap<String, String> pathParameters = info.getQueryParameters();
        for (String pathParameterKey : pathParameters.keySet()) {
            if (parameterName.equalsIgnoreCase(pathParameterKey)) {
                return Long.valueOf(pathParameters.getFirst(pathParameterKey));
            }
        }
        return null;
    }


    static public Boolean getParameterBoolean(UriInfo info, String parameterName) {
        MultivaluedMap<String, String> pathParameters = info.getQueryParameters();
        for (String pathParameterKey : pathParameters.keySet()) {
            if (parameterName.equalsIgnoreCase(pathParameterKey)) {
                return Boolean.valueOf(pathParameters.getFirst(pathParameterKey));
            }
        }
        return null;
    }

}
