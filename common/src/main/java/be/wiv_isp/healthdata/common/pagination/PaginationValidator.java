/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.pagination;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;

public class PaginationValidator {

	public static void validateFormat(String[] ranges) {
		if (ranges.length != 2) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_RANGE_FORMAT);
			throw exception;
		}
		try {
			Integer.parseInt(ranges[0]);
			Integer.parseInt(ranges[1]);
		} catch (NumberFormatException nfe) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_RANGE_FORMAT);
			throw exception;
		}
	}

	public static void validateRange(PaginationRange range) {
		int from = range.getFrom();
		int to = range.getTo();
		if (from < 0 || to < 0 || to < from) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_RANGE, from, to);
			throw exception;
		}
	}
}
