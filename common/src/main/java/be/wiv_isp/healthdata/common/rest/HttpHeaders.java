/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.rest;

public enum HttpHeaders {

	AUTHORIZATION("Authorization"),
	ACCEPT_LANGUAGE("Accept-Language"),
	CLIENT_VERSION("ClientVersion"),
	CONTENT_RANGE("Content-Range"),
	RANGE("Range"),
	REGISTRATION_TOKEN("Registration-Token");

	private String value;

	HttpHeaders(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
