/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.exception;

import javax.ws.rs.core.Response;

public enum ExceptionType {

	// General
	GENERAL_EXCEPTION(Response.Status.INTERNAL_SERVER_ERROR, "{0}"), //
	GENERAL_CONFLICT(Response.Status.CONFLICT, "{0}"), //
	GENERAL_FORBIDDEN(Response.Status.FORBIDDEN, "{0}"), //
	GENERAL_UNAUTHORIZED(Response.Status.UNAUTHORIZED, "The action is unauthorized."), //

	// CRUD operations
	CREATE_GENERAL(Response.Status.INTERNAL_SERVER_ERROR, "Item cannot be created due to an unexpected error."), //
	CREATE_DUPLICATE_KEY(Response.Status.CONFLICT, "Item cannot be created since the key [{0}] already exists in the database."), //
	DELETE_NON_EXISTING(Response.Status.NOT_FOUND, "Attempt to delete a non existing [{0}] object with id [{1}]."), //
	GET_NON_EXISTING(Response.Status.NOT_FOUND, "[{0}] object with id [{1}] not found."), //
	UPDATE_NON_EXISTING(Response.Status.NOT_FOUND, "Attempt to update a non existing [{0}] object with id [{1}]."), //
	UPDATE_CONFLICTING(Response.Status.NOT_FOUND, "Attempt to update an object with id [{0}] with an object with id [{1}]."), //
	UPDATE_FINAL_STATE(Response.Status.FORBIDDEN, "Updating an [{0}] with id [{1}] that is in a final state."), //

	// Missing values
	MISSING_PROPERTY(Response.Status.BAD_REQUEST, "Missing property: {0}"),
	MISSING_USER_REQUEST_PROPERTY(Response.Status.BAD_REQUEST, "Cannot store userRequest if no {0} is specified."),
	MISSING_USER_PROPERTY(Response.Status.BAD_REQUEST, "Cannot store user if no {0} is specified."),

	// Invalid data
	INVALID_ATTACHMENT_SIZE(Response.Status.BAD_REQUEST, "Attachment size after compression ({0} bytes) is larger than the maximum attachment size ({1} bytes)"), //
	INVALID_DATE_FOR_DATA_COLLECTION_DEFINITION(Response.Status.UNAUTHORIZED, "Data collection with name [{0}] is not open for [{1}]."), //
	INVALID_STATUS_MESSAGE_CONTENT(Response.Status.INTERNAL_SERVER_ERROR, "Invalid status message content: {0}"),
	INVALID_TIMING_UNIT(Response.Status.INTERNAL_SERVER_ERROR, "Unknown timing unit: {0}"),
	INVALID_TIMING_CONDITION(Response.Status.BAD_REQUEST, "Invalid timing condition: {0}. Timing condition must contain a number followed by a timing unit (one of {1})."),
	INVALID_TIMING_EXPRESSION(Response.Status.BAD_REQUEST, "Invalid timing expression: [{0}]."),
	INVALID_TAXONOMY_PATH(Response.Status.BAD_REQUEST, "Invalid taxonomy path: [{0}]."),
	INVALID_USER_UPDATE(Response.Status.BAD_REQUEST, "Invalid user update: {0}"), //
	INVALID_CLIENT_VERSION(Response.Status.BAD_REQUEST, "The clientVersion [{0}] isn't supported."), //
	INVALID_FORMAT(Response.Status.BAD_REQUEST, "The value [{0}] could not be parsed. Valid format is: [{1}]."), //
	INVALID_KEY(Response.Status.BAD_REQUEST, "The key [{0}] is unknown in this context and cannot be parsed."), //
	INVALID_DATE_FORMAT(Response.Status.BAD_REQUEST, "The date [{0}] could not be parsed. Valid date formats are: [{1}]."), //
	INVALID_RANGE_FORMAT(Response.Status.BAD_REQUEST, "The range header doesn''t have the correct formatting, correct formatting is: [int]-[int]."), //
	INVALID_RANGE(Response.Status.BAD_REQUEST, "The 2 range variables must be positive numbers and the end value must surpass the begin value. Begin value is [{0}], end value is [{1}]."), //
	INVALID_INPUT(Response.Status.BAD_REQUEST, "Invalid input: {0}"), //
	INVALID_PLATFORM(Response.Status.BAD_REQUEST, "Invalid platform: {0}"), //
	INVALID_PARAMETER(Response.Status.BAD_REQUEST, "Invalid parameter: {0}"), //
	INVALID_ACTION(Response.Status.BAD_REQUEST, "The action [{0}] is unknown."), //
	INVALID_PROPERTY(Response.Status.BAD_REQUEST, "The property [{0}] with value [{1}] is not valid. Reason: [{2}]."), //
	INVALID_USER_PASSWORD(Response.Status.UNAUTHORIZED, "User property [{0}] cannot be updated if provided password is incorrect."), //
	INVALID_USER_REQUEST_PROPERTY(Response.Status.BAD_REQUEST, "Cannot store userRequest. No valid  value {0}."),
	INVALID_USER_PROPERTY(Response.Status.BAD_REQUEST, "Cannot store user. No valid  value {0}."),
	INVALID_IDENTIFICATION(Response.Status.BAD_REQUEST, "Identification type and value must be both specified or must be left empty."), //

	// Unauthorized
	UNAUTHORIZED_CONFIGURATION_UPDATE(Response.Status.UNAUTHORIZED, "Organization [{0}] is not allowed to update configuration with id [{1}]. Reason: {2}"), //
	UNAUTHORIZED_USER_UPDATE(Response.Status.UNAUTHORIZED, "User with name [{0}] is not allowed to update user with name [{1}]."), //
	UNAUTHORIZED_ACTION_ON_WORKFLOW(Response.Status.BAD_REQUEST, "[{0}] user is not allowed to execute action on workflow [{1}]."), //
	UNAUTHORIZED_COMMENT(Response.Status.UNAUTHORIZED, "User is not allowed to modify or delete this comment."),

	// Action not allowed on registration
	ACTION_NOT_ALLOWED_ON_NEW_REGISTRATION(Response.Status.BAD_REQUEST, "The action [{0}] isn''t allowed on a new workflow."), //
	ACTION_NOT_ALLOWED(Response.Status.BAD_REQUEST, "The action [{0}] can not be executed on the workflow."), //
	ACTION_NOT_ALLOWED_VALIDATION(Response.Status.BAD_REQUEST, "The action [{0}] can not be executed on the workflow because it contains validation errors."), //
	ACTION_NOT_ALLOWED_ON_STATUS(Response.Status.BAD_REQUEST, "The action [{0}] isn''t allowed on a workflow with status: [{1}]."), //
	ACTION_NOT_ALLOWED_ON_PLATFORM(Response.Status.BAD_REQUEST, "The action: [{0}] isn''t allowed on platform: [{1}]."), //

	// Conflict
	USERNAME_ALREADY_EXISTS(Response.Status.CONFLICT, "A user with username [{0}] already exists."), //
	EMAIL_ALREADY_EXISTS(Response.Status.CONFLICT, "A user with email address [{0}] already exists."), //
	PARALLEL_MODIFICATION(Response.Status.CONFLICT, "Cannot save document due to parallel modifications. Please reopen document and proceed with your changes. Last version in the database is from [{0}] while document to be saved is based on the version of [{1}]"), //

	// Json
	MARSHALLING_EXCEPTION(Response.Status.INTERNAL_SERVER_ERROR, "Error while marshalling [{0}] object: [{1}]."),
	JSON_PARSE_EXCEPTION(Response.Status.INTERNAL_SERVER_ERROR, "Error while parsing json."),
	XML_PARSE_EXCEPTION(Response.Status.INTERNAL_SERVER_ERROR, "Error while parsing xml."),
	JSON_EXCEPTION(Response.Status.INTERNAL_SERVER_ERROR, "Error while executing operation on json element: {0}"),

	// ADT
	ADT(Response.Status.INTERNAL_SERVER_ERROR, "Error while executing ADT operation: {0}"), //

	// SalesForce
	SALESFORCE_GET(Response.Status.SERVICE_UNAVAILABLE, "Error while retrieving [{0}] from the salesforce server."), //
	SALESFORCE_CREATE(Response.Status.SERVICE_UNAVAILABLE, "Error while creating [{0}] on the salesforce server."), //

	// LDAP
	LDAP_EXCEPTION(Response.Status.INTERNAL_SERVER_ERROR, "LDAP exception: [{0}]."),
	LDAP_USER_REQUEST_EXCEPTION(Response.Status.CONFLICT, "LDAP not configured correct to accept this user request: [{0}]."),

	// ElasticSearch
	ELASTICSEARCH_EXCEPTION(Response.Status.INTERNAL_SERVER_ERROR, "Error while executing operation on Elastic Search : {0}"), //
	ELASTICSEARCH_PUT_MAPPING_EXCEPTION(Response.Status.INTERNAL_SERVER_ERROR, "Error while putting mapping on index [{0}] in Elastic Search. Response: [{1}]"), //
	ELASTICSEARCH_CREATE_INDEX_EXCEPTION(Response.Status.INTERNAL_SERVER_ERROR, "Error while creating index [{0}] in Elastic Search. Response: [{1}]"), //
	ELASTICSEARCH_DELETE_ALL_INDICES_EXCEPTION(Response.Status.INTERNAL_SERVER_ERROR, "Error while deleting all indices in Elastic Search. Response: [{0}]"), //
	ELASTICSEARCH_DELETE_INDEX_EXCEPTION(Response.Status.INTERNAL_SERVER_ERROR, "Error while deleting index {0} in Elastic Search. Response: [{1}]"), //
	ELASTICSEARCH_INDEX_EXCEPTION(Response.Status.INTERNAL_SERVER_ERROR, "Error while indexing data: {0}"), //
	ELASTICSEARCH_DELETE_EXCEPTION(Response.Status.INTERNAL_SERVER_ERROR, "Error while deleting data: {0}"), //

	// Error while...
	ERROR_WHILE_UPLOADING_STABLE_DATA(Response.Status.BAD_REQUEST, "Error while uploading stable data: {0}."),
	ERROR_WHILE_PROCESSING_MESSAGE(Response.Status.INTERNAL_SERVER_ERROR, "Error while processing message {0}: {1}"), //
	ERROR_WHILE_CREATING_STATUS_MESSAGE_ACTION(Response.Status.INTERNAL_SERVER_ERROR, "Error while creating status message action: {0}"), //
	ERROR_WHILE_PROCESSING_STATUS_MESSAGE_ACTION(Response.Status.INTERNAL_SERVER_ERROR, "Error while processing status message action: {0}"), //
	ERROR_WHILE_LOADING_KEY(Response.Status.INTERNAL_SERVER_ERROR, "Error while loading encryption key."), //
	ERROR_WHILE_LOADING_KEYSTORE(Response.Status.INTERNAL_SERVER_ERROR, "Error while loading key store."), //
	ERROR_WHILE_READING_DATA(Response.Status.INTERNAL_SERVER_ERROR, "Error while reading data: [{0}]."), //
	ERROR_WHILE_READING_FILE(Response.Status.INTERNAL_SERVER_ERROR, "Error while reading file: [{0}]."), //
	ERROR_WHILE_WRITING_TO_FILE(Response.Status.INTERNAL_SERVER_ERROR, "Error while writing to file: [{0}]."),
	ERROR_WHILE_SENDING_MAIL(Response.Status.INTERNAL_SERVER_ERROR, "Error while sending mail: [{0}]."),


	// Other
	ASSERTION_ERROR(Response.Status.INTERNAL_SERVER_ERROR, "{0}"),
	INVALID_CENTRAL_PLATFORM_STATUS_CODE(Response.Status.INTERNAL_SERVER_ERROR, "The central platform returned status code {0}."),
	MAPPING_CONNECTION(Response.Status.INTERNAL_SERVER_ERROR, "Could not connect to the mapping API."),
	INVALID_MAPPING_STATUS_CODE(Response.Status.INTERNAL_SERVER_ERROR, "The mapping API returned status code {0}."),
	LOGIN_BLOCKED (Response.Status.UNAUTHORIZED, "You can't log in from your IP for 1 hour because of too many failed login attempts."),
	IGNORE_ACTION(Response.Status.BAD_REQUEST, "Ignoring action, reason: {0}"), //
	METHOD_NOT_ALLOWED(Response.Status.BAD_REQUEST, "Operation is not allowed: {0}"), //
	GENERAL_ZIP(Response.Status.INTERNAL_SERVER_ERROR, "Zip exception: {0}"),
	USER_NOT_AUTHENTICATED(Response.Status.UNAUTHORIZED, "User must be authenticated"),
	EXPIRED(Response.Status.NOT_FOUND, "invalid or expired key"),
	NON_UNIQUE_SEARCH_RESULT(Response.Status.INTERNAL_SERVER_ERROR, "{0} [{1}] items found for search parameter(s) [{2}]");

	private Response.Status status;
	private String message;

	ExceptionType(Response.Status status, String message) {
		this.status = status;
		this.message = message;
	}

	public Response.Status getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}
}
