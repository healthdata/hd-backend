/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.domain.enumeration;


import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;

import java.util.Calendar;

public enum TimeUnit {

    SECOND("s", "second", Calendar.SECOND),
    MINUTE("m", "minute", Calendar.MINUTE),
    HOUR("h", "hour", Calendar.HOUR_OF_DAY),
    DAY("d", "day", Calendar.DAY_OF_YEAR),
    WEEK("w", "week", Calendar.WEEK_OF_YEAR),
    MONTH("M", "month", Calendar.MONTH),
    YEAR("y", "year", Calendar.YEAR);

    private final String label;
    private final String text;
    private final int calendarUnit;

    TimeUnit(String label, String text, int calendarUnit) {
        this.label = label;
        this.text = text;
        this.calendarUnit = calendarUnit;
    }

    public String getLabel() {
        return label;
    }

    public String getText() {
        return text;
    }

    public int getCalendarUnit() {
        return calendarUnit;
    }

    public static TimeUnit getByLabel(String label) {
        for (TimeUnit timeUnit : TimeUnit.values()) {
            if(timeUnit.getLabel().equals(label)) {
                return timeUnit;
            }
        }
        HealthDataException exception = new HealthDataException();
        exception.setExceptionType(ExceptionType.INVALID_TIMING_UNIT, label);
        throw exception;
    }

    @Override
    public String toString() {
        return getLabel();
    }
}
