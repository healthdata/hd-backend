/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.dao.impl;

import be.wiv_isp.healthdata.common.dao.ICrudDaoV2;
import be.wiv_isp.healthdata.common.domain.search.IEntitySearch;
import be.wiv_isp.healthdata.common.domain.search.Ordering;
import be.wiv_isp.healthdata.common.domain.search.QueryResult;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.pagination.PaginationRange;
import org.apache.commons.collections4.CollectionUtils;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.List;

public abstract class CrudDaoV2<Entity, Key extends Serializable, Search extends IEntitySearch> extends CrudDao<Entity, Key> implements ICrudDaoV2<Entity, Key, Search> {

	public CrudDaoV2(Class<Entity> entityClass) {
		super(entityClass);
	}

	@Override
	public List<Entity> getAll() {
		return getAll(null);
	}

	@Override
	public List<Entity> getAll(Search search) {
		return getAllAsQueryResult(search).getResult();
	}

	@Override
	public QueryResult<Entity> getAllAsQueryResult(Search search) {
		boolean distinct = (search == null || search.getDistinct() == null) ? defaultDistinctValue() : search.getDistinct();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Entity> cq = cb.createQuery(entityClass).distinct(distinct);
		Root<Entity> rootEntry = cq.from(entityClass);
		fetchAttributes(rootEntry);

		TypedQuery<Entity> allQuery;
		List<Predicate> predicates = null;

		if(search == null) {
			allQuery = em.createQuery(cq);
		} else {
			predicates = getPredicates(search, cb, rootEntry);
			if (CollectionUtils.isNotEmpty(predicates)) {
				cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
			}

			if(search.getOrdering() != null) {
				final String field = search.getOrdering().getField().toString();
				final Ordering.Type type = search.getOrdering().getType();
				if(Ordering.Type.ASC.equals(type)) {
					cq.orderBy(cb.asc(rootEntry.get(field)));
				} else if(Ordering.Type.DESC.equals(type)) {
					cq.orderBy(cb.desc(rootEntry.get(field)));
				}
			}

			allQuery = em.createQuery(cq);

			if(search.getOffset() != null) {
				allQuery.setFirstResult(search.getOffset());
			}

			if(search.getMaxResults() != null) {
				allQuery.setMaxResults(search.getMaxResults());
			}

		}

		final QueryResult<Entity> queryResult = new QueryResult<>();
		queryResult.setResult(allQuery.getResultList());
		if(search != null && search.isPaginated()){
			final PaginationRange range = search.getPaginationRange();
			range.setTotalCount(count(search));
			queryResult.setRange(range);
		}
		return queryResult;
	}

	@Override
	public long count(Search search) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Entity> rootEntry = cq.from(entityClass);

		List<Predicate> predicates;
		if(search != null) {
			predicates = getPredicates(search, cb, rootEntry);
			if (!predicates.isEmpty()) {
				cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
			}
		}

		CriteriaQuery<Long> count = cq.select(cb.countDistinct(rootEntry));
		TypedQuery<Long> countQuery = em.createQuery(count);
		return countQuery.getSingleResult();
	}

	@Override
	public Entity getUnique(Search search) {
		final List<Entity> all = getAll(search);

		if (CollectionUtils.isEmpty(all)) {
			return null;
		}

		if (all.size() > 1) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.NON_UNIQUE_SEARCH_RESULT, all.size(), entityClass.getName(), search);
			throw exception;
		}

		return all.get(0);
	}

	protected void fetchAttributes(Root<Entity> rootEntry) {

	}

	abstract protected List<Predicate> getPredicates(Search search, CriteriaBuilder cb, Root<Entity> rootEntry);

	protected boolean defaultDistinctValue() {
		return false;
	}
}
