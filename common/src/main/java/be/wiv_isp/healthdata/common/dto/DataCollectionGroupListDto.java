/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.common.dto;

import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class DataCollectionGroupListDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private String dataCollectionGroupName;
    private Long dataCollectionGroupId;
    private int majorVersion;
    private TranslatableStringDto label;
    private TranslatableStringDto description;
    private PeriodDto period;
    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp startDate;
    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp endDateCreation;
    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp endDateSubmission;
    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp endDateComments;
    private List<DataCollectionDefinitionListDto> dataCollectionDefinitions;
    private Long participationId;

    public String getDataCollectionGroupName() {
        return dataCollectionGroupName;
    }

    public void setDataCollectionGroupName(String dataCollectionGroupName) {
        this.dataCollectionGroupName = dataCollectionGroupName;
    }

    public Long getDataCollectionGroupId() {
        return dataCollectionGroupId;
    }

    public void setDataCollectionGroupId(Long dataCollectionGroupId) {
        this.dataCollectionGroupId = dataCollectionGroupId;
    }

    public int getMajorVersion() {
        return majorVersion;
    }

    public void setMajorVersion(int majorVersion) {
        this.majorVersion = majorVersion;
    }

    public TranslatableStringDto getLabel() {
        return label;
    }

    public void setLabel(TranslatableStringDto label) {
        this.label = label;
    }

    public TranslatableStringDto getDescription() {
        return description;
    }

    public void setDescription(TranslatableStringDto description) {
        this.description = description;
    }

    public PeriodDto getPeriod() {
        return period;
    }

    public void setPeriod(PeriodDto period) {
        this.period = period;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDateCreation() {
        return endDateCreation;
    }

    public void setEndDateCreation(Timestamp endDateCreation) {
        this.endDateCreation = endDateCreation;
    }

    public Timestamp getEndDateSubmission() {
        return endDateSubmission;
    }

    public void setEndDateSubmission(Timestamp endDateSubmission) {
        this.endDateSubmission = endDateSubmission;
    }

    public Timestamp getEndDateComments() {
        return endDateComments;
    }

    public void setEndDateComments(Timestamp endDateComments) {
        this.endDateComments = endDateComments;
    }

    public List<DataCollectionDefinitionListDto> getDataCollectionDefinitions() {
        return dataCollectionDefinitions;
    }

    public void setDataCollectionDefinitions(List<DataCollectionDefinitionListDto> dataCollectionDefinitions) {
        this.dataCollectionDefinitions = dataCollectionDefinitions;
    }

    public Long getParticipationId() {
        return participationId;
    }

    public void setParticipationId(Long participationId) {
        this.participationId = participationId;
    }

    @JsonIgnore
    public DataCollectionDefinitionListDto get(String dataCollectionName) {
        for (DataCollectionDefinitionListDto dataCollectionDefinition : dataCollectionDefinitions) {
            if(dataCollectionDefinition.getDataCollectionName().equalsIgnoreCase(dataCollectionName)) {
                return dataCollectionDefinition;
            }
        }
        return null;
    }
}
