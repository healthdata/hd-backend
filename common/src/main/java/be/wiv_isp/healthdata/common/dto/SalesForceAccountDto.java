/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.dto;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.util.Iterator;

public class SalesForceAccountDto {

	public String Name;
	public String ParentId;
	public String HealthData_ID_Type__c;
	public String RIZIV_Number__c;
	public String MultiCenterHost__c;
	public boolean HD4PrC_Installation__c;
	public boolean Test__c;

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getParentId() {
		return ParentId;
	}

	public void setParentId(String parentId) {
		ParentId = parentId;
	}

	public String getHealthData_ID_Type__c() {
		return HealthData_ID_Type__c;
	}

	public void setHealthData_ID_Type__c(String healthData_ID_Type__c) {
		HealthData_ID_Type__c = healthData_ID_Type__c;
	}

	public String getRIZIV_Number__c() {
		return RIZIV_Number__c;
	}

	public void setRIZIV_Number__c(String RIZIV_Number__c) {
		this.RIZIV_Number__c = RIZIV_Number__c;
	}

	public String getMultiCenterHost__c() {
		return MultiCenterHost__c;
	}

	public void setMultiCenterHost__c(String multiCenterHost__c) {
		MultiCenterHost__c = multiCenterHost__c;
	}

	public boolean isHD4PrC_Installation__c() {
		return HD4PrC_Installation__c;
	}

	public void setHD4PrC_Installation__c(boolean HD4PrC_Installation__c) {
		this.HD4PrC_Installation__c = HD4PrC_Installation__c;
	}

	public boolean isTest__c() {
		return Test__c;
	}

	public void setTest__c(boolean test__c) {
		Test__c = test__c;
	}

	static public SalesForceAccountDto map(JSONObject json) throws JSONException{
		SalesForceAccountDto dto = new SalesForceAccountDto();
		for (Iterator<String> iterator = json.keys(); iterator.hasNext();) {
			String key = iterator.next();
			if (!"null".equalsIgnoreCase(json.getString(key))) {
				if("Name".equalsIgnoreCase(key)) {
					dto.setName(json.getString(key));
				} else if("ParentId".equalsIgnoreCase(key)) {
					dto.setParentId(json.getString(key));
				} else if("HealthData_ID_Type__c".equalsIgnoreCase(key)) {
					dto.setHealthData_ID_Type__c(json.getString(key));
				} else if("RIZIV_Number__c".equalsIgnoreCase(key)) {
					dto.setRIZIV_Number__c(json.getString(key));
				} else if("MultiCenterHost__c".equalsIgnoreCase(key)) {
					dto.setMultiCenterHost__c(json.getString(key));
				} else if("HD4PrC_Installation__c".equalsIgnoreCase(key)) {
					dto.setHD4PrC_Installation__c(json.getBoolean(key));
				} else if("Test__c".equalsIgnoreCase(key)) {
					dto.setTest__c(json.getBoolean(key));
				}
			}
		}
		return dto;
	}
}
