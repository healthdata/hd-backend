/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.domain.enumeration;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public enum DateFormat {

	TIME("HH:mm"), //

	DATE("yyyy-MM-dd"), //
	DATE_AND_TIME("yyyy-MM-dd'T'HH:mm:ssXXX"), //

	TIMESTAMP("yyyyMMddHHmmssSSS"), //
	DAY_TIMESTAMP("yyyy_MM_dd"), //

	DDMMYYYY("dd/MM/yyyy"), //
	YY("YY"), //

	@Deprecated
	OLD_DATE("dd/MM/yyyy"), //

	@Deprecated
	DATE_AND_TIME_WITH_MS("yyyy-MM-dd HH:mm:ss.SSS"), //

	@Deprecated
	DATE_AND_TIME_NON_ISO("yyyy-MM-dd HH:mm:ss"); //

	private String pattern;

	DateFormat(String pattern) {
		this.pattern = pattern;
	}

	public String getPattern() {
		return pattern;
	}

	@Override
	public String toString() {
		return pattern;
	}

	public static Date stringToDate(String dateAsString, DateFormat dateFormat) {
		try {
			return new SimpleDateFormat(dateFormat.getPattern()).parse(dateAsString);
		} catch (ParseException e) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, dateAsString, dateFormat.getPattern());
			throw exception;
		}
	}

	public Date parse(String string) {
		return stringToDate(string, this);
	}

	public String format(Date date) {
		if(date == null) {
			return null;
		}
		return new SimpleDateFormat(this.getPattern()).format(date);
	}
}
