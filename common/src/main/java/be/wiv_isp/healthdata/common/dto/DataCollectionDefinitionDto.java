/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.dto;

import be.wiv_isp.healthdata.common.json.deserializer.JsonToByteArrayDeserializer;
import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.ByteArrayToJsonSerializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import be.wiv_isp.healthdata.common.util.StringUtils;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class DataCollectionDefinitionDto {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String dataCollectionName;
    private Integer minorVersion;
    private Long dataCollectionGroupId;
    private TranslatableStringDto label;
    private TranslatableStringDto description;
    @JsonSerialize(using = ByteArrayToJsonSerializer.class)
    @JsonDeserialize(using = JsonToByteArrayDeserializer.class)
    private byte[] content;
    private boolean published;
    private List<FollowUpDefinitionDto> followUpDefinitions;
    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp createdOn;
    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp updatedOn;

    public DataCollectionDefinitionDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDataCollectionName() {
        return dataCollectionName;
    }

    public void setDataCollectionName(String dataCollectionName) {
        this.dataCollectionName = dataCollectionName;
    }

    public Long getDataCollectionGroupId() {
        return dataCollectionGroupId;
    }

    public void setDataCollectionGroupId(Long dataCollectionGroupId) {
        this.dataCollectionGroupId = dataCollectionGroupId;
    }

    public TranslatableStringDto getLabel() {
        return label;
    }

    public void setLabel(TranslatableStringDto label) {
        this.label = label;
    }

    public TranslatableStringDto getDescription() {
        return description;
    }

    public void setDescription(TranslatableStringDto description) {
        this.description = description;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public Integer getMinorVersion() {
        return minorVersion;
    }

    public void setMinorVersion(Integer minorVersion) {
        this.minorVersion = minorVersion;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public List<FollowUpDefinitionDto> getFollowUpDefinitions() {
        return followUpDefinitions;
    }

    public void setFollowUpDefinitions(List<FollowUpDefinitionDto> followUpDefinitions) {
        this.followUpDefinitions = followUpDefinitions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataCollectionDefinitionDto that = (DataCollectionDefinitionDto) o;
        return Objects.equals(published,that.published) &&
                Objects.equals(id, that.id) &&
                Objects.equals(dataCollectionName, that.dataCollectionName) &&
                Objects.equals(dataCollectionGroupId, that.dataCollectionGroupId) &&
                Objects.equals(label, that.label) &&
                Objects.equals(description, that.description) &&
                Arrays.equals(content, that.content) &&
                Objects.equals(minorVersion, that.minorVersion) &&
                Objects.equals(createdOn, that.createdOn) &&
                Objects.equals(updatedOn, that.updatedOn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dataCollectionName, dataCollectionGroupId, label, description, content, minorVersion, published, createdOn, updatedOn);
    }

    @Override
    public String toString() {
        return StringUtils.toStringExclude(this, "content", "followUpDefinitions");
    }
}
