/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.rest;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Objects;

public class SerializableMediaType implements Serializable {

    private static final long serialVersionUID = -5127807875104529585L;

    private MediaType mediaType;

    public SerializableMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof SerializableMediaType) {
            SerializableMediaType other = (SerializableMediaType) o;

            return Objects.equals(mediaType, other.mediaType);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.mediaType);
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.writeObject((mediaType == null) ? null : mediaType.toString());
    }

    private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
        final String mediaTypeAsString = (String) ois.readObject();
        mediaType = (mediaTypeAsString == null) ? null : MediaType.valueOf(mediaTypeAsString);
    }
}
