/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.util;

import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import com.sun.jersey.api.client.ClientResponse;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

public class HttpComponentStringBuilder {

	public static final String REQUEST_URL = "Request URL";
	public static final String REQUEST_METHOD = "Request Method";
	public static final String REQUEST_HEADERS = "Request Headers";
	public static final String QUERY_PARAMETERS = "Query Parameters";
	public static final String CONTENT = "Content";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String RESPONSE_STATUS = "Response status";
	public static final String RESPONSE_HEADERS = "Response Headers";
	public static final String RESPONSE_BODY = "Response body";
	public static final String ACCEPT = "Accept";
	public static final String SEMI_COLUMN_WITH_SPACE = ": ";
	public static final String NEW_LINE = "\n";
	public static final String TABULATION = "\t";
	public static final String GET = "GET";
	public static final String POST = "POST";
	public static final String PUT = "PUT";
	public static final String DELETE = "DELETE";
	public static final String EMPTY_STRING = "";

	public static String clientResponseToString(ClientResponse response, boolean withResponseBody) {
		final StringBuilder sb = new StringBuilder();

		sb.append(RESPONSE_STATUS).append(SEMI_COLUMN_WITH_SPACE).append(response.getStatus()).append(NEW_LINE);

		if (response.getHeaders() != null && !response.getHeaders().isEmpty()) {
			sb.append(RESPONSE_HEADERS).append(SEMI_COLUMN_WITH_SPACE).append(NEW_LINE);
			for (Map.Entry<String, List<String>> entry : response.getHeaders().entrySet()) {
				sb.append(TABULATION).append(entry.getKey()).append(SEMI_COLUMN_WITH_SPACE).append(entry.getValue()).append(NEW_LINE);
			}
		}

		String responseBody;
		if(withResponseBody) {
			try {
				response.bufferEntity();
				final InputStream entityInputStream = response.getEntityInputStream();
				responseBody = IOUtils.toString(entityInputStream, StandardCharsets.UTF_8);
				entityInputStream.reset();
			} catch (IOException e) {
				responseBody = "[Response body could not be retrieved]";
			}
		} else {
			responseBody = "[Response body was not logged]";
		}

		sb.append(RESPONSE_BODY).append(SEMI_COLUMN_WITH_SPACE).append(NEW_LINE).append(StringEscapeUtils.escapeXml10(responseBody)).append(NEW_LINE);

		return sb.toString();
	}

	public static String webServiceBuilderToString(WebServiceBuilder wsb) {
		final StringBuilder sb = new StringBuilder();

		sb.append(REQUEST_URL).append(SEMI_COLUMN_WITH_SPACE).append(wsb.getUrl()).append(NEW_LINE);
		sb.append(REQUEST_METHOD).append(SEMI_COLUMN_WITH_SPACE).append(mapRequestMethodString(wsb)).append(NEW_LINE);

		if (wsb.getHeader() != null && !wsb.getHeader().isEmpty()) {
			sb.append(REQUEST_HEADERS).append(SEMI_COLUMN_WITH_SPACE).append(NEW_LINE);
			for (Map.Entry<HttpHeaders, String> entry : wsb.getHeader().entrySet()) {
				if(HttpHeaders.AUTHORIZATION.equals(entry.getKey()) || HttpHeaders.REGISTRATION_TOKEN.equals(entry.getKey())) {
					sb.append(TABULATION).append(entry.getKey().getValue()).append(SEMI_COLUMN_WITH_SPACE).append("***").append(NEW_LINE);
				} else {
					sb.append(TABULATION).append(entry.getKey().getValue()).append(SEMI_COLUMN_WITH_SPACE).append(entry.getValue()).append(NEW_LINE);
				}
			}
		}
		if (wsb.getAccept() != null) {
			sb.append(ACCEPT).append(SEMI_COLUMN_WITH_SPACE).append(wsb.getAccept()).append(NEW_LINE);
		}
		if (wsb.getType() != null) {
			sb.append(CONTENT_TYPE).append(SEMI_COLUMN_WITH_SPACE).append(wsb.getType()).append(NEW_LINE);
		}

		if (wsb.getPublicParameters() != null && !wsb.getPublicParameters().isEmpty()) {
			sb.append(QUERY_PARAMETERS).append(SEMI_COLUMN_WITH_SPACE).append(NEW_LINE);
			for (Map.Entry<String, String> entry : wsb.getPublicParameters().entrySet()) {
				sb.append(TABULATION).append(entry.getKey()).append(SEMI_COLUMN_WITH_SPACE).append(entry.getValue()).append(NEW_LINE);
			}
		}

		if (wsb.getPrivateParameters() != null && !wsb.getPrivateParameters().isEmpty()) {
			sb.append(QUERY_PARAMETERS).append(SEMI_COLUMN_WITH_SPACE).append(NEW_LINE);
			for (Map.Entry<String, String> entry : wsb.getPrivateParameters().entrySet()) {
				sb.append(TABULATION).append(entry.getKey()).append(SEMI_COLUMN_WITH_SPACE).append("***").append(NEW_LINE);
			}
		}

		if (wsb.getJson() != null) {
			String content;
			try {
				content = new ObjectMapper().writeValueAsString(wsb.getJson());
			} catch (IOException e) {
				content = "[content could not be retrieved]";
			}
			sb.append(CONTENT).append(SEMI_COLUMN_WITH_SPACE).append(NEW_LINE).append(content);
		}

		return sb.toString();
	}

	private static String mapRequestMethodString(WebServiceBuilder wsb) {
		if (wsb.isGet()) {
			return GET;
		}

		if (wsb.isPost()) {
			return POST;
		}

		if (wsb.isPut()) {
			return PUT;
		}

		if (wsb.isDelete()) {
			return DELETE;
		}

		return EMPTY_STRING;
	}
}
