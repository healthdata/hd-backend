/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * Created with IntelliJ IDEA. User: Mathias Date: 21/10/14 Time: 10:30 To
 * change this template use File | Settings | File Templates.
 */
public class CorsFilter extends OncePerRequestFilter {

	private static final String SERVERHEADER = "Server";
	private static final String XFRAMEOPTIONS = "X-Frame-Options";

	private static final Logger LOG = LoggerFactory.getLogger(CorsFilter.class);

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
		response.setHeader("Access-Control-Max-Age", "10");
		response.setHeader("Access-Control-Expose-Headers", "Content-Range");

		// Don't expose internal server information
		response.setHeader(SERVERHEADER, "-");
		// Prevent Clickjacking attacks
		response.setHeader(XFRAMEOPTIONS, "SAMEORIGIN");

		String reqHead = request.getHeader("Access-Control-Request-Headers");

		if (!StringUtils.isEmpty(reqHead)) {
			response.setHeader("Access-Control-Allow-Headers", reqHead);
		}
		if ("OPTIONS".equals(request.getMethod())) {
			try {
				response.getWriter().print("OK");
				response.getWriter().flush();
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
			}
		} else {
			filterChain.doFilter(request, response);
		}
	}
}