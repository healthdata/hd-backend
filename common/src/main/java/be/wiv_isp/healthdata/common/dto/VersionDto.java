/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.dto;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Objects;

public class VersionDto implements Serializable, Comparable<VersionDto> {

	private static final long serialVersionUID = 1L;

	private int major;

	private int minor;

	public VersionDto() {

	}

	public VersionDto(int major, int minor) {
		this.major = major;
		this.minor = minor;
	}

	public int getMajor() {
		return major;
	}

	public void setMajor(int major) {
		this.major = major;
	}

	public int getMinor() {
		return minor;
	}

	public void setMinor(int minor) {
		this.minor = minor;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		VersionDto that = (VersionDto) o;
		return Objects.equals(major, that.major) && Objects.equals(minor, that.minor);
	}

	@Override
	public int hashCode() {
		return Objects.hash(major, minor);
	}

	@Override
	public String toString() {
		return "Version [" +
				"major=" + major +
				", minor=" + minor +
				']';
	}

	public String toStringCompact() {
		return MessageFormat.format("{0}.{1}", major, minor);
	}


	@Override
	public int compareTo(VersionDto other) {
		if(this.getMajor() > other.getMajor()) {
			return 1;
		}
		if(this.getMajor() < other.getMajor()) {
			return -1;
		}
		if(this.getMinor() > other.getMinor()) {
			return 1;
		}
		if(this.getMinor() < other.getMinor()) {
			return -1;
		}
		return 0;
	}
}
