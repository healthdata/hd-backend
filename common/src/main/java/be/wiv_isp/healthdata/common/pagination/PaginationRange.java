/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.pagination;

public class PaginationRange {

	private final int from;
	private final int to;
	private long totalCount;

	public PaginationRange(int from, int to) {
		this.from = from;
		this.to = to;
	}

	public PaginationRange(PaginationRange paginationRange) {
		this.from = paginationRange.from;
		this.to = paginationRange.to;
		this.totalCount = paginationRange.totalCount;
	}

	public int getFrom() {
		return from;
	}

	public int getTo() {
		return to;
	}

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
}
