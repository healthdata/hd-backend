/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.common.util;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class ZipUtils {

    private static final Logger LOG = LoggerFactory.getLogger(ZipUtils.class);

    public static byte[] compress(byte[] data) {
        LOG.debug("Compressing {} bytes of data", data.length);

        Deflater deflater = new Deflater();
        deflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        deflater.finish();
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        try {
            outputStream.close();
        } catch (IOException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.GENERAL_ZIP, "error while compressing data");
            throw exception;
        }
        byte[] output = outputStream.toByteArray();
        LOG.debug("Original data: " + data.length + " bytes");
        LOG.debug("Compressed: " + output.length + " bytes");
        return output;
    }

    public static byte[] decompress(byte[] data) {
        LOG.debug("Decompressing {} bytes of data", data.length);

        Inflater inflater = new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        while (!inflater.finished()) {
            int count = 0;
            try {
                count = inflater.inflate(buffer);
            } catch (DataFormatException e) {
                HealthDataException exception = new HealthDataException(e);
                exception.setExceptionType(ExceptionType.GENERAL_ZIP, "error while decompressing data");
                throw exception;
            }
            outputStream.write(buffer, 0, count);
        }
        try {
            outputStream.close();
        } catch (IOException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.GENERAL_ZIP, "error while decompressing data");
            throw exception;
        }
        byte[] output = outputStream.toByteArray();
        LOG.debug("Compressed: " + data.length + " bytes");
        LOG.debug("Decompressed: " + output.length + " bytes");
        return output;
    }
}
