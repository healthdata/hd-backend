/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.common.dto;

import be.wiv_isp.healthdata.common.domain.DateRange;
import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

public class DataCollectionGroupDtoV7 implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String name;
	private TranslatableStringDto label;
	private TranslatableStringDto description;
	private Integer majorVersion;
	private PeriodDto period;
	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	private Timestamp createdOn;
	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	private Timestamp updatedOn;
	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	private Timestamp startDate;
	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	private Timestamp endDateCreation;
	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	private Timestamp endDateSubmission;
	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	private Timestamp endDateComments;
	private ParticipationDefinitionDto participationDefinition;


	public DataCollectionGroupDtoV7() {
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TranslatableStringDto getLabel() {
		return label;
	}

	public void setLabel(TranslatableStringDto label) {
		this.label = label;
	}

	public TranslatableStringDto getDescription() {
		return description;
	}

	public void setDescription(TranslatableStringDto description) {
		this.description = description;
	}

	public Integer getMajorVersion() {
		return majorVersion;
	}

	public void setMajorVersion(Integer majorVersion) {
		this.majorVersion = majorVersion;
	}

	public PeriodDto getPeriod() {
		return period;
	}

	public void setPeriod(PeriodDto period) {
		this.period = period;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDateCreation() {
		return endDateCreation;
	}

	public void setEndDateCreation(Timestamp endDateCreation) {
		this.endDateCreation = endDateCreation;
	}

	public Timestamp getEndDateSubmission() {
		return endDateSubmission;
	}

	public void setEndDateSubmission(Timestamp endDateSubmission) {
		this.endDateSubmission = endDateSubmission;
	}

	public Timestamp getEndDateComments() {
		return endDateComments;
	}

	public void setEndDateComments(Timestamp endDateComments) {
		this.endDateComments = endDateComments;
	}

	public ParticipationDefinitionDto getParticipationDefinition() {
		return participationDefinition;
	}

	public void setParticipationDefinition(ParticipationDefinitionDto participationDefinition) {
		this.participationDefinition = participationDefinition;
	}

	public boolean isValidForCreation(Date date) {
		return DateRange.isBetween(startDate, date, endDateCreation);
	}

	public boolean isValidForSubmission(Date date) {
		return DateRange.isBetween(startDate, date, endDateSubmission);
	}

	public boolean isValidForComments(Date date) {
		return DateRange.isBetween(startDate, date, endDateComments);
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof DataCollectionGroupDtoV7) {
			DataCollectionGroupDtoV7 other = (DataCollectionGroupDtoV7) o;

			return Objects.equals(id, other.id)
					&& Objects.equals(name, other.name)
					&& Objects.equals(label, other.label)
					&& Objects.equals(description, other.description)
					&& Objects.equals(majorVersion, other.majorVersion)
					&& Objects.equals(period, other.period)
					&& Objects.equals(createdOn, other.createdOn)
					&& Objects.equals(updatedOn, other.updatedOn);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				this.id,
				this.name,
				this.label,
				this.description,
				this.majorVersion,
				this.period,
				this.createdOn,
				this.updatedOn);
	}
}
