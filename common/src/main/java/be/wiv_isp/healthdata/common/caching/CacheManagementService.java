/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.common.caching;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class CacheManagementService implements ICacheManagementService {

    public static final String dataCollectionDefinitionLatestCache = "dataCollectionDefinitionLatestCache";
    public static final String organizationCache = "organizationCache";
    public static final String templateCache = "templateCache";
    public static final String dataReferenceCache = "dataReferenceCache";
    public static final String dcdNameCache = "dcdNameCache";
    public static final String catalogueCache = "catalogueCache";
    public static final String translationCache = "translationCache";
    public static final String configurationCache = "configurationCache";
    public static final String salesForceCache = "salesForceCache";
    public static final String dataCollectionGroupCache = "dataCollectionGroupCache";
    public static final String salesForceOrganizationNameCache = "salesForceOrganizationNameCache";
    public static final String pdfCache = "pdfCache";
    public static final String dataCollectionCache = "dataCollectionCache";
    public static final String registryDependentIdCache = "registryDependentIdCache";


    @Autowired
    private CacheManager cacheManager;

    private final CacheKeyGenerator cacheKeyGenerator = new CacheKeyGenerator();

    @Override
    public void evict(String cacheName) {
        try {
            cacheManager.getCache(cacheName).clear();
        }
        catch (NullPointerException e) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.UPDATE_NON_EXISTING, "cache", cacheName);
            throw exception;
        }
    }

    @Override
    public CacheKeyGenerator getCacheKeyGenerator() {
        return cacheKeyGenerator;
    }

    @Override
    public List<String> getCacheNames(Platform platform) {
        switch (platform) {
            case HD4DP:
                return Arrays.asList(organizationCache, templateCache, dataReferenceCache, dcdNameCache, catalogueCache, translationCache, configurationCache, salesForceOrganizationNameCache, pdfCache, dataCollectionCache, registryDependentIdCache, dataCollectionGroupCache);
            case HD4RES:
                return Arrays.asList(organizationCache, templateCache, dataReferenceCache, dcdNameCache, catalogueCache, translationCache, configurationCache, salesForceOrganizationNameCache, pdfCache, dataCollectionCache, salesForceCache, dataCollectionGroupCache);
            case CATALOGUE:
                return Arrays.asList(dataCollectionDefinitionLatestCache, translationCache, configurationCache, salesForceCache, dataCollectionGroupCache);
            default:
                return Collections.emptyList();
        }
    }

}
