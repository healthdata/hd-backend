/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.common.dto;

import be.wiv_isp.healthdata.common.domain.DateRange;
import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.deserializer.JsonToByteArrayDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.ByteArrayToJsonSerializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import be.wiv_isp.healthdata.common.util.StringUtils;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class DataCollectionGroupDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String name;

    private TranslatableStringDto label;

    private TranslatableStringDto description;

    private Integer majorVersion;

    private Long participationId;

    private List<Long> dataCollectionDefinitions;

    private boolean published;

    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp startDate;

    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp endDateCreation;

    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp endDateSubmission;

    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp endDateComments;

    @JsonSerialize(using = ByteArrayToJsonSerializer.class)
    @JsonDeserialize(using = JsonToByteArrayDeserializer.class)
    private byte[] participationContent;

    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp createdOn;

    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp updatedOn;

    private PeriodDto period;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TranslatableStringDto getLabel() {
        return label;
    }

    public void setLabel(TranslatableStringDto label) {
        this.label = label;
    }

    public TranslatableStringDto getDescription() {
        return description;
    }

    public void setDescription(TranslatableStringDto description) {
        this.description = description;
    }

    public Integer getMajorVersion() {
        return majorVersion;
    }

    public void setMajorVersion(Integer majorVersion) {
        this.majorVersion = majorVersion;
    }

    public List<Long> getDataCollectionDefinitions() {
        return dataCollectionDefinitions;
    }

    public void setDataCollectionDefinitons(List<Long> dataCollectionDefinitons) {
        this.dataCollectionDefinitions = dataCollectionDefinitons;
    }

    public byte[] getParticipationContent() {
        return participationContent;
    }

    public void setParticipationContent(byte[] participationContent) {
        this.participationContent = participationContent;
    }

    public Long getParticipationId() {
        return participationId;
    }

    public void setParticipationId(Long participationId) {
        this.participationId = participationId;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDateCreation() {
        return endDateCreation;
    }

    public void setEndDateCreation(Timestamp endDateCreation) {
        this.endDateCreation = endDateCreation;
    }

    public Timestamp getEndDateSubmission() {
        return endDateSubmission;
    }

    public void setEndDateSubmission(Timestamp endDateSubmission) {
        this.endDateSubmission = endDateSubmission;
    }

    public Timestamp getEndDateComments() {
        return endDateComments;
    }

    public void setEndDateComments(Timestamp endDateComments) {
        this.endDateComments = endDateComments;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public PeriodDto getPeriod() {
        return period;
    }

    public void setPeriod(PeriodDto period) {
        this.period = period;
    }

    public boolean isValidForCreation(Date date) {
        return DateRange.isBetween(startDate, date, endDateCreation);
    }

    public boolean isValidForSubmission(Date date) {
        return DateRange.isBetween(startDate, date, endDateSubmission);
    }

    public boolean isValidForComments(Date date) {
        return DateRange.isBetween(startDate, date, endDateComments);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataCollectionGroupDto that = (DataCollectionGroupDto) o;

        if (published != that.published) return false;
        if (!id.equals(that.id)) return false;
        if (!name.equals(that.name)) return false;
        if (!label.equals(that.label)) return false;
        if (!description.equals(that.description)) return false;
        if (!majorVersion.equals(that.majorVersion)) return false;
        if (dataCollectionDefinitions != null ? !dataCollectionDefinitions.equals(that.dataCollectionDefinitions) : that.dataCollectionDefinitions != null)
            return false;
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;
        if (endDateCreation != null ? !endDateCreation.equals(that.endDateCreation) : that.endDateCreation != null)
            return false;
        if (endDateSubmission != null ? !endDateSubmission.equals(that.endDateSubmission) : that.endDateSubmission != null)
            return false;
        if (endDateComments != null ? !endDateComments.equals(that.endDateComments) : that.endDateComments != null)
            return false;
        if (!Arrays.equals(participationContent, that.participationContent)) return false;
        if (!createdOn.equals(that.createdOn)) return false;
        if (!period.equals(that.period)) return false;
        return updatedOn.equals(that.updatedOn);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + label.hashCode();
        result = 31 * result + description.hashCode();
        result = 31 * result + majorVersion.hashCode();
        result = 31 * result + (dataCollectionDefinitions != null ? dataCollectionDefinitions.hashCode() : 0);
        result = 31 * result + (published ? 1 : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDateCreation != null ? endDateCreation.hashCode() : 0);
        result = 31 * result + (endDateSubmission != null ? endDateSubmission.hashCode() : 0);
        result = 31 * result + (endDateComments != null ? endDateComments.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(participationContent);
        result = 31 * result + createdOn.hashCode();
        result = 31 * result + updatedOn.hashCode();
        result = 31 * result + (period != null ? period.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return StringUtils.toStringExclude(this, "participationContent");
    }
}
