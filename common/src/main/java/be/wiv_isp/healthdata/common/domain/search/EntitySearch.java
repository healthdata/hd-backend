/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.domain.search;

import be.wiv_isp.healthdata.common.pagination.PaginationRange;
import be.wiv_isp.healthdata.common.util.StringUtils;

public class EntitySearch implements IEntitySearch {

    protected Integer offset;
    protected Integer maxResults;
    protected Ordering ordering;
    protected boolean paginated;
    protected Boolean distinct;

    @Override
    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    @Override
    public Integer getMaxResults() {
        return maxResults;
    }

    public void setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
    }

    @Override
    public Ordering getOrdering() {
        return ordering;
    }

    public void setOrdering(Ordering ordering) {
        this.ordering = ordering;
    }

    @Override
    public boolean isPaginated() {
        return paginated;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public PaginationRange getPaginationRange() {
        if(!isPaginated()) {
            return null;
        }
        int from = this.offset;
        int to = this.offset + this.maxResults - 1;
        return new PaginationRange(from, to);
    }

    public void setPaginationRange(PaginationRange paginationRange) {
        if (paginationRange == null) {
            this.paginated = false;
            return;
        }
        this.offset = paginationRange.getFrom();
        this.maxResults = paginationRange.getTo() - paginationRange.getFrom() + 1;
        this.paginated = true;
    }

    @Override
    public String toString() {
        return StringUtils.toString(this);
    }

    @Override
    public String log() {
        return StringUtils.toString(this);
    }
}
