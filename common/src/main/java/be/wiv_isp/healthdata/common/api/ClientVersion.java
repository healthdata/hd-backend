/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.api;

public class ClientVersion {

	public static class DataCollectionDefinition {
		public static final String _20160503 = "20160503"; // Q2 release
		public static final String _20161031 = "20161031"; // Q3 release
		public static final String _20170301 = "20170301";
		public static String getDefault() {
			return _20170301;
		}
	}

	public static class DataCollection {
		public static final String _20151103 = "20151103";
		public static final String _20170301 = "20170301";
		public static String getDefault() {
			return _20170301;
		}
	}

	public static class Translation {
		public static final String _20150813 = "20150813";
		public static String getDefault() {
			return _20150813;
		}
	}

	public static class Template {
		public static final String _20160510 = "20160510";
		public static String getDefault() {
			return _20160510;
		}
	}

	public static class MultiCenter {
		public static final String _20151103 = "20151103";
		public static String getDefault() {
			return _20151103;
		}
	}

	public static class PdfTemplate {
		public static final String _20161031 = "20161031";
		public static String getDefault() {
			return _20161031;
		}
	}

	public static class Hd4prc {
		public static final String _V1 = "V1";
		public static String getDefault() {
			return _V1;
		}
	}

	public static class CentralPlatform {
		public static final String _20170301 = "20170301";
		public static String getDefault() {
			return _20170301;
		}
	}
}
