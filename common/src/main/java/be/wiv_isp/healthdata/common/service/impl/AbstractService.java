/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.service.impl;

import be.wiv_isp.healthdata.common.dao.ICrudDaoV2;
import be.wiv_isp.healthdata.common.domain.search.IEntitySearch;
import be.wiv_isp.healthdata.common.domain.search.QueryResult;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.service.IAbstractService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

public abstract class AbstractService<Entity, Key extends Serializable, Search extends IEntitySearch, Dao extends ICrudDaoV2<Entity, Key, Search>> implements IAbstractService<Entity, Key, Search> {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractService.class);

    protected Class<Entity> entityClass;

    public AbstractService(Class<Entity> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    @Transactional
    public Entity create(Entity entity) {
        LOG.debug("Creating entity {}", entityClass.getClass().getSimpleName());
        return getDao().create(entity);
    }

    @Override
    public Entity get(Key id) {
        LOG.debug("Getting entity {} with key [{}]", entityClass.getClass().getSimpleName(), id);
        return getDao().get(id);
    }

    @Override
    @Transactional
    public Entity update(Entity entity) {
        LOG.debug("Updating entity {}", entityClass.getClass().getSimpleName());
        return getDao().update(entity);
    }

    @Override
    @Transactional
    public void delete(Entity entity) {
        LOG.debug("Deleting entity {}", entityClass.getClass().getSimpleName());
        getDao().delete(entity);
    }

    @Override
    public List<Entity> getAll() {
        LOG.debug("Retrieving all entities {}", entityClass.getClass().getSimpleName());
        return getDao().getAll(null);
    }

    @Override
     public List<Entity> getAll(Search search) {
        LOG.debug("Searching for entities {} with search parameters: {}", entityClass.getClass().getSimpleName(), search);
        return getDao().getAll(search);
    }

    @Override
    public QueryResult<Entity> getAllAsQueryResult(Search search) {
        LOG.debug("Searching for entities {} with search parameters: {}", entityClass.getClass().getSimpleName(), search);
        return getDao().getAllAsQueryResult(search);
    }

    @Override
    public Entity getUnique(Search search) {
        LOG.debug("Searching for unique entity {} with search parameters: {}", entityClass.getClass().getSimpleName(), search);
        final List<Entity> all = getAll(search);

        if(CollectionUtils.isEmpty(all)) {
            LOG.debug("No entity {} found for search parameters: {}", entityClass.getClass().getSimpleName(), search);
            return null;
        }

        if(all.size() > 1) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.NON_UNIQUE_SEARCH_RESULT, all.size(), entityClass, search);
            throw exception;
        }

        return all.get(0);
    }

    protected abstract Dao getDao();
}
