/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.util;

import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import com.sun.jersey.api.client.GenericType;
import org.codehaus.jettison.json.JSONObject;

import javax.ws.rs.core.MediaType;
import java.text.MessageFormat;

public class TokenUtils {

	public static String getAccessTokenCatalogue(String host, String userName, String password) {
		try {
			WebServiceBuilder wsb = new WebServiceBuilder();
			wsb.setUrl(MessageFormat.format("{0}/oauth/token", host));
			wsb.setPost(true);
			wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
			wsb.addParameter("grant_type", "password");
			wsb.addParameter("client_id", "healthdata-client");
			wsb.addParameter("username", userName);
			wsb.addParameter("password", password);
			wsb.setReturnType(new GenericType<JSONObject>() {});

			JSONObject jsonObject = (JSONObject) WebServiceUtils.callWebService(wsb);

			Object accessToken = jsonObject.get("access_token");
			return accessToken.toString();
		} catch (Exception e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION);
			throw exception;
		}
	}

}
