/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.domain.search;

public class Field {

    public static final Field COUNTER = new Field("counter");
    public static final Field CREATED_ON = new Field("createdOn");
    public static final Field UPDATED_ON = new Field("updatedOn");
    public static final Field EXECUTED_ON = new Field("executedOn");

    public static class StableData {
        public static final Field STATUS = new Field("status");
        public static final Field DATA_COLLECTION_NAME = new Field("dataCollectionName");
        public static final Field IDENTIFICATION_VALUE = new Field("identificationValue");
    }

    public static class Message {
        public static final Field SENT_ON = new Field("sentOn");
    }

    public static class PendingRegistration {
        public static final Field OPEN_SINCE = new Field("openSince");
    }

    private Field(String field) {
        this.field = field;
    }

    private String field;

    @Override
    public String toString() {
        return field;
    }
}
