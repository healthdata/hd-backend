/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.dao.impl;

import be.wiv_isp.healthdata.common.dao.ICrudDao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;

public abstract class CrudDao<Entity, Key extends Serializable> implements ICrudDao<Entity, Key> {

	protected Class<Entity> entityClass;

	@PersistenceContext
	protected EntityManager em;

	public CrudDao(Class<Entity> entityClass) {
		this.entityClass = entityClass;
	}

	@Override
	public Entity create(Entity entity) {
		em.persist(entity);
		em.flush();
		return entity;
	}

	@Override
	public Entity get(Key key) {
		return em.find(entityClass, key);
	}

	@Override
	public Entity update(Entity entity) {
		final Entity updatedEntity = em.merge(entity);
		em.flush();
		return updatedEntity;
	}

	@Override
	public void delete(Entity entity) {
		em.remove(em.merge(entity));
		em.flush();
	}

	@Override
	public void detach(Entity entity) {
		em.detach(entity);
	}

	@Override
	public void refresh(Entity entity) {
		em.refresh(entity);
	}
}
