/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.common.exception;

import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.core.Response;

public class HealthDataAssert {

    public static void assertTrue(boolean value, Response.Status responseStatus, String message, Object... arguments) {
        if(!value) {
            final HealthDataException exception = new HealthDataException();
            exception.setResponseStatus(responseStatus);
            exception.setResponseMessage(message, arguments);
            throw exception;
        }
    }

    public static void assertFalse(boolean value, Response.Status responseStatus, String message, Object... arguments) {
        assertTrue(!value, responseStatus, message, arguments);
    }

    public static void assertNotNull(Object object, Response.Status responseStatus, String message, Object... arguments) {
        assertTrue(object != null, responseStatus, message, arguments);
    }

    public static void assertNull(Object object, Response.Status responseStatus, String message, Object... arguments) {
        assertTrue(object == null, responseStatus, message, arguments);
    }

    public static void assertNotBlank(String value, Response.Status responseStatus, String message, Object... arguments) {
        assertTrue(StringUtils.isNotBlank(value), responseStatus, message, arguments);
    }
}
