/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.common.domain;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;

import java.text.MessageFormat;
import java.util.Date;

public class DateRange {

    public DateRange() {

    }

    public DateRange(Date start, Date end) {
        this.start = start;
        this.end = end;
    }

    private Date start;
    private Date end;

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return MessageFormat.format("DateRange[start={0},end={1}]", DateFormat.DATE_AND_TIME.format(start), DateFormat.DATE_AND_TIME.format(end));
    }

    static public boolean isBetween(Date start, Date toCheck, Date end) {
        if (toCheck == null) {
            return false;
        }
        if (start == null) {
            return false;
        }
        if (end == null) {
            return toCheck.after(start);
        } else {
            return toCheck.after(start) && toCheck.before(end);
        }
    }
}
