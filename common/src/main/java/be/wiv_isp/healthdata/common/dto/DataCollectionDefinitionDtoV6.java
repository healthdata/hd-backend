/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.common.dto;

import be.wiv_isp.healthdata.common.json.deserializer.JsonToByteArrayDeserializer;
import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import be.wiv_isp.healthdata.common.json.serializer.ByteArrayToJsonSerializer;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class DataCollectionDefinitionDtoV6 implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;

	private String dataCollectionName;

	private TranslatableStringDto label;

	private TranslatableStringDto description;

	@JsonSerialize(using = ByteArrayToJsonSerializer.class)
	@JsonDeserialize(using = JsonToByteArrayDeserializer.class)
	private byte[] content;

	private VersionDto version;

	private List<FollowUpDefinitionDto> followUpDefinitions;

	private ParticipationDefinitionDto participationDefinition;

	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	private Timestamp createdOn;

	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	private Timestamp updatedOn;

	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	private Timestamp startDate;

	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	private Timestamp endDateCreation;

	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	private Timestamp endDateSubmission;

	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	private Timestamp endDateComments;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDataCollectionName() {
		return dataCollectionName;
	}

	public void setDataCollectionName(String dataCollectionName) {
		this.dataCollectionName = dataCollectionName;
	}

	public TranslatableStringDto getLabel() {
		return label;
	}

	public void setLabel(TranslatableStringDto label) {
		this.label = label;
	}

	public TranslatableStringDto getDescription() {
		return description;
	}

	public void setDescription(TranslatableStringDto description) {
		this.description = description;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public VersionDto getVersion() {
		return version;
	}

	public void setVersion(VersionDto version) {
		this.version = version;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDateCreation() {
		return endDateCreation;
	}

	public void setEndDateCreation(Timestamp endDateCreation) {
		this.endDateCreation = endDateCreation;
	}

	public Timestamp getEndDateSubmission() {
		return endDateSubmission;
	}

	public void setEndDateSubmission(Timestamp endDateSubmission) {
		this.endDateSubmission = endDateSubmission;
	}

	public Timestamp getEndDateComments() {
		return endDateComments;
	}

	public void setEndDateComments(Timestamp endDateComments) {
		this.endDateComments = endDateComments;
	}

	public boolean isValidForCreation(Date date) {
		return isBetween(startDate, date, endDateCreation);
	}

	public boolean isValidForSubmission(Date date) {
		return isBetween(startDate, date, endDateSubmission);
	}

	public boolean isValidForComments(Date date) {
		return isBetween(startDate, date, endDateComments);
	}

	public List<FollowUpDefinitionDto> getFollowUpDefinitions() {
		return followUpDefinitions;
	}

	public void setFollowUpDefinitions(List<FollowUpDefinitionDto> followUpDefinitions) {
		this.followUpDefinitions = followUpDefinitions;
	}

	public ParticipationDefinitionDto getParticipationDefinition() {
		return participationDefinition;
	}

	public void setParticipationDefinition(ParticipationDefinitionDto participationDefinition) {
		this.participationDefinition = participationDefinition;
	}

	private boolean isBetween(Date start, Date toCheck, Date end) {
		if (toCheck == null) {
			return false;
		}
		if (start == null) {
			return false;
		}
		if (end == null) {
			return toCheck.after(start);
		} else {
			return toCheck.after(start) && toCheck.before(end);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof DataCollectionDefinitionDtoV6) {
			DataCollectionDefinitionDtoV6 other = (DataCollectionDefinitionDtoV6) o;

			return Objects.equals(id, other.id)
					&& Objects.equals(dataCollectionName, other.dataCollectionName)
					&& Objects.equals(label, other.label)
					&& Objects.equals(description, other.description)
					&& Objects.equals(content, other.content)
					&& Objects.equals(version, other.version)
					&& Objects.equals(createdOn, other.createdOn)
					&& Objects.equals(updatedOn, other.updatedOn)
					&& Objects.equals(startDate, other.startDate)
					&& Objects.equals(endDateCreation, other.endDateCreation)
					&& Objects.equals(endDateSubmission, other.endDateSubmission)
					&& Objects.equals(endDateComments, other.endDateComments)
					&& Objects.equals(participationDefinition, other.participationDefinition);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				this.id,
				this.dataCollectionName,
				this.label,
				this.description,
				this.content,
				this.version,
				this.createdOn,
				this.updatedOn,
				this.startDate,
				this.endDateCreation,
				this.endDateSubmission,
				this.endDateComments,
				this.participationDefinition);
	}
}
