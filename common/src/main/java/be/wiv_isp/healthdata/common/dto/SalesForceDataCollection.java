/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.common.dto;

public class SalesForceDataCollection {

    public String Name;
    public boolean HD4PRC__c = false;
    public boolean Public__c = false;
    public boolean StableData__c = false;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public boolean isHD4PRC__c() {
        return HD4PRC__c;
    }

    public void setHD4PRC__c(boolean HD4PRC__c) {
        this.HD4PRC__c = HD4PRC__c;
    }

    public boolean isPublic__c() {
        return Public__c;
    }

    public void setPublic__c(boolean public__c) {
        Public__c = public__c;
    }

    public boolean isStableData__c() {
        return StableData__c;
    }

    public void setStableData__c(boolean stableData__c) {
        StableData__c = stableData__c;
    }
}
