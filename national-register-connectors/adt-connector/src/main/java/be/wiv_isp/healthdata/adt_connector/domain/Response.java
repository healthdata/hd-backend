/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.adt_connector.domain;

import java.text.MessageFormat;
import java.util.Date;
import java.util.Objects;

public class Response {
    private String ssin;
    private String internalId;
    private String lastName;
    private String firstName;
    private Date dateOfBirth;
    private String gender;
    private String district;
    private Boolean deceased;
    private Date dateOfDeath;

    public String getSsin() {
        return ssin;
    }

    public void setSsin(String ssin) {
        this.ssin = ssin;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public Boolean isDeceased() {
        return deceased;
    }

    public void setDeceased(Boolean deceased) {
        this.deceased = deceased;
    }

    public Date getDateOfDeath() {
        return dateOfDeath;
    }

    public void setDateOfDeath(Date dateOfDeath) {
        this.dateOfDeath = dateOfDeath;
    }

    @Override
    public String toString() {
        return MessageFormat.format("[" +
                        "ssin: {0}," +
                        "internalId: {1}," +
                        "lastName: {2}," +
                        "firstName: {3}," +
                        "dateOfBirth: {4}," +
                        "gender: {5}," +
                        "district: {6}," +
                        "deceased: {7}," +
                        "dateOfDeath: {8}]",
                ssin,
                internalId,
                lastName,
                firstName,
                dateOfBirth,
                gender,
                district,
                deceased,
                dateOfDeath);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof Response) {
            Response other = (Response) o;

            return Objects.equals(ssin, other.ssin) //
                    && Objects.equals(internalId, other.internalId) //
                    && Objects.equals(lastName, other.lastName) //
                    && Objects.equals(firstName, other.firstName) //
                    && Objects.equals(dateOfBirth, other.dateOfBirth) //
                    && Objects.equals(gender, other.gender) //
                    && Objects.equals(district, other.district) //
                    && Objects.equals(deceased, other.deceased) //
                    && Objects.equals(dateOfDeath, other.dateOfDeath);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(//
                this.ssin, //
                this.internalId, //
                this.lastName, //
                this.firstName, //
                this.dateOfBirth, //
                this.gender, //
                this.district, //
                this.deceased, //
                this.dateOfDeath);
    }
}
