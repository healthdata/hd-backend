/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.adt_connector.factory;

import be.wiv_isp.healthdata.adt_connector.domain.Constants;
import be.wiv_isp.healthdata.adt_connector.exception.AdtConnectorException;
import be.wiv_isp.healthdata.adt_connector.idgenerator.IncrementalIdGenerator;
import ca.uhn.hl7v2.model.primitive.CommonTS;
import ca.uhn.hl7v2.model.v23.message.QRY_A19;
import ca.uhn.hl7v2.parser.Parser;
import ca.uhn.hl7v2.parser.PipeParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.GregorianCalendar;

public class RequestFactory {
    private static final Logger LOG = LoggerFactory.getLogger(RequestFactory.class);

    private static final String QUERY_ID_PATTERN = "Q-{0}";

    private static int QUERY_COUNTER = 0;

    public QRY_A19 createQryA19Request(String patientId) {
        LOG.debug(MessageFormat.format("Creating QRY A19 request with patient id [{0}]", patientId));

        final QRY_A19 request = new QRY_A19();

        final Parser parser = new PipeParser();
        parser.getParserConfiguration().setIdGenerator(new IncrementalIdGenerator());
        request.setParser(parser);

        try {
            request.initQuickstart(Constants.QRY, Constants.A19, Constants.P); // Production QRY_A19 message

            request.getMSH().getSendingApplication().getNamespaceID().setValue("HD4DP");
            request.getMSH().getReceivingApplication().getNamespaceID().setValue("HL7");

            request.getQRD().getQueryDateTime().parse(CommonTS.toHl7TSFormat(new GregorianCalendar()));
            request.getQRD().getQueryFormatCode().setValue(Constants.R); // Record-oriented format
            request.getQRD().getQueryPriority().setValue(Constants.I); // Immediate
            request.getQRD().getQueryID().setValue(MessageFormat.format(QUERY_ID_PATTERN, String.format("%08d", ++QUERY_COUNTER))); // Query counter
            request.getQRD().getQuantityLimitedRequest().getQuantity().setValue("1");
            request.getQRD().getQuantityLimitedRequest().getUnits().getIdentifier().setValue(Constants.RD); // Records
            request.getQRD().insertWhatSubjectFilter(0).getIdentifier().setValue(Constants.DEM); // Demographics
            request.getQRD().insertWhoSubjectFilter(0).getIDNumber().setValue(patientId);

            LOG.debug(MessageFormat.format("Request:\n {0}", request.toString()));
        } catch (Exception e) {
            throw new AdtConnectorException("Error while building request", e);
        }

        return request;
    }
}
