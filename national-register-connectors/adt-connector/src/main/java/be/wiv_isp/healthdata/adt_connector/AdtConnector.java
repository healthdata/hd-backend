/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.adt_connector;

import be.wiv_isp.healthdata.adt_connector.domain.Request;
import be.wiv_isp.healthdata.adt_connector.domain.Response;
import be.wiv_isp.healthdata.adt_connector.exception.AdtConnectorException;
import be.wiv_isp.healthdata.adt_connector.factory.RequestFactory;
import be.wiv_isp.healthdata.adt_connector.mapper.ResponseMapper;
import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.app.Connection;
import ca.uhn.hl7v2.llp.LLPException;
import ca.uhn.hl7v2.model.v23.message.ADR_A19;
import ca.uhn.hl7v2.model.v23.message.QRY_A19;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.MessageFormat;

public class AdtConnector {

	private static final Logger LOG = LoggerFactory.getLogger(AdtConnector.class);

    private static HapiContext CONTEXT;

	public Response execute(Request request) {
		final QRY_A19 qryA19Request = new RequestFactory().createQryA19Request(request.getPatientIdentifier());

        try {
			LOG.debug(MessageFormat.format("Sending QRY A19 request to host [{0}] and port [{1}]: \n {2}", request.getHost(), request.getPort(), qryA19Request));

            final Connection connection = getContext().newClient(request.getHost(), request.getPort(), false);
			final ADR_A19 adrA19Response = (ADR_A19) connection.getInitiator().sendAndReceive(qryA19Request);
			connection.close();

            LOG.debug(MessageFormat.format("Response\n: {0}", getContext().getPipeParser().encode(adrA19Response)));

            return new ResponseMapper().map(adrA19Response);
		} catch(HL7Exception | LLPException | IOException e) {
            throw new AdtConnectorException("Error while sending request", e);
		}
	}

    private HapiContext getContext() {
        if(CONTEXT == null) {
            CONTEXT = new DefaultHapiContext();
            CONTEXT.getExecutorService(); // BUG in HAPI 2.2 (http://sourceforge.net/p/hl7api/bugs/223/)
        }

        return CONTEXT;
    }

    public static void main(String[] args) {

        System.out.println();

        if(args.length != 3) {
            printUsage();
            return;
        }

        final String host = args[0];

        Integer port;
        try {
            port = Integer.valueOf(args[1]);
        } catch (NumberFormatException e) {
            System.err.println("Could not parse port parameter[" + args[1] + "]. Please provide an integer value.");
            System.out.println();
            printUsage();
            return;
        }

        final String patientIdentifier = args[2];

        System.out.println("Host: " + host);
        System.out.println("Port: " + port);
        System.out.println("Patient identifier: " + patientIdentifier);
        System.out.println();

        System.out.println("Sending ADT Request");

        final Request request = new Request();
        request.setHost(host);
        request.setPort(port);
        request.setPatientIdentifier(patientIdentifier);
        final Response response = new AdtConnector().execute(request);

        System.out.println("Response: " + response);

        try {
            CONTEXT.close();
        } catch (IOException e) {
            System.err.println("Error while closing context: " + e.getMessage());
        }
    }

    private static void printUsage() {
        System.out.println("Usage:");
        System.out.println("1st parameter: host (ex: 127.0.0.1)");
        System.out.println("2nd parameter: host (ex: 5687)");
        System.out.println("3rd parameter: patient identifier (ex: 01234567891)");
    }
}
