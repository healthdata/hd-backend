/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.adt_connector.mapper;

import be.wiv_isp.healthdata.adt_connector.domain.Response;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.v23.message.ADR_A19;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;
import java.util.Date;

public class ResponseMapper {

    private static final Logger LOG = LoggerFactory.getLogger(ResponseMapper.class);

    public Response map(ADR_A19 adrA19) {
        LOG.debug("Mapping ADR A19 message to Response object");

        final Response response = new Response();

        /** Last name and first name **/
        response.setLastName(adrA19.getQUERY_RESPONSE().getPID().getPatientName(0).getFamilyName().getValue());
        response.setFirstName(adrA19.getQUERY_RESPONSE().getPID().getPatientName(0).getGivenName().getValue());

        /** SSN number **/
        String ssin = adrA19.getQUERY_RESPONSE().getPID().getPid19_SSNNumberPatient().getValue();

        if(ssin == null) {
            // SSIN can sometimes be found at position 20 instead of 19
            final String pid20 = adrA19.getQUERY_RESPONSE().getPID().getPid20_DriverSLicenseNumber().getDln1_DriverSLicenseNumber().getValue();

            if(pid20.length() == 11) {
                ssin = pid20;
            }
        }

        response.setSsin(ssin);

        /** Internal ID **/
        response.setInternalId(adrA19.getQUERY_RESPONSE().getPID().getPatientIDInternalID(0).getID().getValue());

        /** Date of birth **/
        try {
            response.setDateOfBirth(adrA19.getQUERY_RESPONSE().getPID().getDateOfBirth().getTimeOfAnEvent().getValueAsDate());
        } catch (DataTypeException e) {
            LOG.error("Error while parsing date", e);
        }

        /** Date of death **/
        try {
            final Date dateOfDeath = adrA19.getQUERY_RESPONSE().getPID().getPatientDeathDateAndTime().getTimeOfAnEvent().getValueAsDate();

            response.setDateOfDeath(dateOfDeath);
            response.setDeceased(dateOfDeath != null);
        } catch (DataTypeException e) {
            LOG.error("Error while parsing date", e);
        }

        /** Gender **/
        response.setGender(adrA19.getQUERY_RESPONSE().getPID().getSex().getValue());

        /** ZIP code **/
        response.setDistrict(adrA19.getQUERY_RESPONSE().getPID().getPatientAddress(0).getZipOrPostalCode().getValue());

        LOG.debug(MessageFormat.format("Response: {0}", response.toString()));

        return response;
    }
}
