/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.adt_connector;

import be.wiv_isp.healthdata.adt_connector.factory.RequestFactory;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.v23.message.QRY_A19;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestFactoryTest {
    private static final Logger LOG = LoggerFactory.getLogger(RequestFactoryTest.class);

    @Test
    public void testCreateQryA19Request() throws HL7Exception {

        final String patientId = "01121282";
        final QRY_A19 qryA19 = new RequestFactory().createQryA19Request(patientId);

        LOG.info("\n\nRequest:\n" + qryA19 + "\n\n");

        Assert.assertEquals("HD4DP", qryA19.getMSH().getSendingApplication().getNamespaceID().getValue());
        Assert.assertEquals("HL7", qryA19.getMSH().getReceivingApplication().getNamespaceID().getValue());
        Assert.assertEquals("QRY", qryA19.getMSH().getMessageType().getMessageType().getValue());
        Assert.assertEquals("A19", qryA19.getMSH().getMessageType().getTriggerEvent().getValue());
        Assert.assertEquals("P", qryA19.getMSH().getProcessingID().getProcessingID().getValue());
        Assert.assertEquals("2.3", qryA19.getMSH().getVersionID().getValue());
        Assert.assertEquals("R", qryA19.getQRD().getQueryFormatCode().getValue());
        Assert.assertEquals("I", qryA19.getQRD().getQueryPriority().getValue());
        Assert.assertEquals("Q-00000001", qryA19.getQRD().getQueryID().getValue());
        Assert.assertEquals("1", qryA19.getQRD().getQuantityLimitedRequest().getQuantity().getValue());
        Assert.assertEquals("RD", qryA19.getQRD().getQuantityLimitedRequest().getUnits().getIdentifier().getValue());
        Assert.assertEquals("DEM", qryA19.getQRD().getWhatSubjectFilter(0).getIdentifier().getValue());
        Assert.assertEquals(patientId, qryA19.getQRD().getWhoSubjectFilter(0).getIDNumber().getValue());
    }
}
