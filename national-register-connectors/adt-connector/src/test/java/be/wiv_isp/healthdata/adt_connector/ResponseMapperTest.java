/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.adt_connector;

import be.wiv_isp.healthdata.adt_connector.domain.Response;
import be.wiv_isp.healthdata.adt_connector.mapper.ResponseMapper;
import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.v23.message.ADR_A19;
import org.apache.commons.codec.binary.Base64;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ResponseMapperTest {

    private static final Logger LOG = LoggerFactory.getLogger(ResponseMapperTest.class);

    private HapiContext context;

    @Before
    public void setUp() {
        context = new DefaultHapiContext();
        context.getExecutorService(); // BUG in HAPI 2.2 (http://sourceforge.net/p/hl7api/bugs/223/)
    }

    @After
    public void tearDown() throws IOException {
        context.close();
    }

    @Test
    public void testMapAdrA19ResponseToPersonWithSsinInPID19() throws HL7Exception, ParseException {
        final String base64EncodedResponse = "TVNIfF5+XCZ8T0FaSVN8fHx8MjAxNTA5MDkxNDUyMTh8fEFEUl5BMTl8QUNLUVJZMDAwMXxQfDIuM3x8fHx8fEFTQ0lJDQpNU0F8QUF8UVJZMDAwMSANClFSRHwyMDE1MDkwOTE0MTB8UnxJfFExMDAwfHx8MV5SRHwwMDAwMTQ0MTMwfERFTQ0KUElEfDF8fDAwMDAxNDQxMzB8fERvZV5Kb2hufFBBUlRORVIgTkFBTXwxOTM5MDQxNHxNfHx8U3RhdGlvbiBTdHJhYXReXkJydXNzZWxzXl4xMDAwXkJFfHxeXlBIfHxOTHxXfHx8MzkwNDE0Mzc4NDl8fHx8fHx8QkV8fHwyMDAwMDcwMXxODQpQVjF8MXxOfHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fE4g";
        final String responseString = new String(Base64.decodeBase64(base64EncodedResponse), StandardCharsets.UTF_8);
        LOG.info("Response:\n" + responseString + "\n\n");

        final ADR_A19 adrA19 = (ADR_A19) context.getPipeParser().parse(responseString);

        final Response response = new ResponseMapper().map(adrA19);

        final Response expectedPerson = new Response();
        expectedPerson.setSsin("39041437849");
        expectedPerson.setInternalId("0000144130");
        expectedPerson.setLastName("Doe");
        expectedPerson.setFirstName("John");
        expectedPerson.setGender("M");
        expectedPerson.setDateOfBirth(new SimpleDateFormat("yyyyMMdd").parse("19390414"));
        expectedPerson.setDistrict("1000");
        expectedPerson.setDeceased(true);
        expectedPerson.setDateOfDeath(new SimpleDateFormat("yyyyMMdd").parse("20000701"));

        LOG.info(response.toString());
        LOG.info(expectedPerson.toString());

        Assert.assertEquals(expectedPerson, response);
    }

    @Test
    public void testMapAdrA19ResponseToPersonWithSsinInPID20() throws HL7Exception, ParseException {
        final String base64EncodedResponse = "TVNIfF5+XCZ8T0FaSVN8fHx8MjAxNTA5MDkxNDUyMTh8fEFEUl5BMTl8QUNLUVJZMDAwMXxQfDIuM3x8fHx8fEFTQ0lJDQpNU0F8QUF8UVJZMDAwMSANClFSRHwyMDE1MDkwOTE0MTB8UnxJfFExMDAwfHx8MV5SRHwwMDAwMTQ0MTMwfERFTQ0KUElEfDF8fDAwMDAxNDQxMzB8fERvZV5Kb2hufFBBUlRORVIgTkFBTXwxOTM5MDQxNHxNfHx8U3RhdGlvbiBTdHJhYXReXkJydXNzZWxzXl4xMDAwXkJFfHxeXlBIfHxOTHxXfHx8fDM5MDQxNDM3ODQ5fHx8fHx8QkV8fHwyMDAwMDcwMXxODQpQVjF8MXxOfHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fE4g";
        final String responseString = new String(Base64.decodeBase64(base64EncodedResponse), StandardCharsets.UTF_8);
        LOG.info("Response:\n" + responseString + "\n\n");

        final ADR_A19 adrA19 = (ADR_A19) context.getPipeParser().parse(responseString);

        final Response response = new ResponseMapper().map(adrA19);

        final Response expectedPerson = new Response();
        expectedPerson.setSsin("39041437849");
        expectedPerson.setInternalId("0000144130");
        expectedPerson.setLastName("Doe");
        expectedPerson.setFirstName("John");
        expectedPerson.setGender("M");
        expectedPerson.setDateOfBirth(new SimpleDateFormat("yyyyMMdd").parse("19390414"));
        expectedPerson.setDistrict("1000");
        expectedPerson.setDeceased(true);
        expectedPerson.setDateOfDeath(new SimpleDateFormat("yyyyMMdd").parse("20000701"));

        LOG.info(response.toString());
        LOG.info(expectedPerson.toString());

        Assert.assertEquals(expectedPerson, response);
    }


}
