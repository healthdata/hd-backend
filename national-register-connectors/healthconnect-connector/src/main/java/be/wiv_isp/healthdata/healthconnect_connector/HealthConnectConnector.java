/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.healthconnect_connector;

import be.healthconnect.rrnconnector.ws._1_0.common.LanguageType;
import be.healthconnect.rrnconnector.ws._1_0.common.RequestInfoType;
import be.healthconnect.rrnconnector.ws._1_0.messages.IdentifyPersonRequest;
import be.healthconnect.rrnconnector.ws._1_0.messages.IdentifyPersonResponse;
import be.healthconnect.rrnconnector.ws._1_0.person.PersonType;
import be.healthconnect.schemas.rrnconnector._1_0.ConsultRn;
import be.healthconnect.schemas.rrnconnector._1_0.ConsultRnSoap;
import be.wiv_isp.healthdata.healthconnect_connector.domain.Request;

import javax.xml.ws.BindingProvider;

public class HealthConnectConnector {

    public PersonType execute(Request request) {
        ConsultRnSoap port = new ConsultRn().getConsultRnSoap();

        BindingProvider bp = (BindingProvider) port;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, request.getUrl());

        IdentifyPersonRequest identifyPersonRequest = new IdentifyPersonRequest();
        identifyPersonRequest.setINSS(request.getSsin());

        RequestInfoType requestInfoType = new RequestInfoType();
        requestInfoType.setApplicationToken(request.getApplicationToken());
        requestInfoType.setUserId(request.getUserId());
        requestInfoType.setLanguage(LanguageType.EN);

        identifyPersonRequest.setRequestInfo(requestInfoType);

        IdentifyPersonResponse identifyPerson = port.identifyPerson(identifyPersonRequest);

        return identifyPerson.getPerson();
    }
}
