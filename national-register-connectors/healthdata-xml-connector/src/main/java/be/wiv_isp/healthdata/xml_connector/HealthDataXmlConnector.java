/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.xml_connector;

import be.wiv_isp.healthdata.xml_connector.domain.Person;
import be.wiv_isp.healthdata.xml_connector.domain.Request;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.core.MediaType;

public class HealthDataXmlConnector {

    public Person execute(Request request) {
        String url = request.getUrl();
        final String ssin = request.getSsin();
        final String user = request.getUser();
        final String password = request.getPassword();

        if (url.charAt(url.length() - 1) != '/') {
            url += "/";
        }

        final ClientConfig config = new DefaultClientConfig();
        config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
        final Client client = Client.create(config);

        final WebResource wr = client.resource(url + ssin);

        if (StringUtils.isNotBlank(user)) {
            wr.queryParam("user", user);
            wr.queryParam("password", password);
        }

        return wr.accept(MediaType.APPLICATION_XML_TYPE).get(Person.class);
    }

}
