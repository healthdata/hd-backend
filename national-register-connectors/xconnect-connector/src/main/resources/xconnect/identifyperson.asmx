<?xml version="1.0" encoding="utf-8"?>
<wsdl:definitions xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:s4="urn:be:fgov:ehealth:consultRN:1_0:core" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:s3="urn:be:fgov:ehealth:commons:1_0:core" xmlns:tns="http://XConnect/" xmlns:s2="urn:be:fgov:ehealth:commons:1_0:protocol" xmlns:s1="urn:be:fgov:ehealth:consultRN:1_0:protocol" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tm="http://microsoft.com/wsdl/mime/textMatching/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" targetNamespace="http://XConnect/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">
  <wsdl:types>
    <s:schema elementFormDefault="qualified" targetNamespace="http://XConnect/">
      <s:import namespace="urn:be:fgov:ehealth:consultRN:1_0:protocol" />
      <s:element name="SearchBySSIN">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="SiteInami" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="Campus" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="ApplicationId" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="Niss" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="Debut" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="Fin" type="s:string" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="SearchBySSINResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="SearchBySSINResult" type="tns:RNSearchBySSINResponse" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="RNSearchBySSINResponse">
        <s:complexContent mixed="false">
          <s:extension base="tns:XConnectResponseOfSearchBySSINReply" />
        </s:complexContent>
      </s:complexType>
      <s:complexType name="XConnectResponseOfSearchBySSINReply">
        <s:sequence>
          <s:element minOccurs="1" maxOccurs="1" name="IsSuccess" type="s:boolean" />
          <s:element minOccurs="0" maxOccurs="1" name="OfficialResponse" type="s1:SearchBySSINReply" />
        </s:sequence>
      </s:complexType>
      <s:element name="RNSearchBySSINResponse" nillable="true" type="tns:RNSearchBySSINResponse" />
    </s:schema>
    <s:schema elementFormDefault="qualified" targetNamespace="urn:be:fgov:ehealth:consultRN:1_0:protocol">
      <s:import namespace="urn:be:fgov:ehealth:commons:1_0:protocol" />
      <s:import namespace="urn:be:fgov:ehealth:consultRN:1_0:core" />
      <s:complexType name="SearchBySSINReply">
        <s:complexContent mixed="false">
          <s:extension base="s1:ConsultRnReply">
            <s:sequence>
              <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Person" type="s4:PersonType" />
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="ConsultRnReply">
        <s:complexContent mixed="false">
          <s:extension base="s2:ResponseType">
            <s:sequence>
              <s:element minOccurs="0" maxOccurs="unbounded" form="unqualified" name="ErrorInformation" type="s4:ErrorType" />
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
    </s:schema>
    <s:schema elementFormDefault="qualified" targetNamespace="urn:be:fgov:ehealth:commons:1_0:protocol">
      <s:import namespace="urn:be:fgov:ehealth:commons:1_0:core" />
      <s:complexType name="ResponseType">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" ref="s3:Status" />
        </s:sequence>
        <s:attribute name="Id" type="s:string" />
      </s:complexType>
    </s:schema>
    <s:schema elementFormDefault="qualified" targetNamespace="urn:be:fgov:ehealth:commons:1_0:core">
      <s:element name="Status">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Code" type="s:string" />
            <s:element minOccurs="0" maxOccurs="unbounded" form="unqualified" name="Message" type="s3:LocalisedString" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="LocalisedString">
        <s:simpleContent>
          <s:extension base="s:string">
            <s:attribute name="Lang" type="s3:LangageType" />
          </s:extension>
        </s:simpleContent>
      </s:complexType>
      <s:simpleType name="LangageType">
        <s:restriction base="s:string">
          <s:enumeration value="FR" />
          <s:enumeration value="NL" />
          <s:enumeration value="EN" />
          <s:enumeration value="DE" />
          <s:enumeration value="NA" />
        </s:restriction>
      </s:simpleType>
    </s:schema>
    <s:schema elementFormDefault="qualified" targetNamespace="urn:be:fgov:ehealth:consultRN:1_0:core">
      <s:import namespace="urn:be:fgov:ehealth:commons:1_0:core" />
      <s:complexType name="ErrorType">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Code" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Description" type="s:string" />
          <s:element minOccurs="0" maxOccurs="unbounded" form="unqualified" name="Information">
            <s:complexType>
              <s:sequence>
                <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="FieldName" type="s:string" />
                <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="FieldValue" type="s:string" />
              </s:sequence>
            </s:complexType>
          </s:element>
        </s:sequence>
      </s:complexType>
      <s:complexType name="PersonType">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="SSIN" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="PersonData" type="s4:PersonDataType" />
        </s:sequence>
        <s:attribute name="ModificationDate" type="s:string" />
        <s:attribute name="Origin" type="s4:OriginType" />
      </s:complexType>
      <s:complexType name="PersonDataType">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Birth" type="s4:BirthDeceaseType" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Name" type="s4:NameType" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Gender" type="s4:GenderType" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Nationality" type="s4:NationalityType" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Civilstate" type="s4:CivilStateType" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Decease" type="s4:BirthDeceaseType" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Address" type="s4:AddressType" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="BirthDeceaseType">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Date" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Localisation" type="s4:WhereType" />
        </s:sequence>
        <s:attribute name="ModificationDate" type="s:string" />
        <s:attribute name="Origin" type="s4:OriginType" />
      </s:complexType>
      <s:complexType name="WhereType">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" form="unqualified" name="Description" type="s3:LocalisedString" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Municipality" type="s4:MunicipalityType" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Country" type="s4:CountryType" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="MunicipalityType">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="InsCode" type="s:integer" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="PostalCode" type="s:string" />
          <s:element minOccurs="0" maxOccurs="unbounded" form="unqualified" name="Description" type="s3:LocalisedString" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="CountryType">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="InsCode" type="s:integer" />
          <s:element minOccurs="0" maxOccurs="unbounded" form="unqualified" name="Description" type="s3:LocalisedString" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="NationalityType">
        <s:complexContent mixed="false">
          <s:extension base="s4:CountryType">
            <s:attribute name="ModificationDate" type="s:string" />
            <s:attribute name="Origin" type="s4:OriginType" />
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:simpleType name="OriginType">
        <s:restriction base="s:string">
          <s:enumeration value="BCSS_KSZ" />
          <s:enumeration value="RN_RR" />
        </s:restriction>
      </s:simpleType>
      <s:complexType name="NameType">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="First" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Middle" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Last" type="s:string" />
        </s:sequence>
        <s:attribute name="ModificationDate" type="s:string" />
        <s:attribute name="Origin" type="s4:OriginType" />
      </s:complexType>
      <s:complexType name="GenderType">
        <s:simpleContent>
          <s:extension base="s4:GenderPossibility">
            <s:attribute name="ModificationDate" type="s:string" />
            <s:attribute name="Origin" type="s4:OriginType" />
          </s:extension>
        </s:simpleContent>
      </s:complexType>
      <s:simpleType name="GenderPossibility">
        <s:restriction base="s:string">
          <s:enumeration value="MALE" />
          <s:enumeration value="FEMALE" />
          <s:enumeration value="UNKNOWN" />
        </s:restriction>
      </s:simpleType>
      <s:complexType name="CivilStateType">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Code" type="s:integer" />
          <s:element minOccurs="0" maxOccurs="unbounded" form="unqualified" name="Description" type="s3:LocalisedString" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Localisation" type="s4:WhereType" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Partner" type="s4:InhabitantType" />
        </s:sequence>
        <s:attribute name="ModificationDate" type="s:string" />
        <s:attribute name="Origin" type="s4:OriginType" />
      </s:complexType>
      <s:complexType name="InhabitantType">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="SSIN" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Name" type="s4:NameType" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Gender" type="s4:GenderType" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="BirthDate" type="s:string" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="AddressType">
        <s:sequence>
          <s:choice minOccurs="1" maxOccurs="1">
            <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="PlainAddress" type="s4:PlainAddressType" />
            <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="StandardAddress" type="s4:StandardAddressType" />
          </s:choice>
        </s:sequence>
        <s:attribute name="ModificationDate" type="s:string" />
        <s:attribute name="Origin" type="s4:OriginType" />
      </s:complexType>
      <s:complexType name="PlainAddressType">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Address" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Country" type="s4:CountryType" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="StandardAddressType">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Street" type="s4:ArrayOfLocalisedString" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Housenumber" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Box" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Municipality" type="s4:MunicipalityType" />
          <s:element minOccurs="0" maxOccurs="1" form="unqualified" name="Country" type="s4:CountryType" />
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfLocalisedString">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="unbounded" form="unqualified" name="Description" type="s3:LocalisedString" />
        </s:sequence>
      </s:complexType>
    </s:schema>
  </wsdl:types>
  <wsdl:message name="SearchBySSINSoapIn">
    <wsdl:part name="parameters" element="tns:SearchBySSIN" />
  </wsdl:message>
  <wsdl:message name="SearchBySSINSoapOut">
    <wsdl:part name="parameters" element="tns:SearchBySSINResponse" />
  </wsdl:message>
  <wsdl:message name="SearchBySSINHttpGetIn">
    <wsdl:part name="SiteInami" type="s:string" />
    <wsdl:part name="Campus" type="s:string" />
    <wsdl:part name="ApplicationId" type="s:string" />
    <wsdl:part name="Niss" type="s:string" />
    <wsdl:part name="Debut" type="s:string" />
    <wsdl:part name="Fin" type="s:string" />
  </wsdl:message>
  <wsdl:message name="SearchBySSINHttpGetOut">
    <wsdl:part name="Body" element="tns:RNSearchBySSINResponse" />
  </wsdl:message>
  <wsdl:message name="SearchBySSINHttpPostIn">
    <wsdl:part name="SiteInami" type="s:string" />
    <wsdl:part name="Campus" type="s:string" />
    <wsdl:part name="ApplicationId" type="s:string" />
    <wsdl:part name="Niss" type="s:string" />
    <wsdl:part name="Debut" type="s:string" />
    <wsdl:part name="Fin" type="s:string" />
  </wsdl:message>
  <wsdl:message name="SearchBySSINHttpPostOut">
    <wsdl:part name="Body" element="tns:RNSearchBySSINResponse" />
  </wsdl:message>
  <wsdl:portType name="RegistreNationalIdentifyPersonSoap">
    <wsdl:operation name="SearchBySSIN">
      <wsdl:input message="tns:SearchBySSINSoapIn" />
      <wsdl:output message="tns:SearchBySSINSoapOut" />
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:portType name="RegistreNationalIdentifyPersonHttpGet">
    <wsdl:operation name="SearchBySSIN">
      <wsdl:input message="tns:SearchBySSINHttpGetIn" />
      <wsdl:output message="tns:SearchBySSINHttpGetOut" />
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:portType name="RegistreNationalIdentifyPersonHttpPost">
    <wsdl:operation name="SearchBySSIN">
      <wsdl:input message="tns:SearchBySSINHttpPostIn" />
      <wsdl:output message="tns:SearchBySSINHttpPostOut" />
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:binding name="RegistreNationalIdentifyPersonSoap" type="tns:RegistreNationalIdentifyPersonSoap">
    <soap:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="SearchBySSIN">
      <soap:operation soapAction="http://XConnect/SearchBySSIN" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:binding name="RegistreNationalIdentifyPersonSoap12" type="tns:RegistreNationalIdentifyPersonSoap">
    <soap12:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="SearchBySSIN">
      <soap12:operation soapAction="http://XConnect/SearchBySSIN" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:binding name="RegistreNationalIdentifyPersonHttpGet" type="tns:RegistreNationalIdentifyPersonHttpGet">
    <http:binding verb="GET" />
    <wsdl:operation name="SearchBySSIN">
      <http:operation location="/SearchBySSIN" />
      <wsdl:input>
        <http:urlEncoded />
      </wsdl:input>
      <wsdl:output>
        <mime:mimeXml part="Body" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:binding name="RegistreNationalIdentifyPersonHttpPost" type="tns:RegistreNationalIdentifyPersonHttpPost">
    <http:binding verb="POST" />
    <wsdl:operation name="SearchBySSIN">
      <http:operation location="/SearchBySSIN" />
      <wsdl:input>
        <mime:content type="application/x-www-form-urlencoded" />
      </wsdl:input>
      <wsdl:output>
        <mime:mimeXml part="Body" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:service name="RegistreNationalIdentifyPerson">
    <wsdl:port name="RegistreNationalIdentifyPersonSoap" binding="tns:RegistreNationalIdentifyPersonSoap">
      <soap:address location="https://10.136.77.10/xconnectservices/production/ehealth/registrenational/v1/identifyperson.asmx" />
    </wsdl:port>
    <wsdl:port name="RegistreNationalIdentifyPersonSoap12" binding="tns:RegistreNationalIdentifyPersonSoap12">
      <soap12:address location="https://10.136.77.10/xconnectservices/production/ehealth/registrenational/v1/identifyperson.asmx" />
    </wsdl:port>
    <wsdl:port name="RegistreNationalIdentifyPersonHttpGet" binding="tns:RegistreNationalIdentifyPersonHttpGet">
      <http:address location="https://10.136.77.10/xconnectservices/production/ehealth/registrenational/v1/identifyperson.asmx" />
    </wsdl:port>
    <wsdl:port name="RegistreNationalIdentifyPersonHttpPost" binding="tns:RegistreNationalIdentifyPersonHttpPost">
      <http:address location="https://10.136.77.10/xconnectservices/production/ehealth/registrenational/v1/identifyperson.asmx" />
    </wsdl:port>
  </wsdl:service>
</wsdl:definitions>