/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.xconnect_connector;

import be.fgov.ehealth.consultrn._1_0.core.PersonDataType;
import be.fgov.ehealth.consultrn._1_0.core.PersonType;
import be.wiv_isp.healthdata.xconnect_connector.domain.Request;
import xconnect.RNSearchBySSINResponse;
import xconnect.RegistreNationalIdentifyPerson;
import xconnect.RegistreNationalIdentifyPersonSoap;

import javax.xml.ws.BindingProvider;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class XConnectConnector {

    public RNSearchBySSINResponse execute(Request request) {
        final RegistreNationalIdentifyPersonSoap soap = new RegistreNationalIdentifyPerson().getRegistreNationalIdentifyPersonSoap();

        BindingProvider bp = (BindingProvider) soap;
        final String url = request.getUrl();
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);

        final String ssin = request.getSsin();
        final String identificationValue = request.getIdentificationValue();
        final String campus = request.getCampus();
        final String applicationId = request.getApplicationId();
        final String start = request.getStart();
        final String end = request.getEnd();

        return soap.searchBySSIN(identificationValue, campus, applicationId, ssin, start, end);
    }

    public static void main(String[] args) {

        System.out.println();

        if(args.length != 5) {
            printUsage();
            return;
        }

        final String url = args[0];
        final String ssin = args[1];
        final String identificationValue = args[2];
        final String campus = args[3];
        final String applicationId = args[4];

        System.out.println("Url: " + url);
        System.out.println("Ssin: " + ssin);
        System.out.println("Identification value: " + identificationValue);
        System.out.println("Campus: " + campus);
        System.out.println("Application ID: " + applicationId);
        System.out.println();

        System.out.println("Sending XConnect Request\n");

        final Request request = new Request();
        request.setUrl(url);
        request.setSsin(ssin);
        request.setIdentificationValue(identificationValue);
        request.setCampus(campus);
        request.setApplicationId(applicationId);

        Calendar calendar = Calendar.getInstance();
        final String start = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        final String end = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
        request.setStart(start);
        request.setEnd(end);

        final RNSearchBySSINResponse response = new XConnectConnector().execute(request);

        printResponse(response);
    }

    private static void printUsage() {
        System.out.println("Usage:");
        System.out.println("1st parameter: Url");
        System.out.println("2nd parameter: Ssin");
        System.out.println("3rd parameter: Identification value");
        System.out.println("4th parameter: Campus");
        System.out.println("5th parameter: Application ID");
    }

    private static void printResponse(RNSearchBySSINResponse rnSearchBySSINResponse) {
        final PersonType personType = rnSearchBySSINResponse.getOfficialResponse().getPerson();
        final PersonDataType personData = personType.getPersonData();

        System.out.println("Response:\n");
        System.out.println("Ssin: " + personType.getSSIN());
        System.out.println("Lastname: " + personData.getName().getLast());
        System.out.println("Firstname: " + personData.getName().getFirst());
        System.out.println("Date of birth: " + personData.getBirth().getDate());
        if(personData.getDecease() != null) {
            System.out.println("Date of death: " + personData.getDecease().getDate());
        } else {
            System.out.println("Date of death: none");
        }
        System.out.println("Postal code: " + personData.getAddress().getStandardAddress().getMunicipality().getPostalCode());
        System.out.println("Gender: " + personData.getGender().getValue());
    }
}
