# Java Backend for HD4DP and HD4RES

## Building

- Run `mvn clean install -DskipITs` to compile the source code. The result for HD4DP will be found in `webapps\hd4dp\target\healthdata_hd4dp.war`, the result for HD4RES in `webapps\hd4res\target\healthdata_hd4res.war`.
- Generate the database schema with hibernate (hibernate.hbm2ddl.auto property). The application context can be found in `webapps\hd4dp\src\main\resources\applicationContext.xml`.

## Contact

For questions, please contact support@healthdata.be.

## License
Apache License, Version 2.0
