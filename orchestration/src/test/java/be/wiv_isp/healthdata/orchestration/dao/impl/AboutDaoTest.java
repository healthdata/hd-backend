/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.orchestration.dao.IAboutDao;
import be.wiv_isp.healthdata.orchestration.domain.About;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class AboutDaoTest {

	private static final Logger LOG = LoggerFactory.getLogger(AboutDaoTest.class);

	private static final String ABOUT_KEY_1 = "key1";
	private static final String ABOUT_KEY_2 = "key2";

	private About about1;
	private About about2;

	@Autowired
	private IAboutDao aboutDao;

	@Before
	public void setUp() throws Exception {
		// Creation of first Configuration element
		about1 = new About();
		about1.setKey(ABOUT_KEY_1);
		about1.setValue("value1");

		LOG.info("creating about 1");
		aboutDao.create(about1);

		// Creation of second Configuration element
		about2 = new About();
		about2.setKey(ABOUT_KEY_2);
		about2.setValue("value2");

		LOG.info("creating about 2");
		aboutDao.create(about2);
	}

	@Test
	public void testGet() throws Exception {
		final About returnedAbout = aboutDao.get(about1.getId());
		Assert.assertEquals(about1.getKey(), returnedAbout.getKey());
		Assert.assertEquals(about1.getValue(), returnedAbout.getValue());
	}

	@Test
	public void testUpdate() {
		final String initialValue = "value1";
		final String updatedValue = "value1_modified";

		final About about = aboutDao.get(about1.getId());
		Assert.assertTrue(initialValue.equals(about.getValue()));
		about.setValue(updatedValue);
		aboutDao.update(about);
		final About updatedAbout = aboutDao.get(about1.getId());
		Assert.assertTrue(updatedValue.equals(updatedAbout.getValue()));
	}

	@Test
	public void testDelete() {

		int aboutCardinalityBefore = aboutDao.getAll().size();
		aboutDao.delete(about1);
		int aboutCardinalityAfter = aboutDao.getAll().size();
		final About returnedAbout = aboutDao.get(about1.getId());
		Assert.assertNull(returnedAbout);
		Assert.assertTrue(aboutCardinalityAfter == aboutCardinalityBefore - 1);
	}

	@Test
	public void testDeleteNonExistingConfiguration() {
		final Long unexsitingId = Long.MIN_VALUE;
		final String unexsitingKey = "unexisting_key";
		final About unexistingAbout = new About();
		unexistingAbout.setKey(unexsitingKey);
		unexistingAbout.setValue("value");

		final About returnedAbout = aboutDao.get(unexsitingId);
		Assert.assertNull(returnedAbout);
		int aboutCardinalityBefore = aboutDao.getAll().size();
		aboutDao.delete(unexistingAbout);
		int aboutCardinalityAfter = aboutDao.getAll().size();

		Assert.assertNull(returnedAbout);
		Assert.assertTrue(aboutCardinalityAfter == aboutCardinalityBefore);
	}

	@Test
	public void testgetAll() throws Exception {

		final List<About> returnedAbout = aboutDao.getAll();
		Assert.assertNotNull(returnedAbout);
		Assert.assertFalse(returnedAbout.isEmpty());
	}

	@Test
	public void testgetWithKey() throws Exception {
		final About returnedAbout = aboutDao.get(ABOUT_KEY_1);
		Assert.assertEquals(about1.getKey(), returnedAbout.getKey());
		Assert.assertEquals(about1.getValue(), returnedAbout.getValue());
	}
}
