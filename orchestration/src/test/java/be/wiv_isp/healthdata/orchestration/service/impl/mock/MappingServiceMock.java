/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl.mock;

import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.Progress;
import be.wiv_isp.healthdata.orchestration.dto.ValidationResponse;
import be.wiv_isp.healthdata.orchestration.mapping.IMappingContext;
import be.wiv_isp.healthdata.orchestration.mapping.MappingRequest;
import be.wiv_isp.healthdata.orchestration.mapping.MappingResponse;
import be.wiv_isp.healthdata.orchestration.service.IMappingService;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

public class MappingServiceMock implements IMappingService<AbstractRegistrationWorkflow> {


	@Override
	public MappingResponse mapFromCsvToJson(byte[] dcdContent, String content, IMappingContext context) {
		MappingResponse response = new MappingResponse();

		response.setUniqueID("uniqueId");
		response.setDocumentContent(new JSONObject());
		response.setProgress(new Progress());

		return response;
	}

	@Override
	public MappingResponse mapFromXmlToJson(byte[] dcdContent, String content, IMappingContext context) {
		MappingResponse response = new MappingResponse();

		response.setUniqueID("uniqueId");
		response.setDocumentContent(new JSONObject());
		response.setProgress(new Progress());

		return response;
	}

	@Override
	public JSONObject mapToElasticSearchDocument(AbstractRegistrationWorkflow workflow) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JSONObject getElasticSearchMapping(Long dataCollectionDefinitionId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MappingResponse merge(AbstractRegistrationWorkflow primary, AbstractRegistrationWorkflow secondary, IMappingContext context, byte[] dcdContent) {
		return null;
	}

	@Override
	public String getUniqueID(AbstractRegistrationWorkflow workflow) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getElasticSearchIndexSettings() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String mapFromJsonToCsv(byte[] documentContent, byte[] dcdContent, boolean stable) {
		return null;
	}

	@Override
	public String mapFromJsonToCsv(AbstractRegistrationWorkflow workflow, IMappingContext context, byte[] dcdContent, boolean stable) {
		return null;
	}

	@Override
	public boolean isValid(byte[] documentContent, byte[] dcdContent) {
		return false;
	}

	@Override
	public boolean isValid(AbstractRegistrationWorkflow workflow, IMappingContext context, byte[] dcdContent) {
		return false;
	}

	@Override
	public ValidationResponse getValidationResponse(byte[] documentContent, byte[] dcdContent) {
		return null;
	}

	@Override
	public ValidationResponse getValidationResponse(AbstractRegistrationWorkflow workflow, IMappingContext context, byte[] dcdContent) {
		return null;
	}

	@Override
	public JSONObject getComputedExpressions(AbstractRegistrationWorkflow workflow) {
		return null;
	}

	@Override
	public MappingResponse extractNotes(AbstractRegistrationWorkflow workflow, byte[] dcdContent) {
		MappingResponse mappingResponse = new MappingResponse();
		mappingResponse.setNotes(new JSONArray());
		return mappingResponse;
	}

	@Override
	public JSONObject get(MappingRequest request) {
		return null;
	}

	@Override
	public JSONObject get(MappingRequest request, String endpoint) {
		return null;
	}

}
