/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.common.domain.DateRange;
import be.wiv_isp.healthdata.common.domain.enumeration.TimeUnit;
import be.wiv_isp.healthdata.orchestration.dao.IPendingRegistrationDao;
import be.wiv_isp.healthdata.orchestration.domain.PeriodExpression;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.search.PendingRegistrationsViewSearch;
import be.wiv_isp.healthdata.orchestration.domain.view.PendingRegistrationsView;
import be.wiv_isp.healthdata.orchestration.util.DateUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Transactional
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class PendingRegistrationDaoTest {

	@Autowired
	private IPendingRegistrationDao dao;

	@Test
 	public void testSearchByUserId() {
		final Timestamp now = new Timestamp(new Date().getTime());
		final PendingRegistrationsView pendingRegistration1 = dao.create(buildPendingRegistration(1L, WorkflowStatus.IN_PROGRESS, 1L, "user", now));
		final PendingRegistrationsView pendingRegistration2 = dao.create(buildPendingRegistration(2L, WorkflowStatus.IN_PROGRESS, 2L, "user", now));

		PendingRegistrationsViewSearch search = new PendingRegistrationsViewSearch();
		List<PendingRegistrationsView> pendingRegistrations = dao.getAll(search);
		Assert.assertEquals(2, pendingRegistrations.size());
		Assert.assertTrue(pendingRegistrations.contains(pendingRegistration1));
		Assert.assertTrue(pendingRegistrations.contains(pendingRegistration2));

		search = new PendingRegistrationsViewSearch();
		search.setUserId(1L);
		pendingRegistrations = dao.getAll(search);
		Assert.assertEquals(1, pendingRegistrations.size());
		Assert.assertTrue(pendingRegistrations.contains(pendingRegistration1));
	}

	@Test
	public void testSearchByOpenSinceRange() {
		final Date now = new Date();
		final Timestamp timestamp1 = new Timestamp(DateUtils.remove(now, new PeriodExpression(4, TimeUnit.DAY)).getTime());
		final Timestamp timestamp2 = new Timestamp(DateUtils.remove(now, new PeriodExpression(3, TimeUnit.DAY)).getTime());
		final Timestamp timestamp3 = new Timestamp(DateUtils.remove(now, new PeriodExpression(2, TimeUnit.DAY)).getTime());
		final Timestamp timestamp4 = new Timestamp(DateUtils.remove(now, new PeriodExpression(1, TimeUnit.DAY)).getTime());
		final PendingRegistrationsView pendingRegistration1 = dao.create(buildPendingRegistration(1L, WorkflowStatus.IN_PROGRESS, 1L, "user", timestamp2));
		final PendingRegistrationsView pendingRegistration2 = dao.create(buildPendingRegistration(2L, WorkflowStatus.IN_PROGRESS, 2L, "user", timestamp4));

		PendingRegistrationsViewSearch search = new PendingRegistrationsViewSearch();
		List<PendingRegistrationsView> pendingRegistrations = dao.getAll(search);
		Assert.assertEquals(2, pendingRegistrations.size());
		Assert.assertTrue(pendingRegistrations.contains(pendingRegistration1));
		Assert.assertTrue(pendingRegistrations.contains(pendingRegistration2));

		search = new PendingRegistrationsViewSearch();
		search.setOpenSinceRange(new DateRange(timestamp1, timestamp3));
		pendingRegistrations = dao.getAll(search);
		Assert.assertEquals(1, pendingRegistrations.size());
		Assert.assertTrue(pendingRegistrations.contains(pendingRegistration1));

		search = new PendingRegistrationsViewSearch();
		search.setOpenSinceRange(new DateRange(null, timestamp3));
		pendingRegistrations = dao.getAll(search);
		Assert.assertEquals(1, pendingRegistrations.size());
		Assert.assertTrue(pendingRegistrations.contains(pendingRegistration1));

		search = new PendingRegistrationsViewSearch();
		search.setOpenSinceRange(new DateRange(timestamp3, null));
		pendingRegistrations = dao.getAll(search);
		Assert.assertEquals(1, pendingRegistrations.size());
		Assert.assertTrue(pendingRegistrations.contains(pendingRegistration2));
	}

	@Test
	public void testGetUserIds() {
		final Timestamp now = new Timestamp(new Date().getTime());
		dao.create(buildPendingRegistration(1L, WorkflowStatus.IN_PROGRESS, 1L, "user", now));
		dao.create(buildPendingRegistration(2L, WorkflowStatus.IN_PROGRESS, 1L, "user", now));
		dao.create(buildPendingRegistration(3L, WorkflowStatus.IN_PROGRESS, 2L, "user", now));
		dao.create(buildPendingRegistration(4L, WorkflowStatus.IN_PROGRESS, 3L, "user", now));

		final List<Long> userIds = dao.getUserIds();
		Assert.assertEquals(3, userIds.size());
		Assert.assertTrue(userIds.contains(1L));
		Assert.assertTrue(userIds.contains(2L));
		Assert.assertTrue(userIds.contains(3L));
	}

	private PendingRegistrationsView buildPendingRegistration(Long workflowId, WorkflowStatus status, Long userId, String username, Timestamp openSince) {
		final PendingRegistrationsView pendingRegistration = new PendingRegistrationsView();
		pendingRegistration.setWorkflowId(workflowId);
		pendingRegistration.setStatus(status);
		pendingRegistration.setUserId(userId);
		pendingRegistration.setUsername(username);
		pendingRegistration.setOpenSince(openSince);
		return pendingRegistration;
	}

}
