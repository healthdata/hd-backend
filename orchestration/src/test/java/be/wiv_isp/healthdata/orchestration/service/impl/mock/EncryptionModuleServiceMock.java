/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.service.impl.mock;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.dto.EncryptionModuleMessage;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractEncryptionModuleService;

import java.util.ArrayList;
import java.util.List;


public class EncryptionModuleServiceMock extends AbstractEncryptionModuleService {

    public static List<String> outgoing = new ArrayList<>();
    public static List<String> incoming = new ArrayList<>();

    @Override
    public void send(EncryptionModuleMessage message, Organization organization) {
        outgoing.add(message.getContent());
        return;
    }

    @Override
    public List<EncryptionModuleMessage> receive(Organization organization) {
        List<EncryptionModuleMessage> result = new ArrayList<>();

        for (int index=0; index<incoming.size(); index++) {
            String messageContent = incoming.get(index);
            for (String message : messageContent.split("\n")) {
                final EncryptionModuleMessage encryptionModuleMessage = new EncryptionModuleMessage();
                encryptionModuleMessage.setContent(message.trim());
                result.add(encryptionModuleMessage);
            }
            if (result.size() >= 3) {
                for (int i=index; i>=0; i--)
                    incoming.remove(incoming.get(i));
                break;
            }
        }

        return result;
    }
}
