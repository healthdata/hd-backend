/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.validation;

import org.junit.Rule;
import org.junit.rules.ExpectedException;

public class UserValidatorTest {

	@Rule
	public ExpectedException exception = ExpectedException.none();

	private static final String USERNAME = "admin";
	private static final String PASSWORD = "password";
	private static final String AUTHORITY = "ROLE_USER";

/*	@Test
	public void testUserValidationUsernameEmptyString() {
		final User user = buildUser("", PASSWORD, Collections.singleton(new AuthorityBuilder().withAuthority(AUTHORITY).build()));
		exception.expect(HealthDataException.class);
		exception.expectMessage(ExceptionType.USER_NO_USERNAME_SPECIFIED.getMessage());
		UserValidator.validateUser(user);
	}

	@Test
	public void testUserValidationUsernameNull() {
		final User user = buildUser(null, PASSWORD, Collections.singleton(new AuthorityBuilder().withAuthority(AUTHORITY).build()));
		exception.expect(HealthDataException.class);
		exception.expectMessage(ExceptionType.USER_NO_USERNAME_SPECIFIED.getMessage());
		UserValidator.validateUser(user);
	}

	@Test
	public void testUserValidationPasswordEmptyString() {
		final User user = buildUser(USERNAME, "", Collections.singleton(new AuthorityBuilder().withAuthority(AUTHORITY).build()));
		exception.expect(HealthDataException.class);
		exception.expectMessage(ExceptionType.USER_NO_PASSWORD_SPECIFIED.getMessage());
		UserValidator.validateUser(user);
	}

	@Test
	public void testUserValidationPasswordNull() {
		final User user = buildUser(USERNAME, null, Collections.singleton(new AuthorityBuilder().withAuthority(AUTHORITY).build()));
		exception.expect(HealthDataException.class);
		exception.expectMessage(ExceptionType.USER_NO_PASSWORD_SPECIFIED.getMessage());
		UserValidator.validateUser(user);
	}

	@Test
	public void testUserValidationAuthoritiesEmptyCollection() {
		final User user = buildUser(USERNAME, PASSWORD, Collections.EMPTY_SET);
		exception.expect(HealthDataException.class);
		exception.expectMessage(ExceptionType.USER_NO_AUTHORITIES_SPECIFIED.getMessage());
		UserValidator.validateUser(user);
	}

	@Test
	public void testUserValidationAuthoritiesNull() {
		final User user = buildUser(USERNAME, PASSWORD, null);
		exception.expect(HealthDataException.class);
		exception.expectMessage(ExceptionType.USER_NO_AUTHORITIES_SPECIFIED.getMessage());
		UserValidator.validateUser(user);
	}

	private User buildUser(String username, String password, Set<Authority> authorities) {
		User user = new UserBuilder().withUsername(username).withPassword(password).withEnabled(true).build();
		user.setAuthorities(authorities);
		return user;
	}*/
}
