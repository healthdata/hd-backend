/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl.mock;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.search.OrganizationSearch;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;

import java.util.List;

public class OrganizationServiceMock implements IOrganizationService {

	private Organization main;

	{
		main = new Organization();
		main.setId(1L);
		main.setMain(true);
		main.setHealthDataIDType("RIZIV");
		main.setHealthDataIDValue("11111111");
		main.setName("Main Organization");
		main.setDeleted(false);
	}

	@Override
	public Organization get(Long id) {
		return null;
	}

	@Override
	public Organization getMain() {
		return main;
	}

	@Override
	public List<Organization> getAll(OrganizationSearch search) {
		return null;
	}

	@Override
	public List<Organization> getAll() {
		return null;
	}

	@Override
	public Organization getUnique(OrganizationSearch search) {
		return null;
	}

	@Override
	public void delete(Organization organization) {

	}

	@Override
	public Organization create(Organization organization) {
		return null;
	}

	@Override
	public Organization update(Organization organization) {
		return null;
	}

	@Override
	public Organization getOrganizationOrMain(Long id) {
		return null;
	}
}
