/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl.mock;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.AbstractWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.DataCollection;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.service.IUserDataCollectionService;

import java.util.Set;

public class UserDataCollectionServiceMock implements IUserDataCollectionService {
    @Override
    public boolean isUserAuthorized(HDUserDetails userDetails, AbstractWorkflow workflow) {
        return true;
    }

    @Override
    public boolean isUserAuthorized(HDUserDetails userDetails, String dataCollectionName) {
        return true;
    }

    @Override
    public Set<String> get(HDUserDetails userDetails) {
        return null;
    }

    @Override
    public Set<String> getEmailAddresses(DataCollection dataCollection) {
        return null;
    }

    @Override
    public Set<String> getEmailAddresses(DataCollection dataCollection, Organization organization) {
        return null;
    }
}
