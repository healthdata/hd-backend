/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.dao.IUserConfigurationDao;
import be.wiv_isp.healthdata.orchestration.dao.IUserDao;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.UserConfiguration;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.UserConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.search.UserConfigurationSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.sql.SQLException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class UserConfigurationDaoTest {

    public static final UserConfigurationKey KEY_1 = UserConfigurationKey.LOWER_BOUND_FOR_IMMEDIATE_NOTIFICATION;
    public static final UserConfigurationKey KEY_2 = UserConfigurationKey.REMINDERS_FREQUENCY;

    @Autowired
    private IUserConfigurationDao dao;

    @Autowired
    private IUserDao userDao;

    @Autowired
    private IOrganizationDao organizationDao;

    @Autowired
    private javax.sql.DataSource dataSource;

    @Autowired
    @Qualifier("transactionManager")
    protected PlatformTransactionManager txManager;

    private User user1;
    private User user2;
    private Organization organization;

    @Test
    @Transactional
    public void testSearchByUserId() {
        final UserConfiguration configuration1 = buildConfiguration();
        final UserConfiguration created1 = dao.create(configuration1);
        Assert.assertNotNull(created1);
        Assert.assertEquals(Long.valueOf(1L), configuration1.getUser().getId());

        final UserConfiguration userConfiguration2 = buildConfiguration();
        userConfiguration2.setUser(getUser(2));
        final UserConfiguration created2 = dao.create(userConfiguration2);
        Assert.assertNotNull(created2);

        Assert.assertNotEquals(created1.getUser(), created2.getUser());

        UserConfigurationSearch search = new UserConfigurationSearch();
        List<UserConfiguration> all = dao.getAll(search);
        Assert.assertEquals(2, all.size());
        Assert.assertTrue(all.contains(created1));
        Assert.assertTrue(all.contains(created2));

        search = new UserConfigurationSearch();
        search.setUserId(1L);
        all = dao.getAll(search);
        Assert.assertEquals(1, all.size());
        Assert.assertTrue(all.contains(created1));
    }

    @Test
    @Transactional
    public void testSearchByConfigurationKey() {
        final UserConfiguration userConfiguration1 = buildConfiguration();
        final UserConfiguration created1 = dao.create(userConfiguration1);
        Assert.assertNotNull(created1);
        Assert.assertEquals(KEY_1, userConfiguration1.getKey());

        final UserConfiguration userConfiguration2 = buildConfiguration();
        userConfiguration2.setKey(KEY_2);
        final UserConfiguration created2 = dao.create(userConfiguration2);
        Assert.assertNotNull(created2);

        Assert.assertNotEquals(created1.getKey(), created2.getKey());

        UserConfigurationSearch search = new UserConfigurationSearch();
        List<UserConfiguration> all = dao.getAll(search);
        Assert.assertEquals(2, all.size());
        Assert.assertTrue(all.contains(created1));
        Assert.assertTrue(all.contains(created2));

        search = new UserConfigurationSearch();
        search.setKey(KEY_1);
        all = dao.getAll(search);
        Assert.assertEquals(1, all.size());
        Assert.assertTrue(all.contains(created1));
    }

    @Test
    public void testDeleteWithInvalidKey() throws SQLException {

        // create 2 configurations
        TransactionTemplate txTemplate = new TransactionTemplate(txManager);
        txTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                UserConfiguration configuration = buildConfiguration();
                configuration.setKey(UserConfigurationKey.NEW_REGISTRATION_PERIOD);
                dao.create(configuration);

                configuration = buildConfiguration();
                configuration.setKey(UserConfigurationKey.OLD_REGISTRATION_PERIOD);
                dao.create(configuration);
            }
        });

        Assert.assertEquals(2, dao.getAll().size());

        // introduce an invalid key
        dataSource.getConnection().prepareStatement("UPDATE USER_CONFIGURATIONS SET LOOKUP_KEY = 'NON_EXISTING_CONFIGURATION_KEY' WHERE LOOKUP_KEY = 'NEW_REGISTRATION_PERIOD'").executeUpdate();

        try {
            dao.getAll();
            Assert.fail("An illegal argument exception must be thrown since a non valid key has been introduced");
        } catch (IllegalArgumentException e) {

        }

        // remove invalid keys (method to test)
        txTemplate = new TransactionTemplate(txManager);
        txTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                dao.deleteWithInvalidKey();
            }
        });

        // verify invalid key has been deleted
        final List<UserConfiguration> all = dao.getAll();
        Assert.assertEquals(1, all.size());
        Assert.assertEquals(UserConfigurationKey.OLD_REGISTRATION_PERIOD, all.get(0).getKey());
    }

    private UserConfiguration buildConfiguration() {
        final UserConfiguration configuration = new UserConfiguration();
        configuration.setKey(KEY_1);
        configuration.setValue("10");
        configuration.setDefaultValue("10");
        configuration.setUser(getUser());
        return configuration;
    }

    private User getUser() {
        return getUser(1);
    }

    private User getUser(int id) {
        if(id == 1) {
            if (user1 == null) {
                final User u = new User();
                u.setUsername("user1");
                u.setLastName("doe");
                u.setFirstName("john");
                u.setEmail("john.doe@mail.com");
                u.setLdapUser(false);
                u.setEnabled(true);
                u.setAuthorities(new HashSet<>(Collections.singletonList(new Authority(Authority.USER))));
                u.setDataCollectionNames(new HashSet<>(Collections.singletonList("TEST")));
                u.setOrganization(getOrganization());
                user1 = userDao.create(u);
            }
            return user1;
        }
        if(id == 2) {
            if (user2 == null) {
                final User u = new User();
                u.setUsername("user2");
                u.setLastName("doe");
                u.setFirstName("john");
                u.setEmail("john.doe@mail.com");
                u.setLdapUser(false);
                u.setEnabled(true);
                u.setAuthorities(new HashSet<>(Collections.singletonList(new Authority(Authority.USER))));
                u.setDataCollectionNames(new HashSet<>(Collections.singletonList("TEST")));
                u.setOrganization(getOrganization());
                user2 = userDao.create(u);
            }
            return user2;
        }
        return null;
    }

    private Organization getOrganization() {
        if (organization == null) {
            final Organization o = new Organization();
            o.setHealthDataIDType("RIZIV");
            o.setHealthDataIDValue("11111111");
            o.setName("MyOrganization");
            o.setMain(true);
            o.setDeleted(false);
            organization = organizationDao.create(o);
        }
        return organization;
    }
}
