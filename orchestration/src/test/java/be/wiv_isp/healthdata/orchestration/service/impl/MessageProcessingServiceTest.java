/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.action.standalone.StandAloneAction;
import be.wiv_isp.healthdata.orchestration.action.workflow.WorkflowAction;
import be.wiv_isp.healthdata.orchestration.action.workflow.executor.IParticipationWorkflowActionExecutor;
import be.wiv_isp.healthdata.orchestration.action.workflow.executor.IRegistrationWorkflowActionExecutor;
import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.CSVContent;
import be.wiv_isp.healthdata.orchestration.domain.HDServiceUser;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.factory.IActionFactory;
import be.wiv_isp.healthdata.orchestration.service.IMessageProcessingService;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.test.util.ReflectionTestUtils;

import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

public class MessageProcessingServiceTest {

	private IMessageProcessingService messageProcessingService;

	private IParticipationWorkflowActionExecutor participationWorkflowActionExecutor;
	private IRegistrationWorkflowActionExecutor registrationWorkflowActionExecutor;
	private IActionFactory actionFactory;
	private IMessageService messageService;

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Before
	public void setUp() throws IllegalAccessException, InvocationTargetException {
		participationWorkflowActionExecutor = EasyMock.createMock(IParticipationWorkflowActionExecutor.class);
		registrationWorkflowActionExecutor = EasyMock.createMock(IRegistrationWorkflowActionExecutor.class);
		actionFactory = EasyMock.createMock(IActionFactory.class);
		messageService = EasyMock.createMock(IMessageService.class);

		messageProcessingService = new MessageProcessingService();

		ReflectionTestUtils.setField(messageProcessingService, "participationWorkflowActionExecutor", participationWorkflowActionExecutor);
		ReflectionTestUtils.setField(messageProcessingService, "registrationWorkflowActionExecutor", registrationWorkflowActionExecutor);
		ReflectionTestUtils.setField(messageProcessingService, "actionFactory", actionFactory);
		ReflectionTestUtils.setField(messageProcessingService, "messageService", messageService);
	}

	@Test
	public void testProcessWorkflowAction() {
		final WorkflowAction action = EasyMock.createMock(WorkflowAction.class);

		final CSVContent csvContent = new CSVContent();
		csvContent.putWorkflowId("HD4DP_ID");
		csvContent.setLastField("content");
		csvContent.setUseDescriptors(false);

		final Message message = buildMessage(1L, csvContent, "m_id", MessageStatus.INBOX, MessageType.WORKFLOW_ACTION);

		EasyMock.expect(actionFactory.create(message)).andReturn(action);
		EasyMock.expect(registrationWorkflowActionExecutor.execute(action, HDServiceUser.create(HDServiceUser.Username.MESSAGING))).andReturn(new AbstractRegistrationWorkflow() {
			@Override
			public WorkflowType getWorkflowType() {
				return WorkflowType.REGISTRATION;
			}
		});
		messageService.delete(message.getId());
		EasyMock.expectLastCall();

		EasyMock.replay(actionFactory, participationWorkflowActionExecutor, registrationWorkflowActionExecutor, action);

		messageProcessingService.process(message);

		EasyMock.verify(actionFactory, participationWorkflowActionExecutor, registrationWorkflowActionExecutor, action);
	}

	@Test
	public void testProcessStandAloneAction() {
		final StandAloneAction action = EasyMock.createMock(StandAloneAction.class);

		final CSVContent csvContent = new CSVContent();
		csvContent.putWorkflowId("HD4DP_ID");
		csvContent.setLastField("content");
		csvContent.setUseDescriptors(false);

		final Message message = buildMessage(1L, csvContent, "m_id", MessageStatus.INBOX, MessageType.ACTION);

		EasyMock.expect(actionFactory.create(message)).andReturn(action);
		action.execute();
		EasyMock.expectLastCall();
		messageService.delete(message.getId());
		EasyMock.expectLastCall();

		EasyMock.replay(actionFactory, action);

		messageProcessingService.process(message);

		EasyMock.verify(actionFactory, action);
	}

	@Test
	public void testProcessWorkflowActionThrowsExcpetion() {
		final WorkflowAction action = EasyMock.createMock(WorkflowAction.class);

		final CSVContent csvContent = new CSVContent();
		csvContent.putWorkflowId("HD4DP_ID");
		csvContent.setLastField("content");
		csvContent.setUseDescriptors(false);

		final Message message = buildMessage(1L, csvContent, "m_id", MessageStatus.INBOX, MessageType.WORKFLOW_ACTION);

		EasyMock.expect(actionFactory.create(message)).andThrow(new RuntimeException("exception_message"));
		EasyMock.expect(messageService.update(message)).andReturn(message);

		EasyMock.replay(actionFactory, action);

		exception.expect(HealthDataException.class);
		exception.expectMessage(MessageFormat.format(ExceptionType.ERROR_WHILE_PROCESSING_MESSAGE.getMessage(), message.getMetadata().get(Message.Metadata.MESSAGE_ID), "exception_message"));

		try {
			messageProcessingService.process(message);
		} catch (HealthDataException e) {
			EasyMock.verify(actionFactory, action);
			throw e;
		}

		Assert.assertFalse(true);
	}

	private Message buildMessage(Long id, CSVContent csvContent, String messageId, MessageStatus status, MessageType type) {
		final Map<String, String> metadata = new HashMap<String, String>();
		metadata.put(Message.Metadata.MESSAGE_ID, messageId);

		final Message message = new Message();
		message.setId(id);
		message.setContent(csvContent.toString().getBytes(StandardCharsets.UTF_8));
		message.setStatus(status);
		message.setMetadata(metadata);
		message.setType(type);

		return message;
	}
}
