/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.dao.IUserDao;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class UserDaoTest {

	@Autowired
	private IOrganizationDao organizationDao;

	@Autowired
	private IUserDao dao;

	@Test
	public void testGetById() {
		final Organization organization = organizationDao.create(buildOrganization(true));
		final User user = dao.create(buildUser("username", "password", "firstName", "lastName", "firstName.lastName@email.be", false, organization, new Authority("ROLE_USER")));

		final User retrievedUser = dao.get(user.getId());
		Assert.assertNotNull(retrievedUser);
		Assert.assertEquals(user, retrievedUser);
	}

	@Test
	public void testGetUnexistingId() {
		User user = dao.get(-1L);
		Assert.assertNull(user);
	}

	@Test
	public void testUpdate() {
		final Organization organization = organizationDao.create(buildOrganization(true));
		final User user = dao.create(buildUser("username", "password", "firstName", "lastName", "firstName.lastName@email.be", false, organization, new Authority("ROLE_USER")));

		user.setPassword("updated_password");
		final User updatedUser = dao.update(user);
		Assert.assertNotNull(updatedUser);
		Assert.assertEquals(user, updatedUser);
	}

	@Test
	public void testGetAll() throws Exception {
		final Organization organization = organizationDao.create(buildOrganization(true));

		final User user1 = dao.create(buildUser("username1", "password", "firstName1", "lastName1", "firstName1.lastName1@email.be", false, organization, new Authority("ROLE_USER")));
		final User user2 = dao.create(buildUser("username2", "password", "firstName2", "lastName2", "firstName2.lastName2@email.be", false, organization, new Authority("ROLE_USER")));
		final User user3 = dao.create(buildUser("username3", "password", "firstName3", "lastName3", "firstName3.lastName3@email.be", false, organization, new Authority("ROLE_USER")));

		final List<User> returnedUsers = dao.getAll();

		Assert.assertEquals(3, returnedUsers.size());
		Assert.assertTrue(returnedUsers.contains(user1));
		Assert.assertTrue(returnedUsers.contains(user2));
		Assert.assertTrue(returnedUsers.contains(user3));
	}

	/*@Test
	public void testPagination() {
		final Organization organization = organizationDao.create(buildOrganization(true));

		Map<Long, User> userMap = new HashMap<>();
		for (long i = 1; i <= 11; i++) {
			final User user = dao.create(buildUser("username_" + i, "password", "firstName", "lastName", "firstName.lastName@email.be", false, organization, new Authority("ROLE_USER")));
			userMap.put(user.getId(), user);
		}

		final PaginationRange range = new PaginationRange(1, 10);
		final UserSearch search = new UserSearch();
		search.setRange(range);

		final List<User> users = dao.getAll(search);
		Assert.assertEquals(10, users.size());
		Assert.assertEquals(11, search.getRange().getTotal());
	}*/

	@Test
	public void testSearchByUsername() {
		final Organization organization = organizationDao.create(buildOrganization(false));

		final User user1 = dao.create(buildUser("username1", "password", "firstName", "lastName", "firstName.lastName@email.be", false, organization, new Authority("ROLE_USER")));
		final User user2 = dao.create(buildUser("username2", "password", "firstName", "lastName", "firstName.lastName@email.be", false, organization, new Authority("ROLE_USER")));

		UserSearch search = new UserSearch();
		List<User> users = dao.getAll(search);
		Assert.assertEquals(2, users.size());
		Assert.assertTrue(users.contains(user1));
		Assert.assertTrue(users.contains(user2));

		search = new UserSearch();
		search.setUsername("username1");
		users = dao.getAll(search);
		Assert.assertEquals(1, users.size());
		Assert.assertTrue(users.contains(user1));

		search = new UserSearch();
		search.setUsernames(new ArrayList<String>());
		users = dao.getAll(search);
		Assert.assertEquals(0, users.size());
	}

	@Test
	public void testSearchByEmail() {
		final Organization organization = organizationDao.create(buildOrganization(false));

		final User user1 = dao.create(buildUser("username", "password", "firstName", "lastName", "username1@email.be", false, organization, new Authority("ROLE_USER")));
		final User user2 = dao.create(buildUser("username", "password", "firstName", "lastName", "username2@email.be", false, organization, new Authority("ROLE_USER")));

		UserSearch search = new UserSearch();
		List<User> users = dao.getAll(search);
		Assert.assertEquals(2, users.size());
		Assert.assertTrue(users.contains(user1));
		Assert.assertTrue(users.contains(user2));

		search = new UserSearch();
		search.setEmail("username1@email.be");
		users = dao.getAll(search);
		Assert.assertEquals(1, users.size());
		Assert.assertTrue(users.contains(user1));
	}

	@Test
	public void testSearchByOrganization() {
		final Organization organization1 = organizationDao.create(buildOrganization(false));
		final Organization organization2 = organizationDao.create(buildOrganization(false));

		final User user1 = dao.create(buildUser("username", "password", "firstName", "lastName", "firstName.lastName@email.be", false, organization1, new Authority("ROLE_USER")));
		final User user2 = dao.create(buildUser("username", "password", "firstName", "lastName", "firstName.lastName@email.be", false, organization2, new Authority("ROLE_USER")));

		UserSearch search = new UserSearch();
		List<User> users = dao.getAll(search);
		Assert.assertEquals(2, users.size());
		Assert.assertTrue(users.contains(user1));
		Assert.assertTrue(users.contains(user2));

		search = new UserSearch();
		search.setOrganization(organization1);

		users = dao.getAll(search);
		Assert.assertEquals(1, users.size());
		Assert.assertTrue(users.contains(user1));
	}

	@Test
	public void testSearchByLdapUser() {
		final Organization organization = organizationDao.create(buildOrganization(false));

		final User user1 = dao.create(buildUser("username", "password", "firstName", "lastName", "firstName.lastName@email.be", true, organization, new Authority("ROLE_USER")));
		final User user2 = dao.create(buildUser("username", "password", "firstName", "lastName", "firstName.lastName@email.be", false, organization, new Authority("ROLE_USER")));

		UserSearch search = new UserSearch();
		List<User> users = dao.getAll(search);
		Assert.assertEquals(2, users.size());
		Assert.assertTrue(users.contains(user1));
		Assert.assertTrue(users.contains(user2));

		search = new UserSearch();
		search.setLdapUser(true);

		users = dao.getAll(search);
		Assert.assertEquals(1, users.size());
		Assert.assertTrue(users.contains(user1));
	}

	@Test
	public void testSearchByDataCollections() {
		final Organization organization = organizationDao.create(buildOrganization(false));

		final User user1 = buildUser("username1", "password", "firstName", "lastName", "firstName.lastName@email.be", false, organization, new Authority("ROLE_USER"));
		Set<String> dataCollectionNames = new HashSet<>();
		dataCollectionNames.add("TEST");
		dataCollectionNames.add("TEST1");
		dataCollectionNames.add("TEST2");
		user1.setDataCollectionNames(dataCollectionNames);
		dao.create(user1);

		final User user2 = buildUser("username2", "password", "firstName", "lastName", "firstName.lastName@email.be", false, organization, new Authority("ROLE_USER"));
		dataCollectionNames = new HashSet<>();
		dataCollectionNames.add("TEST");
		dataCollectionNames.add("TEST1");
		user2.setDataCollectionNames(dataCollectionNames);
		dao.create(user2);

		List<User> users = dao.getAll();
		Assert.assertEquals(2, users.size());
		Assert.assertTrue(users.contains(user1));
		Assert.assertTrue(users.contains(user2));

		UserSearch search = new UserSearch();
		search.setDataCollectionName("TEST");
		users = dao.getAll(search);
		Assert.assertEquals(2, users.size());
		Assert.assertTrue(users.contains(user1));
		Assert.assertTrue(users.contains(user2));

		search = new UserSearch();
		search.setDataCollectionName("TEST1");
		users = dao.getAll(search);
		Assert.assertEquals(2, users.size());
		Assert.assertTrue(users.contains(user1));
		Assert.assertTrue(users.contains(user2));

		search = new UserSearch();
		search.setDataCollectionName("TEST2");

		users = dao.getAll(search);
		Assert.assertEquals(1, users.size());
		Assert.assertTrue(users.contains(user1));
	}

	@Test
	public void testSearchByAuthority() {
		final Organization organization = organizationDao.create(buildOrganization(false));

		final User created1 = dao.create(buildUser("admin", "password", "firstName", "lastName", "firstName.lastName@email.be", false, organization, new Authority("ROLE_USER")));
		final User created2 = dao.create(buildUser("user", "password", "firstName", "lastName", "firstName.lastName@email.be", false, organization, new Authority("ROLE_ADMIN")));

		UserSearch search = new UserSearch();
		List<User> users = dao.getAll(search);
		Assert.assertEquals(2, users.size());
		Assert.assertTrue(users.contains(created1));
		Assert.assertTrue(users.contains(created2));

		search = new UserSearch();
		search.setAuthority(new Authority("ROLE_USER"));
		users = dao.getAll(search);
		Assert.assertEquals(1, users.size());
		Assert.assertTrue(users.contains(created1));
	}

	@Test
	public void testSearchByExcludingAuthority() {
		final Organization organization = organizationDao.create(buildOrganization(false));

		final User created1 = dao.create(buildUser("admin", "password", "firstName", "lastName", "firstName.lastName@email.be", false, organization, new Authority("ROLE_USER"), new Authority("ROLE_ADMIN")));
		final User created2 = dao.create(buildUser("user", "password", "firstName", "lastName", "firstName.lastName@email.be", false, organization, new Authority("ROLE_ADMIN")));

		UserSearch search = new UserSearch();
		List<User> users = dao.getAll(search);
		Assert.assertEquals(2, users.size());
		Assert.assertTrue(users.contains(created1));
		Assert.assertTrue(users.contains(created2));

		search = new UserSearch();
		search.setExcludeAuthority(new Authority("ROLE_USER"));
		users = dao.getAll(search);
		Assert.assertEquals(1, users.size());
		Assert.assertTrue(users.contains(created2));
	}

	@Test
	public void testSearchByIncompleteUserInfo() {
		final Organization organization = organizationDao.create(buildOrganization(false));

		final User user1 = dao.create(buildUser("username", "password", null, "lastName", "username@email.be", false, organization, new Authority("ROLE_USER")));
		final User user2 = dao.create(buildUser("username", "password", "firstName", "lastName", "username@email.be", false, organization, new Authority("ROLE_USER")));

		UserSearch search = new UserSearch();
		List<User> users = dao.getAll(search);
		Assert.assertEquals(2, users.size());
		Assert.assertTrue(users.contains(user1));
		Assert.assertTrue(users.contains(user2));

		search = new UserSearch();
		search.setIncompleteUserInfo(true);
		users = dao.getAll(search);
		Assert.assertEquals(1, users.size());
		Assert.assertTrue(users.contains(user1));
	}

	private User buildUser(String userName, String password, String firstName, String lastName, String email, boolean ldapUser, Organization organization, Authority... roles) {
		final User user = new User();
		user.setUsername(userName);
		user.setPassword(password);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
		user.setEnabled(true);
		user.setAuthorities(new HashSet<>(Arrays.asList(roles)));
		user.setOrganization(organization);
		user.setLdapUser(ldapUser);
		return user;
	}

	private Organization buildOrganization(boolean main) {
		final Organization organization = new Organization();
		organization.setMain(main);
		return organization;
	}
}
