/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.util;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class FileSystemUtilsTest {

	private final int NB_FILES_TO_MOVE = 3;

	@Rule
	public TemporaryFolder root = new TemporaryFolder();

	private File srcDir;
	private File destDir;

	private List<String> srcFilenames;

	@Before
	public void setUp() throws IOException {
		srcDir = root.newFolder("src");
		destDir = root.newFolder("dest");

		// insert source filenames
		srcFilenames = new ArrayList<>();
		for (int i = 0; i < NB_FILES_TO_MOVE; ++i) {
			srcFilenames.add(MessageFormat.format("file{0}.txt", i));
		}

		// create source files
		for (final String srcFilename : srcFilenames) {
			Paths.get(srcDir.getAbsolutePath(), srcFilename).toFile().createNewFile();
		}
	}

	@Test
	public void testWithUnexistingDaySubfolder() throws IOException {
		runTestWithDaySubfolder();
	}

	@Test
	public void testWithExistingDaySubfolder() throws IOException {
		Paths.get(destDir.getAbsolutePath(), new SimpleDateFormat(DateFormat.DAY_TIMESTAMP.getPattern()).format(new Date())).toFile().mkdir();
		runTestWithDaySubfolder();
	}

	@Test
	public void testWithoutDaySubfolder() throws IOException {

		FileSystemUtils.moveFilesToDirectory(destDir, false, srcDir.listFiles());

		// verify that all files have been removed from the sub directory
		Assert.assertEquals(0, srcDir.listFiles().length);

		// verify that the dest folder contains all the source filenames
		final String[] names = destDir.list();
		Assert.assertTrue(srcFilenames.containsAll(Arrays.asList(names)));
		Assert.assertTrue(Arrays.asList(names).containsAll(srcFilenames));

	}

	@Test
	public void testWithDaySubfolderAndNonUniqueFiles() throws IOException {
		final File daySubFolder = Paths.get(destDir.getAbsolutePath(), new SimpleDateFormat(DateFormat.DAY_TIMESTAMP.getPattern()).format(new Date())).toFile();
		daySubFolder.mkdir();

		// create a file with the same name as a source filename to generate a
		// conflict
		Paths.get(daySubFolder.getAbsolutePath(), srcFilenames.get(0)).toFile().createNewFile();

		FileSystemUtils.moveFilesToDirectory(destDir, true, srcDir.listFiles());

		// verify that all files have been removed from the sub directory
		Assert.assertEquals(0, srcDir.listFiles().length);

		// verify that the destination directory contains one folder
		File[] files = destDir.listFiles();
		Assert.assertEquals(1, files.length);
		final File subFolder = files[0];
		Assert.assertTrue(subFolder.isDirectory());

		// verify that this folder contains 2 files (the conflicting file and
		// the uniquely created folder)
		files = subFolder.listFiles();
		Assert.assertEquals(2, files.length);

		// retrieve the uniquely created folder
		File uniqueSubFolder = null;
		for (final File file : files) {
			if (file.isDirectory()) {
				uniqueSubFolder = file;
			}
		}
		Assert.assertNotNull(uniqueSubFolder);

		// verify that the dest folder contains all the source filenames
		final String[] names = uniqueSubFolder.list();
		Assert.assertTrue(srcFilenames.containsAll(Arrays.asList(names)));
		Assert.assertTrue(Arrays.asList(names).containsAll(srcFilenames));
	}

	@Test
	public void testWriteStringToFile() throws IOException {
		final File directory = root.newFolder("test");
		final File file = new File(directory + "/test.txt");
		final String fileContents = "test";
		FileSystemUtils.writeStringToFile(file, fileContents);
		Assert.assertTrue(file.exists());
		String fileContentsAfterWrite = FileUtils.readFileToString(file);
		Assert.assertEquals(fileContents, fileContentsAfterWrite);
	}

	private void runTestWithDaySubfolder() {
		FileSystemUtils.moveFilesToDirectory(destDir, true, srcDir.listFiles());

		// verify that all files have been removed from the sub directory
		Assert.assertEquals(0, srcDir.listFiles().length);

		// verify that the destination directory contains one folder
		final File[] files = destDir.listFiles();
		Assert.assertEquals(1, files.length);
		final File subFolder = files[0];
		Assert.assertTrue(subFolder.isDirectory());

		// verify that this folder contains all the source filenames
		final String[] names = subFolder.list();
		Assert.assertTrue(srcFilenames.containsAll(Arrays.asList(names)));
		Assert.assertTrue(Arrays.asList(names).containsAll(srcFilenames));
	}
}
