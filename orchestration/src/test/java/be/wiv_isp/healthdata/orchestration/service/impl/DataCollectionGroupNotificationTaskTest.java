/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.domain.enumeration.TemplateKey;
import be.wiv_isp.healthdata.common.domain.enumeration.TimeUnit;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupDto;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.DataCollection;
import be.wiv_isp.healthdata.orchestration.domain.EventDataCollectionGroup;
import be.wiv_isp.healthdata.orchestration.domain.PeriodExpression;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.EventType;
import be.wiv_isp.healthdata.orchestration.domain.mail.DataCollectionDefinitionContext;
import be.wiv_isp.healthdata.orchestration.service.*;
import be.wiv_isp.healthdata.orchestration.tasks.DataCollectionGroupNotificationTask;
import be.wiv_isp.healthdata.orchestration.util.DateUtils;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class DataCollectionGroupNotificationTaskTest {

    private DataCollectionGroupNotificationTask dataCollectionGroupNotificationTask = new DataCollectionGroupNotificationTask();

    @Autowired
    private IEventDataCollectionGroupService eventDataCollectionGroupService;

    private String GUI_HOST = "http://hd4dp.be";

    private Configuration guiHostConfig;

    private IUserDataCollectionService userDataCollectionServiceMock = EasyMock.createNiceMock(IUserDataCollectionService.class);
    private IMailService mailServiceMock = EasyMock.createNiceMock(IMailService.class);
    private IConfigurationService configurationServiceMock = EasyMock.createNiceMock(IConfigurationService.class);
    private IDataCollectionGroupForwardService dataCollectionGroupForwardServiceMock = EasyMock.createNiceMock(IDataCollectionGroupForwardService.class);

    @Before
    public void before() {
        guiHostConfig = new Configuration();
        guiHostConfig.setKey(ConfigurationKey.GUI_HOST);
        guiHostConfig.setValue(GUI_HOST);

        ReflectionTestUtils.setField(dataCollectionGroupNotificationTask, "eventDataCollectionGroupService", eventDataCollectionGroupService);
        ReflectionTestUtils.setField(dataCollectionGroupNotificationTask, "configurationService", configurationServiceMock);
        ReflectionTestUtils.setField(dataCollectionGroupNotificationTask, "userDataCollectionService", userDataCollectionServiceMock);
        ReflectionTestUtils.setField(dataCollectionGroupNotificationTask, "mailService", mailServiceMock);
        ReflectionTestUtils.setField(dataCollectionGroupNotificationTask, "dataCollectionGroupForwardService", dataCollectionGroupForwardServiceMock);
    }

    @Test
    public void testStartDataCollectionEventIsSentAndEventIsDeleted() {
        final Timestamp startDate = createTimestampIn(-1, TimeUnit.DAY);

        EasyMock.expect(configurationServiceMock.get(ConfigurationKey.GUI_HOST)).andReturn(guiHostConfig).anyTimes();

        final DataCollectionGroupDto group = new DataCollectionGroupDto();
        group.setName("TEST");
        group.setMajorVersion(1);
        group.setStartDate(startDate);
        EasyMock.expect(dataCollectionGroupForwardServiceMock.get("TEST", 1)).andReturn(group);

        final EventDataCollectionGroup event = new EventDataCollectionGroup();
        event.setType(EventType.START_DATA_COLLECTION_PERIOD);
        event.setMajorVersion(1);
        event.setDataCollectionGroupName("TEST");
        event.addNotificationTime(startDate);
        eventDataCollectionGroupService.create(event);

        final Set<String> emailAddresses = new HashSet<>(Arrays.asList("test1@test.be", "test2@test.be"));
        EasyMock.expect(userDataCollectionServiceMock.getEmailAddresses(new DataCollection("TEST"))).andReturn(emailAddresses);

        final DataCollectionDefinitionContext context = new DataCollectionDefinitionContext();
        context.setDataCollectionName("TEST");
        context.setInstallationUrl(GUI_HOST);
        mailServiceMock.createAndSendMail(EasyMock.eq(TemplateKey.START_OF_DATA_COLLECTION_PERIOD), EasyMock.eq(context), EasyMock.eq(emailAddresses));
        EasyMock.expectLastCall();

        Assert.assertEquals(1, eventDataCollectionGroupService.getAll().size());
        EasyMock.replay(userDataCollectionServiceMock, mailServiceMock, configurationServiceMock, dataCollectionGroupForwardServiceMock);

        dataCollectionGroupNotificationTask.run();

        EasyMock.verify(userDataCollectionServiceMock, mailServiceMock, configurationServiceMock, dataCollectionGroupForwardServiceMock);
        Assert.assertEquals(0, eventDataCollectionGroupService.getAll().size());
    }

    @Test
    public void testEndDataCollectionEventIsSentAndEventIsNotDeleted() {
        EasyMock.expect(configurationServiceMock.get(ConfigurationKey.GUI_HOST)).andReturn(guiHostConfig);

        final Timestamp endDateCreation = createTimestampIn(2, TimeUnit.WEEK);
        final Timestamp endDateSubmission = createTimestampIn(3, TimeUnit.WEEK);
        final Timestamp endDateComments = createTimestampIn(4, TimeUnit.WEEK);

        final DataCollectionGroupDto group = new DataCollectionGroupDto();
        group.setName("TEST");
        group.setMajorVersion(1);
        group.setEndDateCreation(endDateCreation);
        group.setEndDateSubmission(endDateSubmission);
        group.setEndDateComments(endDateComments);
        EasyMock.expect(dataCollectionGroupForwardServiceMock.get("TEST", 1)).andReturn(group);

        final EventDataCollectionGroup event = new EventDataCollectionGroup();
        event.setType(EventType.END_DATA_COLLECTION_PERIOD);
        event.setMajorVersion(1);
        event.setDataCollectionGroupName("TEST");
        final Timestamp notificationTime1 = createTimestampIn(-1, TimeUnit.DAY);
        final Timestamp notificationTime2 = createTimestampIn(2, TimeUnit.DAY);
        event.addNotificationTime(notificationTime1);
        event.addNotificationTime(notificationTime2);
        final EventDataCollectionGroup createdEvent = eventDataCollectionGroupService.create(event);

        final Set<String> emailAddresses = new HashSet<>(Arrays.asList("test1@test.be", "test2@test.be"));
        EasyMock.expect(userDataCollectionServiceMock.getEmailAddresses(new DataCollection("TEST"))).andReturn(emailAddresses);

        final DataCollectionDefinitionContext context = new DataCollectionDefinitionContext();
        context.setDataCollectionName("TEST");
        context.setEndDateCreation(DateFormat.DDMMYYYY.format(endDateCreation));
        context.setEndDateSubmission(DateFormat.DDMMYYYY.format(endDateSubmission));
        context.setEndDateComments(DateFormat.DDMMYYYY.format(endDateComments));
        context.setPeriod("2 weeks");
        context.setInstallationUrl(GUI_HOST);
        mailServiceMock.createAndSendMail(EasyMock.eq(TemplateKey.END_OF_DATA_COLLECTION_PERIOD), EasyMock.eq(context), EasyMock.eq(emailAddresses));
        EasyMock.expectLastCall().once();

        Assert.assertEquals(1, eventDataCollectionGroupService.getAll().size());
        EasyMock.replay(userDataCollectionServiceMock, mailServiceMock, configurationServiceMock, dataCollectionGroupForwardServiceMock);

        dataCollectionGroupNotificationTask.run();

        EasyMock.verify(userDataCollectionServiceMock, mailServiceMock, configurationServiceMock, dataCollectionGroupForwardServiceMock);
        Assert.assertEquals(1, eventDataCollectionGroupService.getAll().size()); // the event is not deleted as there is a remaining reminder in the future

        final EventDataCollectionGroup dbEvent = eventDataCollectionGroupService.get(createdEvent.getId());
        Assert.assertEquals(Collections.singletonList(notificationTime2), dbEvent.getNotificationTimes()); // one reminder remaining
    }

    @Test
    public void testStartDataCollectionEventIsNotSentBecauseStartDateIsNotReached() {
        final Timestamp startDate = createTimestampIn(1, TimeUnit.DAY);
        final Timestamp notificationTime = createTimestampIn(-1, TimeUnit.DAY);

        EasyMock.expect(configurationServiceMock.get(ConfigurationKey.GUI_HOST)).andReturn(guiHostConfig).anyTimes();

        final DataCollectionGroupDto group = new DataCollectionGroupDto();
        group.setName("TEST");
        group.setMajorVersion(1);
        group.setStartDate(startDate);
        EasyMock.expect(dataCollectionGroupForwardServiceMock.get("TEST", 1)).andReturn(group);

        final EventDataCollectionGroup event = new EventDataCollectionGroup();
        event.setType(EventType.START_DATA_COLLECTION_PERIOD);
        event.setMajorVersion(1);
        event.setDataCollectionGroupName("TEST");
        event.addNotificationTime(notificationTime);
        eventDataCollectionGroupService.create(event);

        Assert.assertEquals(1, eventDataCollectionGroupService.getAll().size());
        EasyMock.replay(userDataCollectionServiceMock, mailServiceMock, configurationServiceMock, dataCollectionGroupForwardServiceMock);

        dataCollectionGroupNotificationTask.run();

        EasyMock.verify(userDataCollectionServiceMock, mailServiceMock, configurationServiceMock, dataCollectionGroupForwardServiceMock);
        Assert.assertEquals(0, eventDataCollectionGroupService.getAll().size());
    }

    @Test
    public void testEndDataCollectionEventIsNotSentBecauseEndDateIsInThePast() {
        final Timestamp endDateCreation = createTimestampIn(-1, TimeUnit.DAY);
        final Timestamp endDateSubmission = createTimestampIn(3, TimeUnit.WEEK);
        final Timestamp endDateComments = createTimestampIn(4, TimeUnit.WEEK);
        final Timestamp notificationTime = createTimestampIn(-1, TimeUnit.DAY);

        final DataCollectionGroupDto group = new DataCollectionGroupDto();
        group.setName("TEST");
        group.setMajorVersion(1);
        group.setEndDateCreation(endDateCreation);
        group.setEndDateSubmission(endDateSubmission);
        group.setEndDateComments(endDateComments);
        EasyMock.expect(dataCollectionGroupForwardServiceMock.get("TEST", 1)).andReturn(group);

        final EventDataCollectionGroup event = new EventDataCollectionGroup();
        event.setType(EventType.END_DATA_COLLECTION_PERIOD);
        event.setMajorVersion(1);
        event.setDataCollectionGroupName("TEST");
        event.addNotificationTime(notificationTime);
        eventDataCollectionGroupService.create(event);

        Assert.assertEquals(1, eventDataCollectionGroupService.getAll().size());
        EasyMock.replay(userDataCollectionServiceMock, mailServiceMock, configurationServiceMock, dataCollectionGroupForwardServiceMock);

        dataCollectionGroupNotificationTask.run();

        EasyMock.verify(userDataCollectionServiceMock, mailServiceMock, configurationServiceMock, dataCollectionGroupForwardServiceMock);
        Assert.assertEquals(0, eventDataCollectionGroupService.getAll().size());
    }

    private Timestamp createTimestampIn(int amount, TimeUnit unit) {
        return new Timestamp(DateUtils.add(new Date(), new PeriodExpression(amount, unit)).getTime());
    }
}
