/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import org.junit.Assert;
import org.junit.Test;

import java.io.InputStream;
import java.io.InputStreamReader;

public class CsvServiceTest {

	@Test
	public void testMerge1() throws Exception {
		InputStream inputAsStream = CsvServiceTest.class.getResourceAsStream("csv1.csv");

		StringBuilder sb = new StringBuilder();
		CsvService.merge(sb, new InputStreamReader(inputAsStream, "UTF-8"));
		Assert.assertEquals("Header1;Header2;Header3\r\ncsv1field1;csv1field2;csv1field3\r\n", sb.toString());
		inputAsStream.close();
	}

	@Test
	public void testMerge2() throws Exception {
		InputStream inputAsStream1 = CsvServiceTest.class.getResourceAsStream("csv1.csv");
		InputStream inputAsStream2 = CsvServiceTest.class.getResourceAsStream("csv2.csv");

		StringBuilder sb = new StringBuilder();
		CsvService.merge(sb, new InputStreamReader(inputAsStream1, "UTF-8"), new InputStreamReader(inputAsStream2, "UTF-8"));
		Assert.assertEquals("Header1;Header2;Header3;Header4\r\ncsv1field1;csv1field2;csv1field3;\r\ncsv2field1;;csv2field2;csv2field3\r\n", sb.toString());
		inputAsStream1.close();
		inputAsStream2.close();
	}

	@Test
	public void testMerge3() throws Exception {
		InputStream inputAsStream1 = CsvServiceTest.class.getResourceAsStream("csv1.csv");
		InputStream inputAsStream2 = CsvServiceTest.class.getResourceAsStream("csv2.csv");
		InputStream inputAsStream3 = CsvServiceTest.class.getResourceAsStream("csv3.csv");

		StringBuilder sb = new StringBuilder();
		CsvService.merge(sb, new InputStreamReader(inputAsStream1, "UTF-8"), new InputStreamReader(inputAsStream2, "UTF-8"), new InputStreamReader(inputAsStream3, "UTF-8"));
		Assert.assertEquals(
				"Header1;Header2;Header3;Header4;Header5\r\ncsv1field1;csv1field2;csv1field3;;\r\ncsv2field1;;csv2field2;csv2field3;\r\ncsv3row1field1;;;csv3row1field2;csv3row1field3\r\ncsv3row2field1;;;csv3row2field2;csv3row2field3\r\ncsv3row3field1;;;csv3row2field2;csv3row3field3\r\n",
				sb.toString());
		inputAsStream1.close();
		inputAsStream2.close();
		inputAsStream3.close();
	}

	@Test
	public void testCompare1() throws Exception {
		InputStream inputAsStream1 = CsvServiceTest.class.getResourceAsStream("compare1.csv");
		InputStream inputAsStream2 = CsvServiceTest.class.getResourceAsStream("compare1.csv");

		boolean result = CsvService.compare(new InputStreamReader(inputAsStream1, "UTF-8"), new InputStreamReader(inputAsStream2, "UTF-8"));
		Assert.assertTrue(result);
		inputAsStream1.close();
		inputAsStream2.close();
	}

	@Test
	public void testCompare2() throws Exception {
		InputStream inputAsStream1 = CsvServiceTest.class.getResourceAsStream("compare1.csv");
		InputStream inputAsStream2 = CsvServiceTest.class.getResourceAsStream("compare2.csv");

		boolean result = CsvService.compare(new InputStreamReader(inputAsStream1, "UTF-8"), new InputStreamReader(inputAsStream2, "UTF-8"));
		Assert.assertFalse(result);
		inputAsStream1.close();
		inputAsStream2.close();
	}

	@Test
	public void testCompare3() throws Exception {
		InputStream inputAsStream1 = CsvServiceTest.class.getResourceAsStream("compare1.csv");
		InputStream inputAsStream2 = CsvServiceTest.class.getResourceAsStream("compare3.csv");

		boolean result = CsvService.compare(new InputStreamReader(inputAsStream1, "UTF-8"), new InputStreamReader(inputAsStream2, "UTF-8"));
		Assert.assertFalse(result);
		inputAsStream1.close();
		inputAsStream2.close();
	}

	@Test
	public void testCompare7031() throws Exception {
		InputStream inputAsStream1 = CsvServiceTest.class.getResourceAsStream("7031_CSV.csv");
		InputStream inputAsStream2 = CsvServiceTest.class.getResourceAsStream("7031_MAPPING.csv");

		boolean result = CsvService.compare(new InputStreamReader(inputAsStream1, "UTF-8"), new InputStreamReader(inputAsStream2, "UTF-8"));
		Assert.assertFalse(result);
		inputAsStream1.close();
		inputAsStream2.close();
	}
}
