/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.search.OrganizationSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class OrganizationDaoTest {

	@Autowired
	private IOrganizationDao dao;

	@Test
	public void testGetById() throws Exception {
		final Organization organization = dao.create(buildOrganization("NIHII-HOSPITAL", "11111111", true, false));
		final Organization retrievedOrganization = dao.get(organization.getId());

		Assert.assertNotNull(retrievedOrganization);
		Assert.assertEquals(organization.getId(), retrievedOrganization.getId());
		Assert.assertEquals("NIHII-HOSPITAL", retrievedOrganization.getHealthDataIDType());
		Assert.assertEquals("11111111", retrievedOrganization.getHealthDataIDValue());
		Assert.assertEquals(true, retrievedOrganization.isMain());
	}

	@Test
	public void testGetMain() throws Exception {
		Organization main = dao.getMain();
		Assert.assertNull(main);

		final Organization organization1 = dao.create(buildOrganization("NIHII-HOSPITAL", "11111111", true, false));
		final Organization organization2 = dao.create(buildOrganization("NIHII-HOSPITAL", "11111111", false, false));

		main = dao.getMain();

		Assert.assertNotNull(main);
		Assert.assertEquals(organization1, main);
	}

	@Test
	public void testSearchByIdentificationType() {
		final Organization organization1 = dao.create(buildOrganization("NIHII-HOSPITAL-1", "11111111", true, false));
		final Organization organization2 = dao.create(buildOrganization("NIHII-HOSPITAL-2", "11111111", true, false));

		List<Organization> messages = dao.getAll();
		Assert.assertEquals(2, messages.size());
		Assert.assertTrue(messages.contains(organization1));
		Assert.assertTrue(messages.contains(organization2));

		OrganizationSearch search = new OrganizationSearch();
		search.setHealthDataIDType("NIHII-HOSPITAL-1");
		messages = dao.getAll(search);
		Assert.assertEquals(1, messages.size());
		Assert.assertTrue(messages.contains(organization1));
	}

	@Test
	public void testSearchByIdentificationValue() {
		final Organization organization1 = dao.create(buildOrganization("NIHII-HOSPITAL", "11111111", true, false));
		final Organization organization2 = dao.create(buildOrganization("NIHII-HOSPITAL", "22222222", true, false));

		List<Organization> messages = dao.getAll();
		Assert.assertEquals(2, messages.size());
		Assert.assertTrue(messages.contains(organization1));
		Assert.assertTrue(messages.contains(organization2));

		OrganizationSearch search = new OrganizationSearch();
		search.setHealthDataIDValue("11111111");
		messages = dao.getAll(search);
		Assert.assertEquals(1, messages.size());
		Assert.assertTrue(messages.contains(organization1));
	}

	@Test
	public void testSearchByMainFlag() {
		final Organization organization1 = dao.create(buildOrganization("NIHII-HOSPITAL", "11111111", true, false));
		final Organization organization2 = dao.create(buildOrganization("NIHII-HOSPITAL", "11111111", false, false));

		List<Organization> messages = dao.getAll();
		Assert.assertEquals(2, messages.size());
		Assert.assertTrue(messages.contains(organization1));
		Assert.assertTrue(messages.contains(organization2));

		OrganizationSearch search = new OrganizationSearch();
		search.setMain(true);
		messages = dao.getAll(search);
		Assert.assertEquals(1, messages.size());
		Assert.assertTrue(messages.contains(organization1));

		search = new OrganizationSearch();
		search.setMain(false);
		messages = dao.getAll(search);
		Assert.assertEquals(1, messages.size());
		Assert.assertTrue(messages.contains(organization2));
	}

	@Test
	public void testSearchByDeletedFlag() {
		final Organization organization1 = dao.create(buildOrganization("NIHII-HOSPITAL", "11111111", true, true));
		final Organization organization2 = dao.create(buildOrganization("NIHII-HOSPITAL", "11111111", true, false));

		List<Organization> messages = dao.getAll();
		Assert.assertEquals(1, messages.size());
		Assert.assertTrue(messages.contains(organization2));

		OrganizationSearch search = new OrganizationSearch();
		search.setDeleted(true);
		messages = dao.getAll(search);
		Assert.assertEquals(1, messages.size());
		Assert.assertTrue(messages.contains(organization1));

		search = new OrganizationSearch();
		search.setDeleted(false);
		messages = dao.getAll(search);
		Assert.assertEquals(1, messages.size());
		Assert.assertTrue(messages.contains(organization2));
	}

	@Test
	public void testDelete() {
		final Organization organization1 = dao.create(buildOrganization("NIHII-HOSPITAL", "11111111", false, false));
		final Organization organization2 = dao.create(buildOrganization("NIHII-HOSPITAL", "11111111", true, false));

		List<Organization> messages = dao.getAll();
		Assert.assertEquals(2, messages.size());
		Assert.assertTrue(messages.contains(organization1));
		Assert.assertTrue(messages.contains(organization2));

		dao.delete(organization1); // organization is deleted
		Assert.assertTrue(dao.get(organization1.getId()).isDeleted());

		messages = dao.getAll();
		Assert.assertEquals(1, messages.size());
		Assert.assertTrue(messages.contains(organization2));

		dao.delete(organization2); // main organization is never deleted

		messages = dao.getAll();
		Assert.assertEquals(1, messages.size());
		Assert.assertTrue(messages.contains(organization2));
	}

	private Organization buildOrganization(String identifiactionType, String identificationValue, boolean main, boolean deleted) {
		final Organization organization = new Organization();
		organization.setHealthDataIDType(identifiactionType);
		organization.setHealthDataIDValue(identificationValue);
		organization.setMain(main);
		organization.setDeleted(deleted);
		return organization;
	}
}
