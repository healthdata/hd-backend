/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.util;

import be.wiv_isp.healthdata.common.dto.TemplateDtoV1;
import be.wiv_isp.healthdata.common.domain.enumeration.TemplateKey;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;

public class MailTemplateUpdater {

    private static final Logger LOG = LoggerFactory.getLogger(MailTemplateUpdater.class);

    private static final String ENDPOINT_PATTERN = "http://localhost:8080/healthdata_catalogue/templates/{0}";
    private static final String ACCESS_TOKEN = "";
    private static final String ENV_PREFIX = "TEST: ";

    public static void main(String[] args) throws Exception {
        for (TemplateKey key : TemplateKey.values()) {
            final String subject = getSubject(key);
            final String text = getMergedContent(key);
            createOrUpdate(key, subject, text);
        }
    }

    public static void createOrUpdate(TemplateKey key, String subject, String text) throws Exception {
        LOG.info("Create or update template with key {}", key);
        final String url = MessageFormat.format(ENDPOINT_PATTERN, key);
        WebResource wr = createWebResource(url);
        ClientResponse response = wr.get(ClientResponse.class);

        final TemplateDtoV1 template = new TemplateDtoV1();
        template.setKey(key.toString());
        template.setSubject(subject);
        template.setText(text);

        final WebResource.Builder builder = wr
                .header("Authorization", "Bearer " + ACCESS_TOKEN)
                .accept(MediaType.APPLICATION_JSON)
                .type(MediaType.APPLICATION_JSON);

        if (response.getStatus() == ClientResponse.Status.NOT_FOUND.getStatusCode()) {
            LOG.info("Template with key {} does not exist. Creating template.", key);

            response = builder.post(ClientResponse.class, template);
            Assert.assertEquals(ClientResponse.Status.OK.getStatusCode(), response.getStatus());

            if(ClientResponse.Status.OK.getStatusCode() == response.getStatus()) {
                LOG.info("Template with key {} successfully created", key);
            } else {
                throw new Exception(MessageFormat.format("Template with key {0} could not be created. Response code: {1}", key, response.getStatus()));
            }
        } else if(response.getStatus() == ClientResponse.Status.OK.getStatusCode()) {
            LOG.info("Template was found with key {}. Updating template.", key);

            response = builder.put(ClientResponse.class, template);
            Assert.assertEquals(ClientResponse.Status.OK.getStatusCode(), response.getStatus());

            if(ClientResponse.Status.OK.getStatusCode() == response.getStatus()) {
                LOG.info("Template with key {} successfully updated", key);
            } else {
                throw new Exception(MessageFormat.format("Template with key {0} could not be updated. Response code: {1}", key, response.getStatus()));
            }
        } else {
            throw new Exception("Invalid response code: " + response.getStatus());
        }
    }

    private static String getSubject(TemplateKey key) throws Exception {
        switch (key) {
            case USER_REQUEST_NEW_FOR_USER: return ENV_PREFIX + "An HD4DP-account was requested for you";
            case USER_REQUEST_NEW_FOR_REQUESTER: return ENV_PREFIX + "Confirmation of your request for an HD4DP-account for $userRequest.firstName $userRequest.lastName";
            case USER_REQUEST_NEW_FOR_ADMIN: return ENV_PREFIX + "Request for new HD4DP-account - ACTION NEEDED";
            case USER_REQUEST_UPDATE_FOR_USER: return ENV_PREFIX + "Your account request has been changed by an HD4DP administrator";
            case USER_REQUEST_UPDATE_FOR_REQUESTER: return ENV_PREFIX + "An account that you requested has been changed by an HD4DP administrator";
            case USER_REQUEST_ACCEPT_FOR_USER_DB: return ENV_PREFIX + "Your request for an HD4DP-account has been accepted";
            case USER_REQUEST_ACCEPT_FOR_USER_LDAP: return ENV_PREFIX + "Your request for an HD4DP-account has been accepted";
            case USER_REQUEST_ACCEPT_FOR_REQUESTER: return ENV_PREFIX + "$userRequest.firstName $userRequest.lastName has now access to HD4DP";
            case USER_REQUEST_REJECT_FOR_USER: return ENV_PREFIX + "Your request for an HD4DP-account has been rejected";
            case USER_REQUEST_REJECT_FOR_REQUESTER: return ENV_PREFIX + "$userRequest.firstName $userRequest.lastName has been declined access to HD4DP";
            case UPLOADED_STABLE_DATA: return ENV_PREFIX + "New stable data";
            case REGISTRATION_UPDATES: return ENV_PREFIX + "Registration updates";
            case REGISTRATION_UPDATES_HD4RES: return ENV_PREFIX + "Registration updates";
            case START_OF_DATA_COLLECTION_PERIOD: return ENV_PREFIX + "Start of data collection period";
            case END_OF_DATA_COLLECTION_PERIOD: return ENV_PREFIX + "Data collection period notification";
            case DATA_COLLECTION_DEFINITION_UPDATE: return ENV_PREFIX + "Data collection definition update";
            case NEW_USER_ACCOUNT: return ENV_PREFIX + "HD4DP user account";
            case DATA_COLLECTION_ACCESS_REQUEST_NEW: return ENV_PREFIX + "Data collection access request";
            case DATA_COLLECTION_ACCESS_REQUEST_RESULT: return ENV_PREFIX + "Data collection access request";
            case PASSWORD_RESET: return ENV_PREFIX + "Password reset";
            case REGISTRATION_UPDATES_DMA_REP: return ENV_PREFIX + "Registration updates";
        }

        throw new Exception("Subject is not defined for key " + key);
    }

    public static WebResource createWebResource(final String url) {
        final ClientConfig config = new DefaultClientConfig();
        config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
        final Client client = Client.create(config);

        return client.resource(url);
    }

    private static String getMergedContent(final TemplateKey key) throws IOException {
        LOG.info("Processing includes for key {}", key);

        final String sourcePath = "mail-templates/";
        final String templateFilename = key.toString().toLowerCase() + ".vm";

        String line;
        final StringWriter stringWriter = new StringWriter();
        final BufferedWriter writer = new BufferedWriter(stringWriter);
        final BufferedReader in = new BufferedReader(new InputStreamReader(MailTemplateUpdater.class.getClassLoader().getResourceAsStream(sourcePath + templateFilename)));
        while((line = in.readLine()) != null) {
            final String trimmed = line.trim();
            if(trimmed.startsWith("#include")) {
                final String path = trimmed.replace("#include(\"", StringUtils.EMPTY).replace("\")", StringUtils.EMPTY);
                final String content = IOUtils.toString(MailTemplateUpdater.class.getClassLoader().getResourceAsStream(path), StandardCharsets.UTF_8);
                writer.write(content);
            } else {
                writer.write(line);
            }
            writer.newLine();
        }
        writer.close();
        return stringWriter.toString();
    }
}
