/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.common.domain.search.Field;
import be.wiv_isp.healthdata.common.domain.search.Ordering;
import be.wiv_isp.healthdata.common.domain.enumeration.TimeUnit;
import be.wiv_isp.healthdata.orchestration.dao.IMessageDao;
import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.MessageCorrespondent;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.PeriodExpression;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageType;
import be.wiv_isp.healthdata.orchestration.domain.search.GroupByMessage;
import be.wiv_isp.healthdata.orchestration.domain.search.MessageSearch;
import be.wiv_isp.healthdata.orchestration.util.DateUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
@Transactional
public class MessageDaoTest {

	@Autowired
	private IMessageDao dao;

	@Autowired
	private IOrganizationDao organizationDao;

	private Organization org1;
	private Organization org2;

	@Before
	public void init() {
		org1 = organizationDao.create(buildOrganization("NIHII-HOSPITAL", "11111111", "organization-1"));
		org2 = organizationDao.create(buildOrganization("NIHII-HOSPITAL", "22222222", "organization-2"));
	}

	@Test
	 public void testGetAll() {
		dao.create(buildMessage(MessageStatus.INBOX, "11111111", "EHP", org1, 5, null, null, null, MessageType.ACTION));
		dao.create(buildMessage(MessageStatus.INBOX, "11111111", "EHP", org1, 5, null, null, null, MessageType.ACTION));

		final List<Message> messages = dao.getAll();
		Assert.assertEquals(2, messages.size());
	}

	@Test
	public void testSearchById() {
		final Message m1 = dao.create(buildMessage(MessageStatus.INBOX, "11111111", "EHP", org1, 5, null, null, null, MessageType.ACTION));
		final Message m2 = dao.create(buildMessage(MessageStatus.INBOX, "11111111", "EHP", org1, 5, null, null, null, MessageType.ACTION));

		MessageSearch search = new MessageSearch();
		List<Message> messages = dao.getAll(search);
		Assert.assertEquals(2, messages.size());
		Assert.assertTrue(messages.contains(m1));
		Assert.assertTrue(messages.contains(m2));

		search = new MessageSearch();
		search.setId(m1.getId());
		messages = dao.getAll(search);
		Assert.assertEquals(1, messages.size());
		Assert.assertTrue(messages.contains(m1));
	}

	@Test
	public void testSearchByStatus() {
		final Message m1 = dao.create(buildMessage(MessageStatus.INBOX, "11111111", "EHP", org1, 5, null, null, null, MessageType.ACTION));
		final Message m2 = dao.create(buildMessage(MessageStatus.OUTBOX, "11111111", "EHP", org1, 5, null, null, null, MessageType.ACTION));
		final Message m3 = dao.create(buildMessage(MessageStatus.ERROR_SENDING, "11111111", "EHP", org1, 5, null, null, null, MessageType.ACTION));

		MessageSearch search = new MessageSearch();
		List<Message> messages = dao.getAll(search);
		Assert.assertEquals(3, messages.size());
		Assert.assertTrue(messages.contains(m1));
		Assert.assertTrue(messages.contains(m2));
		Assert.assertTrue(messages.contains(m3));

		search = new MessageSearch();
		search.setStatuses(MessageStatus.INBOX, MessageStatus.OUTBOX);
		messages = dao.getAll(search);
		Assert.assertEquals(2, messages.size());
		Assert.assertTrue(messages.contains(m1));
		Assert.assertTrue(messages.contains(m2));
	}

	@Test
	public void testSearchByOrganization() {
		final Message m1 = dao.create(buildMessage(MessageStatus.INBOX, "11111111", "EHP", org1, 5, null, null, null, MessageType.ACTION));
		final Message m2 = dao.create(buildMessage(MessageStatus.INBOX, "11111111", "EHP", org2, 5, null, null, null, MessageType.ACTION));

		MessageSearch search = new MessageSearch();
		List<Message> messages = dao.getAll(search);
		Assert.assertEquals(2, messages.size());
		Assert.assertTrue(messages.contains(m1));
		Assert.assertTrue(messages.contains(m2));

		search = new MessageSearch();
		search.setOrganization(org1);
		messages = dao.getAll(search);
		Assert.assertEquals(1, messages.size());
		Assert.assertTrue(messages.contains(m1));
	}

	@Test
	public void testSearchByLinkedId() {
		final Message m1 = dao.create(buildMessage(MessageStatus.INBOX, "11111111", "EHP", org1, 5, 1L, null, null, MessageType.ACTION));
		final Message m2 = dao.create(buildMessage(MessageStatus.INBOX, "11111111", "EHP", org1, 5, 2L, null, null, MessageType.ACTION));

		MessageSearch search = new MessageSearch();
		List<Message> messages = dao.getAll(search);
		Assert.assertEquals(2, messages.size());
		Assert.assertTrue(messages.contains(m1));
		Assert.assertTrue(messages.contains(m2));

		search = new MessageSearch();
		search.setLinkedId(1L);
		messages = dao.getAll(search);
		Assert.assertEquals(1, messages.size());
		Assert.assertTrue(messages.contains(m1));
	}

	@Test
	public void testSearchByIdentificationType() {
		final Message m1 = dao.create(buildMessage(MessageStatus.INBOX, "11111111", "EHP-1", org1, 5, null, null, null, MessageType.ACTION));
		final Message m2 = dao.create(buildMessage(MessageStatus.INBOX, "11111111", "EHP-2", org1, 5, null, null, null, MessageType.ACTION));

		MessageSearch search = new MessageSearch();
		List<Message> messages = dao.getAll(search);
		Assert.assertEquals(2, messages.size());
		Assert.assertTrue(messages.contains(m1));
		Assert.assertTrue(messages.contains(m2));

		search = new MessageSearch();
		search.setIdentificationType("EHP-1");
		messages = dao.getAll(search);
		Assert.assertEquals(1, messages.size());
		Assert.assertTrue(messages.contains(m1));
	}

	@Test
	public void testSearchByIdentificationValue() {
		final Message m1 = dao.create(buildMessage(MessageStatus.INBOX, "11111111", "EHP", org1, 5, null, null, null, MessageType.ACTION));
		final Message m2 = dao.create(buildMessage(MessageStatus.INBOX, "22222222", "EHP", org1, 5, null, null, null, MessageType.ACTION));

		MessageSearch search = new MessageSearch();
		List<Message> messages = dao.getAll(search);
		Assert.assertEquals(2, messages.size());
		Assert.assertTrue(messages.contains(m1));
		Assert.assertTrue(messages.contains(m2));

		search = new MessageSearch();
		search.setIdentificationValue("11111111");
		messages = dao.getAll(search);
		Assert.assertEquals(1, messages.size());
		Assert.assertTrue(messages.contains(m1));
	}

	@Test
	public void testSearchByWorkflowId() {
		final Message m1 = dao.create(buildMessage(MessageStatus.INBOX, "11111111", "EHP", org1, 5, null, 1L, null, MessageType.ACTION));
		final Message m2 = dao.create(buildMessage(MessageStatus.INBOX, "11111111", "EHP", org1, 5, null, 2L, null, MessageType.ACTION));

		MessageSearch search = new MessageSearch();
		List<Message> messages = dao.getAll(search);
		Assert.assertEquals(2, messages.size());
		Assert.assertTrue(messages.contains(m1));
		Assert.assertTrue(messages.contains(m2));

		search = new MessageSearch();
		search.setWorkflowId(1L);
		messages = dao.getAll(search);
		Assert.assertEquals(1, messages.size());
		Assert.assertTrue(messages.contains(m1));
	}

	@Test
	public void testSearchByType() {
		final Message m1 = dao.create(buildMessage(MessageStatus.INBOX, "11111111", "EHP", org1, 5, null, 1L, null, MessageType.ACTION));
		final Message m2 = dao.create(buildMessage(MessageStatus.INBOX, "11111111", "EHP", org1, 5, null, 1L, null, MessageType.ACKNOWLEDGMENT));

		MessageSearch search = new MessageSearch();
		List<Message> messages = dao.getAll(search);
		Assert.assertEquals(2, messages.size());
		Assert.assertTrue(messages.contains(m1));
		Assert.assertTrue(messages.contains(m2));

		search = new MessageSearch();
		search.setType(MessageType.ACTION);
		messages = dao.getAll(search);
		Assert.assertEquals(1, messages.size());
		Assert.assertTrue(messages.contains(m1));
	}

	@Test
	public void testSearchByRemainingTrials() {
		final Message m1 = dao.create(buildMessage(MessageStatus.OUTBOX, "11111111", "EHP", org1, 5, null, 1L, null, MessageType.ACTION));
		final Message m2 = dao.create(buildMessage(MessageStatus.OUTBOX, "11111111", "EHP", org1, 0, null, 1L, null, MessageType.ACTION));

		MessageSearch search = new MessageSearch();
		List<Message> messages = dao.getAll(search);
		Assert.assertEquals(2, messages.size());
		Assert.assertTrue(messages.contains(m1));
		Assert.assertTrue(messages.contains(m2));

		search = new MessageSearch();
		search.setHasRemainingSendingTrials(true);
		messages = dao.getAll(search);
		Assert.assertEquals(1, messages.size());
		Assert.assertTrue(messages.contains(m1));

		search = new MessageSearch();
		search.setHasRemainingSendingTrials(false);
		messages = dao.getAll(search);
		Assert.assertEquals(1, messages.size());
		Assert.assertTrue(messages.contains(m2));
	}

	@Test
	public void testSearchBySentBefore() {
		final Date now = new Date();
		final Date twoHoursAgo = DateUtils.remove(now, new PeriodExpression(2, TimeUnit.HOUR));
		final Date fourHoursAgo = DateUtils.remove(now, new PeriodExpression(4, TimeUnit.HOUR));
		final Date heightHoursAgo = DateUtils.remove(now, new PeriodExpression(8, TimeUnit.HOUR));

		final Message m1 = dao.create(buildMessage(MessageStatus.SENT, "11111111", "EHP", org1, 5, null, 1L, heightHoursAgo, MessageType.ACTION));
		final Message m2 = dao.create(buildMessage(MessageStatus.SENT, "11111111", "EHP", org1, 5, null, 1L, twoHoursAgo, MessageType.ACTION));

		MessageSearch search = new MessageSearch();
		List<Message> messages = dao.getAll(search);
		Assert.assertEquals(2, messages.size());
		Assert.assertTrue(messages.contains(m1));
		Assert.assertTrue(messages.contains(m2));

		search = new MessageSearch();
		search.setSentBefore(fourHoursAgo);
		messages = dao.getAll(search);
		Assert.assertEquals(1, messages.size());
		Assert.assertTrue(messages.contains(m1));

		search = new MessageSearch();
		search.setSentBefore(now);
		messages = dao.getAll(search);
		Assert.assertEquals(2, messages.size());
		Assert.assertTrue(messages.contains(m1));
		Assert.assertTrue(messages.contains(m2));
	}

	@Test
	public void testOrderBy() throws Exception {
		final Date now = new Date();

		Message message1 = buildMessage(MessageStatus.INBOX, "11111111", "EHP", org1, 5, 50L, 1L, DateUtils.add(now, new PeriodExpression(1, TimeUnit.HOUR)), MessageType.ACTION);
		Thread.sleep(5);
		Message message2 = buildMessage(MessageStatus.INBOX, "22222222", "EHP", org1, 3, 40L, 1L, DateUtils.add(now, new PeriodExpression(2, TimeUnit.HOUR)), MessageType.ACTION);
		Thread.sleep(5);
		Message message3 = buildMessage(MessageStatus.INBOX, "22222222", "EHP", org1, 0, null, null, DateUtils.add(now, new PeriodExpression(3, TimeUnit.HOUR)), MessageType.ACTION);

		message1 = dao.create(message1);
		message2 = dao.create(message2);
		message3 = dao.create(message3);

		MessageSearch search;
		List<Message> messages;

		// SENT_ON - ASCENDING
		search = new MessageSearch();
		search.setOrdering(new Ordering(Field.Message.SENT_ON, Ordering.Type.ASC));
		messages = dao.getAll(search);
		Assert.assertEquals(message1.getId(), messages.get(0).getId());
		Assert.assertEquals(message2.getId(), messages.get(1).getId());
		Assert.assertEquals(message3.getId(), messages.get(2).getId());

		// SENT_ON - DESCENDING
		search = new MessageSearch();
		search.setOrdering(new Ordering(Field.Message.SENT_ON, Ordering.Type.DESC));
		messages = dao.getAll(search);
		Assert.assertEquals(message3.getId(), messages.get(0).getId());
		Assert.assertEquals(message2.getId(), messages.get(1).getId());
		Assert.assertEquals(message1.getId(), messages.get(2).getId());

		// CREATED_ON - ASCENDING
		search = new MessageSearch();
		search.setOrdering(new Ordering(Field.CREATED_ON, Ordering.Type.ASC));
		messages = dao.getAll(search);
		Assert.assertEquals(message1.getId(), messages.get(0).getId());
		Assert.assertEquals(message2.getId(), messages.get(1).getId());
		Assert.assertEquals(message3.getId(), messages.get(2).getId());

		// CREATED_ON - DESCENDING
		// note: some of these assertions might sporadically fail if two messages, one created immediately after the other,
		// happen to have exactly the same created_on timestamp down to the millisecond
		search = new MessageSearch();
		search.setOrdering(new Ordering(Field.CREATED_ON, Ordering.Type.DESC));
		messages = dao.getAll(search);
		Assert.assertEquals(message3.getId(), messages.get(0).getId());
		Assert.assertEquals(message2.getId(), messages.get(1).getId());
		Assert.assertEquals(message1.getId(), messages.get(2).getId());
	}

	@Test
	public void testGetCountByStatuses() {
		dao.create(buildMessage(MessageStatus.INBOX, "11111111", "EHP", org1, 5, 50L, null, null, MessageType.ACTION));
		dao.create(buildMessage(MessageStatus.INBOX, "11111111", "EHP", org1, 5, 50L, null, null, MessageType.ACTION));
		dao.create(buildMessage(MessageStatus.INBOX, "11111111", "EHP", org1, 5, 50L, null, null, MessageType.ACTION));
		dao.create(buildMessage(MessageStatus.OUTBOX, "11111111", "EHP", org1, 5, 50L, null, null, MessageType.ACTION));
		dao.create(buildMessage(MessageStatus.OUTBOX, "11111111", "EHP", org1, 5, 50L, null, null, MessageType.ACTION));
		dao.create(buildMessage(MessageStatus.SENT, "11111111", "EHP", org1, 5, 50L, null, null, MessageType.ACTION));
		dao.create(buildMessage(MessageStatus.SENT, "11111111", "EHP", org1, 5, 50L, null, null, MessageType.ACTION));
		dao.create(buildMessage(MessageStatus.PROCESSED, "11111111", "EHP", org1, 5, 50L, null, null, MessageType.ACTION));
		dao.create(buildMessage(MessageStatus.UNACKNOWLEDGED, "11111111", "EHP", org1, 5, 50L, null, null, MessageType.ACTION));

		dao.create(buildMessage(MessageStatus.ERROR_PROCESSING, "11111111", "EHP", org2, 5, 50L, null, null, MessageType.ACTION));
		dao.create(buildMessage(MessageStatus.ERROR_PROCESSING, "11111111", "EHP", org2, 5, 50L, null, null, MessageType.ACTION));
		dao.create(buildMessage(MessageStatus.ERROR_SENDING, "11111111", "EHP", org2, 5, 50L, null, null, MessageType.ACTION));
		dao.create(buildMessage(MessageStatus.ERROR_SENDING, "11111111", "EHP", org2, 5, 50L, null, null, MessageType.ACTION));
		dao.create(buildMessage(MessageStatus.ACKNOWLEDGED, "11111111", "EHP", org2, 5, 50L, null, null, MessageType.ACTION));
		dao.create(buildMessage(MessageStatus.SENT, "11111111", "EHP", org2, 5, 50L, null, null, MessageType.ACTION));

		final List<GroupByMessage> countByStatuses1 = dao.getCountByStatuses(org1);
		Assert.assertTrue(countByStatuses1.contains(new GroupByMessage(MessageStatus.INBOX, 3)));
		Assert.assertTrue(countByStatuses1.contains(new GroupByMessage(MessageStatus.OUTBOX, 2)));
		Assert.assertTrue(countByStatuses1.contains(new GroupByMessage(MessageStatus.SENT, 2)));
		Assert.assertTrue(countByStatuses1.contains(new GroupByMessage(MessageStatus.PROCESSED, 1)));
		Assert.assertTrue(countByStatuses1.contains(new GroupByMessage(MessageStatus.UNACKNOWLEDGED, 1)));

		final List<GroupByMessage> countByStatuses2 = dao.getCountByStatuses(org2);
		Assert.assertTrue(countByStatuses2.contains(new GroupByMessage(MessageStatus.ERROR_PROCESSING, 2)));
		Assert.assertTrue(countByStatuses2.contains(new GroupByMessage(MessageStatus.ERROR_SENDING, 2)));
		Assert.assertTrue(countByStatuses2.contains(new GroupByMessage(MessageStatus.ACKNOWLEDGED, 1)));
		Assert.assertTrue(countByStatuses2.contains(new GroupByMessage(MessageStatus.SENT, 1)));
	}

	private Message buildMessage(MessageStatus status, String identificationValue, String identificationType, Organization organization, Integer remainingSendingTrials, Long linkedId, Long workflowId, Date sentOn, MessageType type) {
		final MessageCorrespondent correspondent = new MessageCorrespondent();
		correspondent.setIdentificationType(identificationType);
		correspondent.setIdentificationValue(identificationValue);
		correspondent.setApplicationId("");

		final Message m = new Message();
		m.setContent("content".getBytes());
		m.setStatus(status);
		m.setSentOn(sentOn == null ? new Timestamp(new Date().getTime()) : new Timestamp(sentOn.getTime()));
		m.setCorrespondent(correspondent);
		m.setOrganization(organization);
		m.setRemainingSendingTrials(remainingSendingTrials);
		m.setLinkedId(linkedId);
		m.setWorkflowId(workflowId);
		m.setType(type);
		return m;
	}

	private Organization buildOrganization(String idType, String idValue, String name) {
		final Organization organization = new Organization();
		organization.setHealthDataIDType(idType);
		organization.setHealthDataIDValue(idValue);
		organization.setName(name);
		return organization;
	}
}
