/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.orchestration.dao.ITaskDao;
import be.wiv_isp.healthdata.orchestration.domain.Task;
import be.wiv_isp.healthdata.orchestration.domain.search.TaskSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class TaskDaoTest {

	@Autowired
	private ITaskDao taskDao;

	@Test
 	public void testSearchByName() {
		final Task task1 = createTask();
		final Task created1 = taskDao.create(task1);
		Assert.assertNotNull(created1);

		final Task task2 = createTask();
		task2.setName("MyTask-1");
		final Task created2 = taskDao.create(task2);
		Assert.assertNotNull(created2);

		Assert.assertNotEquals(task1.getName(), task2.getName());

		TaskSearch search = new TaskSearch();
		List<Task> all = taskDao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new TaskSearch();
		search.setName("MyTask");
		all = taskDao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	@Test
	public void testSearchByStatus() {
		final Task task1 = createTask();
		final Task created1 = taskDao.create(task1);
		Assert.assertNotNull(created1);

		final Task task2 = createTask();
		task2.setStatus(Task.Status.FAILURE);
		final Task created2 = taskDao.create(task2);
		Assert.assertNotNull(created2);

		Assert.assertNotEquals(task1.getStatus(), task2.getStatus());

		TaskSearch search = new TaskSearch();
		List<Task> all = taskDao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new TaskSearch();
		search.setStatus(Task.Status.FAILURE);
		all = taskDao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	private Task createTask() {
		final Task task = new Task();
		task.setStatus(Task.Status.SUCCESS);
		task.setName("MyTask");
		task.setExecutedOn(null);
		return task;
	}
}
