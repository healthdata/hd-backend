/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.mapping;

import be.wiv_isp.healthdata.orchestration.domain.IConfiguration;
import be.wiv_isp.healthdata.orchestration.domain.PeriodExpression;
import be.wiv_isp.healthdata.common.domain.enumeration.TimeUnit;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.IConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.mapper.PeriodExpressionConfigurationMapper;
import org.junit.Assert;
import org.junit.Test;

public class PeriodExpressionConfigurationMapperTest {

    @Test
    public void testSecond() {
        IConfiguration configuration = getConfiguration(getKey(), "10s");
        final PeriodExpression periodExpression = PeriodExpressionConfigurationMapper.map(configuration, TimeUnit.SECOND);
        Assert.assertEquals(new PeriodExpression(10, TimeUnit.SECOND), periodExpression);
    }

    @Test
    public void testMinute() {
        IConfiguration configuration = getConfiguration(getKey(), "10m");
        final PeriodExpression periodExpression = PeriodExpressionConfigurationMapper.map(configuration, TimeUnit.MINUTE);
        Assert.assertEquals(new PeriodExpression(10, TimeUnit.MINUTE), periodExpression);
    }

    @Test
    public void testDay() {
        IConfiguration configuration = getConfiguration(getKey(), "10d");
        final PeriodExpression periodExpression = PeriodExpressionConfigurationMapper.map(configuration, TimeUnit.DAY);
        Assert.assertEquals(new PeriodExpression(10, TimeUnit.DAY), periodExpression);
    }

    @Test
    public void testHour() {
        IConfiguration configuration = getConfiguration(getKey(), "10h");
        final PeriodExpression periodExpression = PeriodExpressionConfigurationMapper.map(configuration, TimeUnit.HOUR);
        Assert.assertEquals(new PeriodExpression(10, TimeUnit.HOUR), periodExpression);
    }

    @Test
    public void testWeek() {
        IConfiguration configuration = getConfiguration(getKey(), "10w");
        final PeriodExpression periodExpression = PeriodExpressionConfigurationMapper.map(configuration, TimeUnit.WEEK);
        Assert.assertEquals(new PeriodExpression(10, TimeUnit.WEEK), periodExpression);
    }

    @Test
    public void testMonth() {
        IConfiguration configuration = getConfiguration(getKey(), "10M");
        final PeriodExpression periodExpression = PeriodExpressionConfigurationMapper.map(configuration, TimeUnit.MONTH);
        Assert.assertEquals(new PeriodExpression(10, TimeUnit.MONTH), periodExpression);
    }

    @Test
    public void testYear() {
        IConfiguration configuration = getConfiguration(getKey(), "10y");
        final PeriodExpression periodExpression = PeriodExpressionConfigurationMapper.map(configuration, TimeUnit.YEAR);
        Assert.assertEquals(new PeriodExpression(10, TimeUnit.YEAR), periodExpression);
    }

    @Test
    public void testDefaultValue() {
        IConfiguration configuration = getConfiguration(getKey(), "10");
        final PeriodExpression periodExpression = PeriodExpressionConfigurationMapper.map(configuration, TimeUnit.SECOND);
        Assert.assertEquals(new PeriodExpression(10, TimeUnit.SECOND), periodExpression);
    }

    private IConfiguration getConfiguration(final IConfigurationKey key, final String value) {
        return new IConfiguration() {
                @Override
                public IConfigurationKey getKey() {
                    return key;
                }

                @Override
                public String getValue() {
                    return value;
                }
            };
    }

    public IConfigurationKey getKey() {
        return new IConfigurationKey() {
            @Override
            public String getType() {
                return null;
            }
        };
    }
}
