/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.PeriodExpression;
import be.wiv_isp.healthdata.orchestration.util.DateUtils;
import be.wiv_isp.healthdata.common.domain.enumeration.TimeUnit;
import be.wiv_isp.healthdata.orchestration.dao.IPasswordResetTokenDao;
import be.wiv_isp.healthdata.orchestration.dao.IUserDao;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.PasswordResetToken;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.search.PasswordResetTokenSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

@Transactional
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class PasswordResetTokenDaoTest {

	private User user;
	private Organization organization;

	@Autowired
	private IPasswordResetTokenDao dao;

	@Autowired
	private IUserDao userDao;

	@Autowired
	private IOrganizationDao organizationDao;

	@Test
 	public void testSearchByToken() {
		final PasswordResetToken passwordResetToken1 = buildPasswordResetToken();
		final PasswordResetToken created1 = dao.create(passwordResetToken1);
		Assert.assertNotNull(created1);

		final PasswordResetToken passwordResetToken2 = buildPasswordResetToken();
		passwordResetToken2.setToken("token2");
		final PasswordResetToken created2 = dao.create(passwordResetToken2);
		Assert.assertNotNull(created2);

		Assert.assertNotEquals(created1.getToken(), created2.getToken());

		PasswordResetTokenSearch search = new PasswordResetTokenSearch();
		List<PasswordResetToken> all = dao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new PasswordResetTokenSearch();
		search.setToken("token1");
		all = dao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	@Test
	public void testSearchByExpiryDate() {
		final Timestamp now = new Timestamp(new Date().getTime());

		final PasswordResetToken passwordResetToken1 = buildPasswordResetToken();
		final PasswordResetToken created1 = dao.create(passwordResetToken1);
		Assert.assertNotNull(created1);

		final PasswordResetToken passwordResetToken2 = buildPasswordResetToken();
		passwordResetToken2.setExpiryDate(new Timestamp(DateUtils.add(now, new PeriodExpression(-5, TimeUnit.MINUTE)).getTime()));
		final PasswordResetToken created2 = dao.create(passwordResetToken2);
		Assert.assertNotNull(created2);

		Assert.assertNotEquals(created1.getExpiryDate(), created2.getExpiryDate());
		Assert.assertTrue(created1.getExpiryDate().after(now));
		Assert.assertTrue(created2.getExpiryDate().before(now));

		PasswordResetTokenSearch search = new PasswordResetTokenSearch();
		List<PasswordResetToken> all = dao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new PasswordResetTokenSearch();
		search.setExpired(false);
		all = dao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));

		search = new PasswordResetTokenSearch();
		search.setExpired(true);
		all = dao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created2));

	}

	private PasswordResetToken buildPasswordResetToken() {
		final PasswordResetToken passwordResetToken = new PasswordResetToken();
		passwordResetToken.setToken("token1");
		passwordResetToken.setUser(getUser());
		passwordResetToken.setExpiryDate(new Timestamp(DateUtils.add(new Date(), new PeriodExpression(5, TimeUnit.MINUTE)).getTime()));
		return passwordResetToken;
	}

	private User getUser() {
		if (user == null) {
			final User u = new User();
			u.setUsername("user");
			u.setLastName("doe");
			u.setFirstName("john");
			u.setEmail("john.doe@mail.com");
			u.setLdapUser(false);
			u.setEnabled(true);
			u.setAuthorities(new HashSet<>(Collections.singletonList(new Authority(Authority.USER))));
			u.setDataCollectionNames(new HashSet<>(Collections.singletonList("TEST")));
			u.setOrganization(getOrganization());
			user = userDao.create(u);
		}
		return user;
	}

	private Organization getOrganization() {
		if (organization == null) {
			final Organization o = new Organization();
			o.setHealthDataIDType("RIZIV");
			o.setHealthDataIDValue("11111111");
			o.setName("MyOrganization1");
			o.setMain(true);
			o.setDeleted(false);
			organization = organizationDao.create(o);
		}
		return organization;
	}
}
