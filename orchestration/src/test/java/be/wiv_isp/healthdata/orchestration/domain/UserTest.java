/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class UserTest {

	private static final String METADATA_KEY = "METADATA_KEY";

	@Test
	public void testEqualsPositiveScenario() {
		final String USERNAME = "USERNAME";
		final String PASSWORD = "PASSWORD";
		final boolean ENABLED = true;
		final Authority AUTHORITY = new Authority();

		final User user1 = new User();
		user1.setUsername(USERNAME);
		user1.setPassword(PASSWORD);
		user1.setEnabled(ENABLED);
		user1.setAuthorities(new HashSet<>(Collections.singletonList(AUTHORITY)));

		final User user2 = new User();
		user2.setUsername(USERNAME);
		user2.setPassword(PASSWORD);
		user2.setEnabled(ENABLED);
		user2.setAuthorities(new HashSet<>(Collections.singletonList(AUTHORITY)));

		Assert.assertEquals(user1, user2);
	}

	@Test
	public void testEqualsNegativeScenario() {
		final String USERNAME = "USERNAME";
		final String PASSWORD = "PASSWORD";
		final boolean ENABLED = true;
		final Authority AUTHORITY = new Authority();

		final User user1 = new User();
		user1.setUsername(USERNAME);
		user1.setPassword(PASSWORD);
		user1.setEnabled(ENABLED);
		user1.setAuthorities(new HashSet<>(Collections.singletonList(AUTHORITY)));

		final User user2 = new User();
		user2.setUsername("DIFFERENT_USERNAME");
		user2.setPassword(PASSWORD);
		user2.setEnabled(ENABLED);
		user2.setAuthorities(new HashSet<>(Collections.singletonList(AUTHORITY)));

		Assert.assertNotEquals(user1, user2);
	}

	@Test
	public void testEqualsNegativeScenarioDifferentObjectType() {
		final String USERNAME = "USERNAME";
		final String PASSWORD = "PASSWORD";
		final boolean ENABLED = true;
		final Authority AUTHORITY = new Authority();

		final User user = new User();
		user.setUsername(USERNAME);
		user.setPassword(PASSWORD);
		user.setEnabled(ENABLED);
		user.setAuthorities(new HashSet<>(Collections.singletonList(AUTHORITY)));

		Assert.assertNotEquals(user, new String());
	}

	@Test
	public void testHashCodeComparisonPositive() {
		final String USERNAME = "USERNAME";
		final String PASSWORD = "PASSWORD";
		final boolean ENABLED = true;
		final Authority AUTHORITY = new Authority();

		final User user1 = new User();
		user1.setUsername(USERNAME);
		user1.setPassword(PASSWORD);
		user1.setEnabled(ENABLED);
		user1.setAuthorities(new HashSet<>(Collections.singletonList(AUTHORITY)));
		user1.setMetaData(createMap(METADATA_KEY, "TRUE"));

		final User user2 = new User();
		user2.setUsername(USERNAME);
		user2.setPassword(PASSWORD);
		user2.setEnabled(ENABLED);
		user2.setAuthorities(new HashSet<>(Collections.singletonList(AUTHORITY)));
		user2.setMetaData(createMap(METADATA_KEY, "TRUE"));

		Assert.assertEquals(user1.hashCode(), user2.hashCode());
	}

	private Map<String, String> createMap(String key, String value) {
		final Map<String, String> map = new HashMap<>();
		map.put(key, value);
		return map;
	}

	@Test
	public void testHashCodeComparisonNegative() {
		final String USERNAME = "USERNAME";
		final String PASSWORD = "PASSWORD";
		final boolean ENABLED = true;
		final Authority AUTHORITY = new Authority();

		final User user1 = new User();
		user1.setUsername(USERNAME);
		user1.setPassword(PASSWORD);
		user1.setEnabled(ENABLED);
		user1.setAuthorities(new HashSet<>(Collections.singletonList(AUTHORITY)));
		user1.setMetaData(createMap(METADATA_KEY, "TRUE"));

		final User user2 = new User();
		user2.setUsername("DIFFERENT_USERNAME");
		user2.setPassword(PASSWORD);
		user2.setEnabled(ENABLED);
		user2.setAuthorities(new HashSet<>(Collections.singletonList(AUTHORITY)));
		user2.setMetaData(createMap(METADATA_KEY, "TRUE"));

		Assert.assertNotEquals(user1.hashCode(), user2.hashCode());
	}
}
