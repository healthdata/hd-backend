/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.dao.IConfigurationDao;
import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.search.ConfigurationSearch;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import be.wiv_isp.healthdata.orchestration.service.IPlatformService;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class ConfigurationServiceTest {

	// service to test
	private final IConfigurationService configurationService = new ConfigurationService();

	@Autowired
	private IConfigurationDao configurationDao;

	@Autowired
	private IOrganizationDao organizationDao;

	@Autowired
	private CacheManager cacheManager;

	// mocks
	private IPlatformService platformService = EasyMock.createMock(IPlatformService.class);
	private IOrganizationService organizationService = EasyMock.createMock(IOrganizationService.class);

	@Before
	public void before() {
		ReflectionTestUtils.setField(configurationService, "dao", configurationDao);
		ReflectionTestUtils.setField(configurationService, "platformService", platformService);
		ReflectionTestUtils.setField(configurationService, "organizationService", organizationService);
		ReflectionTestUtils.setField(configurationService, "cacheManager", cacheManager);
	}

	@Test
	public void testGetConfiguration() {
		final Organization organization = organizationDao.create(new Organization());
		final Configuration created = configurationDao.create(buildConfiguration(ConfigurationKey.CATALOGUE_HOST, "value", organization, false));

		final Configuration dbConfiguration = configurationService.get(ConfigurationKey.CATALOGUE_HOST, organization);
		Assert.assertEquals(created, dbConfiguration);
	}

	@Test
	public void testGetHealthDataIDType() {
		final ConfigurationKey key = ConfigurationKey.HEALTHDATA_ID_TYPE;

		final Organization organization = organizationDao.create(buildOrganization("RIZIV", "11111111", true));

		final Configuration expectedConfiguration = new Configuration();
		expectedConfiguration.setId(0L);
		expectedConfiguration.setOrganization(organization);
		expectedConfiguration.setKey(key);
		expectedConfiguration.setValue(organization.getHealthDataIDType());
		expectedConfiguration.setDefaultValue(organization.getHealthDataIDType());

		final Configuration configuration = configurationService.get(key, organization);
		Assert.assertEquals(expectedConfiguration, configuration);
	}

	@Test
	public void testGetHealthDataIDValue() {
		final ConfigurationKey key = ConfigurationKey.HEALTHDATA_ID_VALUE;

		final Organization organization = organizationDao.create(buildOrganization("RIZIV", "11111111", true));

		final Configuration expectedConfiguration = new Configuration();
		expectedConfiguration.setId(0L);
		expectedConfiguration.setOrganization(organization);
		expectedConfiguration.setKey(key);
		expectedConfiguration.setValue(organization.getHealthDataIDValue());
		expectedConfiguration.setDefaultValue(organization.getHealthDataIDValue());

		final Configuration configuration = configurationService.get(key, organization);
		Assert.assertEquals(expectedConfiguration, configuration);
	}

	@Test
	public void testGetWithUseFromMainTrue() {
		final ConfigurationKey key = ConfigurationKey.NATIONAL_REGISTER_CONNECTOR;

		final Organization mainOrganization = organizationDao.create(buildOrganization("RIZIV", "11111111", true));
		final Organization subOrganization = organizationDao.create(buildOrganization("RIZIV", "22222222", false));

		final Configuration subConfiguration = configurationDao.create(buildConfiguration(key, "subValue", subOrganization, true));
		final Configuration mainConfiguration = configurationDao.create(buildConfiguration(key, "mainValue", mainOrganization, false));

		EasyMock.expect(organizationService.getMain()).andReturn(mainOrganization).anyTimes();
		EasyMock.replay(organizationService);

		final Configuration configuration = configurationService.get(key, subOrganization);

		EasyMock.verify(organizationService);
		Assert.assertEquals(mainConfiguration, configuration);
	}

	@Test
	public void testUpdate() {
		final Organization organization = organizationDao.create(buildOrganization(null, null, true));
		final Configuration configuration = configurationDao.create(buildConfiguration(ConfigurationKey.EM_INTERFACE_TYPE, "value", organization, false));

		configuration.setValue("updatedValue");

		final Configuration updated = configurationService.update(configuration);

		Assert.assertEquals("updatedValue", updated.getValue());
		Assert.assertEquals("updatedValue", configurationService.get(ConfigurationKey.EM_INTERFACE_TYPE, organization).getValue());
	}

	@Test
	public void testUpdateHealthDataIdValue() {
		organizationService = new OrganizationService();
		ReflectionTestUtils.setField(organizationService, "organizationDao", organizationDao);
		ReflectionTestUtils.setField(organizationService, "cacheManager", cacheManager);
		ReflectionTestUtils.setField(configurationService, "organizationService", organizationService);

		final Organization organization = organizationDao.create(buildOrganization(null, null, true));
		final Configuration configuration = buildConfiguration(ConfigurationKey.HEALTHDATA_ID_VALUE, "11111111", organization, false);
		final Configuration updated = configurationService.update(configuration);

		Assert.assertEquals("11111111", updated.getValue());
		Assert.assertEquals("11111111", configuration.getOrganization().getHealthDataIDValue());
		Assert.assertEquals("11111111", organizationDao.get(organization.getId()).getHealthDataIDValue());
		Assert.assertEquals("11111111", configurationService.get(ConfigurationKey.HEALTHDATA_ID_VALUE, organization).getValue());
	}

	@Test
	public void testUpdateHealthDataIdType() {
		organizationService = new OrganizationService();
		ReflectionTestUtils.setField(organizationService, "organizationDao", organizationDao);
		ReflectionTestUtils.setField(organizationService, "cacheManager", cacheManager);
		ReflectionTestUtils.setField(configurationService, "organizationService", organizationService);

		final Organization organization = organizationDao.create(buildOrganization(null, null, true));
		final Configuration configuration = buildConfiguration(ConfigurationKey.HEALTHDATA_ID_TYPE, "RIZIV", organization, false);
		final Configuration updated = configurationService.update(configuration);

		Assert.assertEquals("RIZIV", updated.getValue());
		Assert.assertEquals("RIZIV", configuration.getOrganization().getHealthDataIDType());
		Assert.assertEquals("RIZIV", organizationDao.get(organization.getId()).getHealthDataIDType());
		Assert.assertEquals("RIZIV", configurationService.get(ConfigurationKey.HEALTHDATA_ID_TYPE, organization).getValue());
	}

	@Test
	public void testGetDefaultWithResult() {
		Assert.assertTrue(configurationDao.getAll().isEmpty());
		final Configuration configuration = configurationService.get(ConfigurationKey.SCHEDULED_TASK_MESSAGING_SERVICE_INTERVAL);
		Assert.assertEquals("3600", configuration.getValue());
	}

	@Test
	public void testGetDefaultWithNoResultThrowsException() {
		Assert.assertTrue(configurationDao.getAll().isEmpty());
		try {
			configurationService.get(ConfigurationKey.SCHEDULED_TASK_ELASTIC_SEARCH_INDEXING_INTERVAL);
			Assert.fail();
		} catch (HealthDataException e) {

		}
	}

	@Test
	public void testGetWithUseFromMainFalse() {
		final ConfigurationKey key = ConfigurationKey.NATIONAL_REGISTER_CONNECTOR;

		final Organization mainOrganization = organizationDao.create(buildOrganization("RIZIV", "11111111", true));
		final Organization subOrganization = organizationDao.create(buildOrganization("RIZIV", "22222222", false));

		final Configuration subConfiguration = configurationDao.create(buildConfiguration(key, "subValue", subOrganization, false));
		final Configuration mainConfiguration = configurationDao.create(buildConfiguration(key, "mainValue", mainOrganization, false));

		final Configuration configuration = configurationService.get(key, subOrganization);
		Assert.assertEquals(subConfiguration, configuration);
	}

	@Test
	public void testCreateMissing() {
		final Organization mainOrganization = organizationDao.create(buildOrganization("RIZIV", "11111111", true));
		final Organization subOrganization1 = organizationDao.create(buildOrganization("RIZIV", "22222222", false));
		final Organization subOrganization2 = organizationDao.create(buildOrganization("RIZIV", "33333333", false));
		final List<Organization> allOrganizations = Arrays.asList(mainOrganization, subOrganization1, subOrganization2);

		EasyMock.expect(platformService.getCurrentPlatform()).andReturn(Platform.HD4DP).anyTimes();
		EasyMock.expect(organizationService.getMain()).andReturn(mainOrganization);
		EasyMock.expect(organizationService.getAll()).andReturn(allOrganizations);

		EasyMock.replay(platformService, organizationService);

		int organizationDependentConfigurations = getConfigurationKeyCount(true);
		int organizationIndependentConfigurations = getConfigurationKeyCount(false);

		Assert.assertTrue(configurationDao.getAll().isEmpty());
		configurationService.createMissing();
		Assert.assertFalse(configurationDao.getAll().isEmpty());
		Assert.assertEquals(3 * organizationDependentConfigurations + organizationIndependentConfigurations, configurationDao.getAll().size());

		EasyMock.verify(platformService, organizationService);
	}

	@Test
	public void testCreateMissingWithExistingMainConfigurations() {
		final Organization mainOrganization = organizationDao.create(buildOrganization("RIZIV", "11111111", true));
		final Organization subOrganization1 = organizationDao.create(buildOrganization("RIZIV", "22222222", false));
		final Organization subOrganization2 = organizationDao.create(buildOrganization("RIZIV", "33333333", false));
		final List<Organization> allOrganizations = Arrays.asList(mainOrganization, subOrganization1, subOrganization2);

		configurationDao.create(buildConfiguration(ConfigurationKey.EM_INTERFACE_TYPE, "mainValue", mainOrganization, false));

		EasyMock.expect(platformService.getCurrentPlatform()).andReturn(Platform.HD4DP).anyTimes();
		EasyMock.expect(organizationService.getMain()).andReturn(mainOrganization);
		EasyMock.expect(organizationService.getAll()).andReturn(allOrganizations);

		EasyMock.replay(platformService, organizationService);

		int organizationDependentConfigurations = getConfigurationKeyCount(true);
		int organizationIndependentConfigurations = getConfigurationKeyCount(false);

		Assert.assertEquals(1, configurationDao.getAll().size());
		configurationService.createMissing();
		Assert.assertFalse(configurationDao.getAll().isEmpty());
		Assert.assertEquals(3 * organizationDependentConfigurations + organizationIndependentConfigurations, configurationDao.getAll().size());
		Assert.assertEquals("mainValue", configurationDao.getUnique(buildSearch(ConfigurationKey.EM_INTERFACE_TYPE, mainOrganization)).getValue());
		Assert.assertEquals("mainValue", configurationDao.getUnique(buildSearch(ConfigurationKey.EM_INTERFACE_TYPE, subOrganization1)).getValue());
		Assert.assertEquals("mainValue", configurationDao.getUnique(buildSearch(ConfigurationKey.EM_INTERFACE_TYPE, subOrganization2)).getValue());

		EasyMock.verify(platformService, organizationService);
	}

	@Test
	public void testUnattachedOrganizationDependentConfiguration() {
		final Organization mainOrganization = organizationDao.create(buildOrganization("RIZIV", "11111111", true));
		final Organization subOrganization1 = organizationDao.create(buildOrganization("RIZIV", "22222222", false));
		final Organization subOrganization2 = organizationDao.create(buildOrganization("RIZIV", "33333333", false));
		final List<Organization> allOrganizations = Arrays.asList(mainOrganization, subOrganization1, subOrganization2);

		configurationDao.create(buildConfiguration(ConfigurationKey.EM_INTERFACE_TYPE, "mainValue", null, false));

		EasyMock.expect(platformService.getCurrentPlatform()).andReturn(Platform.HD4DP).anyTimes();
		EasyMock.expect(organizationService.getMain()).andReturn(mainOrganization);
		EasyMock.expect(organizationService.getAll()).andReturn(allOrganizations);

		EasyMock.replay(platformService, organizationService);

		int organizationDependentConfigurations = getConfigurationKeyCount(true);
		int organizationIndependentConfigurations = getConfigurationKeyCount(false);

		Assert.assertEquals(1, configurationDao.getAll().size());
		configurationService.createMissing();
		configurationService.deleteUnattachedOrganizationConfigurations();
		Assert.assertFalse(configurationDao.getAll().isEmpty());
		Assert.assertEquals(3 * organizationDependentConfigurations + organizationIndependentConfigurations, configurationDao.getAll().size());
		Assert.assertEquals("mainValue", configurationDao.getUnique(buildSearch(ConfigurationKey.EM_INTERFACE_TYPE, mainOrganization)).getValue());
		Assert.assertEquals("mainValue", configurationDao.getUnique(buildSearch(ConfigurationKey.EM_INTERFACE_TYPE, subOrganization1)).getValue());
		Assert.assertEquals("mainValue", configurationDao.getUnique(buildSearch(ConfigurationKey.EM_INTERFACE_TYPE, subOrganization2)).getValue());

		EasyMock.verify(platformService, organizationService);
	}

	@Test
	public void testDeleteInapplicable() {
		final Organization mainOrganization = organizationDao.create(buildOrganization("RIZIV", "11111111", true));
		final Organization subOrganization1 = organizationDao.create(buildOrganization("RIZIV", "22222222", false));

		// applicable on HD4DP
		configurationDao.create(buildConfiguration(ConfigurationKey.EM_INTERFACE_TYPE, "mainValue1", mainOrganization, false));
		configurationDao.create(buildConfiguration(ConfigurationKey.EM_INTERFACE_TYPE, "subValue1", subOrganization1, false));

		// non-applicable on HD4DP (must be removed)
		configurationDao.create(buildConfiguration(ConfigurationKey.SALESFORCE_URL, "mainValue2", mainOrganization, false));
		configurationDao.create(buildConfiguration(ConfigurationKey.SALESFORCE_URL, "subValue2", subOrganization1, false));

		EasyMock.expect(platformService.getCurrentPlatform()).andReturn(Platform.HD4DP).anyTimes();

		EasyMock.replay(platformService);

		Assert.assertEquals(4, configurationDao.getAll().size());
		Assert.assertNotNull(configurationDao.getUnique(buildSearch(ConfigurationKey.SALESFORCE_URL, mainOrganization)));
		Assert.assertNotNull(configurationDao.getUnique(buildSearch(ConfigurationKey.SALESFORCE_URL, subOrganization1)));

		configurationService.deleteInapplicable();

		Assert.assertEquals(2, configurationDao.getAll().size());
		Assert.assertEquals("mainValue1", configurationDao.getUnique(buildSearch(ConfigurationKey.EM_INTERFACE_TYPE, mainOrganization)).getValue());
		Assert.assertEquals("subValue1", configurationDao.getUnique(buildSearch(ConfigurationKey.EM_INTERFACE_TYPE, subOrganization1)).getValue());
		Assert.assertNull(configurationDao.getUnique(buildSearch(ConfigurationKey.SALESFORCE_URL, mainOrganization)));
		Assert.assertNull(configurationDao.getUnique(buildSearch(ConfigurationKey.SALESFORCE_URL, subOrganization1)));

		EasyMock.verify(platformService);
	}

	@Test
	public void testIncludeInvisible() {
		final Organization mainOrganization = organizationDao.create(buildOrganization("RIZIV", "11111111", true));

		final Configuration visibleConfiguration = configurationDao.create(buildConfiguration(ConfigurationKey.EM_INTERFACE_TYPE, "value1", mainOrganization, false));
		final Configuration invisibleConfiguration = configurationDao.create(buildConfiguration(ConfigurationKey.DAILY_TASK_EXECUTION_TIME, "value2", mainOrganization, false));

		Assert.assertTrue(ConfigurationKey.EM_INTERFACE_TYPE.isVisible());
		Assert.assertFalse(ConfigurationKey.DAILY_TASK_EXECUTION_TIME.isVisible());

		ReflectionTestUtils.setField(configurationService, "showInvisible", false);

		ConfigurationSearch search = new ConfigurationSearch();
		List<Configuration> configurations = configurationService.getAll(search);
		Assert.assertEquals(1, configurations.size());
		Assert.assertTrue(configurations.contains(visibleConfiguration));

		search = new ConfigurationSearch();
		search.setIncludeInvisible(false);
		configurations = configurationService.getAll(search);
		Assert.assertEquals(1, configurations.size());
		Assert.assertTrue(configurations.contains(visibleConfiguration));

		search = new ConfigurationSearch();
		search.setIncludeInvisible(true);
		configurations = configurationService.getAll(search);
		Assert.assertEquals(2, configurations.size());
		Assert.assertTrue(configurations.contains(visibleConfiguration));
		Assert.assertTrue(configurations.contains(invisibleConfiguration));

		ReflectionTestUtils.setField(configurationService, "showInvisible", true);

		search = new ConfigurationSearch();
		configurations = configurationService.getAll(search);
		Assert.assertEquals(2, configurations.size());
		Assert.assertTrue(configurations.contains(visibleConfiguration));
		Assert.assertTrue(configurations.contains(invisibleConfiguration));

		search = new ConfigurationSearch();
		search.setIncludeInvisible(false);
		configurations = configurationService.getAll(search);
		Assert.assertEquals(2, configurations.size());
		Assert.assertTrue(configurations.contains(visibleConfiguration));
		Assert.assertTrue(configurations.contains(invisibleConfiguration));

		search = new ConfigurationSearch();
		search.setIncludeInvisible(true);
		configurations = configurationService.getAll(search);
		Assert.assertEquals(2, configurations.size());
		Assert.assertTrue(configurations.contains(visibleConfiguration));
		Assert.assertTrue(configurations.contains(invisibleConfiguration));
	}

	@Test
	public void testMigrateEmInterfaceType() {
		ReflectionTestUtils.setField(configurationService, "showInvisible", true);

		EasyMock.expect(platformService.getCurrentPlatform()).andReturn(Platform.HD4DP).anyTimes();
		EasyMock.replay(platformService);

		final Organization mainOrganization = organizationDao.create(buildOrganization("RIZIV", "11111111", true));
		final Organization subOrganization = organizationDao.create(buildOrganization("RIZIV", "22222222", false));

		configurationDao.create(buildConfiguration(ConfigurationKey.EM_INTERFACE_OUT, "value1", mainOrganization, false));
		configurationDao.create(buildConfiguration(ConfigurationKey.EM_INTERFACE_OUT, "value2", subOrganization, false));

		configurationDao.create(buildConfiguration(ConfigurationKey.EM_INTERFACE_TYPE, "", mainOrganization, false));
		configurationDao.create(buildConfiguration(ConfigurationKey.EM_INTERFACE_TYPE, "", subOrganization, false));

		Assert.assertEquals(4, configurationDao.getAll().size());
		Assert.assertEquals("", configurationService.get(ConfigurationKey.EM_INTERFACE_TYPE, mainOrganization).getValue());
		Assert.assertEquals("", configurationService.get(ConfigurationKey.EM_INTERFACE_TYPE, subOrganization).getValue());

		configurationService.migrateEmInterfaceType();

		Assert.assertEquals(4, configurationDao.getAll().size());
		Assert.assertEquals("value1", configurationService.get(ConfigurationKey.EM_INTERFACE_TYPE, mainOrganization).getValue());
		Assert.assertEquals("value2", configurationService.get(ConfigurationKey.EM_INTERFACE_TYPE, subOrganization).getValue());

		EasyMock.verify(platformService);
	}

	private ConfigurationSearch buildSearch(ConfigurationKey key, Organization organization) {
		final ConfigurationSearch search = new ConfigurationSearch();
		search.setKey(key);
		search.setOrganization(organization);
		return search;
	}

	private Configuration buildConfiguration(ConfigurationKey key, String value, Organization organization, boolean useFromMain) {
		final Configuration configuration = new Configuration();
		configuration.setKey(key);
		configuration.setValue(value);
		configuration.setOrganization(organization);
		configuration.setUseFromMain(useFromMain);
		return configuration;
	}

	private Organization buildOrganization(String type, String value, boolean isMain) {
		final Organization organization = new Organization();
		organization.setHealthDataIDType(type);
		organization.setHealthDataIDValue(value);
		organization.setMain(isMain);
		return organization;
	}

	private int getConfigurationKeyCount(boolean isOrganizationConfig) {
		int count = 0;
		for (ConfigurationKey key : ConfigurationKey.values()) {
			if(!key.isHd4dp()) {
				continue;
			}
			if(ConfigurationKey.HEALTHDATA_ID_TYPE.equals(key) || ConfigurationKey.HEALTHDATA_ID_VALUE.equals(key)) {
				continue;
			}
			if (isOrganizationConfig == key.isOrganizationConfig()) {
				count++;
			}
		}
		return count;
	}
}
