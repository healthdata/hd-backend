/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import org.junit.Assert;
import org.junit.Test;

public class AuthorityTest {

    @Test
      public void testEqualsPositiveScenario() {
        final String AUTHORITY = "ROLE_USER";

        final Authority auth1 = new Authority(AUTHORITY);
        final Authority auth2 = new Authority(AUTHORITY);

        Assert.assertEquals(auth1, auth2);
    }

    @Test
    public void testEqualsNegativeScenario() {
        final String AUTHORITY = "ROLE_USER";

        final Authority auth1 = new Authority(AUTHORITY);
        final Authority auth2 = new Authority("DIFFERENT_AUTHORITY");

        Assert.assertNotEquals(auth1, auth2);
    }

    @Test
    public void testEqualsNegativeScenarioDifferentObjectType() {
        final String AUTHORITY = "ROLE_USER";

        final Authority auth1 = new Authority(AUTHORITY);

        Assert.assertNotEquals(auth1, new String());
    }

    @Test
    public void testToString() {
        final String AUTHORITY = "ROLE_USER";

        final Authority auth = new Authority(AUTHORITY);

        String expectedToStringResult = "Authority {" + "authority = " + AUTHORITY + "}";

        Assert.assertEquals(expectedToStringResult, auth.toString());
    }
}
