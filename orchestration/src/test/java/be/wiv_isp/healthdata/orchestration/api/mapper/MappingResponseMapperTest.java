/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api.mapper;

import be.wiv_isp.healthdata.orchestration.domain.Progress;
import be.wiv_isp.healthdata.orchestration.mapping.MappingResponse;
import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class MappingResponseMapperTest {

	@Test
	public void testMapper1() throws Exception {
		String request = IOUtils.toString(WorkflowIdMapper.class.getResourceAsStream("mappingResponseMapperTest1.json"));
		JSONObject json = new JSONObject(request);

		Progress progress = new Progress();
		progress.setValid(9);
		progress.setInvalid(5);
		progress.setOptional(31);
		progress.setTotal(45);
		progress.setValidRatio(0.2);
		progress.setInvalidRatio(0.1111111111111111);
		progress.setOptionalRatio(0.6888888888888889);
		progress.setErrors(5);
		progress.setWarnings(3);

		Map<String, String> metaData = new HashMap<>();
		metaData.put("sex", "{}");
		metaData.put("PATIENT_IDENTIFIER", "TEST_PATIENTID_0000-0001");

		MappingResponse expected = new MappingResponse();
		expected.setDocumentContent(new JSONObject("{\"value\":{\"favourite_color\":\"Blue\",\"patient_id\":{\"%method\":\"other\",\"%generated\":false}}}"));
		expected.setProgress(progress);
		expected.setMetaData(metaData);

		MappingResponse convert = MappingResponseMapper.convert(json);
		Assert.assertEquals(expected, convert);
	}

	@Test
	public void testMapper2() throws Exception {
		String request = IOUtils.toString(WorkflowIdMapper.class.getResourceAsStream("mappingResponseMapperTest2.json"));
		JSONObject json = new JSONObject(request);

		Progress progress = new Progress();
		progress.setValid(26);
		progress.setInvalid(1);
		progress.setOptional(1);
		progress.setTotal(28);
		progress.setValidRatio(0.9285714285714286);
		progress.setInvalidRatio(0.03571428571428571);
		progress.setOptionalRatio(0.03571428571428571);
		progress.setErrors(1);
		progress.setWarnings(0);

		MappingResponse expected = new MappingResponse();
		expected.setDocumentContent(new JSONObject("{\"value\":{\"nature_of_the_sample\":{\"selectedOption\":\"confiscation\"},\"case_id\":\"Peter1\",\"unique_sample_id\":\"Peter2\",\"receival_date\":\"2015-12-12T00:00:00+01:00\",\"requester_of_the_analysis\":{\"judicial_district\":{\"code\":\"DE\"}},\"contact_information_of_the_requester_of_the_analysis\":{\"name\":\"rechter appelflap\",\"phone_number\":\"ihi\"},\"city_of_origin_of_the_sample\":{\"code\":\"1081\"},\"sample_forwarded_by_another_laboratory_for_extended_analysis\":{\"selectedOption\":\"yes\",\"origin\":\"AML\"},\"form\":{},\"sample_nature\":{\"selectedOption\":\"cap\",\"shape\":{}},\"color\":\"wit\",\"total_weight_of_seizure__shipment__tablets\":1,\"weight_unit_total\":{\"selectedOption\":\"kg\"},\"test_sample_weight\":150,\"weight_unit_test_sample\":{\"selectedOption\":\"mg\"},\"sample_substances\":[{\"substance\":{\"selectedOption\":\"drugs_new_psychoactive_substance\"},\"analysis_date\":\"2015-12-12T00:00:00+01:00\",\"type_of_id_of_the_active_substance\":{\"selectedOption\":\"C\"},\"id_of_the_active_substance\":\"MDMA\",\"concentration\":25,\"concentration_unit\":{\"selectedOption\":\"%\"},\"analysis_method\":{\"selectedOptions\":{\"enzymatic\":false,\"immunoassay\":false,\"gcfid\":true,\"gcnpd\":false,\"gcms\":false,\"gcmsms\":false,\"gcqtof\":false,\"lcms\":false,\"lcmsms\":false,\"lsqtof\":false,\"ftir\":false,\"hplcuv\":false,\"hplcuvdad\":false,\"hplc\":false,\"hplcfluo\":false,\"other\":false}},\"additional_information_on_the_analysis_method\":\"geen info\",\"derivatisation_for_analysis\":{\"selectedOption\":\"yes\",\"specify_method_of_derivatisation\":\"acetyl\"}}],\"comments_to_the_bewsd_regarding_this_sample\":\"dag Peter!\"}}"));
		expected.setProgress(progress);

		MappingResponse convert = MappingResponseMapper.convert(json);
		Assert.assertEquals(expected, convert);
	}

	@Test
	public void testMapper3() throws Exception {
		JSONObject json = new JSONObject("{}");

		Progress progress = new Progress();

		MappingResponse expected = new MappingResponse();
		expected.setProgress(progress);

		MappingResponse convert = MappingResponseMapper.convert(json);
		Assert.assertEquals(expected, convert);
	}

}
