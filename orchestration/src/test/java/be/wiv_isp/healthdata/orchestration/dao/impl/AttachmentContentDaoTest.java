/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.orchestration.dao.IAttachmentContentDao;
import be.wiv_isp.healthdata.orchestration.domain.AttachmentContent;
import be.wiv_isp.healthdata.orchestration.domain.search.AttachmentContentSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class AttachmentContentDaoTest {

    public static final String HASH1 = "1111111111";
    public static final String HASH2 = "2222222222";

    @Autowired
    private IAttachmentContentDao dao;

    @Test
    public void testSearchByHash() {
        final AttachmentContent attachmentContent1 = createAttachmentContent();
        final AttachmentContent created1 = dao.create(attachmentContent1);
        Assert.assertNotNull(created1);

        final AttachmentContent attachmentContent2 = createAttachmentContent();
        attachmentContent2.setHash(HASH2);
        final AttachmentContent created2 = dao.create(attachmentContent2);
        Assert.assertNotNull(created2);

        Assert.assertNotEquals(created1.getHash(), created2.getHash());

        AttachmentContentSearch search = new AttachmentContentSearch();
        List<AttachmentContent> all = dao.getAll(search);
        Assert.assertEquals(2, all.size());
        Assert.assertTrue(all.contains(created1));
        Assert.assertTrue(all.contains(created2));

        search = new AttachmentContentSearch();
        search.setHash(HASH1);
        all = dao.getAll(search);
        Assert.assertEquals(1, all.size());
        Assert.assertTrue(all.contains(created1));
    }

    private AttachmentContent createAttachmentContent() {
        final byte[] bytes = "content".getBytes();
        final AttachmentContent content = new AttachmentContent();
        content.setContent(bytes);
        content.setSize(bytes.length);
        content.setHash(HASH1);
        return content;
    }
}
