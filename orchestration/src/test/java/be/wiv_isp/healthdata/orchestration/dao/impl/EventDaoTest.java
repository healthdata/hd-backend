/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.dao.IEventDao;
import be.wiv_isp.healthdata.orchestration.dao.IUserDao;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.Event;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.EventType;
import be.wiv_isp.healthdata.orchestration.domain.search.EventSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@Transactional
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class EventDaoTest {

	public static final EventType TYPE1 = EventType.START_DATA_COLLECTION_PERIOD;
	public static final EventType TYPE2 = EventType.END_DATA_COLLECTION_PERIOD;
	public static final EventType TYPE3 = EventType.WORKFLOW_STATUS_UPDATE;

	private User user1;
	private User user2;
	private Organization organization;

	@Autowired
	private IEventDao eventDao;

	@Autowired
	private IUserDao userDao;

	@Autowired
	private IOrganizationDao organizationDao;

	@Test
 	public void testSearchByType() {
		final Event event1 = createEvent();
		event1.setType(TYPE1);
		final Event created1 = eventDao.create(event1);
		Assert.assertNotNull(created1);

		final Event event2 = createEvent();
		event2.setType(TYPE2);
		final Event created2 = eventDao.create(event2);
		Assert.assertNotNull(created2);

		Assert.assertNotEquals(created1.getType(), created2.getType());

		EventSearch search = new EventSearch();
		List<Event> all = eventDao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new EventSearch();
		search.setTypes(TYPE1);
		all = eventDao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	@Test
	public void testSearchByTypes() {
		final Event event1 = createEvent();
		event1.setType(TYPE1);
		final Event created1 = eventDao.create(event1);
		Assert.assertNotNull(created1);

		final Event event2 = createEvent();
		event2.setType(TYPE2);
		final Event created2 = eventDao.create(event2);
		Assert.assertNotNull(created2);

		final Event event3 = createEvent();
		event3.setType(TYPE3);
		final Event created3 = eventDao.create(event3);
		Assert.assertNotNull(created3);

		Assert.assertNotEquals(created1.getType(), created2.getType());
		Assert.assertNotEquals(created1.getType(), created3.getType());
		Assert.assertNotEquals(created2.getType(), created3.getType());

		EventSearch search = new EventSearch();
		List<Event> all = eventDao.getAll(search);
		Assert.assertEquals(3, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));
		Assert.assertTrue(all.contains(created3));

		search = new EventSearch();
		search.setTypes(TYPE1, TYPE2);
		all = eventDao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));
	}

	@Test
	public void testSearchByNotificationTime() {

		// Search will be done by retrieving all events with a notification time before 10/09/2016.
		// Event1 and event2 have 15/09/2016 as notification time. 05/09/2016 is added to event1, and not to event2.
		// Hence, event1 must be returned by the query, and not event2.

		final Event event1 = createEvent();
		event1.addNotificationTime(createTimestamp(2016, Calendar.SEPTEMBER, 5));
		final Event created1 = eventDao.create(event1);
		Assert.assertNotNull(created1);

		final Event event2 = createEvent();
		final Event created2 = eventDao.create(event2);
		Assert.assertNotNull(created2);

		Assert.assertNotEquals(created1.getNotificationTimes(), created2.getNotificationTimes());

		EventSearch search = new EventSearch();
		List<Event> all = eventDao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new EventSearch();
		search.setWithNotificationTimeBefore(createTimestamp(2016, Calendar.SEPTEMBER, 10));
		all = eventDao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	@Test
	public void testSearchByUser() {
		final Event event1 = createEvent();
		final Event created1 = eventDao.create(event1);
		Assert.assertNotNull(created1);

		final Event event2 = createEvent();
		event2.getUsers().clear();
		event2.getUsers().add(getUser(2));
		final Event created2 = eventDao.create(event2);
		Assert.assertNotNull(created2);

		Assert.assertNotEquals(created1.getUsers(), created2.getUsers());

		EventSearch search = new EventSearch();
		List<Event> all = eventDao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new EventSearch();
		search.setUser(getUser());
		all = eventDao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	private Event createEvent() {
		final Event event = new Event();
		event.setType(TYPE1);
		event.getUsers().add(getUser());
		event.addNotificationTime(createTimestamp(2016, Calendar.SEPTEMBER, 15));
		return event;
	}

	private User getUser() {
		return getUser(1);
	}

	private User getUser(int id) {
		if(id == 1) {
			if (user1 == null) {
				final User u = new User();
				u.setUsername("user1");
				u.setLastName("doe");
				u.setFirstName("john");
				u.setEmail("john.doe@mail.com");
				u.setLdapUser(false);
				u.setEnabled(true);
				u.setAuthorities(new HashSet<>(Collections.singletonList(new Authority(Authority.USER))));
				u.setDataCollectionNames(new HashSet<>(Collections.singletonList("TEST")));
				u.setOrganization(getOrganization());
				user1 = userDao.create(u);
			}
			return user1;
		}
		if(id == 2) {
			if (user2 == null) {
				final User u = new User();
				u.setUsername("user2");
				u.setLastName("doe");
				u.setFirstName("john");
				u.setEmail("john.doe@mail.com");
				u.setLdapUser(false);
				u.setEnabled(true);
				u.setAuthorities(new HashSet<>(Collections.singletonList(new Authority(Authority.USER))));
				u.setDataCollectionNames(new HashSet<>(Collections.singletonList("TEST")));
				u.setOrganization(getOrganization());
				user2 = userDao.create(u);
			}
			return user2;
		}
		return null;
	}

	private Organization getOrganization() {
		if (organization == null) {
			final Organization o = new Organization();
			o.setHealthDataIDType("RIZIV");
			o.setHealthDataIDValue("11111111");
			o.setName("MyOrganization");
			o.setMain(true);
			o.setDeleted(false);
			organization = organizationDao.create(o);
		}
		return organization;
	}

	private Timestamp createTimestamp(int year, int month, int day) {
		final Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(year, month, day, 12, 0, 0);
		return new Timestamp(calendar.getTimeInMillis());
	}
}
