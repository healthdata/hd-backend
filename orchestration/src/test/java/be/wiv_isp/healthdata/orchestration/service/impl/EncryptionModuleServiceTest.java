/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.service.impl;


import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.Identification;
import be.wiv_isp.healthdata.orchestration.domain.MessageCorrespondent;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.dto.EncryptionModuleMessage;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IEmVersionService;
import be.wiv_isp.healthdata.orchestration.service.impl.mock.EncryptionModuleServiceMock;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class EncryptionModuleServiceTest {

    private EncryptionModuleServiceMock encryptionModuleService = new EncryptionModuleServiceMock();
    private IConfigurationService configurationService;
    private IEmVersionService emVersionService;

    private Identification encryptionAddressee;
    private Identification addressee;
    private MessageCorrespondent messageCorrespondent;

    @Before
    public void init() {
        configurationService = EasyMock.createNiceMock(IConfigurationService.class);
        emVersionService = EasyMock.createNiceMock(IEmVersionService.class);
        ReflectionTestUtils.setField(encryptionModuleService, "configurationService", configurationService);
        ReflectionTestUtils.setField(encryptionModuleService, "emVersionService", emVersionService);

        encryptionAddressee = new Identification();
        encryptionAddressee.setType("NIHII-HOSPITAL");
        encryptionAddressee.setValue("12345678");
        encryptionAddressee.setApplicationId("");
        encryptionAddressee.setQuality("");

        addressee = new Identification();
        addressee.setType("CBE");
        addressee.setValue("0809394427");
        addressee.setApplicationId("TTP");
        addressee.setQuality("INSTITUTION");

        messageCorrespondent = new MessageCorrespondent();
        messageCorrespondent.setIdentificationType("NIHII-HOSPITAL");
        messageCorrespondent.setIdentificationValue("12345678");
        messageCorrespondent.setApplicationId("");
    }


    @Test
    public void testQueueOutgoing() {
        Configuration queueSizeConfig = new Configuration();
        queueSizeConfig.setKey(ConfigurationKey.MESSAGING_QUEUE_SIZE);
        queueSizeConfig.setValue("3");
        EasyMock.expect(configurationService.get(ConfigurationKey.MESSAGING_QUEUE_SIZE)).andReturn(queueSizeConfig).times(25);
        EasyMock.expect(emVersionService.supportsMultiLine(messageCorrespondent)).andReturn(Boolean.TRUE).times(25);
        EasyMock.replay(configurationService);
        EasyMock.replay(emVersionService);

        Organization organization = new Organization();
        organization.setId(1L);
        organization.setHealthDataIDType("NIHII-HOSPITAL");
        organization.setHealthDataIDValue("11111111");

        for (int i=0; i<10; i++) {
            final EncryptionModuleMessage encryptionModuleMessage = new EncryptionModuleMessage();
            encryptionModuleMessage.setSubject("[Batch_codage_encode]");
            encryptionModuleMessage.setAddressee(addressee);
            encryptionModuleMessage.setEncryptionAddressee(encryptionAddressee);
            encryptionModuleMessage.setContent("content_"+i);
            encryptionModuleMessage.addMetadata("", "");

            encryptionModuleService.queueOutgoing(encryptionModuleMessage, organization);
        }
        encryptionModuleService.flushAll();

        List<String> expectedOutgoing = new ArrayList<>();
        expectedOutgoing.add("content_0\r\ncontent_1\r\ncontent_2");
        expectedOutgoing.add("content_3\r\ncontent_4\r\ncontent_5");
        expectedOutgoing.add("content_6\r\ncontent_7\r\ncontent_8");
        expectedOutgoing.add("content_9");
        Assert.assertEquals(expectedOutgoing, EncryptionModuleServiceMock.outgoing);

    }

    @Test
    public void testReceive() {

        final String[] msgs = { "321;321;dummy;{\"document\":\"content1\"}",
                "654;654;dummy;{\"document\":\"content2\"}",
                "432;432;dummy;{\"document\":\"content3\"}",
                "543;543;dummy;{\"document\":\"content4\"}",
                "123;123;{\"document\":\"content5\"}",
                "456;456;{\"document\":\"content6\"}",
                "789;789;{\"document\":\"content7\"}"
        };

        EncryptionModuleServiceMock.incoming = new ArrayList<>(Arrays.asList(
                msgs[0] + "\r\n" + msgs[1],
                msgs[2] + "\r\n" + msgs[3],
                msgs[4] + "\r\n" + msgs[5] + "\r\n" + msgs[6] + "\r\n"
        ));

        List<EncryptionModuleMessage> incomingMessages;

        incomingMessages = encryptionModuleService.receive(new Organization());
        Assert.assertEquals(4, incomingMessages.size());
        for (int i = 0; i < 4; i++)
            Assert.assertEquals(msgs[i], incomingMessages.get(i).getContent());

        incomingMessages = encryptionModuleService.receive(new Organization());
        Assert.assertEquals(3, incomingMessages.size());
        for (int i = 0; i < 3; i++)
            Assert.assertEquals(msgs[i+4], incomingMessages.get(i).getContent());

    }

}
