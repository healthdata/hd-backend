/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.domain;

import org.junit.Assert;
import org.junit.Test;

import java.nio.charset.StandardCharsets;

public class CSVContentTest {

    @Test
    public void testConstruct() {
        final String patientId = "1234567890";
        final String workflowId = "738";
        final String payload = "{\"document\":{\"favourite_color\":\"red\"}}";

        final String expectedCSV0 = "1234567890;738;eyJkb2N1bWVudCI6eyJmYXZvdXJpdGVfY29sb3IiOiJyZWQifX0=";
        final String expectedCSV1 = "738;1234567890;eyJkb2N1bWVudCI6eyJmYXZvdXJpdGVfY29sb3IiOiJyZWQifSwiRklFTERfREVTQ1JJUFRPUiI6W3sibmFtZSI6IldPUktGTE9XX0lEIiwidHlwZSI6IlRFQ0hOSUNBTF9NRVRBREFUQSJ9LHsibmFtZSI6IlBBVElFTlRfSURFTlRJRklFUiIsInR5cGUiOiJwYXRpZW50SUQifV19";
        final String expectedCSV2 = "738;1234567890;9876543210;eyJkb2N1bWVudCI6eyJmYXZvdXJpdGVfY29sb3IiOiJyZWQifSwiRklFTERfREVTQ1JJUFRPUiI6W3sibmFtZSI6IldPUktGTE9XX0lEIiwidHlwZSI6IlRFQ0hOSUNBTF9NRVRBREFUQSJ9LHsibmFtZSI6IlBBVElFTlRfSURFTlRJRklFUiIsInR5cGUiOiJwYXRpZW50SUQifSx7Im5hbWUiOiJwYXRpZW50X2lkX29mX21vdGhlciIsInR5cGUiOiJwYXRpZW50SUQifV19";
        final String expectedCSV3 = "738;1234567890;9876543210;DUMMY_FIELD_0;eyJkb2N1bWVudCI6eyJmYXZvdXJpdGVfY29sb3IiOiJyZWQifSwiRklFTERfREVTQ1JJUFRPUiI6W3sibmFtZSI6IldPUktGTE9XX0lEIiwidHlwZSI6IlRFQ0hOSUNBTF9NRVRBREFUQSJ9LHsibmFtZSI6IlBBVElFTlRfSURFTlRJRklFUiIsInR5cGUiOiJwYXRpZW50SUQifSx7Im5hbWUiOiJwYXRpZW50X2lkX29mX21vdGhlciIsInR5cGUiOiJwYXRpZW50SUQifSx7Im5hbWUiOiJEVU1NWV9GSUVMRF8wIiwidHlwZSI6IkRVTU1ZX0ZJRUxEIn1dfQ==";

        CSVContent csvContent = new CSVContent();
        csvContent.setUseDescriptors(false);
        csvContent.putPatientId(patientId);
        csvContent.putWorkflowId(workflowId);
        csvContent.setLastField(payload);

        Assert.assertEquals(3, csvContent.getSize());
        Assert.assertEquals(expectedCSV0, csvContent.toString());
        csvContent.putField(2, "other");
        csvContent.putField("someKey", "someValue", "someType");
        Assert.assertEquals(3, csvContent.getSize());

        csvContent.setUseDescriptors(true);
        csvContent.putField(2, "other");
        csvContent.putField("someKey", "someValue", "someType");
        Assert.assertEquals(5, csvContent.getSize());

        csvContent = new CSVContent();
        csvContent.setLastField(payload);
        csvContent.toString();
        csvContent.putWorkflowId(workflowId);
        csvContent.toString();
        csvContent.putPatientId(patientId);
        Assert.assertEquals(expectedCSV1, csvContent.toString());

        csvContent.putField("patient_id_of_mother", "9876543210", "patientID");
        Assert.assertEquals(expectedCSV2, csvContent.toString());

        csvContent.addDummyField();
        Assert.assertEquals(expectedCSV3, csvContent.toString());

        Assert.assertEquals(payload, new String(csvContent.getLastField(), StandardCharsets.UTF_8));

    }

    @Test
    public void testRead() {
        final String csv0 = "1234567890;738;eyJkb2N1bWVudCI6eyJmYXZvdXJpdGVfY29sb3IiOiJyZWQifX0=";
        final String csv1 = "738;1234567890;eyJkb2N1bWVudCI6eyJmYXZvdXJpdGVfY29sb3IiOiJyZWQifSwiRklFTERfREVTQ1JJUFRPUiI6W3sibmFtZSI6IldPUktGTE9XX0lEIiwidHlwZSI6IlRFQ0hOSUNBTF9NRVRBREFUQSJ9LHsibmFtZSI6IlBBVElFTlRfSURFTlRJRklFUiIsInR5cGUiOiJwYXRpZW50SUQifV19";
        final String csv2 = "738;1234567890;9876543210;DUMMY_FIELD_0;eyJkb2N1bWVudCI6eyJmYXZvdXJpdGVfY29sb3IiOiJyZWQifSwiRklFTERfREVTQ1JJUFRPUiI6W3sibmFtZSI6IldPUktGTE9XX0lEIiwidHlwZSI6IlRFQ0hOSUNBTF9NRVRBREFUQSJ9LHsibmFtZSI6IlBBVElFTlRfSURFTlRJRklFUiIsInR5cGUiOiJwYXRpZW50SUQifSx7Im5hbWUiOiJwYXRpZW50X2lkX29mX21vdGhlciIsInR5cGUiOiJwYXRpZW50SUQifSx7Im5hbWUiOiJEVU1NWV9GSUVMRF8wIiwidHlwZSI6IkRVTU1ZX0ZJRUxEIn1dfQ==";

        final String expectedPatientID = "1234567890";
        final String expectedWorkflowID = "738";
        final String expectedMotherID = "9876543210";
        final String expectedDummyField = "DUMMY_FIELD_0";
        final String expectedPayload = "{\"document\":{\"favourite_color\":\"red\"}}";

        CSVContent csvContent0 = new CSVContent(csv0);

        Assert.assertFalse(csvContent0.isUseDescriptors());
        Assert.assertEquals(expectedWorkflowID, csvContent0.getWorkflowId());
        Assert.assertEquals(expectedPatientID, csvContent0.getPatientId());
        Assert.assertEquals(expectedPayload, new String(csvContent0.getLastField(), StandardCharsets.UTF_8));

        CSVContent csvContent1 = new CSVContent(csv1);

        Assert.assertTrue(csvContent1.isUseDescriptors());
        Assert.assertEquals(expectedWorkflowID, csvContent1.getWorkflowId());
        Assert.assertEquals(expectedPatientID, csvContent1.getPatientId());
        Assert.assertEquals(expectedPayload, new String(csvContent1.getLastField(), StandardCharsets.UTF_8));

        CSVContent csvContent2 = new CSVContent(csv2);

        Assert.assertTrue(csvContent2.isUseDescriptors());
        Assert.assertEquals(expectedWorkflowID, csvContent2.getWorkflowId());
        Assert.assertTrue(csvContent2.isTechnicalField(0));
        Assert.assertEquals(expectedPatientID, csvContent2.getPatientId());
        Assert.assertTrue(csvContent2.isPatientIdField(1));
        Assert.assertEquals(expectedMotherID, csvContent2.getField("patient_id_of_mother"));
        Assert.assertTrue(csvContent2.isPatientIdField(2));
        Assert.assertEquals(expectedDummyField, csvContent2.getField("DUMMY_FIELD_0"));
        Assert.assertTrue(csvContent2.isDummyField(3));
        Assert.assertEquals(expectedPayload, new String(csvContent2.getLastField(), StandardCharsets.UTF_8));

    }

}
