/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl.mock;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.Version;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionDefinitionForwardService;

import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DataCollectionDefinitionForwardServiceMock implements IDataCollectionDefinitionForwardService {

	private final DataCollectionDefinitionDtoV7 dcd = new DataCollectionDefinitionDtoV7();

	{
		dcd.setContent("{}".getBytes(StandardCharsets.UTF_8));
		dcd.setId(1L);
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.YEAR, 2014);
		cal.set(Calendar.MONTH, 1 - 1);
		cal.set(Calendar.DATE, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		new Timestamp(cal.getTimeInMillis());
		dcd.setDataCollectionName("TEST");
	}

	@Override
	public DataCollectionDefinitionDtoV7 get(Long dataCollectionDefinitionId) {
		if (1L == dataCollectionDefinitionId) {
			dcd.setId(dataCollectionDefinitionId);
			return dcd;
		}
		return null;
	}

	@Override
	public String getDataCollectionName(Long dataCollectionDefinitionId) {
		return "TEST";
	}

	@Override
	public DataCollectionDefinitionDtoV7 get(String dataCollectionName, Version version) {
		return null;
	}

	@Override
	public DataCollectionDefinitionDtoV7 get(Long dataCollectionDefinitionId, Organization organization) {
		if (1L == dataCollectionDefinitionId) {
			dcd.setId(dataCollectionDefinitionId);
			return dcd;
		}
		return null;
	}

	@Override
	public DataCollectionDefinitionDtoV7 getValidForCreation(String dataCollectionName, Organization organization) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DataCollectionDefinitionDtoV7 getLatest(String dataCollectionName, Organization organization) {
		// TODO Auto-generated method stub
		return null;
	}
}
