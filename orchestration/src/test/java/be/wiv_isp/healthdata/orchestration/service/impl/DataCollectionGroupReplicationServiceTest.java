/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.domain.enumeration.TimeUnit;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupListDto;
import be.wiv_isp.healthdata.orchestration.dao.IDataCollectionGroupReplicationDao;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.DataCollectionGroupReplication;
import be.wiv_isp.healthdata.orchestration.domain.EventDataCollectionGroup;
import be.wiv_isp.healthdata.orchestration.domain.PeriodExpression;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.EventType;
import be.wiv_isp.healthdata.orchestration.domain.search.DataCollectionGroupReplicationSearch;
import be.wiv_isp.healthdata.orchestration.domain.search.EventDataCollectionGroupSearch;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionGroupReplicationService;
import be.wiv_isp.healthdata.orchestration.service.IEventDataCollectionGroupService;
import be.wiv_isp.healthdata.orchestration.util.DateUtils;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class DataCollectionGroupReplicationServiceTest {

    private IDataCollectionGroupReplicationService dataCollectionGroupReplicationService = new DataCollectionGroupReplicationService();

    @Autowired
    private IDataCollectionGroupReplicationDao dao;

    @Autowired
    private IEventDataCollectionGroupService eventDataCollectionGroupService;

    private IConfigurationService configurationServiceMock = EasyMock.createNiceMock(IConfigurationService.class);

    @Before
    public void before() {
        ReflectionTestUtils.setField(dataCollectionGroupReplicationService, "dao", dao);
        ReflectionTestUtils.setField(dataCollectionGroupReplicationService, "eventDataCollectionGroupService", eventDataCollectionGroupService);
        ReflectionTestUtils.setField(dataCollectionGroupReplicationService, "configurationService", configurationServiceMock);

        // verify database is cleared
        Assert.assertEquals(0, dataCollectionGroupReplicationService.getAll().size());
        Assert.assertEquals(0, eventDataCollectionGroupService.getAll().size());
    }

    @Test
    public void testDcdVersionAndEventsAreCreatedThenUpdatedWhenDatesAreModified() {
        final Configuration endDataCollectionPeriodReminders = buildConfiguration("1d,2d");
        EasyMock.expect(configurationServiceMock.get(ConfigurationKey.END_DATA_COLLECTION_PERIOD_REMINDERS)).andReturn(endDataCollectionPeriodReminders).anyTimes();
        EasyMock.replay(configurationServiceMock);

        // set of dates
        final Timestamp startDate = createTimestampIn(1, TimeUnit.MONTH);
        final Timestamp endDateCreation = createTimestampIn(2, TimeUnit.MONTH);

        // another set of dates
        final Timestamp startDate2 = createTimestampIn(2, TimeUnit.MONTH);
        final Timestamp endDateCreation2 = createTimestampIn(3, TimeUnit.MONTH);

        // response from the catalogue (mock)
        DataCollectionGroupListDto group = new DataCollectionGroupListDto();
        group.setDataCollectionGroupName("TEST");
        group.setMajorVersion(1);
        group.setStartDate(startDate);
        group.setEndDateCreation(endDateCreation);

        Assert.assertEquals(0, dataCollectionGroupReplicationService.getAll().size());
        Assert.assertEquals(0, eventDataCollectionGroupService.getAll().size());

        dataCollectionGroupReplicationService.process(group);

        Assert.assertEquals(1, dataCollectionGroupReplicationService.getAll().size());
        Assert.assertEquals(2, eventDataCollectionGroupService.getAll().size());

        verifyEventStartDataCollectionIsCreatedOrUpdated("TEST", 1, startDate);
        verifyEventEndDataCollectionIsCreatedOrUpdated("TEST", 1, endDateCreation);
        verifyDataCollectionGroupRepHasBeenCreatedOrUpdated("TEST", 1, startDate, endDateCreation);

        // simulate start date is modified on the catalogue
        group.setStartDate(startDate2);

        dataCollectionGroupReplicationService.process(group);

        Assert.assertEquals(1, dataCollectionGroupReplicationService.getAll().size());
        Assert.assertEquals(2, eventDataCollectionGroupService.getAll().size());

        verifyEventStartDataCollectionIsCreatedOrUpdated("TEST", 1, startDate2);
        verifyEventEndDataCollectionIsCreatedOrUpdated("TEST", 1, endDateCreation);
        verifyDataCollectionGroupRepHasBeenCreatedOrUpdated("TEST", 1, startDate2, endDateCreation);

        // simulate end date is modified on the catalogue
        group.setEndDateCreation(endDateCreation2);

        dataCollectionGroupReplicationService.process(group);

        Assert.assertEquals(1, dataCollectionGroupReplicationService.getAll().size());
        Assert.assertEquals(2, eventDataCollectionGroupService.getAll().size());

        verifyEventStartDataCollectionIsCreatedOrUpdated("TEST", 1, startDate2);
        verifyEventEndDataCollectionIsCreatedOrUpdated("TEST", 1, endDateCreation2);
        verifyDataCollectionGroupRepHasBeenCreatedOrUpdated("TEST", 1, startDate2, endDateCreation2);

        EasyMock.verify(configurationServiceMock);
    }

    @Test
    public void testDcdVersionAndEventsAreCreatedThenNewItemsAreCreatedWhenNewMajorVersionIsAvailable() {
        final Configuration endDataCollectionPeriodReminders = buildConfiguration("1d,2d");
        EasyMock.expect(configurationServiceMock.get(ConfigurationKey.END_DATA_COLLECTION_PERIOD_REMINDERS)).andReturn(endDataCollectionPeriodReminders).anyTimes();
        EasyMock.replay(configurationServiceMock);

        // set of dates
        final Timestamp startDate = createTimestampIn(1, TimeUnit.MONTH);
        final Timestamp endDateCreation = createTimestampIn(2, TimeUnit.MONTH);

        // another set of dates
        final Timestamp startDate2 = createTimestampIn(2, TimeUnit.MONTH);
        final Timestamp endDateCreation2 = createTimestampIn(3, TimeUnit.MONTH);

        // response from the catalogue (mock)
        DataCollectionGroupListDto group = new DataCollectionGroupListDto();
        group.setDataCollectionGroupName("TEST");
        group.setMajorVersion(1);
        group.setStartDate(startDate);
        group.setEndDateCreation(endDateCreation);

        Assert.assertEquals(0, dataCollectionGroupReplicationService.getAll().size());
        Assert.assertEquals(0, eventDataCollectionGroupService.getAll().size());

        dataCollectionGroupReplicationService.process(group);

        Assert.assertEquals(1, dataCollectionGroupReplicationService.getAll().size());
        Assert.assertEquals(2, eventDataCollectionGroupService.getAll().size());

        verifyEventStartDataCollectionIsCreatedOrUpdated("TEST", 1, startDate);
        verifyEventEndDataCollectionIsCreatedOrUpdated("TEST", 1, endDateCreation);
        verifyDataCollectionGroupRepHasBeenCreatedOrUpdated("TEST", 1, startDate, endDateCreation);

        // modify version (simulate major minor version on the catalogue)
        group.setMajorVersion(2);
        group.setStartDate(startDate2);
        group.setEndDateCreation(endDateCreation2);

        dataCollectionGroupReplicationService.process(group);

        Assert.assertEquals(2, dataCollectionGroupReplicationService.getAll().size());
        Assert.assertEquals(4, eventDataCollectionGroupService.getAll().size());

        verifyEventStartDataCollectionIsCreatedOrUpdated("TEST", 1, startDate);
        verifyEventEndDataCollectionIsCreatedOrUpdated("TEST", 1, endDateCreation);
        verifyDataCollectionGroupRepHasBeenCreatedOrUpdated("TEST", 1, startDate, endDateCreation);
        verifyEventStartDataCollectionIsCreatedOrUpdated("TEST", 2, startDate2);
        verifyEventEndDataCollectionIsCreatedOrUpdated("TEST", 2, endDateCreation2);
        verifyDataCollectionGroupRepHasBeenCreatedOrUpdated("TEST", 2, startDate2, endDateCreation2);

        EasyMock.verify(configurationServiceMock);
    }

    private void verifyDataCollectionGroupRepHasBeenCreatedOrUpdated(String dataCollectionName, int majorVersion, Timestamp startDate, Timestamp endDateCreation) {
        final DataCollectionGroupReplicationSearch dcgRepSearch = new DataCollectionGroupReplicationSearch();
        dcgRepSearch.setDataCollectionGroupName(dataCollectionName);
        dcgRepSearch.setMajorVersion(majorVersion);
        final DataCollectionGroupReplication dcgRep = dataCollectionGroupReplicationService.getUnique(dcgRepSearch);

        final DataCollectionGroupReplication expectedDcgRep = new DataCollectionGroupReplication();
        expectedDcgRep.setId(dcgRep.getId());
        expectedDcgRep.setDataCollectionGroupName(dataCollectionName);
        expectedDcgRep.setMajorVersion(majorVersion);
        expectedDcgRep.setStartDate(startDate);
        expectedDcgRep.setEndDateCreation(endDateCreation);
        Assert.assertEquals(expectedDcgRep, dcgRep);
    }

    private void verifyEventStartDataCollectionIsCreatedOrUpdated(String dataCollectionName, int majorVersion, Timestamp startDate) {
        final EventDataCollectionGroupSearch search = new EventDataCollectionGroupSearch();
        search.setTypes(EventType.START_DATA_COLLECTION_PERIOD);
        search.setDataCollectionGroupName(dataCollectionName);
        search.setMajorVersion(majorVersion);
        final EventDataCollectionGroup event = eventDataCollectionGroupService.getUnique(search);

        final EventDataCollectionGroup expectedEvent = new EventDataCollectionGroup();
        expectedEvent.setId(event.getId());
        expectedEvent.setType(EventType.START_DATA_COLLECTION_PERIOD);
        expectedEvent.setDataCollectionGroupName(dataCollectionName);
        expectedEvent.setMajorVersion(majorVersion);
        expectedEvent.setCreatedOn(event.getCreatedOn());
        expectedEvent.setNotificationTimes(Collections.singletonList(startDate));
        Assert.assertEquals(expectedEvent, event);
    }

    private void verifyEventEndDataCollectionIsCreatedOrUpdated(String dataCollectionName, int majorVersion, Timestamp endDateCreation) {
        final EventDataCollectionGroupSearch search = new EventDataCollectionGroupSearch();
        search.setTypes(EventType.END_DATA_COLLECTION_PERIOD);
        search.setDataCollectionGroupName(dataCollectionName);
        search.setMajorVersion(majorVersion);
        final EventDataCollectionGroup event = eventDataCollectionGroupService.getUnique(search);

        final EventDataCollectionGroup expectedEvent = new EventDataCollectionGroup();
        expectedEvent.setId(event.getId());
        expectedEvent.setType(EventType.END_DATA_COLLECTION_PERIOD);
        expectedEvent.setDataCollectionGroupName(dataCollectionName);
        expectedEvent.setMajorVersion(majorVersion);
        expectedEvent.setCreatedOn(event.getCreatedOn());
        expectedEvent.addNotificationTime(new Timestamp(DateUtils.remove(endDateCreation, new PeriodExpression(1, TimeUnit.DAY)).getTime()));
        expectedEvent.addNotificationTime(new Timestamp(DateUtils.remove(endDateCreation, new PeriodExpression(2, TimeUnit.DAY)).getTime()));
        Assert.assertEquals(expectedEvent, event);
    }

    private Configuration buildConfiguration(final String value) {
        final Configuration configuration = new Configuration();
        configuration.setValue(value);
        return configuration;
    }

    private Timestamp createTimestampIn(int amount, TimeUnit unit) {
        return new Timestamp(DateUtils.add(new Date(), new PeriodExpression(amount, unit)).getTime());
    }

}
