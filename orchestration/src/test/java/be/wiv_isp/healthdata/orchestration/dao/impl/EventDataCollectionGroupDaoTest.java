/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.dao.IEventDataCollectionGroupDao;
import be.wiv_isp.healthdata.orchestration.dao.IUserDao;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.EventDataCollectionGroup;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.Version;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.EventType;
import be.wiv_isp.healthdata.orchestration.domain.search.EventDataCollectionGroupSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@Transactional
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class EventDataCollectionGroupDaoTest {

	private static final String DATA_COLLECTION_1 = "DATA_COLLECTION_1";
	private static final String DATA_COLLECTION_2 = "DATA_COLLECTION_2";

	private User user;
	private Organization organization;

	@Autowired
	private IUserDao userDao;

	@Autowired
	private IOrganizationDao organizationDao;

	@Autowired
	private IEventDataCollectionGroupDao eventDao;

	@Test
 	public void testSearchByDataCollectionName() {
		final EventDataCollectionGroup event1 = createEvent();
		final EventDataCollectionGroup created1 = eventDao.create(event1);
		Assert.assertNotNull(created1);

		final EventDataCollectionGroup event2 = createEvent();
		event2.setDataCollectionGroupName(DATA_COLLECTION_2);
		final EventDataCollectionGroup created2 = eventDao.create(event2);
		Assert.assertNotNull(created2);

		Assert.assertNotEquals(created1.getDataCollectionGroupName(), created2.getDataCollectionGroupName());

		EventDataCollectionGroupSearch search = new EventDataCollectionGroupSearch();
		List<EventDataCollectionGroup> all = eventDao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new EventDataCollectionGroupSearch();
		search.setDataCollectionGroupName(DATA_COLLECTION_1);
		all = eventDao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	@Test
	public void testSearchByMajorVersion() {
		final EventDataCollectionGroup event1 = createEvent();
		final EventDataCollectionGroup created1 = eventDao.create(event1);
		Assert.assertNotNull(created1);

		final EventDataCollectionGroup event2 = createEvent();
		event2.setMajorVersion(2);
		final EventDataCollectionGroup created2 = eventDao.create(event2);
		Assert.assertNotNull(created2);

		Assert.assertNotEquals(created1.getMajorVersion(), created2.getMajorVersion());

		EventDataCollectionGroupSearch search = new EventDataCollectionGroupSearch();
		List<EventDataCollectionGroup> all = eventDao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new EventDataCollectionGroupSearch();
		search.setMajorVersion(1);
		all = eventDao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	private EventDataCollectionGroup createEvent() {
		final EventDataCollectionGroup event = new EventDataCollectionGroup();
		event.setDataCollectionGroupName(DATA_COLLECTION_1);
		event.setMajorVersion(1);

		event.setType(EventType.START_DATA_COLLECTION_PERIOD);
		event.getUsers().add(getUser());
		event.addNotificationTime(createTimestamp(2016, Calendar.SEPTEMBER, 1));
		return event;
	}

	private Version createVersion(int major, int minor) {
		final Version version = new Version();
		version.setMajor(major);
		version.setMinor(minor);
		return version;
	}

	private User getUser() {
		if (user == null) {
			final User u = new User();
			u.setUsername("user1");
			u.setLastName("doe");
			u.setFirstName("john");
			u.setEmail("john.doe@mail.com");
			u.setLdapUser(false);
			u.setEnabled(true);
			u.setAuthorities(new HashSet<>(Collections.singletonList(new Authority(Authority.USER))));
			u.setDataCollectionNames(new HashSet<>(Collections.singletonList("TEST")));
			u.setOrganization(getOrganization());
			user = userDao.create(u);
		}
		return user;
	}

	protected Organization getOrganization() {
		if (organization == null) {
			final Organization o = new Organization();
			o.setHealthDataIDType("RIZIV");
			o.setHealthDataIDValue("11111111");
			o.setName("MyOrganization");
			o.setMain(true);
			o.setDeleted(false);
			organization = organizationDao.create(o);
		}
		return organization;
	}

	protected Timestamp createTimestamp(int year, int month, int day) {
		final Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(year, month, day, 12, 0, 0);
		return new Timestamp(calendar.getTimeInMillis());
	}
}
