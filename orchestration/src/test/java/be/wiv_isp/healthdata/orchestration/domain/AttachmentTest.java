/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.domain;

import org.junit.Assert;
import org.junit.Test;

public class AttachmentTest {

    @Test
    public void testMimeType() {
        // test that the commonly used file extensions are covered
        doTestMimeType("file.txt", "text/plain");
        doTestMimeType("file.pdf", "application/pdf");
        doTestMimeType("file.png", "image/png");
        doTestMimeType("file.jpg", "image/jpeg");
        doTestMimeType("file.jpeg", "image/jpeg");
        doTestMimeType("file.json", "application/json");
        doTestMimeType("file.csv", "text/csv");
        doTestMimeType("file.xml", "application/xml");
    }

    private void doTestMimeType(String filename, String expectedMimeType) {
        final Attachment attachment = new Attachment();
        attachment.setFilename(filename);
        Assert.assertEquals(expectedMimeType, attachment.getMimeType());
    }
}
