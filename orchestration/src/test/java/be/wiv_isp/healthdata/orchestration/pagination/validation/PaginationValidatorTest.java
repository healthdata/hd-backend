/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.pagination.validation;

import java.text.MessageFormat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.pagination.PaginationRange;
import be.wiv_isp.healthdata.common.pagination.PaginationValidator;

public class PaginationValidatorTest {

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void testValidateFormatCorrect() {
		String range[] = new String[] { "0", "1" };
		PaginationValidator.validateFormat(range);
	}

	@Test
	public void testValidateFormatOneParameter() {
		String range[] = new String[] { "0" };
		expectedException.expect(HealthDataException.class);
		expectedException.expectMessage(ExceptionType.INVALID_RANGE_FORMAT.getMessage().replaceAll("''", "'"));
		PaginationValidator.validateFormat(range);
	}

	@Test
	public void testValidateFormatThreeParameters() {
		String range[] = new String[] { "0", "1", "2" };
		expectedException.expect(HealthDataException.class);
		expectedException.expectMessage(ExceptionType.INVALID_RANGE_FORMAT.getMessage().replaceAll("''", "'"));
		PaginationValidator.validateFormat(range);
	}

	@Test
	public void testValidateRangeSuccessful() {
		final int from = 0;
		final int to = 24;
		final PaginationRange range = new PaginationRange(from, to);
		PaginationValidator.validateRange(range);
	}

	@Test
	public void testValidateRangeNegativeFrom() {
		final int from = -1;
		final int to = 24;
		final PaginationRange range = new PaginationRange(from, to);
		expectedException.expect(HealthDataException.class);
		expectedException.expectMessage(MessageFormat.format(ExceptionType.INVALID_RANGE.getMessage(), from, to));
		PaginationValidator.validateRange(range);
	}

	@Test
	public void testValidateRangeNegativeTo() {
		final int from = 0;
		final int to = -1;
		final PaginationRange range = new PaginationRange(from, to);
		expectedException.expect(HealthDataException.class);
		expectedException.expectMessage(MessageFormat.format(ExceptionType.INVALID_RANGE.getMessage(), from, to));
		PaginationValidator.validateRange(range);
	}

	@Test
	public void testValidateRangeToSmallerThenFrom() {
		final int from = 24;
		final int to = 0;
		final PaginationRange range = new PaginationRange(from, to);
		expectedException.expect(HealthDataException.class);
		expectedException.expectMessage(MessageFormat.format(ExceptionType.INVALID_RANGE.getMessage(), from, to));
		PaginationValidator.validateRange(range);
	}

}
