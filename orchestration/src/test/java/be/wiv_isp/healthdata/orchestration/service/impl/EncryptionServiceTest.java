/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import com.sun.jersey.core.util.Base64;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;

public class EncryptionServiceTest {

	private final Logger LOG = LoggerFactory.getLogger(EncryptionServiceTest.class);

	@Test
	public void testEncrypt() {
		final String CONFIGURATION_VALUE = "someValue";

		EncryptionService encryptionService = new EncryptionService();

		byte[] originalData = CONFIGURATION_VALUE.getBytes(StandardCharsets.UTF_8);

		byte[] encryptedData = encryptionService.encrypt(originalData);

		byte[] decryptedData = encryptionService.decrypt(encryptedData);

		Assert.assertArrayEquals(originalData, decryptedData);

		LOG.info("base 64 encrypted value: " + new String(Base64.encode(encryptedData)));
	}

	@Test
	public void testDecrypt() {
		final String ENCRYPTED_CONFIGURATION_VALUE = "KcbG2STYQLWXNewK7cn6IQ==";

		EncryptionService encryptionService = new EncryptionService();

		byte[] encryptedData = Base64.decode(ENCRYPTED_CONFIGURATION_VALUE);

		byte[] decryptedData = encryptionService.decrypt(encryptedData);

		Assert.assertArrayEquals("someValue".getBytes(), decryptedData);

		LOG.info("configuration value: " + new String(decryptedData));
	}
}
