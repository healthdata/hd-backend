/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.ApiType;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Date;

public class AuditTest {

	private static Audit audit;
	private static Audit auditSame;
	private static Audit auditDifferent;

	private static final Long ID = Long.valueOf(1);
	private static final ApiType API_TYPE = ApiType.WORKFLOW;
	private static final String METHOD = "GET";
	private static final String USERNAME = "USERNAME";
	private static final String SUBJECT = "SUBJECT";
	private static final String STATUS = "OK";
	private static final Timestamp TIME_IN = new Timestamp(new Date().getTime());
	private static final Timestamp TIME_OUT = new Timestamp(new Date().getTime());
	private static final String EXCEPTION_MESSAGE = "EXCEPTION_MESSAGE";

	@BeforeClass
	public static void init() {

		audit = new Audit();
		audit.setId(ID);
		audit.setApi(API_TYPE.getDescription());
		audit.setMethod(METHOD);
		audit.setUserName(USERNAME);
		audit.setSubject(SUBJECT);
		audit.setStatus(STATUS);
		audit.setTimeIn(TIME_IN);
		audit.setTimeOut(TIME_OUT);
		audit.setExceptionMessage(EXCEPTION_MESSAGE);

		auditSame = new Audit();
		auditSame.setId(ID);
		auditSame.setApi(API_TYPE.getDescription());
		auditSame.setMethod(METHOD);
		auditSame.setUserName(USERNAME);
		auditSame.setSubject(SUBJECT);
		auditSame.setStatus(STATUS);
		auditSame.setTimeIn(TIME_IN);
		auditSame.setTimeOut(TIME_OUT);
		auditSame.setExceptionMessage(EXCEPTION_MESSAGE);

		auditDifferent = new Audit();
		auditDifferent.setId(ID);
		auditDifferent.setApi(API_TYPE.getDescription());
		auditDifferent.setMethod("POST");
		auditDifferent.setUserName(USERNAME);
		auditDifferent.setSubject(SUBJECT);
		auditDifferent.setStatus(STATUS);
		auditDifferent.setTimeIn(TIME_IN);
		auditDifferent.setTimeOut(TIME_OUT);
		auditDifferent.setExceptionMessage(EXCEPTION_MESSAGE);
	}

	@Test
	public void testEquals() {
		Assert.assertEquals(audit, auditSame);
	}

	@Test
	public void testEqualsNegativeScenario() {
		Assert.assertNotEquals("OTHER OBJECT", auditDifferent);
		Assert.assertNotEquals(audit, auditDifferent);
	}

	@Test
	public void testHashCode() {
		Assert.assertEquals(audit.hashCode(), auditSame.hashCode());
	}

	@Test
	public void testHashCodeNegativeScenario() {
		Assert.assertNotEquals(audit.hashCode(), auditDifferent.hashCode());
	}

	@Test
	public void testToString() {
		final String expectedString = "Audit {" + //
				"id = " + ID + ", " + //
				"api = " + API_TYPE.getDescription() + ", " + //
				"method = " + METHOD + ", " + //
				"userName = " + USERNAME + ", " + //
				"subject = " + SUBJECT + ", " + //
				"status = " + STATUS + ", " + //
				"timeIn = " + TIME_IN + ", " + //
				"timeOut = " + TIME_OUT + ", " + //
				"exceptionMessage = " + EXCEPTION_MESSAGE + "}";

		Assert.assertEquals(expectedString, audit.toString());
	}
}
