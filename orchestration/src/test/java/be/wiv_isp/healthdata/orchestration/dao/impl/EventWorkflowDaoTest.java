/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.dao.IEventWorkflowDao;
import be.wiv_isp.healthdata.orchestration.dao.IUserDao;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.EventWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.WorkflowFlags;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.EventType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.search.EventWorkflowSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@Transactional
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class EventWorkflowDaoTest {

	public static final Long WORKFLOW_ID_1 = 1L;
	public static final Long WORKFLOW_ID_2 = 2L;

	private User user;
	private Organization organization;

	@Autowired
	private IUserDao userDao;

	@Autowired
	private IOrganizationDao organizationDao;

	@Autowired
	private IEventWorkflowDao eventDao;

	@Test
 	public void testSearchByWorkflowId() {
		final EventWorkflow event1 = createEvent();
		final EventWorkflow created1 = eventDao.create(event1);
		Assert.assertNotNull(created1);

		final EventWorkflow event2 = createEvent();
		event2.setWorkflowId(WORKFLOW_ID_2);
		final EventWorkflow created2 = eventDao.create(event2);
		Assert.assertNotNull(created2);

		Assert.assertNotEquals(created1.getWorkflowId(), created2.getWorkflowId());

		EventWorkflowSearch search = new EventWorkflowSearch();
		List<EventWorkflow> all = eventDao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new EventWorkflowSearch();
		search.setWorkflowId(WORKFLOW_ID_1);
		all = eventDao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	private EventWorkflow createEvent() {
		final EventWorkflow event = new EventWorkflow();
		event.setWorkflowId(WORKFLOW_ID_1);
		event.setStatus(WorkflowStatus.IN_PROGRESS);
		event.setFlags(createWorkflowFlags());
		event.setType(EventType.WORKFLOW_STATUS_UPDATE);
		event.getUsers().add(getUser());
		event.addNotificationTime(createTimestamp(2016, Calendar.SEPTEMBER, 15));
		return event;
	}

	private WorkflowFlags createWorkflowFlags() {
		final WorkflowFlags flags = new WorkflowFlags();
		flags.setFollowUp(true);
		flags.setCorrections(true);
		return flags;
	}

	private User getUser() {
		if (user == null) {
			final User u = new User();
			u.setUsername("user1");
			u.setLastName("doe");
			u.setFirstName("john");
			u.setEmail("john.doe@mail.com");
			u.setLdapUser(false);
			u.setEnabled(true);
			u.setAuthorities(new HashSet<>(Collections.singletonList(new Authority(Authority.USER))));
			u.setDataCollectionNames(new HashSet<>(Collections.singletonList("TEST")));
			u.setOrganization(getOrganization());
			user = userDao.create(u);
		}
		return user;
	}

	protected Organization getOrganization() {
		if (organization == null) {
			final Organization o = new Organization();
			o.setHealthDataIDType("RIZIV");
			o.setHealthDataIDValue("11111111");
			o.setName("MyOrganization");
			o.setMain(true);
			o.setDeleted(false);
			organization = organizationDao.create(o);
		}
		return organization;
	}

	protected Timestamp createTimestamp(int year, int month, int day) {
		final Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(year, month, day, 12, 0, 0);
		return new Timestamp(calendar.getTimeInMillis());
	}
}
