/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl.mock;

import be.wiv_isp.healthdata.common.domain.search.IEntitySearch;
import be.wiv_isp.healthdata.common.domain.search.QueryResult;
import be.wiv_isp.healthdata.orchestration.domain.AbstractElasticSearchInfo.Status;
import be.wiv_isp.healthdata.orchestration.domain.search.ElasticSearchInfoSearch;
import be.wiv_isp.healthdata.orchestration.service.IElasticSearchInfoService;

import java.io.Serializable;
import java.util.List;

public class ElasticSearchInfoServiceMock implements IElasticSearchInfoService {

	@Override
	public void updateAll(Status status) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateAll(ElasticSearchInfoSearch search, Status toStatus) {

	}

	@Override
	public Long count(ElasticSearchInfoSearch search) {
		return null;
	}

	@Override
	public List<Object[]> countDetailed() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Long> getWorkflowIds(Status status) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object create(Object o) {
		return null;
	}

	@Override
	public Object get(Serializable id) {
		return null;
	}

	@Override
	public Object update(Object o) {
		return null;
	}

	@Override
	public void delete(Object o) {

	}

	@Override
	public List getAll() {
		return null;
	}

	@Override
	public List getAll(IEntitySearch search) {
		return null;
	}

	@Override
	public QueryResult getAllAsQueryResult(IEntitySearch search) {
		return null;
	}

	@Override
	public Object getUnique(IEntitySearch search) {
		return null;
	}
}
