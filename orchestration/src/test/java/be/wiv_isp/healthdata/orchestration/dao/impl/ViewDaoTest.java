/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.dao.IUserDao;
import be.wiv_isp.healthdata.orchestration.dao.IViewDao;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.View;
import be.wiv_isp.healthdata.orchestration.domain.search.ViewSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@Transactional
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class ViewDaoTest {

	@Autowired
	private IViewDao viewDao;

	@Autowired
	private IUserDao userDao;

	@Autowired
	private IOrganizationDao organizationDao;

	private User user1;
	private User user2;
	private Organization organization;

	@Test
	public void testSearchByName() {
		final View view1 = createView();
		final View created1 = viewDao.create(view1);
		Assert.assertNotNull(created1);

		final View view2 = createView();
		view2.setName("MyView-1");
		final View created2 = viewDao.create(view2);
		Assert.assertNotNull(created2);

		Assert.assertNotEquals(created1.getName(), created2.getName());

		ViewSearch search = new ViewSearch();
		List<View> all = viewDao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new ViewSearch();
		search.setName("MyView");
		all = viewDao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	@Test
	public void testSearchByUserId() {
		final View view1 = createView();
		final View created1 = viewDao.create(view1);
		Assert.assertNotNull(created1);
		Assert.assertEquals(Long.valueOf(1L), created1.getUserId());

		final View view2 = createView();
		view2.setUserId(2L);
		final View created2 = viewDao.create(view2);
		Assert.assertNotNull(created2);

		Assert.assertNotEquals(created1.getUserId(), created2.getUserId());

		ViewSearch search = new ViewSearch();
		List<View> all = viewDao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new ViewSearch();
		search.setUserId(1L);
		all = viewDao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	@Test
	public void testSearchByDataCollectionDefinitionId() {
		final View view1 = createView();
		final View created1 = viewDao.create(view1);
		Assert.assertNotNull(created1);
		Assert.assertEquals(Long.valueOf(1L), created1.getDataCollectionDefinitionId());

		final View view2 = createView();
		view2.setDataCollectionDefinitionId(2L);
		final View created2 = viewDao.create(view2);
		Assert.assertNotNull(created2);

		Assert.assertNotEquals(created1.getDataCollectionDefinitionId(), created2.getDataCollectionDefinitionId());

		ViewSearch search = new ViewSearch();
		List<View> all = viewDao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new ViewSearch();
		search.setDataCollectionDefinitionId(1L);
		all = viewDao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	private View createView() {
		final View view = new View();
		view.setUserId(1L);
		view.setDataCollectionDefinitionId(1L);
		view.setName("MyView");
		view.setContent("content".getBytes(StandardCharsets.UTF_8));
		return viewDao.create(view);
	}

	private User getUser() {
		return getUser(1);
	}

	private User getUser(int id) {
		if(id == 1) {
			if (user1 == null) {
				final User u = new User();
				u.setUsername("user1");
				u.setLastName("doe");
				u.setFirstName("john");
				u.setEmail("john.doe@mail.com");
				u.setLdapUser(false);
				u.setEnabled(true);
				u.setAuthorities(new HashSet<>(Collections.singletonList(new Authority(Authority.USER))));
				u.setDataCollectionNames(new HashSet<>(Collections.singletonList("TEST")));
				u.setOrganization(getOrganization());
				user1 = userDao.create(u);
			}
			return user1;
		}
		if(id == 2) {
			if (user2 == null) {
				final User u = new User();
				u.setUsername("user2");
				u.setLastName("doe");
				u.setFirstName("john");
				u.setEmail("john.doe@mail.com");
				u.setLdapUser(false);
				u.setEnabled(true);
				u.setAuthorities(new HashSet<>(Collections.singletonList(new Authority(Authority.USER))));
				u.setDataCollectionNames(new HashSet<>(Collections.singletonList("TEST")));
				u.setOrganization(getOrganization());
				user2 = userDao.create(u);
			}
			return user2;
		}
		return null;
	}

	private Organization getOrganization() {
		if (organization == null) {
			final Organization o = new Organization();
			o.setHealthDataIDType("RIZIV");
			o.setHealthDataIDValue("11111111");
			o.setName("MyOrganization");
			o.setMain(true);
			o.setDeleted(false);
			organization = organizationDao.create(o);
		}
		return organization;
	}
}
