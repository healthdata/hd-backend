/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.Identification;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.common.util.IdentificationType;
import be.wiv_isp.healthdata.orchestration.service.IIdentificationService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.test.util.ReflectionTestUtils;

import static org.easymock.EasyMock.*;

public class IdentificationServiceTest {

	@Rule
	public ExpectedException exception = ExpectedException.none();

	private final IIdentificationService identificationService = new IdentificationService();

	private IConfigurationService configurationService;

	@Before
	public void init() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		configurationService = createNiceMock(IConfigurationService.class);

		ReflectionTestUtils.setField(identificationService, "configurationService", configurationService);
	}

	@Test
	public void testIdentificationOrganiszation() {
		Organization organization = createOrganization(1L, IdentificationType.RIZIV.toString(), "IDENTIFICATION_VALUE");
		final Configuration typeConfiguration = createConfiguration(ConfigurationKey.IDENTIFICATION_TYPE, "NIHII");
		final Configuration valueConfiguration = createConfiguration(ConfigurationKey.IDENTIFICATION_VALUE, "12345678");
		final Configuration appIdConfiguration = createConfiguration(ConfigurationKey.APPLICATION_ID, "");

		expect(configurationService.get(ConfigurationKey.IDENTIFICATION_TYPE, organization)).andReturn(typeConfiguration);
		expect(configurationService.get(ConfigurationKey.IDENTIFICATION_VALUE, organization)).andReturn(valueConfiguration);
		expect(configurationService.get(ConfigurationKey.APPLICATION_ID, organization)).andReturn(appIdConfiguration);
		replay(configurationService);

		final Identification identification = identificationService.getEtkIdentification(organization);

		verify(configurationService);
		Assert.assertEquals("NIHII", identification.getType());
		Assert.assertEquals("12345678", identification.getValue());
		Assert.assertEquals("", identification.getApplicationId());
	}

	@Test
	public void testLandingZoneIdentification() {
		final Configuration idTypeConfiguration = createConfiguration(ConfigurationKey.HD4RES_IDENTIFICATION_TYPE, IdentificationType.RIZIV.toString());
		final Configuration idValueConfiguration = createConfiguration(ConfigurationKey.HD4RES_IDENTIFICATION_VALUE, "HD4RES_IDENTIFICATION_VALUE");
		final Configuration appIdConfiguration = createConfiguration(ConfigurationKey.HD4RES_APPLICATION_ID, "HD4RES_APPLICATION_ID");

		expect(configurationService.get(ConfigurationKey.HD4RES_IDENTIFICATION_TYPE)).andReturn(idTypeConfiguration);
		expect(configurationService.get(ConfigurationKey.HD4RES_IDENTIFICATION_VALUE)).andReturn(idValueConfiguration);
		expect(configurationService.get(ConfigurationKey.HD4RES_APPLICATION_ID)).andReturn(appIdConfiguration);
		replay(configurationService);

		final Identification identification = identificationService.getHd4ResIdentification();

		verify(configurationService);
		Assert.assertEquals(identification.getType().toString(), idTypeConfiguration.getValue());
		Assert.assertEquals(identification.getValue(), idValueConfiguration.getValue());
		Assert.assertEquals(identification.getApplicationId(), appIdConfiguration.getValue());
	}

	@Test
	public void testEHealthCodageIdentification() {
		final Configuration idTypeConfiguration = createConfiguration(ConfigurationKey.EHEALTH_CODAGE_IDENTIFICATION_TYPE, IdentificationType.CBE.toString());
		final Configuration idValueConfiguration = createConfiguration(ConfigurationKey.EHEALTH_CODAGE_IDENTIFICATION_VALUE, "EHEALTH_CODAGE_IDENTIFICATION_VALUE");
		final Configuration appIdConfiguration = createConfiguration(ConfigurationKey.EHEALTH_CODAGE_APPLICATION_ID, "EHEALTH_CODAGE_APPLICATION_ID");

		expect(configurationService.get(ConfigurationKey.EHEALTH_CODAGE_IDENTIFICATION_TYPE)).andReturn(idTypeConfiguration);
		expect(configurationService.get(ConfigurationKey.EHEALTH_CODAGE_IDENTIFICATION_VALUE)).andReturn(idValueConfiguration);
		expect(configurationService.get(ConfigurationKey.EHEALTH_CODAGE_APPLICATION_ID)).andReturn(appIdConfiguration);
		replay(configurationService);

		final Identification identification = identificationService.getEHealthCodageIdentification();

		verify(configurationService);
		Assert.assertEquals(identification.getType().toString(), idTypeConfiguration.getValue());
		Assert.assertEquals(identification.getValue(), idValueConfiguration.getValue());
		Assert.assertEquals(identification.getApplicationId(), appIdConfiguration.getValue());
	}

	private Organization createOrganization(Long id, String identificationType, String identificationValue) {
		Organization organization = new Organization();
		organization.setId(id);
		organization.setHealthDataIDType(identificationType);
		organization.setHealthDataIDValue(identificationValue);
		return organization;
	}

	private Configuration createConfiguration(final ConfigurationKey key, final String value) {
		final Configuration configuration = new Configuration();

		configuration.setKey(key);
		configuration.setValue(value);

		return configuration;
	}
}
