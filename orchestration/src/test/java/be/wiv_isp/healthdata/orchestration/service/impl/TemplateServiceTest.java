/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.common.domain.enumeration.TemplateKey;
import be.wiv_isp.healthdata.common.domain.enumeration.TimeUnit;
import be.wiv_isp.healthdata.orchestration.domain.PeriodExpression;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.DisplayableWorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.mail.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.apache.velocity.runtime.resource.loader.StringResourceLoader;
import org.apache.velocity.runtime.resource.util.StringResourceRepositoryImpl;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * This class is used to
 * 1. Test the TemplateService class
 * 2. Visualize template outputs (useful when creating or updating templates)
 *    The private process method write the output to an html file which can be opened in a browser.
 */
@Ignore
public class TemplateServiceTest {

    private static final Logger LOG = LoggerFactory.getLogger(TemplateServiceTest.class);

    private static VelocityEngine ve = new VelocityEngine();

    private static final File EXAMPLES_DIR = new File("src/test/resources/mail-templates/examples");

    @BeforeClass
    public static void beforeClass() {
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "string,classpath");
        ve.setProperty("string.resource.loader.class", StringResourceLoader.class.getName());
        ve.setProperty("string.resource.loader.repository.class", StringResourceRepositoryImpl.class.getName());
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        ve.init();
    }

    @Test
    public void testRequestAcceptForRequester() throws IOException {
        final UserRequestContext.UserRequest userRequest = new UserRequestContext.UserRequest();
        userRequest.setUsername("user");
        userRequest.setPassword("pazz");
        userRequest.setLastName("Doe");
        userRequest.setFirstName("John");

        final UserRequestContext context = new UserRequestContext();
        context.setUserRequest(userRequest);
        process(TemplateKey.USER_REQUEST_ACCEPT_FOR_REQUESTER, context);
    }

    @Test
    public void testRequestAcceptForUserDb() throws IOException {
        final UserRequestContext.UserRequest userRequest = new UserRequestContext.UserRequest();
        userRequest.setUsername("user");
        userRequest.setPassword("pazz");
        userRequest.setLastName("Doe");
        userRequest.setFirstName("John");
        userRequest.setDataCollections(getDataCollections());

        final UserRequestContext context = new UserRequestContext();
        context.setUserRequest(userRequest);
        context.setInstallationUrl("http://hd4dp.healthdata.be");
        process(TemplateKey.USER_REQUEST_ACCEPT_FOR_USER_DB, context);
    }

    @Test
    public void testRequestAcceptForUserLdap() throws IOException {
        final UserRequestContext.UserRequest userRequest = new UserRequestContext.UserRequest();
        userRequest.setLastName("Doe");
        userRequest.setFirstName("John");

        final UserRequestContext context = new UserRequestContext();
        context.setUserRequest(userRequest);
        context.setInstallationUrl("http://hd4dp.healthdata.be");
        process(TemplateKey.USER_REQUEST_ACCEPT_FOR_USER_LDAP, context);
    }

    @Test
    public void testRequestNewForAdmin() throws IOException {
        final UserRequestContext.UserRequest userRequest = new UserRequestContext.UserRequest();
        userRequest.setLastName("Doe");
        userRequest.setFirstName("John");
        userRequest.setUrl("http://hd4dp.healthdata.be/userrequests/1");
        userRequest.setDataCollections(getDataCollections());

        final UserRequestContext context = new UserRequestContext();
        context.setUserRequest(userRequest);
        context.setInstallationUrl("http://hd4dp.healthdata.be");
        process(TemplateKey.USER_REQUEST_NEW_FOR_ADMIN, context);
    }

    @Test
    public void testRequestNewForRequester() throws IOException {
        final UserRequestContext.UserRequest userRequest = new UserRequestContext.UserRequest();
        userRequest.setLastName("Doe");
        userRequest.setFirstName("John");
        userRequest.setDataCollections(getDataCollections());

        final UserRequestContext context = new UserRequestContext();
        context.setUserRequest(userRequest);
        context.setInstallationUrl("http://hd4dp.healthdata.be");
        process(TemplateKey.USER_REQUEST_NEW_FOR_REQUESTER, context);
    }

    @Test
    public void testRequestNewForUser() throws IOException {
        final UserRequestContext.UserRequest userRequest = new UserRequestContext.UserRequest();
        userRequest.setLastName("Doe");
        userRequest.setFirstName("John");
        userRequest.setDataCollections(getDataCollections());

        final UserRequestContext context = new UserRequestContext();
        context.setUserRequest(userRequest);
        context.setInstallationUrl("http://hd4dp.healthdata.be");
        process(TemplateKey.USER_REQUEST_NEW_FOR_USER, context);
    }

    @Test
    public void testRequestRejectForRequester() throws IOException {
        final UserRequestContext.UserRequest userRequest = new UserRequestContext.UserRequest();
        userRequest.setLastName("Doe");
        userRequest.setFirstName("John");

        final UserRequestContext context = new UserRequestContext();
        context.setUserRequest(userRequest);
        process(TemplateKey.USER_REQUEST_REJECT_FOR_REQUESTER, context);
    }

    @Test
    public void testRequestRejectForUser() throws IOException {
        final UserRequestContext.UserRequest userRequest = new UserRequestContext.UserRequest();
        final UserRequestContext context = new UserRequestContext();
        context.setUserRequest(userRequest);
        process(TemplateKey.USER_REQUEST_REJECT_FOR_USER, context);
    }

    @Test
    public void testRequestUpdateForRequester() throws IOException {
        final UserRequestContext.UserRequest userRequest = new UserRequestContext.UserRequest();
        userRequest.setLastName("Doe");
        userRequest.setFirstName("John");
        userRequest.setDataCollections(getDataCollections());

        final UserRequestContext context = new UserRequestContext();
        context.setUserRequest(userRequest);
        process(TemplateKey.USER_REQUEST_UPDATE_FOR_REQUESTER, context);
    }

    @Test
    public void testRequestUpdateForUser() throws IOException {
        final UserRequestContext.UserRequest userRequest = new UserRequestContext.UserRequest();
        userRequest.setLastName("Doe");
        userRequest.setFirstName("John");
        userRequest.setDataCollections(getDataCollections());

        final UserRequestContext context = new UserRequestContext();
        context.setUserRequest(userRequest);
        process(TemplateKey.USER_REQUEST_UPDATE_FOR_USER, context);
    }

    @Test
    public void testRegistrationUpdates() throws IOException {
        final RegistrationUpdatesContext.RegistrationDetails details1 = createDetails(1L, "http://hd4dp.healthdata.be/registrations/1", DisplayableWorkflowStatus.CORRECTIONS_NEEDED);
        final RegistrationUpdatesContext.RegistrationDetails details2 = createDetails(2L, "http://hd4dp.healthdata.be/registrations/2", DisplayableWorkflowStatus.FOLLOW_UP_NEEDED);
        final RegistrationUpdatesContext.RegistrationDetails details3 = createDetails(3L, "http://hd4dp.healthdata.be/registrations/3", DisplayableWorkflowStatus.CORRECTIONS_NEEDED, DisplayableWorkflowStatus.FOLLOW_UP_NEEDED);

        final RegistrationUpdatesContext context = new RegistrationUpdatesContext();
        context.setInstallationUrl("http://hd4dp.healthdata.be");
        context.add("DATA_COLLECTION_1", details1);
        context.add("DATA_COLLECTION_1", details2);
        context.add("DATA_COLLECTION_2", details3);

        process(TemplateKey.REGISTRATION_UPDATES, context);
    }

    @Test
    public void testRegistrationUpdatesHd4res() throws IOException {
        final RegistrationUpdatesContext.RegistrationDetails details1 = createDetails(1L, "http://hd4dp.healthdata.be/registrations/1", DisplayableWorkflowStatus.NEW);
        final RegistrationUpdatesContext.RegistrationDetails details2 = createDetails(2L, "http://hd4dp.healthdata.be/registrations/2", DisplayableWorkflowStatus.NEW_CORRECTION);
        final RegistrationUpdatesContext.RegistrationDetails details3 = createDetails(3L, "http://hd4dp.healthdata.be/registrations/3", DisplayableWorkflowStatus.NEW_FOLLOW_UP);
        final RegistrationUpdatesContext.RegistrationDetails details4 = createDetails(4L, "http://hd4dp.healthdata.be/registrations/4", DisplayableWorkflowStatus.NEW_CORRECTION, DisplayableWorkflowStatus.NEW_FOLLOW_UP);
        final RegistrationUpdatesContext.RegistrationDetails details5 = createDetails(5L, "http://hd4dp.healthdata.be/registrations/5", DisplayableWorkflowStatus.COMMENTS_NEEDED);
        final RegistrationUpdatesContext.RegistrationDetails details6 = createDetails(6L, "http://hd4dp.healthdata.be/registrations/6", DisplayableWorkflowStatus.COMMENTS_NEEDED);

        final RegistrationUpdatesContext context = new RegistrationUpdatesContext();
        context.setInstallationUrl("http://hd4dp.healthdata.be");
        context.add("DATA_COLLECTION_1", details1);
        context.add("DATA_COLLECTION_1", details2);
        context.add("DATA_COLLECTION_1", details3);
        context.add("DATA_COLLECTION_2", details4);
        context.add("DATA_COLLECTION_2", details5);
        context.add("DATA_COLLECTION_2", details6);

        process(TemplateKey.REGISTRATION_UPDATES_HD4RES, context);
    }

    @Test
    public void testUploadedStableData() throws IOException {
        final DataCollectionDefinitionContext context = new DataCollectionDefinitionContext();
        context.setDataCollectionName("TEST");
        process(TemplateKey.UPLOADED_STABLE_DATA, context);
    }

    @Test
     public void testStartOfDataCollectionPeriod() throws IOException {
        final DataCollectionDefinitionContext context = new DataCollectionDefinitionContext();
        context.setDataCollectionName("TEST");
        context.setInstallationUrl("http://hd4dp.healthdata.be");
        process(TemplateKey.START_OF_DATA_COLLECTION_PERIOD, context);
    }

    @Test
    public void testEndOfDataCollectionPeriodAllDatesDifferent() throws IOException {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 7);
        final Date inOneWeek = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 7);
        final Date inTwoWeeks = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 7);
        final Date inThreeweeks = calendar.getTime();

        final DataCollectionDefinitionContext context = new DataCollectionDefinitionContext();
        context.setDataCollectionName("TEST");
        context.setEndDateCreation(DateFormat.DDMMYYYY.format(inOneWeek));
        context.setEndDateSubmission(DateFormat.DDMMYYYY.format(inTwoWeeks));
        context.setEndDateComments(DateFormat.DDMMYYYY.format(inThreeweeks));
        context.setPeriod("one week");
        context.setInstallationUrl("http://hd4dp.healthdata.be");
        process(TemplateKey.END_OF_DATA_COLLECTION_PERIOD, context, "end_of_dcd_period_all_dates_different.html");
    }

    @Test
    public void testEndOfDataCollectionPeriodAllDatesEqual() throws IOException {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 7);
        final Date inOneWeek = calendar.getTime();

        final DataCollectionDefinitionContext context = new DataCollectionDefinitionContext();
        context.setDataCollectionName("TEST");
        context.setEndDateCreation(DateFormat.DDMMYYYY.format(inOneWeek));
        context.setEndDateSubmission(DateFormat.DDMMYYYY.format(inOneWeek));
        context.setEndDateComments(DateFormat.DDMMYYYY.format(inOneWeek));
        context.setPeriod("one week");
        context.setInstallationUrl("http://hd4dp.healthdata.be");
        process(TemplateKey.END_OF_DATA_COLLECTION_PERIOD, context, "end_of_dcd_period_all_dates_equals.html");
    }

    @Test
    public void testEndOfDataCollectionPeriodCreationAndSubmissionDatesAreEqual() throws IOException {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 7);
        final Date inOneWeek = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 7);
        final Date inTwoWeeks = calendar.getTime();

        final DataCollectionDefinitionContext context = new DataCollectionDefinitionContext();
        context.setDataCollectionName("TEST");
        context.setEndDateCreation(DateFormat.DDMMYYYY.format(inOneWeek));
        context.setEndDateSubmission(DateFormat.DDMMYYYY.format(inOneWeek));
        context.setEndDateComments(DateFormat.DDMMYYYY.format(inTwoWeeks));
        context.setPeriod("one week");
        context.setInstallationUrl("http://hd4dp.healthdata.be");
        process(TemplateKey.END_OF_DATA_COLLECTION_PERIOD, context, "end_of_dcd_period_creation_and_submission_dates_are_equal.html");
    }

    @Test
    public void testEndOfDataCollectionPeriodSubmissionAndCommentsDatesAreEqual() throws IOException {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 7);
        final Date inOneWeek = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 7);
        final Date inTwoWeeks = calendar.getTime();

        final DataCollectionDefinitionContext context = new DataCollectionDefinitionContext();
        context.setDataCollectionName("TEST");
        context.setEndDateCreation(DateFormat.DDMMYYYY.format(inOneWeek));
        context.setEndDateSubmission(DateFormat.DDMMYYYY.format(inTwoWeeks));
        context.setEndDateComments(DateFormat.DDMMYYYY.format(inTwoWeeks));
        context.setPeriod("one week");
        context.setInstallationUrl("http://hd4dp.healthdata.be");
        process(TemplateKey.END_OF_DATA_COLLECTION_PERIOD, context, "end_of_dcd_period_submission_and_comments_dates_are_equal.html");
    }

    @Test
    public void testEndOfDataCollectionPeriodSubmissionAndCommentsDatesAreNull() throws IOException {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 7);
        final Date inOneWeek = calendar.getTime();

        final DataCollectionDefinitionContext context = new DataCollectionDefinitionContext();
        context.setDataCollectionName("TEST");
        context.setEndDateCreation(DateFormat.DDMMYYYY.format(inOneWeek));
        context.setPeriod("one week");
        context.setInstallationUrl("http://hd4dp.healthdata.be");
        process(TemplateKey.END_OF_DATA_COLLECTION_PERIOD, context, "end_of_dcd_period_submission_and_comments_dates_are_null.html");
    }

    @Test
    public void testEndOfDataCollectionPeriodCommentDateIsNullAndEqual() throws IOException {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 7);
        final Date inOneWeek = calendar.getTime();

        final DataCollectionDefinitionContext context = new DataCollectionDefinitionContext();
        context.setDataCollectionName("TEST");
        context.setEndDateCreation(DateFormat.DDMMYYYY.format(inOneWeek));
        context.setEndDateSubmission(DateFormat.DDMMYYYY.format(inOneWeek));
        context.setPeriod("one week");
        context.setInstallationUrl("http://hd4dp.healthdata.be");
        process(TemplateKey.END_OF_DATA_COLLECTION_PERIOD, context, "end_of_dcd_period_comment_date_is_null_and_equal.html");
    }


    @Test
    public void testEndOfDataCollectionPeriodCommentDateIsNullAndNotEqual() throws IOException {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 7);
        final Date inOneWeek = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 7);
        final Date inTwoWeeks = calendar.getTime();

        final DataCollectionDefinitionContext context = new DataCollectionDefinitionContext();
        context.setDataCollectionName("TEST");
        context.setEndDateCreation(DateFormat.DDMMYYYY.format(inOneWeek));
        context.setEndDateSubmission(DateFormat.DDMMYYYY.format(inTwoWeeks));
        context.setPeriod("one week");
        context.setInstallationUrl("http://hd4dp.healthdata.be");
        process(TemplateKey.END_OF_DATA_COLLECTION_PERIOD, context, "end_of_dcd_period_comment_date_is_null_and_not_equal.html");
    }

    @Test
    public void testDataCollectionDefinitionUpdate() throws IOException {
        final DataCollectionDefinitionContext context = new DataCollectionDefinitionContext();
        context.setDataCollectionName("TEST");
        context.setUpdateDescription("The validation rule \"BMI cannot be higher than 100\" has been added");
        process(TemplateKey.DATA_COLLECTION_DEFINITION_UPDATE, context);
    }

    @Test
    public void testNewUserAccount() throws IOException {
        final UserAccountContext context = new UserAccountContext();
        context.setInstallationUrl("http://hd4dp.healthdata.be");
        context.setUsername("user");
        context.setPassword("pazz");
        context.setLastName("Doe");
        context.setFirstName("John");
        context.setDataCollections(getDataCollections());
        process(TemplateKey.NEW_USER_ACCOUNT, context);
    }

    @Test
    public void testNewUserAccountWithoutLastNameAndFirstName() throws IOException {
        final UserAccountContext context = new UserAccountContext();
        context.setInstallationUrl("http://hd4dp.healthdata.be");
        context.setUsername("user");
        context.setPassword("pazz");
        context.setDataCollections(getDataCollections());
        process(TemplateKey.NEW_USER_ACCOUNT, context);
    }

    @Test
     public void testNewDataCollectionAccessRequest() throws IOException {
        final DataCollectionAccessRequestContext context = new DataCollectionAccessRequestContext();
        context.setInstallationUrl("http://hd4dp.healthdata.be");
        context.setLastName("Doe");
        context.setFirstName("John");
        context.setRequestedDataCollections(getDataCollections());
        process(TemplateKey.DATA_COLLECTION_ACCESS_REQUEST_NEW, context);
    }

    @Test
    public void testDataCollectionAccessRequestResult() throws IOException {
        final DataCollectionAccessRequestContext context = new DataCollectionAccessRequestContext();
        context.setInstallationUrl("http://hd4dp.healthdata.be");
        context.setLastName("Doe");
        context.setFirstName("John");
        context.setRequestedDataCollections(getDataCollections());
        final SortedMap<String, Boolean> decisions = new TreeMap<>();
        decisions.put("DATA_COLLECTION_1", true);
        decisions.put("DATA_COLLECTION_2", false);
        decisions.put("DATA_COLLECTION_3", true);
        context.setDecisions(decisions);
        process(TemplateKey.DATA_COLLECTION_ACCESS_REQUEST_RESULT, context);
    }

    @Test
    public void testPasswordReset() throws IOException {
        final PasswordResetContext context = new PasswordResetContext();
        context.setLastName("Doe");
        context.setFirstName("John");
        context.setResetPasswordLink("http://healthdata.be/resetPassword");
        context.setValidityPeriod(new PeriodExpression(10, TimeUnit.MINUTE));
        context.setSupportEmail("support@test.com");
        context.setPlatform(Platform.HD4DP);
        process(TemplateKey.PASSWORD_RESET, context);
    }

    private RegistrationUpdatesContext.RegistrationDetails createDetails(long id, String link, DisplayableWorkflowStatus... statuses) {
        final RegistrationUpdatesContext.RegistrationDetails details = new RegistrationUpdatesContext.RegistrationDetails();
        details.setId(id);
        details.setLink(link);
        details.setStatuses(new HashSet<>(Arrays.asList(statuses)));
        details.setOpenSince(DateFormat.DDMMYYYY.format(new Date()));
        return details;
    }

    private void process(TemplateKey key, AbstractVelocityContext context) throws IOException {
        final String outputFile = key.toString().toLowerCase() + ".html";
        process(key, context, outputFile);
    }

    private void process(TemplateKey key, AbstractVelocityContext context, String outputFile) throws IOException {
        final String inputFile = key.toString().toLowerCase() + ".vm";
        final String templateContent = IOUtils.toString(TemplateServiceTest.class.getClassLoader().getResourceAsStream("mail-templates/" + inputFile), StandardCharsets.UTF_8);
        final String content = new TemplateService().process(templateContent, context);
        final File outputDir = EXAMPLES_DIR;
        if(!outputDir.exists()) {
            outputDir.mkdirs();
        }
        final File file = new File(outputDir, outputFile);
        FileUtils.writeStringToFile(file, content);
        LOG.info("template written to {}", file.getAbsolutePath());
    }

    private Set<String> getDataCollections() {
        return new HashSet<>(Arrays.asList("DATA_COLLECTION_1", "DATA_COLLECTION_2", "DATA_COLLECTION_3"));
    }
}
