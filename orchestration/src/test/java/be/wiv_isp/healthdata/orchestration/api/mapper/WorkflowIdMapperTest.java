/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONArray;
import org.junit.Assert;
import org.junit.Test;

public class WorkflowIdMapperTest {

	private final WorkflowIdMapper mapper = new WorkflowIdMapper();

	@Test
	public void testMapper1() throws Exception {
		String request = IOUtils.toString(WorkflowIdMapper.class.getResourceAsStream("workflowIdMapperTest1.json"));
		JSONArray json = new JSONArray(request);

		List<Long> expected = new ArrayList<>();
		expected.add(1L);
		expected.add(2L);
		expected.add(3L);

		List<Long> convert = mapper.convert(json);
		Assert.assertEquals(expected, convert);
	}

	@Test
	public void testMapper2() throws Exception {
		String request = IOUtils.toString(WorkflowIdMapper.class.getResourceAsStream("workflowIdMapperTest2.json"));
		JSONArray json = new JSONArray(request);

		List<Long> expected = new ArrayList<>();
		expected.add(1L);
		expected.add(2L);
		expected.add(3L);

		List<Long> convert = mapper.convert(json);
		Assert.assertEquals(expected, convert);
	}

	@Test(expected = NumberFormatException.class)
	public void testMapper3() throws Exception {
		String request = IOUtils.toString(WorkflowIdMapper.class.getResourceAsStream("workflowIdMapperTest3.json"));
		JSONArray json = new JSONArray(request);

		mapper.convert(json);
	}

}
