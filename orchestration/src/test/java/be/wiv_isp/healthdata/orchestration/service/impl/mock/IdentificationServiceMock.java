/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl.mock;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.orchestration.domain.Identification;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.service.IIdentificationService;

public class IdentificationServiceMock implements IIdentificationService {
    @Override
    public HealthDataIdentification getHealthDataIdentification(Organization organization) {
        return null;
    }

    @Override
    public Identification getEtkIdentification(Organization organization) {
        return null;
    }

    @Override
    public Identification getHd4ResIdentification() {
        return null;
    }

    @Override
    public Identification getEHealthCodageIdentification() {
        return null;
    }
}
