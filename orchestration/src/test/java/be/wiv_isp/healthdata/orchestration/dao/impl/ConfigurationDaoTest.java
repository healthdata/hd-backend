/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.orchestration.dao.IConfigurationDao;
import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.search.ConfigurationSearch;
import be.wiv_isp.healthdata.orchestration.service.impl.EncryptionService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class ConfigurationDaoTest {

	public static final ConfigurationKey KEY_1 = ConfigurationKey.CATALOGUE_HOST;
	public static final ConfigurationKey KEY_2 = ConfigurationKey.USER_MANAGEMENT_TYPE;

	@Autowired
	private IConfigurationDao dao;

	@Autowired
	private IOrganizationDao organizationDao;

	@Autowired
	private DataSource dataSource;

	@Autowired
	@Qualifier("transactionManager")
	protected PlatformTransactionManager txManager;

	@After
	public void after() throws SQLException {
		final Connection connection = dataSource.getConnection();
		connection.prepareStatement("DELETE FROM CONFIGURATIONS").execute();
		connection.prepareStatement("DELETE FROM ORGANIZATIONS").execute();
	}

	@Test
	@Transactional
	public void testGetById() throws Exception {
		final Configuration configuration1 = dao.create(buildConfiguration(KEY_1, "value1", "value1", null));
		final Configuration configuration2 = dao.create(buildConfiguration(KEY_1, "value1", "value1", null));

		final Configuration returnedConfiguration = dao.get(configuration1.getId());
		Assert.assertEquals(configuration1, returnedConfiguration);
	}

	@Test
	@Transactional
	public void testUpdate() {
		final Configuration configuration1 = dao.create(buildConfiguration(KEY_1, "value1", "value1", null));
		final Configuration configuration2 = dao.create(buildConfiguration(KEY_2, "value2", "value2", null));

		final Configuration configuration = dao.get(configuration1.getId());
		Assert.assertTrue("value1".equals(configuration.getValue()));
		configuration.setValue("value1_modified");
		dao.update(configuration);
		final Configuration updatedConfiguration = dao.get(configuration1.getId());
		Assert.assertTrue("value1_modified".equals(updatedConfiguration.getValue()));
	}

	@Test
	@Transactional
	public void testDelete() {
		final Configuration configuration1 = dao.create(buildConfiguration(KEY_1, "value1", "value1", null));
		final Configuration configuration2 = dao.create(buildConfiguration(KEY_1, "value1", "value1", null));

		Assert.assertEquals(2, dao.getAll().size());
		dao.delete(configuration1);
		Assert.assertEquals(1, dao.getAll().size());
		Assert.assertNull(dao.get(configuration1.getId()));
	}

	@Test
	@Transactional
	public void testSearchByKey() throws Exception {
		final Configuration configuration1 = dao.create(buildConfiguration(KEY_1, "value1", "value1", null));
		final Configuration configuration2 = dao.create(buildConfiguration(KEY_2, "value1", "value1", null));

		ConfigurationSearch search = new ConfigurationSearch();
		List<Configuration> configurations = dao.getAll(search);
		Assert.assertEquals(2, configurations.size());
		Assert.assertTrue(configurations.contains(configuration1));
		Assert.assertTrue(configurations.contains(configuration2));

		search = new ConfigurationSearch();
		search.setKey(KEY_1);
		configurations = dao.getAll(search);
		Assert.assertEquals(1, configurations.size());
		Assert.assertTrue(configurations.contains(configuration1));
	}

	@Test
	@Transactional
	public void testSearchByOrganization() throws Exception {
		final Organization organization1 = organizationDao.create(buildOrganization("NIHII-HOSPITAL", "11111111", true));
		final Organization organization2 = organizationDao.create(buildOrganization("NIHII-HOSPITAL", "22222222", true));

		final Configuration configuration1 = dao.create(buildConfiguration(KEY_1, "value1", "value1", organization1));
		final Configuration configuration2 = dao.create(buildConfiguration(KEY_1, "value1", "value1", organization2));

		ConfigurationSearch search = new ConfigurationSearch();
		List<Configuration> configurations = dao.getAll(search);
		Assert.assertEquals(2, configurations.size());
		Assert.assertTrue(configurations.contains(configuration1));
		Assert.assertTrue(configurations.contains(configuration2));

		search = new ConfigurationSearch();
		search.setOrganization(organization1);
		configurations = dao.getAll(search);
		Assert.assertEquals(1, configurations.size());
		Assert.assertTrue(configurations.contains(configuration1));
	}

	@Test
	@Transactional
	public void testSearchByOrganizationCanBeNull() throws Exception {
		final Organization organization = organizationDao.create(buildOrganization("NIHII-HOSPITAL", "11111111", true));

		final Configuration configuration1 = dao.create(buildConfiguration(KEY_1, "value1", "value1", organization));
		final Configuration configuration2 = dao.create(buildConfiguration(KEY_1, "value1", "value1", null));

		ConfigurationSearch search = new ConfigurationSearch();
		List<Configuration> configurations = dao.getAll(search);
		Assert.assertEquals(2, configurations.size());
		Assert.assertTrue(configurations.contains(configuration1));
		Assert.assertTrue(configurations.contains(configuration2));

		search = new ConfigurationSearch();
		search.setOrganization(organization);
		configurations = dao.getAll(search);
		Assert.assertEquals(2, configurations.size());
		Assert.assertTrue(configurations.contains(configuration1));
		Assert.assertTrue(configurations.contains(configuration2));

		search = new ConfigurationSearch();
		search.setOrganization(organization);
		search.setOrganizationCanBeNull(false);
		configurations = dao.getAll(search);
		Assert.assertEquals(1, configurations.size());
		Assert.assertTrue(configurations.contains(configuration1));

		search = new ConfigurationSearch();
		search.setOrganizationCanBeNull(false);
		configurations = dao.getAll(search);
		Assert.assertEquals(1, configurations.size());
		Assert.assertTrue(configurations.contains(configuration1));
	}

	@Test
	public void testReadableValueIsStoredUnencrypted() throws SQLException {
		final String value = "value";

		final Configuration configuration = buildConfiguration(ConfigurationKey.CATALOGUE_HOST, "", "", null);

		final TransactionTemplate txTemplate = new TransactionTemplate(txManager);
		txTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
				dao.create(configuration);
				configuration.setValue(value);
				dao.update(configuration);
			}
		});

		final Connection connection = dataSource.getConnection();
		final PreparedStatement preparedStatement = connection.prepareStatement("SELECT VALUE FROM CONFIGURATIONS WHERE CONFIGURATION_ID = ?");
		preparedStatement.setLong(1, configuration.getId());
		final ResultSet rs = preparedStatement.executeQuery();
		String persistedValue = null;
		while (rs.next()) {
			persistedValue = rs.getString("VALUE");
		}

		Assert.assertEquals(persistedValue, value);

		final Configuration retrievedConfiguration = dao.get(configuration.getId());
		Assert.assertEquals(retrievedConfiguration.getValue(), value);
	}

	@Test
	public void testUnreadableValueIsStoredEncrypted() throws SQLException {
		final String value = "value";

		final Configuration configuration = buildConfiguration(ConfigurationKey.CATALOGUE_PASSWORD, "", "", null);

		final TransactionTemplate txTemplate = new TransactionTemplate(txManager);
		txTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
				dao.create(configuration);
				configuration.setValue(value);
				dao.update(configuration);
			}
		});

		final Connection connection = dataSource.getConnection();
		final PreparedStatement preparedStatement = connection.prepareStatement("SELECT VALUE FROM CONFIGURATIONS WHERE CONFIGURATION_ID = ?");
		preparedStatement.setLong(1, configuration.getId());
		final ResultSet rs = preparedStatement.executeQuery();
		String persistedValue = null;
		while (rs.next()) {
			persistedValue = rs.getString("VALUE");
		}

		Assert.assertNotEquals(persistedValue, value);
		Assert.assertEquals(persistedValue, new EncryptionService().encrypt(value));

		final Configuration retrievedConfiguration = dao.get(configuration.getId());
		Assert.assertEquals(retrievedConfiguration.getValue(), value);
	}

	@Test
	public void testDeleteWithInvalidKey() throws SQLException {

		// create 2 configurations
		TransactionTemplate txTemplate = new TransactionTemplate(txManager);
		txTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
				Configuration configuration = new Configuration();
				configuration.setKey(ConfigurationKey.GUI_HOST);
				configuration.setValue("guiHost");
				configuration.setDefaultValue("guiHost");
				dao.create(configuration);

				configuration = new Configuration();
				configuration.setKey(ConfigurationKey.CATALOGUE_HOST);
				configuration.setValue("catalogueHost");
				configuration.setDefaultValue("catalogueHost");
				dao.create(configuration);
			}
		});

		Assert.assertEquals(2, dao.getAll().size());

		// introduce an invalid key
		dataSource.getConnection().prepareStatement("UPDATE CONFIGURATIONS SET LOOKUP_KEY = 'NON_EXISTING_CONFIGURATION_KEY' WHERE LOOKUP_KEY = 'GUI_HOST'").executeUpdate();

		try {
			dao.getAll();
			Assert.fail("An illegal argument exception must be thrown since a non valid key has been introduced");
		} catch (IllegalArgumentException e) {

		}

		// remove invalid keys (method to test)
		txTemplate = new TransactionTemplate(txManager);
		txTemplate.execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
				dao.deleteWithInvalidKey();
			}
		});

		// verify invalid key has been deleted
		final List<Configuration> all = dao.getAll();
		Assert.assertEquals(1, all.size());
		Assert.assertEquals(ConfigurationKey.CATALOGUE_HOST, all.get(0).getKey());
	}

	private Organization buildOrganization(String identifiactionType, String identificationValue, boolean main) {
		final Organization organization = new Organization();
		organization.setHealthDataIDType(identifiactionType);
		organization.setHealthDataIDValue(identificationValue);
		organization.setMain(main);
		return organization;
	}

	private Configuration buildConfiguration(ConfigurationKey key, String value, String defaultValue, Organization organization) {
		final Configuration configuration = new Configuration();
		configuration.setKey(key);
		configuration.setValue(value);
		configuration.setDefaultValue(defaultValue);
		configuration.setOrganization(organization);
		return configuration;
	}
}
