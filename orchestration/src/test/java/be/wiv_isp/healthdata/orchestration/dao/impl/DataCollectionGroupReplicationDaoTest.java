/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.orchestration.dao.IDataCollectionGroupReplicationDao;
import be.wiv_isp.healthdata.orchestration.domain.DataCollectionGroupReplication;
import be.wiv_isp.healthdata.orchestration.domain.search.DataCollectionGroupReplicationSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class DataCollectionGroupReplicationDaoTest {

	@Autowired
	private IDataCollectionGroupReplicationDao dao;

	@Test
 	public void testSearchByDataCollectionGroupName() {
		final DataCollectionGroupReplication dcgRep1 = buildDcgRep();
		final DataCollectionGroupReplication created1 = dao.create(dcgRep1);

		final DataCollectionGroupReplication dcgRep2 = buildDcgRep();
		dcgRep2.setDataCollectionGroupName("TEST_2");
		final DataCollectionGroupReplication created2 = dao.create(dcgRep2);

		Assert.assertNotEquals(created1.getDataCollectionGroupName(), created2.getDataCollectionGroupName());

		DataCollectionGroupReplicationSearch search = new DataCollectionGroupReplicationSearch();
		List<DataCollectionGroupReplication> all = dao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new DataCollectionGroupReplicationSearch();
		search.setDataCollectionGroupName("TEST");
		all = dao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	@Test
	public void testSearchByMajorVersion() {
		final DataCollectionGroupReplication dcgRep1 = buildDcgRep();
		final DataCollectionGroupReplication created1 = dao.create(dcgRep1);

		final DataCollectionGroupReplication dcgRep = buildDcgRep();
		dcgRep.setMajorVersion(2);
		final DataCollectionGroupReplication created2 = dao.create(dcgRep);

		Assert.assertNotEquals(created1.getMajorVersion(), created2.getMajorVersion());

		DataCollectionGroupReplicationSearch search = new DataCollectionGroupReplicationSearch();
		List<DataCollectionGroupReplication> all = dao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new DataCollectionGroupReplicationSearch();
		search.setMajorVersion(1);
		all = dao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	private DataCollectionGroupReplication buildDcgRep() {
		final DataCollectionGroupReplication dcgRep = new DataCollectionGroupReplication();
		dcgRep.setDataCollectionGroupName("TEST");
		dcgRep.setMajorVersion(1);
		return dcgRep;
	}

}
