/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.orchestration.dao.IAttachmentDao;
import be.wiv_isp.healthdata.orchestration.domain.Attachment;
import be.wiv_isp.healthdata.orchestration.domain.AttachmentContent;
import be.wiv_isp.healthdata.orchestration.domain.search.AttachmentSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class AttachmentDaoTest {

    public static final String UUID1 = "18255e82-48a5-4bc1-b3ba-7214ec693a29";
    public static final String UUID2 = "b6e39065-4734-4faf-bfc8-f9d4742a33a2";

    @Autowired
    private IAttachmentDao dao;

    @Test
    public void testSearchByUuid() {
        final Attachment attachment1 = createAttachment();
        final Attachment created1 = dao.create(attachment1);
        Assert.assertNotNull(created1);

        final Attachment attachment2 = createAttachment();
        attachment2.setUuid(UUID2);
        final Attachment created2 = dao.create(attachment2);
        Assert.assertNotNull(created2);

        Assert.assertNotEquals(created1.getUuid(), created2.getUuid());

        AttachmentSearch search = new AttachmentSearch();
        List<Attachment> all = dao.getAll(search);
        Assert.assertEquals(2, all.size());
        Assert.assertTrue(all.contains(created1));
        Assert.assertTrue(all.contains(created2));

        search = new AttachmentSearch();
        search.setUuid(UUID1);
        all = dao.getAll(search);
        Assert.assertEquals(1, all.size());
        Assert.assertTrue(all.contains(created1));
    }

    @Test
    public void testSearchByContentId() {
        final Attachment attachment1 = createAttachment();
        final Attachment created1 = dao.create(attachment1);
        Assert.assertNotNull(created1);

        final Attachment attachment2 = createAttachment();
        final Attachment created2 = dao.create(attachment2);
        Assert.assertNotNull(created2);

        Assert.assertNotEquals(created1.getContent().getId(), created2.getContent().getId());

        AttachmentSearch search = new AttachmentSearch();
        List<Attachment> all = dao.getAll(search);
        Assert.assertEquals(2, all.size());
        Assert.assertTrue(all.contains(created1));
        Assert.assertTrue(all.contains(created2));

        search = new AttachmentSearch();
        search.setContentId(attachment1.getContent().getId());
        all = dao.getAll(search);
        Assert.assertEquals(1, all.size());
        Assert.assertTrue(all.contains(created1));
    }

    private Attachment createAttachment() {
        final Attachment header = new Attachment();
        header.setUuid(UUID1);
        header.setFilename("test.txt");
        header.setMimeType("plain/text");
        header.setContent(createAttachmentContent());
        return header;
    }

    private AttachmentContent createAttachmentContent() {
        final byte[] bytes = "content".getBytes();
        final AttachmentContent content = new AttachmentContent();
        content.setContent(bytes);
        content.setSize(bytes.length);
        content.setHash(String.valueOf(Arrays.hashCode(bytes)));
        return content;
    }
}
