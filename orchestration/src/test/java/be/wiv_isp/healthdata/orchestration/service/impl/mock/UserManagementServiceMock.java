/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl.mock;

import be.wiv_isp.healthdata.common.domain.search.QueryResult;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.UserManagementType;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import be.wiv_isp.healthdata.orchestration.security.authentication.strategy.IUserManagementStrategy;
import be.wiv_isp.healthdata.orchestration.service.IUserManagementService;

import java.util.Map;
import java.util.Set;

public class UserManagementServiceMock implements IUserManagementService {
    @Override
    public IUserManagementStrategy getUserManagementStrategy() {
        return new IUserManagementStrategy() {

            @Override
            public User create(User user, Organization organization) {
                return null;
            }

            @Override
            public User read(Long id) {
                return null;
            }

            @Override
            public User read(Organization organization) {
                return null;
            }

            @Override
            public User update(Long id, User user) {
                return null;
            }

            @Override
            public User updatePassword(Long id, String oldPassword, String newPassword) {
                return null;
            }

            @Override
            public void sendPasswordResetInstructions(Organization organization, String email) {

            }

            @Override
            public void resetPassword(Organization organization, String token, String newPassword) {

            }

            @Override
            public User updateMetadata(Long id, Map<String, String> metaData) {
                return null;
            }

            @Override
            public void delete(Long id) {

            }

            @Override
            public boolean supports(UserManagementType userManagementType) {
                return true;
            }

            @Override
            public Set<String> getEmailAdmins(Organization organization) {
                return null;
            }

            @Override
            public QueryResult<User> getAll(UserSearch search) {
                return new QueryResult();
            }
        };
    }

    @Override
    public UserManagementType getUserManagementType(Organization organization) {
        return null;
    }

    @Override
    public UserManagementType getUserManagementType() {
        return UserManagementType.DATABASE;
    }
}
