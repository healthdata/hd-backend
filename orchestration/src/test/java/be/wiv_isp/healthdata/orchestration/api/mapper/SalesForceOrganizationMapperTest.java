/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import be.wiv_isp.healthdata.common.dto.SalesForceOrganizationDto;

public class SalesForceOrganizationMapperTest {

	@Test
	public void testMapper1() throws Exception {
		String request = IOUtils.toString(WorkflowIdMapper.class.getResourceAsStream("salesForceOrganizationMapperTest1.json"));
		JSONArray json = new JSONArray(request);

		List<SalesForceOrganizationDto> expected = new ArrayList<>();
		expected.add(getOrganisation("NIHII-HOSPITAL", "11111111", "hospital 1"));
		expected.add(getOrganisation("NIHII-HOSPITAL", "22222222", "hospital 2"));
		expected.add(getOrganisation("NIHII-HOSPITAL", "33333333", "hospital 3"));

		List<SalesForceOrganizationDto> convert = SalesForceOrganizationMapper.convert(json);
		Assert.assertEquals(expected, convert);
	}

	@Test
	public void testMapper2() throws Exception {
		String request = IOUtils.toString(WorkflowIdMapper.class.getResourceAsStream("salesForceOrganizationMapperTest2.json"));
		JSONObject json = new JSONObject(request);

		SalesForceOrganizationDto expected = getOrganisation("NIHII-HOSPITAL", "11111111", "hospital 1");

		SalesForceOrganizationDto convert = SalesForceOrganizationMapper.convert(json);
		Assert.assertEquals(expected, convert);
	}

	@Test
	public void testMapper3() throws Exception {
		String request = IOUtils.toString(WorkflowIdMapper.class.getResourceAsStream("salesForceOrganizationMapperTest3.json"));
		JSONObject json = new JSONObject(request);

		SalesForceOrganizationDto expected = new SalesForceOrganizationDto();

		SalesForceOrganizationDto convert = SalesForceOrganizationMapper.convert(json);
		Assert.assertEquals(expected, convert);
	}

	public SalesForceOrganizationDto getOrganisation(String identificationType, String identificationValue, String name) {
		SalesForceOrganizationDto salesForceOrganizationDto = new SalesForceOrganizationDto();
		salesForceOrganizationDto.setIdentificationType(identificationType);
		salesForceOrganizationDto.setIdentificationValue(identificationValue);
		salesForceOrganizationDto.setName(name);
		return salesForceOrganizationDto;
	}
}
