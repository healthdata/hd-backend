/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class ConfigurationTest {

	private static Configuration configuration;
	private static Configuration configurationSame;
	private static Configuration configurationDifferent;

	private static final Long ID = 1L;
	private static final String VALUE = "CONFIGURATION_VALUE";
	private static final String DEFAULT_VALUE = "DEFAULT_CONFIGURATION_VALUE";
	private static final ConfigurationKey CONFIGURATION_KEY = ConfigurationKey.APPLICATION_ID;

	@BeforeClass
	public static void init() {

		configuration = new Configuration();
		configuration.setId(ID);
		configuration.setKey(CONFIGURATION_KEY);
		configuration.setValue(VALUE);
		configuration.setDefaultValue(DEFAULT_VALUE);

		configurationSame = new Configuration();
		configurationSame.setId(ID);
		configurationSame.setKey(CONFIGURATION_KEY);
		configurationSame.setValue(VALUE);
		configurationSame.setDefaultValue(DEFAULT_VALUE);

		configurationDifferent = new Configuration();
		configurationDifferent.setId(ID);
		configurationDifferent.setKey(CONFIGURATION_KEY);
		configurationDifferent.setValue("DIFFERENT_VALUE");
		configurationDifferent.setDefaultValue(DEFAULT_VALUE);
	}

	@Test
	public void testEquals() {
		Assert.assertEquals(configuration, configurationSame);
	}

	@Test
	public void testEqualsNegativeScenario() {
		Assert.assertNotEquals("OTHER_OBJECT", configuration);
		Assert.assertNotEquals(configuration, configurationDifferent);
	}

	@Test
	public void testHashCode() {
		Assert.assertEquals(configuration.hashCode(), configurationSame.hashCode());
	}

	@Test
	public void testHashCodeNegativeScenario() {
		Assert.assertNotEquals(configuration.hashCode(), configurationDifferent.hashCode());
	}
}
