/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.dao.IDataCollectionAccessRequestDao;
import be.wiv_isp.healthdata.orchestration.dao.IUserDao;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.DataCollectionAccessRequest;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.search.DataCollectionAccessRequestSearch;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@Transactional
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class DataCollectionAccessRequestDaoTest {

	private User user1;
	private User user2;
	private User user3;
	private Organization organization1;
	private Organization organization2;

	@Autowired
	private IDataCollectionAccessRequestDao dao;

	@Autowired
	private IUserDao userDao;

	@Autowired
	private IOrganizationDao organizationDao;

	@Test
 	public void testSearchById() {
		final DataCollectionAccessRequest request1 = createRequest();
		final DataCollectionAccessRequest created1 = dao.create(request1);
		Assert.assertNotNull(created1);

		final DataCollectionAccessRequest request2 = createRequest();
		final DataCollectionAccessRequest created2 = dao.create(request2);
		Assert.assertNotNull(created2);

		Assert.assertNotEquals(created1.getId(), created2.getId());

		DataCollectionAccessRequestSearch search = new DataCollectionAccessRequestSearch(getOrganization(1).getId());
		List<DataCollectionAccessRequest> all = dao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new DataCollectionAccessRequestSearch(getOrganization(1).getId());
		search.setId(created1.getId());
		all = dao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	@Test
	public void testSearchByUserId() {
		final DataCollectionAccessRequest request1 = createRequest();
		final DataCollectionAccessRequest created1 = dao.create(request1);
		Assert.assertNotNull(created1);

		final DataCollectionAccessRequest request2 = createRequest();
		request2.setUser(getUser(2));
		final DataCollectionAccessRequest created2 = dao.create(request2);
		Assert.assertNotNull(created2);

		Assert.assertNotEquals(created1.getUser().getId(), created2.getUser().getId());

		DataCollectionAccessRequestSearch search = new DataCollectionAccessRequestSearch(getOrganization(1).getId());
		List<DataCollectionAccessRequest> all = dao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new DataCollectionAccessRequestSearch(getOrganization(1).getId());
		search.setUserId(created1.getUser().getId());
		all = dao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));
	}

	@Test
	public void testSearchByOrganizationId() {
		final DataCollectionAccessRequest request1 = createRequest();
		final DataCollectionAccessRequest created1 = dao.create(request1);
		Assert.assertNotNull(created1);

		final DataCollectionAccessRequest request2 = createRequest();
		request2.setUser(getUser(3));
		final DataCollectionAccessRequest created2 = dao.create(request2);
		Assert.assertNotNull(created2);

		Assert.assertNotEquals(created1.getUser().getOrganization().getId(), created2.getUser().getOrganization().getId());

		DataCollectionAccessRequestSearch search = new DataCollectionAccessRequestSearch(getOrganization(1).getId());
		List<DataCollectionAccessRequest> all = dao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));

		search = new DataCollectionAccessRequestSearch(getOrganization(2).getId());
		all = dao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created2));
	}

	@Test
	public void testSearchByProcessed() {
		final DataCollectionAccessRequest request1 = createRequest();
		final DataCollectionAccessRequest created1 = dao.create(request1);
		Assert.assertNotNull(created1);

		final DataCollectionAccessRequest request2 = createRequest();
		request2.setProcessed(true);
		final DataCollectionAccessRequest created2 = dao.create(request2);
		Assert.assertNotNull(created2);

		Assert.assertNotEquals(created1.isProcessed(), created2.isProcessed());

		DataCollectionAccessRequestSearch search = new DataCollectionAccessRequestSearch(getOrganization(1).getId());
		List<DataCollectionAccessRequest> all = dao.getAll(search);
		Assert.assertEquals(2, all.size());
		Assert.assertTrue(all.contains(created1));
		Assert.assertTrue(all.contains(created2));

		search = new DataCollectionAccessRequestSearch(getOrganization(1).getId());
		search.setProcessed(false);
		all = dao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created1));

		search = new DataCollectionAccessRequestSearch(getOrganization(1).getId());
		search.setProcessed(true);
		all = dao.getAll(search);
		Assert.assertEquals(1, all.size());
		Assert.assertTrue(all.contains(created2));
	}

	private DataCollectionAccessRequest createRequest() {
		final DataCollectionAccessRequest request = new DataCollectionAccessRequest();
		request.setUser(getUser());
		request.setDataCollections(new HashSet<>(Arrays.asList("TEST1", "TEST2")), false);
		request.setProcessed(false);
		return request;
	}

	private User getUser() {
		return getUser(1);
	}

	private User getUser(int id) {
		if(id == 1) {
			if (user1 == null) {
				final User u = new User();
				u.setUsername("user1");
				u.setLastName("doe");
				u.setFirstName("john");
				u.setEmail("john.doe@mail.com");
				u.setLdapUser(false);
				u.setEnabled(true);
				u.setAuthorities(new HashSet<>(Collections.singletonList(new Authority(Authority.USER))));
				u.setDataCollectionNames(new HashSet<>(Collections.singletonList("TEST")));
				u.setOrganization(getOrganization(1));
				user1 = userDao.create(u);
			}
			return user1;
		}
		if(id == 2) {
			if (user2 == null) {
				final User u = new User();
				u.setUsername("user2");
				u.setLastName("doe");
				u.setFirstName("john");
				u.setEmail("john.doe@mail.com");
				u.setLdapUser(false);
				u.setEnabled(true);
				u.setAuthorities(new HashSet<>(Collections.singletonList(new Authority(Authority.USER))));
				u.setDataCollectionNames(new HashSet<>(Collections.singletonList("TEST")));
				u.setOrganization(getOrganization(1));
				user2 = userDao.create(u);
			}
			return user2;
		}
		if(id == 3) {
			if (user3 == null) {
				final User u = new User();
				u.setUsername("user2");
				u.setLastName("doe");
				u.setFirstName("john");
				u.setEmail("john.doe@mail.com");
				u.setLdapUser(false);
				u.setEnabled(true);
				u.setAuthorities(new HashSet<>(Collections.singletonList(new Authority(Authority.USER))));
				u.setDataCollectionNames(new HashSet<>(Collections.singletonList("TEST")));
				u.setOrganization(getOrganization(2));
				user3 = userDao.create(u);
			}
			return user3;
		}
		return null;
	}

	private Organization getOrganization(int id) {
		if(id == 1) {
			if (organization1 == null) {
				final Organization o = new Organization();
				o.setHealthDataIDType("RIZIV");
				o.setHealthDataIDValue("11111111");
				o.setName("MyOrganization1");
				o.setMain(true);
				o.setDeleted(false);
				organization1 = organizationDao.create(o);
			}
			return organization1;
		}
		if(id == 2) {
			if (organization2 == null) {
				final Organization o = new Organization();
				o.setHealthDataIDType("RIZIV");
				o.setHealthDataIDValue("22222222");
				o.setName("MyOrganization2");
				o.setMain(true);
				o.setDeleted(false);
				organization2 = organizationDao.create(o);
			}
			return organization2;
		}
		throw new RuntimeException("no organization found for id " + id);
	}
}
