/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.orchestration.dao.ICommentDao;
import be.wiv_isp.healthdata.orchestration.dao.INoteDao;
import be.wiv_isp.healthdata.orchestration.domain.Comment;
import be.wiv_isp.healthdata.orchestration.domain.Note;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = { "classpath:/applicationContext-test-dao.xml" })
public class NoteDaoTest {

    @Autowired
    private INoteDao noteDao;

    @Autowired
    private ICommentDao commentDao;

    private Comment note1Comment1;
    private Comment note1Comment2;
    private Comment note2Comment1;
    private Comment note3Comment1;
    private Comment note3Comment2;

    private void setup() {
        Note note1 = new Note();
        note1.setWorkflowType(WorkflowType.REGISTRATION);
        note1.setDocumentId(1L);
        note1.setOrder(0L);
        note1.setCommentMaxOrder(1L);
        note1.setFieldPath("root");
        note1.setResolved(false);
        note1.setTitle("TITLE NOTE 1");
        note1.setDeleted(false);
        noteDao.create(note1);

        note1Comment1 = new Comment();
        note1Comment1.setNoteId(note1.getId());
        note1Comment1.setOrder(0L);
        note1Comment1.setPlatform(Platform.HD4DP);
        note1Comment1.setUsername("UD4DP_USER");
        note1Comment1.setContent("COMMENT 1");
        note1Comment1.setDeleted(false);
        commentDao.create(note1Comment1);

        note1Comment2 = new Comment();
        note1Comment2.setNoteId(note1.getId());
        note1Comment2.setOrder(1L);
        note1Comment2.setPlatform(Platform.HD4RES);
        note1Comment2.setUsername("UD4RES_USER");
        note1Comment2.setContent("COMMENT 2");
        note1Comment2.setDeleted(false);
        commentDao.create(note1Comment2);

        Note note2 = new Note();
        note2.setWorkflowType(WorkflowType.REGISTRATION);
        note2.setDocumentId(1L);
        note2.setOrder(1L);
        note2.setCommentMaxOrder(0L);
        note2.setFieldPath("root.field1");
        note2.setResolved(false);
        note2.setTitle("TITLE NOTE 2");
        note2.setDeleted(false);
        noteDao.create(note2);

        note2Comment1 = new Comment();
        note2Comment1.setNoteId(note2.getId());
        note2Comment1.setOrder(0L);
        note2Comment1.setPlatform(Platform.HD4DP);
        note2Comment1.setUsername("UD4DP_USER");
        note2Comment1.setContent("COMMENT 1");
        note2Comment1.setDeleted(false);
        commentDao.create(note2Comment1);

        Note note3 = new Note();
        note3.setWorkflowType(WorkflowType.REGISTRATION);
        note3.setDocumentId(2L);
        note3.setOrder(0L);
        note3.setCommentMaxOrder(1L);
        note3.setFieldPath("root");
        note3.setResolved(false);
        note3.setTitle("TITLE NOTE 1");
        note3.setDeleted(false);
        noteDao.create(note3);

        note3Comment1 = new Comment();
        note3Comment1.setNoteId(note3.getId());
        note3Comment1.setOrder(0L);
        note3Comment1.setPlatform(Platform.HD4DP);
        note3Comment1.setUsername("UD4DP_USER");
        note3Comment1.setContent("COMMENT 1");
        note3Comment1.setDeleted(false);
        commentDao.create(note3Comment1);

        note3Comment2 = new Comment();
        note3Comment2.setNoteId(note3.getId());
        note3Comment2.setOrder(1L);
        note3Comment2.setPlatform(Platform.HD4RES);
        note3Comment2.setUsername("UD4RES_USER");
        note3Comment2.setContent("COMMENT 2");
        note3Comment2.setDeleted(false);
        commentDao.create(note3Comment2);
    }

    @Test
    public void testCommentsByWorkflowId() {
        setup();
        List<Comment> commentsWorkflow1 = commentDao.getByDocumentId(1L);
        Assert.assertEquals(3, commentsWorkflow1.size());
        Assert.assertTrue(commentsWorkflow1.contains(note1Comment1));
        Assert.assertTrue(commentsWorkflow1.contains(note1Comment2));
        Assert.assertTrue(commentsWorkflow1.contains(note2Comment1));
        Assert.assertFalse(commentsWorkflow1.contains(note3Comment1));
        Assert.assertFalse(commentsWorkflow1.contains(note3Comment2));

        List<Comment> commentsWorkflow2 = commentDao.getByDocumentId(2L);
        Assert.assertEquals(2, commentsWorkflow2.size());
        Assert.assertFalse(commentsWorkflow2.contains(note1Comment1));
        Assert.assertFalse(commentsWorkflow2.contains(note1Comment2));
        Assert.assertFalse(commentsWorkflow2.contains(note2Comment1));
        Assert.assertTrue(commentsWorkflow2.contains(note3Comment1));
        Assert.assertTrue(commentsWorkflow2.contains(note3Comment2));


    }

//    @Test
//    public void testSearchByWorkflowId() {
//        final Note note1 = createNote(1L);
//        final Note created1 = noteDao.create(note1);
//        Assert.assertNotNull(created1);
//
//        final Note note2 = createNote(1L);
//        final Note created2 = noteDao.create(note2);
//        Assert.assertNotNull(created2);
//
//        final Note note3 = createNote(1L);
//        final Note created3 = noteDao.create(note3);
//        Assert.assertNotNull(created3);
//
//        final Note note4 = createNote(2L);
//        final Note created4 = noteDao.create(note4);
//        Assert.assertNotNull(created4);
//
//
//        NoteSearch search1 = new NoteSearch();
//        search1.setWorkflowId(1L);
//        List<Note> all1 = noteDao.getAll(search1);
//        Assert.assertEquals(3, all1.size());
//        Assert.assertTrue(all1.contains(created1));
//        Assert.assertTrue(all1.contains(created2));
//        Assert.assertTrue(all1.contains(created3));
//        Assert.assertFalse(all1.contains(created4));
//
//        NoteSearch search2 = new NoteSearch();
//        search2.setWorkflowId(2L);
//        List<Note> all2 = noteDao.getAll(search2);
//        Assert.assertEquals(1, all2.size());
//        Assert.assertFalse(all2.contains(created1));
//        Assert.assertFalse(all2.contains(created2));
//        Assert.assertFalse(all2.contains(created3));
//        Assert.assertTrue(all2.contains(created4));
//    }
//
//    private Note createNote(Long workflowId) {
//
//        final Note note = new Note();
//        note.setWorkflowId(workflowId);
//        note.setUuid(UUID.randomUUID().toString());
//        note.setPlatform(Platform.HD4DP);
//        note.setUsername("username");
//        note.setContent("content");
//        return note;
//    }
}
