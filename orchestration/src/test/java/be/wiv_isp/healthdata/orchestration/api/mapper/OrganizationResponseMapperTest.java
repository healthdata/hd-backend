/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api.mapper;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import be.wiv_isp.healthdata.orchestration.domain.Organization;

public class OrganizationResponseMapperTest {

	@Test
	public void test() throws Exception {
		List<Organization> organizations = new ArrayList<>();
		organizations.add(getOrganization(1L, "NIHII-HOSPITAL", "11111111", "hospital 1", true));
		organizations.add(getOrganization(2L, "NIHII-HOSPITAL", "22222222", "hospital 2", false));
		organizations.add(getOrganization(3L, "NIHII-HOSPITAL", "33333333", "hospital 3", false));

		String convert = OrganizationResponseMapper.convert(organizations);
		Assert.assertEquals("[{\"id\":1,\"healthDataIDType\":\"NIHII-HOSPITAL\",\"healthDataIDValue\":\"11111111\",\"name\":\"hospital 1\",\"main\":true}"
				+ ",{\"id\":2,\"healthDataIDType\":\"NIHII-HOSPITAL\",\"healthDataIDValue\":\"22222222\",\"name\":\"hospital 2\",\"main\":false}"
				+ ",{\"id\":3,\"healthDataIDType\":\"NIHII-HOSPITAL\",\"healthDataIDValue\":\"33333333\",\"name\":\"hospital 3\",\"main\":false}]", convert);
	}

	@Test
	public void testEmptyCollection() throws Exception {
		List<Organization> organisations = new ArrayList<>();

		String convert = OrganizationResponseMapper.convert(organisations);
		Assert.assertEquals("[]", convert);
	}

	private Organization getOrganization(Long id, String healthDataIDType, String healthDataIDValue, String name, boolean main) {
		Organization organization = new Organization();
		organization.setId(id);
		organization.setHealthDataIDType(healthDataIDType);
		organization.setHealthDataIDValue(healthDataIDValue);
		organization.setName(name);
		organization.setMain(main);
		return organization;
	}

}
