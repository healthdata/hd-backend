/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api.mapper;

import org.apache.commons.io.IOUtils;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import be.wiv_isp.healthdata.orchestration.domain.Organization;

public class OrganizationResquestMapperTest {

	@Test
	public void testMapNl() throws Exception {
		String request = IOUtils.toString(OrganizationResquestMapperTest.class.getResourceAsStream("organizationResquestMapperTest.json"));
		JSONObject json = new JSONObject(request);

		Organization expected = new Organization();
		expected.setId(1L);
		expected.setHealthDataIDType("Riziv");
		expected.setHealthDataIDValue("11111111");
		expected.setName("TEST hospital 2");
		expected.setMain(true);

		Organization convert = OrganizationRequestMapper.convert(json);
		Assert.assertEquals(expected, convert);
	}

}
