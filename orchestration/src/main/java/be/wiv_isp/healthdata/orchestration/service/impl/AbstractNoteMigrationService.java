/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import be.wiv_isp.healthdata.orchestration.dto.NoteDto;
import be.wiv_isp.healthdata.orchestration.jaxb.json.JsonUnmarshaller;
import be.wiv_isp.healthdata.orchestration.mapping.MappingResponse;
import be.wiv_isp.healthdata.orchestration.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.List;

abstract  public class AbstractNoteMigrationService<Workflow extends AbstractRegistrationWorkflow> implements INoteMigrationService<Workflow> {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractNoteMigrationService.class);

    @Autowired
    private INoteService noteService;
    @Autowired
    private IAbstractRegistrationWorkflowService workflowService;
    @Autowired
    private IMappingService mappingService;
    @Autowired
    private IDataCollectionDefinitionForwardService catalogueService;
    @Autowired
    private IElasticSearchService elasticSearchService;


    @Override
    public void migrateCommentsToNotes(Workflow workflow) {
        final DataCollectionDefinitionDtoV7 dataCollectionDefinition = catalogueService.get(workflow.getDataCollectionDefinitionId());
        migrateCommentsToNotes(workflow, dataCollectionDefinition.getContent());

    }

    @Override
    public void migrateCommentsToNotes(Workflow workflow, byte[] dcdContent) {
        LOG.info(MessageFormat.format("Migrating comments of workflow with id [{0}] to notes.", workflow.getId()));

        final MappingResponse mappingResponse = mappingService.extractNotes(workflow, dcdContent);

        if (mappingResponse.getNotes().length() == 0) {
            LOG.info("No comments to be migrated for workflow with id [{}].", workflow.getId());
            setMigrated(workflow.getId());
            elasticSearchService.index(workflow.getId());
            return;
        }

        final byte[] documentContent = mappingResponse.getDocumentContent().toString().getBytes(StandardCharsets.UTF_8);
        DocumentData documentData = getDocumentData(workflow);
        documentData.setContent(documentContent);
        workflowService.setDocumentData(workflow, documentData, null, false); // don't add a history item

        final List<NoteDto> migratedComments = JsonUnmarshaller.unmarshal(mappingResponse.getNotes(), NoteDto.class);
        noteService.migrateNotes(WorkflowType.REGISTRATION, workflow.getDocument(), migratedComments);

        setMigrated(workflow.getId());

        elasticSearchService.index(workflow.getId());
    }

    abstract protected DocumentData getDocumentData(Workflow workflow);

    abstract protected void setMigrated(Long workflowId);

}
