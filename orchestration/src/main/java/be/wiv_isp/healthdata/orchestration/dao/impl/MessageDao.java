/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.orchestration.dao.IMessageDao;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.MessageCorrespondent;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.search.GroupByMessage;
import be.wiv_isp.healthdata.orchestration.domain.search.MessageSearch;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class MessageDao extends CrudDaoV2<Message, Long, MessageSearch> implements IMessageDao {

	public MessageDao() {
		super(Message.class);
	}

	@Override
	public List<GroupByMessage> getCountByStatuses(Organization organization) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<GroupByMessage> cq = cb.createQuery(GroupByMessage.class);
		Root<Message> rootEntry = cq.from(Message.class);
		cq.multiselect(rootEntry.get("status"), cb.count(rootEntry.get("status")));
		cq.where(cb.equal(rootEntry.get("organization"), organization));
		cq.groupBy(rootEntry.get("status"));

		TypedQuery<GroupByMessage> allQuery = em.createQuery(cq);
		return allQuery.getResultList();
	}

	@Override
	protected List<Predicate> getPredicates(MessageSearch search, CriteriaBuilder cb, Root<Message> rootEntry) {
		final List<Predicate> predicates = new ArrayList<>();

		if (search.getId() != null) {
			predicates.add(cb.equal(rootEntry.get("id"), search.getId()));
		}

		if (search.getStatuses() != null) {
			predicates.add(rootEntry.get("status").in(search.getStatuses()));
		}

		if (search.getOrganization() != null) {
			predicates.add(rootEntry.get("organization").in(search.getOrganization()));
		}

		if (search.getSentBefore() != null) {
			predicates.add(cb.lessThan(rootEntry.<Date>get("sentOn"), search.getSentBefore()));
		}

		if (search.getLinkedId() != null) {
			predicates.add(cb.equal(rootEntry.get("linkedId"), search.getLinkedId()));
		}

		if (search.getWorkflowId() != null) {
			predicates.add(cb.equal(rootEntry.get("workflowId"), search.getWorkflowId()));
		}

		if (search.getWorkflowId() != null) {
			predicates.add(cb.equal(rootEntry.get("workflowId"), search.getWorkflowId()));
		}

		if (search.getType() != null) {
			predicates.add(cb.equal(rootEntry.get("type"), search.getType()));
		}

		if (search.getIdentificationType() != null) {
			predicates.add(cb.equal(rootEntry.<MessageCorrespondent>get("correspondent").get("identificationType"), search.getIdentificationType()));
		}

		if (search.getIdentificationValue() != null) {
			predicates.add(cb.equal(rootEntry.<MessageCorrespondent>get("correspondent").get("identificationValue"), search.getIdentificationValue()));
		}

		if (search.isHasRemainingSendingTrials() != null) {
			if(search.isHasRemainingSendingTrials()) {
				predicates.add(cb.greaterThan(rootEntry.<Integer>get("remainingSendingTrials"), 0));
			} else {
				predicates.add(cb.equal(rootEntry.<Integer>get("remainingSendingTrials"), 0));
			}
		}

		return predicates;
	}
}
