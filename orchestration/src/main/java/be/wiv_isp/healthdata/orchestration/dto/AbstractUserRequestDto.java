/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dto;

import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import be.wiv_isp.healthdata.orchestration.domain.AbstractUserRequest;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

abstract public class AbstractUserRequestDto<UserRequest extends AbstractUserRequest> {

    protected Long id;
    protected String firstName;
    protected String lastName;
    protected String email;
    protected String emailRequester;
    protected Set<String> dataCollectionNames;
    protected Boolean approved;
    protected Timestamp createdOn;
    protected Timestamp updatedOn;

    public AbstractUserRequestDto(UserRequest userRequest) {
        id = userRequest.getId();
        firstName = userRequest.getFirstName();
        lastName = userRequest.getLastName();
        email = userRequest.getEmail();
        emailRequester = userRequest.getEmailRequester();
        approved = userRequest.getApproved();
        updatedOn = userRequest.getUpdatedOn();
        createdOn = userRequest.getCreatedOn();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailRequester() {
        return emailRequester;
    }

    public void setEmailRequester(String emailRequester) {
        this.emailRequester = emailRequester;
    }

    public Set<String> getDataCollectionNames() {
        if (dataCollectionNames == null) {
            dataCollectionNames = new HashSet<>();
        }
        return dataCollectionNames;
    }

    public void setDataCollectionNames(Set<String> dataCollectionNames) {
        this.dataCollectionNames = dataCollectionNames;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    @JsonSerialize(using = TimestampSerializer.class)
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    @JsonSerialize(using = TimestampSerializer.class)
    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }
}
