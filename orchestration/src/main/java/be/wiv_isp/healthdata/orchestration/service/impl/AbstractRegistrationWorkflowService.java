/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.action.workflow.AbstractWorkflowAction;
import be.wiv_isp.healthdata.orchestration.action.workflow.executor.IRegistrationWorkflowActionExecutor;
import be.wiv_isp.healthdata.orchestration.action.workflow.executor.IWorkflowActionExecutor;
import be.wiv_isp.healthdata.orchestration.dao.IRegistrationWorkflowDao;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.domain.search.AbstractRegistrationWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import be.wiv_isp.healthdata.orchestration.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.List;

abstract public class AbstractRegistrationWorkflowService<Workflow extends AbstractRegistrationWorkflow<Document>, Search extends AbstractRegistrationWorkflowSearch, Document extends AbstractRegistrationDocument, ElasticSearchInfo extends AbstractElasticSearchInfo> extends AbstractWorkflowService<Workflow, Search, Document> implements IAbstractRegistrationWorkflowService<Workflow, Search> {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractRegistrationWorkflowService.class);

    @Autowired
    private IElasticSearchInfoService<ElasticSearchInfo> elasticSearchInfoService;

    @Autowired
    private IRegistrationWorkflowActionExecutor workflowActionExecutor;

    @Autowired
    protected IRegistrationWorkflowDao<Workflow, Search> workflowDao;

    @Autowired
    protected IDataCollectionDefinitionForwardService catalogueService;

    @Autowired
    private IAbstractRegistrationDocumentService<Document, ?> documentService;

    public AbstractRegistrationWorkflowService(Class<Workflow> clazz) {
        super(clazz);
    }

    @Override
    public void delete(List<Workflow> workflows, HDUserDetails principal) {
        final AbstractWorkflowAction deleteAction = (AbstractWorkflowAction) actionFactory.getActionByActionType(WorkflowActionType.DELETE, WorkflowType.REGISTRATION);
        for (Workflow workflow : workflows) {
            if (workflow != null) {
                workflowActionExecutor.execute(deleteAction, principal, workflow);
            }
        }
    }

    @Override
    @Transactional
    public void updateSendStatus(Long workflowId, String sendStatus) {
        if(workflowId == null) {
            return;
        }

        LOG.info("Registration[{}]: updating send status to {}", workflowId, sendStatus);

        final Workflow workflow = get(workflowId);
        workflow.setSendStatus(sendStatus);
        update(workflow);

        final ElasticSearchInfo elasticSearchInfo = elasticSearchInfoService.get(workflowId);
        elasticSearchInfo.setStatus(AbstractElasticSearchInfo.Status.NOT_INDEXED);
        elasticSearchInfoService.update(elasticSearchInfo);
    }

    @Override
    public List<String> getDataCollectionNames(Long organizationId) {
        return getDao().getDataCollectionNames(organizationId);
    }

    @Override
    protected WorkflowType getWorkflowType() {
        return WorkflowType.REGISTRATION;
    }

    @Override
    public IRegistrationWorkflowDao<Workflow, Search> getDao() {
        return workflowDao;
    }

    @Override
    protected IWorkflowActionExecutor getActionExecutor() {
        return workflowActionExecutor;
    }

    @Override
    protected IAbstractDocumentService<Document, ?> getDocumentService() {
        return documentService;
    }

    @Override
    public void setDocumentData(Workflow workflow, DocumentData documentData, List<DataSource> dataSources, boolean addHistoryEntry) {
        LOG.debug(MessageFormat.format("Setting document content for workflow [{0}]", workflow.getId().toString()));
        Document document =  workflow.getDocument();

        if (document == null) {
            LOG.debug("Document does not exist, creating new document");
            document = getDocumentService().getEntityInstance();
            document.setDocumentContent(documentData.getContent());
            document.setCodedContent(documentData.getCoded());
            if (dataSources != null)
                document.setDataSources(dataSources);
            document.setWorkflow(workflow);
            document.setVersion((long) workflow.getDocumentHistory().size());
            document = getDocumentService().create(document);
            workflow.setDocument(document);
            workflow.getDocumentHistory().add(document);
        } else if (addHistoryEntry) {
            LOG.debug("Document exists and new history entry must be added, creating new document");
            document = getDocumentService().getEntityInstance();
            document.setDocumentContent(documentData.getContent());
            document.setCodedContent(documentData.getCoded());
            if (dataSources != null)
                document.setDataSources(dataSources);
            document.setWorkflow(workflow);
            document.setVersion((long) workflow.getDocumentHistory().size());
            document = getDocumentService().create(document);
            workflow.setDocument(document);
            workflow.getDocumentHistory().add(document);
        } else {
            LOG.debug("Document exists and new history entry must not be added, updating existing document");
            document.setDocumentContent(documentData.getContent());
            document.setCodedContent(documentData.getCoded());
            if (dataSources != null)
                document.setDataSources(dataSources);
            getDocumentService().update(document);
        }
    }
}