/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.enumeration;

import be.wiv_isp.healthdata.common.domain.enumeration.Platform;

import java.util.ArrayList;
import java.util.List;

public enum WorkflowActionType implements ActionType {

	SAVE(Platform.HD4DP, null, WorkflowType.REGISTRATION, WorkflowType.PARTICIPATION, true, true),
	SUBMIT(Platform.HD4DP, null, WorkflowType.REGISTRATION, WorkflowType.PARTICIPATION, true, false),
	REOPEN(Platform.HD4DP, null, WorkflowType.REGISTRATION, WorkflowType.PARTICIPATION, true, false),
	UPDATE_FOR_CORRECTION(Platform.HD4DP, null, WorkflowType.REGISTRATION, WorkflowType.PARTICIPATION, true, false),

	CREATE_FOR_REVIEW(Platform.HD4RES, null, WorkflowType.REGISTRATION, WorkflowType.PARTICIPATION, true, true),
	SUBMIT_ANNOTATIONS(Platform.HD4RES, null, WorkflowType.REGISTRATION, WorkflowType.PARTICIPATION, true, false),
	RESUBMIT(Platform.HD4RES, null, WorkflowType.REGISTRATION, WorkflowType.PARTICIPATION, true, false),
	UPDATE_FOLLOW_UP_ACTION(Platform.HD4RES, null, WorkflowType.REGISTRATION, null, false, false),

	SEND(Platform.HD4DP, Platform.HD4RES, WorkflowType.REGISTRATION, WorkflowType.PARTICIPATION, true, false),
	DELETE(Platform.HD4DP, Platform.HD4RES, WorkflowType.REGISTRATION, null, true, false),
	UPDATE_STATUS(Platform.HD4DP, Platform.HD4RES, WorkflowType.REGISTRATION, WorkflowType.PARTICIPATION, true, false),
	START(Platform.HD4DP, Platform.HD4RES, WorkflowType.PARTICIPATION, null, true, true),
	STOP(Platform.HD4DP, Platform.HD4RES, WorkflowType.PARTICIPATION, null, true, false);

	private List<Platform> platforms = new ArrayList<>();
	private List<WorkflowType> workflowTypes = new ArrayList<>();
	private boolean initialAction;
	private boolean modifiesWorkflowState;

	WorkflowActionType(Platform platform1, Platform platform2, WorkflowType workflowType1, WorkflowType workflowType2, boolean modifiesWorkflowState, boolean initialAction) {
		if(platform1 != null) {
			platforms.add(platform1);
		}
		if(platform2 != null) {
			platforms.add(platform2);
		}
		if(workflowType1 != null) {
			workflowTypes.add(workflowType1);
		}
		if(workflowType2 != null) {
			workflowTypes.add(workflowType2);
		}
		this.modifiesWorkflowState = modifiesWorkflowState;
		this.initialAction = initialAction;
	}

	public boolean appliesTo(Platform platform) {
		return platforms.contains(platform);
	}

	public boolean appliesTo(WorkflowType workflowType) {
		return workflowTypes.contains(workflowType);
	}

	public boolean modifiesWorkflowState() {
		return modifiesWorkflowState;
	}

	public boolean isInitialAction() {
		return initialAction;
	}
}