/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.domain.mapper;

import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.dto.ConfigurationDto;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConfigurationDtoToConfigurationMapper implements Mapper<ConfigurationDto, Configuration> {

	private static final Logger LOG = LoggerFactory.getLogger(ConfigurationDtoToConfigurationMapper.class);

	@Autowired
	private IConfigurationService configurationService;

	@Override
	public Configuration map(ConfigurationDto configurationDto) {
		final Configuration configuration = new Configuration();
		configuration.setId(configurationDto.getId());
		configuration.setKey(configurationDto.getKey());

		if (configurationDto.getKey().isReadable()) {
			configuration.setValue(configurationDto.getValue());
			configuration.setDefaultValue(configurationDto.getDefaultValue());
		} else {
			LOG.debug("Configuration is not readable. Retrieving existing configuration from database.");
			final Configuration configurationInDb = configurationService.get(configurationDto.getId());
			if (configurationInDb == null) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "Configuration", configurationDto.getId());
				throw exception;
			}
			if (ConfigurationDto.UNREADABLE_VALUE.equals(configurationDto.getValue())) {
				LOG.debug("Value has not been modified, resetting value from database");
				configuration.setValue(configurationInDb.getValue());
			} else {
				LOG.debug("Value has been modified, keeping provided value");
				configuration.setValue(configurationDto.getValue());
			}
			configuration.setDefaultValue(configurationInDb.getDefaultValue());
		}

		configuration.setOrganization(configurationDto.getOrganization());
		configuration.setUpdatedOn(configurationDto.getUpdatedOn());
		configuration.setUseFromMain(configurationDto.getUseFromMain());
		return configuration;
	}
}
