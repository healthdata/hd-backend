/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.domain;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import java.util.Objects;

@JsonSerialize(include = Inclusion.NON_NULL)
public class Identification {

	private String type;
	private String value;
	private String applicationId;
	private String quality;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getApplicationId() {
		return applicationId == null ? StringUtils.EMPTY : applicationId ;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof Identification) {
			Identification other = (Identification) o;

			return Objects.equals(type, other.type) //
					&& Objects.equals(value, other.value) //
					&& Objects.equals(applicationId, other.applicationId) //
					&& Objects.equals(quality, other.quality); //
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash( //
				this.type, //
				this.value, //
				this.applicationId, //
				this.quality);
	}

	@Override
	public String toString() {
		return "Identification {" + //
				"type = " + Objects.toString(this.type) + ", " + //
				"value = " + Objects.toString(this.value) + ", " + //
				"quality = " + Objects.toString(this.quality) + ", " + //
				"applicationId = " + Objects.toString(this.applicationId) + "}";
	}
}
