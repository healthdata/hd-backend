/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.jaxb.json;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JsonUnmarshaller {

    static private final ObjectMapper mapper = new ObjectMapper().disable(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS);

    static public <T> T unmarshal(String content, Class<T> valueType) {
        try {
            return mapper.readValue(content, valueType);
        } catch (IOException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.JSON_PARSE_EXCEPTION, e.getMessage());
            throw exception;
        }
    }

    static public <T> List<T> unmarshal(JSONArray array, Class<T> valueType) {
        if (valueType.equals(Long.class)) {
            return (List<T>) unmarshalLong(array);
        } else if (valueType.equals(String.class)) {
            return (List<T>) unmarshalString(array);
        } else {
            final List<T> list = new ArrayList<>();
            for (int i = 0; i < array.length(); ++i) {
                final JSONObject object = JsonUtils.getJSONObject(array, i);
                list.add(unmarshal(object.toString(), valueType));
            }
            return list;
        }
    }

    static private List<Long> unmarshalLong(JSONArray array) {
        final List<Long> list = new ArrayList<>();
        for (int i = 0; i < array.length(); ++i) {
            list.add(JsonUtils.getLong(array, i));
        }
        return list;
    }

    static private List<String> unmarshalString(JSONArray array) {
        final List<String> list = new ArrayList<>();
        for (int i = 0; i < array.length(); ++i) {
            list.add(JsonUtils.getString(array, i));
        }
        return list;
    }
}
