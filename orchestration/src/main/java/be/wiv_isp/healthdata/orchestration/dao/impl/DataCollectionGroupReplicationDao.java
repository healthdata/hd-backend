/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.orchestration.dao.IDataCollectionGroupReplicationDao;
import be.wiv_isp.healthdata.orchestration.domain.DataCollectionGroupReplication;
import be.wiv_isp.healthdata.orchestration.domain.search.DataCollectionGroupReplicationSearch;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DataCollectionGroupReplicationDao extends CrudDaoV2<DataCollectionGroupReplication, Long, DataCollectionGroupReplicationSearch> implements IDataCollectionGroupReplicationDao {

    public DataCollectionGroupReplicationDao() {
        super(DataCollectionGroupReplication.class);
    }

    protected List<Predicate> getPredicates(DataCollectionGroupReplicationSearch search, CriteriaBuilder cb, Root<DataCollectionGroupReplication> rootEntry) {
        final List<Predicate> predicates = new ArrayList<>();
        if (search.getDataCollectionGroupName() != null) {
            predicates.add(cb.equal(rootEntry.get("dataCollectionGroupName"), search.getDataCollectionGroupName()));
        }
        if (search.getMajorVersion() != null) {
            predicates.add(cb.equal(rootEntry.get("majorVersion"), search.getMajorVersion()));
        }
        return predicates;
    }

}
