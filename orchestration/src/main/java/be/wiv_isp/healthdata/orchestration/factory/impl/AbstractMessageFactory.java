/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.factory.impl;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageType;
import be.wiv_isp.healthdata.orchestration.dto.EncryptionModuleMessage;
import be.wiv_isp.healthdata.orchestration.factory.IAbstractMessageFactory;
import be.wiv_isp.healthdata.orchestration.jaxb.json.JsonMarshaller;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IIdentificationService;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.Date;

public abstract class AbstractMessageFactory<RegistrationWorkflow extends AbstractRegistrationWorkflow, ParticipationWorkflow extends AbstractParticipationWorkflow> implements IAbstractMessageFactory<RegistrationWorkflow, ParticipationWorkflow> {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractMessageFactory.class);

	// TODO-messaging
	public static final String MESSAGING_VERSION = "messaging_version";
	public static final String TYPE = "type";
	public static final String SENT_ON = "sentOn";
	public static final String SENDER = "sender";
	public static final String LINKED_ID = "linkedId";
	public static final String ACTION = "action";
	public static final String IDENTIFICATION = "identification";
	public static final String ACKNOWLEDGED_MESSAGE_ID = "acknowledgedMessageId";
	public static final String V_2 = "v2";

	public static final Integer MAX_TRIALS = 5;

	@Autowired
	protected IConfigurationService configurationService;

	@Autowired
	protected IIdentificationService identificationService;

	@Override
	public Message createIncomingMessage(final EncryptionModuleMessage encryptionModuleMessage, Organization organization) {
		LOG.debug("Mapping message to domain object");

		final Message message = new Message();
		message.setContent(encryptionModuleMessage.getContent().getBytes(StandardCharsets.UTF_8));
		message.setMetadata(encryptionModuleMessage.getMetadata());
		message.setStatus(MessageStatus.INBOX);
		message.setOrganization(organization);

		final CSVContent csvContent = new CSVContent(encryptionModuleMessage.getContent());
		final JSONObject jsonObject = JsonUtils.createJsonObject(csvContent.getLastField());

		LOG.debug("Messaging version: {}", JsonUtils.getString(jsonObject, MESSAGING_VERSION));
		if(!jsonObject.isNull(LINKED_ID)) { // ACKNOWLEDGMENT message do not have linked_in attribute
			message.setLinkedId(JsonUtils.getLong(jsonObject, LINKED_ID));
		}

		final JSONObject senderJsonObject = JsonUtils.getJSONObject(jsonObject, SENDER);
		final MessageCorrespondent correspondent = new MessageCorrespondent();
		correspondent.setIdentificationType(JsonUtils.getString(senderJsonObject, "identificationType"));
		correspondent.setIdentificationValue(JsonUtils.getString(senderJsonObject, "identificationValue"));
		correspondent.setApplicationId(JsonUtils.getString(senderJsonObject, "applicationId"));

		message.setType(MessageType.valueOf(JsonUtils.getString(jsonObject, TYPE)));
		message.setCorrespondent(correspondent);
		message.setSentOn(new Timestamp(DateFormat.DATE_AND_TIME.parse(JsonUtils.getString(jsonObject, SENT_ON)).getTime()));
		return message;
	}

	@Override
	public EncryptionModuleMessage createEncryptionModuleMessage(Message message) {
		LOG.debug("Creating EncryptionModuleMessage from Message domain object");

		final CSVContent csvContent = new CSVContent(message.getContent());
		final JSONObject jsonObject = JsonUtils.createJsonObject(csvContent.getLastField());
		JsonUtils.put(jsonObject, MESSAGING_VERSION, V_2);
		JsonUtils.put(jsonObject, TYPE, message.getType());
		JsonUtils.put(jsonObject, SENT_ON, DateFormat.DATE_AND_TIME.format(new Date()));
		JsonUtils.put(jsonObject, SENDER, createSender(message));
		JsonUtils.put(jsonObject, LINKED_ID, message.getId());

		csvContent.setLastField(jsonObject.toString());
		return createEncryptionModuleMessage(message, csvContent);
	}

	@Override
	public EncryptionModuleMessage createAcknowledgment(Message message) {
		LOG.debug(MessageFormat.format("Creating acknowledgment message for message with linked id {0}", message));

		final CSVContent csvContent = new CSVContent(message.getContent());
		final JSONObject jsonObject = new JSONObject();
		JsonUtils.put(jsonObject, MESSAGING_VERSION, V_2);
		JsonUtils.put(jsonObject, TYPE, MessageType.ACKNOWLEDGMENT);
		JsonUtils.put(jsonObject, SENT_ON, DateFormat.DATE_AND_TIME.format(new Date()));
		JsonUtils.put(jsonObject, SENDER, createSender(message));
		JsonUtils.put(jsonObject, ACKNOWLEDGED_MESSAGE_ID, message.getLinkedId());

		csvContent.setLastField(jsonObject.toString());
		return createEncryptionModuleMessage(message, csvContent);
	}

	@Override
	public Long getAcknowledgedMessageId(Message message) {
		final CSVContent csvContent = new CSVContent(message.getContent());
		final JSONObject jsonObject = JsonUtils.createJsonObject(csvContent.getLastField());
		return JsonUtils.getLong(jsonObject, ACKNOWLEDGED_MESSAGE_ID);
	}

	protected abstract EncryptionModuleMessage createEncryptionModuleMessage(Message message, CSVContent csvContent);

	protected MessageCorrespondent createMessageCorrespondent(Identification addressee) {
		final MessageCorrespondent correspondent = new MessageCorrespondent();
		correspondent.setIdentificationType(addressee.getType());
		correspondent.setIdentificationValue(addressee.getValue());
		correspondent.setApplicationId(addressee.getApplicationId());
		return correspondent;
	}

	protected Identification createIdentification(MessageCorrespondent correspondent) {
		final Identification identification = new Identification();
		identification.setType(correspondent.getIdentificationType());
		identification.setValue(correspondent.getIdentificationValue());
		identification.setApplicationId(correspondent.getApplicationId());
		return identification;
	}

	private JSONObject createSender(Message message) {
		final Identification identification = identificationService.getEtkIdentification(message.getOrganization());
		final MessageCorrespondent correspondent = createMessageCorrespondent(identification);
		return JsonUtils.createJsonObject(JsonMarshaller.marshal(correspondent));
	}
}
