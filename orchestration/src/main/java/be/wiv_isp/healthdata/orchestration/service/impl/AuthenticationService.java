/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.mapper.BooleanConfigurationMapper;
import be.wiv_isp.healthdata.orchestration.domain.mapper.UserManagementTypeConfigurationMapper;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.AuthenticationType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.UserManagementType;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import be.wiv_isp.healthdata.orchestration.service.IUserService;
import be.wiv_isp.healthdata.orchestration.service.IAuthenticationService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationService implements IAuthenticationService {

    private static final Logger LOG = LoggerFactory.getLogger(AuthenticationService.class);

    @Autowired
    private IOrganizationService organizationService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IConfigurationService configurationService;

    @Override
    public boolean supports(Long organizationId, String username, AuthenticationType supportedAuthenticationType) {
        try {
            final Organization organization = organizationService.getOrganizationOrMain(organizationId);
            final UserManagementType userManagementType = UserManagementTypeConfigurationMapper.map(configurationService.get(ConfigurationKey.USER_MANAGEMENT_TYPE, organization));
            final AuthenticationType authenticationType = userManagementType.getAuthenticationType();

            UserSearch search = new UserSearch();
            search.setUsername(username);
            search.setOrganization(organization);
            User unique = userService.getUnique(search);

            if(unique == null) {
                return supportedAuthenticationType.isSupportsDatabaseAuthentication();
            }
            if(unique.isSuperAdmin()) {
                if(BooleanConfigurationMapper.map(configurationService.get(ConfigurationKey.SUPER_ADMIN_ENABLED))) {
                    return supportedAuthenticationType.isSupportsDatabaseAuthentication();
                }
            }
            if(unique.isSupport()) {
                if(BooleanConfigurationMapper.map(configurationService.get(ConfigurationKey.SUPPORT_ENABLED))) {
                    return supportedAuthenticationType.isSupportsDatabaseAuthentication();
                }
            }
            return supportedAuthenticationType.equals(authenticationType);

        } catch (HealthDataException e) {
            LOG.error(e.getMessage(), e);
            return false;
        }
    }

    static public boolean isAuthenticated() {
        final Authentication authentication = getAuthentication();

        if(authentication == null) {
            return false;
        }

        return !(authentication instanceof AnonymousAuthenticationToken);
    }

    static public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    static public Organization getAuthenticatedOrganization() {
        return getUserDetails().getOrganization();
    }

    static public HDUserDetails getUserDetails() {
        if(isAuthenticated()) {
            return (HDUserDetails) getAuthentication().getPrincipal();
        }
        HealthDataException exception = new HealthDataException();
        exception.setExceptionType(ExceptionType.USER_NOT_AUTHENTICATED);
        throw exception;
    }
}
