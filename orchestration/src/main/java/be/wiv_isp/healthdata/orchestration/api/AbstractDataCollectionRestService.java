/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.common.api.ClientVersion;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.orchestration.annotation.Auditable;
import be.wiv_isp.healthdata.orchestration.api.mapper.OrganizationRequestMapper;
import be.wiv_isp.healthdata.orchestration.api.uri.DataCollectionUri;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ApiType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionService;
import be.wiv_isp.healthdata.orchestration.service.IUserDataCollectionService;
import be.wiv_isp.healthdata.orchestration.service.IWebServiceClientService;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class AbstractDataCollectionRestService {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractDataCollectionRestService.class);

	@Autowired
	protected IUserDataCollectionService userDataCollectionService;
	@Autowired
	protected IDataCollectionService dataCollectionService;
	@Autowired
	protected IWebServiceClientService webServiceClientService;
	@Autowired
	protected IConfigurationService configurationService;

	@GET
	@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {
		LOG.info("GET Request: Get all data collections");
		final Map<String,Set<String>> result = new HashMap<>();

		final HDUserDetails principal = AuthenticationService.getUserDetails();

		Map<String,Set<String>> allDataCollections = dataCollectionService.getDataCollections(principal.getOrganization());

		if (principal.isSupport() || principal.isAdmin()) {
			result.putAll(allDataCollections);
		} else if (principal.isUser()) {
			final Set<String> userDataCollections = userDataCollectionService.get(principal);

			for (String userDataCollection : userDataCollections) {
				final String dataCollectionName = getDataCollectionName(allDataCollections.keySet(), userDataCollection);
				if (dataCollectionName != null) {
					result.put(dataCollectionName, new HashSet<>(allDataCollections.get(dataCollectionName)));
				}
			}
		}

		return Response.ok(result).build();
	}

	@GET
	@Path("/{groupName}")
	@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
	@Auditable(apiType = ApiType.DATA_COLLECTION)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMembers(@PathParam("groupName") String groupName) {
		LOG.info("GET Request: Get data collections for group [{}]", groupName);
		Set<String> members = dataCollectionService.getMembers(groupName);
		if (members == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "data collection group", groupName);
			throw exception;
		}

		return Response.ok(members).build();
	}

	private String getDataCollectionName(final Set<String> allDataCollections, final String dataCollection) {
		if (dataCollection == null) {
			return null;
		}
		for (String dc : allDataCollections) {
			if (dataCollection.equalsIgnoreCase(dc))
				return dataCollection;
		}

		return null;
	}

	@POST
	@Path("/organization")
	@Auditable(apiType = ApiType.DATA_COLLECTION)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllFromOrganization(String json) {
		LOG.info("GET Request: Get all data collections for organization [{}]", json);
		Organization organization;
		try {
			organization = OrganizationRequestMapper.convert(new JSONObject(json));
		} catch (JSONException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.JSON_PARSE_EXCEPTION);
			throw exception;
		}

		final Map<String, Set<String>> result = dataCollectionService.getDataCollections(organization);
		return Response.ok(result).build();
	}

	@GET
	@Auditable(apiType = ApiType.DATA_COLLECTION)
	@Path("/status")
	public Response status() {
		LOG.info("GET Request: Get status information for data collections REST service.");

		Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

		final DataCollectionUri dataCollectionUri = new DataCollectionUri();
		dataCollectionUri.setHost(catalogueHost.getValue());

		WebServiceBuilder wsb = new WebServiceBuilder();
		wsb.addHeader(HttpHeaders.CLIENT_VERSION, ClientVersion.DataCollection.getDefault());
		wsb.setUrl(dataCollectionUri + "/status");
		wsb.setGet(true);
		wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb.setReturnType(new GenericType<ClientResponse>() {
		});

		ClientResponse response = (ClientResponse) webServiceClientService.callWebService(wsb);

		if (response.getStatus() == Response.Status.NO_CONTENT.getStatusCode()) {
			return Response.noContent().build();
		} else {
			return Response.serverError().build();
		}
	}
}
