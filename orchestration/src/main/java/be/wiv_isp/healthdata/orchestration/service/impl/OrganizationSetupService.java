/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.dto.SalesForceOrganizationDto;
import be.wiv_isp.healthdata.orchestration.domain.AbstractElasticSearchInfo;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.search.ElasticSearchInfoSearch;
import be.wiv_isp.healthdata.orchestration.domain.search.OrganizationSearch;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import be.wiv_isp.healthdata.orchestration.service.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class OrganizationSetupService implements IOrganizationSetupService {

	private static final Logger LOG = LoggerFactory.getLogger(OrganizationSetupService.class);

	@Autowired
	private IOrganizationService organizationService;
	@Autowired
	private ISalesForceOrganizationService salesForceOrganizationService;
	@Autowired
	private IUserService userService;
	@Autowired
	private IElasticSearchInfoService elasticSearchInfoService;
	@Autowired
	private IConfigurationService configurationService;

	@Override
	@Transactional
	public void updateOrganizations() {
		LOG.info("Updating local organizations");
		final Organization mainOrganization = checkMainOrganization();

		if (isValidIdentification(mainOrganization)) {
			final String mainIdType = mainOrganization.getHealthDataIDType();
			final String mainIdValue = mainOrganization.getHealthDataIDValue();
			final SalesForceOrganizationDto sfMainOrganization = salesForceOrganizationService.getSalesForceOrganization(mainIdType, mainIdValue);
			final List<SalesForceOrganizationDto> sfSubOrganizations = salesForceOrganizationService.getSalesForceSubOrganizations(mainIdType, mainIdValue);
			synchronizeLocalOrganizations(sfMainOrganization, sfSubOrganizations);
			configurationService.createMissing(); // update configurations
		} else {
			LOG.warn("Main organization is not properly configured. Please fill in the {} and {} configurations of the main organization.", ConfigurationKey.HEALTHDATA_ID_TYPE, ConfigurationKey.HEALTHDATA_ID_VALUE);
		}

		LOG.info("Local organizations successfully updated");
	}

	private Organization checkMainOrganization() {
		LOG.debug("Verifying main organization is present");

		final Organization main = organizationService.getMain();

		if(main != null) {
			LOG.debug("Main organization is present");
			return main;
		}

		LOG.info("No main organization found. This is a new installation.");
		final Organization created = createMainOrganization();
		createAdmin(created);
		return created;
	}

	private Organization createMainOrganization() {
		LOG.info("Creating main organization");
		final Organization main = new Organization();
		main.setMain(true);
		main.setDeleted(false);
		main.setHealthDataIDType(null);
		main.setHealthDataIDValue(null);
		main.setName("Main organization not configured");
		main.setHd4prc(false);

		final Organization created = organizationService.create(main);
		LOG.info("Main organization has been created: {}", created);
		return created;
	}

	private void synchronizeLocalOrganizations(SalesForceOrganizationDto sfMainOrganization, List<SalesForceOrganizationDto> sfSubOrganizations) {
		// update local main organization
		updateLocalOrganization(organizationService.getMain(), sfMainOrganization);

		// update local sub organizations
		final OrganizationSearch search = new OrganizationSearch();
		search.setMain(false);
		search.setDeleted(null);
		final List<Organization> dbSubOrganizations = organizationService.getAll(search);

		for (final SalesForceOrganizationDto sfSubOrganization : sfSubOrganizations) {
			if (!isValidIdentification(sfSubOrganization)) {
				LOG.warn("Ignoring organization with invalid identification: [{}]", sfSubOrganization);
				continue;
			}

			final Organization dbSubOrganization = find(dbSubOrganizations, sfSubOrganization);

			if(dbSubOrganization == null) {
				createLocalOrganization(sfSubOrganization);
			} else if(dbSubOrganization.isDeleted()) {
				reactiveLocalOrganization(dbSubOrganization, sfSubOrganization);
			} else {
				updateLocalOrganization(dbSubOrganization, sfSubOrganization);
			}
		}

		for (final Organization dbSubOrganization : dbSubOrganizations) {
			if(!dbSubOrganization.isDeleted() && find(sfSubOrganizations, dbSubOrganization) == null) {
				deactivateLocalOrganization(dbSubOrganization);
			}
		}
	}

	private void createLocalOrganization(SalesForceOrganizationDto salesForceOrganization) {
		LOG.info("Creating local organization [{}]", salesForceOrganization);
		Organization organization = new Organization();
		organization.setHealthDataIDType(salesForceOrganization.getIdentificationType());
		organization.setHealthDataIDValue(salesForceOrganization.getIdentificationValue());
		organization.setName(salesForceOrganization.getName());
		organization.setHd4prc(salesForceOrganization.isHd4prc());
		organization.setMain(false);
		organization.setDeleted(false);
		organization = organizationService.create(organization);

		createAdmin(organization);
		createSupport(organization);
	}

	private void updateLocalOrganization(Organization organization, SalesForceOrganizationDto salesForceOrganization) {
		if (salesForceOrganization.getName() != null && !salesForceOrganization.getName().equals(organization.getName())) {
			LOG.info("Updating organization name from [{}] to [{}]", organization.getName(), salesForceOrganization.getName());
			organization.setName(salesForceOrganization.getName());
			organizationService.update(organization);
		}

		if (organization.isHd4prc() != salesForceOrganization.isHd4prc()) {
			if(salesForceOrganization.isHd4prc()) {
				LOG.info("Marking organization [{}] as HD4PRC installation", organization.getName());
			} else {
				LOG.info("Marking organization [{}] as non-HD4PRC installation", organization.getName());
			}
			organization.setHd4prc(salesForceOrganization.isHd4prc());
			organizationService.update(organization);
		}

		checkAdmin(organization);
		checkSupport(organization);
	}

	private void reactiveLocalOrganization(Organization organization, SalesForceOrganizationDto salesForceOrganization) {
		LOG.info("Reactivating local organization [{}]", salesForceOrganization);
		organization.setName(salesForceOrganization.getName());
		organization.setDeleted(false);
		organizationService.update(organization);

		checkAdmin(organization);
		checkSupport(organization);

		final ElasticSearchInfoSearch search = new ElasticSearchInfoSearch();
		search.setOrganizationId(organization.getId());
		elasticSearchInfoService.updateAll(search, AbstractElasticSearchInfo.Status.NOT_INDEXED);
	}

	private void deactivateLocalOrganization(Organization organization) {
		LOG.info("Deactivating local organization {}", organization);
		organizationService.delete(organization);

		final ElasticSearchInfoSearch search = new ElasticSearchInfoSearch();
		search.setOrganizationId(organization.getId());
		elasticSearchInfoService.updateAll(search, AbstractElasticSearchInfo.Status.DELETED);
	}

	private void checkAdmin(Organization organization) {
		LOG.debug("Check admin for organization {}", organization);

		final UserSearch search = new UserSearch();
		search.setUsername(User.ADMIN);
		search.setOrganization(organization);

		final User user = userService.getUnique(search);
		if(user == null) {
			createAdmin(organization);
		} else if(!user.isSuperAdmin()) {
			user.addAuthority(new Authority(Authority.SUPER_ADMIN));
			userService.update(user);
		}
	}

	private void createAdmin(Organization organization) {
		LOG.info("Creating admin for organization {}", organization);

		final User user = new User();
		user.setUsername(User.ADMIN);
		user.setEnabled(true);
		user.setEmail("noreply-admin@healthdata.be");
		user.addAuthority(new Authority(Authority.ADMIN));
		user.addAuthority(new Authority(Authority.SUPER_ADMIN));
		user.setOrganization(organization);

		if(organization.isMain()) {
			user.setPassword("admin_password");
		} else {
			user.setPassword(organization.getHealthDataIDValue());
		}

		userService.create(user, false);
	}

	private void checkSupport(Organization organization) {
		checkSupportAdmin(organization);
		checkSupportUser(organization);
	}

	private void checkSupportAdmin(Organization organization) {
		LOG.debug("Check support admin for organization {}", organization);
		final UserSearch search = new UserSearch();
		search.setUsername(User.SUPPORT_ADMIN);
		search.setOrganization(organization);
		final User user = userService.getUnique(search);
		if(user == null) {
			createSupportAdmin(organization);
		}
	}

	private void checkSupportUser(Organization organization) {
		LOG.debug("Check support user for organization {}", organization);
		final UserSearch search = new UserSearch();
		search.setUsername(User.SUPPORT_USER);
		search.setOrganization(organization);
		final User user = userService.getUnique(search);
		if(user == null) {
			createSupportUser(organization);
		}
	}

	private void createSupport(Organization organization) {
		createSupportAdmin(organization);
		createSupportUser(organization);
	}

	private void createSupportAdmin(Organization organization) {
		LOG.info("Creating support admin for organization {}", organization);

		final User admin = new User();
		admin.setUsername(User.SUPPORT_ADMIN);
		admin.setEnabled(true);
		admin.setEmail("noreply-support-admin@healthdata.be");
		admin.addAuthority(new Authority(Authority.ADMIN));
		admin.addAuthority(new Authority(Authority.SUPPORT));
		admin.setOrganization(organization);
		admin.setPassword(organization.getHealthDataIDValue());
		userService.create(admin, false);
	}

	private void createSupportUser(Organization organization) {
		LOG.info("Creating support user for organization {}", organization);

		final User user = new User();
		user.setUsername(User.SUPPORT_USER);
		user.setEnabled(true);
		user.setEmail("noreply-support-user@healthdata.be");
		user.addAuthority(new Authority(Authority.USER));
		user.addAuthority(new Authority(Authority.SUPPORT));
		user.setOrganization(organization);
		user.setPassword(organization.getHealthDataIDValue());
		userService.create(user, false);
	}

	private boolean isValidIdentification(Organization organization) {
		return isValidIdentification(organization.getHealthDataIDType(), organization.getHealthDataIDType());
	}

	private boolean isValidIdentification(SalesForceOrganizationDto organization) {
		return isValidIdentification(organization.getIdentificationType(), organization.getIdentificationValue());
	}

	private boolean isValidIdentification(String identificationType, String identificationValue) {
		if (StringUtils.isAnyBlank(identificationType, identificationValue))
			return false;
		if ("NULL".equalsIgnoreCase(identificationType))
			return false;
		if ("NULL".equalsIgnoreCase(identificationValue))
			return false;
		return true;
	}

	private SalesForceOrganizationDto find(List<SalesForceOrganizationDto> dtos, Organization organization) {
		for (SalesForceOrganizationDto dto : dtos) {
			if(equals(organization, dto)) {
				return dto;
			}
		}
		return null;
	}

	private Organization find(List<Organization> organizations, SalesForceOrganizationDto dto) {
		for (Organization organization : organizations) {
			if(equals(organization, dto)) {
				return organization;
			}
		}
		return null;
	}

	private boolean equals(final Organization organization, final SalesForceOrganizationDto salesForceOrganization) {
		return salesForceOrganization.getIdentificationType().equals(organization.getHealthDataIDType()) && salesForceOrganization.getIdentificationValue().equals(organization.getHealthDataIDValue());
	}
}