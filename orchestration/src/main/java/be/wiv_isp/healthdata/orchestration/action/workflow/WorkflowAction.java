/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.action.workflow;

import be.wiv_isp.healthdata.orchestration.action.Action;
import be.wiv_isp.healthdata.orchestration.domain.AbstractWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;

import java.util.Map;

public interface WorkflowAction<Workflow extends AbstractWorkflow> extends Action<WorkflowActionType> {

	Workflow preExecute(Workflow workflow, HDUserDetails userDetails);

	Workflow execute(Workflow workflow, HDUserDetails userDetails);

	Workflow postExecute(Workflow workflow, HDUserDetails userDetails);

	Workflow retrieveWorkflow();

	WorkflowType getWorkflowType();

	WorkflowStatus getEndStatus();

	String getSendStatus();

	Map<String, String> getMetaData();

	String getUpdatedOn();
}
