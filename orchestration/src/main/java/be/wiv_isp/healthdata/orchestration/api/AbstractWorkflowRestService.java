/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.orchestration.annotation.Auditable;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ApiType;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.api.mapper.IAbstractDocumentMapper;
import be.wiv_isp.healthdata.orchestration.api.mapper.IAbstractWorkflowMapper;
import be.wiv_isp.healthdata.orchestration.domain.AbstractDocument;
import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.AbstractWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.domain.search.AbstractWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.dto.AbstractWorkflowDto;
import be.wiv_isp.healthdata.orchestration.factory.IActionFactory;
import be.wiv_isp.healthdata.orchestration.service.IAbstractWorkflowService;
import be.wiv_isp.healthdata.orchestration.service.INoteMigrationService;
import be.wiv_isp.healthdata.orchestration.service.INoteService;
import be.wiv_isp.healthdata.orchestration.service.IUserDataCollectionService;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

abstract public class AbstractWorkflowRestService<Workflow extends AbstractWorkflow, Search extends AbstractWorkflowSearch, FullDto extends AbstractWorkflowDto, Dto extends AbstractWorkflowDto, Document extends AbstractDocument> {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractWorkflowRestService.class);

	@Autowired
	protected IActionFactory actionFactory;
	@Autowired
	protected IAbstractWorkflowMapper<Workflow, Dto> workflowMapper;
	@Autowired(required = false)
	protected IAbstractDocumentMapper<Document> documentMapper;

	@Autowired
	protected IUserDataCollectionService userDataCollectionService;
	@Autowired
	protected INoteService noteService;
	@Autowired
	private INoteMigrationService noteMigrationService;

	@GET
	@PreAuthorize("hasRole('ROLE_USER')")
	@Auditable(apiType = ApiType.WORKFLOW)
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("id") Long id, @QueryParam("fullSearch") String fullSearch, @QueryParam("returnDeleted") String returnDeleted) {
		LOG.info("GET Request: {} with id [{}]", getWorkflowType().getLabel(), id);
		Workflow workflow = getWorkflowService().get(id);
		if (workflow == null || (ignoreDeletedWorkflows() && WorkflowStatus.DELETED.equals(workflow.getStatus()) && (StringUtils.isBlank(returnDeleted) || !Boolean.valueOf(returnDeleted)))) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "WORKFLOW", id);
			throw exception;
		}
		final HDUserDetails principal = AuthenticationService.getUserDetails();

		if (!userDataCollectionService.isUserAuthorized(principal, workflow)) {
			return Response.status(Response.Status.FORBIDDEN).build();
		}

		if (WorkflowType.REGISTRATION.equals(workflow.getWorkflowType())) {
			if (!noteMigrationService.isMigrated((AbstractRegistrationWorkflow) workflow)) {
				noteMigrationService.migrateCommentsToNotes((AbstractRegistrationWorkflow) workflow);
			}
		}

		if (Boolean.valueOf(fullSearch)) {
			return Response.ok(getFullDtoInstance(workflowMapper.convert(workflow))).build();
		}

		return Response.ok(workflowMapper.convert(workflow)).build();
	}

	@GET
	@PreAuthorize("hasRole('ROLE_USER')")
	@Auditable(apiType = ApiType.WORKFLOW)
	@Path("/{id}/documents")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDocuments(@PathParam("id") Long id) {
		LOG.info("GET Request: Get document for {} with id [{}]", getWorkflowType().getLabel(), id);
		Workflow workflow = getWorkflowService().get(id);
		if (workflow == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "WORKFLOW", id);
			throw exception;
		}
		if (ignoreDeletedWorkflows() && workflow.getStatus().equals(WorkflowStatus.DELETED)) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "WORKFLOW", id);
			throw exception;
		}
		final HDUserDetails principal = AuthenticationService.getUserDetails();

		if (!userDataCollectionService.isUserAuthorized(principal, workflow)) {
			return Response.status(Response.Status.FORBIDDEN).build();
		}

		if (WorkflowType.REGISTRATION.equals(workflow.getWorkflowType())) {
			if (!noteMigrationService.isMigrated((AbstractRegistrationWorkflow) workflow)) {
				noteMigrationService.migrateCommentsToNotes((AbstractRegistrationWorkflow) workflow);
			}
		}

		Document document = (Document) workflow.getDocument();
		if (document == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "DOCUMENT", id);
			throw exception;
		}
		return Response.ok(documentMapper.convert(document)).build();
	}

	@GET
	@Path("/status")
	public Response status() {
		return Response.noContent().build();
	}

	protected abstract FullDto getFullDtoInstance(Dto dto);

	protected abstract IAbstractWorkflowService<Workflow, Search> getWorkflowService();

	protected abstract boolean ignoreDeletedWorkflows();

	protected abstract WorkflowType getWorkflowType();
}
