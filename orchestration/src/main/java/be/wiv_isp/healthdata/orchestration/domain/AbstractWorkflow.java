/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@MappedSuperclass
abstract public class AbstractWorkflow<Document extends AbstractDocument, WorkflowHistory extends AbstractWorkflowHistory> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "WORKFLOW_ID")
	protected Long id;

	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	protected WorkflowStatus status;

	@Fetch(FetchMode.JOIN)
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "DOCUMENT_ID")
	protected Document document;

	@Fetch(FetchMode.SUBSELECT)
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinColumn(name = "WORKFLOW_ID")
	@OrderColumn(name = "VERSION")
	protected List<Document> documentHistory;

	@Column(name = "DATA_COLLECTION_NAME")
	protected String dataCollectionName;

	@Column(name = "DATA_COLLECTION_DEFINITION_ID")
	protected Long dataCollectionDefinitionId;

	@JsonSerialize(using = TimestampSerializer.class) // TODO: should be removed as we use DTO object when exchanging object with other components
	@JsonDeserialize(using = TimestampDeserializer.class) // TODO: should be removed as we use DTO object when exchanging object with other components
	@Column(name = "CREATED_ON", nullable = false)
	protected Timestamp createdOn;

	@JsonSerialize(using = TimestampSerializer.class) // TODO: should be removed as we use DTO object when exchanging object with other components
	@JsonDeserialize(using = TimestampDeserializer.class) // TODO: should be removed as we use DTO object when exchanging object with other components
	@Column(name = "UPDATED_ON", nullable = false)
	protected Timestamp updatedOn;

	@Fetch(FetchMode.SUBSELECT)
	@OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
	@JoinColumn(name = "WORKFLOW_ID")
	protected List<WorkflowHistory> history;

	@Column(name = "SEND_STATUS")
	protected String sendStatus;

	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ORGANIZATION_ID")
	protected Organization organization;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public WorkflowStatus getStatus() {
		return status;
	}

	public void setStatus(WorkflowStatus status) {
		this.status = status;
	}

	public String getDataCollectionName() {
		return dataCollectionName;
	}

	public void setDataCollectionName(String dataCollectionName) {
		this.dataCollectionName = dataCollectionName;
	}

	public Long getDataCollectionDefinitionId() {
		return dataCollectionDefinitionId;
	}

	public void setDataCollectionDefinitionId(Long dataCollectionDefinitionId) {
		this.dataCollectionDefinitionId = dataCollectionDefinitionId;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	@Transactional
	public List<Document> getDocumentHistory() {
		if(documentHistory == null) {
			documentHistory = new ArrayList<>();
		}
		return documentHistory;
	}

	public void setDocumentHistory(List<Document> documentHistory) {
		this.documentHistory = documentHistory;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	@PrePersist
	public void onCreate() {
		Timestamp createdOnTimestamp = new Timestamp(new Date().getTime());
		setCreatedOn(createdOnTimestamp);
		setUpdatedOn(createdOnTimestamp);
	}

	@PreUpdate
	public void onUpdate() {
		setUpdatedOn(new Timestamp(new Date().getTime()));
	}

	public List<WorkflowHistory> getHistory() {
		return history;
	}

	public void setHistory(List<WorkflowHistory> history) {
		this.history = history;
	}

	public boolean addHistory(WorkflowHistory workflowHistory) {
		if (history == null) {
			history = new ArrayList<>();
		}
		return history.add(workflowHistory);
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public String getSendStatus() {
		return sendStatus;
	}

	public void setSendStatus(String sendStatus) {
		this.sendStatus = sendStatus;
	}

	public abstract WorkflowType getWorkflowType();

	@Deprecated
	public class MetaData {
		public static final String PATIENT_IDENTIFIER = "PATIENT_IDENTIFIER";
		public static final String CODED_PATIENT_IDENTIFIER = "CODED_PATIENT_IDENTIFIER";
		public static final String OLD_CODED_PATIENT_IDENTIFIER = "OLD_CODED_PATIENT_IDENTIFIER";
	}

	public class CodedDocumentData {
		public static final String PATIENT_ID = "patient_id";
	}
}