/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.util;

import be.wiv_isp.healthdata.orchestration.domain.PeriodExpression;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormat;
import org.springframework.scheduling.support.CronSequenceGenerator;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    public static String getPrettyPrintedPeriod(final Date start, final Date end, boolean dayLevel) {
        final Calendar startC = createCalendar(start);
        final Calendar endC = createCalendar(end);

        if(dayLevel) {
            removeTime(startC);
            removeTime(endC);
        }

        final long millis = endC.getTimeInMillis() - startC.getTimeInMillis();
        final Period period = new Duration(millis).toPeriodFrom(new DateTime(start));
        return PeriodFormat.getDefault().print(period);
    }

    public static void removeTime(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    public static Date add(final Date date, final PeriodExpression periodExpression) {
        if(periodExpression == null) {
            return date;
        }
        final Calendar calendar = createCalendar(date);
        final int amount = periodExpression.getAmount();
        final int unit = periodExpression.getUnit().getCalendarUnit();
        calendar.add(unit, amount);
        return calendar.getTime();
    }

    public static Date remove(final Date date, final PeriodExpression periodExpression) {
        final PeriodExpression negativePeriodExpression = new PeriodExpression(-periodExpression.getAmount(), periodExpression.getUnit());
        return add(date, negativePeriodExpression);
    }

    private static Calendar createCalendar(final Date date) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    public static Date getNextDate(final String cronExpression) {
        return new CronSequenceGenerator(cronExpression).next(new Date());
    }

    public static Timestamp currentTimestamp() {
        return new Timestamp(new Date().getTime());
    }
}
