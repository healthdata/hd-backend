/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.sql.Timestamp;

@MappedSuperclass
abstract public class AbstractParticipationWorkflow<Document extends AbstractParticipationDocument> extends AbstractWorkflow<Document, ParticipationWorkflowHistory> {

	@Column(name = "COMPLETED")
	protected Boolean completed;

	@Column(name = "COMPLETED_ON")
	protected Timestamp completedOn;

	public Boolean getCompleted() {
		return completed;
	}

	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}

	public Timestamp getCompletedOn() {
		return completedOn;
	}

	public void setCompletedOn(Timestamp completedOn) {
		this.completedOn = completedOn;
	}

	@Override
	public WorkflowType getWorkflowType() {
		return WorkflowType.PARTICIPATION;
	}
}