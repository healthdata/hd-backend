/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.tasks;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.orchestration.domain.Task;
import be.wiv_isp.healthdata.orchestration.domain.search.TaskSearch;
import be.wiv_isp.healthdata.orchestration.service.ITaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.*;

public abstract class AbstractTaskExecutorTask extends SchedulableHealthDataTask {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractTaskExecutorTask.class);

    @Autowired
    private ITaskService taskService;

    protected Map<String, HealthDataTask> executableTasks;

    @Override
    public String getName() {
        return "TASK_EXECUTOR";
    }

    @Override
    protected String getTimingExpression() {
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.MINUTE, 2); // execute 2 minutes after application startup
        return DateFormat.DATE_AND_TIME.format(cal.getTime());
    }

    @Override
    public void execute() {
        LOG.info("Collecting list of executable tasks");
        final List<HealthDataTask> executableTasksList = getExecutableTasks();

        executableTasks = new HashMap<>();
        for (HealthDataTask executableTask : executableTasksList) {
            executableTasks.put(executableTask.getName(), executableTask);
        }

        final TaskSearch search = new TaskSearch();
        search.setStatus(Task.Status.NOT_EXECUTED);
        final List<Task> tasks = taskService.getAll(search);

        for(Task task : tasks) {
            final HealthDataTask executableTask = executableTasks.get(task.getName());
            if(executableTask == null) {
                LOG.error("Task [{}] was not initialized", task.getName());
            } else {
                executableTask.run();

                if(!executableTask.hasException()) {
                    task.setStatus(Task.Status.SUCCESS);
                } else {
                    LOG.error(MessageFormat.format("Setting status of task [{0}] to FAILURE.", task.getName().toLowerCase()), executableTask.getException());
                    task.setStatus(Task.Status.FAILURE);
                }

                task.setExecutedOn(new Timestamp(new Date().getTime()));
                taskService.update(task);
            }
        }
    }

    protected abstract List<HealthDataTask> getExecutableTasks();
}
