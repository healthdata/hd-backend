/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.orchestration.dao.INoteDao;
import be.wiv_isp.healthdata.orchestration.domain.Note;
import be.wiv_isp.healthdata.orchestration.domain.search.NoteSearch;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class NoteDao extends CrudDaoV2<Note, Long, NoteSearch> implements INoteDao {

    public NoteDao() {  super(Note.class); }

    @Override
    protected List<Predicate> getPredicates(NoteSearch search, CriteriaBuilder cb, Root<Note> rootEntry) {
        final List<Predicate> predicates = new ArrayList<>();

        if(search.getDocumentId() != null) {
            predicates.add(cb.equal(rootEntry.get("documentId"), search.getDocumentId()));
        }
        if(search.getWorkflowType() != null) {
            predicates.add(cb.equal(rootEntry.get("workflowType"), search.getWorkflowType()));
        }
        if(search.getOrder() != null) {
            predicates.add(cb.equal(rootEntry.get("order"), search.getDocumentId()));
        }
        if(StringUtils.isNoneBlank(search.getFieldId())) {
            predicates.add(cb.equal(rootEntry.get("fieldId"), search.getFieldId()));
        }
        if(StringUtils.isNoneBlank(search.getFieldPath())) {
            predicates.add(cb.equal(rootEntry.get("fieldPath"), search.getFieldId()));
        }
        if (search.getDeleted() != null) {
            predicates.add(cb.equal(rootEntry.get("deleted"), search.getDeleted()));
        }
        return predicates;
    }
}
