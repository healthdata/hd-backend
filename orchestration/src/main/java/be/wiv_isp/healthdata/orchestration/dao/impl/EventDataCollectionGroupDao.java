/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *     
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.orchestration.dao.IEventDataCollectionGroupDao;
import be.wiv_isp.healthdata.orchestration.domain.EventDataCollectionGroup;
import be.wiv_isp.healthdata.orchestration.domain.search.EventDataCollectionGroupSearch;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class EventDataCollectionGroupDao extends AbstractEventDao<EventDataCollectionGroup, EventDataCollectionGroupSearch> implements IEventDataCollectionGroupDao {

    public EventDataCollectionGroupDao() {
        super(EventDataCollectionGroup.class);
    }

    @Override
    protected List<Predicate> getPredicates(EventDataCollectionGroupSearch search, CriteriaBuilder cb, Root<EventDataCollectionGroup> rootEntry) {
        final List<Predicate> predicates = super.getPredicates(search, cb, rootEntry);

        if (search.getDataCollectionGroupName() != null) {
            predicates.add(cb.equal(rootEntry.get("dataCollectionGroupName"), search.getDataCollectionGroupName()));
        }
        if (search.getMajorVersion() != null) {
            predicates.add(cb.equal(rootEntry.get("majorVersion"), search.getMajorVersion()));
        }

        return predicates;
    }

}
