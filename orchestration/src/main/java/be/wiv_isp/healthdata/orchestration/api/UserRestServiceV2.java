/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Component
@Path("/organizations/{organizationId}/users")
public class UserRestServiceV2 extends UserRestService {

    @Autowired
    private IOrganizationService organizationService;

    @POST
    @Path("/forgotPassword")
    public Response forgotPassword(@Context UriInfo info) {
        final String email = RestUtils.getParameterString(info, "email");
        final Long organizationId = RestUtils.getPathLong(info, "organizationId");
        final Organization organization = organizationService.get(organizationId);

        userManagementService.getUserManagementStrategy().sendPasswordResetInstructions(organization, email);
        return Response.ok().build();
    }

    @POST
    @Path("/resetPassword")
    public Response resetPassword(@Context UriInfo info) {
        final String token = RestUtils.getParameterString(info, "token");
        final Long organizationId = RestUtils.getPathLong(info, "organizationId");
        final String newPassword = RestUtils.getParameterString(info, "newPassword");
        final Organization organization = organizationService.get(organizationId);
        userManagementService.getUserManagementStrategy().resetPassword(organization, token, newPassword);
        return Response.ok().build();
    }

}
