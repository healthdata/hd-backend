/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.domain.mapper;

import be.wiv_isp.healthdata.orchestration.domain.Comment;
import be.wiv_isp.healthdata.orchestration.domain.Note;
import be.wiv_isp.healthdata.orchestration.dto.CommentMessageDto;
import be.wiv_isp.healthdata.orchestration.dto.NoteMessageDto;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

public class NoteMessageDtoMapper {

    public static NoteMessageDto mapToNoteDto(Note note) {
        final NoteMessageDto dto = new NoteMessageDto();
        dto.setOrder(note.getOrder());
        dto.setWorkflowType(note.getWorkflowType());
        dto.setCommentMaxOrder(note.getCommentMaxOrder());
        dto.setTitle(note.getTitle());
        dto.setResolved(note.isResolved());
        dto.setFieldId(note.getFieldId());
        dto.setFieldPath(note.getFieldPath());
        dto.setCreatedOn(note.getCreatedOn());
        SortedSet<Comment> comments = note.getComments();
        if(CollectionUtils.isNotEmpty(comments)) {
            dto.setComments(mapToCommentDtos(comments));
        }
        return dto;
    }

    public static List<NoteMessageDto> mapToNoteDtos(List<Note> notes) {
        if(notes == null) {
            return new ArrayList<>();
        }

        final List<NoteMessageDto> dtos = new ArrayList<>();
        for(Note note : notes) {
            dtos.add(mapToNoteDto(note));
        }
        return dtos;
    }

    private static CommentMessageDto  mapToCommentDto(Comment comment) {
        CommentMessageDto dto = new CommentMessageDto();
        dto.setOrder(comment.getOrder());
        dto.setContent(comment.getContent());
        dto.setUsername(comment.getUsername());
        dto.setPlatform(comment.getPlatform());
        dto.setDeleted(comment.getDeleted());
        dto.setCreatedOn(comment.getCreatedOn());
        return dto;
    }

    private static SortedSet<CommentMessageDto>  mapToCommentDtos(SortedSet<Comment> comments) {
        if(comments == null) {
            return new TreeSet<>();
        }

        final SortedSet<CommentMessageDto> dtos = new TreeSet<>();
        for(Comment comment : comments) {
            dtos.add(mapToCommentDto(comment));
        }
        return dtos;
    }
}
