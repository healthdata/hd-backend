/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.json.serializer.DateSerializer;
import be.wiv_isp.healthdata.orchestration.domain.search.ElasticSearchInfoSearch;
import be.wiv_isp.healthdata.orchestration.elasticsearch.client.IElasticSearchClient;
import be.wiv_isp.healthdata.orchestration.elasticsearch.client.impl.DeleteRequest;
import be.wiv_isp.healthdata.orchestration.elasticsearch.client.impl.ElasticSearchBulkResponse;
import be.wiv_isp.healthdata.orchestration.elasticsearch.client.impl.IndexingRequest;
import be.wiv_isp.healthdata.orchestration.api.mapper.IAbstractWorkflowMapper;
import be.wiv_isp.healthdata.orchestration.domain.AbstractElasticSearchInfo.Status;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.mapper.NoteMapper;
import be.wiv_isp.healthdata.orchestration.domain.search.AbstractRegistrationWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.dto.AbstractRegistrationWorkflowDto;
import be.wiv_isp.healthdata.orchestration.dto.DocumentDto;
import be.wiv_isp.healthdata.orchestration.jaxb.json.JsonMarshaller;
import be.wiv_isp.healthdata.orchestration.service.*;
import be.wiv_isp.healthdata.orchestration.tasks.ElasticSearchTask;
import org.apache.commons.collections.CollectionUtils;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;

import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

abstract public class AbstractElasticSearchService<Workflow extends AbstractRegistrationWorkflow, Search extends AbstractRegistrationWorkflowSearch, FullDto extends AbstractRegistrationWorkflowDto, Dto extends AbstractRegistrationWorkflowDto> implements IElasticSearchService<FullDto, Dto> {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractElasticSearchService.class);

	public static final String UNDERSCORE = "_";
	public static final String BLANK_SPACE = " ";

	@Autowired
	private IElasticSearchClient elasticSearchClient;
	@Autowired
	private IAbstractRegistrationWorkflowService<Workflow, Search> workflowService;
	@Autowired
	private IAbstractWorkflowMapper<Workflow, Dto> workflowMapper;
	@Autowired
	private IElasticSearchInfoService<AbstractElasticSearchInfo> elasticSearchInfoService;
	@Autowired
	private IMappingService mappingService;
	@Autowired
	private IAbstractRegistrationDocumentService<? extends AbstractRegistrationDocument, ?> documentService;
	@Autowired
	private TaskScheduler elasticSearchIndexingTaskScheduler;
	@Autowired
	private IElasticSearchDocumentMappingService elasticSearchDocumentMappingService;
	@Autowired
	private ElasticSearchTask elasticSearchIndexingTask;
	@Autowired
	protected IOrganizationService organizationService;
	@Autowired
	protected ISalesForceOrganizationService salesForceOrganizationService;
	@Autowired
	private INoteService noteService;
	@Autowired
	private IDataCollectionService dataCollectionService;

	@Override
	public String search(String index, String type, String searchQuery) {
		return search(new String[] { index }, new String[] { type }, searchQuery);
	}

	@Override
	public String search(String[] indices, String[] types, String searchQuery) {
		return elasticSearchClient.search(indices, types, searchQuery);
	}

	@Override
	public void index(Long workflowId) {
		index(workflowId, null);
	}

	@Override
	public void index(final List<Long> workflowIds) {
		elasticSearchDocumentMappingService.initialize();
		elasticSearchDocumentMappingService.add(workflowIds.toArray(new Long[workflowIds.size()]));
		elasticSearchDocumentMappingService.start();

		Entry<Long, String> entry = elasticSearchDocumentMappingService.next();

		while (entry != null) {
			this.index(entry.getKey(), entry.getValue());
			entry = elasticSearchDocumentMappingService.next();
		}
	}

	abstract protected boolean ignoreDeletedWorkflows();

	private void index(Long workflowId, String elasticSearchDocument) {
		LOG.debug("Indexing registration[{}] in elasticsearch", workflowId);

		final Workflow workflow = workflowService.get(workflowId);

		if (workflow == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "WORKFLOW", workflowId);
			throw exception;
		}

		final AbstractElasticSearchInfo elasticSearchInfo = elasticSearchInfoService.get(workflow.getId());
		try {
			if (ignoreDeletedWorkflows() && workflow.getStatus() == WorkflowStatus.DELETED) {
				LOG.debug("Registration is deleted, nothing to index.");
				updateElasticSearchInfoToDeleted(elasticSearchInfo, workflow);
				return;
			}

			if (workflow.getDocument() == null) {
				LOG.debug("Document is null, nothing to index. Marking registration as indexed.");
				updateElasticSearchInfoToIndexed(elasticSearchInfo, workflow);
				return;
			}

			if (workflow.getOrganization().isDeleted()) {
				LOG.debug("Registration belongs to deleted organization, nothing to index.");
				updateElasticSearchInfoToDeleted(elasticSearchInfo, workflow);
				return;
			}

			final String index = buildIndexName(workflow.getOrganization(), workflow.getDataCollectionName());
			final String type = workflow.getDataCollectionDefinitionId().toString();

			LOG.debug("Indexing registration [{}] in elasticsearch using index [{}] and type [{}])", workflow.getId(), index, type);

			if (!elasticSearchClient.indexExists(index)) {
				LOG.debug("Index [{}] does not exist, creating index", index);
				final String settings = mappingService.getElasticSearchIndexSettings();
				elasticSearchClient.createIndex(index, settings);
			}

			if (!elasticSearchClient.typeExists(index, type)) {
				LOG.debug("Retrieving mapping-file from the mapping API for data collection definition id [{}]", workflow.getDataCollectionDefinitionId());
				final JSONObject mappingContent = mappingService.getElasticSearchMapping(workflow.getDataCollectionDefinitionId());

				LOG.debug("Adding elasticsearch type on top of mapping file");
				final JSONObject result = new JSONObject();
				result.put(type, mappingContent);

				LOG.debug("Pushing mapping-file to elasticsearch for index [{}] and type [{}]", index, type);
				elasticSearchClient.putMapping(index, type, result.toString());
			}

			if (elasticSearchDocument == null) {
				LOG.debug("Elasticsearch document has not previously been computed. Computing now.");
				elasticSearchDocument = mappingService.mapToElasticSearchDocument(workflow).toString();
			}
			List<Note> notes = noteService.getByDocumentId(workflow.getWorkflowType(), workflow.getDocument().getId());
			FullDto elasticSearchWorkflow = getDtoInstance(workflowMapper.convert(workflow), NoteMapper.mapToNoteDtos(notes));

			AbstractRegistrationDocument document = documentService.getEntityInstance();
			document.setId(workflow.getDocument().getId());
			document.setDocumentContent(elasticSearchDocument.getBytes(StandardCharsets.UTF_8));
			document.setWorkflow(workflow);
			elasticSearchWorkflow.setDocument(new DocumentDto(document));

			final String workflowJson = JsonMarshaller.marshal(elasticSearchWorkflow);

			final IndexingRequest indexingRequest = new IndexingRequest();
			indexingRequest.setIndex(index);
			indexingRequest.setType(type);
			indexingRequest.setId(workflow.getId().toString());
			indexingRequest.setContent(workflowJson);

			final ElasticSearchBulkResponse response = elasticSearchClient.index(indexingRequest);
			if(response.hasFailures()) {
				String message = "could not retrieve error message";
				if(CollectionUtils.isNotEmpty(response.getResponses())) {
					message = response.getResponses().get(0).getMessage();
				}
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.ELASTICSEARCH_INDEX_EXCEPTION, message);
				throw exception;
			}

			updateElasticSearchInfoToIndexed(elasticSearchInfo, workflow);
		} catch (Exception e) {
			String message = e.getMessage() == null ? e.getClass().getName() : e.getMessage();
			LOG.error(MessageFormat.format(ExceptionType.ELASTICSEARCH_EXCEPTION.getMessage(), e.getMessage()), e);
			if (elasticSearchInfo.getRetryCount() >= 2) {
				updateElasticSearchInfo(elasticSearchInfo, workflow, Status.INDEXING_FAILED, message, 0);
			} else {
				updateElasticSearchInfo(elasticSearchInfo, workflow, Status.NOT_INDEXED, message, elasticSearchInfo.getRetryCount() + 1);
			}
		}
	}

	@Override
	public void rebuild() {
		LOG.info("Rebuilding elasticsearch index");

		elasticSearchClient.deleteAllIndices();
		elasticSearchInfoService.updateAll(Status.NOT_INDEXED);
		elasticSearchIndexingTaskScheduler.schedule(elasticSearchIndexingTask, new Date());
	}

	@Override
	public void rebuild(Long organizationId) {
		LOG.info("Rebuilding elasticsearch index for organization [{}]", organizationId);

		for (String dataCollectionName: workflowService.getDataCollectionNames(organizationId)) {
			final String indexName = buildIndexName(organizationService.get(organizationId), dataCollectionName);
			elasticSearchClient.deleteIndex(indexName);
		}
		final ElasticSearchInfoSearch search = new ElasticSearchInfoSearch();
		search.setOrganizationId(organizationId);
		elasticSearchInfoService.updateAll(search, Status.NOT_INDEXED);
		elasticSearchIndexingTaskScheduler.schedule(elasticSearchIndexingTask, new Date());
	}

	@Override
	public void rebuild(Long organizationId, String dataCollectionGroup) {
		LOG.info("Rebuilding elasticsearch index for organization [{}] and data collection group [{}]", organizationId, dataCollectionGroup);

		if (dataCollectionGroup == null) {
			return;
		}
		for (String dataCollectionName : dataCollectionService.getMembers(dataCollectionGroup)){
			final String indexName = buildIndexName(organizationService.get(organizationId), dataCollectionName);
			elasticSearchClient.deleteIndex(indexName);

			final ElasticSearchInfoSearch search = new ElasticSearchInfoSearch();
			search.setOrganizationId(organizationId);
			search.setDataCollectionName(dataCollectionName);
			elasticSearchInfoService.updateAll(search, Status.NOT_INDEXED);
		}
		elasticSearchIndexingTaskScheduler.schedule(elasticSearchIndexingTask, new Date());
	}

	@Override
	public void repair() {
		LOG.info("Repairing elasticsearch index");

		final ElasticSearchInfoSearch search = new ElasticSearchInfoSearch();
		search.setStatus(Status.INDEXING_FAILED);
		elasticSearchInfoService.updateAll(search, Status.NOT_INDEXED);
		elasticSearchIndexingTaskScheduler.schedule(elasticSearchIndexingTask, new Date());
	}

	@Override
	public void repair(Long organizationId) {
		LOG.info("Repairing elasticsearch index for organization [{}]", organizationId);

		final ElasticSearchInfoSearch search = new ElasticSearchInfoSearch();
		search.setStatus(Status.INDEXING_FAILED);
		search.setOrganizationId(organizationId);
		elasticSearchInfoService.updateAll(search, Status.NOT_INDEXED);
		elasticSearchIndexingTaskScheduler.schedule(elasticSearchIndexingTask, new Date());
	}

	@Override
	public void repair(Long organizationId, String dataCollectionGroup) {
		LOG.info("Repairing elasticsearch index for organization [{}] and data collection group [{}]", organizationId, dataCollectionGroup);

		for (String dataCollectionName : dataCollectionService.getMembers(dataCollectionGroup)) {
			final ElasticSearchInfoSearch search = new ElasticSearchInfoSearch();
			search.setStatus(Status.INDEXING_FAILED);
			search.setOrganizationId(organizationId);
			search.setDataCollectionName(dataCollectionName);
			elasticSearchInfoService.updateAll(search, Status.NOT_INDEXED);
		}
		elasticSearchIndexingTaskScheduler.schedule(elasticSearchIndexingTask, new Date());
	}

	@Override
	public boolean status() {
		return "OK".equals(this.advancedStatus().getStatus());
	}

	@Override
	public boolean status(Long organizationId) {
		return "OK".equals(this.advancedStatus().getStatus(organizationId, null));
	}

	@Override
	public boolean status(Long organizationId, String dataCollectionName) {
		return "OK".equals(this.advancedStatus().getStatus(organizationId, dataCollectionName));
	}

	@Override
	public AdvancedStatus advancedStatus() {
		final AdvancedStatus advancedStatus = new AdvancedStatus();

		advancedStatus.setClusterStatus(elasticSearchClient.status() ? "UP" : "DOWN");
		advancedStatus.setDocumentsInElasticSearch(elasticSearchClient.getDocumentCount());
		advancedStatus.getDocumentsInDatabase().setTotal(documentService.count());
		advancedStatus.getDocumentsInDatabase().setCountIndexed(elasticSearchInfoService.count(buildSearch(Status.INDEXED)));
		advancedStatus.getDocumentsInDatabase().setCountNotIndexed(elasticSearchInfoService.count(buildSearch(Status.NOT_INDEXED)));
		advancedStatus.getDocumentsInDatabase().setCountFailed(elasticSearchInfoService.count(buildSearch(Status.INDEXING_FAILED)));
		advancedStatus.setIndexingFailed(elasticSearchInfoService.getAll(buildSearch(Status.INDEXING_FAILED)));

		for (Organization organization : organizationService.getAll()) {
			AdvancedStatus.DocumentsInDatabase orgDetails = advancedStatus.new DocumentsInDatabase();

			final Search search = getWorkflowSearchInstance();
			search.setOrganization(organization);
			search.setReturnDeleted(!ignoreDeletedWorkflows());
			orgDetails.setTotal(workflowService.count(search));
			for (String dcdName : workflowService.getDataCollectionNames(organization.getId())) {
				AdvancedStatus.DocumentsInDatabase dcdDetails = advancedStatus.new DocumentsInDatabase();
				final Search search2 = getWorkflowSearchInstance();
				search2.setOrganization(organization);
				search2.setDataCollectionName(dcdName);
				search2.setReturnDeleted(!ignoreDeletedWorkflows());
				dcdDetails.setTotal(workflowService.count(search2));
				orgDetails.getDetails().put(dcdName, dcdDetails);
			}
			advancedStatus.getDocumentsInDatabase().getDetails().put(organization.getHealthDataIDValue(), orgDetails);
		}

		final List<ElasticSearchInfoCountDetails> details = elasticSearchInfoService.countDetailed();

		for (ElasticSearchInfoCountDetails detail : details) {
			final Long orgId = detail.getOrganizationId();
			final String dcdName = detail.getDataCollectionName();
			final AbstractElasticSearchInfo.Status status = detail.getStatus();
			final Long count = detail.getCount();

			Organization organization = organizationService.get(orgId);
			if (organization.isDeleted()) {
				continue;
			}
			AdvancedStatus.DocumentsInDatabase orgDetails = advancedStatus.getDocumentsInDatabase().getDetails().get(organization.getHealthDataIDValue());
			AdvancedStatus.DocumentsInDatabase dcdDetails = orgDetails.getDetails().get(dcdName);
			switch (status) {
				case INDEXED:
					dcdDetails.setCountIndexed(count);
					orgDetails.setCountIndexed(orgDetails.getCountIndexed() + count);
					break;
				case NOT_INDEXED:
					dcdDetails.setCountNotIndexed(count);
					orgDetails.setCountNotIndexed(orgDetails.getCountNotIndexed() + count);
					break;
				case INDEXING_FAILED:
					dcdDetails.setCountFailed(count);
					orgDetails.setCountFailed(orgDetails.getCountFailed() + count);
					break;
				case DELETED:
					break;
			}
		}

		if (elasticSearchIndexingTask.isRunning()) {
			advancedStatus.getIndexingTask().setStatus("RUNNING");
			advancedStatus.getIndexingTask().setStartTime(elasticSearchIndexingTask.getStartTime());
		} else {
			advancedStatus.getIndexingTask().setStatus("NOT_RUNNING");
			advancedStatus.getIndexingTask().setStartTime(elasticSearchIndexingTask.getStartTime());
			advancedStatus.getIndexingTask().setEndTime(elasticSearchIndexingTask.getEndTime());
		}

		advancedStatus.validate();

		return advancedStatus;
	}

	protected ElasticSearchInfoSearch buildSearch(Status status) {
		final ElasticSearchInfoSearch search = new ElasticSearchInfoSearch();
		search.setStatus(status);
		return search;
	}

	protected abstract Search getWorkflowSearchInstance();

	@Override
	public String buildIndexName(Organization organization, String dataCollectionName) {
		// indices must be lower case in ElasticSearch
		return MessageFormat.format("{0}_{1}", organization.getId().toString(), dataCollectionName.toLowerCase().replace(BLANK_SPACE, UNDERSCORE));
	}

	private void updateElasticSearchInfoToIndexed(final AbstractElasticSearchInfo elasticSearchInfo, AbstractRegistrationWorkflow workflow) {
		updateElasticSearchInfo(elasticSearchInfo, workflow, Status.INDEXED, null, 0);
	}

	private void updateElasticSearchInfoToDeleted(final AbstractElasticSearchInfo elasticSearchInfo, AbstractRegistrationWorkflow workflow) {
		updateElasticSearchInfo(elasticSearchInfo, workflow, Status.DELETED, null, 0);
	}

	private void updateElasticSearchInfo(final AbstractElasticSearchInfo elasticSearchInfo, AbstractRegistrationWorkflow workflow, Status status, String error, int retryCount) {
		if (elasticSearchInfo == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "ELASTICSEARCH_INFO", workflow.getId());
			throw exception;
		}

		elasticSearchInfo.setStatus(status);
		elasticSearchInfo.setError(error);
		elasticSearchInfo.setRetryCount(retryCount);

		elasticSearchInfoService.update(elasticSearchInfo);
	}

	@Override
	public void delete(Long workflowId) {
		final Workflow workflow = workflowService.get(workflowId);
		final String index = buildIndexName(workflow.getOrganization(), workflow.getDataCollectionName());
		final String type = workflow.getDataCollectionDefinitionId().toString();

		final DeleteRequest request = new DeleteRequest();
		request.setIndex(index);
		request.setType(type);
		request.setId(workflowId.toString());

		final ElasticSearchBulkResponse response = elasticSearchClient.delete(request);
		if(response.hasFailures()) {
			String message = "could not retrieve error message";
			if(CollectionUtils.isNotEmpty(response.getResponses())) {
				message = response.getResponses().get(0).getMessage();
			}
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.ELASTICSEARCH_DELETE_EXCEPTION, message);
			throw exception;
		}

		final AbstractElasticSearchInfo elasticSearchInfo = elasticSearchInfoService.get(workflow.getId());
		updateElasticSearchInfo(elasticSearchInfo, workflow, Status.DELETED, null, 0);
	}

	@JsonPropertyOrder({"status","cluster_status","indexing_task","indexing_failed","documents_in_elasticsearch","documents_in_database"})
	public class AdvancedStatus {

		private String status;
		private String clusterStatus;
		private Long documentsInElasticSearch;
		private DocumentsInDatabase documentsInDatabase = new DocumentsInDatabase();
		private IndexingTask indexingTask = new IndexingTask();
		private List<AbstractElasticSearchInfo> indexingFailed;

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getStatus(Long organizationId, String dataCollectionName) {
			final Organization organization = organizationService.get(organizationId);
			if (organization == null)
				return "OK";
			DocumentsInDatabase did = this.documentsInDatabase.getDetails().get(organization.getHealthDataIDValue());
			if (did == null)
				return "OK";

			if (dataCollectionName != null) {
				did = did.getDetails().get(dataCollectionName);
				if (did == null)
					return "OK";
			}

			return did.status;
		}

		@JsonProperty("cluster_status")
		public String getClusterStatus() {
			return clusterStatus;
		}

		public void setClusterStatus(String clusterStatus) {
			this.clusterStatus = clusterStatus;
		}

		@JsonProperty("documents_in_database")
		public DocumentsInDatabase getDocumentsInDatabase() {
			return documentsInDatabase;
		}

		public void setDocumentsInDatabase(DocumentsInDatabase documentsInDatabase) {
			this.documentsInDatabase = documentsInDatabase;
		}

		@JsonProperty("documents_in_elasticsearch")
		public Long getDocumentsInElasticSearch() {
			return documentsInElasticSearch;
		}

		public void setDocumentsInElasticSearch(Long documentsInElasticSearch) {
			this.documentsInElasticSearch = documentsInElasticSearch;
		}

		@JsonProperty("indexing_task")
		public IndexingTask getIndexingTask() {
			return indexingTask;
		}

		public void setIndexTask(IndexingTask indexingTask) {
			this.indexingTask = indexingTask;
		}

		@JsonProperty("indexing_failed")
		public List<AbstractElasticSearchInfo> getIndexingFailed() {
			return indexingFailed;
		}

		public void setIndexingFailed(List<AbstractElasticSearchInfo> indexingFailed) {
			this.indexingFailed = indexingFailed;
		}

		public void validate() {
			validate(this.documentsInDatabase);
			if (!documentsInElasticSearch.equals(documentsInDatabase.getTotal())) {
				this.documentsInDatabase.status = "NOK";
			}

			this.status = this.documentsInDatabase.status;

			for (DocumentsInDatabase org : documentsInDatabase.getDetails().values()) {
				validate(org);
				for (DocumentsInDatabase dcd : org.getDetails().values()) {
					validate(dcd);
				}
			}
		}

		private void validate(DocumentsInDatabase did) {
			did.status = "OK";
			if (!"UP".equals(clusterStatus))
				did.status = "NOK";
			if (!did.getTotal().equals(did.getCountFailed() + did.getCountNotIndexed() + did.getCountIndexed()))
				did.status = "NOK";
			if (!did.getCountNotIndexed().equals(0L))
				did.status = "NOK";
			if (!did.getCountFailed().equals(0L))
				did.status = "NOK";
		}

		@JsonPropertyOrder({"status", "total", "documents_indexed", "documents_not_indexed", "documents_indexing_failed", "details"})
		public class DocumentsInDatabase {
			private String status;
			private Long total = 0L;
			private Long countIndexed = 0L;
			private Long countNotIndexed = 0L;
			private Long countFailed = 0L;
			private Map<String, DocumentsInDatabase> details = new HashMap<>();

			public String getStatus() {
				return status;
			}

			public void setStatus(String status) {
				this.status = status;
			}

			@JsonProperty("total")
			public Long getTotal() {
				return total;
			}

			public void setTotal(Long total) {
				this.total = total;
			}

			@JsonProperty("documents_indexed")
			public Long getCountIndexed() {
				return countIndexed;
			}

			public void setCountIndexed(Long countIndexed) {
				this.countIndexed = countIndexed;
			}

			@JsonProperty("documents_not_indexed")
			public Long getCountNotIndexed() {
				return countNotIndexed;
			}

			public void setCountNotIndexed(Long countNotIndexed) {
				this.countNotIndexed = countNotIndexed;
			}

			@JsonProperty("documents_indexing_failed")
			public Long getCountFailed() {
				return countFailed;
			}

			public void setCountFailed(Long countFailed) {
				this.countFailed = countFailed;
			}

			@JsonProperty("details")
			public Map<String, DocumentsInDatabase> getDetails() {
				return details;
			}

			public void setDetails(Map<String, DocumentsInDatabase> details) {
				this.details = details;
			}

		}

		public class IndexingTask {

			private String status;
			private Date startTime;
			private Date endTime;

			@JsonProperty("status")
			public String getStatus() {
				return status;
			}

			public void setStatus(String status) {
				this.status = status;
			}

			@JsonProperty("start_time")
			@JsonSerialize(using = DateSerializer.class)
			public Date getStartTime() {
				return startTime;
			}

			public void setStartTime(Date startTime) {
				this.startTime = startTime;
			}

			@JsonProperty("end_time")
			@JsonSerialize(using = DateSerializer.class)
			public Date getEndTime() {
				return endTime;
			}

			public void setEndTime(Date endTime) {
				this.endTime = endTime;
			}

			@JsonProperty("duration")
			public String getDuration() {
				if (this.startTime == null) {
					return null;
				}

				long millis = this.endTime == null ? new Date().getTime() - startTime.getTime() : this.endTime.getTime() - this.startTime.getTime();

				final Calendar c = Calendar.getInstance();
				c.set(Calendar.HOUR_OF_DAY, 0);
				c.set(Calendar.MINUTE, 0);
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				c.setTimeInMillis(c.getTimeInMillis() + millis);

				final SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss.SSS");

				return df.format(c.getTime());
			}
		}
	}
}
