/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.mail;

import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import org.apache.velocity.VelocityContext;

public abstract class AbstractVelocityContext extends VelocityContext {

    protected InstallationDetails installationDetails = new InstallationDetails();
    protected String supportEmail;
    protected Platform platform;

    public void setInstallationUrl(String url) {
        installationDetails.setUrl(url);
    }

    public void setSupportEmail(String supportEmail) {
        this.supportEmail = supportEmail;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public void build() {
        put("installationDetails", installationDetails);
        put("supportEmail", supportEmail);
        put("platform", platform);
        doBuild();
    }

    protected abstract void doBuild();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractVelocityContext)) return false;

        AbstractVelocityContext that = (AbstractVelocityContext) o;

        if (installationDetails != null ? !installationDetails.equals(that.installationDetails) : that.installationDetails != null)
            return false;
        return !(supportEmail != null ? !supportEmail.equals(that.supportEmail) : that.supportEmail != null);

    }

    @Override
    public int hashCode() {
        int result = installationDetails != null ? installationDetails.hashCode() : 0;
        result = 31 * result + (supportEmail != null ? supportEmail.hashCode() : 0);
        return result;
    }

    public class InstallationDetails {

        private String url;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o)
                return true;
            if (!(o instanceof InstallationDetails))
                return false;

            InstallationDetails that = (InstallationDetails) o;

            return !(url != null ? !url.equals(that.url) : that.url != null);

        }

        @Override
        public int hashCode() {
            return url != null ? url.hashCode() : 0;
        }
    }
}
