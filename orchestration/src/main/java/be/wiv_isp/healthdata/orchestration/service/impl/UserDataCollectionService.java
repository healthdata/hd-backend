/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import be.wiv_isp.healthdata.orchestration.service.IUserService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionService;
import be.wiv_isp.healthdata.orchestration.service.IUserDataCollectionService;
import be.wiv_isp.healthdata.orchestration.service.IUserManagementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserDataCollectionService implements IUserDataCollectionService {

    private static final Logger LOG = LoggerFactory.getLogger(UserDataCollectionService.class);

    @Autowired
    private IUserService userService;
    @Autowired
    private IDataCollectionService dataCollectionService;
    @Autowired
    private IUserManagementService userManagementService;

    @Override
    public boolean isUserAuthorized(HDUserDetails userDetails, AbstractWorkflow workflow) {
        if (userDetails instanceof HDServiceUser) {
            return true; // HealthData service users can always execute action
        }
        if (!userDetails.getOrganization().getId().equals(workflow.getOrganization().getId())) {
            return false;
        }
        return isUserAuthorized(userDetails, workflow.getDataCollectionName());
    }

    @Override
    public boolean isUserAuthorized(HDUserDetails userDetails, String dataCollectionName) {
        if (userDetails instanceof HDServiceUser) {
            return true; // HealthData service users can always execute action
        }
        final Set<String> dataCollectionNames = get(userDetails);
        final String masterName = dataCollectionService.getGroup(dataCollectionName);
        for (String userDataCollectionName : dataCollectionNames) {
            if (userDataCollectionName.equalsIgnoreCase(masterName)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Set<String> get(HDUserDetails userDetails) {
        Set<String> userDataCollectionNames = getUserDataCollectionNames(userDetails);

        Set<String> union = new HashSet<>();
        union.addAll(userDataCollectionNames);
        if (userDetails.isSupport() || userDetails.isAdmin()) {
            union.addAll(getPublicDataCollectionNames());
        }

        return union;
    }

    @Override
    public Set<String> getEmailAddresses(DataCollection dataCollection) {
        return getEmailAddresses(dataCollection, null);
    }

    @Override
    public Set<String> getEmailAddresses(DataCollection dataCollection, Organization organization) {
        LOG.debug("Retrieving email addresses for data collection [{}] and organization [{}]", dataCollection.getName(), organization);
        final Set<String> emails = new HashSet<>();

        final UserSearch search = new UserSearch();
        search.setDataCollectionName(dataCollection.getName());
        search.setOrganization(organization);
        final List<User> users = userManagementService.getUserManagementStrategy().getAll(search).getResult();
        LOG.debug("{} users found which have access to data collection {}", users.size(), dataCollection.getName());
        for (final User user : users) {
            if (user.getEmail() != null) {
                emails.add(user.getEmail());
            } else {
                LOG.warn("User[{}]: user has no email address", user.getId());
            }
        }

        LOG.debug("{} email address(es) found for data collection {}: {}", emails.size(), dataCollection.getName(), emails);
        return emails;
    }


    private Set<String> getUserDataCollectionNames(HDUserDetails userDetails) {
        final UserSearch userSearch = new UserSearch();
        userSearch.setUsername(userDetails.getUsername());
        userSearch.setOrganization(userDetails.getOrganization());
        final User user = userService.getUnique(userSearch);
        if (user == null) {
            return Collections.emptySet();
        }
        Set<String> organizationDataCollections = dataCollectionService.getDataCollectionGroups(userDetails.getOrganization());
        if(user.isSupport() || user.isHd4prc()) {
            return organizationDataCollections;
        }
        Set<String> result = new HashSet<>();
        for (String dataCollection : user.getDataCollectionNames())
            if (organizationDataCollections.contains(dataCollection))
                result.add(dataCollection);
        return result;
    }

    protected Set<String> getPublicDataCollectionNames() {
        return dataCollectionService.getPublicDataCollectionGroups();
    }

}
