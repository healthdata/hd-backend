/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.api.ClientVersion;
import be.wiv_isp.healthdata.common.caching.CacheManagementService;
import be.wiv_isp.healthdata.common.dto.SalesForceAccountDto;
import be.wiv_isp.healthdata.common.dto.SalesForceOrganizationDto;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.orchestration.api.mapper.SalesForceOrganizationMapper;
import be.wiv_isp.healthdata.orchestration.api.uri.MultiCenterNameUri;
import be.wiv_isp.healthdata.orchestration.api.uri.MultiCenterUri;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.ISalesForceOrganizationService;
import be.wiv_isp.healthdata.orchestration.service.IWebServiceClientService;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Service
public class SalesForceOrganizationService implements ISalesForceOrganizationService {

	private static final Logger LOG = LoggerFactory.getLogger(SalesForceOrganizationService.class);

	@Autowired
	private IWebServiceClientService webServiceClientService;

	@Autowired
	private IConfigurationService configurationService;

	@Override
	public void createSalesForceOrganization(String parentIdentificationType, String parentIdentificationValue, SalesForceAccountDto organization) {
		Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

		MultiCenterUri multiCenterUri = new MultiCenterUri();
		multiCenterUri.setHost(catalogueHost.getValue());

		WebServiceBuilder wsb = new WebServiceBuilder();
		wsb.setUrl(multiCenterUri.toString());
		wsb.setPost(true);
		wsb.addHeader(HttpHeaders.CLIENT_VERSION, ClientVersion.MultiCenter.getDefault());
		wsb.setJson(organization);
		wsb.setType(MediaType.APPLICATION_JSON_TYPE);
		wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
		if(!StringUtils.isAnyBlank(parentIdentificationType, parentIdentificationValue)) {
			wsb.addParameter(RestUtils.ParameterName.PARENT_IDENTIFICATION_TYPE, parentIdentificationType);
			wsb.addParameter(RestUtils.ParameterName.PARENT_IDENTIFICATION_VALUE, parentIdentificationValue);
		}

		wsb.setReturnType(new GenericType<ClientResponse>() {});


		final ClientResponse response = (ClientResponse) webServiceClientService.callWebService(wsb);

		if(response.getStatus() != ClientResponse.Status.NO_CONTENT.getStatusCode()) {
			String message;
			try {
				message = IOUtils.toString(response.getEntityInputStream(), StandardCharsets.UTF_8);
			} catch (IOException e) {
				message = "Couldn't get the response entity";
			}
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, message);
			throw exception;
		}
	}

	/**
	 * Returns always a SalesForceOrganizationDto. If no match was found by the
	 * catalogue for the given parameters, the attributes of the returned object
	 * are left empty.
	 * 
	 * @param identificationType
	 * @param identificationValue
	 * @throws HealthDataException
	 *             if the response received from the catalogue is not a valid
	 *             JSON object
	 * @return
	 */
	@Override
	@Cacheable(value = CacheManagementService.salesForceOrganizationNameCache)
	public SalesForceOrganizationDto getSalesForceOrganization(String identificationType, String identificationValue) {
		if (StringUtils.isAnyBlank(identificationType, identificationValue)) {
			return new SalesForceOrganizationDto();
		}

		final MultiCenterNameUri multiCenterNameUri = new MultiCenterNameUri();
		multiCenterNameUri.setHost(configurationService.get(ConfigurationKey.CATALOGUE_HOST).getValue());

		final WebServiceBuilder wsb = new WebServiceBuilder();
		wsb.setGet(true);
		wsb.setUrl(multiCenterNameUri.toString());
		wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb.addHeader(HttpHeaders.CLIENT_VERSION, ClientVersion.MultiCenter.getDefault());
		wsb.addParameter(RestUtils.ParameterName.IDENTIFICATION_TYPE, identificationType);
		wsb.addParameter(RestUtils.ParameterName.IDENTIFICATION_VALUE, identificationValue);
		wsb.setReturnType(new GenericType<JSONObject>() {});

		LOG.debug("Identification type query parameter: {}", identificationType);
		LOG.debug("Identification value query parameter: {}", identificationValue);

		final JSONObject response = (JSONObject) webServiceClientService.callWebService(wsb);

		try {
			return SalesForceOrganizationMapper.convert(response);
		} catch (Exception e) {
			LOG.info(response.toString());
			LOG.error(e.getMessage(), e);
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.JSON_PARSE_EXCEPTION);
			throw exception;
		}
	}

	@Override
	public List<SalesForceOrganizationDto> getSalesForceSubOrganizations(String identificationType, String identificationValue) {
		LOG.debug("Retrieving multi center organizations from the catalogue");

		final MultiCenterUri multiCenterUri = new MultiCenterUri();
		multiCenterUri.setHost(configurationService.get(ConfigurationKey.CATALOGUE_HOST).getValue());

		final WebServiceBuilder wsb = new WebServiceBuilder();
		wsb.setGet(true);
		wsb.setUrl(multiCenterUri.toString());
		wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb.addHeader(HttpHeaders.CLIENT_VERSION, ClientVersion.MultiCenter.getDefault());
		wsb.addParameter(RestUtils.ParameterName.IDENTIFICATION_TYPE, identificationType);
		wsb.addParameter(RestUtils.ParameterName.IDENTIFICATION_VALUE, identificationValue);
		wsb.setReturnType(new GenericType<JSONArray>() {});

		LOG.debug("Identification type query parameter: {}", identificationType);
		LOG.debug("Identification value query parameter: {}", identificationValue);

		final JSONArray response = (JSONArray) webServiceClientService.callWebService(wsb);
		LOG.debug("Multi center organizations: {}", response);

		try {
			return SalesForceOrganizationMapper.convert(response);
		} catch (Exception e) {
			LOG.info(response.toString());
			LOG.error(e.getMessage(), e);
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.JSON_PARSE_EXCEPTION);
			throw exception;
		}
	}
}