/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.tasks;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;

public abstract class AbstractStartupTask extends SchedulableHealthDataTask {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractStartupTask.class);

    @Override
    public String getName() {
        return "STARTUP";
    }

    @Override
    protected String getTimingExpression() {
        return DateFormat.DATE_AND_TIME.format(new Date()); // start immediately at startup
    }

    @Override
    public void execute() {
        final List<HealthDataTask> tasks = getTasks();
        for (HealthDataTask task : tasks) {
            task.run();
        }
    }

    protected abstract List<HealthDataTask> getTasks();
}
