/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dto;

import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.mapper.AttachmentMapper;
import be.wiv_isp.healthdata.orchestration.mapping.IMappingContext;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.*;

abstract public class AbstractRegistrationWorkflowDto<Workflow extends AbstractRegistrationWorkflow<Document>, Document extends AbstractRegistrationDocument, MappingContext extends IMappingContext> extends AbstractWorkflowDto<Workflow, Document, RegistrationWorkflowHistory>{

	@Deprecated
	protected Map<String, String> metaData;
	protected List<FollowUp> followUps;
	protected WorkflowFlags flags;
	protected List<AttachmentDto> attachments;
	protected MappingContext context;
	protected Set<WorkflowActionType> availableActions;

	public AbstractRegistrationWorkflowDto() {

	}

	public AbstractRegistrationWorkflowDto(Workflow workflow) {
		super(workflow);
		setMetaData(workflow.getMetaData());
		setFollowUps(workflow.getFollowUps());
		setFlags(workflow.getFlags());
		setAttachments(AttachmentMapper.mapToAttachmentDtos(workflow.getDocument().getAttachments(), false));
	}

	public AbstractRegistrationWorkflowDto(AbstractRegistrationWorkflowDto dto) {
		super(dto);
		metaData = dto.getMetaData();
		followUps = dto.getFollowUps();
		flags = dto.getFlags();
		attachments = dto.getAttachments();
		context = (MappingContext) dto.getContext();
		availableActions = dto.getAvailableActions();
	}

	@JsonIgnore
	@Deprecated
	public Map<String, String> getMetaData() {
		return metaData;
	}

	@Deprecated
	public void setMetaData(Map<String, String> metaData) {
		this.metaData = metaData;
	}

	@JsonIgnore
	public List<FollowUp> getFollowUps() {
		return followUps;
	}

	public void setFollowUps(List<FollowUp> followUps) {
		this.followUps = followUps;
	}

	public MappingContext getContext() {
		return context;
	}

	public void setContext(MappingContext context) {
		this.context = context;
	}

	public WorkflowFlags getFlags() {
		return flags;
	}

	public void setFlags(WorkflowFlags flags) {
		this.flags = flags;
	}

	public List<AttachmentDto> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<AttachmentDto> attachments) {
		this.attachments = attachments;
	}

	public Set<WorkflowActionType> getAvailableActions() {
		return availableActions;
	}

	public void setAvailableActions(Set<WorkflowActionType> availableActions) {
		this.availableActions = availableActions;
	}

	public class WorkflowHistoryComparator implements Comparator<AbstractWorkflowHistory> {

		@Override
		public int compare(AbstractWorkflowHistory h1, AbstractWorkflowHistory h2) {
			if(WorkflowActionType.SAVE.equals(h1.getAction())) {
				if(WorkflowActionType.SAVE.equals(h2.getAction())) {
					return h1.getExecutedOn().compareTo(h2.getExecutedOn());
				} else {
					return -1;
				}
			} else {
				if(WorkflowActionType.SAVE.equals(h2.getAction())) {
					return 1;
				} else {
					return h1.getExecutedOn().compareTo(h2.getExecutedOn());
				}
			}
		}
	}
}