/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.orchestration.dao.IUserConfigurationDao;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.UserConfiguration;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.UserConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.search.UserConfigurationSearch;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserConfigurationDao extends CrudDaoV2<UserConfiguration, Long, UserConfigurationSearch> implements IUserConfigurationDao {

    public UserConfigurationDao() {
        super(UserConfiguration.class);
    }

    @Override
    protected List<Predicate> getPredicates(UserConfigurationSearch search, CriteriaBuilder cb, Root<UserConfiguration> rootEntry) {
        final List<Predicate> predicates = new ArrayList<>();

        if (search.getKey() != null) {
            predicates.add(cb.equal(rootEntry.<String> get("key"), search.getKey()));
        }
        if (search.getUserId() != null) {
            predicates.add(cb.equal(rootEntry.<User>get("user").get("id"), search.getUserId()));
        }

        return predicates;
    }

    @Override
    public List<String> deleteWithInvalidKey() {
        List<String> deletedKeys = new ArrayList<>();
        for (Long id : getAllIds()) {
            try {
                getKeyById(id);
            }
            catch (IllegalArgumentException e) {
                deleteId(id);
                final String invalidKey = e.getMessage().substring(e.getMessage().indexOf("[")+1, e.getMessage().indexOf("]"));
                deletedKeys.add(invalidKey);
            }
        }
        return deletedKeys;
    }

    private List<Long> getAllIds() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<UserConfiguration> rootEntry = cq.from(UserConfiguration.class);
        cq.select(rootEntry.<Long>get("id"));

        TypedQuery<Long> allQuery = em.createQuery(cq);
        return allQuery.getResultList();
    }

    private UserConfigurationKey getKeyById(Long id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<UserConfigurationKey> cq = cb.createQuery(UserConfigurationKey.class);
        Root<UserConfiguration> rootEntry = cq.from(UserConfiguration.class);
        cq.select(rootEntry.<UserConfigurationKey>get("key"));
        cq.where(cb.equal(rootEntry.<Long>get("id"), id));
        TypedQuery<UserConfigurationKey> allQuery = em.createQuery(cq);
        return allQuery.getSingleResult();
    }

    private int deleteId(Long id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaDelete<UserConfiguration> delete = cb.createCriteriaDelete(UserConfiguration.class);
        Root root = delete.from(UserConfiguration.class);
        delete.where(cb.equal(root.get("id"), id));
        return em.createQuery(delete).executeUpdate();
    }
}
