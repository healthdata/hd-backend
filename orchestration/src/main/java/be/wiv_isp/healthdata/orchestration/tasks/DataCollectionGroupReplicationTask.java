/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.tasks;

import be.wiv_isp.healthdata.common.dto.DataCollectionGroupListDto;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupListMap;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionGroupForwardService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionGroupReplicationService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public class DataCollectionGroupReplicationTask extends HealthDataTask {

    private static final Logger LOG = LoggerFactory.getLogger(DataCollectionGroupReplicationTask.class);

    @Autowired
    private IDataCollectionService dataCollectionService;

    @Autowired
    private IDataCollectionGroupReplicationService dataCollectionGroupReplicationService;

    @Autowired
    private IDataCollectionGroupForwardService dataCollectionGroupForwardService;

    @Autowired
    private IOrganizationService organizationService;

    @Override
    public String getName() {
        return "DATA_COLLECTION_GROUP_REPLICATION";
    }

    @Override
    protected void execute() {
        final Set<String> processedDataCollectionGroupNames = new HashSet<>();
        for (final Organization organization : organizationService.getAll()) {
            final DataCollectionGroupListMap list = dataCollectionGroupForwardService.list(organization);
            for (Map.Entry<String, List<DataCollectionGroupListDto>> entry : list.entrySet()) {
                final String dataCollectionGroupName = entry.getKey();

                if(processedDataCollectionGroupNames.contains(dataCollectionGroupName)) {
                    continue;
                }

                try {
                    LOG.debug("Synchronizing local information with the catalogue for data collection group [{}]", dataCollectionGroupName);
                    final List<DataCollectionGroupListDto> dataCollectionGroupList = entry.getValue();

                    if(CollectionUtils.isEmpty(dataCollectionGroupList)) {
                        LOG.debug("No data collection definition found for data collection group [{}]", dataCollectionGroupName);
                        continue;
                    }

                    for (DataCollectionGroupListDto dataCollectionGroup : dataCollectionGroupList) {
                        dataCollectionGroupReplicationService.process(dataCollectionGroup);
                    }
                } catch (Exception e) {
                    LOG.error(MessageFormat.format("Error while processing data collection group {0}", dataCollectionGroupName), e);
                } finally {
                    processedDataCollectionGroupNames.add(dataCollectionGroupName);
                }
            }
        }
    }
}
