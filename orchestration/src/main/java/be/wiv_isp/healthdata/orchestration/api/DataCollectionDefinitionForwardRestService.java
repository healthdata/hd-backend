/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionDefinitionForwardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Component
@Path("/datacollectiondefinitions")
public class DataCollectionDefinitionForwardRestService {

	@Autowired
	private IDataCollectionDefinitionForwardService collectionDefinitionForwardService;

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@HeaderParam("ClientVersion") String clientVersion, @Context UriInfo info) {
		DataCollectionDefinitionDtoV7 dataCollectionDefinitionDtoV7 = collectionDefinitionForwardService.get(getId(info));
		if(dataCollectionDefinitionDtoV7 == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		return Response.ok(dataCollectionDefinitionDtoV7).build();
	}

	private Long getId(UriInfo info) {
		return RestUtils.getPathLong(info, RestUtils.ParameterName.ID);
	}
}
