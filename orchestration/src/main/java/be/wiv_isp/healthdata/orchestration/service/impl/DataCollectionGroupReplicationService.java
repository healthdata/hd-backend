/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupListDto;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.orchestration.dao.IDataCollectionGroupReplicationDao;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.EventType;
import be.wiv_isp.healthdata.orchestration.domain.mapper.PeriodExpressionsConfigurationMapper;
import be.wiv_isp.healthdata.orchestration.domain.search.DataCollectionGroupReplicationSearch;
import be.wiv_isp.healthdata.orchestration.domain.search.EventDataCollectionGroupSearch;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionGroupReplicationService;
import be.wiv_isp.healthdata.orchestration.service.IEventDataCollectionGroupService;
import be.wiv_isp.healthdata.orchestration.util.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class DataCollectionGroupReplicationService extends AbstractService<DataCollectionGroupReplication, Long, DataCollectionGroupReplicationSearch, IDataCollectionGroupReplicationDao> implements IDataCollectionGroupReplicationService {

    private static final Logger LOG = LoggerFactory.getLogger(DataCollectionGroupReplicationService.class);

    @Autowired
    private IDataCollectionGroupReplicationDao dao;

    @Autowired
    private IEventDataCollectionGroupService eventDataCollectionGroupService;

    @Autowired
    private IConfigurationService configurationService;

    public DataCollectionGroupReplicationService() {
        super(DataCollectionGroupReplication.class);
    }

    @Override
    protected IDataCollectionGroupReplicationDao getDao() {
        return dao;
    }

    @Override
    @Transactional
    public void process(DataCollectionGroupListDto dataCollectionGroup) {
        final String dataCollectionGroupName = dataCollectionGroup.getDataCollectionGroupName();
        LOG.debug("Synchronizing data collection group {} (version {})", dataCollectionGroupName, dataCollectionGroup.getMajorVersion());

        final DataCollectionGroupReplicationSearch search = new DataCollectionGroupReplicationSearch();
        search.setDataCollectionGroupName(dataCollectionGroupName);
        search.setMajorVersion(dataCollectionGroup.getMajorVersion());
        final DataCollectionGroupReplication localDcg = getUnique(search);

        if(localDcg == null) {
            logNewVersion(dataCollectionGroup);
            create(buildDcgReplication(dataCollectionGroup));
            createOrUpdateStartDataCollectionEvent(dataCollectionGroup);
            createOrUpdateEndDataCollectionEvent(dataCollectionGroup);
        } else {
            boolean modified = false;

            if (startDateModified(dataCollectionGroup, localDcg)) {
                localDcg.setStartDate(dataCollectionGroup.getStartDate());
                createOrUpdateStartDataCollectionEvent(dataCollectionGroup);
                modified = true;
            }

            if (endDateModified(dataCollectionGroup, localDcg)) {
                localDcg.setEndDateCreation(dataCollectionGroup.getEndDateCreation());
                createOrUpdateEndDataCollectionEvent(dataCollectionGroup);
                modified = true;
            }

            if(modified) {
                update(localDcg);
            }
        }
    }

    private void logNewVersion(DataCollectionGroupListDto dataCollectionGroup) {
        LOG.info("New major version available for data collection group {}. New version: {}.", dataCollectionGroup.getDataCollectionGroupName(), dataCollectionGroup.getMajorVersion());
    }

    private void createOrUpdateStartDataCollectionEvent(DataCollectionGroupListDto dataCollectionGroup) {
        if(dataCollectionGroup.getStartDate() == null || dataCollectionGroup.getStartDate().before(new Date())) {
            LOG.warn("Start date is not defined or is in the past. Not creating event");
            return;
        }

        final EventDataCollectionGroupSearch search = new EventDataCollectionGroupSearch();
        search.setTypes(EventType.START_DATA_COLLECTION_PERIOD);
        search.setDataCollectionGroupName(dataCollectionGroup.getDataCollectionGroupName());
        search.setMajorVersion(dataCollectionGroup.getMajorVersion());
        final EventDataCollectionGroup existing = eventDataCollectionGroupService.getUnique(search);

        if(existing == null) {
            LOG.debug("Creating start of data collection event for data collection group {} and major version {} to {}", dataCollectionGroup.getDataCollectionGroupName(), dataCollectionGroup.getMajorVersion(), DateFormat.DATE_AND_TIME.format(dataCollectionGroup.getStartDate()));
            final EventDataCollectionGroup event = new EventDataCollectionGroup();
            event.setType(EventType.START_DATA_COLLECTION_PERIOD);
            event.setDataCollectionGroupName(dataCollectionGroup.getDataCollectionGroupName());
            event.setMajorVersion(dataCollectionGroup.getMajorVersion());
            event.addNotificationTime(dataCollectionGroup.getStartDate());
            eventDataCollectionGroupService.create(event);
        } else {
            LOG.debug("Updating start of data collection event for data collection group {} and major version {} to {}", dataCollectionGroup.getDataCollectionGroupName(), dataCollectionGroup.getMajorVersion(), DateFormat.DATE_AND_TIME.format(dataCollectionGroup.getStartDate()));
            existing.setMajorVersion(dataCollectionGroup.getMajorVersion());
            existing.clearNotificationTimes();
            existing.addNotificationTime(dataCollectionGroup.getStartDate());
            eventDataCollectionGroupService.update(existing);
        }
    }

    private void createOrUpdateEndDataCollectionEvent(DataCollectionGroupListDto dataCollectionGroup) {
        if(dataCollectionGroup.getEndDateCreation() == null || dataCollectionGroup.getEndDateCreation().before(new Date())) {
            LOG.warn("End creation date is not defined or is in the past. Not creating event");
            return;
        }

        final EventDataCollectionGroupSearch search = new EventDataCollectionGroupSearch();
        search.setTypes(EventType.END_DATA_COLLECTION_PERIOD);
        search.setDataCollectionGroupName(dataCollectionGroup.getDataCollectionGroupName());
        search.setMajorVersion(dataCollectionGroup.getMajorVersion());
        final EventDataCollectionGroup existing = eventDataCollectionGroupService.getUnique(search);

        if(existing == null) {
            LOG.debug("Creating end of data collection event for data collection group {} and major version {} to {}", dataCollectionGroup.getDataCollectionGroupName(), dataCollectionGroup.getMajorVersion(), DateFormat.DATE_AND_TIME.format(dataCollectionGroup.getEndDateCreation()));
            final EventDataCollectionGroup event = new EventDataCollectionGroup();
            event.setType(EventType.END_DATA_COLLECTION_PERIOD);
            event.setDataCollectionGroupName(dataCollectionGroup.getDataCollectionGroupName());
            event.setMajorVersion(dataCollectionGroup.getMajorVersion());
            setEndDataCollectionPeriodNotificationTimes(event, dataCollectionGroup.getEndDateCreation());
            eventDataCollectionGroupService.create(event);
        } else {
            LOG.debug("Updating end of data collection event for data collection group {} and major version {} to {}", dataCollectionGroup.getDataCollectionGroupName(), dataCollectionGroup.getMajorVersion(), DateFormat.DATE_AND_TIME.format(dataCollectionGroup.getEndDateCreation()));
            existing.setMajorVersion(dataCollectionGroup.getMajorVersion());
            setEndDataCollectionPeriodNotificationTimes(existing, dataCollectionGroup.getEndDateCreation());
            eventDataCollectionGroupService.update(existing);
        }
    }

    private void setEndDataCollectionPeriodNotificationTimes(final EventDataCollectionGroup event, final Timestamp endDateCreation) {
        event.clearNotificationTimes();
        if(endDateCreation == null) {
            return;
        }

        final Configuration configuration = configurationService.get(ConfigurationKey.END_DATA_COLLECTION_PERIOD_REMINDERS);
        final List<PeriodExpression> periodExpressions = PeriodExpressionsConfigurationMapper.map(configuration);
        LOG.debug("Mail reminders before end of data collection: {}", periodExpressions);

        final Date now = new Date();
        for (final PeriodExpression periodExpression : periodExpressions) {
            final Date notificationTime = DateUtils.remove(endDateCreation, periodExpression);
            if(notificationTime.after(now)) {
                event.addNotificationTime(notificationTime);
            }
        }
    }

    private DataCollectionGroupReplication buildDcgReplication(final DataCollectionGroupListDto dataCollectionGroup) {
        final DataCollectionGroupReplication dcgRep = new DataCollectionGroupReplication();
        dcgRep.setDataCollectionGroupName(dataCollectionGroup.getDataCollectionGroupName());
        dcgRep.setMajorVersion(dataCollectionGroup.getMajorVersion());
        dcgRep.setStartDate(dataCollectionGroup.getStartDate());
        dcgRep.setEndDateCreation(dataCollectionGroup.getEndDateCreation());
        return dcgRep;
    }

    private Version createVersion(Integer majorVersion, Integer minorVersion) {
        return new Version(majorVersion,minorVersion);
    }

    private boolean startDateModified(DataCollectionGroupListDto dataCollectionGroup, DataCollectionGroupReplication dcgRep) {
        return !Objects.equals(dataCollectionGroup.getStartDate(), dcgRep.getStartDate());
    }

    private boolean endDateModified(DataCollectionGroupListDto dataCollectionGroup, DataCollectionGroupReplication dcgRep) {
        return !Objects.equals(dataCollectionGroup.getEndDateCreation(), dcgRep.getEndDateCreation());
    }
}
