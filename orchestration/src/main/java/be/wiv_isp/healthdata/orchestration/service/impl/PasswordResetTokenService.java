/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.mapper.PeriodExpressionConfigurationMapper;
import be.wiv_isp.healthdata.orchestration.domain.PeriodExpression;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.util.DateUtils;
import be.wiv_isp.healthdata.common.domain.enumeration.TimeUnit;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.orchestration.dao.IPasswordResetTokenDao;
import be.wiv_isp.healthdata.orchestration.domain.PasswordResetToken;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.search.PasswordResetTokenSearch;
import be.wiv_isp.healthdata.orchestration.service.IPasswordResetTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@Service
public class PasswordResetTokenService extends AbstractService<PasswordResetToken, Long, PasswordResetTokenSearch, IPasswordResetTokenDao> implements IPasswordResetTokenService {

    @Autowired
    private IPasswordResetTokenDao dao;

    @Autowired
    private IConfigurationService configurationService;

    public PasswordResetTokenService() {
        super(PasswordResetToken.class);
    }

    @Override
    protected IPasswordResetTokenDao getDao() {
        return dao;
    }

    @Override
    @Transactional
    public PasswordResetToken generate(User user) {
        final PasswordResetToken passwordResetToken = new PasswordResetToken();

        String token = UUID.randomUUID().toString();
        final PasswordResetTokenSearch search = new PasswordResetTokenSearch();
        search.setToken(token);
        while(getUnique(search) != null) {
            token = UUID.randomUUID().toString();
            search.setToken(token);
        }

        final PeriodExpression tokenValidityPeriod = PeriodExpressionConfigurationMapper.map(configurationService.get(ConfigurationKey.PASSWORD_RESET_TOKEN_VALIDITY_PERIOD), TimeUnit.SECOND);

        passwordResetToken.setToken(token);
        passwordResetToken.setExpiryDate(new Timestamp(DateUtils.add(new Date(), tokenValidityPeriod).getTime()));
        passwordResetToken.setUser(user);
        return dao.create(passwordResetToken);
    }
}
