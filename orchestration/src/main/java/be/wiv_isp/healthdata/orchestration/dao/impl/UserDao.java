/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.orchestration.dao.IUserDao;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.SetJoin;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Repository
public class UserDao extends CrudDaoV2<User, Long, UserSearch> implements IUserDao {

    public UserDao() {
        super(User.class);
    }

    @Override
    protected List<Predicate> getPredicates(UserSearch search, CriteriaBuilder cb, Root<User> rootEntry) {
        final List<Predicate> predicates = new ArrayList<>();

        if (search.getUsernames() != null) {
            if (search.getUsernames().isEmpty()) {
                predicates.add(cb.or()); // always false
            } else {
                predicates.add(rootEntry.get("username").in(search.getUsernames()));
            }
        }
        if (StringUtils.isNoneBlank(search.getEmail())) {
            predicates.add(cb.equal(rootEntry.get("email"), search.getEmail()));
        }
        if (search.getLdapUser() != null) {
            predicates.add(cb.equal(rootEntry.get("ldapUser"), search.getLdapUser()));
        }
        if (search.getOrganization() != null) {
            predicates.add(cb.equal(rootEntry.get("organization"), search.getOrganization()));
        }
        if (search.getDataCollectionName() != null) {
            final SetJoin<Object, Object> join = rootEntry.joinSet("dataCollectionNames");
            predicates.add(cb.equal(join, search.getDataCollectionName()));
        }
        if (Boolean.TRUE.equals(search.getIncompleteUserInfo())) {
            predicates.add(cb.or(cb.isNull(rootEntry.get("firstName")), cb.isNull(rootEntry.get("lastName")), cb.isNull(rootEntry.get("email"))));
        }
        if (search.getAuthority() != null) {
            predicates.add(cb.isMember(search.getAuthority().getAuthority(), rootEntry.<Set<String>>get("authorities")));
        }
        if (search.getExcludeAuthority() != null) {
            predicates.add(cb.isNotMember(search.getExcludeAuthority().getAuthority(), rootEntry.<Set<String>>get("authorities")));
        }

        return predicates;
    }
}
