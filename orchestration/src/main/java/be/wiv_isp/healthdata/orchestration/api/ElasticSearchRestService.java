/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.common.exception.ExceptionResponse;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionService;
import be.wiv_isp.healthdata.orchestration.service.IElasticSearchService;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractElasticSearchService.AdvancedStatus;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import be.wiv_isp.healthdata.orchestration.tasks.ElasticSearchTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;
import java.util.Objects;

@Component
@Path("/elasticsearch")
public class ElasticSearchRestService {
	
	@Autowired
	private IElasticSearchService elasticSearchService;
	
	@Autowired
	private ElasticSearchTask elasticSearchIndexingTask;

	final private static String ORGANIZATION_ID = "organizationId";
	final private static String DATA_COLLECTION_GROUP = "dataCollectionGroup";

	@POST
	@Path("/rebuild")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Response rebuildIndex(@Context UriInfo info) {

		if(elasticSearchIndexingTask.isRunning()) {
			ExceptionResponse response = new ExceptionResponse();
			response.setError("indexing_in_progress");
			response.setError_description("ElasticSearch indexing task is currently running");
			return Response.status(Response.Status.FORBIDDEN)
					.entity(response)
					.type(MediaType.APPLICATION_JSON_TYPE)
					.build();
		}

		Long organizationId = null;
		String dataCollectionGroup = null;

		final MultivaluedMap<String, String> params = info.getQueryParameters();
		for (String key : params.keySet()) {
			final String value = params.getFirst(key);
			if (ORGANIZATION_ID.equalsIgnoreCase(key))
				organizationId = Long.valueOf(value);
			if (DATA_COLLECTION_GROUP.equalsIgnoreCase(key))
				dataCollectionGroup = value.toUpperCase();
		}

		final Organization userOrg = AuthenticationService.getUserDetails().getOrganization();
		final boolean isAuthorized = userOrg.isMain() || Objects.equals(userOrg.getId(), organizationId);
		if (!isAuthorized) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		if (organizationId == null) {
			elasticSearchService.rebuild();
		}
		else if (dataCollectionGroup == null) {
			elasticSearchService.rebuild(organizationId);
		}
		else {
			elasticSearchService.rebuild(organizationId, dataCollectionGroup);
		}

		return Response.ok().build();
	}

	@POST
	@Path("/repair")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Response repair(@Context UriInfo info) {
		if(elasticSearchIndexingTask.isRunning()) {
			ExceptionResponse response = new ExceptionResponse();
			response.setError("indexing_in_progress");
			response.setError_description("ElasticSearch indexing task is currently running");
			return Response.status(Response.Status.FORBIDDEN)
					.entity(response)
					.type(MediaType.APPLICATION_JSON_TYPE)
					.build();
		}

		Long organizationId = null;
		String dataCollectionGroup = null;
		final MultivaluedMap<String, String> params = info.getQueryParameters();
		for (String key : params.keySet()) {
			final String value = params.getFirst(key);
			if(ORGANIZATION_ID.equalsIgnoreCase(key))
				organizationId = Long.valueOf(value);
			if(DATA_COLLECTION_GROUP.equalsIgnoreCase(key))
				dataCollectionGroup = value.toUpperCase();
		}

		final Organization userOrg = AuthenticationService.getUserDetails().getOrganization();
		final boolean isAuthorized = userOrg.isMain() || Objects.equals(userOrg.getId(), organizationId);
		if (!isAuthorized) {
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

		if (organizationId == null) {
			elasticSearchService.repair();
		}
		else if (dataCollectionGroup == null) {
			elasticSearchService.repair(organizationId);
		}
		else {
			elasticSearchService.repair(organizationId, dataCollectionGroup);
		}

		return Response.ok().build();
	}

	@GET
	@Path("/status")
	public Response status(@Context UriInfo info) {

		Long organizationId = null;
		String dataCollectionGroup = null;

		final MultivaluedMap<String, String> params = info.getQueryParameters();
		for (String key : params.keySet()) {
			final String value = params.getFirst(key);
			if(ORGANIZATION_ID.equalsIgnoreCase(key))
				organizationId = Long.valueOf(value);
			if(DATA_COLLECTION_GROUP.equalsIgnoreCase(key))
				dataCollectionGroup = value.toUpperCase();
		}

		if (organizationId == null) {
			if (elasticSearchService.status()) {
				return Response.noContent().build();
			}
		}
		else {
			if (elasticSearchService.status(organizationId, dataCollectionGroup)) {
				return Response.noContent().build();
			}
		}

		return Response.status(Response.Status.SERVICE_UNAVAILABLE).build();
	}
	
	@GET
	@Path("/status/advanced")
	@Produces(MediaType.APPLICATION_JSON)
	public Response advancedStatus() {
		final AdvancedStatus advancedStatus = elasticSearchService.advancedStatus();
		return Response.ok(advancedStatus).build();
	}
}
