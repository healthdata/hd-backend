/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.service.IEncryptionService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import com.sun.jersey.core.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateException;
import java.text.MessageFormat;

public class EncryptionService implements IEncryptionService {

	private static final String ALGORITHM = "AES";
	private static final String KEY_STORE_TYPE = "JCEKS";
	private static final String KEY_STORE_FILE = "keystore.jceks";
	private static final String KEY_STORE_PASSWORD = "TlRxk8tN6g";
	private static final String HEALTHDATA_KEY_PASSWORD = "m4lcrRUrZM";
	private static final String HEALTHDATA_KEY_ALIAS = "hd-key";

	private static Cipher ENCRYPTION_CIPHER;
	private static Cipher DECRYPTION_CIPHER;

	static {
		final KeyStore keyStore = loadKeyStore();

		final Key key = getKey(keyStore);

		ENCRYPTION_CIPHER = createCipher(key, Cipher.ENCRYPT_MODE);
		DECRYPTION_CIPHER = createCipher(key, Cipher.DECRYPT_MODE);
	}

	private static Cipher createCipher(Key key, int encryptMode) {
		try {
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			cipher.init(encryptMode, key);
			return cipher;
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, "Error while creating encryption cipher");
			throw exception;
		}
	}

	private static KeyStore loadKeyStore() {
		try {
			final KeyStore ks = KeyStore.getInstance(KEY_STORE_TYPE);
			ks.load(EncryptionService.class.getClassLoader().getResourceAsStream(KEY_STORE_FILE), KEY_STORE_PASSWORD.toCharArray());
			return ks;
		} catch (NoSuchAlgorithmException | CertificateException | IOException | KeyStoreException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.ERROR_WHILE_LOADING_KEYSTORE);
			throw exception;
		}
	}

	private static Key getKey(KeyStore keyStore) {
		try {
			return keyStore.getKey(HEALTHDATA_KEY_ALIAS, HEALTHDATA_KEY_PASSWORD.toCharArray());
		} catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.ERROR_WHILE_LOADING_KEY);
			throw exception;
		}
	}

	@Override
	public byte[] encrypt(byte[] bytes) {
		try {
			return ENCRYPTION_CIPHER.doFinal(bytes);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, "Error while encrypting data");
			throw exception;
		}
	}

	@Override
	public byte[] decrypt(byte[] bytes) {
		try {
			return DECRYPTION_CIPHER.doFinal(bytes);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, MessageFormat.format("Error while decrypting data: {0}", new String(Base64.encode(bytes))));
			throw exception;
		}
	}

	@Override
	public String encrypt(String value) {
		if (value == null) {
			return null;
		}

		byte[] encryptedValue = this.encrypt(value.getBytes(StandardCharsets.UTF_8));
		return new String(Base64.encode(encryptedValue), StandardCharsets.UTF_8);
	}

	@Override
	public String decrypt(String base64EncryptedValue) {
		if (base64EncryptedValue == null) {
			return null;
		}

		byte[] encryptedValue = Base64.decode(base64EncryptedValue.getBytes(StandardCharsets.UTF_8));
 		return new String(this.decrypt(encryptedValue), StandardCharsets.UTF_8);
	}
}
