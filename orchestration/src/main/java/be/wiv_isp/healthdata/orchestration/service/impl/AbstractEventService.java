/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *     
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.orchestration.dao.IAbstractEventDao;
import be.wiv_isp.healthdata.orchestration.domain.Event;
import be.wiv_isp.healthdata.orchestration.domain.search.EventSearch;
import be.wiv_isp.healthdata.orchestration.service.IAbstractEventService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

public abstract class AbstractEventService<Entity extends Event, Search extends EventSearch, Dao extends IAbstractEventDao<Entity, Search>> extends AbstractService<Entity, Long, Search, Dao> implements IAbstractEventService<Entity, Search> {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractEventService.class);

    public AbstractEventService(Class<Entity> entityClass) {
        super(entityClass);
    }

    @Override
    @Transactional
    public Entity create(Entity event) {
        if(CollectionUtils.isEmpty(event.getNotificationTimes())) {
            LOG.debug("No notification times specified. Adding current timestamp to notification times before creating event.");
            event.addNotificationTime(new Date());
        }
        LOG.debug("Creating event {}", event);
        return getDao().create(event);
    }

    @Override
    @Transactional
    public Entity update(Entity event) {
        if(CollectionUtils.isEmpty(event.getNotificationTimes())) {
            LOG.debug("No notification times specified. Adding current timestamp to notification times before updating event.");
            event.addNotificationTime(new Date());
        }
        LOG.debug("Updating event {}", event);
        return getDao().update(event);
    }
}
