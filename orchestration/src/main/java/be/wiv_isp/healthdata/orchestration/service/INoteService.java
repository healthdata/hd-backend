/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.service;

import be.wiv_isp.healthdata.common.service.IAbstractService;
import be.wiv_isp.healthdata.orchestration.domain.AbstractDocument;
import be.wiv_isp.healthdata.orchestration.domain.Note;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.domain.search.NoteSearch;
import be.wiv_isp.healthdata.orchestration.dto.NoteDto;
import be.wiv_isp.healthdata.orchestration.dto.NoteMessageDto;

import java.util.List;

public interface INoteService extends IAbstractService<Note, Long, NoteSearch> {

    Note getWithComments(Long noteId);

    Note create(WorkflowType workflowType, AbstractDocument document, NoteDto noteDto, String username);

    Note update(NoteDto noteDto, String username);

    List<Note> getByDocumentId(WorkflowType workflowType, Long documentId);

    Note getByOrder(WorkflowType workflowType, Long documentId, Long order);

    void updateNotes(WorkflowType workflowType, AbstractDocument document, List<Note> currentNotes, List<NoteDto> noteDtos, String username);

    List<Note> updateNotesFromMessage(WorkflowType workflowType, Long documentId, List<NoteMessageDto> noteDtos);

    void migrateNotes(WorkflowType workflowType, AbstractDocument newDocument, List<NoteDto> oldNotes);

    void deleteAllNotes(WorkflowType workflowType, Long documentId);

}
