/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.service.IPlatformService;
import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.orchestration.dao.ICommentDao;
import be.wiv_isp.healthdata.orchestration.dao.INoteDao;
import be.wiv_isp.healthdata.orchestration.dao.IParticipationDocumentDao;
import be.wiv_isp.healthdata.orchestration.dao.IRegistrationDocumentDao;
import be.wiv_isp.healthdata.orchestration.domain.AbstractDocument;
import be.wiv_isp.healthdata.orchestration.domain.Comment;
import be.wiv_isp.healthdata.orchestration.domain.Note;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.domain.search.NoteSearch;
import be.wiv_isp.healthdata.orchestration.dto.CommentDto;
import be.wiv_isp.healthdata.orchestration.dto.CommentMessageDto;
import be.wiv_isp.healthdata.orchestration.dto.NoteDto;
import be.wiv_isp.healthdata.orchestration.dto.NoteMessageDto;
import be.wiv_isp.healthdata.orchestration.service.INoteService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class NoteService extends AbstractService<Note, Long, NoteSearch, INoteDao> implements INoteService {

    @Autowired
    private INoteDao noteDao;
    @Autowired
    private ICommentDao commentDao;
    @Autowired
    private IRegistrationDocumentDao registrationDocumentDao;
    @Autowired
    private IParticipationDocumentDao participationDocumentDao;
    @Autowired
    private IPlatformService platformService;


    public NoteService() {
        super(Note.class);
    }

    @Override
    protected INoteDao getDao() {
        return noteDao;
    }

    @Override
    public Note getWithComments(Long noteId) {
        Note note = get(noteId);
        if (note == null || note.getDeleted())
            return null;

        SortedSet<Comment> comments = new TreeSet<>(commentDao.getByNoteId(noteId));
        note.setComments(comments);

        return note;
    }

    @Override
    public List<Note> getByDocumentId(WorkflowType workflowType, Long documentId) {
        NoteSearch search = new NoteSearch();
        search.setWorkflowType(workflowType);
        search.setDocumentId(documentId);
        search.setDeleted(false);
        List<Note> notes = getAll(search);
        List<Comment> byDocumentId = commentDao.getByDocumentId(documentId);
        Map<Long, SortedSet<Comment>> commentsByNoteId = getCommentsByNoteId(byDocumentId);
        for (Note note : notes) {
            note.setComments(commentsByNoteId.get(note.getId()));
        }
        return notes;
    }

    @Override
    public Note getByOrder(WorkflowType workflowType, Long documentId, Long order) {
        NoteSearch search = new NoteSearch();
        search.setWorkflowType(workflowType);
        search.setDocumentId(documentId);
        search.setOrder(order);
        search.setDeleted(false);
        return getUnique(search);
    }

    @Override
    @Transactional
    public Note create(WorkflowType workflowType, AbstractDocument document, NoteDto noteDto, String username) {
        if (StringUtils.isBlank(username)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.UNAUTHORIZED_COMMENT);
            throw exception;

        }

        return create(workflowType, document, noteDto, username, false);

    }

    private Note create(WorkflowType workflowType, AbstractDocument document, NoteDto noteDto, String username, boolean fullCopy) {

        Long noteMaxOrder = document.getNoteMaxOrder();
        if(noteMaxOrder == null) {
            noteMaxOrder = 0L;
        } else {
            noteMaxOrder += 1L;
        }
        document.setNoteMaxOrder(noteMaxOrder);
        updateDocument(document, workflowType);

        Note note = new Note();
        note.setWorkflowType(workflowType);
        note.setDocumentId(document.getId());
        note.setTitle(noteDto.getTitle());
        note.setFieldId(noteDto.getFieldId());
        note.setFieldPath(noteDto.getFieldPath());
        note.setOrder(noteMaxOrder);
        note.setCommentMaxOrder(0L);
        note.setCreatedOn(noteDto.getCreatedOn());
        note.setResolved(fullCopy ? noteDto.isResolved() : false);
        note.setDeleted(false);
        noteDao.create(note);

        SortedSet<Comment> comments = new TreeSet<>();
        for (CommentDto commentDto : noteDto.getComments()) {
            Comment comment = createComment(note, commentDto, username, fullCopy);
            comments.add(comment);
        }
        note.setComments(comments);

        return note;
    }

    @Override
    @Transactional
    public Note update(NoteDto noteDto, String username) {
        if (StringUtils.isBlank(username)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.UNAUTHORIZED_COMMENT);
            throw exception;
        }

        Note note = noteDao.get(noteDto.getId());
        List<Comment> byDocumentId = commentDao.getByDocumentId(note.getDocumentId());
        note.setComments(getCommentsByNoteId(byDocumentId).get(note.getId()));

        for (CommentDto commentDto : noteDto.getComments()) {
            Comment comment = getComment(note.getComments(), commentDto.getId());
            if (comment!=null && !username.equals(comment.getUsername()) && !comment.getContent().equals(commentDto.getContent())) {// edit comment
                HealthDataException exception = new HealthDataException();
                exception.setExceptionType(ExceptionType.UNAUTHORIZED_COMMENT);
                throw exception;
            }
        }
        for (Comment comment : note.getComments()) {
            if (getCommentDto(noteDto.getComments(), comment.getId()) == null) {
                if (!username.equals(comment.getUsername())) { // delete comment
                    HealthDataException exception = new HealthDataException();
                    exception.setExceptionType(ExceptionType.UNAUTHORIZED_COMMENT);
                    throw exception;
                }
            }
        }

        if (noteDto.getTitle() != null)
            note.setTitle(noteDto.getTitle());
        if (noteDto.isResolved() != null)
            note.setResolved(noteDto.isResolved());
        for (CommentDto commentDto : noteDto.getComments()) {
            if (commentDto.getId() == null) {
                createComment(note, commentDto, username, false);
            }
            else {
                Comment comment = getComment(note.getComments(), commentDto.getId());
                if (comment != null) {
                    comment.setContent(commentDto.getContent());
                    commentDao.update(comment);
                }
            }
        }
        for (Comment comment : note.getComments()) {
            if (getCommentDto(noteDto.getComments(), comment.getId()) == null) {
                comment.setDeleted(true);
                commentDao.update(comment);
            }
        }
        Note updatedNote = noteDao.update(note);

        byDocumentId = commentDao.getByDocumentId(note.getDocumentId());
        updatedNote.setComments(getCommentsByNoteId(byDocumentId).get(note.getId()));

        return updatedNote;
    }

    @Override
    @Transactional
    public List<Note> updateNotesFromMessage(WorkflowType workflowType, Long documentId, List<NoteMessageDto> noteDtos) {
        Map<Long, Note> notesByOrder = getNotesByOrder(getByDocumentId(workflowType, documentId));
        List<Note> updatedNotes = new ArrayList<>();
        for(final NoteMessageDto noteDto : noteDtos) {
            Note note = notesByOrder.get(noteDto.getOrder());
            if(note == null) {
                note = createNote(workflowType, documentId, noteDto);
            } else {
                updateNotes(note, noteDto);
            }
            updatedNotes.add(note);
        }
        return updatedNotes;
    }

    @Override
    @Transactional
    public void updateNotes(WorkflowType workflowType, AbstractDocument document, List<Note> currentNotes, List<NoteDto> noteDtos, String username) {

        // check if any operation is unauthorized before updating anything
        if (StringUtils.isBlank(username)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.UNAUTHORIZED_COMMENT);
            throw exception;
        }
        for (final NoteDto noteDto : noteDtos) {
            Note note = getNote(currentNotes, noteDto.getId());
            if (note != null) {
                if (note.getTitle()!=null && noteDto.getTitle()!=null && !note.getTitle().equals(noteDto.getTitle())) { // edit title
                    for (Comment comment : note.getComments()) {
                        if (!username.equals(comment.getUsername())) {
                            HealthDataException exception = new HealthDataException();
                            exception.setExceptionType(ExceptionType.UNAUTHORIZED_COMMENT);
                            throw exception;
                        }
                    }
                }
                for (CommentDto commentDto : noteDto.getComments()) {
                    Comment comment = getComment(note.getComments(), commentDto.getId());
                    if (comment!=null && !username.equals(comment.getUsername()) && !comment.getContent().equals(commentDto.getContent())) { // edit comment
                        HealthDataException exception = new HealthDataException();
                        exception.setExceptionType(ExceptionType.UNAUTHORIZED_COMMENT);
                        throw exception;
                    }
                }
                for (Comment comment : note.getComments()) {
                    if (getCommentDto(noteDto.getComments(), comment.getId()) == null) {
                        if (!username.equals(comment.getUsername())) { // delete comment
                            HealthDataException exception = new HealthDataException();
                            exception.setExceptionType(ExceptionType.UNAUTHORIZED_COMMENT);
                            throw exception;
                        }
                    }
                }
            }
        }
        for (Note note : currentNotes) {
            if (getNoteDto(noteDtos, note.getId()) == null) { // delete note
                if (note.getComments().size() == 1) {
                    if (!username.equals(note.getComments().first().getUsername())) {
                        HealthDataException exception = new HealthDataException();
                        exception.setExceptionType(ExceptionType.UNAUTHORIZED_COMMENT);
                        throw exception;
                    }
                }
                else if (note.getComments().size() > 1) {
                    HealthDataException exception = new HealthDataException();
                    exception.setExceptionType(ExceptionType.UNAUTHORIZED_COMMENT);
                    throw exception;
                }
            }
        }

        // perform the operations
        for (final NoteDto noteDto : noteDtos) {
            Note note = getNote(currentNotes, noteDto.getId());
            if (note == null) {
                create(workflowType, document, noteDto, username, false);
            } else {
                if (noteDto.getTitle() != null)
                    note.setTitle(noteDto.getTitle());
                if (noteDto.isResolved() != null)
                    note.setResolved(noteDto.isResolved());
                for (CommentDto commentDto : noteDto.getComments()) {
                    Comment comment = getComment(note.getComments(), commentDto.getId());
                    if (comment == null) {
                        createComment(note, commentDto, username, false);
                    }
                    else {
                        comment.setContent(commentDto.getContent());
                        commentDao.update(comment);
                    }
                }
                for (Comment comment : note.getComments()) {
                    if (getCommentDto(noteDto.getComments(), comment.getId()) == null) {
                        comment.setDeleted(true);
                        commentDao.update(comment);
                    }
                }
                noteDao.update(note);
            }
        }

        for (Note note : currentNotes) {
            if (getNoteDto(noteDtos, note.getId()) == null) {
                if (note.getComments().size() == 1) { // already verified to be 1 or 0
                    Comment onlyComment = note.getComments().first();
                    onlyComment.setDeleted(true);
                    commentDao.update(onlyComment);
                }
                note.setDeleted(true);
                noteDao.update(note);
            }
        }
    }

    @Override
    @Transactional
    public void migrateNotes(WorkflowType workflowType, AbstractDocument newDocument, List<NoteDto> oldNotes) {
        for (NoteDto oldNote : oldNotes) {
            create(workflowType, newDocument, oldNote, null, true);
        }
    }

    @Override
    @Transactional
    public void deleteAllNotes(WorkflowType workflowType, Long documentId) {
        List<Note> notes = getByDocumentId(workflowType, documentId);
        for (Note note : notes) {
            note.setDeleted(true);
            noteDao.update(note);
        }
    }

    private Note createNote(WorkflowType workflowType, Long documentId, NoteMessageDto noteDto) {
        Note note = new Note();
        note.setWorkflowType(workflowType);
        note.setDocumentId(documentId);
        note.setOrder(noteDto.getOrder());
        note.setCommentMaxOrder(noteDto.getCommentMaxOrder());
        note.setTitle(noteDto.getTitle());
        note.setResolved(noteDto.isResolved());
        note.setFieldPath(noteDto.getFieldPath());
        note.setFieldId(noteDto.getFieldId());
        note.setCreatedOn(noteDto.getCreatedOn());
        note.setDeleted(false);
        noteDao.create(note);

        SortedSet<Comment> comments = new TreeSet<>();
        SortedSet<CommentMessageDto> commentDtos = noteDto.getComments();
        if(commentDtos != null) {
            for (CommentMessageDto commentDto : commentDtos) {
                Comment comment = createComment(note, commentDto);
                comments.add(comment);
            }
        }
        note.setComments(comments);
        return note;
    }

    private void updateNotes(Note note, NoteMessageDto noteDto) {
        note.setCommentMaxOrder(noteDto.getCommentMaxOrder());
        note.setTitle(noteDto.getTitle());
        note.setResolved(noteDto.isResolved());
        note.setFieldPath(noteDto.getFieldPath());
        note.setFieldId(noteDto.getFieldId());
        noteDao.update(note);
        updateComments(note, noteDto);
    }

    private Comment createComment(Note note, CommentMessageDto commentDto) {
        Comment comment = new Comment();
        comment.setNoteId(note.getId());
        comment.setPlatform(commentDto.getPlatform());
        comment.setUsername(commentDto.getUsername());
        comment.setContent(commentDto.getContent());
        comment.setOrder(commentDto.getOrder());
        comment.setCreatedOn(commentDto.getCreatedOn());
        comment.setDeleted(commentDto.getDeleted());
        commentDao.create(comment);
        return comment;
    }

    private void updateComments(Note note, NoteMessageDto noteDto) {
        Map<Long, Comment> commentsByOrder = getCommentsByOrder(note.getComments());
        SortedSet<CommentMessageDto> comments = noteDto.getComments();
        for (CommentMessageDto commentDto : comments) {
            Comment comment = commentsByOrder.get(commentDto.getOrder());
            if(comment == null) {
                comment = createComment(note, commentDto);
                note.getComments().add(comment);
            } else {
                updateComment(comment, commentDto);
            }
        }
    }

    private Comment updateComment(Comment comment, CommentMessageDto commentDto) {
        comment.setContent(commentDto.getContent());
        comment.setDeleted(commentDto.getDeleted());
        commentDao.update(comment);
        return comment;
    }

    private Comment createComment(Note note, CommentDto commentDto, String username, boolean fullCopy) {
        Comment comment = new Comment();
        comment.setNoteId(note.getId());
        Long commentMaxOrder = note.getCommentMaxOrder();
        if (commentMaxOrder == null)
            commentMaxOrder = 0L;
        else
            commentMaxOrder += 1L;
        note.setCommentMaxOrder(commentMaxOrder);
        comment.setOrder(commentMaxOrder);
        comment.setPlatform(fullCopy ? (commentDto.getPlatform()==null ? null : Platform.valueOf(commentDto.getPlatform().toUpperCase())) : platformService.getCurrentPlatform());
        comment.setUsername(fullCopy ? commentDto.getUsername() : username);
        comment.setCreatedOn(commentDto.getCreatedOn());
        comment.setContent(commentDto.getContent());
        comment.setDeleted(false);
        commentDao.create(comment);

        return comment;
    }


    private NoteDto getNoteDto(List<NoteDto> notes, Long id) {
        if (id == null)
            return null;
        for (NoteDto noteDto : notes) {
            if(id.equals(noteDto.getId())) {
                return noteDto;
            }
        }
        return null;
    }

    private Note getNote(List<Note> notes, Long id) {
        if (id == null)
            return null;
        for (Note note : notes) {
            if(id.equals(note.getId())) {
                return note;
            }
        }
        return null;
    }

    private Comment getComment(SortedSet<Comment> comments, Long id) {
        if (id == null)
            return null;
        for (Comment comment : comments) {
            if(id.equals(comment.getId())) {
                return comment;
            }
        }
        return null;
    }

    private CommentDto getCommentDto(SortedSet<CommentDto> comments, Long id) {
        if (id == null)
            return null;
        for (CommentDto comment : comments) {
            if(id.equals(comment.getId())) {
                return comment;
            }
        }
        return null;
    }

    private Map<Long, Note> getNotesByOrder(List<Note> notes) {
        Map<Long, Note> result = new HashMap<>();
        for (Note note : notes) {
            result.put(note.getOrder(), note);
        }
        return result;
    }

    private Map<Long, Comment> getCommentsByOrder(SortedSet<Comment> comments) {
        Map<Long, Comment> result = new HashMap<>();
        for (Comment comment : comments) {
            result.put(comment.getOrder(), comment);
        }
        return result;
    }

    private Map<Long, SortedSet<Comment>> getCommentsByNoteId(Collection<Comment> comments) {
        Map<Long, SortedSet<Comment>> result = new HashMap<>();
        for (Comment comment : comments) {
            SortedSet<Comment> commentsForNoteId = result.get(comment.getNoteId());
            if(commentsForNoteId == null) {
                commentsForNoteId = new TreeSet<>();
                result.put(comment.getNoteId(), commentsForNoteId);
            }
            commentsForNoteId.add(comment);
        }
        return result;
    }

    private void updateDocument(AbstractDocument document, WorkflowType workflowType) {
        switch (workflowType) {
            case REGISTRATION:
                registrationDocumentDao.update(document);
                break;
            case PARTICIPATION:
                participationDocumentDao.update(document);
                break;
            default:
                break;
        }
    }

}
