/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.action.workflow.executor;

import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.action.workflow.WorkflowAction;
import be.wiv_isp.healthdata.orchestration.domain.AbstractWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;

abstract public class AbstractWorkflowActionExecutor<Workflow extends AbstractWorkflow> implements IWorkflowActionExecutor<Workflow> {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractWorkflowActionExecutor.class);

    @Override
    public Workflow execute(final WorkflowAction<Workflow> action, final HDUserDetails userDetails) {
        return execute(action, userDetails, null);
    }

    @Override
    public Workflow execute(final WorkflowAction<Workflow> action, final HDUserDetails userDetails, Workflow workflow) {
        if (workflow == null) {
            workflow = action.retrieveWorkflow();
        }

        if (action.getEndStatus() == null) {
            LOG.info("{}[{}] - User[{}]: start executing action {}", getWorkflowType().getLabel(), getIdentifier(workflow), userDetails.getUsername(), action.getAction());
        } else {
            LOG.info("{}[{}] - User[{}]: start executing action {} (end status: {})", getWorkflowType().getLabel(), getIdentifier(workflow), userDetails.getUsername(), action.getAction(), action.getEndStatus());
        }

        try {
            workflow = action.preExecute(workflow, userDetails);
            workflow = action.execute(workflow, userDetails);
            workflow = action.postExecute(workflow, userDetails);
        } catch (Exception e) {
            if (e instanceof HealthDataException && e.getMessage().startsWith("Ignoring action, reason:")) {
                LOG.info("{}[{}] - User[{}]: action {} successfully executed (end status: {}) ", getWorkflowType().getLabel(), getIdentifier(workflow), userDetails.getUsername(), action.getAction(), action.getEndStatus());
            } else {
                LOG.error(MessageFormat.format("{0}[{1}] - User[{2}]: error while executing action {3}. Cause: {4}", getWorkflowType().getLabel(), getIdentifier(workflow), userDetails.getUsername(), action.getAction(), e.getMessage()), e);
                throw e;
            }
        }

        return workflow;
    }

    private String getIdentifier(Workflow workflow) {
        if(workflow == null || workflow.getId() == null) {
            return "NEW";
        }
        return workflow.getId().toString();
    }

    abstract protected WorkflowType getWorkflowType();
}
