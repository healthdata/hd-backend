/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.api.ClientVersion;
import be.wiv_isp.healthdata.common.caching.CacheManagementService;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupDto;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupListMap;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.orchestration.api.uri.DataCollectionGroupUri;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionGroupForwardService;
import be.wiv_isp.healthdata.orchestration.service.IWebServiceClientService;
import com.sun.jersey.api.client.GenericType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;

import javax.ws.rs.core.MediaType;

abstract public class AbstractDataCollectionGroupForwardService implements IDataCollectionGroupForwardService {

	@Autowired
	protected IConfigurationService configurationService;
	@Autowired
	protected IWebServiceClientService webServiceClientService;

	@Override
	@Cacheable(value = CacheManagementService.catalogueCache)
	public DataCollectionGroupDto get(long id) {
		final Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

		final DataCollectionGroupUri dataCollectionGroupUri= new DataCollectionGroupUri(catalogueHost.getValue(), id);

		final WebServiceBuilder wsb = new WebServiceBuilder();

		wsb.setUrl(dataCollectionGroupUri.toString());
		wsb.setGet(true);
		wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb.setReturnType(new GenericType<DataCollectionGroupDto>() {});
		wsb.addHeader(HttpHeaders.CLIENT_VERSION, ClientVersion.DataCollectionDefinition.getDefault());
		wsb.setLogResponseBody(false);

		return (DataCollectionGroupDto) webServiceClientService.callWebService(wsb);
	}

	@Override
	@Cacheable(value = CacheManagementService.catalogueCache)
	public DataCollectionGroupDto get(String groupName, Integer majorVersion) {
		final Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

		final DataCollectionGroupUri dataCollectionGroupUri= new DataCollectionGroupUri(catalogueHost.getValue(), groupName, majorVersion);

		final WebServiceBuilder wsb = new WebServiceBuilder();

		wsb.setUrl(dataCollectionGroupUri.toString());
		wsb.setGet(true);
		wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb.setReturnType(new GenericType<DataCollectionGroupDto>() {});
		wsb.addHeader(HttpHeaders.CLIENT_VERSION, ClientVersion.DataCollectionDefinition.getDefault());
		wsb.setLogResponseBody(false);

		return (DataCollectionGroupDto) webServiceClientService.callWebService(wsb);
	}

	@Override
	@Cacheable(value = CacheManagementService.catalogueCache)
	public DataCollectionGroupListMap list(Organization organization) {
		return list(organization, null, null);
	}
}
