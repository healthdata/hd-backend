/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;


import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "DCG_REPLICATIONS")
public class DataCollectionGroupReplication {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "DATA_COLLECTION_GROUP_NAME", nullable = false)
    private String dataCollectionGroupName;

    @Column(name = "MAJOR_VERSION", nullable = false)
    private int majorVersion;

    @Column(name = "START_DATE")
    private Timestamp startDate;

    @Column(name = "END_DATE_CREATION")
    private Timestamp endDateCreation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDataCollectionGroupName() {
        return dataCollectionGroupName;
    }

    public void setDataCollectionGroupName(String dataCollectionGroupName) {
        this.dataCollectionGroupName = dataCollectionGroupName;
    }

    public int getMajorVersion() {
        return majorVersion;
    }

    public void setMajorVersion(int majorVersion) {
        this.majorVersion = majorVersion;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDateCreation() {
        return endDateCreation;
    }

    public void setEndDateCreation(Timestamp endDateCreation) {
        this.endDateCreation = endDateCreation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DataCollectionGroupReplication)) return false;

        DataCollectionGroupReplication that = (DataCollectionGroupReplication) o;

        if (majorVersion != that.majorVersion) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (dataCollectionGroupName != null ? !dataCollectionGroupName.equals(that.dataCollectionGroupName) : that.dataCollectionGroupName != null)
            return false;
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;
        return !(endDateCreation != null ? !endDateCreation.equals(that.endDateCreation) : that.endDateCreation != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (dataCollectionGroupName != null ? dataCollectionGroupName.hashCode() : 0);
        result = 31 * result + majorVersion;
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDateCreation != null ? endDateCreation.hashCode() : 0);
        return result;
    }
}
