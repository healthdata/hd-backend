/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.domain.LdapUser;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import be.wiv_isp.healthdata.orchestration.dto.UserDto;
import be.wiv_isp.healthdata.orchestration.security.ldap.service.ILdapSearchService;
import be.wiv_isp.healthdata.orchestration.security.ldap.service.ILdapService;
import be.wiv_isp.healthdata.orchestration.service.IUserService;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import com.sun.jersey.api.client.ClientResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@Component
@Path("/ldap")
public class LdapRestService {

	private static final Logger LOG = LoggerFactory.getLogger(LdapRestService.class);

	public static final int COUNT_LIMIT = 50;

	@Autowired
	private ILdapSearchService ldapSearchService;

	@Autowired
	private ILdapService ldapService;

	@Autowired
	private IUserService userService;

	@GET
	@Path("/users")
	@Produces(MediaType.APPLICATION_JSON)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Response search(@QueryParam("search") String searchTerm) {
		final Organization organization = AuthenticationService.getUserDetails().getOrganization();
		final List<LdapUser> ldapUsers = ldapService.getUsers(searchTerm, organization, COUNT_LIMIT);

		LOG.debug("Retrieving users that were already migrated from the database");
		final UserSearch search = new UserSearch();
		search.setLdapUser(true);
		search.setOrganization(organization);
		search.setUsernames(new ArrayList<String>());
		for(final LdapUser ldapUser : ldapUsers) {
			search.addUsername(ldapUser.getUsername());
		}
		final List<User> migratedUsers = userService.getAll(search);
		LOG.debug(MessageFormat.format("{0} migrated users retrieved", migratedUsers.size()));

		final List<UserDto> nonMigratedUserDtos = convertWithoutMigratedUsers(ldapUsers, migratedUsers);
		final List<UserDto> migratedUsersDtos = convert(migratedUsers);

		final List<UserDto> result = new ArrayList<>();
		result.addAll(migratedUsersDtos);
		result.addAll(nonMigratedUserDtos);

		if(result.size() == COUNT_LIMIT) {
			return Response.ok(result).status(ClientResponse.Status.PARTIAL_CONTENT).build();
		}
		return Response.ok(result).build();
	}

	@GET
	@Path("/users/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Response getUser(@PathParam("username") String username) {
		final Organization organization = AuthenticationService.getUserDetails().getOrganization();
		final LdapUser ldapUser = ldapService.getUser(username, organization);

		if(ldapUser == null) {
			return Response.status(Status.NOT_FOUND).build();
		}

		final UserDto userDto = createUserDto(ldapUser);
		return Response.ok(userDto).build();
	}

	private List<UserDto> convertWithoutMigratedUsers(List<LdapUser> ldapUsers, List<User> migratedUsers) {
		LOG.debug("Filtering the migrated users out of the user list returned by the LDAP server");
		final List<UserDto> dtos = new ArrayList<>();
		for (final LdapUser ldapUser : ldapUsers) {
			final String username = ldapUser.getUsername();
			if(!contains(migratedUsers, username)) {
				dtos.add(createUserDto(ldapUser));
			}
		}
		return dtos;
	}

	private UserDto createUserDto(LdapUser ldapUser) {
		final UserDto dto = new UserDto();
		dto.setUsername(ldapUser.getUsername());
		dto.setLastName(ldapUser.getLastname());
		dto.setFirstName(ldapUser.getFirstname());
		dto.setEmail(ldapUser.getEmail());
		return dto;
	}

	private boolean contains(List<User> users, String username) {
		for(final User u : users) {
			if(username.equals(u.getUsername())) {
				return true;
			}
		}
		return false;
	}

	private List<UserDto> convert(List<User> users) {
		final List<UserDto> dtos = new ArrayList<>();
		for (User user : users) {
			dtos.add(new UserDto(user));
		}
		return dtos;
	}

	@GET
	@Path("/status")
	@Produces(MediaType.APPLICATION_JSON)
	public Response status() {
		try {
			ldapSearchService.connectionStatus();
			return Response.noContent().build();
		} catch (HealthDataException e) {
			LOG.debug("Error while connecting to the LDAP server", e);
			return Response.status(Status.SERVICE_UNAVAILABLE).build();
		}
	}
}
