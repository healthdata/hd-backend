/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.common.util.StringUtils;
import be.wiv_isp.healthdata.common.util.ZipUtils;
import org.apache.commons.codec.digest.DigestUtils;

import javax.persistence.*;

@Entity
@Table(name = "ATTACHMENT_CONTENTS")
public class AttachmentContent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CONTENT_ID")
    private Long id;

    @Lob
    @Access(AccessType.PROPERTY)
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "CONTENT", nullable = false)
    private byte[] content;

    @Access(AccessType.PROPERTY)
    @Column(name = "HASH", nullable = false)
    private String hash;

    @Access(AccessType.PROPERTY)
    @Column(name = "\"SIZE\"", nullable = false) // SIZE is a reserved keyword in Oracle, hence it must be quoted
    private Integer size;

    private transient byte[] zippedContent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getContent() {
        if(content == null && zippedContent != null) {
            content = ZipUtils.decompress(zippedContent);
        }
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
        this.hash = null;
        this.size = null;
    }

    public String getHash() {
        if(hash == null) {
            computeHash();
        }
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Integer getSize() {
        if(size == null) {
            computeSize();
        }
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public byte[] getZippedContent() {
        if(zippedContent == null && content != null) {
            zippedContent = ZipUtils.compress(content);
        }
        return zippedContent;
    }

    public void setZippedContent(byte[] zippedContent) {
        this.zippedContent = zippedContent;
    }

    private void computeHash() {
        if(getContent() != null) {
            hash = DigestUtils.md5Hex(getContent());
        }
    }

    private void computeSize() {
        if(getContent() != null) {
            size = getContent().length;
        }
    }

    @Override
    public String toString() {
        return StringUtils.toStringExclude(this, "content", "zippedContent");
    }
}
