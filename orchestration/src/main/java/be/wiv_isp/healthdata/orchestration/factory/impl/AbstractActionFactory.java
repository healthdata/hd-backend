/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.factory.impl;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.orchestration.action.Action;
import be.wiv_isp.healthdata.orchestration.action.StatusMessageContent;
import be.wiv_isp.healthdata.orchestration.action.dto.Hd4prcCreateDto;
import be.wiv_isp.healthdata.orchestration.action.dto.UserRequestHd4resActionDto;
import be.wiv_isp.healthdata.orchestration.action.dto.UserUpdateHd4resActionDto;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.dto.AttachmentDto;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import be.wiv_isp.healthdata.orchestration.dto.NoteDto;
import be.wiv_isp.healthdata.orchestration.dto.NoteMessageDto;
import be.wiv_isp.healthdata.orchestration.factory.IActionFactory;
import be.wiv_isp.healthdata.orchestration.jaxb.json.JsonUnmarshaller;
import be.wiv_isp.healthdata.orchestration.service.IAutowireService;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.*;

public abstract class AbstractActionFactory implements IActionFactory {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractActionFactory.class);

	private static final String ACTION = "action";
	private static final String WORKFLOW_TYPE = "workflowType";

	@Autowired
	private IAutowireService autowireService;

	@Override
	public Action getActionByActionType(final ActionType actionType, final WorkflowType workflowType) {
		return getActionByActionType(actionType.toString(), workflowType);
	}

	private Action getActionByActionType(final String actionType, final WorkflowType workflowType) {
		final List<Action> actions = getActionList(workflowType);

		for (final Action action : actions) {
			if (action.getAction().toString().equals(actionType)) {
				return autowireService.autowire(action);
			}
		}
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.INVALID_ACTION, actionType);
		throw exception;
	}

	@Override
	public Action createAction(final byte[] json) {
		LOG.debug("Creating action from string content: {}", new String(json, StandardCharsets.UTF_8));

		final JSONObject jsonObject = JsonUtils.createJsonObject(json);

		final String actionTypeString = JsonUtils.getString(jsonObject, ACTION);
		if(actionTypeString == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.MISSING_PROPERTY, ACTION);
			throw exception;
		}
		final String actionType = actionTypeString.toUpperCase();

		WorkflowType workflowType = WorkflowType.REGISTRATION;
		final String workflowTypeString = JsonUtils.getString(jsonObject, WORKFLOW_TYPE);
		if(workflowTypeString != null) {
			workflowType = WorkflowType.getValue(workflowTypeString.toUpperCase());
		}

		LOG.debug("Action type: {}", actionType);
		final Action action = getActionByActionType(actionType, workflowType);
		mapProperties(jsonObject, action);

		return action;
	}

	private void mapProperties(JSONObject jsonObject, Action action) {
		LOG.debug("Mapping json attributes to Action object attributes");

		if (jsonObject.names() == null) {
			LOG.debug("Json object contains no attributes");
			return;
		}

		final Iterator<String> it = jsonObject.keys();
		while(it.hasNext()) {
            final String key = it.next();
			String actionProperty = key;
            if (ACTION.equalsIgnoreCase(key)) {
                continue;
            }
            if (WORKFLOW_TYPE.equalsIgnoreCase(key)) {
                continue;
            }
            if (jsonObject.isNull(key)) {
                continue;
            }

            Object value;
            if ("workflowId".equalsIgnoreCase(key)) {
                value = JsonUtils.getLong(jsonObject, key);
            } else if ("dataCollectionDefinitionId".equalsIgnoreCase(key)) {
                value = JsonUtils.getLong(jsonObject, key);
            } else if ("dataCollectionName".equalsIgnoreCase(key)) {
				value = JsonUtils.getString(jsonObject, key);
			} else if ("endStatus".equalsIgnoreCase(key)) {
                value = WorkflowStatus.valueOf(JsonUtils.getString(jsonObject, key));
            } else if ("documentContent".equalsIgnoreCase(key) || "csv".equalsIgnoreCase(key)) {
                value = JsonUtils.getString(jsonObject, key).getBytes(StandardCharsets.UTF_8);
            } else if ("content".equalsIgnoreCase(key)) { // status message action
				value = StatusMessageContent.create(JsonUtils.getString(jsonObject, key));
            } else if ("metaData".equalsIgnoreCase(key)) {
				value = JsonUnmarshaller.unmarshal(JsonUtils.getString(jsonObject, key), HashMap.class);

				final Map<String, String> metaData = (Map<String, String>) value;
                final Set<String> keysToRemove = new HashSet<>();
                for (Map.Entry<String, String> entry : metaData.entrySet()) {
                    if (StringUtils.isBlank(entry.getValue())) {
                        keysToRemove.add(entry.getKey());
                    }
                }
                for (String keyToRemove : keysToRemove) {
                    metaData.remove(keyToRemove);
                }
            } else if ("documentData".equalsIgnoreCase(key)) { // new format >= v1.8.0
				final JSONObject docDataObject = JsonUtils.getJSONObject(jsonObject, key);
				final Iterator<String> docDataIt = docDataObject.keys();
				DocumentData documentData = new DocumentData();
				while (docDataIt.hasNext()) {
					final String docDataKey = docDataIt.next();
					if ("content".equalsIgnoreCase(docDataKey)) {
						documentData.setContent(JsonUtils.getString(docDataObject, docDataKey).getBytes(StandardCharsets.UTF_8));
					} else if ("patientID".equalsIgnoreCase(docDataKey) && !docDataObject.isNull(docDataKey)) {
						final Map<String, String> patiendId = (Map<String, String>) JsonUnmarshaller.unmarshal(JsonUtils.getString(docDataObject, docDataKey), HashMap.class);
						final Set<String> keysToRemove = new HashSet<>();
						for (Map.Entry<String, String> entry : patiendId.entrySet()) {
							if (StringUtils.isBlank(entry.getValue()))
								keysToRemove.add(entry.getKey());
						}
						for (String keyToRemove : keysToRemove) {
							patiendId.remove(keyToRemove);
						}
						documentData.setPatientID(patiendId);
					} else if ("coded".equalsIgnoreCase(docDataKey) && !docDataObject.isNull(docDataKey)) {
						documentData.setCoded(mapMapStringTypedValue(JsonUtils.getJSONObject(docDataObject, docDataKey)));
					} else if ("private".equalsIgnoreCase(docDataKey) && !docDataObject.isNull(docDataKey)) {
						documentData.setPrivate(JsonUtils.getString(docDataObject, docDataKey).getBytes(StandardCharsets.UTF_8));
					}
				}
				value = documentData;
			} else if ("toBeCoded".equalsIgnoreCase(key)) {
				value = mapMapStringTypedValue(JsonUtils.getJSONObject(jsonObject, key));
			} else if ("identification".equalsIgnoreCase(key)) {
                value = JsonUnmarshaller.unmarshal(JsonUtils.getString(jsonObject, key), Identification.class);
            } else if ("identificationWorkflow".equalsIgnoreCase(key) || "healthDataIdentification".equalsIgnoreCase(key)) {
                value = JsonUnmarshaller.unmarshal(JsonUtils.getString(jsonObject, key), HealthDataIdentification.class);
            } else if ("progress".equalsIgnoreCase(key)) {
                value = JsonUnmarshaller.unmarshal(JsonUtils.getString(jsonObject, key), Progress.class);
            } else if ("userRequestDto".equalsIgnoreCase(key)) {
                value = JsonUnmarshaller.unmarshal(JsonUtils.getString(jsonObject, key), UserRequestHd4resActionDto.class);
			} else if ("userUpdateDto".equalsIgnoreCase(key)) {
				value = JsonUnmarshaller.unmarshal(JsonUtils.getString(jsonObject, key), UserUpdateHd4resActionDto.class);
			} else if ("flags".equalsIgnoreCase(key)) {
				value = JsonUnmarshaller.unmarshal(JsonUtils.getString(jsonObject, key), WorkflowFlags.class);
			} else if ("sentOn".equalsIgnoreCase(key) ||"startedOn".equalsIgnoreCase(key) || "completedOn".equalsIgnoreCase(key)) {
				value = DateFormat.DATE_AND_TIME.parse(JsonUtils.getString(jsonObject, key));
            } else if ("documentVersion".equalsIgnoreCase(key)) {
				value = JsonUtils.getLong(jsonObject, key);
			} else if ("computedExpressions".equalsIgnoreCase(key)) {
				value = JsonUtils.getJSONObject(jsonObject, key);
			} else if ("followUps".equalsIgnoreCase(key)) {
				final JSONArray array = JsonUtils.getJSONArray(jsonObject, key);
				value = JsonUnmarshaller.unmarshal(array, getFollowUpClass());
			} else if ("successes".equalsIgnoreCase(key) || "failures".equalsIgnoreCase(key)) {
				final JSONArray array = JsonUtils.getJSONArray(jsonObject, key);
				value = JsonUnmarshaller.unmarshal(array, Long.class);
			} else if ("emails".equalsIgnoreCase(key)) {
				final JSONArray array = JsonUtils.getJSONArray(jsonObject, key);
				value = new HashSet<>(JsonUnmarshaller.unmarshal(array, String.class));
			} else if ("attachments".equalsIgnoreCase(key)) {
				// support for version < Q1-2017
				actionProperty = "attachmentDtos";
				final JSONArray array = JsonUtils.getJSONArray(jsonObject, key);
				value = JsonUnmarshaller.unmarshal(array, AttachmentDto.class);
			} else if ("attachmentDtos".equalsIgnoreCase(key)) {
				final JSONArray array = JsonUtils.getJSONArray(jsonObject, key);
				value = JsonUnmarshaller.unmarshal(array, AttachmentDto.class);
			} else if ("hd4prcCreateDto".equalsIgnoreCase(key)) {
				value = JsonUnmarshaller.unmarshal(JsonUtils.getString(jsonObject, key), Hd4prcCreateDto.class);
			} else if ("notes".equalsIgnoreCase(key)) {
				final JSONArray array = JsonUtils.getJSONArray(jsonObject, key);
				value = JsonUnmarshaller.unmarshal(array, NoteDto.class);
			} else if ("notesMessage".equalsIgnoreCase(key)) {
				final JSONArray array = JsonUtils.getJSONArray(jsonObject, key);
				value = JsonUnmarshaller.unmarshal(array, NoteMessageDto.class);
			} else if ("affectedFields".equalsIgnoreCase(key)) {
            	value = mapMapStringJsonObject(JsonUtils.getJSONObject(jsonObject, key));
			} else if ("dataSources".equalsIgnoreCase(key)) {
				value = JsonUnmarshaller.unmarshal(JsonUtils.getString(jsonObject, key), HashMap.class);
			} else {
                value = JsonUtils.getString(jsonObject, key);
            }

            try {
                BeanUtils.setProperty(action, actionProperty, value);
            } catch (IllegalAccessException | InvocationTargetException e) {
                LOG.warn(MessageFormat.format("could not map property [{0}]: {1}", key, e.getMessage()));
            }
        }
	}

	@Override
	public Action create(final Message message) {
		LOG.debug("Creating action from message with id {}", message.getId());
		final CSVContent csvContent = new CSVContent(message.getContent());

		final Action action = createAction(csvContent.getLastField());
		action.extractInfo(message);

		return action;
	}

	private Map<String,TypedValue> mapMapStringTypedValue(JSONObject jsonObject) {
		Map<String,TypedValue> map = new HashMap<>();
		try {
			final Iterator<String> it = jsonObject.keys();
			while (it.hasNext()) {
				String key = it.next();
				JSONObject jsonTypedValue = JsonUtils.getJSONObject(jsonObject, key);
				map.put(key, new TypedValue(jsonTypedValue.getString("value"), jsonTypedValue.getString("type")));
			}
		}
		catch (JSONException e) {
			return null;
		}
		return map;
	}

	private Map<String,JSONObject> mapMapStringJsonObject(JSONObject jsonObject) {
		Map<String,JSONObject> map = new HashMap<>();
		final Iterator<String> it = jsonObject.keys();
		while (it.hasNext()) {
			String key = it.next();
			map.put(key, JsonUtils.getJSONObject(jsonObject, key));
		}

		return map;
	}

	abstract protected Class<? extends FollowUp> getFollowUpClass();

	abstract protected List<Action> getActionList(WorkflowType workflowType);
}
