/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api.mapper;

import be.wiv_isp.healthdata.common.dto.SalesForceOrganizationDto;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SalesForceOrganizationMapper {

	static public List<SalesForceOrganizationDto> convert(JSONArray json) throws Exception {
		List<SalesForceOrganizationDto> salesForceOrganizations = new ArrayList<>();

		for (int i = 0; i < json.length(); i++) {
			JSONObject organizationJSON = json.getJSONObject(i);
			salesForceOrganizations.add(convertSalesForceOrganisationDto(organizationJSON));
		}

		return salesForceOrganizations;
	}

	static public SalesForceOrganizationDto convert(JSONObject json) throws Exception {
		return convertSalesForceOrganisationDto(json);
	}

	private static SalesForceOrganizationDto convertSalesForceOrganisationDto(JSONObject jsonObject) throws JSONException {
		SalesForceOrganizationDto salesForceOrganization = new SalesForceOrganizationDto();
		for (Iterator<String> iterator = jsonObject.keys(); iterator.hasNext();) {
			String key = iterator.next();
			if ("identificationType".equalsIgnoreCase(key)) {
				salesForceOrganization.setIdentificationType(jsonObject.getString(key));
			} else if ("identificationValue".equalsIgnoreCase(key)) {
				salesForceOrganization.setIdentificationValue(jsonObject.getString(key));
			} else if ("name".equalsIgnoreCase(key)) {
				salesForceOrganization.setName(jsonObject.getString(key));
			} else if ("hd4prc".equalsIgnoreCase(key)) {
				salesForceOrganization.setHd4prc(jsonObject.getBoolean(key));
			}
		}
		return salesForceOrganization;
	}
}
