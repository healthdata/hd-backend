/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import org.apache.commons.csv.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Reader;
import java.util.*;
import java.util.Map.Entry;

public class CsvService  {

	private static final Logger LOG = LoggerFactory.getLogger(CsvService.class);


	static public void merge(Appendable appendable, Reader... readers) throws IOException {
		CSVFormat csvFormat = CSVFormat.DEFAULT //
				.withDelimiter(';')//
				.withEscape('\\') //
				.withQuoteMode(QuoteMode.MINIMAL) //
				.withHeader();
		Map<String, List<String>> mergedMap = new TreeMap<>();
		int counter = 0;
		for (Reader reader : readers) {
			final CSVParser parser = new CSVParser(reader, csvFormat);
			for (CSVRecord record : parser) {
				Map<String, String> map = record.toMap();
				for (Entry<String, String> entry : map.entrySet()) {
					List<String> list = mergedMap.get(entry.getKey());
					if (list == null) {
						list = new ArrayList<>();
						mergedMap.put(entry.getKey(), list);
						for (int i = 0; i < counter; i++) {
							list.add(null);
						}
					}
					list.add(entry.getValue());
				}
				counter++;
				for (List<String> values : mergedMap.values()) {
					if (values.size() != counter) {
						values.add(null);
					}
				}
			}
			parser.close();
		}

		Set<String> keySet = mergedMap.keySet();
		final CSVPrinter printer = csvFormat.withHeader(keySet.toArray(new String[keySet.size()])).print(appendable);
		for (int i = 0; i < counter; i++) {
			List<String> record = new ArrayList<>();
			for (String key : keySet) {
				List<String> list = mergedMap.get(key);
				record.add(list.get(i));
			}
			printer.printRecord(record);
		}
		printer.close();
	}

	static public boolean compare(Reader reader1, Reader reader2) throws IOException {
		boolean equals = true;
		CSVFormat csvFormat = CSVFormat.DEFAULT //
				.withDelimiter(';')//
				.withEscape('\\') //
				.withQuoteMode(QuoteMode.MINIMAL) //
				.withHeader();
		final CSVParser parser1 = new CSVParser(reader1, csvFormat);
		final CSVParser parser2 = new CSVParser(reader2, csvFormat);

		List<CSVRecord> records1 = parser1.getRecords();
		List<CSVRecord> records2 = parser2.getRecords();

		if(records1.size() != records2.size()) {
			return false;
		}

		for (CSVRecord record1 : records1) {
			for (CSVRecord record2 : records2) {
				if (record2.getRecordNumber() == record1.getRecordNumber()) {
					Map<String, String> recordMap1 = record1.toMap();
					Map<String, String> recordMap2 = record2.toMap();
					if(!recordMap1.equals(recordMap2)) {
						return false;
					}
				}
			}
		}
		return equals;
	}

	static public List<String> getList(CSVRecord record) {
		List<String> result = new ArrayList<>();
		for (int i = 0; i < record.size(); i++) {
			result.add(record.get(i));
		}
		return result;
	}
}
