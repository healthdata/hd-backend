/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.aspect;

import be.wiv_isp.healthdata.orchestration.annotation.Truncate;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

@Aspect
@Component
public class TruncateAspect {

    private static final Logger LOG = LoggerFactory.getLogger(TruncateAspect.class);

    private static final String SUFFIX = "...";
    private static final double SAFETY_MARGIN_MULTIPLICATOR = 0.9; // (safety margin multiplicator for avoiding issues with special characters and encoding)

    @Around("@annotation(truncate)")
    public void truncate(ProceedingJoinPoint pjp, Truncate truncate) throws Throwable {
        final Object[] parameterValues = pjp.getArgs();

        String value;
        try {
            value = getStringParameter(parameterValues);
        } catch (HealthDataException e) {
            LOG.error(e.getMessage(), e);
            return;
        }

        if(value != null && value.length() > truncate.maxLength()) {
            LOG.debug(MessageFormat.format("Value: {0}", value));
            LOG.debug(MessageFormat.format("Value length: {0} characters", value.length()));
            LOG.debug(MessageFormat.format("Maximum length: {0} characters", truncate.maxLength()));
            int length = (int) (truncate.maxLength() * SAFETY_MARGIN_MULTIPLICATOR);
            LOG.debug(MessageFormat.format("Truncating to {0} characters", length));

            length -= SUFFIX.length();
            final String truncatedValue = value.substring(0, length);
            final String truncatedValueWithSuffix = truncatedValue + SUFFIX;
            parameterValues[0] = truncatedValueWithSuffix;
            LOG.debug(MessageFormat.format("Truncated value: {0} characters", truncatedValueWithSuffix.length()));
        }

        pjp.proceed(parameterValues);
    }

    private String getStringParameter(Object[] parameterValues) {
        if(parameterValues == null) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_PARAMETER, "INVALID_PARAMETER");
            throw exception;
        }
        if(parameterValues.length != 1) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_PARAMETER, "parameters must contain exactly one string value");
            throw exception;
        }
        if(parameterValues[0] == null) {
            return null;
        }
        if(!(parameterValues[0] instanceof String)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_PARAMETER, "the parameter is not a string");
            throw exception;
        }
        return (String) parameterValues[0];
    }
}
