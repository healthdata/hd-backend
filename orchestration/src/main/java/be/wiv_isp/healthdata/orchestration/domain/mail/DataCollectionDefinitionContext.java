/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.mail;

public class DataCollectionDefinitionContext extends AbstractVelocityContext {

    private String dataCollectionName;
    private String endDateCreation;
    private String endDateSubmission;
    private String endDateComments;
    private String period;
    private String updateDescription;

    public String getDataCollectionName() {
        return dataCollectionName;
    }

    public void setDataCollectionName(String dataCollectionName) {
        this.dataCollectionName = dataCollectionName;
    }

    public String getEndDateCreation() {
        return endDateCreation;
    }

    public void setEndDateCreation(String endDateCreation) {
        this.endDateCreation = endDateCreation;
    }

    public String getEndDateSubmission() {
        return endDateSubmission;
    }

    public void setEndDateSubmission(String endDateSubmission) {
        this.endDateSubmission = endDateSubmission;
    }

    public String getEndDateComments() {
        return endDateComments;
    }

    public void setEndDateComments(String endDateComments) {
        this.endDateComments = endDateComments;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getUpdateDescription() {
        return updateDescription;
    }

    public void setUpdateDescription(String updateDescription) {
        this.updateDescription = updateDescription;
    }

    @Override
    protected void doBuild() {
        put("dataCollectionName", dataCollectionName);
        put("endDateCreation", endDateCreation);
        put("endDateSubmission", endDateSubmission);
        put("endDateComments", endDateComments);
        put("period", period);
        put("updateDescription", updateDescription);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof DataCollectionDefinitionContext))
            return false;
        if (!super.equals(o))
            return false;

        DataCollectionDefinitionContext that = (DataCollectionDefinitionContext) o;

        if (dataCollectionName != null ? !dataCollectionName.equals(that.dataCollectionName) : that.dataCollectionName != null)
            return false;
        if (endDateCreation != null ? !endDateCreation.equals(that.endDateCreation) : that.endDateCreation != null)
            return false;
        if (endDateSubmission != null ? !endDateSubmission.equals(that.endDateSubmission) : that.endDateSubmission != null)
            return false;
        if (endDateComments != null ? !endDateComments.equals(that.endDateComments) : that.endDateComments != null)
            return false;
        if (period != null ? !period.equals(that.period) : that.period != null)
            return false;
        return !(updateDescription != null ? !updateDescription.equals(that.updateDescription) : that.updateDescription != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (dataCollectionName != null ? dataCollectionName.hashCode() : 0);
        result = 31 * result + (endDateCreation != null ? endDateCreation.hashCode() : 0);
        result = 31 * result + (endDateSubmission != null ? endDateSubmission.hashCode() : 0);
        result = 31 * result + (endDateComments != null ? endDateComments.hashCode() : 0);
        result = 31 * result + (period != null ? period.hashCode() : 0);
        result = 31 * result + (updateDescription != null ? updateDescription.hashCode() : 0);
        return result;
    }
}
