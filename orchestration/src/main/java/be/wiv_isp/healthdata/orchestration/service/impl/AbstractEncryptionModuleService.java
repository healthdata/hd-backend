/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.CSVContent;
import be.wiv_isp.healthdata.orchestration.domain.Identification;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.domain.MessageCorrespondent;
import be.wiv_isp.healthdata.orchestration.dto.EncryptionModuleMessage;
import be.wiv_isp.healthdata.orchestration.service.IEncryptionModuleService;
import be.wiv_isp.healthdata.orchestration.service.IEmVersionService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

abstract public class AbstractEncryptionModuleService implements IEncryptionModuleService {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractEncryptionModuleService.class);

    private Map<EMMessageKey, List<EncryptionModuleMessage> > queues = new HashMap<>();

    private static final Long MAX_MESSAGE_SIZE = 10_000_000L;

    @Autowired
    protected IConfigurationService configurationService;
    @Autowired
    protected IEmVersionService emVersionService;

    @Override
    public void queueOutgoing(EncryptionModuleMessage encryptionModuleMessage, Organization organization) {

        int maxQueueSize = Integer.parseInt(configurationService.get(ConfigurationKey.MESSAGING_QUEUE_SIZE).getValue());

        if (maxQueueSize == 1) {
            send(encryptionModuleMessage, organization);
            return;
        }

        EMMessageKey key = new EMMessageKey(organization, encryptionModuleMessage);

        if (queues.get(key)==null)
            queues.put(key, new ArrayList<EncryptionModuleMessage>());
        queues.get(key).add(encryptionModuleMessage);

        boolean doFlush = queues.get(key).size() >= maxQueueSize;

        if (!doFlush) {
            MessageCorrespondent correspondent = new MessageCorrespondent();
            correspondent.setIdentificationType(encryptionModuleMessage.getEncryptionAddressee().getType());
            correspondent.setIdentificationValue(encryptionModuleMessage.getEncryptionAddressee().getValue());
            correspondent.setApplicationId(encryptionModuleMessage.getEncryptionAddressee().getApplicationId());
            if (!emVersionService.supportsMultiLine(correspondent))
                doFlush = true;
        }

        if (doFlush)
            flushQueue(key);
    }

    @Override
    public void flushAll() {
        for (EMMessageKey key : queues.keySet()) {
            try {
                flushQueue(key);
            }
            catch (Exception e) {
                LOG.error("Could not flush outgoing message queue for organization {}", key.getOrganization());
            }
        }
        queues.clear();
    }

    private void flushQueue(EMMessageKey key) {
        final List<EncryptionModuleMessage> queue = queues.get(key);
        if (queue == null || queue.isEmpty())
            return;

        final int maxQueueSize = Integer.parseInt(configurationService.get(ConfigurationKey.MESSAGING_QUEUE_SIZE).getValue());

        if (maxQueueSize == 1) {
            for (EncryptionModuleMessage emMessage : queue)
                send(emMessage, key.getOrganization());
            queues.get(key).clear();
            return;
        }

        int maxCSVSize = 0, minCSVSize = Integer.MAX_VALUE, csvSize;
        for (EncryptionModuleMessage emMessage : queue) {
            csvSize = StringUtils.countMatches(emMessage.getContent(), ";") + 1;
            maxCSVSize = Math.max(maxCSVSize, csvSize);
            minCSVSize = Math.min(minCSVSize, csvSize);
        }
        if (minCSVSize < maxCSVSize) {
            for (EncryptionModuleMessage emMessage : queue) {
                if (StringUtils.countMatches(emMessage.getContent(), ";") + 1 < maxCSVSize) {
                    CSVContent csvContent = new CSVContent(emMessage.getContent());
                    for (int i = csvContent.getSize(); i < maxCSVSize; i++)
                        csvContent.addDummyField();
                    emMessage.setContent(csvContent.toString());
                }
            }
        }

        // bin backing approximation, sends at most 2*opt number of messages
        int msgCounter = 0, cumEstMsgSize = 0, estMsgSize;
        EncryptionModuleMessage combinedMessage = new EncryptionModuleMessage();
        for (EncryptionModuleMessage emMessage : queue) {
            estMsgSize = estimateEncryptedMessageLength(emMessage);
            if (cumEstMsgSize + estMsgSize > MAX_MESSAGE_SIZE || msgCounter + 1 > maxQueueSize) {
                send(combinedMessage, key.getOrganization());
                combinedMessage = new EncryptionModuleMessage();
                msgCounter = 0;
                cumEstMsgSize = 0;
            }
            combinedMessage.merge(emMessage);
            msgCounter += 1;
            cumEstMsgSize += estMsgSize;
        }
        send(combinedMessage, key.getOrganization());

        queues.get(key).clear();
    }

    private static int estimateEncryptedMessageLength(EncryptionModuleMessage encryptionModuleMessage) {
        final float slope = 1.8f;
        final int intercept = 4400;

        float estimate = 0f, split;
        for (String line : encryptionModuleMessage.getContent().split("\n")) {
            split = line.lastIndexOf(';');
            estimate += split + 1;
            estimate += slope * (line.length()-split) + intercept;
        }

        return (int) estimate;
    }

    private class EMMessageKey {
        private Organization organization;
        private Identification encryptionAddressee;
        private Identification addressee;
        private String subject;
        private Map<String,String> metadata;

        public EMMessageKey(Organization organization, EncryptionModuleMessage encryptionModuleMessage) {
            this.organization = organization;
            this.encryptionAddressee = encryptionModuleMessage.getEncryptionAddressee();
            this.addressee = encryptionModuleMessage.getAddressee();
            this.subject = encryptionModuleMessage.getSubject();
            this.metadata = encryptionModuleMessage.getMetadata();
        }

        public Organization getOrganization() {
            return organization;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            EMMessageKey that = (EMMessageKey) o;

            if (!organization.equals(that.organization)) return false;
            if (!encryptionAddressee.equals(that.encryptionAddressee)) return false;
            if (!addressee.equals(that.addressee)) return false;
            if (!subject.equals(that.subject)) return false;
            return metadata.equals(that.metadata);
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.organization,
                    this.encryptionAddressee,
                    this.addressee,
                    this.subject,
                    this.metadata);
        }

        @Override
        public String toString() {
            return "EMMessageKey {" +
                    "organization=" + organization +
                    ", encryptionAddressee=" + encryptionAddressee +
                    ", addressee=" + addressee +
                    ", subject='" + subject + '\'' +
                    ", metadata=" + metadata +
                    '}';
        }
    }

}
