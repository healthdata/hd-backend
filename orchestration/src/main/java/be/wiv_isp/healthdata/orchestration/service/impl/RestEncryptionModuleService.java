/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.CSVContent;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.MessageCorrespondent;
import be.wiv_isp.healthdata.orchestration.dto.EncryptionModuleMessage;
import be.wiv_isp.healthdata.orchestration.service.IEmVersionService;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.MediaType;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class RestEncryptionModuleService extends AbstractEncryptionModuleService {

	private static final Logger LOG = LoggerFactory.getLogger(RestEncryptionModuleService.class);

	@Autowired
	private IConfigurationService configurationService;
	@Autowired
	private WebServiceClientService webServiceClientService;
	@Autowired
	private IEmVersionService emVersionService;


	@Override
	public void send(EncryptionModuleMessage message, Organization organization) {
		final String encryptionModuleUri = configurationService.get(ConfigurationKey.EM_REST_URL_OUT, organization).getValue();

		LOG.debug(MessageFormat.format("Posting message to {0}", encryptionModuleUri));

		final WebServiceBuilder wsBuilder = new WebServiceBuilder();
		wsBuilder.setUrl(encryptionModuleUri);
		wsBuilder.setPost(true);
		wsBuilder.setAccept(MediaType.TEXT_PLAIN_TYPE);
		wsBuilder.setType(MediaType.APPLICATION_JSON_TYPE);
		wsBuilder.setReturnType(new GenericType<ClientResponse>() {});
		wsBuilder.setJson(message);

		ClientResponse response = (ClientResponse) webServiceClientService.callWebService(wsBuilder);
		if (response.getStatus() < 200 || response.getStatus() >= 300) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.ERROR_WHILE_PROCESSING_MESSAGE, "NEW_MESSAGE", message);
			throw exception;
		}
	}

	@Override
	public List<EncryptionModuleMessage> receive(Organization organization) {
		final String encryptionModuleUri = configurationService.get(ConfigurationKey.EM_REST_URL_IN, organization).getValue();

		final WebServiceBuilder wsBuilder = new WebServiceBuilder();
		wsBuilder.setUrl(encryptionModuleUri);
		wsBuilder.setGet(true);
		wsBuilder.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsBuilder.setReturnType(new GenericType<JSONArray>() {});

		final JSONArray messages = (JSONArray) webServiceClientService.callWebService(wsBuilder);

		if (messages == null || messages.length() == 0) {
			LOG.debug("No messages received from Encryption Module");
			return new ArrayList<>();
		}
		LOG.debug(MessageFormat.format("{0} message(s) received from Encryption Module", messages.length()));

		final List<EncryptionModuleMessage> emMessages = new ArrayList<>();
		for (int i = 0; i < messages.length(); ++i) {
			final JSONObject message = getJsonObject(messages, i);

			if(message == null) {
				continue;
			}

			try {
				int lineCount = 0;
				for (String csvRecord : message.getString("csvContent").split("\n")) {
					final EncryptionModuleMessage emMessage = new EncryptionModuleMessage();
					emMessage.setContent(csvRecord.trim());
					emMessage.setMetadata(unmarshalMetadata(message.getJSONObject("metadata")));
					String msgId = emMessage.getMetadata().get(Message.Metadata.MESSAGE_ID);
					if (!StringUtils.isBlank(msgId))
						emMessage.getMetadata().put(Message.Metadata.MESSAGE_ID, msgId + "_" + lineCount);
					emMessages.add(emMessage);
					lineCount++;
				}
				if (lineCount > 0) {
					CSVContent firstRecord = new CSVContent(message.getString("csvContent").split("\n")[0].trim());
					String messageContent = new String(firstRecord.getLastField(), StandardCharsets.UTF_8);
					final JSONObject senderJsonObject = JsonUtils.getJSONObject(new JSONObject(messageContent), "sender");
					final MessageCorrespondent correspondent = new MessageCorrespondent();
					correspondent.setIdentificationType(JsonUtils.getString(senderJsonObject, "identificationType"));
					correspondent.setIdentificationValue(JsonUtils.getString(senderJsonObject, "identificationValue"));
					correspondent.setApplicationId(JsonUtils.getString(senderJsonObject, "applicationId"));
					emVersionService.updateCorrespondent(correspondent, true);
				}
			} catch (JSONException e) {
				LOG.error(MessageFormat.format("Error while retrieving information from jsonObject, skipping message. Json object: {0}", message), e);
			}
		}

		return emMessages;
	}

	private JSONObject getJsonObject(JSONArray messages, int i) {
		try {
            return messages.getJSONObject(i);
        } catch (JSONException e) {
            LOG.error(MessageFormat.format("Error while retrieving message with index {0} from json array.", i), e);
			return null;
        }
	}

	private Map<String, String> unmarshalMetadata(JSONObject metadataJsonObject) throws JSONException {
		final Map<String, String> metadata = new HashMap<>();

		if (metadataJsonObject.names() != null) {
			for (int i = 0; i < metadataJsonObject.names().length(); ++i) {
				final String key = metadataJsonObject.names().getString(i);
				final String value = metadataJsonObject.getString(key);

				metadata.put(key, value);
			}
		}

		return metadata;
	}
}
