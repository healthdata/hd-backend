/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.orchestration.dao.IRegistrationDocumentDao;
import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationDocument;
import be.wiv_isp.healthdata.orchestration.domain.search.AbstractRegistrationDocumentSearch;

abstract public class AbstractRegistrationDocumentDao<Document extends AbstractRegistrationDocument, Search extends AbstractRegistrationDocumentSearch> extends AbstractDocumentDao<Document, Search> implements IRegistrationDocumentDao<Document, Search> {

	public AbstractRegistrationDocumentDao(Class<Document> entityClass) {
		super(entityClass);
	}

}
