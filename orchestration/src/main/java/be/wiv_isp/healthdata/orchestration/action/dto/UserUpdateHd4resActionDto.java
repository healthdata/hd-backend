/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.action.dto;

import be.wiv_isp.healthdata.orchestration.dto.HealthDataIdentificationDto;
import be.wiv_isp.healthdata.orchestration.domain.Authority;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class UserUpdateHd4resActionDto {

    private List<UserDto> users = new ArrayList<>();

    public List<UserDto> getUsers() {
        return users;
    }

    public void setUsers(List<UserDto> users) {
        this.users = users;
    }

    public static class UserDto {

        private Long hd4dpId;
        private String username;
        private String firstName;
        private String lastName;
        private String email;
        private HealthDataIdentificationDto healthDataIdentification;
        private Set<String> dataCollectionNames;
        private Set<Authority> authorities;
        private boolean enabled;
        private boolean ldapUser;
        private boolean deleted;

        public Long getHd4dpId() {
            return hd4dpId;
        }

        public void setHd4dpId(Long hd4dpId) {
            this.hd4dpId = hd4dpId;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public HealthDataIdentificationDto getHealthDataIdentification() {
            return healthDataIdentification;
        }

        public void setHealthDataIdentification(HealthDataIdentificationDto healthDataIdentification) {
            this.healthDataIdentification = healthDataIdentification;
        }

        public Set<String> getDataCollectionNames() {
            return dataCollectionNames;
        }

        public void setDataCollectionNames(Set<String> dataCollectionNames) {
            this.dataCollectionNames = dataCollectionNames;
        }

        public Set<Authority> getAuthorities() {
            return authorities;
        }

        public void setAuthorities(Set<Authority> authorities) {
            this.authorities = authorities;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public boolean isLdapUser() {
            return ldapUser;
        }

        public void setLdapUser(boolean ldapUser) {
            this.ldapUser = ldapUser;
        }

        public boolean isDeleted() {
            return deleted;
        }

        public void setDeleted(boolean deleted) {
            this.deleted = deleted;
        }

    }

}
