/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.orchestration.annotation.Truncate;
import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.Date;

@MappedSuperclass
abstract public class AbstractElasticSearchInfo<Workflow extends AbstractRegistrationWorkflow> implements Serializable {

	private static final long serialVersionUID = 1L;

	public enum Status {
		INDEXED, NOT_INDEXED, INDEXING_FAILED, DELETED
	}

	@Id
	@GeneratedValue(generator = "workflow-primary-key")
	@Column(name = "WORKFLOW_ID")
	private Long workflowId;

	@OneToOne(optional = true, fetch = FetchType.LAZY)
	@PrimaryKeyJoinColumn
	private Workflow workflow;

	@Column(name = "STATUS", nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status;

	@Column(name = "ERROR", nullable = true)
	private String error;

	@Column(name = "UPDATED_ON", nullable = false)
	private Timestamp updatedOn;

	@Column(name = "RETRY_COUNT", nullable = false)
	private int retryCount;

	public Long getWorkflowId() {
		return workflowId;
	}

	public void setWorkflowId(Long workflowId) {
		this.workflowId = workflowId;
	}

	@JsonIgnore
	public Workflow getWorkflow() {
		return workflow;
	}

	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	@Truncate(maxLength = 4000)
	public void setError(String error) {
		this.error = error;
	}

	@JsonIgnore
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public int getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}

	@PrePersist
	public void onCreate() {
		setUpdatedOn(new Timestamp(new Date().getTime()));
	}

	@PreUpdate
	public void onUpdate() {
		setUpdatedOn(new Timestamp(new Date().getTime()));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof AbstractElasticSearchInfo)) return false;

		AbstractElasticSearchInfo<?> that = (AbstractElasticSearchInfo<?>) o;

		if (workflowId != null ? !workflowId.equals(that.workflowId) : that.workflowId != null) return false;
		if (status != that.status) return false;
		if (error != null ? !error.equals(that.error) : that.error != null) return false;
		return !(updatedOn != null ? !updatedOn.equals(that.updatedOn) : that.updatedOn != null);

	}

	@Override
	public int hashCode() {
		int result = workflowId != null ? workflowId.hashCode() : 0;
		result = 31 * result + (status != null ? status.hashCode() : 0);
		result = 31 * result + (error != null ? error.hashCode() : 0);
		result = 31 * result + (updatedOn != null ? updatedOn.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		Long workflowId = this.workflowId;

		if (workflowId == null && workflow != null) {
			workflowId = workflow.getId();
		}

		return MessageFormat.format("[workflowId = {0}, status = {1}, updatedOn = {2}, error = {3}]", workflowId, status, updatedOn, error);
	}
}
