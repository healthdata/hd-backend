/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.api.uri;

import java.text.MessageFormat;

public class DataCollectionGroupUri {

	private static final String ROOT_PATTERN = "{0}/datacollectiongroups";

	private static final String GET_BY_ID_PATTERN = ROOT_PATTERN + "/{1}";
	private static final String GET_BY_NAME_AND_MAJOR_VERSION_PATTERN = ROOT_PATTERN + "/{1}/{2}";

	private String host;
	private Long id;
	private String groupName;
	private Integer majorVersion;

	public DataCollectionGroupUri(String host) {
		this.host = host;
	}

	public DataCollectionGroupUri(String host, Long id) {
		this.host = host;
		this.id = id;
	}

	public DataCollectionGroupUri(String host, String groupName, Integer majorVersion) {
		this.host = host;
		this.groupName = groupName;
		this.majorVersion = majorVersion;
	}

	@Override
	public String toString() {
		if (id != null) {
			return MessageFormat.format(GET_BY_ID_PATTERN, host, String.valueOf(id));
		}

		if (groupName != null && majorVersion != null) {
			return MessageFormat.format(GET_BY_NAME_AND_MAJOR_VERSION_PATTERN, host, groupName, String.valueOf(majorVersion));
		}

		return MessageFormat.format(ROOT_PATTERN, host);
	}
}
