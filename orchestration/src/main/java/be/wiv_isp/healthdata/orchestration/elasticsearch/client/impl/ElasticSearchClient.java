/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.elasticsearch.client.impl;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.elasticsearch.client.IElasticSearchClient;
import be.wiv_isp.healthdata.orchestration.elasticsearch.server.AbstractElasticSearchServer;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthRequestBuilder;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthStatus;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequestBuilder;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.admin.indices.exists.types.TypesExistsRequestBuilder;
import org.elasticsearch.action.admin.indices.exists.types.TypesExistsResponse;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingRequestBuilder;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingResponse;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.count.CountRequestBuilder;
import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.action.delete.DeleteRequestBuilder;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.client.Client;
import org.elasticsearch.indices.IndexMissingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class ElasticSearchClient implements IElasticSearchClient, DisposableBean {

	private static final Logger LOG = LoggerFactory.getLogger(ElasticSearchClient.class);

	public static final String[] EMPTY_STRING_ARRAY = new String[]{};

	private Client client;

	@Autowired
	private AbstractElasticSearchServer server;

	@Override
	public void destroy() throws Exception {
		if (client != null) {
			client.close();
		}
	}

	private Client getClient() {
		if (client == null) {
			client = server.getClient();
		}

		return client;
	}

	@Override
	public boolean status() {
		try {
			final ClusterHealthResponse healthResponse = new ClusterHealthRequestBuilder(this.getClient().admin().cluster()).setIndices(EMPTY_STRING_ARRAY).get();
			if (ClusterHealthStatus.GREEN.equals(healthResponse.getStatus())) {
				return true;
			}

			LOG.warn("ElasticSearch health response: {}", healthResponse);
			return false;
		} catch (ElasticsearchException e) {
			LOG.error(e.getMessage());
			return false;
		}
	}

	@Override
	public boolean indexExists(String index) {
		LOG.debug("Checking whether elasticsearch index [{}] exists", index);

		try {
			final IndicesExistsResponse indicesExistsResponse = new IndicesExistsRequestBuilder(this.getClient().admin().indices()).setIndices(index).get();
			LOG.debug("Index [{}] exists: {}", index, indicesExistsResponse.isExists());
			return indicesExistsResponse.isExists();
		} catch (ElasticsearchException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.ELASTICSEARCH_EXCEPTION, e.getMessage());
			throw exception;
		}
	}

	@Override
	public boolean typeExists(final String index, final String type) {
		LOG.debug("Checking whether elasticsearch index [{}] and type [{}] exist", index, type);

		try {
			if (!this.indexExists(index)) {
				LOG.debug("Index [{}] does not exist. Returning false", index);
				return false;
			}

			LOG.debug("Index [{}] exists. Checking whether elasticsearch type [{}] exists", index, type);

			final TypesExistsResponse typesExistsResponse = new TypesExistsRequestBuilder(this.getClient().admin().indices()).setIndices(new String[] { index }).setTypes(type).get();
			LOG.debug("Index [{}] and type [{}] exist: {}", index, type, typesExistsResponse.isExists());
			return typesExistsResponse.isExists();
		} catch (ElasticsearchException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.ELASTICSEARCH_EXCEPTION, e.getMessage());
			throw exception;
		}
	}

	@Override
	public void createIndex(final String index, final String settings) {
		LOG.debug("Creating index [{}] with settings [{}]", index, settings);

		CreateIndexResponse response;

		try {
			response = new CreateIndexRequestBuilder(this.getClient().admin().indices()).setIndex(index).setSource(settings).get();
		} catch (ElasticsearchException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.ELASTICSEARCH_EXCEPTION, e.getMessage());
			throw exception;
		}

		if (response.isAcknowledged()) {
			return;
		}
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.ELASTICSEARCH_CREATE_INDEX_EXCEPTION, index, response.toString());
		throw exception;
	}

	@Override
	public void putMapping(final String index, final String type, final String mapping) {
		LOG.debug("Putting mapping file on index [{}] and type [{}]. Mapping file: [{}]", index, type, mapping);

		PutMappingResponse response;

		try {
			response = new PutMappingRequestBuilder(this.getClient().admin().indices()).setIndices(index).setType(type).setSource(mapping).get();
		} catch (ElasticsearchException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.ELASTICSEARCH_EXCEPTION, e.getMessage());
			throw exception;
		}

		if (response.isAcknowledged()) {
			return;
		}
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.ELASTICSEARCH_PUT_MAPPING_EXCEPTION, index, response.toString());
		throw exception;
	}

	@Override
	public ElasticSearchBulkResponse index(final IndexingRequest... requests) {
		if (requests.length == 0) {
			return ElasticSearchBulkResponse.ok();
		}

		final BulkRequestBuilder bulkRequestBuilder = new BulkRequestBuilder(this.getClient());

		for (IndexingRequest r : requests) {
			bulkRequestBuilder.add(new IndexRequestBuilder(this.getClient()).setIndex(r.getIndex()).setType(r.getType()).setId(r.getId()).setSource(r.getContent()).request());
		}

		LOG.debug("Executing {} indexing requests", bulkRequestBuilder.request().subRequests().size());

		final BulkResponse response = bulkRequestBuilder.get();

		if(!response.hasFailures()) {
			LOG.debug("{} indexing requests executed.", bulkRequestBuilder.request().subRequests().size());
			return ElasticSearchBulkResponse.ok();
		}

		return map(response);
	}

	@Override
	public String search(final String[] indices, final String[] types, final String searchQuery) {
		LOG.debug("Searching in indices {} and types {} with search query [{}]", Arrays.toString(indices), Arrays.toString(types), searchQuery);

		final IndicesOptions indicesOptions = IndicesOptions.fromOptions(true, true, true, false);

		try {
			final SearchRequestBuilder request = new SearchRequestBuilder(this.getClient()).setIndices(indices).setIndicesOptions(indicesOptions).setSource(searchQuery.getBytes());

			if (types != null) {
				request.setTypes(types);
			}

			final SearchResponse response = request.get();
			return response.toString();
		} catch (ElasticsearchException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.ELASTICSEARCH_EXCEPTION, e.getMessage());
			throw exception;
		}
	}

	@Override
	public void deleteAllIndices() {
		LOG.info("Deleting all indices");

		DeleteIndexResponse response;

		try {
			response = new DeleteIndexRequestBuilder(this.getClient().admin().indices(), "_all").get();
		} catch (ElasticsearchException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.ELASTICSEARCH_EXCEPTION, e.getMessage());
			throw exception;
		}

		if (response.isAcknowledged()) {
			return;
		}
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.ELASTICSEARCH_DELETE_ALL_INDICES_EXCEPTION, response.toString());
		throw exception;
	}

	@Override
	public void deleteIndex(String index) {
		LOG.info("Deleting index {}", index);

		DeleteIndexResponse response;

		try {
			response = new DeleteIndexRequestBuilder(this.getClient().admin().indices(), index).get();
		} catch (IndexMissingException ime) {
			return;
		} catch (ElasticsearchException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.ELASTICSEARCH_EXCEPTION, e.getMessage());
			throw exception;
		}

		if (response.isAcknowledged()) {
			return;
		}
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.ELASTICSEARCH_DELETE_INDEX_EXCEPTION, index, response.toString());
		throw exception;
	}

	@Override
	public ElasticSearchBulkResponse delete(final DeleteRequest... requests) {
		if (requests.length == 0) {
			return ElasticSearchBulkResponse.ok();
		}

		final BulkRequestBuilder bulkRequestBuilder = new BulkRequestBuilder(this.getClient());

		for (DeleteRequest r : requests) {
			bulkRequestBuilder.add(new DeleteRequestBuilder(this.getClient()).setIndex(r.getIndex()).setType(r.getType()).setId(r.getId()));
		}

		LOG.info("Executing {} delete requests", bulkRequestBuilder.request().subRequests().size());

		final BulkResponse response = bulkRequestBuilder.get();

		if(!response.hasFailures()) {
			LOG.debug("{} delete requests executed.", bulkRequestBuilder.request().subRequests().size());
			return ElasticSearchBulkResponse.ok();
		}

		return map(response);
	}

	@Override
	public Long getDocumentCount() {
		try {
			CountResponse response = new CountRequestBuilder(this.getClient()).get();
			return response.getCount();
		} catch (ElasticsearchException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.ELASTICSEARCH_EXCEPTION, e.getMessage());
			throw exception;
		}
	}

	private ElasticSearchBulkResponse map(BulkResponse response) {
		if(!response.hasFailures()) {
			return ElasticSearchBulkResponse.ok();
		}

		final ElasticSearchBulkResponse result = new ElasticSearchBulkResponse();
		result.setHasFailures(response.hasFailures());
		for (BulkItemResponse r : response) {
			final ElasticSearchResponse elasticSearchResponse = new ElasticSearchResponse();
			elasticSearchResponse.setFailed(r.isFailed());
			elasticSearchResponse.setIndex(r.getIndex());
			elasticSearchResponse.setType(r.getType());
			elasticSearchResponse.setId(r.getId());
			elasticSearchResponse.setMessage(r.getFailureMessage());
			result.addResponse(elasticSearchResponse);
		}

		return result;
	}
}
