/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.orchestration.dao.IElasticSearchInfoDao;
import be.wiv_isp.healthdata.orchestration.domain.AbstractElasticSearchInfo;
import be.wiv_isp.healthdata.orchestration.domain.AbstractElasticSearchInfo.Status;
import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.ElasticSearchInfoCountDetails;
import be.wiv_isp.healthdata.orchestration.domain.search.ElasticSearchInfoSearch;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

abstract public class AbstractElasticSearchInfoDao<ElasticSearchInfo extends AbstractElasticSearchInfo, Workflow extends AbstractRegistrationWorkflow> extends CrudDaoV2<ElasticSearchInfo, Long, ElasticSearchInfoSearch> implements IElasticSearchInfoDao<ElasticSearchInfo> {

	public AbstractElasticSearchInfoDao(Class<ElasticSearchInfo> entityClass) {
		super(entityClass);
	}

	@Override
	public void updateAll(Status status) {
		this.updateAll(new ElasticSearchInfoSearch(), status);
	}

	@Override
	public void updateAll(ElasticSearchInfoSearch search, Status toStatus) {
		CriteriaBuilder cb = this.em.getCriteriaBuilder();
		CriteriaUpdate<ElasticSearchInfo> update = cb.createCriteriaUpdate(entityClass);
		Root<ElasticSearchInfo> rootEntry = update.from(entityClass);

		update.set("status", toStatus);
		update.set("error", null);

		final List<Predicate> predicates = new ArrayList<>();

		if (search.getStatus() != null) {
			predicates.add(cb.equal(rootEntry.get("status"), search.getStatus()));
		}

		if (search.getOrganizationId() != null) {
			Subquery<Long> sq = update.subquery(Long.class);
			Root<ElasticSearchInfo> sqc = sq.correlate(rootEntry);
			Join<AbstractElasticSearchInfo, Workflow> sqo = sqc.join("workflow");
			sq.select(sqc.get("workflow").<Long> get("id"));

			final List<Predicate> subPredicates = new ArrayList<>();
			subPredicates.add(cb.equal(sqo.get("organization").get("id"), search.getOrganizationId()));
			if (search.getDataCollectionName() != null) {
				subPredicates.add(cb.equal(cb.upper(sqo.<String>get("dataCollectionName")), search.getDataCollectionName().toUpperCase()));
			}
			sq.where(cb.and(subPredicates.toArray(new Predicate[subPredicates.size()])));

			predicates.add(rootEntry.get("workflowId").in(sq));
		}

		if (search.getWorkflowId() != null) {
			predicates.add(cb.equal(rootEntry.get("workflowId"), search.getWorkflowId()));
		}

		if (!predicates.isEmpty()) {
			update.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		}

		em.createQuery(update).executeUpdate();
	}

	@Override
	public List<ElasticSearchInfoCountDetails> countDetailed() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ElasticSearchInfoCountDetails> cq = cb.createQuery(ElasticSearchInfoCountDetails.class);
		Root<ElasticSearchInfo> rootEntry = cq.from(entityClass);
		Join<ElasticSearchInfo, Workflow> workflowJoin = rootEntry.join("workflow");

		cq.multiselect(
				workflowJoin.get("organization").get("id"),
				workflowJoin.get("dataCollectionName"),
				rootEntry.get("status"),
				cb.count(rootEntry))
		.groupBy(
				workflowJoin.get("organization").get("id"),
				workflowJoin.get("dataCollectionName"),
				rootEntry.get("status"));

		return em.createQuery(cq).getResultList();
	}

	@Override
	public List<Long> getWorkflowIds(Status status) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<ElasticSearchInfo> rootEntry = cq.from(entityClass);
		cq.select(rootEntry.<Long> get("workflowId"));

		cq.where(cb.equal(rootEntry.get("status"), status));

		TypedQuery<Long> allQuery = em.createQuery(cq);
		return allQuery.getResultList();
	}

	@Override
	protected List<Predicate> getPredicates(ElasticSearchInfoSearch search, CriteriaBuilder cb, Root<ElasticSearchInfo> rootEntry) {
		final List<Predicate> predicates = new ArrayList<>();

		if(search.getStatus() != null) {
			predicates.add(cb.equal(rootEntry.get("status"), search.getStatus()));
		}

		if (search.getOrganizationId() != null) {
			final Join<ElasticSearchInfo, Workflow> workflows = rootEntry.join("workflow");
			predicates.add(cb.equal(workflows.get("organization").get("id"), search.getOrganizationId()));
		}

		if (search.getDataCollectionName() != null) {
			final Join<ElasticSearchInfo, Workflow> workflows = rootEntry.join("workflow");
			predicates.add(cb.equal(workflows.get("dataCollectionName"), search.getDataCollectionName()));
		}

		if (search.getWorkflowId() != null) {
			predicates.add(cb.equal(rootEntry.get("workflowId"), search.getWorkflowId()));
		}

		return predicates;
	}
}
