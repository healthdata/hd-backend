/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.orchestration.dao.IGuestUserDao;
import be.wiv_isp.healthdata.orchestration.dao.IUserDao;
import be.wiv_isp.healthdata.orchestration.dao.IViewDao;
import be.wiv_isp.healthdata.orchestration.domain.GuestUser;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.View;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import be.wiv_isp.healthdata.orchestration.domain.search.ViewSearch;
import be.wiv_isp.healthdata.orchestration.domain.validator.IUserValidator;
import be.wiv_isp.healthdata.orchestration.security.oauth2.OrganizationUsernamePasswordAuthenticationToken;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionService;
import be.wiv_isp.healthdata.orchestration.service.IUserConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

abstract public class AbstractUserService extends AbstractService<User, Long, UserSearch, IUserDao> implements IUserService {

	@Autowired
	protected IUserDao userDao;
	@Autowired
	private IViewDao viewDao;
	@Autowired
	private IUserConfigurationService userConfigurationService;
	@Autowired
	protected IConfigurationService configurationService;
	@Autowired
	private IGuestUserDao guestUserDao;
	@Resource(name = "passwordEncoder")
	protected PasswordEncoder passwordEncoder;
	@Autowired
	protected IUserValidator userValidator;
	@Resource(name = "tokenStore")
	protected TokenStore tokenStore;
	@Autowired
	private IDataCollectionService dataCollectionService;

	public AbstractUserService() {
		super(User.class);
	}

	@Override
	protected IUserDao getDao() {
		return userDao;
	}

	@Override
	@Transactional
	public User create(User user) {
		return create(user, false);
	}

	@Override
	public User create(User user, boolean notifyUser) {
		getUserValidator().validate(user);
		String encPassword = encodePassword(user.getPassword());
		user.setPassword(encPassword);
		final User created = userDao.create(user);
		userConfigurationService.createConfigurations(created);
		return created;
	}

	@Override
	public User get(String userName, Organization organization) {
		final UserSearch userSearch = new UserSearch();
		userSearch.setUsername(userName);
		userSearch.setOrganization(organization);
		final User unique = getUnique(userSearch);
		if (unique == null) {
			throw new UsernameNotFoundException(MessageFormat.format("No user found with username [{0}]", userName));
		}
		return unique;
	}

	@Override
	public User getUnique(UserSearch search) {
		final User user = super.getUnique(search);

		if(user == null) {
			return null;
		}

		userDao.detach(user);
		return updateDataCollections(user);
	}

	private User updateDataCollections(User user) {
		if(user.isSupport()) {
			user.setDataCollectionNames(dataCollectionService.getDataCollectionsFlat(user.getOrganization())); // TODO change flat to groups
		}
		return user;
	}

	@Override
	@Transactional
	public User update(User user) {
		return update(user, false);
	}

	@Override
	@Transactional
	public User update(User user, boolean passwordEncodingNeeded) {
		if(isGuestAccount(user.getUsername())) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GENERAL_UNAUTHORIZED);
			throw exception;
		}
		getUserValidator().validate(user);
		final User existingUser = userDao.get(user.getId());
		if (!existingUser.getUsername().equals(user.getUsername())) {
			removeOAuthTokens(existingUser);
		}
		if (passwordEncodingNeeded) {
			String encPassword = encodePassword(user.getPassword());
			user.setPassword(encPassword);
		}
		return userDao.update(user);
	}

	@Override
	@Transactional
	public User updatePassword(User user, String newPassword) {
		removeOAuthTokens(user);
		user.setPassword(encodePassword(newPassword));
		return userDao.update(user);
	}

	@Override
	@Transactional
	public void delete(User user) {
		if(isGuestAccount(user.getUsername())) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GENERAL_UNAUTHORIZED);
			throw exception;
		}
		deleteViews(user.getId());
		userDao.delete(user);
		removeOAuthTokens(user);
	}

	protected void deleteViews(Long userId) {
		ViewSearch search = new ViewSearch();
		search.setUserId(userId);
		List<View> all = viewDao.getAll(search);
		for (View view : all) {
			viewDao.delete(view);
		}
	}

	@Override
	public boolean isGuestAccount(String userName) {
		if(configurationService.isTrue(ConfigurationKey.USE_GUEST_ACCOUNT)) {
			List<GuestUser> all = guestUserDao.getAll();
			for (GuestUser guestUser : all) {
				if(userName.equals(guestUser.getUsername())) {
					return true;
				}
			}
		}
		return false;
	}

	protected String encodePassword(String password) {
		if (password == null) {
			return null;
		}

		return passwordEncoder.encode(password);
	}

	protected void removeOAuthTokens(User user) {
		OAuth2Request oAuth2Request = new OAuth2Request() {

			private static final long serialVersionUID = 1L;

			@Override
			public String getClientId() {
				return "healthdata-client";
			}

			@Override
			public Set<String> getScope() {
				Set<String> scope = new TreeSet<>();
				scope.add("read");
				scope.add("trust");
				scope.add("write");
				return scope;
			}
		};

		Authentication authentication = new OrganizationUsernamePasswordAuthenticationToken(user.getOrganization().getId(), user, user.getPassword());

		OAuth2Authentication oAuth2Authentication = new OAuth2Authentication(oAuth2Request, authentication);
		OAuth2AccessToken accessToken = tokenStore.getAccessToken(oAuth2Authentication);
		if (accessToken != null) {
			tokenStore.removeAccessToken(accessToken);
			tokenStore.removeRefreshToken(accessToken.getRefreshToken());
		}
	}

	public IUserValidator getUserValidator() {
		return userValidator;
	}
}
