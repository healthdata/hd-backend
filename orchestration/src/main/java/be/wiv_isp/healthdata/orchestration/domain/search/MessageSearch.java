/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.search;

import be.wiv_isp.healthdata.common.domain.search.EntitySearch;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageType;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class MessageSearch extends EntitySearch {

	private Long id;
	private List<MessageStatus> statuses;
	private Organization organization;
	private Date sentBefore;
	private Long linkedId;
	private String identificationType;
	private String identificationValue;
	private Boolean workflowIdNull;
	private Long workflowId;
	private MessageType type;
	private Boolean hasRemainingSendingTrials;

	public List<MessageStatus> getStatuses() {
		return statuses;
	}

	public void setStatuses(MessageStatus... statuses) {
		this.statuses = Arrays.asList(statuses);
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public void setSentBefore(Date sentBefore) {
		this.sentBefore = sentBefore;
	}

	public Date getSentBefore() {
		return sentBefore;
	}

	public void setLinkedId(Long linkedId) {
		this.linkedId = linkedId;
	}

	public Long getLinkedId() {
		return linkedId;
	}

	public String getIdentificationType() {
		return identificationType;
	}

	public void setIdentificationType(String identificationType) {
		this.identificationType = identificationType;
	}

	public String getIdentificationValue() {
		return identificationValue;
	}

	public void setIdentificationValue(String identificationValue) {
		this.identificationValue = identificationValue;
	}

	public void setWorkflowId(Long workflowId) {
		this.workflowId = workflowId;
	}

	public Long getWorkflowId() {
		return workflowId;
	}

	public MessageType getType() {
		return type;
	}

	public void setType(MessageType type) {
		this.type = type;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setHasRemainingSendingTrials(Boolean hasRemainingSendingTrials) {
		this.hasRemainingSendingTrials = hasRemainingSendingTrials;
	}

	public Boolean isHasRemainingSendingTrials() {
		return hasRemainingSendingTrials;
	}

}
