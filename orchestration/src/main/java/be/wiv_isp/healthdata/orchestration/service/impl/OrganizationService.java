/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.caching.CacheManagementService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.search.OrganizationSearch;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class OrganizationService implements IOrganizationService {

	private static final Logger LOG = LoggerFactory.getLogger(OrganizationService.class);

	@Autowired
	private CacheManager cacheManager;

	@Autowired
	private IOrganizationDao organizationDao;

	@Override
	@Cacheable(value = CacheManagementService.organizationCache)
	public Organization get(Long id) {
		return organizationDao.get(id);
	}

	@Override
	@Cacheable(value = CacheManagementService.organizationCache)
	public Organization getMain() {
		return organizationDao.getMain();
	}

	@Override
	public List<Organization> getAll() {
		return organizationDao.getAll();
	}

	@Override
	public Organization getUnique(OrganizationSearch search) {
		final Organization organization = organizationDao.getUnique(search);

		if(organization == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.NON_UNIQUE_SEARCH_RESULT, 0, "ORGANIZATION", search);
			throw exception;
		}

		return organization;
	}

	@Override
	public List<Organization> getAll(OrganizationSearch search) {
		return organizationDao.getAll(search);
	}

	@Override
	@Transactional
	public Organization create(Organization organization) {
		final Organization created = organizationDao.create(organization);
		cacheManager.getCache(CacheManagementService.organizationCache).clear();
		return created;
	}

	@Override
	@Transactional
	public Organization update(Organization organization) {
		final Organization updated = organizationDao.update(organization);
		cacheManager.getCache(CacheManagementService.organizationCache).clear();
		return updated;
	}

	@Override
	public Organization getOrganizationOrMain(Long organizationId) {
		if (organizationId == null) {
			return organizationDao.getMain();
		} else {
			return organizationDao.get(organizationId);
		}
	}

	@Override
	@Transactional
	public void delete(Organization organization) {
		organizationDao.delete(organization);
		cacheManager.getCache(CacheManagementService.organizationCache).clear();
	}
}