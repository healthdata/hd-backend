/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.security.authentication.provider.impl;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.AuthenticationType;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.security.authentication.provider.OrganisationProvider;
import be.wiv_isp.healthdata.orchestration.security.ldap.LdapOrganizationUserDetailsImpl;
import be.wiv_isp.healthdata.orchestration.security.ldap.authorities.populator.IAuthoritiesPopulator;
import be.wiv_isp.healthdata.orchestration.security.ldap.service.ILdapService;
import be.wiv_isp.healthdata.orchestration.security.oauth2.OrganizationUsernamePasswordAuthenticationToken;
import be.wiv_isp.healthdata.orchestration.service.IAuthenticationService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.ldap.NamingException;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.authority.mapping.NullAuthoritiesMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.ldap.authentication.LdapAuthenticator;
import org.springframework.security.ldap.ppolicy.PasswordPolicyException;
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;
import org.springframework.security.ldap.userdetails.LdapUserDetailsMapper;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.util.Collection;

public class CustomLdapAuthenticationProvider implements AuthenticationProvider, MessageSourceAware, OrganisationProvider {

    private static final Logger LOG = LoggerFactory.getLogger(CustomLdapAuthenticationProvider.class);

    @Autowired
    protected ILdapService ldapService;

    @Autowired
    protected IConfigurationService configurationService;

    @Autowired
    protected AuthenticationProvider daoAuthenticationProvider;

    @Autowired
    protected IOrganizationService organizationService;

    @Autowired
    private IAuthenticationService authenticationService;

    protected MessageSourceAccessor messages = SpringSecurityMessageSource.getAccessor();
    private boolean useAuthenticationRequestCredentials = true;
    private GrantedAuthoritiesMapper authoritiesMapper = new NullAuthoritiesMapper();
    protected UserDetailsContextMapper userDetailsContextMapper = new LdapUserDetailsMapper();
    private LdapAuthenticator authenticator;
    private IAuthoritiesPopulator authoritiesPopulator;
    private boolean hideUserNotFoundExceptions = true;

    // ~ Constructor
    // ===================================================================================================

    /**
     * Create an instance with the supplied authenticator and authorities
     * populator implementations.
     *
     * @param authenticator
     *            the authentication strategy (bind, password comparison, etc)
     *            to be used by this provider for authenticating users.
     * @param authoritiesPopulator
     *            the strategy for obtaining the authorities for a given user
     *            after they've been authenticated.
     */
    public CustomLdapAuthenticationProvider(LdapAuthenticator authenticator, IAuthoritiesPopulator authoritiesPopulator) {
        this.setAuthenticator(authenticator);
        this.setAuthoritiesPopulator(authoritiesPopulator);
    }

    public Authentication mainAuthenticate(Authentication authentication) throws AuthenticationException {
        Assert.isInstanceOf(OrganizationUsernamePasswordAuthenticationToken.class, authentication, messages.getMessage("LdapAuthenticationProvider.onlySupports", "Only OrganizationUsernamePasswordAuthenticationToken is supported"));

        final OrganizationUsernamePasswordAuthenticationToken userToken = (OrganizationUsernamePasswordAuthenticationToken) authentication;

        String username = userToken.getName();
        String password = (String) authentication.getCredentials();

        if (LOG.isDebugEnabled()) {
            LOG.debug("Processing authentication request for user: " + username);
        }

        if (!StringUtils.hasLength(username)) {
            throw new BadCredentialsException(messages.getMessage("LdapAuthenticationProvider.emptyUsername", "Empty Username"));
        }

        if (!StringUtils.hasLength(password)) {
            throw new BadCredentialsException(messages.getMessage("AbstractLdapAuthenticationProvider.emptyPassword", "Empty Password"));
        }

        Assert.notNull(password, "Null password was supplied in authentication token");

        DirContextOperations userData = doAuthentication(userToken);

        UserDetails user = userDetailsContextMapper.mapUserFromContext(userData, authentication.getName(), loadUserAuthorities(userData, authentication.getName(), (String) authentication.getCredentials(), userToken.getOrganizationId()));

        return createSuccessfulAuthentication(userToken, user);
    }

    /**
     * Creates the final {@code Authentication} object which will be returned
     * from the {@code authenticate} method.
     *
     * @param authentication
     *            the original authentication request token
     * @param user
     *            the <tt>UserDetails</tt> instance returned by the configured
     *            <tt>UserDetailsContextMapper</tt>.
     * @return the Authentication object for the fully authenticated user.
     */
    protected Authentication createSuccessfulAuthentication(OrganizationUsernamePasswordAuthenticationToken authentication, UserDetails user) {
        Object password = useAuthenticationRequestCredentials ? authentication.getCredentials() : user.getPassword();

        final Organization organization = organizationService.getOrganizationOrMain(authentication.getOrganizationId());

        HDUserDetails hdUserDetails = new LdapOrganizationUserDetailsImpl((LdapUserDetailsImpl) user, organization);

        OrganizationUsernamePasswordAuthenticationToken result = new OrganizationUsernamePasswordAuthenticationToken(authentication.getOrganizationId(), hdUserDetails, password, authoritiesMapper.mapAuthorities(user.getAuthorities()));
        result.setDetails(authentication.getDetails());

        return result;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return OrganizationUsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }

    /**
     * Determines whether the supplied password will be used as the credentials
     * in the successful authentication token. If set to false, then the
     * password will be obtained from the UserDetails object created by the
     * configured {@code UserDetailsContextMapper}. Often it will not be
     * possible to read the password from the directory, so defaults to true.
     *
     * @param useAuthenticationRequestCredentials
     */
    public void setUseAuthenticationRequestCredentials(boolean useAuthenticationRequestCredentials) {
        this.useAuthenticationRequestCredentials = useAuthenticationRequestCredentials;
    }

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messages = new MessageSourceAccessor(messageSource);
    }

    public void setAuthoritiesMapper(GrantedAuthoritiesMapper authoritiesMapper) {
        this.authoritiesMapper = authoritiesMapper;
    }

    /**
     * Allows a custom strategy to be used for creating the <tt>UserDetails</tt>
     * which will be stored as the principal in the <tt>Authentication</tt>
     * returned by the
     * {@link #createSuccessfulAuthentication(org.springframework.security.authentication.UsernamePasswordAuthenticationToken, org.springframework.security.core.userdetails.UserDetails)}
     * method.
     *
     * @param userDetailsContextMapper
     *            the strategy instance. If not set, defaults to a simple
     *            <tt>LdapUserDetailsMapper</tt>.
     */
    public void setUserDetailsContextMapper(UserDetailsContextMapper userDetailsContextMapper) {
        Assert.notNull(userDetailsContextMapper, "UserDetailsContextMapper must not be null");
        this.userDetailsContextMapper = userDetailsContextMapper;
    }

    /**
     * Provides access to the injected {@code UserDetailsContextMapper} strategy
     * for use by subclasses.
     */
    protected UserDetailsContextMapper getUserDetailsContextMapper() {
        return userDetailsContextMapper;
    }

    private void setAuthenticator(LdapAuthenticator authenticator) {
        Assert.notNull(authenticator, "An LdapAuthenticator must be supplied");
        this.authenticator = authenticator;
    }

    private LdapAuthenticator getAuthenticator() {
        return authenticator;
    }

    private void setAuthoritiesPopulator(IAuthoritiesPopulator authoritiesPopulator) {
        Assert.notNull(authoritiesPopulator, "An LdapOrganizationLdapAuthoritiesPopulator must be supplied");
        this.authoritiesPopulator = authoritiesPopulator;
    }

    protected IAuthoritiesPopulator getAuthoritiesPopulator() {
        return authoritiesPopulator;
    }

    public void setHideUserNotFoundExceptions(boolean hideUserNotFoundExceptions) {
        this.hideUserNotFoundExceptions = hideUserNotFoundExceptions;
    }

    protected DirContextOperations doAuthentication(UsernamePasswordAuthenticationToken authentication) {
        try {
            return getAuthenticator().authenticate(authentication);
        } catch (PasswordPolicyException ppe) {
            // The only reason a ppolicy exception can occur during a bind is
            // that the account is locked.
            throw new LockedException(messages.getMessage(ppe.getStatus().getErrorCode(), ppe.getStatus().getDefaultMessage()));
        } catch (UsernameNotFoundException notFound) {
            if (hideUserNotFoundExceptions) {
                throw new BadCredentialsException(messages.getMessage("LdapAuthenticationProvider.badCredentials", "Bad credentials"));
            } else {
                throw notFound;
            }
        } catch (NamingException ldapAccessFailure) {
            throw new InternalAuthenticationServiceException(ldapAccessFailure.getMessage(), ldapAccessFailure);
        }
    }

    protected Collection<? extends GrantedAuthority> loadUserAuthorities(DirContextOperations userData, String username, String password, Long organizationId) {
        return getAuthoritiesPopulator().getGrantedAuthorities(userData, username, organizationId);
    }

    @Override
    public boolean supports(Long organizationId, String username) {
        return authenticationService.supports(organizationId, username, AuthenticationType.LDAP);
    }

    @Override
    public Authentication authenticate(Authentication authentication) {
        if (!supports(authentication.getClass())) {
            return null;
        }

        return mainAuthenticate(authentication);
    }
}
