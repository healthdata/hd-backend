/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.tasks;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.tasks.trigger.DelayedTrigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;

import javax.annotation.PostConstruct;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

public abstract class SchedulableHealthDataTask extends HealthDataTask {

    private static final Logger LOG = LoggerFactory.getLogger(SchedulableHealthDataTask.class);

    private static final int STARTUP_DELAY_SECONDS = 30; // scheduled tasks will have first execution 5 minutes after application startup

    @Autowired
    private TaskScheduler taskScheduler;

    @PostConstruct
    public void schedule() {
        final String timingExpression = getTimingExpression();
        LOG.debug("Scheduling [{}] task with timing expression [{}]", getName().toLowerCase(), timingExpression);
        boolean scheduled = scheduleWithFixedDelay(timingExpression);
        scheduled = scheduled || scheduleWithTimeExpression(timingExpression);
        scheduled = scheduled || scheduleUniqueExecution(timingExpression);
        scheduled = scheduled || scheduleWithCronExpression(timingExpression);
        if(!scheduled) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_TIMING_EXPRESSION, timingExpression);
            throw exception;
        }
    }

    private boolean scheduleWithFixedDelay(String timingExpression) {
        try {
            int seconds = Integer.valueOf(timingExpression);
            final Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.SECOND, STARTUP_DELAY_SECONDS);
            final Date firstExecutionTime = calendar.getTime();
            LOG.info("Scheduling [{}] task with fixed delay ({} seconds). First execution on {}", getName().toLowerCase(), seconds, DateFormat.DATE_AND_TIME.format(firstExecutionTime));
            taskScheduler.scheduleWithFixedDelay(this, firstExecutionTime, 1000 * seconds);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean scheduleUniqueExecution(String timingExpression) {
        try {
            Date date = DateFormat.DATE_AND_TIME.parse(timingExpression);
            LOG.info("Scheduling [{}] task with unique execution on {}", getName().toLowerCase(), DateFormat.DATE_AND_TIME.format(date));
            taskScheduler.schedule(this, date);
            return true;
        } catch (HealthDataException e1) {
            return false;
        }
    }

    private boolean scheduleWithTimeExpression(String timingExpression) {
        boolean correctFormat = Pattern.compile("[0-1][0-9]:[0-5][0-9]").matcher(timingExpression).matches();

        if (!correctFormat) {
            return false;
        }

        final String[] split = timingExpression.split(":");
        final Integer hour = Integer.valueOf(split[0]);
        final Integer minute = Integer.valueOf(split[1]);
        final String cronExpression = MessageFormat.format("0 {0} {1} ? * *", minute, hour);
        return scheduleWithCronExpression(cronExpression);
    }

    private boolean scheduleWithCronExpression(String cronExpression) {
        try {
            final Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.SECOND, STARTUP_DELAY_SECONDS);
            final Date firstExecutionTime = calendar.getTime();

            LOG.info("Scheduling [{}] task with cron expression [{}]. First execution on {}", getName().toLowerCase(), cronExpression, DateFormat.DATE_AND_TIME.format(firstExecutionTime));
            taskScheduler.schedule(this, new DelayedTrigger(new CronTrigger(cronExpression), firstExecutionTime));
            return true;
        } catch (IllegalArgumentException e) {
            LOG.debug("Invalid cron expression [{}]: {}", cronExpression, e.getMessage());
            return false;
        }
    }

    protected abstract String getTimingExpression();
}
