/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.service.IPlatformService;
import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.orchestration.dao.IUserConfigurationDao;
import be.wiv_isp.healthdata.orchestration.dao.IUserDao;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.UserConfiguration;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.UserConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.search.UserConfigurationSearch;
import be.wiv_isp.healthdata.orchestration.service.IUserConfigurationService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

@Service
public class UserConfigurationService extends AbstractService<UserConfiguration, Long, UserConfigurationSearch, IUserConfigurationDao> implements IUserConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(UserConfigurationService.class);

    @Autowired
    private IUserConfigurationDao dao;

    @Autowired
    private IPlatformService platformService;

    @Autowired
    private IUserDao userDao;

    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("main");

    public UserConfigurationService() {
        super(UserConfiguration.class);
    }

    @Override
    protected IUserConfigurationDao getDao() {
        return dao;
    }

    private boolean isApplicable(UserConfigurationKey configurationKey, Platform platform) {
        switch (platform) {
            case HD4DP:
                return configurationKey.isHd4dp();
            case HD4RES:
                return configurationKey.isHd4res();
            default:
                return false;
        }
    }

    @Override
    @Transactional
    public void deleteInvalid() {
        final List<String> deletedKeys = dao.deleteWithInvalidKey();
        for (String deletedKey : deletedKeys) {
            LOG.info(MessageFormat.format("Removed invalid configuration: {0}", deletedKey));
        }
    }

    @Override
    @Transactional
    public  void deleteInapplicable() {
        final Platform platform = platformService.getCurrentPlatform();
        for (UserConfiguration configuration : dao.getAll()) {
            if (!isApplicable(configuration.getKey(), platform)) {
                LOG.info(MessageFormat.format("Removing non-applicable configuration {0}", configuration.getKey()));
                dao.delete(configuration);
            }
        }
    }

    @Override
    @Transactional
    public void createMissing() {
        final List<User> users = userDao.getAll();
        for (final User user : users) {
            createConfigurations(user);
        }
    }

    @Override
    public void createConfigurations(User user) {
        LOG.debug("Creating configurations for user {}", user.toStringCompact());
        final Platform platform = platformService.getCurrentPlatform();
        for (UserConfigurationKey key : UserConfigurationKey.values()) {
            final List<Authority> authorities = new ArrayList<>(key.getAuthorities());
            authorities.retainAll(user.getAuthorities());
            if(authorities.isEmpty()) {
                LOG.debug("Configuration {} is not applicable authorities {}", key, user.getAuthorities());
                continue;
            }
            if(!isApplicable(key, platform)) {
                LOG.debug("Configuration {} is not applicable for platform {}", key, platform);
                continue;
            }
            final UserConfigurationSearch search = new UserConfigurationSearch();
            search.setUserId(user.getId());
            search.setKey(key);
            final UserConfiguration existingConfiguration = getUnique(search);
            if(existingConfiguration == null) {
                final String defaultValue = getDefaultValue(key);

                LOG.debug("Creating configuration {} for user {} with default value [{}]", key, user.toStringCompact(), defaultValue);
                final UserConfiguration newConfiguration = new UserConfiguration();
                newConfiguration.setKey(key);
                newConfiguration.setUser(user);
                newConfiguration.setValue(defaultValue);
                newConfiguration.setDefaultValue(defaultValue);
                create(newConfiguration);
            }
        }
    }

    private String getDefaultValue(UserConfigurationKey key) {
        try {
            return resourceBundle.getString(key.name());
        } catch (Exception e) {
            return StringUtils.EMPTY;
        }
    }
}
