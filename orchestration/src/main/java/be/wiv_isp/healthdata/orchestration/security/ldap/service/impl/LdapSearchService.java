/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.security.ldap.service.impl;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.security.ldap.domain.LdapNode;
import be.wiv_isp.healthdata.orchestration.security.ldap.factory.IContextSourceFactory;
import be.wiv_isp.healthdata.orchestration.security.ldap.service.ILdapSearchService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.DistinguishedName;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;
import org.springframework.stereotype.Service;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.PartialResultException;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.text.MessageFormat;
import java.util.*;

@Service
public class LdapSearchService implements ILdapSearchService {

	private static final Logger LOG = LoggerFactory.getLogger(LdapSearchService.class);

	@Autowired
	private IContextSourceFactory contextSourceFactory;

	private Map<String, ContextSource> contextSources = new HashMap<>();

	@Override
	public List<LdapNode> search(String searchBase, String searchFilter, Object[] filterValues, SearchControls searchControls) {
		for (String referralValue : new String[] { "follow", "ignore" }) {
			final List<LdapNode> nodes = new ArrayList<>();

			final DirContext directoryContext = getDirectoryContext(referralValue);

			try {
				final NamingEnumeration<SearchResult> results = directoryContext.search(searchBase, searchFilter, filterValues, searchControls);

				while (results.hasMore()) {
					nodes.add(new LdapNode(results.next()));
				}

				return nodes;
			} catch (PartialResultException e) {
				if (referralValue.equals("follow")) {
					LOG.warn("Error while searching with referral [follow]. Trying with referral [ignore]");
					LOG.debug("PartialResultException stackstrace", e);
				} else {
					LOG.warn("Ignoring PartialResultException with referral [ignore]");
					LOG.debug("PartialResultException stackstrace", e);
					return nodes;
				}
			} catch (NamingException e) {
				if (referralValue.equals("follow")) {
					LOG.warn("Error while searching with referral [follow]. Trying with referral [ignore]");
					LOG.debug("NamingException stackstrace", e);
				} else {
					HealthDataException exception = new HealthDataException(e);
					exception.setExceptionType(ExceptionType.LDAP_EXCEPTION, e.getMessage());
					throw exception;
				}
			} finally {
				try {
					directoryContext.close();
				} catch (NamingException e) {
					HealthDataException exception = new HealthDataException(e);
					exception.setExceptionType(ExceptionType.LDAP_EXCEPTION, e.getMessage());
					throw exception;
				}
			}
		}
		return new ArrayList<>();
	}

	@Override
	public LdapNode uniqueSearch(String searchBase, String searchFilter, Object[] filterValues, boolean exactlyOneResult) {
		return uniqueSearch(searchBase, searchFilter, filterValues, exactlyOneResult, defaultSearchControls());
	}

	@Override
	public LdapNode uniqueSearch(String searchBase, String searchFilter, Object[] filterValues, boolean exactlyOneResult, SearchControls searchControls) {
		final List<LdapNode> nodes = search(searchBase, searchFilter, filterValues, searchControls);

		if (CollectionUtils.isEmpty(nodes)) {
			if(exactlyOneResult) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.LDAP_EXCEPTION, MessageFormat.format("no result found for search base [{0}], search filter [{1}] and filter values [{2}].", searchBase, searchFilter, Arrays.toString(filterValues)));
				throw exception;
			} else {
				return null;
			}
		}

		if (nodes.size() > 1) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.LDAP_EXCEPTION, MessageFormat.format("no result found for search base [{0}], search filter [{1}] and filter values [{2}].", searchBase, searchFilter, Arrays.toString(filterValues)));
			throw exception;
		}

		return nodes.get(0);
	}

	private ContextSource getContextSource() {
		return getContextSource("follow");
	}

	private ContextSource getContextSource(String referralValue) {
		if(contextSources.get(referralValue) == null) {
			final ContextSource context = contextSourceFactory.create(referralValue);
			contextSources.put(referralValue, context);
		}
		return contextSources.get(referralValue);
	}

	private DirContext getDirectoryContext() {
		return getDirectoryContext("follow");
	}

	private DirContext getDirectoryContext(String referralValue) {
		return getContextSource(referralValue).getReadOnlyContext();
	}

	@Override
	public void connectionStatus() {
		final DirContext directoryContext = getDirectoryContext();

		try {
			directoryContext.close();
		} catch (NamingException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.LDAP_EXCEPTION, e.getMessage());
			throw exception;
		}
	}

	@Override
	public String getRelativeDistinguishedName(String dn) {
		final DistinguishedName basePath = ((DefaultSpringSecurityContextSource) getContextSource()).getBaseLdapPath();
		final DistinguishedName distinguishedName = new DistinguishedName(dn);

		if(distinguishedName.startsWith(basePath)) {
			return distinguishedName.getSuffix(basePath.size()).toString();
		} else {
			return dn;
		}
	}

	private SearchControls defaultSearchControls() {
		final SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		return searchControls;
	}
}
