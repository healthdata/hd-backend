/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.orchestration.dao.IWorkflowDao;
import be.wiv_isp.healthdata.orchestration.domain.AbstractWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.search.AbstractWorkflowSearch;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

abstract public class AbstractWorkflowDao<Workflow extends AbstractWorkflow, Search extends AbstractWorkflowSearch> extends CrudDaoV2<Workflow, Long, Search> implements IWorkflowDao<Workflow, Search> {

	public AbstractWorkflowDao(Class<Workflow> entityClass) {
		super(entityClass);
	}

	@Override
	protected void fetchAttributes(Root<Workflow> rootEntry) {
		rootEntry.fetch("history", JoinType.LEFT);
	}

	@Override
	protected List<Predicate> getPredicates(Search search, CriteriaBuilder cb, Root<Workflow> rootEntry) {
		List<Predicate> predicates = new ArrayList<>();
		if (search.getStatuses() != null) {
			if (search.getStatuses().isEmpty()) {
				predicates.add(cb.or()); // predicate that is always false (to avoid syntax exception in DB2)
			} else {
				predicates.add(rootEntry.get("status").in(search.getStatuses()));
			}
		}
		if (search.getDataCollectionName() != null) {
			predicates.add(cb.equal(cb.upper(rootEntry.<String>get("dataCollectionName")), search.getDataCollectionName().toUpperCase()));
		}
		if (search.getDataCollectionDefinitionId() != null) {
			predicates.add(cb.equal(rootEntry.get("dataCollectionDefinitionId"), search.getDataCollectionDefinitionId()));
		}
		if (search.getNotWorkflowId() != null) {
			predicates.add(cb.notEqual(rootEntry.get("id"), search.getNotWorkflowId()));
		}
		if (!search.getReturnDeleted()) {
			// soft delete implementation
			predicates.add(cb.notEqual(rootEntry.get("status"), WorkflowStatus.DELETED));
		}
		if (search.getNotUpdatedAfterDate() != null) {
			predicates.add(cb.lessThan(rootEntry.<Date> get("updatedOn"), search.getNotUpdatedAfterDate()));
		}
		if (search.getCreatedAfterDate() != null) {
			predicates.add(cb.greaterThan(rootEntry.<Date> get("createdOn"), search.getCreatedAfterDate()));
		}
		if (search.getUpdatedAfterDate() != null) {
			predicates.add(cb.greaterThan(rootEntry.<Date> get("updatedOn"), search.getUpdatedAfterDate()));
		}
		if (search.getSendStatus() != null) {
			predicates.add(cb.equal(rootEntry.get("sendStatus"), search.getSendStatus()));
		}
		if (search.getWorkflowIds() != null) {
			if (search.getWorkflowIds().isEmpty()) {
				predicates.add(cb.or()); // predicate that is always false (to avoid syntax exception in DB2)
			} else {
				predicates.add(rootEntry.get("id").in(search.getWorkflowIds()));
			}
		}
		if (search.getOrganization() != null) {
			predicates.add(rootEntry.get("organization").in(search.getOrganization()));
		}
		return predicates;
	}

	@Override
	public List<Long> getWorkflowIds(Search search) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Workflow> rootEntry = cq.from(entityClass);
		cq.select(rootEntry.<Long> get("id"));

		List<Predicate> predicates = getPredicates(search, cb, rootEntry);

		if (!predicates.isEmpty()) {
			cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		}

		TypedQuery<Long> allQuery = em.createQuery(cq);

		return allQuery.getResultList();
	}

	@Override
	public List<Workflow> getAllForUniqueIDCalculation() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Workflow> cq = cb.createQuery(entityClass).distinct(true);
		Root<Workflow> rootEntry = cq.from(entityClass);

		cq.where(cb.isNull(rootEntry.get("uniqueID")));

		TypedQuery<Workflow> allQuery = em.createQuery(cq);

		return allQuery.getResultList();
	}

	@Override
	public List<Long> getDataCollectionDefinitionIds() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Workflow> rootEntry = cq.from(entityClass);
		cq.select(rootEntry.<Long> get("dataCollectionDefinitionId")).distinct(true);

		TypedQuery<Long> allQuery = em.createQuery(cq);
		return allQuery.getResultList();
	}

	@Override
	public List<String> getDataCollectionNames(Long organizationId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<String> cq = cb.createQuery(String.class);
		Root<Workflow> rootEntry = cq.from(entityClass);
		cq.select(rootEntry.<String> get("dataCollectionName")).distinct(true);

		if(organizationId != null) {
			cq.where(cb.equal(rootEntry.get("organization").get("id"), organizationId));
		}

		TypedQuery<String> allQuery = em.createQuery(cq);

		return allQuery.getResultList();

	}

	@Override
	protected boolean defaultDistinctValue() {
		return true;
	}
}
