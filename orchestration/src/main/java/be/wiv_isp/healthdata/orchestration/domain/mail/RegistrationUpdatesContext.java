/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.mail;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.DisplayableWorkflowStatus;

import java.util.*;

public class RegistrationUpdatesContext extends AbstractVelocityContext {

    private Map<String, List<RegistrationDetails>> dataCollectionsMap = new HashMap<>();
    private Map<String, Map<Set<DisplayableWorkflowStatus>, Long>> dataCollectionCounts = new HashMap<>();
    private long totalNumberOfRegistrations = 0;

    public void add(String dataCollection, RegistrationDetails details) {
        // Used by HD4DP
        if(dataCollectionsMap.get(dataCollection) == null) {
            dataCollectionsMap.put(dataCollection, new ArrayList<RegistrationDetails>());
        }
        dataCollectionsMap.get(dataCollection).add(details);

        // Used by HD4RES
        if(dataCollectionCounts.get(dataCollection) == null) {
            dataCollectionCounts.put(dataCollection, new HashMap<Set<DisplayableWorkflowStatus>, Long>());
        }

        final Map<Set<DisplayableWorkflowStatus>, Long> map = dataCollectionCounts.get(dataCollection);
        if(map.get(details.getStatuses()) == null) {
            map.put(details.getStatuses(), 1L);
        } else {
            long counter = map.get(details.getStatuses());
            map.put(details.getStatuses(), counter+1);
        }
        totalNumberOfRegistrations++;
    }

    @Override
    protected void doBuild() {
        put("dataCollectionsMap", dataCollectionsMap);
        put("dataCollectionCounts", dataCollectionCounts);
        put("totalNumberOfRegistrations", totalNumberOfRegistrations);
    }

    public static class RegistrationDetails {

        private Long id;
        private String link;
        private Set<DisplayableWorkflowStatus> statuses;
        private String openSince;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public Set<DisplayableWorkflowStatus> getStatuses() {
            return statuses;
        }

        public void setStatuses(Set<DisplayableWorkflowStatus> statuses) {
            this.statuses = statuses;
        }

        public String getOpenSince() {
            return openSince;
        }

        public void setOpenSince(String openSince) {
            this.openSince = openSince;
        }
    }
}
