/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import javax.persistence.*;

@Entity
@Table(name = "EVENTS_DATA_COLLECTION_GROUP")
@PrimaryKeyJoinColumn(name = "EVENT_ID")
public class EventDataCollectionGroup extends Event {

    @Column(name = "DATA_COLLECTION_GROUP_NAME", nullable = false)
    private String dataCollectionGroupName;

    @Column(name = "MAJOR_VERSION", nullable = false)
    private int majorVersion;

    public String getDataCollectionGroupName() {
        return dataCollectionGroupName;
    }

    public void setDataCollectionGroupName(String dataCollectionGroupName) {
        this.dataCollectionGroupName = dataCollectionGroupName;
    }

    public int getMajorVersion() {
        return majorVersion;
    }

    public void setMajorVersion(int majorVersion) {
        this.majorVersion = majorVersion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EventDataCollectionGroup)) return false;
        if (!super.equals(o)) return false;

        EventDataCollectionGroup that = (EventDataCollectionGroup) o;

        if (majorVersion != that.majorVersion) return false;
        return !(dataCollectionGroupName != null ? !dataCollectionGroupName.equals(that.dataCollectionGroupName) : that.dataCollectionGroupName != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (dataCollectionGroupName != null ? dataCollectionGroupName.hashCode() : 0);
        result = 31 * result + majorVersion;
        return result;
    }
}
