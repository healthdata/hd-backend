/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.common.domain.enumeration.TimeUnit;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;

import java.text.MessageFormat;

public class PeriodExpression {

    private int amount;
    private TimeUnit unit;

    public PeriodExpression(int amount, TimeUnit unit) {
        this.amount = amount;
        this.unit = unit;
    }

    public PeriodExpression(final String value) {
        this(value, null);
    }

    public PeriodExpression(final String value, final TimeUnit defaultTimeUnit) {
        try {
            // check if the value only contains a value
            final int amount = Integer.valueOf(value);
            if(defaultTimeUnit != null) {
                this.amount = amount;
                this.unit = defaultTimeUnit;
                return;
            }
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, "no default time unit defined");
            throw exception;

        } catch (NumberFormatException e) {
            // nothing to do. Value can contain a number followed by a unit, which is verified below.
        }

        try {
            // the value must contain a number followed by a unit
            final String firstPart = value.substring(0, value.length() - 1);
            final String secondPart = value.substring(value.length() - 1);

            this.amount = Integer.valueOf(firstPart);
            this.unit = TimeUnit.getByLabel(secondPart);
        } catch (Exception e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, MessageFormat.format("invalid period expression value: {0}", value));
            throw exception;
        }
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public TimeUnit getUnit() {
        return unit;
    }

    public void setUnit(TimeUnit unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        if(amount <= 1) {
            return MessageFormat.format("{0} {1}", amount, unit.getText());
        }
        return MessageFormat.format("{0} {1}s", amount, unit.getText());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof PeriodExpression))
            return false;

        PeriodExpression that = (PeriodExpression) o;

        if (amount != that.amount)
            return false;
        return unit == that.unit;

    }

    @Override
    public int hashCode() {
        int result = amount;
        result = 31 * result + (unit != null ? unit.hashCode() : 0);
        return result;
    }
}
