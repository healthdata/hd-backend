/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.security.authentication.strategy.impl;

import be.wiv_isp.healthdata.common.domain.search.QueryResult;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import be.wiv_isp.healthdata.orchestration.security.authentication.strategy.IUserManagementStrategy;
import be.wiv_isp.healthdata.orchestration.service.IUserService;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class AbstractUserManagementStrategy implements IUserManagementStrategy {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractUserManagementStrategy.class);

    @Autowired
    protected IUserService userService;

    @Override
    public QueryResult<User> getAll(UserSearch search) {
        updateSearch(search);
        return userService.getAllAsQueryResult(search);
    }

    @Override
    public User create(User user, Organization organization) {

        final String username = user.getUsername();

        final UserSearch userSearch = new UserSearch();
        updateSearch(userSearch);
        userSearch.setUsername(username);
        userSearch.setOrganization(organization);

        if (!userService.getAll(userSearch).isEmpty()) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.USERNAME_ALREADY_EXISTS, username);
            throw exception;
        }
        User create;
        try {
            user.setOrganization(organization);
            create = userService.create(user);
        } catch (DataIntegrityViolationException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.CREATE_DUPLICATE_KEY, username);
            throw exception;
        } catch (HealthDataException e){
            throw e;
        } catch (Exception e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.CREATE_GENERAL, "USER");
            throw exception;
        }

        return create;
    }

    @Override
    @Transactional
    public User update(Long id, User user) {
        final User retrievedUser = read(id);

        if(!retrievedUser.getAuthorities().equals(user.getAuthorities())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_USER_UPDATE, "not allowed to modify the role of a user");
            throw exception;
        }

        if (StringUtils.isEmpty(user.getPassword())) {
            user.setPassword(retrievedUser.getPassword());
            return userService.update(user, false);
        } else {
            return userService.update(user, true);
        }
    }

    @Override
    public User read(Long id) {
        final User user = userService.get(id);
        if (user == null) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "USER", id);
            throw exception;
        }
        return user;
    }

    @Override
    public User read(Organization organization) {
        final String username = AuthenticationService.getUserDetails().getUsername();

        final UserSearch userSearch = new UserSearch();
        userSearch.setLdapUser(false);
        userSearch.setUsername(username);
        userSearch.setOrganization(organization);
        return userService.getUnique(userSearch);
    }

    @Override
    public User updateMetadata(Long id, Map<String, String> metaData) {
        final User user = read(id);
        final HDUserDetails principal = AuthenticationService.getUserDetails();
        final String username = user.getUsername();
        if (!username.equals(principal.getUsername())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.UNAUTHORIZED_USER_UPDATE, principal.getUsername(), username);
            throw exception;
        }
        user.setMetaData(metaData);
        final User updatedUser = userService.update(user);
        LOG.info("New metadata has been set for user with id [{}]", id);

        return updatedUser;
    }

    @Override
    public void delete(Long id) {
        final User user = userService.get(id);
        if (user == null) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "USER", id);
            throw exception;
        }
        userService.delete(user);
    }

    @Override
    public Set<String> getEmailAdmins(Organization organization) {
        final UserSearch search = new UserSearch();
        search.setOrganization(organization);
        search.setAuthority(new Authority(Authority.ADMIN));

        final List<User> users = getAll(search).getResult();

        final Set<String> emails = new HashSet<>();
        for (User user : users) {
            if(user.getEmail() != null) {
                emails.add(user.getEmail());
            }
        }

        return emails;
    }

    protected abstract UserSearch updateSearch(UserSearch search);

}
