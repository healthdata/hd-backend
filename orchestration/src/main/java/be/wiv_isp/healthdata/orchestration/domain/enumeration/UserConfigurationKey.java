/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.domain.enumeration;

import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.orchestration.domain.Authority;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static be.wiv_isp.healthdata.common.domain.enumeration.Platform.HD4DP;
import static be.wiv_isp.healthdata.common.domain.enumeration.Platform.HD4RES;

public enum UserConfigurationKey implements IConfigurationKey {

    LOWER_BOUND_FOR_IMMEDIATE_NOTIFICATION(UserConfigurationGroup.NOTIFICATIONS, "Minimal number of new registrations for triggering immediate notification", Integer.class, Arrays.asList(Platform.HD4DP, Platform.HD4RES), Collections.singletonList(Authority.USER_AUTHORITY)),
    NEW_REGISTRATION_PERIOD(UserConfigurationGroup.NOTIFICATIONS, "Period during which a registration is considered new", Integer.class, Arrays.asList(Platform.HD4DP, Platform.HD4RES), Collections.singletonList(Authority.USER_AUTHORITY)),
    OLD_REGISTRATION_PERIOD(UserConfigurationGroup.NOTIFICATIONS, "Period after which a registration is considered old", Integer.class, Arrays.asList(Platform.HD4DP, Platform.HD4RES), Collections.singletonList(Authority.USER_AUTHORITY)),
    REMINDERS_FREQUENCY(UserConfigurationGroup.NOTIFICATIONS, "Frequency of notification reminders", Integer.class, Arrays.asList(Platform.HD4DP, Platform.HD4RES), Collections.singletonList(Authority.USER_AUTHORITY));

    private UserConfigurationGroup group;
    private String description;
    private Class type;
    private List<Platform> platforms;
    private List<Authority> authorities;

    UserConfigurationKey(UserConfigurationGroup group, String description, Class type, List<Platform> platforms, List<Authority> authorities) {
        this.group = group;
        this.description = description;
        this.type = type;
        this.platforms = platforms;
        this.authorities = authorities;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String getType() {
        if (type.isEnum())
            return "ENUM";
        return type.getSimpleName().toUpperCase();
    }

    public Class getFullType() {
        return type;
    }

    public boolean isHd4dp() {
        return platforms.contains(HD4DP);
    }

    public boolean isHd4res() {
        return platforms.contains(HD4RES);
    }

    public List<Authority> getAuthorities() {
        return authorities;
    }
}
