/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.orchestration.annotation.Auditable;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ApiType;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.api.mapper.MailRequestMapper;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.Mail;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.service.IUserService;
import be.wiv_isp.healthdata.orchestration.service.IMailService;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.Message;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Set;

@Component
@Path("/mail")
public class MailRestService {

	@Autowired
	private IMailService mailService;
	@Autowired
	private IUserService userService;

	@Transactional
	@POST
	@Path("/users")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.MAIL)
	@Produces(MediaType.APPLICATION_JSON)
	public Response sendToUsers(@Context UriInfo info, String json) throws JSONException{
		final HDUserDetails principal = AuthenticationService.getUserDetails();
		Mail mail = MailRequestMapper.convert(new JSONObject(json));

		User user = userService.get(principal.getUsername(), principal.getOrganization());
		if(StringUtils.isBlank(user.getEmail())) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_PARAMETER, "your email needs to be configured");
			throw exception;
		}
		mail.setFrom(user.getEmail());

		Set<String> emailUsers = mailService.getEmailUsers(principal.getOrganization());
		mail.setTo(emailUsers);
		mail.setStatus(Mail.Status.NEW);

		mailService.create(mail);
		mailService.send(mail, Message.RecipientType.BCC);
		if( Mail.Status.SENT_WITH_ERROR.equals(mail.getStatus())) {
			return Response.ok(mail.getError()).build();
		}
		return Response.ok().build();
	}
}
