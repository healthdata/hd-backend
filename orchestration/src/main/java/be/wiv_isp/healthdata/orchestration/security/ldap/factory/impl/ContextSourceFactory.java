/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.security.ldap.factory.impl;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.security.ldap.factory.IContextSourceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;
import org.springframework.stereotype.Service;

import java.util.Hashtable;

@Service
public class ContextSourceFactory implements IContextSourceFactory {

    private static final Logger LOG = LoggerFactory.getLogger(ContextSourceFactory.class);

    public static final String FORWARD_SLASH = "/";

    @Autowired
    private IConfigurationService configurationService;

    @Override
    public ContextSource create() {
        return create(null);
    }

    @Override
    public ContextSource create(String referralValue) {
        final String serverUrl = configurationService.get(ConfigurationKey.LDAP_SERVER_URL).getValue();
        LOG.debug("serverUrl = " + serverUrl);
        final String username = configurationService.get(ConfigurationKey.LDAP_USER).getValue();
        LOG.debug("username = " + username);
        final String password = configurationService.get(ConfigurationKey.LDAP_PASSWORD).getValue();
        LOG.debug("password = ***");

        final int i = serverUrl.indexOf(FORWARD_SLASH, 10); // starting at 10 to ignore forward slash in protocol (ldap:// or ldaps://)

        String url, base;
        if(i == -1) {
            url = serverUrl;
            base = null;
        } else {
            url = serverUrl.substring(0, i);
            base = serverUrl.substring(i+1);
        }

        LOG.debug("url = " + url);
        LOG.debug("base = " + base);

        DefaultSpringSecurityContextSource contextSource;
        try {
            contextSource = new DefaultSpringSecurityContextSource(url);
        } catch (Exception e) {
            LOG.warn("Could not create LDAP context source: {}", e.getMessage());
            return new LdapContextSource();
        }

        contextSource.setUserDn(username);
        contextSource.setPassword(password);
        contextSource.setReferral(referralValue);
        contextSource.setBase(base);

        final Hashtable<String, Object> env = new Hashtable<>();
        env.put("com.sun.jndi.ldap.read.timeout", "30000");
        env.put("com.sun.jndi.ldap.connect.timeout", "30000");
        contextSource.setBaseEnvironmentProperties(env);

        try {
            contextSource.afterPropertiesSet();
        } catch (Exception e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.LDAP_EXCEPTION, "error while creating context source");
            throw exception;
        }
        return contextSource;
    }
}


