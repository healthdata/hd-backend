/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.tasks;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionListDto;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupListDto;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupListMap;
import be.wiv_isp.healthdata.orchestration.domain.AbstractParticipationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.search.AbstractParticipationWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.service.IAbstractParticipationWorkflowService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionGroupForwardService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.*;

@Component
public class ParticipationGroupMigrationTask extends HealthDataTask {

    private static final Logger LOG = LoggerFactory.getLogger(ParticipationGroupMigrationTask.class);

    @Autowired
    private IOrganizationService organizationService;
    @Autowired
    private IAbstractParticipationWorkflowService<AbstractParticipationWorkflow, AbstractParticipationWorkflowSearch> workflowService;
    @Autowired
    private IDataCollectionGroupForwardService dataCollectionGroupService;
    @Autowired
    @Qualifier("transactionManager")
    protected PlatformTransactionManager txManager;

    @Override
    public String getName() {
        return "PARTICIPATION_MIGRATION";
    }

    @Override
    protected void execute() {
        LOG.info("Start Participation-Group migration");
        TransactionTemplate tmpl = new TransactionTemplate(txManager);
        tmpl.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                List<Organization> organizations = organizationService.getAll();
                for (Organization organization : organizations) {
                    Map<Long, Long> dcdToDcgIds = dcdToDcgIds(organization);
                    AbstractParticipationWorkflowSearch search = workflowService.getSearchInstance();
                    search.setOrganization(organization);
                    search.setWithDocument(null);
                    List<Long> workflows = workflowService.getWorkflowIds(search);
                    Set<Long> ids = new HashSet<>(workflows);
                    for (Long workflowId : ids) {
                        AbstractParticipationWorkflow workflow = workflowService.get(workflowId);
                        Long oldDcgId = workflow.getDataCollectionDefinitionId();
                        Long newDcgId = dcdToDcgIds.get(oldDcgId);
                        LOG.info("Participation workflow [{}]: change dcg from [{}] to [{}]", workflow.getId(), oldDcgId, newDcgId);
                        if(newDcgId != null) {
                            workflow.setDataCollectionDefinitionId(newDcgId);
                            workflowService.update(workflow);
                        }
                    }
                }
            }
        });
    }

    private Map<Long, Long> dcdToDcgIds(Organization organization) {
        Map<Long, Long> dcdToDcgIds = new HashMap<>();
        DataCollectionGroupListMap map = dataCollectionGroupService.list(organization);
        for (List<DataCollectionGroupListDto> dataCollectionGroupListDtos : map.values()) {
            for (DataCollectionGroupListDto dcg : dataCollectionGroupListDtos) {
                for (DataCollectionDefinitionListDto dcd : dcg.getDataCollectionDefinitions()) {
                    dcdToDcgIds.put(dcd.getDataCollectionDefinitionId(), dcg.getDataCollectionGroupId());
                }
            }
        }
        return dcdToDcgIds;
    }
}
