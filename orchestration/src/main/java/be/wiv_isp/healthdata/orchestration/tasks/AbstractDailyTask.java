/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.tasks;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public abstract class AbstractDailyTask extends SchedulableHealthDataTask {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractDailyTask.class);

    @Autowired
    protected DataCollectionGroupNotificationTask dataCollectionGroupNotificationTask;
    @Autowired
    protected StatusMessageTask statusMessageTask;
    @Autowired
    protected DataCollectionGroupReplicationTask dataCollectionGroupReplicationTask;
    @Autowired
    protected PasswordResetTokenCleanUpTask passwordResetTokenCleanUpTask;

    @Override
    public String getName() {
        return "DAILY";
    }

    @Override
    protected String getTimingExpression() {
        return configurationService.get(ConfigurationKey.DAILY_TASK_EXECUTION_TIME).getValue();
    }

    @Override
    public void execute() {
        final List<HealthDataTask> tasks = getTasks();
        for (HealthDataTask task : tasks) {
            task.run();

            if(task.hasException()) {
                handleException(task.getException());
            }
        }
    }

    protected abstract void handleException(Exception e);

    protected abstract List<HealthDataTask> getTasks();
}
