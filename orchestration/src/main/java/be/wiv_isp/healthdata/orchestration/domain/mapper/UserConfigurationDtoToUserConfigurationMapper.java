/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.domain.mapper;

import be.wiv_isp.healthdata.orchestration.domain.UserConfiguration;
import be.wiv_isp.healthdata.orchestration.dto.UserConfigurationDto;

public class UserConfigurationDtoToUserConfigurationMapper implements Mapper<UserConfigurationDto, UserConfiguration> {

	@Override
	public UserConfiguration map(UserConfigurationDto dto) {
		final UserConfiguration userConfiguration = new UserConfiguration();
		userConfiguration.setId(dto.getId());
		userConfiguration.setKey(dto.getKey());
		userConfiguration.setValue(dto.getValue());
		userConfiguration.setDefaultValue(dto.getDefaultValue());
		return userConfiguration;
	}
}