/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dto;

import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import be.wiv_isp.healthdata.orchestration.domain.AbstractDocument;
import be.wiv_isp.healthdata.orchestration.domain.AbstractWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.AbstractWorkflowHistory;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.sql.Timestamp;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

abstract public class AbstractWorkflowDto<Workflow extends AbstractWorkflow<Document, WorkflowHistory>, Document extends AbstractDocument, WorkflowHistory extends AbstractWorkflowHistory> {

	protected String id;
	protected WorkflowStatus status;
	protected DocumentDto document;
	protected String dataCollectionName;
	protected Long dataCollectionDefinitionId;
	protected SortedSet<WorkflowHistory> history;
	protected String sendStatus;
	protected Timestamp createdOn;
	protected Timestamp updatedOn;

	public AbstractWorkflowDto() {

	}

	public AbstractWorkflowDto(Workflow workflow) {
		setIdAsLong(workflow.getId());
		setStatus(workflow.getStatus());
		if(workflow.getDocument() != null) {
			setDocument(new DocumentDto(workflow.getDocument()));
		}
		setDataCollectionName(workflow.getDataCollectionName());
		setDataCollectionDefinitionId(workflow.getDataCollectionDefinitionId());
		setFilteredHistory(workflow.getHistory());
		setSendStatus(workflow.getSendStatus());
		setCreatedOn(workflow.getCreatedOn());
		setUpdatedOn(workflow.getUpdatedOn());
	}

	public AbstractWorkflowDto(AbstractWorkflowDto dto) {
		id = dto.getId();
		status = dto.getStatus();
		document = dto.getDocument();
		dataCollectionName = dto.getDataCollectionName();
		dataCollectionDefinitionId = dto.getDataCollectionDefinitionId();
		history = dto.getHistory();
		sendStatus = dto.getSendStatus();
		createdOn = dto.getCreatedOn();
		updatedOn = dto.getUpdatedOn();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonIgnore
	public Long getIdAsLong() {
		return Long.valueOf(id);
	}

	public void setIdAsLong(Long id) {
		this.id = String.valueOf(id);
	}


	public WorkflowStatus getStatus() {
		return status;
	}

	public void setStatus(WorkflowStatus status) {
		this.status = status;
	}

	@JsonIgnore
	public DocumentDto getDocument() {
		return document;
	}

	public void setDocument(DocumentDto document) {
		this.document = document;
	}

	public String getDataCollectionName() {
		return dataCollectionName;
	}

	public void setDataCollectionName(String dataCollectionName) {
		this.dataCollectionName = dataCollectionName;
	}

	public Long getDataCollectionDefinitionId() {
		return dataCollectionDefinitionId;
	}

	public void setDataCollectionDefinitionId(Long dataCollectionDefinitionId) {
		this.dataCollectionDefinitionId = dataCollectionDefinitionId;
	}

	@JsonSerialize(using = TimestampSerializer.class)
	public Timestamp getCreatedOn() {
		return createdOn;
	}

	@JsonDeserialize(using = TimestampDeserializer.class)
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	@JsonSerialize(using = TimestampSerializer.class)
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	@JsonDeserialize(using = TimestampDeserializer.class)
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public SortedSet<WorkflowHistory> getHistory() {
		return history;
	}

	public void setHistory(SortedSet<WorkflowHistory> history) {
		this.history = history;
	}

	public void setFilteredHistory(List<WorkflowHistory> history) {
		if (history != null) {
			this.history = new TreeSet<>(history);
		}
	}

	public String getSendStatus() {
		return sendStatus;
	}

	public void setSendStatus(String sendStatus) {
		this.sendStatus = sendStatus;
	}

}