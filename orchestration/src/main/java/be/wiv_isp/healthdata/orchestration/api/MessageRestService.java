/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageStatus;
import be.wiv_isp.healthdata.orchestration.domain.search.MessageSearch;
import be.wiv_isp.healthdata.orchestration.service.IMessageProcessingService;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.text.MessageFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Path("/messages")
public class MessageRestService {

	private static final Logger LOG = LoggerFactory.getLogger(MessageRestService.class);
	
	@Autowired
	private IMessageService messageService;
	
	@Autowired
	private IMessageProcessingService messageProcessingService;

	@Autowired
	private IOrganizationService organizationService;

	@GET
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(@Context UriInfo info) {
		final List<Message> messages = getMessages(info);
		return Response.ok(messages).build();
	}
	
	@POST
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Path("/reprocess")
	@Produces(MediaType.APPLICATION_JSON)
	public Response reprocess(@Context UriInfo info) {
		final List<Message> messages = getMessages(info);

		final Map<Long, String> results = new HashMap<>();
		for(final Message message : messages) {
			LOG.info(MessageFormat.format("Reprocessing message with id [{0}].", message.getId()));

			if(MessageStatus.ERROR_PROCESSING.equals(message.getStatus())) {
				try {
					messageProcessingService.process(message);
					results.put(message.getId(), "OK");
				} catch (Exception e) {
					results.put(message.getId(), "NOK: " + e.getMessage());
				}
			} else {
				results.put(message.getId(), MessageFormat.format("Message can not be reprocessed. Message status must be equal to {0}. Message status is {1}.", MessageStatus.ERROR_PROCESSING, message.getStatus()));
			}
		}
		return Response.ok(results).build();
	}

	@POST
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Path("/resend")
	@Produces(MediaType.APPLICATION_JSON)
	public Response resend(@Context UriInfo info) {
		final List<Message> messages = getMessages(info);

		final Map<Long, String> results = new HashMap<>();
		for(final Message message : messages) {
			if (MessageStatus.ERROR_SENDING.equals(message.getStatus()) || MessageStatus.SENT.equals(message.getStatus())) {
				LOG.info(MessageFormat.format("Updating status of message with id [{0}] to [{1}].", message.getId(), MessageStatus.OUTBOX));
				message.setStatus(MessageStatus.OUTBOX);
				message.setRemainingSendingTrials(Message.SENDING_TRIALS);
				messageService.update(message);
				results.put(message.getId(), "OK");
			}
			else {
				results.put(message.getId(), "NOK: " + MessageFormat.format("Message can not be resent. Message status must be equal to {0} or {1}. Message status is {2}.", MessageStatus.ERROR_PROCESSING, MessageStatus.SENT, message.getStatus()));
			}
		}

		return Response.ok(results).build();
	}

	@DELETE
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Path("/delete")
	public Response delete(@Context UriInfo info) {
		final List<Message> messages = getMessages(info);

		for(final Message message : messages) {
			final Long messageId = message.getId();
			LOG.info(MessageFormat.format("Deleting message with id {0}", messageId));
			messageService.delete(messageId);
		}

		return Response.ok().build();
	}

	private List<Message> getMessages(UriInfo info) {
		final MessageSearch search = new MessageSearch();
		final MultivaluedMap<String, String> params = info.getQueryParameters();

		for (String key : params.keySet()) {
			final String value = params.getFirst(key);
			if ("id".equalsIgnoreCase(key)) {
				search.setId(Long.valueOf(value));
			}
			if ("status".equalsIgnoreCase(key)) {
				search.setStatuses(MessageStatus.valueOf(value));
			}
			if ("organizationId".equalsIgnoreCase(key)) {
				final Long organizationId = Long.valueOf(value);
				final Organization organization = organizationService.get(organizationId);
				search.setOrganization(organization);
			}
			if ("sentBefore".equalsIgnoreCase(key)) {
				final Date sentBefore = DateFormat.stringToDate(value, DateFormat.DATE);
				search.setSentBefore(sentBefore);
			}
			if ("linkedId".equalsIgnoreCase(key)) {
				final Long linkedId = Long.valueOf(value);
				search.setLinkedId(linkedId);
			}
			if ("identificationType".equalsIgnoreCase(key)) {
				search.setIdentificationType(value);
			}
			if ("identificationValue".equalsIgnoreCase(key)) {
				search.setIdentificationValue(value);
			}
			if ("workflowId".equalsIgnoreCase(key)) {
				search.setWorkflowId(Long.valueOf(value));
			}
			if ("hasRemainingTrials".equalsIgnoreCase(key)) {
				search.setHasRemainingSendingTrials(Boolean.valueOf(value));
			}
		}
		return messageService.getAll(search);
	}
}
