/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.domain.search;

import be.wiv_isp.healthdata.common.domain.search.EntitySearch;
import be.wiv_isp.healthdata.orchestration.domain.AbstractElasticSearchInfo;

public class ElasticSearchInfoSearch extends EntitySearch {

    private Long workflowId;
    private AbstractElasticSearchInfo.Status status;
    private Long organizationId;
    private String dataCollectionName;

    public Long getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(Long workflowId) {
        this.workflowId = workflowId;
    }

    public AbstractElasticSearchInfo.Status getStatus() {
        return status;
    }

    public void setStatus(AbstractElasticSearchInfo.Status status) {
        this.status = status;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getDataCollectionName() {
        return dataCollectionName;
    }

    public void setDataCollectionName(String dataCollectionName) {
        this.dataCollectionName = dataCollectionName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ElasticSearchInfoSearch)) return false;

        ElasticSearchInfoSearch that = (ElasticSearchInfoSearch) o;

        if (workflowId != null ? !workflowId.equals(that.workflowId) : that.workflowId != null) return false;
        if (status != that.status) return false;
        if (organizationId != null ? !organizationId.equals(that.organizationId) : that.organizationId != null)
            return false;
        return !(dataCollectionName != null ? !dataCollectionName.equals(that.dataCollectionName) : that.dataCollectionName != null);

    }

    @Override
    public int hashCode() {
        int result = workflowId != null ? workflowId.hashCode() : 0;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (organizationId != null ? organizationId.hashCode() : 0);
        result = 31 * result + (dataCollectionName != null ? dataCollectionName.hashCode() : 0);
        return result;
    }
}
