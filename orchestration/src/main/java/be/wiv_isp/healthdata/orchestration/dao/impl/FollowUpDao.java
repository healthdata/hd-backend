/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.orchestration.dao.IFollowUpDao;
import be.wiv_isp.healthdata.orchestration.domain.FollowUp;
import be.wiv_isp.healthdata.orchestration.domain.search.FollowUpSearch;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class FollowUpDao extends CrudDaoV2<FollowUp, Long, FollowUpSearch> implements IFollowUpDao<FollowUp> {

    public FollowUpDao() {
        super(FollowUp.class);
    }

    @Override
    protected List<Predicate> getPredicates(FollowUpSearch search, CriteriaBuilder cb, Root<FollowUp> rootEntry) {
        final List<Predicate> predicates = new ArrayList<>();

        if (search.getName() != null) {
            predicates.add(cb.equal(rootEntry.get("name"), search.getName()));
        }
        if (search.isActive() != null) {
            predicates.add(cb.equal(rootEntry.get("active"), search.isActive()));
        }
        if (search.getActivationDateBefore() != null) {
            predicates.add(cb.lessThanOrEqualTo(rootEntry.<Date>get("activationDate"), search.getActivationDateBefore()));
        }

        return predicates;
    }
}
