/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.common.api.exception.BadRequestException;
import be.wiv_isp.healthdata.common.api.exception.NotFoundException;
import be.wiv_isp.healthdata.common.api.exception.UnauthorizedException;
import be.wiv_isp.healthdata.common.domain.search.QueryResult;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.pagination.PaginationHelper;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.orchestration.annotation.Auditable;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ApiType;
import be.wiv_isp.healthdata.orchestration.domain.mapper.UserConfigurationDtoToUserConfigurationMapper;
import be.wiv_isp.healthdata.orchestration.domain.search.UserConfigurationSearch;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import be.wiv_isp.healthdata.orchestration.domain.search.ViewSearch;
import be.wiv_isp.healthdata.orchestration.dto.UserConfigurationDto;
import be.wiv_isp.healthdata.orchestration.dto.UserDto;
import be.wiv_isp.healthdata.orchestration.service.IUserConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IUserManagementService;
import be.wiv_isp.healthdata.orchestration.service.IUserService;
import be.wiv_isp.healthdata.orchestration.service.IViewService;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@Path("/users")
public class UserRestService {

	private static final Logger LOG = LoggerFactory.getLogger(UserRestService.class);

	@Autowired
	protected IUserManagementService userManagementService;

	@Autowired
	protected IViewService viewService;

	@Autowired
	protected IUserConfigurationService userConfigurationService;

	@Autowired
	protected IUserService userService;

	@GET
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.USER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(@HeaderParam("Range") String range) {
		LOG.info("GET Request: Retrieve the list with all users");

		final UserSearch search = new UserSearch();
		search.setPaginationRange(PaginationHelper.buildPaginationRange(range));
		search.setOrganization(AuthenticationService.getAuthenticatedOrganization());
		search.setExcludeAuthority(Authority.SUPPORT_AUTHORITY);
		final QueryResult<User> queryResult = userManagementService.getUserManagementStrategy().getAll(search);

		if (search.isPaginated()) {
			final String responseRange = PaginationHelper.buildResponseRangeString(queryResult.getRange());
			return Response.ok(convertUsers(queryResult.getResult())).status(206).header(HttpHeaders.CONTENT_RANGE.getValue(), responseRange).build();
		} else {
			return Response.ok(convertUsers(queryResult.getResult())).build();
		}
	}

	@POST
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.USER)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(User user) {
		LOG.info("POST Request: Creation of a new user with username [{}]", user.getUsername());
		final User create = userManagementService.getUserManagementStrategy().create(user, AuthenticationService.getAuthenticatedOrganization());
		return Response.ok(new UserDto(create)).build();
	}

	@GET
	@Path("/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.USER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response read(@PathParam("id") Long id) {
		LOG.info("GET Request: Get user with id [{}]", id);
		final User user = userManagementService.getUserManagementStrategy().read(id);
		return Response.ok(new UserDto(user)).build();
	}

	@GET
	@Path("/me")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
	@Auditable(apiType = ApiType.USER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response read() {
		LOG.info("GET Request: Get own user information");
		final User user = userManagementService.getUserManagementStrategy().read(AuthenticationService.getAuthenticatedOrganization());
		return Response.ok(new UserDto(user)).build();
	}

	@PUT
	@Path("/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.USER)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") Long id, User user) {
		LOG.info("PUT Request: Update user with id [{}]", id);
		final User updatedUser = userManagementService.getUserManagementStrategy().update(id, user);
		return Response.ok(new UserDto(updatedUser)).build();
	}

	@PUT
	@Path("/password/{id}")
	@Auditable(apiType = ApiType.USER)
	@PreAuthorize("hasRole('ROLE_USER')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updatePassword(@PathParam("id") Long id, @Context UriInfo info) {
		LOG.info("PUT Request: Update password for id [{}]", id);
		final String oldPassword = RestUtils.getParameterString(info, "oldPassword");
		final String newPassword = RestUtils.getParameterString(info, "newPassword");
		final User updatedUser = userManagementService.getUserManagementStrategy().updatePassword(id, oldPassword, newPassword);
		return Response.ok(new UserDto(updatedUser)).build();
	}

	@PUT
	@Path("/metadata/{id}")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
	@Auditable(apiType = ApiType.USER)
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateMetadata(@PathParam("id") Long id, Map<String, String> metaData) {
		LOG.info("PUT Request: Update metaData for id [{}]", id);
		final User updatedUser = userManagementService.getUserManagementStrategy().updateMetadata(id, metaData);
		return Response.ok(new UserDto(updatedUser)).build();
	}

	@DELETE
	@Path("/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.USER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") Long id) {
		LOG.info("DELETE Request: Delete user with id [{}]", id);
		userManagementService.getUserManagementStrategy().delete(id);
		return Response.noContent().build();
	}

	@POST
	@Path("/views/{dataCollectionDefinitionId}/{viewName}")
	@Auditable(apiType = ApiType.USER)
	@PreAuthorize("hasRole('ROLE_USER')")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createView(@Context UriInfo info, String json) {
		final ViewSearch search = getViewSearch(info);
		LOG.info("POST Request: creating view with name [{}] for data collection definition id [{}]", search.getName(), search.getDataCollectionDefinitionId());

		final View existingView = viewService.getUnique(search);
		if(existingView != null) {
			return Response.status(Response.Status.CONFLICT).entity("View already exists").build();
		}
		final View view = new View();
		view.setName(search.getName());
		User user = userManagementService.getUserManagementStrategy().read(AuthenticationService.getAuthenticatedOrganization());
		view.setUserId(user.getId());
		view.setDataCollectionDefinitionId(search.getDataCollectionDefinitionId());
		view.setContent(json.getBytes(StandardCharsets.UTF_8));

		final View created = viewService.create(view);
		return Response.ok(created).build();
	}

	@PUT
	@Path("/views/{dataCollectionDefinitionId}/{viewName}")
	@Auditable(apiType = ApiType.USER)
	@PreAuthorize("hasRole('ROLE_USER')")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateView(@Context UriInfo info, String json) {
		final ViewSearch search = getViewSearch(info);
		LOG.info("PUT Request: updating view with name [{}] for data collection definition id [{}]", search.getName(), search.getDataCollectionDefinitionId());

		final View view = viewService.getUnique(search);
		if(view == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		view.setContent(json.getBytes(StandardCharsets.UTF_8));
		final View updated = viewService.update(view);
		return Response.ok(updated).build();
	}

	@GET
	@Path("/views/{dataCollectionDefinitionId}")
	@Auditable(apiType = ApiType.USER)
	@PreAuthorize("hasRole('ROLE_USER')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getViews(@Context UriInfo info) {
		final ViewSearch search = getViewSearch(info);
		LOG.info("GET Request: retrieving views for data collection definition id [{}]", search.getDataCollectionDefinitionId());

		final List<View> views = viewService.getAll(search);
		return Response.ok(views).build();
	}

	@DELETE
	@Path("/views/{dataCollectionDefinitionId}/{viewName}")
	@Auditable(apiType = ApiType.USER)
	@PreAuthorize("hasRole('ROLE_USER')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteView(@Context UriInfo info) {
		final ViewSearch search = getViewSearch(info);
		LOG.info("DELETE Request: deleting view with name [{}] for data collection definition id [{}]", search.getName(), search.getDataCollectionDefinitionId());

		final View view = viewService.getUnique(search);
		if(view == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		viewService.delete(view);
		return Response.noContent().build();
	}

	private ViewSearch getViewSearch(final UriInfo info) {
		final User user = userManagementService.getUserManagementStrategy().read(AuthenticationService.getAuthenticatedOrganization());

		final ViewSearch search = new ViewSearch();
		search.setName(RestUtils.getPathString(info, RestUtils.ParameterName.VIEW_NAME));
		search.setDataCollectionDefinitionId(RestUtils.getPathLong(info, RestUtils.ParameterName.DATA_COLLECTION_DEFINITION_ID));
		search.setUserId(user.getId());
		return search;
	}

	@GET
	@Path("/configurations")
	@Auditable(apiType = ApiType.USER)
	@PreAuthorize("hasRole('ROLE_USER')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getConfigurations() {
		final User currentUser = getCurrentUser();

		LOG.info("GET Request: retrieving configurations for user {}", currentUser.toStringCompact());
		final UserConfigurationSearch search = new UserConfigurationSearch();
		search.setUserId(currentUser.getId());

		final List<UserConfiguration> configurations = userConfigurationService.getAll(search);
		return Response.ok(convertConfigurations(configurations)).build();
	}

	@PUT
	@Path("/configurations/{id}")
	@Auditable(apiType = ApiType.USER)
	@PreAuthorize("hasRole('ROLE_USER')")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateConfiguration(@PathParam("id") Long id, final UserConfigurationDto userConfigurationDto) {
		LOG.info("PUT Request: Update configuration: [{}]", userConfigurationDto);

		if (id == null || !id.equals(userConfigurationDto.getId())) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_INPUT, MessageFormat.format("The id provided in the url ({0}) does not match with the id provided in the request body ({1})", id != null ? id.toString() : null, userConfigurationDto.getId().toString()));
			throw exception;
		}
		final UserConfiguration existingUserConfiguration = userConfigurationService.get(id);
		if(existingUserConfiguration == null) {
			throw new NotFoundException("No user configuration found with id " + id);
		}

		final User currentUser = getCurrentUser();
		if(!currentUser.getId().equals(existingUserConfiguration.getUser().getId())) {
			throw new UnauthorizedException(MessageFormat.format("Authenticated user (id= {0}) is not allowed to updated the configuration of another user (id={1})", currentUser.getId(), existingUserConfiguration.getUser().getId()));
		}

		if(!existingUserConfiguration.getKey().equals(userConfigurationDto.getKey())) {
			throw new BadRequestException(MessageFormat.format("Key can not be updated. Key for configuration with id {0} is {1}. Key in the body is {2}", id, existingUserConfiguration.getKey(), userConfigurationDto.getKey()));
		}

		final UserConfiguration configuration = new UserConfigurationDtoToUserConfigurationMapper().map(userConfigurationDto);
		configuration.setUser(currentUser);
		final UserConfiguration updatedConfiguration = userConfigurationService.update(configuration);
		return Response.ok(new UserConfigurationDto(updatedConfiguration)).build();
	}

	@GET
	@Path("/admins")
	@PreAuthorize("hasRole('ROLE_USER')")
	@Auditable(apiType = ApiType.USER)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllAdmins() {
		LOG.info("GET Request: Get all admins");
		UserSearch search = new UserSearch();
		search.setAuthority(Authority.ADMIN_AUTHORITY);
		search.setExcludeAuthority(Authority.SUPPORT_AUTHORITY);
		search.setOrganization(AuthenticationService.getAuthenticatedOrganization());
		List<User> admins = userService.getAll(search);

		return Response.ok(convertUsers(admins)).build();
	}

	private List<UserConfigurationDto> convertConfigurations(final List<UserConfiguration> configurations) {
		final List<UserConfigurationDto> dtos = new ArrayList<>();
		for (UserConfiguration configuration : configurations) {
			dtos.add(new UserConfigurationDto(configuration));
		}
		return dtos;
	}

	private User getCurrentUser() {
		final HDUserDetails userDetails = AuthenticationService.getUserDetails();
		final UserSearch userSearch = new UserSearch();
		userSearch.setUsername(userDetails.getUsername());
		userSearch.setOrganization(userDetails.getOrganization());
		return userService.getUnique(userSearch);
	}

	private List<UserDto> convertUsers(List<User> users) {
		final List<UserDto> dtos = new ArrayList<>();
		for (User user : users) {
			dtos.add(new UserDto(user));
		}
		return dtos;
	}
}
