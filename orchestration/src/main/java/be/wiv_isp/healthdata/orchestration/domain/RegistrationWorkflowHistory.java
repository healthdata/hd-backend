/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Entity
@Table(name = "WORKFLOW_HISTORIES")
public class RegistrationWorkflowHistory extends AbstractWorkflowHistory {

    @Fetch(FetchMode.JOIN)
	@OneToOne(mappedBy = "workflowHistory", cascade = CascadeType.ALL)
	private RegistrationProgress progress;

    @Embedded
    protected WorkflowFlags flags = new WorkflowFlags();

    public WorkflowFlags getFlags() {
        return flags;
    }

    public void setFlags(WorkflowFlags flags) {
        this.flags = flags;
    }

    @Override
    public RegistrationProgress getProgress() {
		return progress;
	}

    @Override
	public void setProgress(Progress progress) {
        this.progress = (RegistrationProgress) progress;
	}


}