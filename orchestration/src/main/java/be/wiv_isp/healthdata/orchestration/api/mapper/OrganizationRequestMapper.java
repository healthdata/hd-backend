/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api.mapper;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.util.Iterator;

public class OrganizationRequestMapper {

	@SuppressWarnings("unchecked")
	static public Organization convert(JSONObject json) throws JSONException {
		Organization organisation = new Organization();
		for (Iterator<String> iterator = json.keys(); iterator.hasNext();) {
			String key = iterator.next();
			if ("id".equalsIgnoreCase(key)) {
				organisation.setId(json.getLong(key));
			} else if ("healthDataIDType".equalsIgnoreCase(key)) {
				organisation.setHealthDataIDType(json.getString(key));
			} else if ("healthDataIDValue".equalsIgnoreCase(key)) {
				organisation.setHealthDataIDValue(json.getString(key));
			} else if ("name".equalsIgnoreCase(key)) {
				organisation.setName(json.getString(key));
			} else if ("main".equalsIgnoreCase(key)) {
				organisation.setMain(json.getBoolean(key));
			}
		}
		return organisation;
	}
}
