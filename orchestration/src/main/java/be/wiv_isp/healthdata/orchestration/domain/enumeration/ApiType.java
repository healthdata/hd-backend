/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.domain.enumeration;

public enum ApiType {
	ABOUT("ABOUT"),
	ATTACHMENT("ATTACHMENTS"),
	CONFIGURATION("CONFIGURATIONS"),
	DATA_COLLECTION("DATA COLLECTIONS"),
	DATA_COLLECTION_ACCESS_REQUEST("DATA COLLECTION ACCESS REQUEST"),
	DATA_COLLECTION_DEFINITION_MANAGER("DATA COLLECTION DEFINITION MANAGER"),
	DWH("DATA WAREHOUSE"),
	LDAP("LDAP"),
	MAIL("MAIL"),
	MESSAGE("MESSAGES"),
	ORGANIZATION("ORGANIZATION"),
	PATIENT("PATIENT"),
	REGISTRATION("REGISTRATION"),
	REGISTRATION_STATISTICS("REGISTRATION_STATISTICS"),
	STABLE_DATA("STABLE_DATA"),
	STATUS_MESSAGE("STATUS_MESSAGES"),
	TEMPLATE("TEMPLATES"),
	TRANSLATION("TRANSLATIONS"),
	USER("USERS"),
	USER_REQUEST("USER_REQUESTS"),
	WORKFLOW("WORKFLOWS");

	private final String description;

	ApiType(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}
}
