/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.AbstractParticipationDocument;
import be.wiv_isp.healthdata.orchestration.domain.search.AbstractParticipationDocumentSearch;
import be.wiv_isp.healthdata.orchestration.service.IAbstractParticipationDocumentService;

abstract public class AbstractParticipationDocumentService<Document extends AbstractParticipationDocument, Search extends AbstractParticipationDocumentSearch> extends AbstractDocumentService<Document, Search> implements IAbstractParticipationDocumentService<Document, Search> {
}