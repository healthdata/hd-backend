/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.action.standalone;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.common.json.serializer.ByteArrayToStringSerializer;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.StandAloneActionType;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public abstract class AbstractStableDataUploadAction extends AbstractStandAloneAction {

    protected Long stableDataId;
    protected HealthDataIdentification healthDataIdentification;
    protected String dataCollectionName;
    protected String patientId;
    protected byte[] csv;

    protected List<Long> successes = new ArrayList<>();
    protected List<Long> failures = new ArrayList<>();
    protected Set<String> emails;

    @Override
    public StandAloneActionType getAction() {
        return StandAloneActionType.STABLE_DATA_UPLOAD;
    }

    public Long getStableDataId() {
        return stableDataId;
    }

    public void setStableDataId(Long stableDataId) {
        this.stableDataId = stableDataId;
    }

    public HealthDataIdentification getHealthDataIdentification() {
        return healthDataIdentification;
    }

    public void setHealthDataIdentification(HealthDataIdentification healthDataIdentification) {
        this.healthDataIdentification = healthDataIdentification;
    }

    public String getDataCollectionName() {
        return dataCollectionName;
    }

    public void setDataCollectionName(String dataCollectionName) {
        this.dataCollectionName = dataCollectionName;
    }

    @JsonSerialize(using = ByteArrayToStringSerializer.class)
    public byte[] getCsv() {
        return csv;
    }

    public void setCsv(byte[] csv) {
        this.csv = csv;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getPatientId() {
        return patientId;
    }

    public List<Long> getSuccesses() {
        return successes;
    }

    public void setSuccesses(List<Long> successes) {
        this.successes = successes;
    }

    public List<Long> getFailures() {
        return failures;
    }

    public void setFailures(List<Long> failures) {
        this.failures = failures;
    }

    public Set<String> getEmails() {
        return emails;
    }

    public void setEmails(Set<String> emails) {
        this.emails = emails;
    }
}
