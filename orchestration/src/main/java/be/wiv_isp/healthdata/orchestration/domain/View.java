/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.common.json.deserializer.JsonToByteArrayDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.ByteArrayToJsonSerializer;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.persistence.*;
import java.util.Arrays;

@Entity
@Table(name = "VIEWS")
public class View {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;

	@Column(name = "USER_ID", nullable = false)
	private Long userId;

	@Column(name = "NAME")
	private String name;

	@Column(name = "DATA_COLLECTION_DEFINITION_ID")
	private Long dataCollectionDefinitionId;

	@Lob
	@Column(name = "CONTENT", nullable = false)
	private byte[] content;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getDataCollectionDefinitionId() {
		return dataCollectionDefinitionId;
	}

	public void setDataCollectionDefinitionId(Long dataCollectionDefinitionId) {
		this.dataCollectionDefinitionId = dataCollectionDefinitionId;
	}

	@JsonIgnore
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonSerialize(using = ByteArrayToJsonSerializer.class)
	public byte[] getContent() {
		return content;
	}

	@JsonDeserialize(using = JsonToByteArrayDeserializer.class)
	public void setContent(byte[] content) {
		this.content = content;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof View))
			return false;

		View view = (View) o;

		if (!userId.equals(view.userId))
			return false;
		if (!name.equals(view.name))
			return false;
		if (!dataCollectionDefinitionId.equals(view.dataCollectionDefinitionId))
			return false;
		return Arrays.equals(content, view.content);

	}

	@Override
	public int hashCode() {
		int result = userId.hashCode();
		result = 31 * result + name.hashCode();
		result = 31 * result + dataCollectionDefinitionId.hashCode();
		result = 31 * result + Arrays.hashCode(content);
		return result;
	}
}