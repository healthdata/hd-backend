/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.PeriodExpression;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.util.DateUtils;
import be.wiv_isp.healthdata.common.domain.enumeration.TimeUnit;
import be.wiv_isp.healthdata.common.service.IExceptionService;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.SendStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageType;
import be.wiv_isp.healthdata.orchestration.factory.IAbstractMessageFactory;
import be.wiv_isp.healthdata.orchestration.domain.search.MessageSearch;
import be.wiv_isp.healthdata.orchestration.dto.EncryptionModuleMessage;
import be.wiv_isp.healthdata.orchestration.service.IEncryptionModuleService;
import be.wiv_isp.healthdata.orchestration.service.IAbstractRegistrationWorkflowService;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import be.wiv_isp.healthdata.orchestration.service.IMessagingService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MessagingService implements IMessagingService {

	private static final Logger LOG = LoggerFactory.getLogger(MessagingService.class);

	@Autowired
	private IMessageService messageService;
	@Autowired
	private IAbstractMessageFactory messageFactory;
	@Autowired
	private IEncryptionModuleService encryptionModuleService;
	@Autowired
	private IOrganizationService organizationService;
	@Autowired
	private IAbstractRegistrationWorkflowService workflowService;
	@Autowired
	private IConfigurationService configurationService;
	@Autowired
	private IExceptionService exceptionService;

	@Override
	public void send() {
		LOG.debug("Sending messages");

		final MessageSearch search = new MessageSearch();
		search.setStatuses(MessageStatus.OUTBOX, MessageStatus.UNACKNOWLEDGED, MessageStatus.ERROR_SENDING);
		search.setHasRemainingSendingTrials(true);
		search.setMaxResults(50);

		final OutgoingMessagesReport report = new OutgoingMessagesReport();
		List<Message> messages = messageService.getAll(search);
		while(CollectionUtils.isNotEmpty(messages)) {
			for(final Message message : messages) {
				final MessageStatus status = message.getStatus();
				boolean success = sendMessage(message);
				report.add(status, success);
			}
			LOG.debug("Checking whether there are still messages to send");
			messages = messageService.getAll(search);
		}
		encryptionModuleService.flushAll();
		report.log();
	}

	@Override
	public void receive() {
		LOG.debug("Receiving messages");

		final IncomingMessagesReport report = new IncomingMessagesReport();
		final List<Organization> organizations = organizationService.getAll();
		for (Organization organization : organizations) {
			try {
				receive(organization, report);
			} catch (Exception e) {
				exceptionService.report(MessageFormat.format("Error while receiving messages for organization {0}", organization), e);
			}
		}
		report.log();
	}

	@Override
	public void checkUnacknowledgedMessages() {
		LOG.debug("Checking for unacknowledged messages");

		final String configurationValue = configurationService.get(ConfigurationKey.ACKNOWLEDGMENT_TIME_LIMIT).getValue();
		final Date acknowledgmentExpiredDate = DateUtils.remove(new Date(), new PeriodExpression(configurationValue, TimeUnit.SECOND));

		final MessageSearch search = new MessageSearch();
		search.setStatuses(MessageStatus.SENT);
		search.setSentBefore(acknowledgmentExpiredDate);

		final List<Message> messages = messageService.getAll(search);
		for(final Message message : messages) {
			message.setStatus(MessageStatus.UNACKNOWLEDGED);
			messageService.update(message);
			workflowService.updateSendStatus(message.getWorkflowId(),  SendStatus.NOK);
		}
	}

	private void receive(Organization organization, IncomingMessagesReport report) {
		List<EncryptionModuleMessage> messages = encryptionModuleService.receive(organization);
		while(CollectionUtils.isNotEmpty(messages)) {
			for(final EncryptionModuleMessage message : messages) {
				try {
					processMessage(organization, report, message);
				} catch (Exception e) {
					LOG.error("Error while processing message", e);
				}
			}

			LOG.debug("Checking whether there are still incoming messages");
			messages = encryptionModuleService.receive(organization);
		}
		encryptionModuleService.flushAll(); // send acknowledgements
	}

	private boolean sendMessage(Message message) {
		try {
			LOG.debug("Sending message with id {}. Status: {}", message.getId(), message.getStatus());
			final EncryptionModuleMessage encryptionModuleMessage = messageFactory.createEncryptionModuleMessage(message);
			encryptionModuleService.queueOutgoing(encryptionModuleMessage, message.getOrganization());
			updateMessageAfterSending(message, MessageStatus.SENT);
			return true;
		} catch (Exception e) {
			exceptionService.report(MessageFormat.format("Error while sending message with id {0}. Setting status to {1}", message.getId(), MessageStatus.ERROR_SENDING), e);
			updateMessageAfterSending(message, MessageStatus.ERROR_SENDING);
			if(message.getWorkflowId() != null) {
				workflowService.updateSendStatus(message.getWorkflowId(), SendStatus.NOK);
			}
			return false;
		}
	}

	private void updateMessageAfterSending(Message message, MessageStatus status) {
		message.setStatus(status);
		message.setSentOn(new Timestamp(new Date().getTime()));

		final int remainingSendingTrials = message.getRemainingSendingTrials();
		if(remainingSendingTrials > 0) {
			message.setRemainingSendingTrials(remainingSendingTrials - 1);
		}

		messageService.update(message);
	}

	private void processMessage(Organization organization, IncomingMessagesReport report, EncryptionModuleMessage encryptionModuleMessage) {
		final Message message = messageFactory.createIncomingMessage(encryptionModuleMessage, organization);

		final Long linkedId = message.getLinkedId();

		if(MessageType.ACKNOWLEDGMENT.equals(message.getType())) {
			LOG.debug("Message[{}]: saving message to the database with status [{}]", message.getMetadata().get(Message.Metadata.MESSAGE_ID), message.getStatus());
			messageService.create(message);
			report.addAcknowledgment();
		} else {
			final MessageSearch search = new MessageSearch();
			search.setLinkedId(linkedId);
			search.setIdentificationType(message.getCorrespondent().getIdentificationType());
			search.setIdentificationValue(message.getCorrespondent().getIdentificationValue());

			final Message existingMessage = messageService.getUnique(search);
			if (existingMessage == null) {
				LOG.debug("Message[{}]: saving message to the database with status [{}]", message.getMetadata().get(Message.Metadata.MESSAGE_ID), message.getStatus());
				messageService.create(message);
				report.addNew();
			} else {
				LOG.warn("Message[{}]: message already exists in the database. This message is a duplicate and will hence be ignored.", message.getMetadata().get(Message.Metadata.MESSAGE_ID));
				report.addDuplicate();
			}

			if (message.getType().requiresAcknowledgment()) {
				LOG.debug("Sending acknowledgment message for message with linked id {}", linkedId);
				final EncryptionModuleMessage acknowledgment = messageFactory.createAcknowledgment(message);
				encryptionModuleService.queueOutgoing(acknowledgment, organization);
			}
		}
	}

	private class IncomingMessagesReport {

		private int newMessages = 0;
		private int duplicates = 0;
		private int acknowledgments = 0;

		public void addNew() {
			++newMessages;
		}
		public void addDuplicate() {
			++duplicates;
		}
		public void addAcknowledgment() {
			++acknowledgments;
		}

		public void log() {
			if (newMessages == 0 && duplicates == 0 && acknowledgments == 0) {
				LOG.trace("No incoming message received");
			} else {
				if(newMessages > 0 || acknowledgments > 0) {
					LOG.info("{} message(s) received", newMessages + acknowledgments);
				}
				LOG.debug("{} new, {} duplicate, {} acknowledgment messages received", newMessages, duplicates, acknowledgments);
			}
		}
	}

	private class OutgoingMessagesReport {

		private Map<MessageStatus, Integer> successes = new HashMap<>();
		private Map<MessageStatus, Integer> failures = new HashMap<>();

		public void add(MessageStatus status, boolean success) {
			if(success) {
				add(successes, status);
			} else {
				add(failures, status);
			}
		}

		private void add(Map<MessageStatus, Integer> map, MessageStatus status) {
			if(!map.containsKey(status)) {
				map.put(status, 0);
			}
			map.put(status, map.get(status)+1);
		}

		public void log() {
			if (successes.isEmpty()) {
				LOG.trace("No messages sent");
			} else {
				int total = 0;
				for (Integer value : successes.values()) {
					total += value;
				}
				LOG.info("{} message(s) sent", total);
				LOG.debug("Sent messages: {}", successes);
			}

			if (failures.isEmpty()) {
				LOG.trace("No sending failures");
			} else {
				int total = 0;
				for (Integer value : failures.values()) {
					total += value;
				}
				LOG.warn("{} sending failure(s)", total);
				LOG.debug("Sending failed messages: {}", failures);
			}
		}
	}
}
