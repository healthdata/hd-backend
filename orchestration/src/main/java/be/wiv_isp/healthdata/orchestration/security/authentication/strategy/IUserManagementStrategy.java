/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.security.authentication.strategy;

import be.wiv_isp.healthdata.common.domain.search.QueryResult;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.UserManagementType;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;

import java.util.Map;
import java.util.Set;

public interface IUserManagementStrategy {

	QueryResult<User> getAll(UserSearch search);

	User create(User user, Organization organization);

	User read(Long id);

	User read(Organization organization);

	User update(Long id, User user);

	User updatePassword(Long id, String oldPassword, String newPassword);

	void sendPasswordResetInstructions(Organization organization, String email);

	void resetPassword(Organization organization, String token, String newPassword);

	User updateMetadata(Long id, Map<String, String> metaData);

	void delete(Long id);

	boolean supports(UserManagementType userManagementType);

	Set<String> getEmailAdmins(Organization organization);

}
