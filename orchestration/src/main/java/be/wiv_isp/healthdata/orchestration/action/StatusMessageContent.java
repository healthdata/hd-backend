/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.action;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.json.serializer.JsonObjectSerializer;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.nio.charset.StandardCharsets;

@JsonSerialize(include = JsonSerialize.Inclusion.ALWAYS, using = JsonObjectSerializer.class)
public class StatusMessageContent extends JSONObject {

    public static final String IS_MAIN_ORGANIZATION = "is_main_organization";
    public static final String MAIN_ORGANIZATION = "main_organization";
    @Deprecated
    public static final String CONFIGURATIONS = "configurations";
    @Deprecated
    public static final String VERSIONS = "versions";
    @Deprecated
    public static final String STATUSES = "statuses";
    @Deprecated
    public static final String REGISTRATIONS = "registrations";
    public static final String HD_IDENTIFICATION_NAME = "hd_identification_name";
    public static final String HD_IDENTIFICATION_TYPE = "hd_identification_type";
    public static final String HD_IDENTIFICATION_VALUE = "hd_identification_value";
    public static final String CREATED_ON = "created_on";
    public static final String VALIDATION = "validation";
    public static final String LAST_HD4RES_STATUS_MESSAGE_RECEIVED = "last_hd4res_status_message_received";
    @Deprecated
    public static final String MESSAGES = "messages";

    public static final String STATUS = "status";
    public static final String ERROR = "error";
    public static final String OK = "OK";
    public static final String NOK = "NOK";

    public StatusMessageContent() {
        super();
    }

    private StatusMessageContent(String s) throws JSONException {
        super(s);
    }

    public static StatusMessageContent create(String s) {
        try {
            return new StatusMessageContent(s);
        } catch (JSONException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.JSON_PARSE_EXCEPTION, e.getMessage());
            throw exception;
        }
    }

    public static StatusMessageContent create(byte[] b) {
        return create(new String(b, StandardCharsets.UTF_8));
    }

    public void set(String key, Object value) {
        try {
            this.put(key, value);
        } catch (JSONException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.ERROR_WHILE_CREATING_STATUS_MESSAGE_ACTION, e.getMessage());
            throw exception;
        }
    }

    public void setValidation(String key, String status) throws JSONException {
        this.getValidationObject(key).put(StatusMessageContent.STATUS, status);
    }

    public void addValidationError(String key, String message) throws JSONException {
        this.setValidation(key, StatusMessageContent.NOK);

        final JSONObject validationObject = this.getValidationObject(key);

        if(validationObject.isNull(StatusMessageContent.ERROR)) {
            validationObject.put(StatusMessageContent.ERROR, new JSONArray());
        }

        validationObject.getJSONArray(StatusMessageContent.ERROR).put(message);
    }

    private JSONObject getValidationObject(String key) throws JSONException {
        if(this.isNull(StatusMessageContent.VALIDATION)) {
            this.put(StatusMessageContent.VALIDATION, new JSONObject());
        }

        if(this.getJSONObject(StatusMessageContent.VALIDATION).isNull(key)) {
            this.getJSONObject(StatusMessageContent.VALIDATION).put(key, new JSONObject());
        }

        return this.getJSONObject(StatusMessageContent.VALIDATION).getJSONObject(key);
    }
}
