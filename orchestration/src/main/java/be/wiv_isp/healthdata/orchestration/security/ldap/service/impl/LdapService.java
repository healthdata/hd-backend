/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.security.ldap.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.LdapUser;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.security.ldap.ILdapConfiguration;
import be.wiv_isp.healthdata.orchestration.security.ldap.domain.LdapNode;
import be.wiv_isp.healthdata.orchestration.security.ldap.service.ILdapSearchService;
import be.wiv_isp.healthdata.orchestration.security.ldap.service.ILdapService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.naming.directory.SearchControls;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class LdapService implements ILdapService {

	private static final Logger LOG = LoggerFactory.getLogger(LdapService.class);

	@Autowired
	private ILdapConfiguration ldapConfiguration;

	@Autowired
	private ILdapSearchService ldapSearchService;

	@Override
	public boolean fillUserInfo(User user) {
		LdapUser ldapUser = null;
		boolean modified = false;

		if(user.getLastName() == null) {
			ldapUser = getLdapUser(user.getUsername(), user.getOrganization());
			if(!Objects.equals(user.getLastName(), ldapUser.getLastname())) {
				user.setLastName(ldapUser.getLastname());
				modified = true;
			}
		}
		if(user.getFirstName() == null) {
			if(ldapUser == null) {
				ldapUser = getLdapUser(user.getUsername(), user.getOrganization());
			}
			if(!Objects.equals(user.getFirstName(), ldapUser.getFirstname())) {
				user.setFirstName(ldapUser.getFirstname());
				modified = true;
			}
		}
		if(user.getEmail() == null) {
			if(ldapUser == null) {
				ldapUser = getLdapUser(user.getUsername(), user.getOrganization());
			}
			if(!Objects.equals(user.getEmail(), ldapUser.getEmail())) {
				user.setEmail(ldapUser.getEmail());
				modified = true;
			}
		}
		return modified;
	}

	@Override
	public boolean isValidUserName(String userName, Organization organization) {
		final String searchBase = ldapConfiguration.getUserSearchBase(organization);
		final String searchFilter = ldapConfiguration.getUserSearchFilter(organization);
		final LdapNode ldapNode = ldapSearchService.uniqueSearch(searchBase, searchFilter, new Object[]{userName}, false);
		return ldapNode != null;
	}

	@Override
	public List<LdapUser> getUsers(String searchTerm, Organization organization, int countLimit) {
		LOG.debug(MessageFormat.format("Searching users in LDAP with search term [{0}]", searchTerm));

		final SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		searchControls.setCountLimit(countLimit);

		final String searchBase = ldapConfiguration.getUserSearchBase(organization);
		final String searchFilter = ldapConfiguration.getUserSearchFilterWithWildCards(organization);
		final List<LdapNode> nodes = ldapSearchService.search(searchBase, searchFilter, new String[] { searchTerm }, searchControls);
		LOG.debug(MessageFormat.format("{0} results returned", nodes.size()));

		final List<LdapUser> ldapUsers = new ArrayList<>();
		for (final LdapNode node : nodes) {
			ldapUsers.add(createLdapUser(node, organization));
		}
		return ldapUsers;
	}

	@Override
	public LdapUser getUser(String username, Organization organization) {
		LOG.debug(MessageFormat.format("Searching user in LDAP with username [{0}]", username));

		final SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		final String searchBase = ldapConfiguration.getUserSearchBase(organization);
		final String searchFilter = ldapConfiguration.getUserSearchFilter(organization);
		final LdapNode node = ldapSearchService.uniqueSearch(searchBase, searchFilter, new String[] { username }, false, searchControls);

		if(node == null) {
			return null;
		}
		return createLdapUser(node, organization);
	}

	private LdapUser getLdapUser(String userName, Organization organization) {
		try {
			final String searchBase = ldapConfiguration.getUserSearchBase(organization);
			final String searchFilter = ldapConfiguration.getUserSearchFilter(organization);
			final LdapNode node = ldapSearchService.uniqueSearch(searchBase, searchFilter, new Object[]{userName}, true);

			return createLdapUser(node, organization);
		} catch (Exception e) {
			LOG.warn(MessageFormat.format("Could not retrieve user {0} from the ldap server", userName), e);
			return new LdapUser();
		}
	}

	private LdapUser createLdapUser(LdapNode node, Organization organization) {
		final LdapUser user = new LdapUser();
		user.setUsername(node.getAttribute(ldapConfiguration.getUsernameAttribute(organization), true));
		user.setLastname(node.getAttribute(ldapConfiguration.getLastNameAttribute(organization)));
		user.setFirstname(node.getAttribute(ldapConfiguration.getFirstNameAttribute(organization)));
		user.setEmail(node.getAttribute(ldapConfiguration.getMailAttribute(organization)));
		return user;
	}
}
