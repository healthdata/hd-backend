/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.search;

import be.wiv_isp.healthdata.common.domain.search.EntitySearch;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;

import java.text.MessageFormat;
import java.util.Date;

public class FollowUpSearch extends EntitySearch {

    private String name;
    private Boolean active;
    private Date activationDateBefore;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean isActive() {
        return active;
    }

    public Date getActivationDateBefore() {
        return activationDateBefore;
    }

    public void setActivationDateBefore(Date activationDateBefore) {
        this.activationDateBefore = activationDateBefore;
    }

    @Override
    public String toString() {
        return MessageFormat.format("[name = {0}, active = {1}, activationDateBefore = {2}, ]", name, active, DateFormat.DATE.format(activationDateBefore));
    }

}

