/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.common.domain.enumeration.TemplateKey;
import be.wiv_isp.healthdata.orchestration.action.dto.UserRequestHd4resActionDto;
import be.wiv_isp.healthdata.orchestration.api.uri.UserRequestUrl;
import be.wiv_isp.healthdata.orchestration.dto.MailDto;
import be.wiv_isp.healthdata.orchestration.domain.mail.UserRequestContext;
import be.wiv_isp.healthdata.orchestration.service.IMailService;
import be.wiv_isp.healthdata.orchestration.service.IUserRequestMailService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserRequestMailService implements IUserRequestMailService {

    @Autowired
    private IMailService mailService;

    @Autowired
    private IConfigurationService configurationService;

    @Override
    public void sendMails(final UserRequestHd4resActionDto dto) {
        final List<MailDto> mails = new ArrayList<>();
        switch (dto.getAction()) {
            case NEW:
                mails.add(getNewForUserMail(dto));
                if(hasRequester(dto)) {
                    mails.add(getNewForRequesterMail(dto));
                }
                mails.add(getNewForAdminMail(dto));
                break;
            case UPDATE:
                mails.add(getUpdateForUserMail(dto));
                if(hasRequester(dto)) {
                    mails.add(getUpdateForRequesterMail(dto));
                }
                break;

            case ACCEPT:
                mails.add(getAcceptForUserMail(dto));
                if(hasRequester(dto)) {
                    mails.add(getAcceptForRequesterMail(dto));
                }
                break;

            case REJECT:
                mails.add(getRejectForUserMail(dto));
                if(hasRequester(dto)) {
                    mails.add(getRejectForRequesterMail(dto));
                }
                break;
        }
        for (MailDto mail : mails) {
            mailService.send(mail);
        }
    }

    private boolean hasRequester(final UserRequestHd4resActionDto dto) {
        return StringUtils.isNotBlank(dto.getEmailRequester());
    }

    private MailDto getNewForUserMail(final UserRequestHd4resActionDto dto) {
        return mailService.createMail(TemplateKey.USER_REQUEST_NEW_FOR_USER, getContext(dto), dto.getEmail());
    }

    private MailDto getNewForRequesterMail(final UserRequestHd4resActionDto dto) {
        return mailService.createMail(TemplateKey.USER_REQUEST_NEW_FOR_REQUESTER, getContext(dto), dto.getEmailRequester());
    }

    private MailDto getNewForAdminMail(final UserRequestHd4resActionDto dto) {
        final Set<String> to = new HashSet<>();
        if(CollectionUtils.isNotEmpty(dto.getEmailAdmins())) {
            to.addAll(dto.getEmailAdmins());
        } else {
            final Configuration configuration = configurationService.get(ConfigurationKey.HEALTHDATA_EMAIL);
            to.add(configuration.getValue());
        }
        return mailService.createMail(TemplateKey.USER_REQUEST_NEW_FOR_ADMIN, getContext(dto), to);
    }

    private MailDto getUpdateForUserMail(final UserRequestHd4resActionDto dto) {
        return mailService.createMail(TemplateKey.USER_REQUEST_UPDATE_FOR_USER, getContext(dto), dto.getEmail());
    }

    private MailDto getUpdateForRequesterMail(final UserRequestHd4resActionDto dto) {
        return mailService.createMail(TemplateKey.USER_REQUEST_UPDATE_FOR_REQUESTER, getContext(dto), dto.getEmailRequester());
    }

    private MailDto getAcceptForUserMail(final UserRequestHd4resActionDto dto) {
        if(StringUtils.isNotBlank(dto.getDefaultPassword())) {
            return mailService.createMail(TemplateKey.USER_REQUEST_ACCEPT_FOR_USER_DB, getContext(dto), dto.getEmail());
        } else {
            Configuration configuration = configurationService.get(ConfigurationKey.HEALTHDATA_EMAIL);
            return mailService.createMail(TemplateKey.USER_REQUEST_ACCEPT_FOR_USER_LDAP, getContext(dto), dto.getEmail(), configuration.getValue());
        }
    }

    private MailDto getAcceptForRequesterMail(final UserRequestHd4resActionDto dto) {
        return mailService.createMail(TemplateKey.USER_REQUEST_ACCEPT_FOR_REQUESTER, getContext(dto), dto.getEmailRequester());
    }

    private MailDto getRejectForUserMail(final UserRequestHd4resActionDto dto) {
        return mailService.createMail(TemplateKey.USER_REQUEST_REJECT_FOR_USER, getContext(dto), dto.getEmail());
    }

    private MailDto getRejectForRequesterMail(final UserRequestHd4resActionDto dto) {
        return mailService.createMail(TemplateKey.USER_REQUEST_REJECT_FOR_REQUESTER, getContext(dto), dto.getEmailRequester());
    }

    private UserRequestContext getContext(final UserRequestHd4resActionDto dto) {
        final UserRequestUrl url = new UserRequestUrl();
        url.setHost(dto.getHd4dpUrl());
        url.setId(dto.getHd4dpId());

        final UserRequestContext.UserRequest req = new UserRequestContext.UserRequest();
        req.setUrl(url.toString());
        req.setLastName(dto.getLastName());
        req.setFirstName(dto.getFirstName());
        req.setDataCollections(dto.getDataCollectionNames());
        req.setUsername(dto.getUsername());
        req.setPassword(dto.getDefaultPassword());

        final UserRequestContext context = new UserRequestContext();
        context.setInstallationUrl(dto.getHd4dpUrl());
        context.setUserRequest(req);
        return context;
    }
}
