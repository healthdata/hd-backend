/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.mapping;

import be.wiv_isp.healthdata.orchestration.domain.TypedValue;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.orchestration.domain.Attachment;
import be.wiv_isp.healthdata.orchestration.domain.Progress;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MappingResponse {

	protected JSONObject documentContent;
	protected Map<String, TypedValue> codedData;
	protected JSONObject privateData;
	protected Map<String, String> patientID; // aka metaData for <v1.8.0
	protected Map<String, TypedValue> toBeCoded;
	protected String csv;
	protected String stableCsv;
	protected JSONObject computedExpressions;
	protected JSONObject validation;
	protected JSONObject esDocument;
	protected JSONObject esMapping;
	protected String uniqueID;
	protected Progress progress;
	protected List<Attachment> pendingAttachments;
	protected byte[] pdfTemplate;
	protected JSONArray notes;
	protected Map<String, JSONObject> affectedFields;

	public Progress getProgress() {
		return progress;
	}

	public void setProgress(Progress progress) {
		this.progress = progress;
	}

	public JSONObject getDocumentContent() {
		return documentContent;
	}

	public void setDocumentContent(JSONObject documentContent) {
		this.documentContent = documentContent;
	}

	public String getCsv() {
		return csv;
	}

	public void setCsv(String csv) {
		this.csv = csv;
	}

	public String getStableCsv() {
		return stableCsv;
	}

	public void setStableCsv(String stableCsv) {
		this.stableCsv = stableCsv;
	}

	public JSONObject getComputedExpressions() {
		return computedExpressions;
	}

	public void setComputedExpressions(JSONObject computedExpressions) {
		this.computedExpressions = computedExpressions;
	}

	public JSONObject getValidation() {
		return validation;
	}

	public void setValidation(JSONObject validation) {
		this.validation = validation;
	}

	public JSONObject getEsDocument() {
		return esDocument;
	}

	public void setEsDocument(JSONObject esDocument) {
		this.esDocument = esDocument;
	}

	public JSONObject getEsMapping() {
		return esMapping;
	}

	public void setEsMapping(JSONObject esMapping) {
		this.esMapping = esMapping;
	}

	public String getUniqueID() {
		return uniqueID;
	}

	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}

	public List<Attachment> getPendingAttachments() {
		return pendingAttachments;
	}

	public void setPendingAttachments(List<Attachment> pendingAttachments) {
		this.pendingAttachments = pendingAttachments;
	}

	public Map<String, String> getPatientID() {
		return patientID;
	}

	public void setPatientID(Map<String, String> patientID) {
		this.patientID = patientID;
	}

	@Deprecated // alias
	public Map<String, String> getMetaData() {
		return patientID;
	}

	@Deprecated // alias
	public void setMetaData(Map<String, String> metaData) {
		this.patientID = metaData;
	}

	public Map<String, TypedValue> getCodedData() {
		return codedData;
	}

	public void setCodedData(Map<String, TypedValue> codedData) {
		this.codedData = codedData;
	}

	public JSONObject getPrivateData() {
		return privateData;
	}

	public void setPrivateData(JSONObject privateDate) {
		this.privateData = privateDate;
	}

	public Map<String, TypedValue> getToBeCoded() {
		return toBeCoded;
	}

	public void setToBeCoded(Map<String, TypedValue> toBeCoded) {
		this.toBeCoded = toBeCoded;
	}

	public byte[] getPdfTemplate() {
		return pdfTemplate;
	}

	public void setPdfTemplate(byte[] pdfTemplate) {
		this.pdfTemplate = pdfTemplate;
	}

	public JSONArray getNotes() {
		return notes;
	}
	
	public void setNotes(JSONArray notes) {
		this.notes = notes;
	}

	public Map<String, JSONObject> getAffectedFields() {
		return affectedFields;
	}

	public void setAffectedFields(Map<String, JSONObject> affectedFields) {
		this.affectedFields = affectedFields;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof MappingResponse) {
			MappingResponse other = (MappingResponse) o;

			return JsonUtils.equals(documentContent, other.documentContent)
					&& Objects.equals(patientID, other.patientID)
					&& Objects.equals(codedData, other.codedData)
					&& Objects.equals(privateData, other.privateData)
					&& Objects.equals(progress, other.progress)
					&& JsonUtils.equals(esMapping, other.esMapping)
					&& JsonUtils.equals(esDocument, other.esDocument)
					&& JsonUtils.equals(computedExpressions, other.computedExpressions)
					&& JsonUtils.equals(validation, other.validation)
					&& Objects.equals(pendingAttachments, other.pendingAttachments)
					&& Objects.equals(pdfTemplate, other.pdfTemplate)
					&& Objects.equals(uniqueID, other.uniqueID)
					&& Objects.equals(notes, other.notes)
					&& Objects.equals(affectedFields, other.affectedFields);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				this.documentContent,
				this.esMapping,
				this.esDocument,
				this.computedExpressions,
				this.uniqueID,
				this.patientID,
				this.codedData,
				this.privateData,
				this.pendingAttachments,
				this.pdfTemplate,
				this.progress,
				this.notes,
				this.affectedFields);
	}

	@Override
	public String toString() {
		return "MappingResponse {" +
				"documentContent = " + Objects.toString(this.documentContent) + ", " +
				"patientID = " + Objects.toString(this.patientID) + ", " +
				"codedData = " + Objects.toString(this.codedData) + ", " +
				"privateData = " + Objects.toString(this.privateData) + ", " +
				"esMapping = " + Objects.toString(this.esMapping) + ", " +
				"esDocument = " + Objects.toString(this.esDocument) + ", " +
				"computedExpressions = " + Objects.toString(this.computedExpressions) + ", " +
				"uniqueID = " + Objects.toString(this.uniqueID) + ", " +
				"pendingAttachments = " + Objects.toString(this.pendingAttachments) + ", " +
				"pdfTemplate = " + Objects.toString(this.pdfTemplate) + ", " +
				"progress = " + Objects.toString(this.progress) + ", " +
				"notes = " + Objects.toString(this.notes) + ", " +
				"affectedFields = " + Objects.toString(this.affectedFields) + "}";
	}
}