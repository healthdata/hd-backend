/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.domain.search;

import be.wiv_isp.healthdata.common.domain.search.EntitySearch;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;

import java.util.Objects;

public class ConfigurationSearch extends EntitySearch {

	private ConfigurationKey key;
	private Organization organization;
	private Boolean organizationCanBeNull = true;
	private Boolean replaceIfUseFromMain;
	private Boolean includeInvisible;

	public ConfigurationKey getKey() {
		return key;
	}

	public void setKey(ConfigurationKey key) {
		this.key = key;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Boolean getReplaceIfUseFromMain() {
		return replaceIfUseFromMain;
	}

	public void setReplaceIfUseFromMain(Boolean replaceIfUseFromMain) {
		this.replaceIfUseFromMain = replaceIfUseFromMain;
	}

	public Boolean getOrganizationCanBeNull() {
		return organizationCanBeNull;
	}

	public void setOrganizationCanBeNull(Boolean organizationCanBeNull) {
		this.organizationCanBeNull = organizationCanBeNull;
	}

	public Boolean getIncludeInvisible() {
		return includeInvisible;
	}

	public void setIncludeInvisible(Boolean includeInvisible) {
		this.includeInvisible = includeInvisible;
	}

	@Override
	public String toString() {
		return "ConfigurationSearch {" +
				"key = " + Objects.toString(this.key) + ", " +
				"organization = " + Objects.toString(this.organization) + ", " +
				"replaceIfUseFromMain = " + Objects.toString(this.replaceIfUseFromMain) + ", " +
				"organizationCanBeNull = " + Objects.toString(this.organizationCanBeNull) + "}";
	}

	@Override
	public boolean equals(Object other) {
		if(this == other) {
			return true;
		}

		if(other instanceof ConfigurationSearch) {
			ConfigurationSearch o = (ConfigurationSearch) other;

			return Objects.equals(this.key, o.key)
					&& Objects.equals(this.organization, o.organization)
					&& Objects.equals(this.replaceIfUseFromMain, o.replaceIfUseFromMain)
					&& Objects.equals(this.organizationCanBeNull, o.organizationCanBeNull);
		}

		return false;
	}
}
