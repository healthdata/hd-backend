/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 *  In a previous version, each bean was instantiated twice due to
 *  an incorrect Spring configuration. This has been corrected.
 *  However, to ensure that it does not happen anymore, this service
 *  checks that it does not get instantiated (initialized) twice.
 *  In that case, it throws an exception, preventing the application
 *  to start.
 */
@Service
public class DummyService {

    private static boolean INITIALIZED = false;

    @PostConstruct
    public void init() throws Exception {
        if (INITIALIZED) {
            throw new Exception("Bean already instantiated");
        }
        INITIALIZED = true;
    }
}
