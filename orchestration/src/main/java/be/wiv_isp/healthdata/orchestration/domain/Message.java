/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import be.wiv_isp.healthdata.orchestration.util.constant.EHealthCodageConstants;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageType;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "MESSAGES")
public class Message {

	public static final Integer SENDING_TRIALS = 5;

	public class Metadata {
		public static final String FILE_NAME = EHealthCodageConstants.MetaData.FILE_NAME;
		public static final String TTP_PROJECT = EHealthCodageConstants.MetaData.TTP_PROJECT;
		public static final String MESSAGE_ID = "MESSAGE_ID";
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "MESSAGE_ID")
	private Long id;

	@Column(name = "LINKED_ID")
	private Long linkedId;

	@Lob
	@Column(name = "CONTENT", nullable = false)
	private byte[] content;

	@Column(name = "WORKFLOW_ID")
	private Long workflowId;

	@Column(name = "STATUS", nullable = true)
	@Enumerated(EnumType.STRING)
	private MessageStatus status;

	@Column(name = "TYPE", nullable = true)
	@Enumerated(EnumType.STRING)
	private MessageType type;

	@Fetch(FetchMode.JOIN)
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "MESSAGE_METADATA", joinColumns = @JoinColumn(name = "MESSAGE_ID"))
	@MapKeyColumn(name = "METADATA_KEY")
	@Column(name = "METADATA_VALUE")
	private Map<String, String> metadata;

	@Column(name = "SENT_ON", nullable = true)
	private Timestamp sentOn;

	@Column(name = "CREATED_ON", nullable = false)
	private Timestamp createdOn;

	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "ORGANIZATION_ID", nullable = false)
	private Organization organization;

	@Embedded
	private MessageCorrespondent correspondent;

	@Column(name = "REMAINING_SENDING_TRIALS", nullable = true)
	private Integer remainingSendingTrials;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getLinkedId() {
		return linkedId;
	}

	public void setLinkedId(Long linkedId) {
		this.linkedId = linkedId;
	}

	public Long getWorkflowId() {
		return workflowId;
	}

	public void setWorkflowId(Long workflowId) {
		this.workflowId = workflowId;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public MessageStatus getStatus() {
		return status;
	}

	public void setStatus(MessageStatus status) {
		this.status = status;
	}

	public MessageType getType() {
		return type;
	}

	public void setType(MessageType type) {
		this.type = type;
	}

	public Map<String, String> getMetadata() {
		return metadata;
	}

	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}

	@JsonSerialize(using = TimestampSerializer.class)
	public Timestamp getSentOn() {
		return sentOn;
	}

	@JsonDeserialize(using = TimestampDeserializer.class)
	public void setSentOn(Timestamp sentOn) {
		this.sentOn = sentOn;
	}

	@JsonSerialize(using = TimestampSerializer.class)
	public Timestamp getCreatedOn() {
		return createdOn;
	}

	@JsonDeserialize(using = TimestampDeserializer.class)
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public MessageCorrespondent getCorrespondent() {
		return correspondent;
	}

	public void setCorrespondent(MessageCorrespondent correspondent) {
		this.correspondent = correspondent;
	}

	@PrePersist
	public void onCreate() {
		setCreatedOn(new Timestamp(new Date().getTime()));
	}

	public Integer getRemainingSendingTrials() {
		return remainingSendingTrials;
	}

	public void setRemainingSendingTrials(Integer remainingSendingTrials) {
		this.remainingSendingTrials = remainingSendingTrials;
	}

	public void addMetadata(String key, String value) {
		if(metadata == null) {
			metadata = new HashMap<>();
		}
		metadata.put(key, value);
	}

	@Override
	public String toString() {
		return new StringBuilder("[ ")
				.append("id: ").append(id).append(", ")
				.append("type: ").append(type).append(", ")
				.append("status: ").append(status).append(", ")
				.append("correspondent: ").append(correspondent).append(", ")
				.append("linkedId: ").append(linkedId).append(", ")
				.append("workflowId: ").append(workflowId).append(", ")
				.append("sentOn: ").append(sentOn).append(", ")
				.append("createdOn: ").append(createdOn).append(", ")
				.append("organization: ").append(organization).append(", ")
				.append("remainingSendingTrials: ").append(remainingSendingTrials).append(" ]")
				.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Message)) return false;

		Message message = (Message) o;

		if (id != null ? !id.equals(message.id) : message.id != null) return false;
		if (linkedId != null ? !linkedId.equals(message.linkedId) : message.linkedId != null) return false;
		if (!Arrays.equals(content, message.content)) return false;
		if (workflowId != null ? !workflowId.equals(message.workflowId) : message.workflowId != null) return false;
		if (status != message.status) return false;
		if (type != message.type) return false;
		if (metadata != null ? !metadata.equals(message.metadata) : message.metadata != null) return false;
		if (sentOn != null ? !sentOn.equals(message.sentOn) : message.sentOn != null) return false;
		if (createdOn != null ? !createdOn.equals(message.createdOn) : message.createdOn != null) return false;
		if (organization != null ? !organization.equals(message.organization) : message.organization != null)
			return false;
		if (correspondent != null ? !correspondent.equals(message.correspondent) : message.correspondent != null)
			return false;
		return !(remainingSendingTrials != null ? !remainingSendingTrials.equals(message.remainingSendingTrials) : message.remainingSendingTrials != null);

	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (linkedId != null ? linkedId.hashCode() : 0);
		result = 31 * result + (content != null ? Arrays.hashCode(content) : 0);
		result = 31 * result + (workflowId != null ? workflowId.hashCode() : 0);
		result = 31 * result + (status != null ? status.hashCode() : 0);
		result = 31 * result + (type != null ? type.hashCode() : 0);
		result = 31 * result + (metadata != null ? metadata.hashCode() : 0);
		result = 31 * result + (sentOn != null ? sentOn.hashCode() : 0);
		result = 31 * result + (createdOn != null ? createdOn.hashCode() : 0);
		result = 31 * result + (organization != null ? organization.hashCode() : 0);
		result = 31 * result + (correspondent != null ? correspondent.hashCode() : 0);
		result = 31 * result + (remainingSendingTrials != null ? remainingSendingTrials.hashCode() : 0);
		return result;
	}
}
