/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api.mapper;

import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.dto.AbstractRegistrationWorkflowDto;
import be.wiv_isp.healthdata.orchestration.mapping.IMappingContextFactory;
import be.wiv_isp.healthdata.orchestration.service.IAbstractRegistrationWorkflowService;
import org.springframework.beans.factory.annotation.Autowired;

abstract public class AbstractRegistrationWorkflowMapper<Workflow extends AbstractRegistrationWorkflow, Dto extends AbstractRegistrationWorkflowDto> extends AbstractWorkflowMapper<Workflow, Dto> {

	@Autowired
	private IMappingContextFactory mappingContextFactory;

	@Autowired
	private IAbstractRegistrationWorkflowService workflowService;

	@Override
	public Dto convert(Workflow workflow) {
		if (workflow == null) {
			return null;
		}
		Dto converted = getDtoInstance(workflow);
		converted.setContext(mappingContextFactory.create(workflow));
		converted.setAvailableActions(workflowService.getAvailableActions(workflow));
		return converted;
	}
}
