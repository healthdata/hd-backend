/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.action.standalone.StandAloneAction;
import be.wiv_isp.healthdata.orchestration.action.workflow.WorkflowAction;
import be.wiv_isp.healthdata.orchestration.action.workflow.executor.IParticipationWorkflowActionExecutor;
import be.wiv_isp.healthdata.orchestration.action.workflow.executor.IRegistrationWorkflowActionExecutor;
import be.wiv_isp.healthdata.orchestration.domain.AbstractWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.HDServiceUser;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.SendStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageType;
import be.wiv_isp.healthdata.orchestration.domain.search.MessageSearch;
import be.wiv_isp.healthdata.orchestration.factory.IAbstractMessageFactory;
import be.wiv_isp.healthdata.orchestration.factory.IActionFactory;
import be.wiv_isp.healthdata.orchestration.service.IAbstractRegistrationWorkflowService;
import be.wiv_isp.healthdata.orchestration.service.IMessageProcessingService;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.List;

@Service
public class MessageProcessingService implements IMessageProcessingService {

	private static final Logger LOG = LoggerFactory.getLogger(MessageProcessingService.class);

	@Autowired
	private IMessageService messageService;

	@Autowired
	private IActionFactory actionFactory;

	@Autowired
	private IAbstractMessageFactory messageFactory;

	@Autowired
	private IAbstractRegistrationWorkflowService workflowService;

	@Autowired
	private IParticipationWorkflowActionExecutor participationWorkflowActionExecutor;

	@Autowired
	private IRegistrationWorkflowActionExecutor registrationWorkflowActionExecutor;

	@Override
	public void process() {
		LOG.debug("Processing incoming messages");

		final ProcessedMessagesReport report = new ProcessedMessagesReport();

		final MessageSearch search = new MessageSearch();
		search.setStatuses(MessageStatus.INBOX);
		search.setMaxResults(50);

		List<Message> messages = messageService.getAll(search);
		while(CollectionUtils.isNotEmpty(messages)) {
			for (Message message : messages) {
				try {
					process(message);
					report.addSuccess();
				} catch (HealthDataException e) {
					// nothing to do. Logging is performed in process method
					report.addFailure();
				}
			}
			LOG.debug("Checking whether there are still messages to process");
			messages = messageService.getAll(search);
		}

		report.log();
	}

	@Override
	public void process(Message message) {
		String messageId = null;
		try {
			messageId = message.getMetadata().get(Message.Metadata.MESSAGE_ID);
			final MessageType type = message.getType();

			LOG.debug("Message[{}]: start processing message (type: {})", messageId, message.getType());

			switch (type) {
				case ACTION:
					processAction(message);
					break;
				case PARTICIPATION_ACTION:
					processParticipationWorkflowAction(message);
					break;
				case WORKFLOW_ACTION:
					processRegistrationWorkflowAction(message);
					break;
				case ACKNOWLEDGMENT:
					processAcknowledgment(message);
					break;
			}

			if(MessageType.ACKNOWLEDGMENT.equals(type)) {
				LOG.debug("Message[{}]: acknowledgment message, deleting message after processing", messageId);
				messageService.delete(message.getId());
			} else {
				final MessageStatus status = MessageStatus.PROCESSED;
				LOG.debug("Message[{}]: message successfully processed, updating message status to {}", messageId, status);
				message.setStatus(status);
				messageService.update(message);
			}
		} catch (Exception e) {
			final MessageStatus status = MessageStatus.ERROR_PROCESSING;
			LOG.error(MessageFormat.format("Message[{0}]: error while processing message, updating message status to {1}", messageId, status), e);
			message.setStatus(status);
			messageService.update(message);

			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.ERROR_WHILE_PROCESSING_MESSAGE, messageId, e.getMessage());
			throw exception;
		}
	}

	private void processAction(Message message) {
		final StandAloneAction action = (StandAloneAction) actionFactory.create(message);
		action.execute();
	}

	private void processParticipationWorkflowAction(Message message) {
		final WorkflowAction action = (WorkflowAction) actionFactory.create(message);
		final AbstractWorkflow workflow = participationWorkflowActionExecutor.execute(action, HDServiceUser.create(HDServiceUser.Username.MESSAGING));
		message.setWorkflowId(workflow.getId());
	}

	private void processRegistrationWorkflowAction(Message message) {
		final WorkflowAction action = (WorkflowAction) actionFactory.create(message);
		final AbstractWorkflow workflow = registrationWorkflowActionExecutor.execute(action, HDServiceUser.create(HDServiceUser.Username.MESSAGING));
		message.setWorkflowId(workflow.getId());
	}

	private void processAcknowledgment(Message message) {
		final Long messageId = messageFactory.getAcknowledgedMessageId(message);
		LOG.debug("Acknowledgment received for message with id {}. Updating message status to {}", messageId, MessageStatus.ACKNOWLEDGED);

		final Message acknowledgedMessage = messageService.get(messageId);
		acknowledgedMessage.setStatus(MessageStatus.ACKNOWLEDGED);
		messageService.update(acknowledgedMessage);
		workflowService.updateSendStatus(acknowledgedMessage.getWorkflowId(),  SendStatus.OK);
	}

	private class ProcessedMessagesReport {

		private int success = 0;
		private int failures = 0;

		public void addSuccess() {
			++success;
		}
		public void addFailure() {
			++failures;
		}

		public void log() {
			if (success == 0 && failures == 0) {
				LOG.trace("No messages processed");
			} else if(failures == 0) {
				LOG.info("{} message(s) successfully processed", success);
			} else {
				LOG.warn("{} message(s) successfully processed. {} message(s) processed with failure.", success, failures);
			}
		}
	}
}
