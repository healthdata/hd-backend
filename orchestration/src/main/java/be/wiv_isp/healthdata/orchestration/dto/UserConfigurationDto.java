/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dto;

import be.wiv_isp.healthdata.common.util.StringUtils;
import be.wiv_isp.healthdata.orchestration.domain.UserConfiguration;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.UserConfigurationKey;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserConfigurationDto {

	private Long id;
	private UserConfigurationKey key;
	private String value;
	private String defaultValue;

	public UserConfigurationDto() {
	} // needed for jersey to instantiate new object

	public UserConfigurationDto(UserConfiguration configuration) {
		this.id = configuration.getId();
		this.key = configuration.getKey();
		this.value = configuration.getValue();
		this.defaultValue = configuration.getDefaultValue();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserConfigurationKey getKey() {
		return key;
	}

	public void setKey(UserConfigurationKey key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getDescription() {
		return key.getDescription();
	}

	public String getType() {
		return key.getType();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof UserConfigurationDto))
			return false;

		UserConfigurationDto that = (UserConfigurationDto) o;

		if (key != that.key)
			return false;
		if (value != null ? !value.equals(that.value) : that.value != null)
			return false;
		return !(defaultValue != null ? !defaultValue.equals(that.defaultValue) : that.defaultValue != null);

	}

	@Override
	public int hashCode() {
		int result = key != null ? key.hashCode() : 0;
		result = 31 * result + (value != null ? value.hashCode() : 0);
		result = 31 * result + (defaultValue != null ? defaultValue.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return StringUtils.toString(this);
	}
}
