/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api.mapper;

import be.wiv_isp.healthdata.orchestration.domain.AbstractUserRequest;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.util.Iterator;

abstract public class AbstractUserRequestMapper<T extends AbstractUserRequest> {

	public T convert(JSONObject json) throws JSONException {
		T userRequest = getInstance();
		for (Iterator<String> iterator = json.keys(); iterator.hasNext();) {
			String key = iterator.next();
			if (!"null".equalsIgnoreCase(json.getString(key))) {
				if ("id".equalsIgnoreCase(key)) {
						userRequest.setId(json.getLong(key));
				} else if ("firstName".equalsIgnoreCase(key)) {
					userRequest.setFirstName(json.getString(key));
				} else if ("lastName".equalsIgnoreCase(key)) {
					userRequest.setLastName(json.getString(key));
				} else if ("email".equalsIgnoreCase(key)) {
					userRequest.setEmail(json.getString(key));
				} else if ("emailRequester".equalsIgnoreCase(key)) {
					userRequest.setEmailRequester(json.getString(key));
				}
			}
		}
		return userRequest;
	}

	abstract  protected T getInstance();
}
