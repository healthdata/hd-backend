/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dto;

import be.wiv_isp.healthdata.common.util.StringUtils;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class MailDto {

    private Set<String> to = new HashSet<>();

    private String subject;

    private String text;

    public Set<String> getTo() {
        return to;
    }

    public void setTo(Set<String> to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof MailDto) {
            MailDto other = (MailDto) o;

            return Objects.equals(to, other.to)
                    && Objects.equals(subject, other.subject)
                    && Objects.equals(text, other.text);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                this.to,
                this.subject,
                this.text);
    }

    @Override
    public String toString() {
        return StringUtils.toStringExclude(this, "text");
    }
}
