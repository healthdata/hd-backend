/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.orchestration.dao.IViewDao;
import be.wiv_isp.healthdata.orchestration.domain.View;
import be.wiv_isp.healthdata.orchestration.domain.search.ViewSearch;
import be.wiv_isp.healthdata.orchestration.service.IViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ViewService extends AbstractService<View, Long, ViewSearch, IViewDao> implements IViewService {

    @Autowired
    private IViewDao dao;

    public ViewService() {
        super(View.class);
    }

    @Override
    protected IViewDao getDao() {
        return dao;
    }
}
