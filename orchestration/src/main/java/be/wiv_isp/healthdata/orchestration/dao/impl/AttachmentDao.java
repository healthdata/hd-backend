/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.orchestration.dao.IAttachmentDao;
import be.wiv_isp.healthdata.orchestration.domain.Attachment;
import be.wiv_isp.healthdata.orchestration.domain.AttachmentContent;
import be.wiv_isp.healthdata.orchestration.domain.search.AttachmentSearch;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AttachmentDao extends CrudDaoV2<Attachment, Long, AttachmentSearch> implements IAttachmentDao {

    public AttachmentDao() {
        super(Attachment.class);
    }

    @Override
    public List<Attachment> getAll() {
        return getAll(null);
    }

    @Override
    protected List<Predicate> getPredicates(AttachmentSearch search, CriteriaBuilder cb, Root<Attachment> rootEntry) {
        final List<Predicate> predicates = new ArrayList<>();

        if(search.getUuid() != null) {
            predicates.add(cb.equal(rootEntry.get("uuid"), search.getUuid()));
        }

        if(search.getContentId() != null) {
            final Join<Attachment, AttachmentContent> content = rootEntry.join("content");
            predicates.add(cb.equal(content.get("id"), search.getContentId()));
        }

        return predicates;
    }
}
