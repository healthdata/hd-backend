/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.orchestration.annotation.Auditable;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ApiType;
import be.wiv_isp.healthdata.orchestration.service.IPlatformService;
import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.action.workflow.WorkflowAction;
import be.wiv_isp.healthdata.orchestration.action.workflow.executor.IWorkflowActionExecutor;
import be.wiv_isp.healthdata.orchestration.api.mapper.WorkflowIdMapper;
import be.wiv_isp.healthdata.orchestration.domain.AbstractElasticSearchInfo;
import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationDocument;
import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.domain.search.AbstractRegistrationWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.dto.AbstractRegistrationWorkflowDto;
import be.wiv_isp.healthdata.orchestration.service.*;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.codehaus.jettison.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

abstract public class AbstractRegistrationWorkflowRestService<Workflow extends AbstractRegistrationWorkflow, Search extends AbstractRegistrationWorkflowSearch, FullDto extends AbstractRegistrationWorkflowDto, Dto extends AbstractRegistrationWorkflowDto, Document extends AbstractRegistrationDocument> extends AbstractWorkflowRestService<Workflow, Search, FullDto, Dto, Document> {

	@Autowired
	private IElasticSearchService elasticSearchService;
	@Autowired
	private IElasticSearchInfoService<AbstractElasticSearchInfo> elasticSearchInfoService;
	@Autowired
	private IPlatformService platformService;
	@Autowired
	private IDownloadService downloadService;
	@Autowired
	private IDataCollectionDefinitionForwardService dataCollectionDefinitionService;
	@Autowired
	protected IAbstractRegistrationWorkflowService<Workflow, Search> workflowService;


	@POST
	@PreAuthorize("hasRole('ROLE_USER')")
	@Auditable(apiType = ApiType.WORKFLOW)
	@Path("/actions")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response executeActions(final String json) {
		final WorkflowAction<Workflow> action = (WorkflowAction<Workflow>) actionFactory.createAction(json.getBytes(StandardCharsets.UTF_8));

		final HDUserDetails principal = AuthenticationService.getUserDetails();
		final Workflow execute = getWorkflowActionExecutor().execute(action, principal);

		final AbstractElasticSearchInfo.Status elasticSearchStatus = elasticSearchInfoService.get(execute.getId()).getStatus();

		if (elasticSearchStatus != AbstractElasticSearchInfo.Status.INDEXED && !(action.getAction() == WorkflowActionType.DELETE && elasticSearchStatus == AbstractElasticSearchInfo.Status.DELETED)) {
			return Response.status(Response.Status.ACCEPTED).entity(workflowMapper.convert(execute)).build();
		}
		return Response.ok(workflowMapper.convert(execute)).build();
	}

	@DELETE
	@PreAuthorize("hasRole('ROLE_USER')")
	@Auditable(apiType = ApiType.WORKFLOW)
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteAll(String json) {
		WorkflowIdMapper mapper = new WorkflowIdMapper();
		List<Long> workflowIds;
		try {
			workflowIds = mapper.convert(new JSONArray(json));
		} catch (Exception e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.JSON_PARSE_EXCEPTION);
			throw exception;
		}

		if (!Platform.HD4DP.equals(platformService.getCurrentPlatform())) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.ACTION_NOT_ALLOWED_ON_PLATFORM, WorkflowActionType.DELETE, platformService.getCurrentPlatform());
			throw exception;
		}
		final HDUserDetails principal = AuthenticationService.getUserDetails();

		List<Workflow> workflows = new ArrayList<>();
		for (Long workflowId : workflowIds) {
			Workflow workflow = workflowService.get(workflowId);
			if (userDataCollectionService.isUserAuthorized(principal, workflow)) {
				workflows.add(workflow);
			}
		}
		workflowService.delete(workflows, principal);
		return Response.ok().build();
	}

	@POST
	@Path("/search/{dataCollectionDefinitionId}")
	@PreAuthorize("hasRole('ROLE_USER')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response search(@PathParam("dataCollectionDefinitionId") final Long dataCollectionDefinitionId, final String searchQuery) {

		final HDUserDetails principal = AuthenticationService.getUserDetails();

		final DataCollectionDefinitionDtoV7 dcd = dataCollectionDefinitionService.get(dataCollectionDefinitionId);

		if (!userDataCollectionService.isUserAuthorized(principal, dcd.getDataCollectionName())) {
			return Response.status(Response.Status.FORBIDDEN).build();
		}

		final String index = elasticSearchService.buildIndexName(principal.getOrganization(), dcd.getDataCollectionName());
		final String type = dataCollectionDefinitionId.toString();

		final String response = elasticSearchService.search(index, type, searchQuery);

		return Response.ok(response).build();
	}

	@GET
	@Path("/download/{datacollectiondefinitionid}")
	@Auditable(apiType = ApiType.WORKFLOW)
	@PreAuthorize("hasRole('ROLE_USER')")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response download(@Context UriInfo info) {
		Long dcdId = null;
		MultivaluedMap<String, String> pathParameters = info.getPathParameters();
		for (String pathParameterKey : pathParameters.keySet()) {
			if ("datacollectiondefinitionid".equalsIgnoreCase(pathParameterKey)) {
				dcdId = Long.valueOf(pathParameters.getFirst(pathParameterKey));
			}
		}
		if (dcdId == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_PROPERTY, "datacollectiondefinitionid", dcdId, "MANDATORY");
			throw exception;
		}
		boolean stable = false;
		boolean deleted = false;
		MultivaluedMap<String, String> queryParameters = info.getQueryParameters();
		for (String queryParameterKey : queryParameters.keySet()) {
			if ("stable".equalsIgnoreCase(queryParameterKey)) {
				stable = Boolean.valueOf(queryParameters.getFirst(queryParameterKey));
			}
			if ("deleted".equalsIgnoreCase(queryParameterKey)) {
				deleted = Boolean.valueOf(queryParameters.getFirst(queryParameterKey));
			}
		}

		final HDUserDetails principal = AuthenticationService.getUserDetails();

		String csv = downloadService.download(dcdId, principal.getOrganization(), stable, deleted);
		return Response.ok(new ByteArrayInputStream(csv.getBytes(StandardCharsets.UTF_8)), MediaType.APPLICATION_OCTET_STREAM) //
				.header("Content-Disposition", "attachment; filename=\"download.csv\"") //
				.build();
	}

	@Override
	protected WorkflowType getWorkflowType() {
		return WorkflowType.REGISTRATION;
	}

	private boolean containsIgnoreCase(Set<String> dataCollectionNames, String dataCollection) {
		for (String dc : dataCollectionNames) {
			if (dataCollection.equalsIgnoreCase(dc)) {
				return true;
			}
		}
		return false;
	}

	protected abstract FullDto getFullDtoInstance(Dto dto);

	protected abstract IWorkflowActionExecutor<Workflow> getWorkflowActionExecutor();

}
