/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.search;

import be.wiv_isp.healthdata.common.domain.search.EntitySearch;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.Organization;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class UserSearch extends EntitySearch {

	private List<String> usernames;
	private String email;
	private Organization organization;
	private Boolean ldapUser;
	private String dataCollectionName;
	private Authority authority;
	private Authority excludeAuthority;
	private Boolean incompleteUserInfo;

	public List<String> getUsernames() {
		return usernames;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setUsernames(List<String> usernames) {
		this.usernames = usernames;
	}

	public void setUsername(String username) {
		usernames = new ArrayList<>();
		usernames.add(username);
	}

	public void addUsername(String username) {
		if(usernames == null) {
			usernames = new ArrayList<>();
		}
		usernames.add(username);
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Boolean getLdapUser() {
		return ldapUser;
	}

	public void setLdapUser(Boolean ldapUser) {
		this.ldapUser = ldapUser;
	}

	public void setDataCollectionName(String dataCollectionName) {
		this.dataCollectionName = dataCollectionName;
	}

	public String getDataCollectionName() {
		return dataCollectionName;
	}

	public Authority getAuthority() {
		return authority;
	}

	public void setAuthority(Authority authority) {
		this.authority = authority;
	}

	public Authority getExcludeAuthority() {
		return excludeAuthority;
	}

	public void setExcludeAuthority(Authority excludeAuthority) {
		this.excludeAuthority = excludeAuthority;
	}

	public void setIncompleteUserInfo(Boolean incompleteUserInfo) {
		this.incompleteUserInfo = incompleteUserInfo;
	}

	public Boolean getIncompleteUserInfo() {
		return incompleteUserInfo;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof UserSearch) {
			UserSearch other = (UserSearch) o;

			return Objects.equals(usernames, other.usernames) //
					&& Objects.equals(organization, other.organization) //
					&& Objects.equals(ldapUser, other.ldapUser);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.usernames, this.organization, this.ldapUser);
	}

	@Override
	public String toString() {
		return "UserSearch {" + //
				"username = " + this.usernames + ", " + //
				"email = " + this.email + ", " + //
				"organization = " + this.organization + ", " + //
				"ldapUser = " + this.ldapUser + "}";
	}
}
