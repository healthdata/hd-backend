/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.mapper;

import be.wiv_isp.healthdata.orchestration.domain.Attachment;
import be.wiv_isp.healthdata.orchestration.domain.AttachmentContent;
import be.wiv_isp.healthdata.orchestration.dto.AttachmentDto;

import java.util.ArrayList;
import java.util.List;

public class AttachmentMapper {

    public static AttachmentDto mapToAttachmentDto(final Attachment attachment, boolean withContent) {
        final AttachmentDto dto = new AttachmentDto();
        dto.setUuid(attachment.getUuid());
        dto.setFilename(attachment.getFilename());
        dto.setMimeType(attachment.getMimeType());

        final AttachmentContent content = attachment.getContent();
        dto.setHash(content.getHash());
        dto.setSize(content.getSize());

        if(withContent) {
            dto.setZippedContent(content.getZippedContent());
        }

        return dto;
    }

    public static List<AttachmentDto> mapToAttachmentDtos(final List<Attachment> attachments, boolean withContent) {
        if(attachments == null) {
            return new ArrayList<>();
        }

        final List<AttachmentDto> dtos = new ArrayList<>();
        for(final Attachment header : attachments) {
            dtos.add(mapToAttachmentDto(header, withContent));
        }
        return dtos;
    }

    private static Attachment mapToAttachment(final AttachmentDto dto) {
        final Attachment attachment = new Attachment();
        attachment.setUuid(dto.getUuid());
        attachment.setFilename(dto.getFilename());
        attachment.setMimeType(dto.getMimeType());

        final AttachmentContent content = new AttachmentContent();
        content.setSize(dto.getSize());
        content.setHash(dto.getHash());
        content.setZippedContent(dto.getZippedContent());
        attachment.setContent(content);
        return attachment;
    }

    public static List<Attachment> mapToAttachments(final List<AttachmentDto> dtos) {
        if(dtos == null) {
            return new ArrayList<>();
        }

        final List<Attachment> attachments = new ArrayList<>();
        for(final AttachmentDto dto : dtos) {
            attachments.add(mapToAttachment(dto));
        }
        return attachments;
    }
}
