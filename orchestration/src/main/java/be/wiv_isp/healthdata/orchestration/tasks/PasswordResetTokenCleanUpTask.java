/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.tasks;

import be.wiv_isp.healthdata.orchestration.domain.PasswordResetToken;
import be.wiv_isp.healthdata.orchestration.domain.search.PasswordResetTokenSearch;
import be.wiv_isp.healthdata.orchestration.service.IPasswordResetTokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PasswordResetTokenCleanUpTask extends HealthDataTask {

    private static final Logger LOG = LoggerFactory.getLogger(PasswordResetTokenCleanUpTask.class);

    @Autowired
    private IPasswordResetTokenService passwordResetTokenService;

    @Override
    public String getName() {
        return "PASSWORD_RESET_TOKEN_CLEAN_UP";
    }

    @Override
    protected void execute() {
        final PasswordResetTokenSearch search = new PasswordResetTokenSearch();
        search.setExpired(true);
        final List<PasswordResetToken> all = passwordResetTokenService.getAll(search);

        int deleted = 0;
        for (PasswordResetToken token : all) {
            if(token.isExpired()) {
                passwordResetTokenService.delete(token);
                ++deleted;
            }
        }
        if(deleted > 0) {
            LOG.debug("{} tokens deleted", deleted);
        }
    }
}
