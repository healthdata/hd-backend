/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.security.ldap;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.ldap.ppolicy.PasswordPolicyData;
import org.springframework.security.ldap.userdetails.LdapUserDetails;
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;

import java.util.Collection;

public class LdapOrganizationUserDetailsImpl implements LdapUserDetails, PasswordPolicyData, HDUserDetails {

	private final LdapUserDetailsImpl ldapUserDetails;

	private final Organization organization;

	public LdapOrganizationUserDetailsImpl(LdapUserDetailsImpl ldapUserDetails, Organization organization) {
		this.ldapUserDetails = ldapUserDetails;
		this.organization = organization;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return ldapUserDetails.getAuthorities();
	}

	@Override
	public String getPassword() {
		return ldapUserDetails.getPassword();
	}

	@Override
	public String getUsername() {
		return ldapUserDetails.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return ldapUserDetails.isAccountNonExpired();
	}

	@Override
	public boolean isAccountNonLocked() {
		return ldapUserDetails.isAccountNonLocked();
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return ldapUserDetails.isCredentialsNonExpired();
	}

	@Override
	public boolean isEnabled() {
		return ldapUserDetails.isEnabled();
	}

	@Override
	public Organization getOrganization() {
		return organization;
	}

	@Override
	public int getTimeBeforeExpiration() {
		return ldapUserDetails.getTimeBeforeExpiration();
	}

	@Override
	public int getGraceLoginsRemaining() {
		return ldapUserDetails.getGraceLoginsRemaining();
	}

	@Override
	public String getDn() {
		return ldapUserDetails.getDn();
	}

	@Override
	public boolean isAdmin() {
		return hasAuthority(Authority.ADMIN);
	}

	@Override
	public boolean isUser() {
		return hasAuthority(Authority.USER);
	}

	@Override
	public boolean isSupport() {
		return hasAuthority(Authority.SUPPORT);
	}

	@Override
	public boolean isHd4prc() {
		return hasAuthority(Authority.HD4PRC);
	}

	@Override
	public boolean isSuperAdmin() {
		return hasAuthority(Authority.SUPER_ADMIN);
	}

//	@Override
//	public void eraseCredentials() {
//		this.ldapUserDetails.eraseCredentials();
//	}

	private boolean hasAuthority(String authorityToCheck) {
		if(CollectionUtils.isEmpty(ldapUserDetails.getAuthorities())) {
			return false;
		}
		for (GrantedAuthority authority : ldapUserDetails.getAuthorities()) {
			if(authorityToCheck.equalsIgnoreCase(authority.getAuthority())) {
				return true;
			}
		}
		return false;
	}
}
