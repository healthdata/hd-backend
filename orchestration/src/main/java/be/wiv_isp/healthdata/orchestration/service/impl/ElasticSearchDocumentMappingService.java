/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.service.IElasticSearchDocumentMappingService;
import be.wiv_isp.healthdata.orchestration.service.IMappingService;
import be.wiv_isp.healthdata.orchestration.service.IAbstractRegistrationWorkflowService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

@Service
public class ElasticSearchDocumentMappingService implements IElasticSearchDocumentMappingService {

	private static final Logger LOG = LoggerFactory.getLogger(ElasticSearchDocumentMappingService.class);

	private static final int NB_THREADS = 10;
	private static final int MAX_RESULTS_SIZE = 20;
	private static final int WAIT_TIMEOUT_OFFER = 60;
	private static final int WAIT_TIMEOUT_POLL = 60;

	private Queue<Long> toProcess;
	private List<Thread> threads;

	@Autowired
	private IMappingService mappingService;

	@Autowired
	private IAbstractRegistrationWorkflowService<? extends AbstractRegistrationWorkflow, ?> workflowService;

	private BlockingQueue<Entry<Long, String>> results;

	@Override
	public void initialize() {
		LOG.debug(MessageFormat.format("Initializing {0}", ElasticSearchDocumentMappingService.class));
		toProcess = new LinkedList<>();
		threads = new LinkedList<>();
		results = new ArrayBlockingQueue<>(MAX_RESULTS_SIZE);
	}

	@Override
	public void add(Long... ids) {
		LOG.debug(MessageFormat.format("Adding {0} workflows to the queue", ids.length));
		toProcess.addAll(Arrays.asList(ids));
	}

	@Override
	public void start() {
		LOG.debug(MessageFormat.format("Start computing elasticsearch documents, creating {0} threads.", NB_THREADS));

		for(int i=0; i<NB_THREADS; ++i) {
			final Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
					Long id = getNextIdToProcess();
					MDC.put("logWSReqResp", "false");
					while (id != null) {
						LOG.debug(MessageFormat.format("Retrieving elasticsearch document for workflow [{0}] from mapping service", id));

						String elasticSearchDocument = null;

						try {
							final AbstractRegistrationWorkflow workflow = workflowService.get(id);
							elasticSearchDocument = mappingService.mapToElasticSearchDocument(workflow).toString();

							LOG.debug(MessageFormat.format("Elasticsearch document retrieved for workflow [{0}]", workflow.getId()));
						} catch (Exception e) {
							LOG.error("The Elasticsearch document could not be retrieved", e);
						}

						try {
							results.offer(new AbstractMap.SimpleEntry<>(id, elasticSearchDocument), WAIT_TIMEOUT_OFFER, TimeUnit.SECONDS);
						} catch (InterruptedException e) {
							LOG.error("Could not insert entry into blocking queue.", e);
						}

						id = getNextIdToProcess();
					}
				}
			});
			threads.add(t);
		}

		for(Thread t : threads) {
			t.start();
		}
	}

	@Override
	public Entry<Long, String> next() {
		LOG.debug("Polling next result");

		if(toProcess.isEmpty()) {
			if(allThreadsFinished()) {
				LOG.debug("No ids left to processed, and all threads have finished. Returning null.");
				return null;
			}
		}

		try {
			final Entry<Long, String> result = results.poll(WAIT_TIMEOUT_POLL, TimeUnit.SECONDS);

			if(result != null) {
				LOG.debug(MessageFormat.format("Returning result for workflow [{0}]", result.getKey()));
			} else {
				LOG.debug(MessageFormat.format("No more results found after {0} seconds. Returning null.", WAIT_TIMEOUT_POLL));
			}

			return result;
		} catch (InterruptedException e) {
			LOG.error("", e);
			return null;
		}
	}

	private synchronized Long getNextIdToProcess() {
		LOG.debug("Retrieving next id to process");
		final Long id = toProcess.poll();
		LOG.debug(MessageFormat.format("Returning id [{0}]", id));

		return id;
	}

	private boolean allThreadsFinished() {
		for(Thread t : threads) {
			if(!Thread.State.TERMINATED.equals(t.getState())) {
				return false;
			}
		}
		return true;
	}
}
