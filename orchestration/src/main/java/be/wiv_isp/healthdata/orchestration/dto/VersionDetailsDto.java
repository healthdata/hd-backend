/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.dto;

import be.wiv_isp.healthdata.common.util.StringUtils;
import org.codehaus.jackson.annotate.JsonProperty;

public class VersionDetailsDto {

    private String version;
    private String frontendCommit;
    private String backendCommit;
    private String mappingCommit;

    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @JsonProperty("frontend")
    public String getFrontendCommit() {
        return frontendCommit;
    }

    public void setFrontendCommit(String frontendCommit) {
        this.frontendCommit = frontendCommit;
    }

    @JsonProperty("backend")
    public String getBackendCommit() {
        return backendCommit;
    }

    public void setBackendCommit(String backendCommit) {
        this.backendCommit = backendCommit;
    }

    @JsonProperty("mapping")
    public String getMappingCommit() {
        return mappingCommit;
    }

    public void setMappingCommit(String mappingCommit) {
        this.mappingCommit = mappingCommit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof VersionDetailsDto))
            return false;

        VersionDetailsDto that = (VersionDetailsDto) o;

        if (version != null ? !version.equals(that.version) : that.version != null)
            return false;
        if (frontendCommit != null ? !frontendCommit.equals(that.frontendCommit) : that.frontendCommit != null)
            return false;
        if (backendCommit != null ? !backendCommit.equals(that.backendCommit) : that.backendCommit != null)
            return false;
        return !(mappingCommit != null ? !mappingCommit.equals(that.mappingCommit) : that.mappingCommit != null);

    }

    @Override
    public int hashCode() {
        int result = version != null ? version.hashCode() : 0;
        result = 31 * result + (frontendCommit != null ? frontendCommit.hashCode() : 0);
        result = 31 * result + (backendCommit != null ? backendCommit.hashCode() : 0);
        result = 31 * result + (mappingCommit != null ? mappingCommit.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return StringUtils.toString(this);
    }
}
