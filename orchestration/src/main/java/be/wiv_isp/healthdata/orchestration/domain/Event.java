/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.EventType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "EVENTS")
@Inheritance(strategy=InheritanceType.JOINED)
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "EVENT_ID")
    protected Long id;

    @Column(name = "TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    protected EventType type;

    @Fetch(FetchMode.SUBSELECT)
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "EVENTS_NOTIFICATION_USERS", joinColumns = @JoinColumn(name = "EVENT_ID"), inverseJoinColumns = @JoinColumn(name = "USER_ID"))
    protected List<User> users = new ArrayList<>();

    @ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "EVENTS_NOTIFICATION_TIMES", joinColumns = @JoinColumn(name = "EVENT_ID"))
	@Column(name = "NOTIFICATION_TIME")
    protected List<Timestamp> notificationTimes = new ArrayList<>();

    @Column(name = "CREATED_ON", nullable = false)
    protected Timestamp createdOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Timestamp> getNotificationTimes() {
        return notificationTimes;
    }

    public void setNotificationTimes(List<Timestamp> notificationTimes) {
        this.notificationTimes = notificationTimes;
    }

    public void clearNotificationTimes() {
        notificationTimes.clear();
    }

    public void addNotificationTime(final Date notificationTime) {
        if(notificationTime == null) {
            return;
        }

        notificationTimes.add(new Timestamp(notificationTime.getTime()));
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    @PrePersist
    public void onCreate() {
        setCreatedOn(new Timestamp(new Date().getTime()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Event))
            return false;

        Event event = (Event) o;

        if (id != null ? !id.equals(event.id) : event.id != null)
            return false;
        if (type != event.type)
            return false;
        if (users != null ? !users.equals(event.users) : event.users != null)
            return false;
        if (notificationTimes != null ? !notificationTimes.equals(event.notificationTimes) : event.notificationTimes != null)
            return false;
        return !(createdOn != null ? !createdOn.equals(event.createdOn) : event.createdOn != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (users != null ? users.hashCode() : 0);
        result = 31 * result + (notificationTimes != null ? notificationTimes.hashCode() : 0);
        result = 31 * result + (createdOn != null ? createdOn.hashCode() : 0);
        return result;
    }

    public String notificationTimesAsString() {
        if(notificationTimes == null) {
            return null;
        }
        final StringBuilder sb = new StringBuilder("(");
        for (int i = 0; i < notificationTimes.size(); i++) {
            sb.append(DateFormat.DATE_AND_TIME.format(notificationTimes.get(i)));
            if(i < notificationTimes.size()-1) {
                sb.append(", ");
            }
        }
        return sb.append(")").toString();
    }
}
