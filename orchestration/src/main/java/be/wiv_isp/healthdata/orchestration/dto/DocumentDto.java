/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dto;

import be.wiv_isp.healthdata.common.json.deserializer.JsonToByteArrayDeserializer;
import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.ByteArrayToJsonSerializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import be.wiv_isp.healthdata.orchestration.domain.AbstractDocument;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.sql.Timestamp;

public class DocumentDto {

    @JsonSerialize(using = ByteArrayToJsonSerializer.class)
    @JsonDeserialize(using = JsonToByteArrayDeserializer.class)
    @Deprecated
    private byte[] documentContent;
    protected DocumentData documentData;
    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp createdOn;
    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private Timestamp updatedOn;

    public DocumentDto() {
        documentData = new DocumentData();
    }

    public DocumentDto(AbstractDocument document) {
        documentData = new DocumentData();
        documentData.setContent(document.getDocumentContent());
        documentContent = document.getDocumentContent();
        createdOn = document.getCreatedOn();
        updatedOn = document.getUpdatedOn();
    }

    @Deprecated
    public byte[] getDocumentContent() {
        documentContent = documentData.getContent();
        return documentContent;
    }

    @Deprecated
    public void setDocumentContent(byte[] documentContent) {
        this.documentData.setContent(documentContent);
        this.documentContent = documentContent;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public DocumentData getDocumentData() {
        return documentData;
    }

    public void setDocumentData(DocumentData documentData) {
        this.documentData = documentData;
    }

}
