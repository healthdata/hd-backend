/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.enumeration;

import be.wiv_isp.healthdata.orchestration.domain.WorkflowStatusTransition;

import java.util.*;

public enum WorkflowStatus {

	// HD4DP
	IN_PROGRESS,
	SUBMITTED,
	UNAUTHORIZED,
	ACTION_NEEDED,

	// HD4RES
	NEW,
	ANNOTATIONS_NEEDED,
	CORRECTIONS_REQUESTED,
	TECHNICAL_ERROR,
	AUTHORIZATION_FAILED,
	VALIDATION_FAILED,

	// HD4DP and HD4RES
	DELETED,
	OUTBOX,
	APPROVED,

	@Deprecated
	CORRECTIONS_NEEDED,
	@Deprecated
	LOADED_IN_STG; // not used after 1.7.0 (Q2)

	static private Map<WorkflowStatusTransition, WorkflowStatusTransitionType> transitionTypes = new HashMap<>();

	static {

		// NORMAL (HD4DP)
		transitionTypes.put(new WorkflowStatusTransition(null, IN_PROGRESS), WorkflowStatusTransitionType.NORMAL);
		transitionTypes.put(new WorkflowStatusTransition(IN_PROGRESS, IN_PROGRESS), WorkflowStatusTransitionType.NORMAL);
		transitionTypes.put(new WorkflowStatusTransition(IN_PROGRESS, OUTBOX), WorkflowStatusTransitionType.NORMAL);
		transitionTypes.put(new WorkflowStatusTransition(IN_PROGRESS, DELETED), WorkflowStatusTransitionType.NORMAL);
		transitionTypes.put(new WorkflowStatusTransition(OUTBOX, SUBMITTED), WorkflowStatusTransitionType.NORMAL);
		transitionTypes.put(new WorkflowStatusTransition(SUBMITTED, IN_PROGRESS), WorkflowStatusTransitionType.NORMAL);
		transitionTypes.put(new WorkflowStatusTransition(SUBMITTED, APPROVED), WorkflowStatusTransitionType.NORMAL);
		transitionTypes.put(new WorkflowStatusTransition(SUBMITTED, ACTION_NEEDED), WorkflowStatusTransitionType.NORMAL);
		transitionTypes.put(new WorkflowStatusTransition(SUBMITTED, UNAUTHORIZED), WorkflowStatusTransitionType.NORMAL);
		transitionTypes.put(new WorkflowStatusTransition(ACTION_NEEDED, ACTION_NEEDED), WorkflowStatusTransitionType.NORMAL);
		transitionTypes.put(new WorkflowStatusTransition(ACTION_NEEDED, OUTBOX), WorkflowStatusTransitionType.NORMAL);
		transitionTypes.put(new WorkflowStatusTransition(UNAUTHORIZED, OUTBOX), WorkflowStatusTransitionType.NORMAL);

		// NORMAL (HD4RES)
		transitionTypes.put(new WorkflowStatusTransition(null, NEW), WorkflowStatusTransitionType.NORMAL);
		transitionTypes.put(new WorkflowStatusTransition(NEW, APPROVED), WorkflowStatusTransitionType.NORMAL);
		transitionTypes.put(new WorkflowStatusTransition(NEW, ANNOTATIONS_NEEDED), WorkflowStatusTransitionType.NORMAL);
		transitionTypes.put(new WorkflowStatusTransition(NEW, TECHNICAL_ERROR), WorkflowStatusTransitionType.NORMAL);
		transitionTypes.put(new WorkflowStatusTransition(ANNOTATIONS_NEEDED, ANNOTATIONS_NEEDED), WorkflowStatusTransitionType.NORMAL);
		transitionTypes.put(new WorkflowStatusTransition(ANNOTATIONS_NEEDED, NEW), WorkflowStatusTransitionType.NORMAL);
		transitionTypes.put(new WorkflowStatusTransition(ANNOTATIONS_NEEDED, OUTBOX), WorkflowStatusTransitionType.NORMAL);
		transitionTypes.put(new WorkflowStatusTransition(OUTBOX, CORRECTIONS_REQUESTED), WorkflowStatusTransitionType.NORMAL);
		transitionTypes.put(new WorkflowStatusTransition(CORRECTIONS_REQUESTED, NEW), WorkflowStatusTransitionType.NORMAL);

	}

	public Set<WorkflowActionType> getAvailableActions() {
		final Set<WorkflowActionType> actions = new HashSet<>();

		// actions that are allowed on all statuses
		actions.add(WorkflowActionType.START);
		actions.add(WorkflowActionType.STOP);
		actions.add(WorkflowActionType.UPDATE_STATUS);
		actions.add(WorkflowActionType.UPDATE_FOLLOW_UP_ACTION);

		switch (this) {
			case IN_PROGRESS:
				actions.add(WorkflowActionType.SUBMIT);
				actions.add(WorkflowActionType.SAVE);
				actions.add(WorkflowActionType.DELETE);
				break;
			case OUTBOX:
				actions.add(WorkflowActionType.SEND);
				break;
			case SUBMITTED:
				actions.add(WorkflowActionType.REOPEN);
				actions.add(WorkflowActionType.UPDATE_FOR_CORRECTION);
				break;
			case UNAUTHORIZED:
				actions.add(WorkflowActionType.SUBMIT);
				break;
			case ACTION_NEEDED:
				actions.add(WorkflowActionType.SAVE);
				actions.add(WorkflowActionType.SUBMIT);
				actions.add(WorkflowActionType.DELETE);
				break;
			case AUTHORIZATION_FAILED:
				actions.add(WorkflowActionType.CREATE_FOR_REVIEW);
				break;
			case VALIDATION_FAILED:
				actions.add(WorkflowActionType.CREATE_FOR_REVIEW);
				break;
			case NEW:
				actions.add(WorkflowActionType.CREATE_FOR_REVIEW);
				actions.add(WorkflowActionType.DELETE);
				break;
			case ANNOTATIONS_NEEDED:
				actions.add(WorkflowActionType.CREATE_FOR_REVIEW); // in the case the document was rejected on the HD4RES side, reopened (and submitted) at the HD4DP side
				actions.add(WorkflowActionType.SUBMIT_ANNOTATIONS);
				actions.add(WorkflowActionType.RESUBMIT);
				actions.add(WorkflowActionType.DELETE);
				break;
			case CORRECTIONS_REQUESTED:
				actions.add(WorkflowActionType.CREATE_FOR_REVIEW);
				actions.add(WorkflowActionType.DELETE);
				break;
			case APPROVED:
				actions.add(WorkflowActionType.UPDATE_FOR_CORRECTION);
				actions.add(WorkflowActionType.CREATE_FOR_REVIEW); // in the case the document was approved on the HD4RES side, and reopened at the HD4DP side before the APPROVED status arrived at HD4DP
				break;
		}
		return actions;
	}

	public static WorkflowStatusTransitionType getTransitionType(WorkflowStatus initialStatus, WorkflowStatus nextStatus) {
		final WorkflowStatusTransitionType type = transitionTypes.get(new WorkflowStatusTransition(initialStatus, nextStatus));
		if(type == null) {
			return WorkflowStatusTransitionType.ERROR;
		}
		return type;
	}
}
