/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.text.MessageFormat;
import java.util.Objects;

@Embeddable
public class WorkflowFlags {

    @Column(name = "CORRECTIONS")
    private boolean corrections;

    @Column(name = "FOLLOW_UP")
    private boolean followUp;

    public WorkflowFlags() {

    }

    public WorkflowFlags(WorkflowFlags flags) {
        this.corrections = flags.isCorrections();
        this.followUp = flags.isFollowUp();
    }

    public boolean isCorrections() {
        return corrections;
    }

    public void setCorrections(boolean corrections) {
        this.corrections = corrections;
    }

    public boolean isFollowUp() {
        return followUp;
    }

    public void setFollowUp(boolean followUp) {
        this.followUp = followUp;
    }

    @Override
    public int hashCode() {
        return Objects.hash(corrections, followUp);
    }

    @Override
    public boolean equals(Object other) {
        if(other == this) {
            return true;
        }

        if(other instanceof WorkflowFlags) {
            WorkflowFlags o = (WorkflowFlags) other;
            return Objects.equals(corrections, o.corrections) && Objects.equals(followUp, o.followUp);
        }

        return false;
    }

    @Override
    public String toString() {
        return MessageFormat.format("[ Flags : corrections = {0}, followUp = {1}]", corrections, followUp);
    }
}
