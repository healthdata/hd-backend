/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.aspect;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.common.api.exception.UnauthorizedException;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.orchestration.annotation.OrganizationPreAuthorize;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.UriInfo;

@Aspect
@Component
public class OrganizationPreAuthorizeAspect {

	@Around("@annotation(organizationPreAuthorize)")
	public Object preAuthorize(ProceedingJoinPoint pjp, OrganizationPreAuthorize organizationPreAuthorize) throws Throwable {

		final UriInfo info = getUriInfo(pjp);
		final Long organizationId = RestUtils.getPathLong(info, organizationPreAuthorize.organizationIdPathParam());
		final Organization authenticatedOrganization = AuthenticationService.getAuthenticatedOrganization();
		if(!authenticatedOrganization.getId().equals(organizationId)) {
			throw new UnauthorizedException();
		}

		return pjp.proceed();
	}

	private UriInfo getUriInfo(final ProceedingJoinPoint pjp) {
		for (final Object arg : pjp.getArgs()) {
			if(arg instanceof UriInfo) {
				return (UriInfo) arg;
			}
		}
		return null;
	}
}
