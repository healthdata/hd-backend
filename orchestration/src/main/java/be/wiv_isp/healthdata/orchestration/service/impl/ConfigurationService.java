/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.caching.CacheManagementService;
import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataAssert;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.orchestration.dao.IConfigurationDao;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.mapper.BooleanConfigurationMapper;
import be.wiv_isp.healthdata.orchestration.domain.search.ConfigurationSearch;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import be.wiv_isp.healthdata.orchestration.service.IPlatformService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;


@Component
public class ConfigurationService extends AbstractService<Configuration, Long, ConfigurationSearch, IConfigurationDao> implements IConfigurationService {

	private static final Logger LOG = LoggerFactory.getLogger(ConfigurationService.class);

	@Value("${healthdata.configurations.showInvisible:false}")
	private Boolean showInvisible;

	@Value("${guiHost}")
	private String guiHost; // migrate from Tomcat context to HD4DP configuration, can be removed in Q1 2017

	@Autowired
	private CacheManager cacheManager;

	@Autowired
	private IConfigurationDao dao;

	@Autowired
	private IOrganizationService organizationService;

	@Autowired
	private IPlatformService platformService;

	private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("main");

	public ConfigurationService() {
		super(Configuration.class);
	}

	@Override
	protected IConfigurationDao getDao() {
		return dao;
	}

	@Override
	public Configuration get(Long id) {
		return dao.get(id);
	}

	@Override
	@Cacheable(value = CacheManagementService.configurationCache)
	public Configuration get(ConfigurationKey key) {
		return get(key, null);
	}

	@Override
	@Cacheable(value = CacheManagementService.configurationCache)
	public Configuration get(ConfigurationKey key, Organization organization) {
		HealthDataAssert.assertTrue(!key.isOrganizationConfig() || organization != null, Response.Status.INTERNAL_SERVER_ERROR, "no organization is specified when retrieving organization specific configuration");

		final ConfigurationSearch search = new ConfigurationSearch();
		search.setKey(key);
		search.setOrganization(organization);
		search.setReplaceIfUseFromMain(true);
		search.setIncludeInvisible(true);

		final Configuration configuration = getUnique(search);

		if (configuration == null) {
			final Configuration defaultConfiguration = new Configuration();
			defaultConfiguration.setKey(key);
			defaultConfiguration.setValue(defaultValueOrException(key));
			defaultConfiguration.setOrganization(organization);
			return defaultConfiguration;
		}

		return configuration;
	}

	@Override
	public boolean isTrue(ConfigurationKey key) {
		return isTrue(key, null);
	}

	@Override
	public boolean isTrue(ConfigurationKey key, Organization organization) {
		return BooleanConfigurationMapper.map(get(key, organization));
	}

	@Override
	public List<Configuration> getAll(ConfigurationSearch search) {
		List<Configuration> configurations = dao.getAll(search);

		if (canSearchHealthDataIDType(search)) {
			configurations.add(getHealthDataIDTypeConfiguration(search.getOrganization()));
		}
		if (canSearchHealthDataIDValue(search)) {
			configurations.add(getHealthDataIDValueConfiguration(search.getOrganization()));
		}

		// replace configuration by configuration from main organization where
		// USE_FROM_MAIN flag is set to TRUE
		if (Boolean.TRUE.equals(search.getReplaceIfUseFromMain())) {
			Organization main = null;
			final List<Configuration> configs = new ArrayList<>();
			for (final Configuration c : configurations) {
				if (Boolean.TRUE.equals(c.getUseFromMain())) {
					LOG.debug(MessageFormat.format("USE_FROM_MAIN flag set to TRUE for key [{0}]. Retrieving configuration from main organization.", c.getKey()));
					if (main == null) {
						main = organizationService.getMain();
					}
					configs.add(get(c.getKey(), main));
				} else {
					configs.add(c);
				}
			}
			configurations = configs;
		}

		if (!(Boolean.TRUE.equals(search.getIncludeInvisible()) || this.showInvisible)) {
			configurations = removeHiddenConfigurations(configurations);
		}

		return configurations;
	}

	private List<Configuration> removeHiddenConfigurations(final List<Configuration> configurations) {
		final List<Configuration> filteredConfigurations = new ArrayList<>();
		for (Configuration configuration : configurations) {
            if (configuration.getKey().isVisible()) {
                filteredConfigurations.add(configuration);
            }
        }
		return filteredConfigurations;
	}

	@Override
	public Configuration create(Configuration configuration) {
		HealthDataAssert.assertTrue(!configuration.getKey().isOrganizationConfig() || configuration.getOrganization() != null, Response.Status.INTERNAL_SERVER_ERROR, "no organization is specified when creating organization specific configuration");
		return super.create(configuration);
	}

	@Override
	@Transactional
	public Configuration update(Configuration configuration) {
		if (ConfigurationKey.HEALTHDATA_ID_TYPE.equals(configuration.getKey())) {
			if (configuration.getOrganization().isMain()) {
				Organization organization = organizationService.get(configuration.getOrganization().getId());
				organization.setHealthDataIDType(configuration.getValue());
				organization = organizationService.update(organization);
				cacheManager.getCache(CacheManagementService.configurationCache).clear();
				configuration.setOrganization(organization);
			}
			return configuration;
		}

		if (ConfigurationKey.HEALTHDATA_ID_VALUE.equals(configuration.getKey())) {
			if (configuration.getOrganization().isMain()) {
				Organization organization = organizationService.get(configuration.getOrganization().getId());
				organization.setHealthDataIDValue(configuration.getValue());
				organization = organizationService.update(organization);
				cacheManager.getCache(CacheManagementService.configurationCache).clear();
				configuration.setOrganization(organization);
			}
			return configuration;
		}

		HealthDataAssert.assertTrue(!configuration.getKey().isOrganizationConfig() || configuration.getOrganization() != null, Response.Status.INTERNAL_SERVER_ERROR, "no organization is specified when updating organization specific configuration");

		final Configuration updatedConfiguration = dao.update(configuration);
		cacheManager.getCache(CacheManagementService.configurationCache).clear();
		return updatedConfiguration;
	}

	private boolean canSearchHealthDataIDType(ConfigurationSearch search) {
		return (search.getKey() == null || search.getKey().equals(ConfigurationKey.HEALTHDATA_ID_TYPE)) && search.getOrganization() != null;
	}

	private boolean canSearchHealthDataIDValue(ConfigurationSearch search) {
		return (search.getKey() == null || search.getKey().equals(ConfigurationKey.HEALTHDATA_ID_VALUE)) && search.getOrganization() != null;
	}

	private Configuration getHealthDataIDTypeConfiguration(Organization organization) {
		Configuration configuration = new Configuration();
		configuration.setId(0L);
		configuration.setOrganization(organization);
		configuration.setKey(ConfigurationKey.HEALTHDATA_ID_TYPE);
		configuration.setValue(organization.getHealthDataIDType());
		configuration.setDefaultValue(organization.getHealthDataIDType());
		return configuration;
	}

	private Configuration getHealthDataIDValueConfiguration(Organization organization) {
		Configuration configuration = new Configuration();
		configuration.setId(0L);
		configuration.setOrganization(organization);
		configuration.setKey(ConfigurationKey.HEALTHDATA_ID_VALUE);
		configuration.setValue(organization.getHealthDataIDValue());
		configuration.setDefaultValue(organization.getHealthDataIDValue());
		return configuration;
	}

	@Override
	@Transactional
	public void deleteInvalid() {
		final List<String> deletedKeys = dao.deleteWithInvalidKey();
		for (String deletedKey : deletedKeys) {
			LOG.info("Removed invalid configuration: {}", deletedKey);
		}
	}

	@Override
	@Transactional
	public  void deleteInapplicable() {
		final Platform platform = platformService.getCurrentPlatform();
		for (Configuration configuration : dao.getAll()) {
			if (!configuration.getKey().isApplicable(platform)) {
				LOG.info("Removing non-applicable configuration {}", configuration.getKey());
				dao.delete(configuration);
			}
		}
	}

	@Override
	@Transactional
	public void createMissing() {
		final Platform platform = platformService.getCurrentPlatform();
		final Organization main = organizationService.getMain();
		final List<Organization> organizations = organizationService.getAll();

		for (final ConfigurationKey key : ConfigurationKey.values()) {
			if (key.equals(ConfigurationKey.HEALTHDATA_ID_TYPE) || key.equals(ConfigurationKey.HEALTHDATA_ID_VALUE))
				continue;
			if (!key.isApplicable(platform))
				continue;

			final String defaultValue = defaultValueOrEmptyString(key);

			if (key.isOrganizationConfig()) {
				String value = defaultValue; // main value if it exists, default value otherwise

				if(main != null) {
					ConfigurationSearch search = new ConfigurationSearch();
					search.setKey(key);
					search.setOrganization(main);
					search.setOrganizationCanBeNull(false);
					Configuration mainConfig = dao.getUnique(search);
					
					if(mainConfig == null) {
						search.setOrganizationCanBeNull(true);
						mainConfig = dao.getUnique(search);
					}

					if(mainConfig != null) {
						value = mainConfig.getValue();
					}
				}

				for (final Organization organization : organizations) {
					final ConfigurationSearch search = new ConfigurationSearch();
					search.setKey(key);
					search.setOrganization(organization);
					search.setOrganizationCanBeNull(false);
					final Configuration existingConfiguration = dao.getUnique(search);
					if (existingConfiguration == null) {
						LOG.info("Creating configuration [{}] with value [{}] for organization [{}]", key.name(), value, organization);
						final Configuration configuration = new Configuration();
						configuration.setKey(key);
						configuration.setValue(value);
						configuration.setDefaultValue(defaultValue);
						configuration.setOrganization(organization);
						configuration.setUseFromMain(false);
						configuration.setUpdatedOn(new Timestamp(new Date().getTime()));
						create(configuration);
					}
				}
			} else {
				String value = defaultValue;
				if (key.equals(ConfigurationKey.GUI_HOST) && !StringUtils.isBlank(guiHost)) { // migration to configuration, can be removed in Q1 2017
					value = guiHost;
				}

				final ConfigurationSearch search = new ConfigurationSearch();
				search.setKey(key);
				final Configuration existingConfiguration = dao.getUnique(search);
				if (existingConfiguration == null) {
					LOG.info("Creating configuration [{}] with value [{}]", key.name(), defaultValue);
					final Configuration configuration = new Configuration();
					configuration.setKey(key);
					configuration.setValue(value);
					configuration.setDefaultValue(defaultValue);
					configuration.setOrganization(null);
					configuration.setUseFromMain(false);
					configuration.setUpdatedOn(new Timestamp(new Date().getTime()));
					create(configuration);
				}
			}
		}
	}

	@Override
	@Transactional
	// can be removed when Q1-2017 is installed everywhere
	public void migrateEmInterfaceType() {
		if(!platformService.getCurrentPlatform().equals(Platform.HD4DP)) {
			return;
		}

		final ConfigurationSearch search = new ConfigurationSearch();
		search.setKey(ConfigurationKey.EM_INTERFACE_TYPE);
		final List<Configuration> configurations = getAll(search);

		for (Configuration configuration : configurations) {
			final Configuration emInterfaceOut = get(ConfigurationKey.EM_INTERFACE_OUT, configuration.getOrganization());
			if(!emInterfaceOut.getValue().equals(configuration.getValue())) {
				configuration.setValue(emInterfaceOut.getValue());
				update(configuration);
			}
		}
	}

	@Override
	@Transactional
	public void deleteUnattachedOrganizationConfigurations() {
		for (Configuration configuration : dao.getAll()) {
			if(configuration.getKey().isOrganizationConfig() && configuration.getOrganization() == null) {
				delete(configuration);
			}
		}
	}

	private String defaultValueOrEmptyString(ConfigurationKey key) {
		try {
            return resourceBundle.getString(key.name());
        } catch (Exception e) {
            return "";
        }
	}

	private String defaultValueOrException(ConfigurationKey key) {
		try {
			return resourceBundle.getString(key.name());
		} catch (Exception e) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.NON_UNIQUE_SEARCH_RESULT, 0, "CONFIGURATION", "key = " + key);
			throw exception;
		}
	}
}
