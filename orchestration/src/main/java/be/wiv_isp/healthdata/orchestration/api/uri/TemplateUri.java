/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.api.uri;

import be.wiv_isp.healthdata.common.api.ClientVersion;
import be.wiv_isp.healthdata.common.domain.enumeration.TemplateKey;

import java.text.MessageFormat;

public class TemplateUri {

	private static final String SEARCH_URI_PATTERN = "{0}/templates";

	private static final String URI_PATTERN = SEARCH_URI_PATTERN + "/{1}";

	private String host;

	private TemplateKey templateKey;

	public void setHost(String host) {
		this.host = host;
	}

	public void setKey(TemplateKey templateKey) {
		this.templateKey = templateKey;
	}

	static public String getClientVersion() {
		return ClientVersion.Template.getDefault();
	}

	@Override
	public String toString() {
		if (templateKey != null) {
			return MessageFormat.format(URI_PATTERN, host, templateKey.toString());
		}
		return MessageFormat.format(SEARCH_URI_PATTERN, host);
	}
}
