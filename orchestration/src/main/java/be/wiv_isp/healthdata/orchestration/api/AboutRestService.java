/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.orchestration.annotation.Auditable;
import be.wiv_isp.healthdata.orchestration.domain.About;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ApiType;
import be.wiv_isp.healthdata.orchestration.service.IAboutService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Component
@Path("/about")
public class AboutRestService {

	private static final Logger LOG = LoggerFactory.getLogger(AboutRestService.class);

	@Autowired
	private IAboutService aboutService;

	@GET
	@Auditable(apiType = ApiType.ABOUT)
	@Path("/{key}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("key") String key) {
		LOG.info("GET Request: Get about with key [{}]", key);
		final About about = aboutService.get(key);

		if (about == null) {
			LOG.info("No about found for key [{}]", key);
			return Response.status(Status.NOT_FOUND).build();
		}

		return Response.ok(about).build();
	}
}
