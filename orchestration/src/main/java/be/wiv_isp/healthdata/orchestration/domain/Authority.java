/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import org.springframework.security.core.GrantedAuthority;

import java.util.Objects;

public class Authority implements GrantedAuthority {

	private static final long serialVersionUID = 1L;

	public static final String ADMIN = "ROLE_ADMIN";
	public static final String USER = "ROLE_USER";
	public static final String SUPPORT = "ROLE_SUPPORT";
	public static final String SUPER_ADMIN = "ROLE_SUPER_ADMIN";
	public static final String HD4PRC = "ROLE_HD4PRC";

	public static final Authority USER_AUTHORITY = new Authority(Authority.USER);
	public static final Authority ADMIN_AUTHORITY = new Authority(Authority.ADMIN);
	public static final Authority SUPPORT_AUTHORITY = new Authority(Authority.SUPPORT);
	public static final Authority HD4PRC_AUTHORITY = new Authority(Authority.HD4PRC);
	public static final Authority SUPER_ADMIN_AUTHORITY = new Authority(Authority.SUPER_ADMIN);

	public Authority() {}

	public Authority(String authority) {
		this.authority = authority;
	}

	private String authority;

	@Override
	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof Authority) {
			Authority other = (Authority) o;

			return Objects.equals(authority, other.authority);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.authority);
	}

	@Override
	public String toString() {
		return "Authority {" + "authority = " + authority + "}";
	}
}
