/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.common.domain.enumeration.Language;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.orchestration.service.IPdfTemplateForwardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Component
@Path("/pdf/templates")
public class PdfTemplateRestService {

	@Autowired
	private IPdfTemplateForwardService pdfTemplateForwardService;

	@GET
	@Path("/{dataCollectionDefinitionId}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response get(@HeaderParam("Accept-Language") String languageHeader, @Context UriInfo info) {
		Long dataCollectionDefinitionId = RestUtils.getPathLong(info, RestUtils.ParameterName.DATA_COLLECTION_DEFINITION_ID);
		byte[] pfdTemplate = pdfTemplateForwardService.get(dataCollectionDefinitionId, Language.getFromHeader(languageHeader));
		return Response.ok(pfdTemplate).build();
	}

}
