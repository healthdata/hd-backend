/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.orchestration.dao.IStatusMessageDao;
import be.wiv_isp.healthdata.orchestration.domain.AbstractStatusMessage;
import be.wiv_isp.healthdata.orchestration.domain.search.GroupByStatusMessage;
import be.wiv_isp.healthdata.orchestration.domain.search.StatusMessageSearch;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AbstractStatusMessageDao<Entity extends AbstractStatusMessage> extends CrudDaoV2<Entity, Long, StatusMessageSearch> implements IStatusMessageDao<Entity> {

	public static final long _1_DAY_AS_MILLIS = 24L * 60L * 60L * 1000L;

	public AbstractStatusMessageDao(Class<Entity> entityClass) {
		super(entityClass);
	}

	@Override
	protected List<Predicate> getPredicates(StatusMessageSearch search, CriteriaBuilder cb, Root<Entity> rootEntry) {
		final List<Predicate> predicates = new ArrayList<>();

		if(Boolean.TRUE.equals(search.getLastOnly())) {
			final List<GroupByStatusMessage> groupByStatusMessages = getAllGroupBy();
			final List<Predicate> groupByPredicates = new ArrayList<>();
			for (GroupByStatusMessage groupByStatusMessage : groupByStatusMessages) {
				final Predicate identificationType = cb.equal(rootEntry.<HealthDataIdentification>get("healthDataIdentification").get("type"), groupByStatusMessage.getIdentificationType());
				final Predicate identificationValue = cb.equal(rootEntry.<HealthDataIdentification>get("healthDataIdentification").get("value"), groupByStatusMessage.getIdentificationValue());
				final Predicate createdOn = cb.equal(rootEntry.get("createdOn"), groupByStatusMessage.getCreatedOn());
				groupByPredicates.add(cb.and(identificationType, identificationValue, createdOn));
			}

			predicates.add(cb.or(groupByPredicates.toArray(new Predicate[groupByPredicates.size()])));
		}
		if(search.getCreatedOn() != null) {
			final SimpleDateFormat sdf = new SimpleDateFormat(DateFormat.DATE.getPattern());
			final Date start = DateFormat.stringToDate(sdf.format(search.getCreatedOn()), DateFormat.DATE);
			final Date end = new Date(start.getTime() + _1_DAY_AS_MILLIS);

			predicates.add(cb.between(rootEntry.<Date>get("createdOn"), start, end));
		}
		if(search.getHealthDataIdentification() != null) {
			Predicate pred1 = cb.and();
			if(search.getHealthDataIdentification().getType() != null) {
				pred1 = cb.equal(rootEntry.<HealthDataIdentification>get("healthDataIdentification").get("type"), search.getHealthDataIdentification().getType());
			}

			Predicate pred2 = cb.and();
			if(search.getHealthDataIdentification().getValue() != null) {
				pred2 = cb.equal(rootEntry.<HealthDataIdentification>get("healthDataIdentification").get("value"), search.getHealthDataIdentification().getValue());
			}

			predicates.add(cb.and(pred1, pred2));
		}
		if(search.getCreatedOnAfter() != null) {
			predicates.add(cb.greaterThanOrEqualTo(rootEntry.<Date>get("createdOn"), search.getCreatedOnAfter()));
		}

		return predicates;
	}

	private List<GroupByStatusMessage> getAllGroupBy() {
		final CriteriaBuilder cb = em.getCriteriaBuilder();
		final CriteriaQuery<GroupByStatusMessage> cq = cb.createQuery(GroupByStatusMessage.class);
		final Root<Entity> rootEntry = cq.from(entityClass);
		cq.multiselect(rootEntry.<HealthDataIdentification>get("healthDataIdentification").get("type"), rootEntry.<HealthDataIdentification>get("healthDataIdentification").get("value"), cb.greatest(rootEntry.<Date> get("createdOn")));
		cq.groupBy(rootEntry.<HealthDataIdentification>get("healthDataIdentification").get("type"), rootEntry.<HealthDataIdentification>get("healthDataIdentification").get("value"));
		final TypedQuery<GroupByStatusMessage> allQuery = em.createQuery(cq);
		return allQuery.getResultList();
	}
}
