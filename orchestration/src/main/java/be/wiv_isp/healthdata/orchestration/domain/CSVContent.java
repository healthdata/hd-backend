/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import com.sun.jersey.core.util.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class CSVContent {

	private static final String SEMICOLON = ";";
    private static final String FIELD_DESCRIPTOR = "FIELD_DESCRIPTOR";

    // field types
    private static final String TECHNICAL_METADATA_TYPE = "TECHNICAL_METADATA";
    private static final String PATIENT_ID_TYPE = "patientID";
    private static final String DUMMY_TYPE = "DUMMY_FIELD";

    // field names
    @Deprecated
    public static final String PATIENT_IDENTIFIER = "PATIENT_IDENTIFIER";
    private static final String WORKFLOW_ID = "WORKFLOW_ID";
    private static final String DEFAULT_PATIENT_ID_FIELD_NAME = "patient_id.%id";
    private static final String DUMMY_FIELD_PREFIX = "DUMMY_FIELD_";

    private List<String> firstFields;
    private List<Descriptor> fieldDescriptors;
    private byte[] lastField;
    private boolean useDescriptors = true;


    public CSVContent() {
        this.firstFields = new ArrayList<>();
        this.fieldDescriptors = new ArrayList<>();
    }
    
    public CSVContent(File file) {
    	String csvLine;
    	
		try {
			csvLine = FileUtils.readFileToString(file);
		} catch (IOException e) {
            HealthDataException exception = new HealthDataException(e);
            exception.setExceptionType(ExceptionType.ERROR_WHILE_READING_FILE, file.getAbsolutePath());
            throw exception;
        }
    	
    	this.initialize(csvLine);
    }

    public CSVContent(String csvLine) {
        this.initialize(csvLine);
    }

    public CSVContent(byte[] content) {
        this(new String(content, StandardCharsets.UTF_8));
    }

    public int getSize() {
        return this.firstFields.size() + 1;
    }

    public String getField(int index) {
        return this.firstFields.get(index);
    }

    public void putField(int index, String value) {
        if (!useDescriptors && index > 1)
            return;
        for (int i = this.firstFields.size(); i<=index; i++)
            this.firstFields.add(null);
        this.firstFields.set(index, value);
    }

    public void putField(String fieldName, String value, String type) {
        if (!useDescriptors)
            return;
        int index = this.fieldDescriptors.indexOf(new Descriptor(fieldName, null));
        if (index == -1)
            index = this.firstFields.size();
        this.putField(index, value);
        this.putFieldDescriptor(index, fieldName, type);
    }

    public String getFieldName(int i) {
        return this.fieldDescriptors.get(i).name;
    }

    public String getFieldType(int i) {
        return this.fieldDescriptors.get(i).type;
    }

    public String getField(String fieldName) {
        if (!useDescriptors)
            return null;
        int index = fieldDescriptors.indexOf(new Descriptor(fieldName, null));
        if (index >= 0)
            return getField(index);
        return null;
    }

    private void putFieldDescriptor(int index, String name, String type) {
        for (int i=fieldDescriptors.size(); i<=index; i++)
            this.fieldDescriptors.add(null);
        this.fieldDescriptors.set(index, new Descriptor(name, type));
    }

    public byte[] getLastField() {
        return lastField;
    }

    public void setLastField(byte[] lastField) {
        this.lastField = lastField;
    }

    public void setLastField(String lastField) {
        this.lastField = lastField.getBytes(StandardCharsets.UTF_8);
    }

    public byte[] getBytes() {
        return this.toString().getBytes(StandardCharsets.UTF_8);
    }

    public boolean isUseDescriptors() {
        return useDescriptors;
    }

    public void setUseDescriptors(boolean useDescriptors) {
        if (this.useDescriptors == useDescriptors)
            return;
        if (this.useDescriptors) {
            fieldDescriptors.clear();
            while (firstFields.size() > 2)
                firstFields.remove(firstFields.size() - 1);
        }
        else {
            fieldDescriptors.clear();
            if (firstFields.size() >= 1) {
                putFieldDescriptor(0, PATIENT_IDENTIFIER, PATIENT_ID_TYPE);
            }
            if (firstFields.size() >= 2) {
                putFieldDescriptor(1, WORKFLOW_ID, TECHNICAL_METADATA_TYPE);
            }
        }
        this.useDescriptors = useDescriptors;
    }

    @Deprecated
    public String getPatientId() {
        if (useDescriptors) {
            String patientId = getField(PATIENT_IDENTIFIER);
            return patientId != null ? patientId : getField(DEFAULT_PATIENT_ID_FIELD_NAME);
        }
        else {
            return getField(0);
        }
    }

    @Deprecated
    public void putPatientId(String patientId) {
        if (useDescriptors)
            this.putField(PATIENT_IDENTIFIER, patientId, PATIENT_ID_TYPE);
        else
            this.putField(0, patientId);
    }

    public String getWorkflowId() {
        if (useDescriptors)
            return getField(WORKFLOW_ID);
        else
            return getField(1);
    }

    public void putWorkflowId(String workflowId) {
        if (useDescriptors)
            this.putField(WORKFLOW_ID, workflowId, TECHNICAL_METADATA_TYPE);
        else
            this.putField(1, workflowId);
    }

    public void addDummyField(String value) {
        setUseDescriptors(true);
        final int dummyCount = countDummyFields();
        this.putField(DUMMY_FIELD_PREFIX + dummyCount, (value!=null) ? value : DUMMY_FIELD_PREFIX + dummyCount, DUMMY_TYPE);
    }

    public void addDummyField() {
        addDummyField(null);
    }

    private int countDummyFields() {
        int count = 0;
        for (Descriptor descriptor : fieldDescriptors) {
            if (DUMMY_TYPE.equals(descriptor.type))
                ++count;
        }
        return count;
    }

    public boolean isPatientIdField(int i) {
        if (useDescriptors)
            return PATIENT_ID_TYPE.equals(getFieldType(i));
        else
            return i == 0;
    }

    public boolean isDummyField(int i) {
        return DUMMY_TYPE.equals(getFieldType(i));
    }

    public boolean isTechnicalField(int i) {
        return TECHNICAL_METADATA_TYPE.equals(getFieldType(i));
    }

    public void removeField(int i) {
        if (i>=0 && i<this.firstFields.size()) {
            this.firstFields.remove(i);
            if (useDescriptors)
                this.fieldDescriptors.remove(i);
        }
    }

    @Override
    public String toString() {
        if (useDescriptors) {
            return StringUtils.join(firstFields.toArray(), SEMICOLON) + SEMICOLON + new String(Base64.encode(getWithFieldNamesInLastField()), StandardCharsets.UTF_8);
        } else {
            return StringUtils.join(firstFields.toArray(), SEMICOLON) + SEMICOLON + new String(Base64.encode(lastField), StandardCharsets.UTF_8);
        }
    }

    private void initialize(String csvLine) {
   	    final String[] chunks = csvLine.split(SEMICOLON);

        if (chunks.length < 3) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, "CSV line must contain at least 3 fields");
            throw exception;

        }

        this.firstFields = new ArrayList<>();
        for (int i=0; i<chunks.length-1; i++) {
            this.putField(i, chunks[i]);
        }

        this.setLastField(Base64.decode(chunks[chunks.length-1].getBytes(StandardCharsets.UTF_8)));

        this.fieldDescriptors = new ArrayList<>();
        if (useDescriptors) {
            retrieveFieldNamesFromLastField();
        }
	}

	private void retrieveFieldNamesFromLastField() {
	    try {
            JSONObject jsonObject = new JSONObject(new String(lastField, StandardCharsets.UTF_8));
            JSONArray jsonArray = jsonObject.getJSONArray(FIELD_DESCRIPTOR);
            this.fieldDescriptors = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject descriptor = jsonArray.getJSONObject(i);
                this.fieldDescriptors.add(new Descriptor(descriptor.getString("name"), descriptor.getString("type")));
            }
            jsonObject.remove(FIELD_DESCRIPTOR);
            setLastField(jsonObject.toString());
        }
        catch (JSONException jsonException) {
            if (this.firstFields.size() != 2) {
                HealthDataException exception = new HealthDataException();
                exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, "CSV content without a valid field descriptor must contain exactly 3 fields");
                throw exception;
            }
            this.fieldDescriptors = new ArrayList<>();
            this.useDescriptors = false;
        }
    }

    private String getWithFieldNamesInLastField() {
            String lastField = new String(this.lastField, StandardCharsets.UTF_8);
        try {
            JSONObject content = new JSONObject(lastField);
            JSONArray jsonArray = new JSONArray();
            for (Descriptor descriptor : fieldDescriptors) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", descriptor.name);
                jsonObject.put("type", descriptor.type);
                jsonArray.put(jsonObject);
            }
            content.put(FIELD_DESCRIPTOR, jsonArray);
            return content.toString();
        }
        catch (JSONException jsonException) {
            HealthDataException exception = new HealthDataException(jsonException);
            exception.setExceptionType(ExceptionType.JSON_EXCEPTION, lastField);
            throw exception;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof CSVContent) {
            CSVContent other = (CSVContent) o;

            return Objects.equals(firstFields, other.firstFields)
                    && Objects.equals(fieldDescriptors, other.fieldDescriptors)
                    && Arrays.equals(lastField, other.lastField);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash( //
                this.firstFields, //
                this.fieldDescriptors, //
                this.lastField);
    }

    private class Descriptor {
        public String name;
        public String type;

        Descriptor(String name, String type) {
            this.name = name;
            this.type = type;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Descriptor that = (Descriptor) o;

            return name.equals(that.name);
        }

        @Override
        public int hashCode() {
            return name.hashCode();
        }
    }

}
