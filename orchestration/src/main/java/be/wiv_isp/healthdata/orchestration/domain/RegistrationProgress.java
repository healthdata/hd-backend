/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.domain;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Entity
@Table(name = "PROGRESS")
public class RegistrationProgress extends Progress {

    @Fetch(FetchMode.JOIN)
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, optional=false)
    @JoinColumn(name = "WORKFLOW_HISTORY_ID")
    @JsonIgnore
    private RegistrationWorkflowHistory workflowHistory;

    public RegistrationProgress() {
    }

    public RegistrationProgress(Progress progress) {
        setValid(progress.getValid());
        setInvalid(progress.getInvalid());
        setOptional(progress.getOptional());
        setTotal(progress.getTotal());
        setValidRatio(progress.getValidRatio());
        setInvalidRatio(progress.getInvalidRatio());
        setOptionalRatio(progress.getOptionalRatio());
        setErrors(progress.getErrors());
        setWarnings(progress.getWarnings());
    }

    public RegistrationWorkflowHistory getWorkflowHistory() {
		return workflowHistory;
	}

	public void setWorkflowHistory(AbstractWorkflowHistory workflowHistory) {
        this.workflowHistory = (RegistrationWorkflowHistory) workflowHistory;
    }

}
