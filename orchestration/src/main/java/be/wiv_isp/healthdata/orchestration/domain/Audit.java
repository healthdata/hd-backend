/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.orchestration.annotation.Truncate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

/**
 * Audit domain object.
 * 
 * @author Mathias Ghys
 * 
 * @since 23/01/2015
 * 
 *        © Wiv-isp
 */
@Entity
@Table(name = "AUDIT_LOGS")
public class Audit {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;

	@Column(name = "API", unique = false, nullable = false)
	private String apiType;

	@Column(name = "METHOD", unique = false, nullable = false)
	private String method;

	@Column(name = "USERNAME", unique = false, nullable = false)
	private String userName;

	@Column(name = "SUBJECT", unique = false, nullable = true, length = 1024)
	private String subject;

	@Column(name = "STATUS", unique = false, nullable = true)
	private String status;

	@Column(name = "TIME_IN", unique = false, nullable = true)
	private Timestamp timeIn;

	@Column(name = "TIME_OUT", unique = false, nullable = true)
	private Timestamp timeOut;

	@Column(name = "EXCEPTION_MESSAGE", unique = false, nullable = true)
	private String exceptionMessage;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getApi() {
		return apiType;
	}

	public void setApi(String apiType) {
		this.apiType = apiType;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSubject() {
		return subject;
	}

	@Truncate(maxLength = 1024)
	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getTimeIn() {
		return timeIn;
	}

	public void setTimeIn(Timestamp timeIn) {
		this.timeIn = timeIn;
	}

	public Timestamp getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(Timestamp timeOut) {
		this.timeOut = timeOut;
	}

	public String getExceptionMessage() {
		return exceptionMessage;
	}

	@Truncate(maxLength = 255)
	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	@PrePersist
	public void onCreate() {
		long currentTime = new Date().getTime();
		setTimeIn(new Timestamp(currentTime));
	}

	@PreUpdate
	public void onUpdate() {
		long currentTime = new Date().getTime();
		setTimeOut(new Timestamp(currentTime));
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof Audit) {
			Audit other = (Audit) o;

			return Objects.equals(id, other.id) //
					&& Objects.equals(apiType, other.apiType) //
					&& Objects.equals(method, other.method) //
					&& Objects.equals(userName, other.userName) //
					&& Objects.equals(subject, other.subject) //
					&& Objects.equals(status, other.status) //
					&& Objects.equals(timeIn, other.timeIn) //
					&& Objects.equals(timeOut, other.timeOut) //
					&& Objects.equals(exceptionMessage, other.exceptionMessage);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash( //
				this.id, //
				this.apiType, //
				this.method, //
				this.userName, //
				this.subject, //
				this.status, //
				this.timeIn, //
				this.timeOut, //
				this.exceptionMessage);
	}

	@Override
	public String toString() {
		return "Audit {" + //
				"id = " + Objects.toString(this.id) + ", " + //
				"api = " + Objects.toString(this.apiType) + ", " + //
				"method = " + Objects.toString(this.method) + ", " + //
				"userName = " + Objects.toString(this.userName) + ", " + //
				"subject = " + Objects.toString(this.subject) + ", " + //
				"status = " + Objects.toString(this.status) + ", " + //
				"timeIn = " + Objects.toString(timeIn) + ", " + //
				"timeOut = " + Objects.toString(timeOut) + ", " + //
				"exceptionMessage = " + Objects.toString(this.exceptionMessage) + "}";
	}
}
