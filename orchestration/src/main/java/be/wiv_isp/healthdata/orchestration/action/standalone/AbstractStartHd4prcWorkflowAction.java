/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.action.standalone;

import be.wiv_isp.healthdata.orchestration.action.dto.Hd4prcCreateDto;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.StandAloneActionType;

abstract public class AbstractStartHd4prcWorkflowAction extends AbstractStandAloneAction {

    private Hd4prcCreateDto hd4prcCreateDto;

    @Override
    public StandAloneActionType getAction() {
        return StandAloneActionType.START_HD4PRC_WORKFLOW;
    }


    public Hd4prcCreateDto getHd4prcCreateDto() {
        return hd4prcCreateDto;
    }

    public void setHd4prcCreateDto(Hd4prcCreateDto hd4prcCreateDto) {
        this.hd4prcCreateDto = hd4prcCreateDto;
    }
}
