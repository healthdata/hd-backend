/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.orchestration.dao.IUserRequestDao;
import be.wiv_isp.healthdata.orchestration.domain.AbstractUserRequest;
import be.wiv_isp.healthdata.orchestration.domain.search.AbstractUserRequestSearch;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

abstract public class AbstractUserRequestDao<Entity extends AbstractUserRequest, Search extends AbstractUserRequestSearch> extends CrudDaoV2<Entity, Long, Search> implements IUserRequestDao<Entity, Search> {

    public AbstractUserRequestDao(Class<Entity> entityClass) {
        super(entityClass);
    }

    protected List<Predicate> getSharedPredicates(Search search, CriteriaBuilder cb, Root<Entity> rootEntry) {
        final List<Predicate> predicates = new ArrayList<>();
        if (StringUtils.isNotBlank(search.getFirstName())) {
            predicates.add(cb.equal(rootEntry.get("firstName"), search.getFirstName()));
        }
        if (StringUtils.isNotBlank(search.getLastName())) {
            predicates.add(cb.equal(rootEntry.get("lastName"), search.getLastName()));
        }
        if (StringUtils.isNotBlank(search.getEmail())) {
            predicates.add(cb.equal(rootEntry.get("email"), search.getEmail()));
        }
        if (StringUtils.isNotBlank(search.getEmailRequester())) {
            predicates.add(cb.equal(rootEntry.get("emailRequester"), search.getEmailRequester()));
        }
        return predicates;
    }
}
