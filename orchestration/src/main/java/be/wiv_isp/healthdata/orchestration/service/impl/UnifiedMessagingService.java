/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.service.impl;

import be.healthconnect.adr._1_0.ADR;
import be.healthconnect.adr._1_0.Addressee;
import be.healthconnect.adr._1_0.Attachment;
import be.healthconnect.adr._1_0.Patient;
import be.healthconnect.schemas.unifiedmessagingservice._1_0.UMService;
import be.healthconnect.schemas.unifiedmessagingservice._1_0.UMServicePortType;
import be.healthconnect.unifiedmessagingservice._1_0.common.SendMessageRequestType;
import be.healthconnect.unifiedmessagingservice._1_0.common.SendMessageResponseType;
import be.healthconnect.unifiedmessagingservice._1_0.common.Status;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import com.sun.jersey.core.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.ws.BindingProvider;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Component
public class UnifiedMessagingService {

    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationService.class);
    private static long MESSAGE_ID = 0;

    @Autowired
    private IConfigurationService configurationService;


    public void sendMessage(ADR adr) {
        Long messageId = getNewRequestId();
        LOG.info("sending message with id [{}]", messageId);

        UMServicePortType port = new UMService().getUMServicePortType();
        BindingProvider bp = (BindingProvider) port;
        bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "endPointConfigurationKey");

//        <?xml version="1.0" encoding="UTF-8"?>
//        <ADR xmlns="urn:be:healthconnect:adr:1_0" targetNamespace="urn:be:healthconnect:adr:1_0">
//        <Addressee>
//        <Quality>DOCTOR</Quality>
//        <Identifier>10050881</Identifier>
//        <EhboxIdType>NIHII</EhboxIdType>
//        </Addressee>
//        <Subject>eforms_medidoc_report_20170126153901</Subject>
//        <Patient>
//        <INSS>changeme_toavalidinss</INSS>
//        </Patient>
//        <Attachment>
//        <Content>base64valueofmedidoccontent</Content>
//        <Name>eforms_medidoc_report_20170126153901.dat</Name>
//        <MimeType>plain/text</MimeType>
//        <FunctionalType>dma-rep</FunctionalType>
//        </Attachment>
//        </ADR>



//        ADR adr = new ADR();
        List<Addressee> adressees = new ArrayList<>();
        Addressee adressee = new Addressee();
        adressee.setQuality("DOCTOR");
        adressee.setIdentifier("NIHII of doctor"); //TODO
        adressee.setEhboxIdType("NIHII");
        adressees.add(adressee);
        adr.setAddressee(adressees);

        adr.setSubject("");

        Patient patient = new Patient();
        patient.setINSS("");
        adr.setPatient(patient);

        List<Attachment> attachments = new ArrayList<>();
        Attachment attachment = new Attachment();
        attachment.setName("name");
        attachment.setContent(Base64.decode("content".getBytes(StandardCharsets.UTF_8)));
        attachment.setMimeType("plain/text");
        attachment.setFunctionalType("dma-rep");
        attachments.add(attachment);
        adr.setAttachment(attachments);

        SendMessageRequestType requestType = new SendMessageRequestType();
        requestType.setAdr(adr);

        SendMessageResponseType responseType = port.sendMessage(requestType);
        Status status = responseType.getStatus();
        LOG.info("message with id [{}]: code = [{}], message [{}]", messageId, status.getCode(), status.getMessage());
    }

    private synchronized long getNewRequestId() {
        return ++MESSAGE_ID;
    }
}
