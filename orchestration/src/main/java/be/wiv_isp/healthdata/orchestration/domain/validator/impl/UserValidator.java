/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.validator.impl;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.dao.IUserDao;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import be.wiv_isp.healthdata.orchestration.domain.validator.IUserValidator;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Service
public class UserValidator implements IUserValidator {

	private static final Logger LOG = LoggerFactory.getLogger(UserValidator.class);

	@Autowired
	private IUserDao userDao;
	@Autowired
	private IDataCollectionService dataCollectionService;

	@Override
	public void validate(User user) {
		validateUsername(user);
		validateOrganization(user);
		validateAuthorities(user);
		validateDataCollectionNames(user);

		if (!user.isLdapUser()) {
			validatePassword(user);
			validateFirstName(user);
			validateLastName(user);
			validateEmail(user);
		}
	}

	protected void validatePassword(User user) {
		ValidatorUtils.verifyNotBlank(user.getPassword(), "password");
	}

	protected void validateOrganization(User user) {
		ValidatorUtils.verifyNotNull(user.getOrganization(), "organization");
	}

	protected void validateAuthorities(User user) {
		ValidatorUtils.verifyNotEmpty(user.getAuthorities(), "authorities");
		for (Authority authority : user.getAuthorities()) {
			ValidatorUtils.verifyNotBlank(authority.getAuthority(), "authorities.authority");
		}
	}

	protected void validateFirstName(User user) {
		if(StringUtils.isBlank(user.getFirstName())) {
			LOG.warn("No first name is specified.");
		}
	}

	protected void validateLastName(User user) {
		if(StringUtils.isBlank(user.getLastName())) {
			LOG.warn("No last name is specified.");
		}
	}

	protected void validateEmail(User user) {
		ValidatorUtils.validateEmail(user.getEmail());

		final UserSearch userSearch = new UserSearch();
		userSearch.setEmail(user.getEmail());
		userSearch.setOrganization(user.getOrganization());
		final List<User> users = userDao.getAll(userSearch);

		if(CollectionUtils.isNotEmpty(users)) {
			if(users.size() > 1) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, "multiple users found with same email address");
				throw exception;
			}
			final User dbUser = users.get(0);
			if(!dbUser.getId().equals(user.getId())) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.EMAIL_ALREADY_EXISTS, user.getEmail());
				throw exception;
			}
		}
	}

	protected void validateUsername(User user) {
		ValidatorUtils.verifyNotBlank(user.getUsername(), "username");

		final UserSearch userSearch = new UserSearch();
		userSearch.setUsername(user.getUsername());
		userSearch.setOrganization(user.getOrganization());
		final List<User> users = userDao.getAll(userSearch);

		if(CollectionUtils.isNotEmpty(users)) {
			if(users.size() > 1) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, "multiple users found with same username");
				throw exception;
			}
			final User dbUser = users.get(0);
			if(!dbUser.getId().equals(user.getId())) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.USERNAME_ALREADY_EXISTS, user.getUsername());
				throw exception;
			}
		}
	}

	protected void validateDataCollectionNames(User user) {
		if (isUserOnly(user)) {
			ValidatorUtils.verifyNotNull(user.getDataCollectionNames(), "data collections");
			final Set<String> dataCollectionsForOrganization = dataCollectionService.getDataCollectionGroups(user.getOrganization());
			if (!dataCollectionsForOrganization.containsAll(user.getDataCollectionNames())) {
				final Collection<String> toBeRemovedDataCollections = CollectionUtils.removeAll(user.getDataCollectionNames(), dataCollectionsForOrganization);
				LOG.warn("The following set of data collection is removed from the user with id [{}] since his organization does not have access to it anymore: {}", user.getId(), toBeRemovedDataCollections);
				user.getDataCollectionNames().retainAll(dataCollectionsForOrganization);
			}
		}
	}

	protected boolean isUserOnly(User user) {
		return user.isUser() && user.getAuthorities().size() == 1;
	}
}
