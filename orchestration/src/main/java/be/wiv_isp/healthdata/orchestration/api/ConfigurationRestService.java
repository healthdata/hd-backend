/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.annotation.Auditable;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.ConfigurationDataReferenceResponse;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ApiType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.mapper.Mapper;
import be.wiv_isp.healthdata.orchestration.domain.search.ConfigurationSearch;
import be.wiv_isp.healthdata.orchestration.dto.ConfigurationDto;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@Component
@Path("/configurations")
public class ConfigurationRestService {

	private static final Logger LOG = LoggerFactory.getLogger(ConfigurationRestService.class);

	@Autowired
	private IConfigurationService configurationService;

	@Autowired
	private Mapper<ConfigurationDto, Configuration> configurationDtoToConfigurationMapper;

	@Value("${host}")
	private String host;

	@GET
	@Auditable(apiType = ApiType.CONFIGURATION)
	@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(@QueryParam("key") String keyAsString, @Context UriInfo info) {
		LOG.debug("GET Request: Get all configurations");

		ConfigurationKey key = null;
		if (StringUtils.isNotBlank(keyAsString)) {
			try {
				key = ConfigurationKey.valueOf(keyAsString);
			} catch (IllegalArgumentException e) {
				LOG.warn(MessageFormat.format("Could not map [{0}] to a configuration key", keyAsString));
				return Response.ok(new ArrayList<Configuration>()).build();
			}
		}

		final HDUserDetails principal = AuthenticationService.getUserDetails();

		boolean includeInvisible = principal.isSupport() && (principal.isAdmin() || principal.isSuperAdmin());
		for (String paramKey : info.getQueryParameters().keySet()) {
			if ("all".equalsIgnoreCase(paramKey)) {
				includeInvisible = Boolean.parseBoolean(info.getQueryParameters().getFirst(paramKey));
			}
		}

		final ConfigurationSearch search = new ConfigurationSearch();
		search.setKey(key);
		search.setOrganization(principal.getOrganization());
		search.setOrganizationCanBeNull(principal.getOrganization().isMain());
		search.setIncludeInvisible(includeInvisible);

		final List<Configuration> configurations = configurationService.getAll(search);
		return Response.ok(mapConfigurations(configurations)).build();
	}

	private List<ConfigurationDto> mapConfigurations(List<Configuration> configurations) {
		final List<ConfigurationDto> configurationDtos = new ArrayList<>();
		for (Configuration configuration : configurations) {
			configurationDtos.add(new ConfigurationDto(configuration));
		}
		return configurationDtos;
	}

	@GET
	@Auditable(apiType = ApiType.CONFIGURATION)
	@Path("/{id}")
	@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("id") Long id) {
		LOG.debug("GET Request: Get configuration with key [{}]", id);
		final Configuration configuration = configurationService.get(id);

		if (configuration == null) {
			LOG.info("No configuration found for key [{}]", id);
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(new ConfigurationDto(configuration)).build();
	}

	@PUT
	@Path("/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Auditable(apiType = ApiType.CONFIGURATION)
	@Produces(MediaType.APPLICATION_JSON)
	@Transactional
	public Response update(@PathParam("id") Long id, ConfigurationDto configurationDto) {
		LOG.info("PUT Request: Update configuration: [{}]", configurationDto);

		if (id == null || !id.equals(configurationDto.getId())) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_INPUT, MessageFormat.format("The id provided in the url ({0}) does not match with the id provided in the request body ({1})", id.toString(), configurationDto.getId().toString()));
			throw exception;
		}

		final HDUserDetails principal = AuthenticationService.getUserDetails();
		final Organization organization = principal.getOrganization();

		if (configurationDto.getKey().isOrganizationConfig()) {
			if (!organization.equals(configurationDto.getOrganization())) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.UNAUTHORIZED_CONFIGURATION_UPDATE, organization.getId().toString(), configurationDto.getId().toString(), "not allowed to modify configuration of other organization");
				throw exception;
			}
		} else {
			if (!organization.isMain()) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.UNAUTHORIZED_CONFIGURATION_UPDATE, organization.getId().toString(), configurationDto.getId().toString(), "only main organization can modifiy non organization specific configurations");
				throw exception;
			}
		}

		final Configuration configuration = configurationDtoToConfigurationMapper.map(configurationDto);
		final Configuration updatedConfiguration = configurationService.update(configuration);
		return Response.ok(new ConfigurationDto(updatedConfiguration)).build();
	}

	@GET
	@Auditable(apiType = ApiType.CONFIGURATION)
	@Path("/datareference")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDataReference() {
		LOG.debug("GET Request: Get configuration for dataReferences");
		ConfigurationDataReferenceResponse response = new ConfigurationDataReferenceResponse();

		response.setHostname(host);
		response.setPathname("/datareference");

		return Response.ok(response).build();
	}
}
