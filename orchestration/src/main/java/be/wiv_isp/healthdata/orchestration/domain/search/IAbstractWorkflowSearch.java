/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.search;

import be.wiv_isp.healthdata.common.domain.search.IEntitySearch;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface IAbstractWorkflowSearch extends IEntitySearch {

    Set<WorkflowStatus> getStatuses();

    void setStatuses(Set<WorkflowStatus> statuses);

    String getDataCollectionName();

    void setDataCollectionName(String dataCollectionName);

    Long getDataCollectionDefinitionId();

    void setDataCollectionDefinitionId(Long dataCollectionDefinitionId);

    Long getNotWorkflowId();

    void setNotWorkflowId(Long notWorkflowId);

    boolean getReturnDeleted();

    void setReturnDeleted(boolean returnDeleted);

    Date getNotUpdatedAfterDate();

    void setNotUpdatedAfterDate(Date notUpdatedAfterDate);

    Date getCreatedAfterDate();

    void setCreatedAfterDate(Date createdAfterDate);

    Date getUpdatedAfterDate();

    void setUpdatedAfterDate(Date updatedAfterDate);

    List<Long> getWorkflowIds();

    void setWorkflowIds(List<Long> workflowIds);

    Boolean getWithDocument();

    void setWithDocument(Boolean withDocument);

    Organization getOrganization();

    void setOrganization(Organization organization);

    String getSendStatus();

    void setSendStatus(String sendStatus);

}
