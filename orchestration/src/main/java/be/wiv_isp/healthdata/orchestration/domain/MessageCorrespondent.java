/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.persistence.*;
import java.text.MessageFormat;
import java.util.Objects;

@Embeddable
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class MessageCorrespondent {

    @Column(name = "IDENTIFICATION_TYPE", nullable = false)
    private String identificationType;

    @Column(name = "IDENTIFICATION_VALUE", nullable = false)
    private String identificationValue;

    @Column(name = "APPLICATION_ID", nullable = true)
    private String applicationId;

    public String getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(String identificationType) {
        this.identificationType = identificationType;
    }

    public String getIdentificationValue() {
        return identificationValue;
    }

    public void setIdentificationValue(String identificationValue) {
        this.identificationValue = identificationValue;
    }

    public String getApplicationId() {
        return applicationId == null ? StringUtils.EMPTY : applicationId ;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof MessageCorrespondent) {
            MessageCorrespondent other = (MessageCorrespondent) o;

            return Objects.equals(identificationType, other.identificationType) //
                    && Objects.equals(identificationValue, other.identificationValue) //
                    && Objects.equals(applicationId, other.applicationId);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(//
                this.identificationType, //
                this.identificationValue, //
                this.applicationId);
    }

    @Override
    public String toString() {
        return MessageFormat.format("[identificationType = {0}, identificationValue = {1}, applicationId = {2}]",
                identificationType, identificationValue, applicationId);
    }
}
