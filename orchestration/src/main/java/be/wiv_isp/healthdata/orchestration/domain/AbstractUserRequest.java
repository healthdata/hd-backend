/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@MappedSuperclass
abstract public class AbstractUserRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "USER_REQUEST_ID", nullable = false)
    protected Long id;

    @Column(name = "USERNAME", nullable = false)
    protected String username;

    @Column(name = "FIRST_NAME", nullable = false)
    protected String firstName;

    @Column(name = "LAST_NAME", nullable = false)
    protected String lastName;

    @Column(name = "EMAIL", nullable = false)
    protected String email;

    @Column(name = "EMAIL_REQUESTER")
    protected String emailRequester;

    @Column(name = "APPROVED")
    protected Boolean approved;

    @Column(name = "CREATED_ON", nullable = false)
    protected Timestamp createdOn;

    @Column(name = "UPDATED_ON", nullable = false)
    protected Timestamp updatedOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailRequester() {
        return emailRequester;
    }

    public void setEmailRequester(String emailRequester) {
        this.emailRequester = emailRequester;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @PrePersist
    public void onCreate() {
        Timestamp createdOnTimestamp = new Timestamp(new Date().getTime());
        setCreatedOn(createdOnTimestamp);
        setUpdatedOn(createdOnTimestamp);
    }

    @PreUpdate
    public void onUpdate() {
        setUpdatedOn(new Timestamp(new Date().getTime()));
    }
}
