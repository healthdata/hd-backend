/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.common.util.StringUtils;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "COMMENTS")
@NamedQuery(name = "commentsByDocumentId",
        query = "Select c from Comment c, Note n where c.noteId = n.id and c.deleted = false and n.documentId = :documentId")
public class Comment implements Comparable<Comment>{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @Column(name = "NOTE_ID", nullable = false)
    private Long noteId;
    @Column(name = "NOTE_ORDER", nullable = false)
    private Long order;
    @Column(name = "USERNAME", nullable = false)
    private String username;
    @Column(name = "CONTENT", nullable = false)
    private String content;
    @Enumerated(EnumType.STRING)
    @Column(name = "PLATFORM", nullable = false)
    private Platform platform;
    @Column(name = "DELETED", nullable = false)
    private Boolean deleted;
    @Column(name = "CREATED_ON", nullable = false)
    private Timestamp createdOn;
    @Column(name = "UPDATED_ON", nullable = false)
    private Timestamp updatedOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNoteId() {
        return noteId;
    }

    public void setNoteId(Long noteId) {
        this.noteId = noteId;
    }

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Platform getPlatform() {
        return platform;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @PrePersist
    public void onCreate() {
        Timestamp createdOnTimestamp = new Timestamp(new Date().getTime());
        if(createdOn == null) {
            setCreatedOn(createdOnTimestamp);
        }
        setUpdatedOn(createdOnTimestamp);
    }

    @PreUpdate
    public void onUpdate() {
        setUpdatedOn(new Timestamp(new Date().getTime()));
    }


    @Override
    public boolean equals(Object other) {
        if(this == other) {
            return true;
        }

        if(other instanceof Comment) {
            Comment o = (Comment) other;

            return Objects.equals(noteId, o.noteId)
                    && Objects.equals(order, o.order);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(noteId,
                order);
    }

    @Override
    public String toString() {
        return StringUtils.toString(this);
    }

    @Override
    public int compareTo(Comment o) {
        return (int) (o.order - order);
    }
}
