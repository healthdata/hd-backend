/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.tasks;

import be.wiv_isp.healthdata.common.domain.DateRange;
import be.wiv_isp.healthdata.common.domain.search.Field;
import be.wiv_isp.healthdata.common.domain.search.Ordering;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.domain.enumeration.TemplateKey;
import be.wiv_isp.healthdata.common.domain.enumeration.TimeUnit;
import be.wiv_isp.healthdata.orchestration.api.uri.DocumentUrl;
import be.wiv_isp.healthdata.orchestration.dao.IUserDao;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.UserConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.mapper.BooleanConfigurationMapper;
import be.wiv_isp.healthdata.orchestration.domain.mapper.PeriodExpressionConfigurationMapper;
import be.wiv_isp.healthdata.orchestration.domain.mapper.WorkflowStatusMapper;
import be.wiv_isp.healthdata.orchestration.domain.search.AbstractRegistrationWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.domain.search.PendingRegistrationsViewSearch;
import be.wiv_isp.healthdata.orchestration.domain.search.UserConfigurationSearch;
import be.wiv_isp.healthdata.orchestration.domain.view.PendingRegistrationsView;
import be.wiv_isp.healthdata.orchestration.domain.mail.RegistrationUpdatesContext;
import be.wiv_isp.healthdata.orchestration.service.IAbstractRegistrationWorkflowService;
import be.wiv_isp.healthdata.orchestration.service.IMailService;
import be.wiv_isp.healthdata.orchestration.service.IPendingRegistrationService;
import be.wiv_isp.healthdata.orchestration.service.IUserConfigurationService;
import be.wiv_isp.healthdata.orchestration.util.DateUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

public abstract class AbstractPendingRegistrationsNotificationTask extends HealthDataTask {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractPendingRegistrationsNotificationTask.class);

    @Autowired
    private IAbstractRegistrationWorkflowService<? extends AbstractRegistrationWorkflow, ? extends AbstractRegistrationWorkflowSearch> workflowService;

    @Autowired
    private IMailService mailService;

    @Autowired
    private IPendingRegistrationService pendingRegistrationService;

    @Autowired
    private IUserDao userDao;

    @Autowired
    private IUserConfigurationService userConfigurationService;

    @Autowired
    @Qualifier("transactionManager")
    protected PlatformTransactionManager txManager;

    @Override
    public String getName() {
        return "PENDING_REGISTRATIONS_NOTIFICATION_TASK";
    }

    @Override
    protected void execute() {
        final Boolean registrationsNotificationsEnabled = BooleanConfigurationMapper.map(configurationService.get(ConfigurationKey.REGISTRATIONS_EMAIL_NOTIFICATION_ENABLED));
        LOG.debug("Registrations notification enabled: {}", registrationsNotificationsEnabled);
        if(registrationsNotificationsEnabled) {
            notifyPendingRegistrations();
        }
    }

    private void notifyPendingRegistrations() {
        final List<Long> userIds = pendingRegistrationService.getUserIds();
        LOG.debug("{} users found with pending registrations", userIds.size());
        for (final Long userId : userIds) {
            try {
                final User user = userDao.get(userId);
                if (user == null) {
                    LOG.error("No user found with id {}. Skipping user.", userId);
                    continue;
                }
                if (!user.isUser()) {
                    LOG.debug("User has not authority {}, Skipping user.", Authority.USER);
                    continue;
                }
                LOG.debug("Checking whether user {} must be notified", user.toStringCompact());
                final StringBuilder reason = new StringBuilder();
                final boolean userMustBeNotified = userMustBeNotified(user, reason);
                LOG.debug("User {} must be notified: {}. Reason: {}", user.toStringCompact(), userMustBeNotified, reason);
                if (userMustBeNotified) {
                    notifyPendingRegistrations(user);
                }
            } catch (Exception e) {
                LOG.error(MessageFormat.format("Error while notifying user: {0}", e.getMessage()), e);
            }
        }
    }

    private boolean userMustBeNotified(final User user, final StringBuilder reason) {
        // ************************** RULE 1 **************************
        int lowerBoundForImmediateNotification = getLowerBoundForImmediateNotification(user);
        final PeriodExpression newRegistrationPeriod = getNewRegistrationPeriod(user);

        final PendingRegistrationsViewSearch searchRecent = new PendingRegistrationsViewSearch();
        searchRecent.setUserId(user.getId());
        searchRecent.setOpenSinceRange(new DateRange(DateUtils.remove(new Date(), newRegistrationPeriod), null));
        final List<PendingRegistrationsView> recentPendingRegistrations = pendingRegistrationService.getAll(searchRecent);

        if(recentPendingRegistrations.size() >= lowerBoundForImmediateNotification) {
            reason.append(MessageFormat.format("The number of new registrations ({0}) is higher than the lower bound for immediate notification ({1}).", recentPendingRegistrations.size(), lowerBoundForImmediateNotification));
            return true;
        }

        // ************************** RULE 2 **************************
        final Date lastNotificationTime = getLastNotificationTime(user);
        final PeriodExpression remindersFrequency = getRemindersFrequency(user);
        final Date reminderFrequencyUpperBound = DateUtils.remove(new Date(), remindersFrequency);
        if(lastNotificationTime != null && lastNotificationTime.after(reminderFrequencyUpperBound)) {
            reason.append(MessageFormat.format("User was last notified on {0} (less than {1} ago)", DateFormat.DATE_AND_TIME.format(lastNotificationTime), remindersFrequency));
            return false;
        }

        final PendingRegistrationsView oldestPendingRegistration = getOldestPendingRegistration(user);

        if(oldestPendingRegistration == null) {
            reason.append(MessageFormat.format("No pending registrations found for user {}", user.toStringCompact()));
            return false;
        }

        final PeriodExpression oldRegistrationPeriod = getOldRegistrationPeriod(user);
        final Date oldRegistrationUpperBound = DateUtils.remove(new Date(), oldRegistrationPeriod);

        if(oldestPendingRegistration.getOpenSince().after(oldRegistrationUpperBound)) {
            reason.append(MessageFormat.format("There are no old pending registrations (open since more than {0})", oldRegistrationPeriod));
            return false;
        }

        if(lastNotificationTime == null) {
            reason.append(MessageFormat.format("There are old pending registrations (open since more than {0}) and user has never been notified", oldRegistrationPeriod));
        } else {
            reason.append(MessageFormat.format("There are old pending registrations (open since more than {0}) and user was last notified on {1} (more than {2} ago)", oldRegistrationPeriod, DateFormat.DATE_AND_TIME.format(lastNotificationTime), remindersFrequency));
        }
        return true;
    }

    private int getLowerBoundForImmediateNotification(User user) {
        final UserConfigurationSearch search = new UserConfigurationSearch();
        search.setUserId(user.getId());
        search.setKey(UserConfigurationKey.LOWER_BOUND_FOR_IMMEDIATE_NOTIFICATION);
        final UserConfiguration configuration = userConfigurationService.getUnique(search);
        return Integer.valueOf(configuration.getValue());
    }

    private PeriodExpression getNewRegistrationPeriod(User user) {
        final UserConfigurationSearch search = new UserConfigurationSearch();
        search.setUserId(user.getId());
        search.setKey(UserConfigurationKey.NEW_REGISTRATION_PERIOD);
        final UserConfiguration configuration = userConfigurationService.getUnique(search);
        return PeriodExpressionConfigurationMapper.map(configuration, TimeUnit.DAY);
    }

    private PeriodExpression getOldRegistrationPeriod(User user) {
        final UserConfigurationSearch search = new UserConfigurationSearch();
        search.setUserId(user.getId());
        search.setKey(UserConfigurationKey.OLD_REGISTRATION_PERIOD);
        final UserConfiguration configuration = userConfigurationService.getUnique(search);
        return PeriodExpressionConfigurationMapper.map(configuration, TimeUnit.DAY);
    }

    private PeriodExpression getRemindersFrequency(User user) {
        final UserConfigurationSearch search = new UserConfigurationSearch();
        search.setUserId(user.getId());
        search.setKey(UserConfigurationKey.REMINDERS_FREQUENCY);
        final UserConfiguration configuration = userConfigurationService.getUnique(search);
        return PeriodExpressionConfigurationMapper.map(configuration, TimeUnit.DAY);
    }

    private PendingRegistrationsView getOldestPendingRegistration(User user) {
        final PendingRegistrationsViewSearch searchOldest = new PendingRegistrationsViewSearch();
        searchOldest.setUserId(user.getId());
        searchOldest.setMaxResults(1);
        searchOldest.setOrdering(new Ordering(Field.PendingRegistration.OPEN_SINCE, Ordering.Type.ASC));
        return pendingRegistrationService.getUnique(searchOldest);
    }

    private void notifyPendingRegistrations(final User user) {
        LOG.info("Notifying user {}", user.toStringCompact());

        if(user.getEmail() == null) {
            LOG.warn("User {} can not be notified since it has no email address.", user.toStringCompact());
            return;
        }

        final PendingRegistrationsViewSearch search = new PendingRegistrationsViewSearch();
        search.setUserId(user.getId());
        final List<PendingRegistrationsView> all = pendingRegistrationService.getAll(search);

        if(CollectionUtils.isEmpty(all)) {
            LOG.warn("No pending registrations found. User will not be notified");
            return;
        }

        final RegistrationUpdatesContext context = new RegistrationUpdatesContext();
        context.setInstallationUrl(configurationService.get(ConfigurationKey.GUI_HOST).getValue());
        for (PendingRegistrationsView pendingRegistrationsView : all) {
            final AbstractRegistrationWorkflow workflow = workflowService.get(pendingRegistrationsView.getWorkflowId());

            final RegistrationUpdatesContext.RegistrationDetails details = new RegistrationUpdatesContext.RegistrationDetails();
            details.setId(workflow.getId());
            details.setLink(createDocumentUrl(workflow.getId()));
            details.setStatuses(WorkflowStatusMapper.map(pendingRegistrationsView.getStatus(), workflow.getFlags()));
            details.setOpenSince(DateFormat.DDMMYYYY.format(pendingRegistrationsView.getOpenSince()));
            context.add(workflow.getDataCollectionName(), details);
        }

        mailService.createAndSendMail(getRegistrationUpdatesTemplateKey(), context, user.getEmail());
        setLastNotificationTime(user, new Date());
    }

    private Date getLastNotificationTime(final User user) {
        final String lastNotificationTimeString = user.getMetaData().get(User.MetadataKey.LAST_NOTIFICATION_TIME);
        if(lastNotificationTimeString == null) {
            return null;
        }
        return DateFormat.DATE_AND_TIME.parse(lastNotificationTimeString);
    }

    private void setLastNotificationTime(final User user, final Date date) {
        LOG.debug("Updating last notification time for user {} to {}", user.toStringCompact(), DateFormat.DATE_AND_TIME.format(date));

        TransactionTemplate tmpl = new TransactionTemplate(txManager);
        tmpl.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                user.getMetaData().put(User.MetadataKey.LAST_NOTIFICATION_TIME, DateFormat.DATE_AND_TIME.format(date));
                userDao.update(user);
            }
        });
    }

    private String createDocumentUrl(Long workflowId) {
        return DocumentUrl.createUrl(configurationService.get(ConfigurationKey.GUI_HOST).getValue(), workflowId);
    }

    protected abstract TemplateKey getRegistrationUpdatesTemplateKey();
}
