/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.util;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileSystemUtils {

	private static final Logger LOG = LoggerFactory.getLogger(FileSystemUtils.class);

	private static final String NEW_FILE_PATTERN = "{0} ({1}).{2}";
	private static final String TEMP_FILE_EXTENSION = "TMP";

	public static void moveFilesToDirectory(final File destDir, boolean useDaySubfolder, File... files) {
		File destinationDirectory = destDir;

		if (useDaySubfolder) {
			destinationDirectory = createDaySubfolder(destDir);
		}

		boolean allUnique = true;

		for (final File file : files) {
			if (Paths.get(destinationDirectory.getAbsolutePath(), file.getName()).toFile().exists()) {
				allUnique = false;
				break;
			}
		}

		if (allUnique) {
			moveUniqueFilesToDirectory(destinationDirectory, files);
		} else {
			final File uniqueFolder = createUniqueFolder(destinationDirectory);
			moveUniqueFilesToDirectory(uniqueFolder, files);
		}
	}

	public static File getDaySubFolderUnique(final File destDir, String fileName) {
		File destinationDirectory = createDaySubfolder(destDir);
		if (!destinationDirectory.exists()) {
		}
		if (Paths.get(destinationDirectory.getAbsolutePath(), fileName).toFile().exists()) {
			destinationDirectory = createUniqueFolder(destinationDirectory);
		}
		return destinationDirectory;
	}

	public static void writeStringToFile(final File file, final String data) {
		writeStringToFile(file, data, CharEncoding.UTF_8);
	}

	public static synchronized void writeStringToFile(final File file, final String data, final String encoding) {

		File f = file;

		final File parentFile = file.getParentFile();
		final String extension = FilenameUtils.getExtension(file.getName());
		final String basename = FilenameUtils.getBaseName(file.getName());

		int counter = 0;

		while (f.exists()) {
			counter++;
			f = new File(parentFile, MessageFormat.format(NEW_FILE_PATTERN, basename, counter, extension));
		}

		final File tmp = new File(parentFile, MessageFormat.format(NEW_FILE_PATTERN, basename, counter, TEMP_FILE_EXTENSION));

		try {
			FileUtils.writeStringToFile(tmp, data, encoding);
			FileUtils.moveFile(tmp, f);
		} catch (IOException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.ERROR_WHILE_WRITING_TO_FILE, e.getMessage());
			throw exception;
		}

		LOG.debug(MessageFormat.format("File written to {0}", f.getAbsolutePath()));
	}

	private static void moveUniqueFilesToDirectory(File uniqueFolder, File[] files) {
		for (final File file : files) {
			try {
				FileUtils.moveFileToDirectory(file, uniqueFolder, true);
			} catch (IOException e) {
				LOG.error(MessageFormat.format("error while moving file {0} to {1} : {2}", file.getName(), uniqueFolder.getAbsolutePath(), e.getMessage()));
			}
		}
	}

	private static File createDaySubfolder(File destDir) {
		return createTimestampFolder(destDir, DateFormat.DAY_TIMESTAMP.getPattern());
	}

	private static File createUniqueFolder(File destDir) {
		return createTimestampFolder(destDir, DateFormat.TIMESTAMP.getPattern());
	}

	private static File createTimestampFolder(final File destDir, final String pattern) {
		final String timestamp = new SimpleDateFormat(pattern).format(new Date());
		return Paths.get(destDir.getAbsolutePath(), timestamp).toFile();
	}
}
