/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dto;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.common.util.StringUtils;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.User;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class UserDto {

	private Long id;
	private String username;
	private String password;
	protected String firstName;
	protected String lastName;
	protected String email;
	private boolean enabled;
	private Set<Authority> authoritySet = new HashSet<>();
	private Set<String> dataCollectionNames = new HashSet<>();
	private Map<String, String> metaData = new HashMap<>();
	private Organization organization;

	public UserDto() {

	}

	public UserDto(User user) {
		setId(user.getId());
		setUsername(user.getUsername());
		setPassword(user.getPassword());
		setLastName(user.getLastName());
		setFirstName(user.getFirstName());
		setEmail(user.getEmail());
		setEnabled(user.isEnabled());
		setDataCollectionNames(user.getDataCollectionNames());
		setAuthorities(user.getAuthorities());
		setMetaData(user.getMetaData());
		setOrganization(user.getOrganization());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return this.username;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}

	@JsonIgnore // password is never returned to the client
	public String getPassword() {
		return this.password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Set<Authority> getAuthorities() {
		return authoritySet;
	}

	public void setAuthorities(Set<Authority> authoritySet) {
		this.authoritySet = authoritySet;
	}

	public Map<String, String> getMetaData() {
		return metaData;
	}

	public void setMetaData(Map<String, String> metaData) {
		this.metaData = metaData;
	}

	public Set<String> getDataCollectionNames() {
		return dataCollectionNames;
	}

	public void setDataCollectionNames(Set<String> dataCollectionNames) {
		this.dataCollectionNames = dataCollectionNames;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof UserDto))
			return false;

		UserDto userDto = (UserDto) o;

		if (enabled != userDto.enabled)
			return false;
		if (id != null ? !id.equals(userDto.id) : userDto.id != null)
			return false;
		if (username != null ? !username.equals(userDto.username) : userDto.username != null)
			return false;
		if (password != null ? !password.equals(userDto.password) : userDto.password != null)
			return false;
		if (firstName != null ? !firstName.equals(userDto.firstName) : userDto.firstName != null)
			return false;
		if (lastName != null ? !lastName.equals(userDto.lastName) : userDto.lastName != null)
			return false;
		if (email != null ? !email.equals(userDto.email) : userDto.email != null)
			return false;
		if (authoritySet != null ? !authoritySet.equals(userDto.authoritySet) : userDto.authoritySet != null)
			return false;
		if (dataCollectionNames != null ? !dataCollectionNames.equals(userDto.dataCollectionNames) : userDto.dataCollectionNames != null)
			return false;
		if (metaData != null ? !metaData.equals(userDto.metaData) : userDto.metaData != null)
			return false;
		return !(organization != null ? !organization.equals(userDto.organization) : userDto.organization != null);

	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (username != null ? username.hashCode() : 0);
		result = 31 * result + (password != null ? password.hashCode() : 0);
		result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
		result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
		result = 31 * result + (email != null ? email.hashCode() : 0);
		result = 31 * result + (enabled ? 1 : 0);
		result = 31 * result + (authoritySet != null ? authoritySet.hashCode() : 0);
		result = 31 * result + (dataCollectionNames != null ? dataCollectionNames.hashCode() : 0);
		result = 31 * result + (metaData != null ? metaData.hashCode() : 0);
		result = 31 * result + (organization != null ? organization.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return StringUtils.toString(this);
	}
}