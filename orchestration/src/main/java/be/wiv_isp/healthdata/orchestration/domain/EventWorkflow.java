/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;

import javax.persistence.*;
import java.text.MessageFormat;

@Entity
@Table(name = "EVENTS_WORKFLOWS")
@PrimaryKeyJoinColumn(name = "EVENT_ID")
public class EventWorkflow extends Event {

    @Column(name = "WORKFLOW_ID")
    private Long workflowId;

    @Column(name = "STATUS", nullable = false)
    @Enumerated(EnumType.STRING)
    protected WorkflowStatus status;

    @Embedded
    protected WorkflowFlags flags = new WorkflowFlags();

    public Long getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(Long workflowId) {
        this.workflowId = workflowId;
    }

    public WorkflowStatus getStatus() {
        return status;
    }

    public void setStatus(WorkflowStatus status) {
        this.status = status;
    }

    public WorkflowFlags getFlags() {
        return flags;
    }

    public void setFlags(WorkflowFlags flags) {
        this.flags = flags;
    }

    @Override
    public String toString() {
        return MessageFormat.format("[ type = {0}, workflowId = {1}, status = {2}, notificationTimes = {3} ]", type, workflowId, status, notificationTimesAsString());
    }
}
