/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.api.ClientVersion;
import be.wiv_isp.healthdata.common.caching.CacheManagementService;
import be.wiv_isp.healthdata.common.dto.*;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.orchestration.api.uri.DataCollectionDefinitionUri;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.Version;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionDefinitionForwardService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionGroupForwardService;
import be.wiv_isp.healthdata.orchestration.service.IWebServiceClientService;
import com.sun.jersey.api.client.GenericType;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class DataCollectionDefinitionForwardService implements IDataCollectionDefinitionForwardService {

	@Autowired
	private IDataCollectionGroupForwardService dataCollectionGroupForwardService;
	@Autowired
	private IWebServiceClientService webServiceClientService;

	@Autowired
	private IConfigurationService configurationService;

	private static final Logger LOG = LoggerFactory.getLogger(DataCollectionDefinitionForwardService.class);

	@Override
	@Cacheable(value = CacheManagementService.catalogueCache)
	public DataCollectionDefinitionDtoV7 get(Long dataCollectionDefinitionId) {
		LOG.info("Getting data collection definition with id " + dataCollectionDefinitionId);

		Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

		final DataCollectionDefinitionUri dataCollectionDefinitionUri = new DataCollectionDefinitionUri();
		dataCollectionDefinitionUri.setHost(catalogueHost.getValue());
		dataCollectionDefinitionUri.setId(dataCollectionDefinitionId);

		WebServiceBuilder wsb = new WebServiceBuilder();
		wsb.setUrl(dataCollectionDefinitionUri.toString());
		wsb.addHeader(HttpHeaders.CLIENT_VERSION, getClientVersion());
		wsb.setGet(true);
		wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb.setReturnType(new GenericType<DataCollectionDefinitionDtoV7>() {});
		wsb.setLogResponseBody(false);

		return (DataCollectionDefinitionDtoV7) webServiceClientService.callWebService(wsb);
	}

	@Override
	@Cacheable(value = CacheManagementService.catalogueCache)
	public String getDataCollectionName(Long dataCollectionDefinitionId) {
		DataCollectionDefinitionDtoV7 dataCollectionDefinitionDtoV7 = get(dataCollectionDefinitionId);
		if(dataCollectionDefinitionDtoV7 == null) {
			return null;
		}
		return dataCollectionDefinitionDtoV7.getDataCollectionName();
	}

	@Override
	public DataCollectionDefinitionDtoV7 get(String dataCollectionName, Version version) {
		LOG.info("Getting data collection definition with data collection name {} and version {}.", dataCollectionName, version.toStringCompact());

		Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

		final DataCollectionDefinitionUri dataCollectionDefinitionUri = new DataCollectionDefinitionUri();
		dataCollectionDefinitionUri.setHost(catalogueHost.getValue());

		WebServiceBuilder wsb = new WebServiceBuilder();
		wsb.setUrl(dataCollectionDefinitionUri.toString());
		wsb.addHeader(HttpHeaders.CLIENT_VERSION, getClientVersion());
		wsb.addParameter("version", version.getMajor() + "." + version.getMinor());
		wsb.setGet(true);
		wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb.setReturnType(new GenericType<List<DataCollectionDefinitionDtoV7>>() {});
		wsb.setLogResponseBody(false);

		final List<DataCollectionDefinitionDtoV7> dtos = (List<DataCollectionDefinitionDtoV7>) webServiceClientService.callWebService(wsb);

		if(CollectionUtils.isEmpty(dtos)) {
			return null;
		}

		// TODO: verify result size = 1

		final DataCollectionDefinitionDtoV7 dto = dtos.get(0);
		// TODO: verify data collection name and version

		return dto;
	}

	@Override
	@Cacheable(value = CacheManagementService.catalogueCache)
	public DataCollectionDefinitionDtoV7 get(Long dataCollectionDefinitionId, Organization organization) {
		LOG.info("Getting data collection definition with id " + dataCollectionDefinitionId);

		Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

		final DataCollectionDefinitionUri dataCollectionDefinitionUri = new DataCollectionDefinitionUri();
		dataCollectionDefinitionUri.setHost(catalogueHost.getValue());
		dataCollectionDefinitionUri.setId(dataCollectionDefinitionId);

		WebServiceBuilder wsb = new WebServiceBuilder();
		wsb.setUrl(dataCollectionDefinitionUri.toString());
		wsb.addHeader(HttpHeaders.CLIENT_VERSION, getClientVersion());
		wsb.addParameter(RestUtils.ParameterName.IDENTIFICATION_TYPE, organization.getHealthDataIDType());
		wsb.addParameter(RestUtils.ParameterName.IDENTIFICATION_VALUE, organization.getHealthDataIDValue());
		wsb.setGet(true);
		wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb.setReturnType(new GenericType<DataCollectionDefinitionDtoV7>() {});
		wsb.setLogResponseBody(false);

		return (DataCollectionDefinitionDtoV7) webServiceClientService.callWebService(wsb);
	}

	@Override
	@Cacheable(value = CacheManagementService.catalogueCache)
	public DataCollectionDefinitionDtoV7 getValidForCreation(String dataCollectionName, Organization organization) {
		Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

		final DataCollectionDefinitionUri dataCollectionDefinitionUri = new DataCollectionDefinitionUri();
		dataCollectionDefinitionUri.setHost(catalogueHost.getValue());

		SimpleDateFormat sdf = new SimpleDateFormat(DateFormat.DATE.getPattern());

		WebServiceBuilder wsb = new WebServiceBuilder();
		wsb.setUrl(dataCollectionDefinitionUri.toString());
		wsb.addHeader(HttpHeaders.CLIENT_VERSION, getClientVersion());
		wsb.setGet(true);
		wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb.addParameter(RestUtils.ParameterName.IDENTIFICATION_TYPE, organization.getHealthDataIDType());
		wsb.addParameter(RestUtils.ParameterName.IDENTIFICATION_VALUE, organization.getHealthDataIDValue());
		wsb.addParameter(RestUtils.ParameterName.DATA_COLLECTION_NAME, dataCollectionName);
		wsb.addParameter("validForCreation", sdf.format(new Date()));

		wsb.setReturnType(new GenericType<List<DataCollectionDefinitionDtoV7>>() {});
		wsb.setLogResponseBody(false);

		@SuppressWarnings("unchecked")
		List<DataCollectionDefinitionDtoV7> dataCollectionDefinitions = (List<DataCollectionDefinitionDtoV7>) webServiceClientService.callWebService(wsb);
		if (dataCollectionDefinitions.isEmpty()) {
			return null;
		} else {
			return dataCollectionDefinitions.get(0);
		}
	}

	@Override
	@Cacheable(value = CacheManagementService.catalogueCache)
	public DataCollectionDefinitionDtoV7 getLatest(String dataCollectionName, Organization organization) {
		DataCollectionGroupListMap listMap = dataCollectionGroupForwardService.list(organization);
		DataCollectionGroupListDto last = listMap.getLastByDataCollectionName(dataCollectionName);
		DataCollectionDefinitionListDto dataCollectionDefinitionListDto = last.get(dataCollectionName);
		return get(dataCollectionDefinitionListDto.getDataCollectionDefinitionId());
	}

	protected String getClientVersion() {
		return ClientVersion.DataCollectionDefinition.getDefault();
	}
}
