/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.security.ldap;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.service.IOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.support.BaseLdapPathContextSource;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.ldap.search.FilterBasedLdapUserSearch;
import org.springframework.security.ldap.search.LdapUserSearch;

public class OrganizationFilterBasedLdapUserSearch implements LdapUserSearch {

	@Autowired
	private IOrganizationService organizationService;

	@Autowired
	private ILdapConfiguration ldapConfiguration;

	private final BaseLdapPathContextSource contextSource;

	public OrganizationFilterBasedLdapUserSearch(BaseLdapPathContextSource contextSource) {
		this.contextSource = contextSource;
	}

	public DirContextOperations searchForUser(String username, Long organizationId) {
		final Organization organization = organizationService.getOrganizationOrMain(organizationId);
		final String searchBase = ldapConfiguration.getUserSearchBase(organization);
		final String searchFilter = ldapConfiguration.getUserSearchFilter(organization);
		final FilterBasedLdapUserSearch userSearch = new FilterBasedLdapUserSearch(searchBase, searchFilter, contextSource);
		return userSearch.searchForUser(username);
	}

	@Override
	public DirContextOperations searchForUser(String username) throws UsernameNotFoundException {
		return searchForUser(username, null);
	}
}
