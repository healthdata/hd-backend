/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import be.wiv_isp.healthdata.orchestration.dao.IAboutDao;
import be.wiv_isp.healthdata.orchestration.domain.About;
import be.wiv_isp.healthdata.common.dao.impl.CrudDao;

@Repository
public class AboutDao extends CrudDao<About, Long> implements IAboutDao {

	public AboutDao() {
		super(About.class);
	}

	@Override
	public List<About> getAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<About> cq = cb.createQuery(About.class);
		Root<About> rootEntry = cq.from(About.class);
		CriteriaQuery<About> all = cq.select(rootEntry);
		TypedQuery<About> allQuery = em.createQuery(all);
		return allQuery.getResultList();
	}

	@Override
	public About get(String key) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<About> cq = cb.createQuery(About.class);
		Root<About> rootEntry = cq.from(About.class);
		CriteriaQuery<About> all = cq.select(rootEntry);

		List<Predicate> predicates = new ArrayList<>();
		if (key != null) {
			predicates.add(cb.equal(rootEntry.get("key"), key));
		}
		if (!predicates.isEmpty()) {
			cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		}
		TypedQuery<About> allQuery = em.createQuery(all);
		return allQuery.getSingleResult();
	}
}
