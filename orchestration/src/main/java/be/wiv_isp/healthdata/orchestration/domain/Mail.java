/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.common.util.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "MAILS")
public class Mail {

    public enum Status {
        NEW, SENT, SENT_WITH_ERROR, ERROR
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "FROM_EMAIL")
    private String from;

    @Fetch(FetchMode.JOIN)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "MAIL_TO", joinColumns = @JoinColumn(name = "MAIL_ID"))
    @Column(name = "EMAIL", nullable = false)
    private Set<String> to;

    @Column(name = "SUBJECT")
    private String subject;

    @Column(name = "CONTENT", nullable = false, length = 4000)
    private String content;

    @Column(name = "STATUS", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "ERROR", length = 4000)
    private String error;

    @Column(name = "CREATED_ON", nullable = false)
    private Timestamp createdOn;

    @Column(name = "UPDATED_ON", nullable = false)
    private Timestamp updatedOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Set<String> getTo() {
        return to;
    }

    public void setTo(Set<String> to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content.substring(0, Math.min(4000, content.length()));
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error.substring(0, Math.min(4000, error.length()));
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @PrePersist
    public void onCreate() {
        Timestamp createdOn = new Timestamp(new Date().getTime());
        setCreatedOn(createdOn);
        setUpdatedOn(createdOn);
    }

    @PreUpdate
    public void onUpdate() {
        setUpdatedOn(new Timestamp(new Date().getTime()));
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof Mail) {
            Mail other = (Mail) o;

            return Objects.equals(id, other.id)
                    && Objects.equals(from, other.from)
                    && Objects.equals(to, other.to)
                    && Objects.equals(subject, other.subject);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                this.id,
                this.from,
                this.to,
                this.subject);
    }

    @Override
    public String toString() {
        return StringUtils.toString(this);
    }
}
