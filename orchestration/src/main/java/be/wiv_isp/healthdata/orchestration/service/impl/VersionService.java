/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.orchestration.api.VersionRestService;
import be.wiv_isp.healthdata.orchestration.dto.VersionDetailsDto;
import be.wiv_isp.healthdata.orchestration.service.IVersionService;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StreamUtils;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public abstract class VersionService implements IVersionService {

	private static final Logger LOG = LoggerFactory.getLogger(VersionRestService.class);

	private String frontendVersion;
	private String backendVersion;

	@Autowired
	private IConfigurationService configurationService;

	@Override
	public VersionDetailsDto getVersionDetails() {
		final VersionDetailsDto versionDetails = new VersionDetailsDto();
		final String versionLabel = getVersionLabel();
		versionDetails.setVersion(versionLabel);
		versionDetails.setFrontendCommit(checkFrontendVersion());
		versionDetails.setBackendCommit(checkBackendVersion());
		versionDetails.setMappingCommit(checkMappingVersion());
		return versionDetails;
	}

	protected abstract String getVersionLabel();

	private String checkFrontendVersion() {
		if (frontendVersion != null)
			return frontendVersion;
		try {
			ClientConfig config = new DefaultClientConfig();
			config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
			Client client = Client.create(config);
			WebResource wr = client.resource(configurationService.get(ConfigurationKey.GUI_HOST).getValue() + "/commit.txt");

			String commitString = wr.get(String.class);
			frontendVersion = commitString.replace("\n", "");
		} catch (Exception e) {
			LOG.error("Couldn't get the version for frontend: ", e.getMessage());
			LOG.trace("Couldn't get the version for frontend: ", e);
			frontendVersion = "Couldn't get the version";
		}
		return frontendVersion;
	}

	private String checkBackendVersion() {
		if (backendVersion != null)
			return backendVersion;
		try {
			InputStream commitIS = VersionRestService.class.getResourceAsStream("/commit.txt");
			String commitString = StreamUtils.copyToString(commitIS, StandardCharsets.UTF_8);
			backendVersion = commitString.replace("\n", "");
		} catch (Exception e) {
			LOG.error("Couldn't get the version for backend: ", e.getMessage());
			LOG.trace("Couldn't get the version for backend: ", e);
			backendVersion = "Couldn't get the version";
		}
		return backendVersion;
	}

	private String checkMappingVersion() {
		final String mappingHost = configurationService.get(ConfigurationKey.MAPPING_HOST).getValue();
		final String url = mappingHost + "/version";
		try {
			ClientConfig config = new DefaultClientConfig();
			config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
			Client client = Client.create(config);
			WebResource wr = client.resource(url);

			JSONObject response = wr.get(JSONObject.class);
			return JsonUtils.getString(response, "commit");
		} catch (Exception e) {
			LOG.error("Couldn't get the version for url: " + url, e.getMessage());
			LOG.trace("Couldn't get the version for url: " + url, e);
			return "Couldn't get the version";
		}
	}

}
