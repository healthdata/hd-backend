/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.search.IAbstractWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.mapping.IMappingContextFactory;
import be.wiv_isp.healthdata.orchestration.service.IAbstractRegistrationWorkflowService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionDefinitionForwardService;
import be.wiv_isp.healthdata.orchestration.service.IDownloadService;
import be.wiv_isp.healthdata.orchestration.service.IMappingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class DownloadService implements IDownloadService {

	private static final Logger LOG = LoggerFactory.getLogger(DownloadService.class);

	@Autowired
	private IAbstractRegistrationWorkflowService workflowService;
	@Autowired
	private IDataCollectionDefinitionForwardService catalogueService;
	@Autowired
	private IMappingService mappingService;
	@Autowired
	private IMappingContextFactory mappingContextFactory;

	@Override
	public String download(Long dataCollectionDefinitionId, Organization organization) {
		return download(dataCollectionDefinitionId, organization, false, false);
	}

	@Override
	public String download(Long dataCollectionDefinitionId, Organization organization, boolean stable, boolean deleted) {
		LOG.info("Downloading dcd [{}], organization [{}], stable [{}], deleted [{}]", dataCollectionDefinitionId, organization, stable, deleted);
		long start = new Date().getTime();
		IAbstractWorkflowSearch search = workflowService.getSearchInstance();
		search.setDataCollectionDefinitionId(dataCollectionDefinitionId);
		search.setOrganization(organization);
		search.setReturnDeleted(deleted);
		List<AbstractRegistrationWorkflow> all = workflowService.getAll(search);

		final DataCollectionDefinitionDtoV7 dcd = catalogueService.get(dataCollectionDefinitionId);


		List<StringReader> readers = new ArrayList<>();
		for (AbstractRegistrationWorkflow workflow : all) {
			String csv = mappingService.mapFromJsonToCsv(workflow, mappingContextFactory.create(workflow), dcd.getContent(), stable);
			readers.add(new StringReader(csv));
		}
		long mapping = new Date().getTime();
		StringBuilder sb = new StringBuilder();
		try {
			CsvService.merge(sb, readers.toArray(new StringReader[readers.size()]));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long merge = new Date().getTime();
		LOG.info("Mapping time = " + (mapping - start));
		LOG.info("Merge time = " + (merge - mapping));
		return sb.toString();
	}
}
