/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.orchestration.domain.AbstractDocument;
import be.wiv_isp.healthdata.orchestration.domain.AbstractWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.Note;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.domain.mapper.NoteMapper;
import be.wiv_isp.healthdata.orchestration.domain.search.IAbstractWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.dto.NoteDto;
import be.wiv_isp.healthdata.orchestration.jaxb.json.JsonUnmarshaller;
import be.wiv_isp.healthdata.orchestration.service.*;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Date;
import java.util.List;

@Component
@Path("/notes")
public class NoteRestService {
	
	@Autowired
	private INoteService noteService;
	@Autowired
	private IUserDataCollectionService userDataCollectionService;
	@Autowired
	private IAbstractRegistrationWorkflowService registrationWorkflowService;
	@Autowired
	private IAbstractParticipationWorkflowService participationWorkflowService;
	@Autowired
	private IAbstractRegistrationDocumentService registrationDocumentService;
	@Autowired
	private IAbstractParticipationDocumentService participationDocumentService;
	@Autowired
	private IDataCollectionDefinitionForwardService catalogueService;
	@Autowired
	private IElasticSearchService elasticSearchService;


	private static final String noteIdParam = "noteId";
	private static final String workflowIdParam = "workflowId";
	private static final String workflowTypeParam = "workflowType";

	@GET
	@PreAuthorize("hasRole('ROLE_USER')")
	@Path("/{noteId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context UriInfo info) {
		Long noteId = getNoteId(info);
		Note note = noteService.getWithComments(noteId);
		if (note == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "Note", noteId);
			throw exception;
		}
		checkAuthorization(getDocument(note.getDocumentId(), note.getWorkflowType()).getWorkflow(), AuthenticationService.getUserDetails(), false);

		return Response.ok(NoteMapper.mapToNoteDto(note)).build();
	}

	@GET
	@PreAuthorize("hasRole('ROLE_USER')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(@Context UriInfo info) {
		Long workflowId = getWorkflowId(info);
		WorkflowType workflowType = getWorkflowType(info);

		final AbstractWorkflow workflow = getWorkflow(workflowId, workflowType);
		checkAuthorization(workflow, AuthenticationService.getUserDetails(), false);

		List<Note> notes = noteService.getByDocumentId(WorkflowType.REGISTRATION, workflow.getDocument().getId());
		return Response.ok(NoteMapper.mapToNoteDtos(notes)).build();
	}

	@POST
	@PreAuthorize("hasRole('ROLE_USER')")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(@Context UriInfo info, String json) {

		Long workflowId = getWorkflowId(info);
		WorkflowType workflowType = getWorkflowType(info);

		final NoteDto noteDto = JsonUnmarshaller.unmarshal(json, NoteDto.class);

		final HDUserDetails principal = AuthenticationService.getUserDetails();
		final AbstractWorkflow workflow = getWorkflow(workflowId, workflowType);
		checkAuthorization(workflow, principal, true);

		Note note = noteService.create(workflowType, workflow.getDocument(), noteDto, principal.getUsername());
		elasticSearchService.index(workflowId);

		return Response.ok(NoteMapper.mapToNoteDto(note)).build();
	}

	@PUT
	@PreAuthorize("hasRole('ROLE_USER')")
	@Path("/{noteId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(@Context UriInfo info, String json) {
		Long noteId = getNoteId(info);
		final NoteDto noteDto = JsonUnmarshaller.unmarshal(json, NoteDto.class);

		Note note = noteService.get(noteId);
		if (note == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "Note", noteId);
			throw exception;
		}

		AbstractWorkflow workflow = getDocument(note.getDocumentId(), note.getWorkflowType()).getWorkflow();

		final HDUserDetails principal = AuthenticationService.getUserDetails();
		checkAuthorization(workflow, principal, true);

		noteDto.setId(noteId);
		Note updatedNote = noteService.update(noteDto, principal.getUsername());

		elasticSearchService.index(workflow.getId());

		return Response.ok(NoteMapper.mapToNoteDto(updatedNote)).build();
	}

	@DELETE
	@PreAuthorize("hasRole('ROLE_USER')")
	@Path("/{noteId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@Context UriInfo info) {
		Long noteId = getNoteId(info);
		Note note= noteService.get(noteId);
		if (note == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "Note", noteId);
			throw exception;
		}
		AbstractWorkflow workflow = getDocument(note.getDocumentId(), note.getWorkflowType()).getWorkflow();

		final HDUserDetails principal = AuthenticationService.getUserDetails();
		checkAuthorization(workflow, principal, true);

		note.setDeleted(true);
		noteService.update(note);

		elasticSearchService.index(workflow.getId());

		return Response.noContent().build();
	}


	private void checkAuthorization(AbstractWorkflow workflow, HDUserDetails principal, boolean checkDate) {
		if (!userDataCollectionService.isUserAuthorized(principal, workflow)) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GENERAL_FORBIDDEN, "User not allowed to access this note.");
			throw exception;
		}
		if (checkDate) {
			DataCollectionDefinitionDtoV7 dataCollectionDefinition = catalogueService.get(workflow.getDataCollectionDefinitionId());
			if (!dataCollectionDefinition.getDataCollectionGroup().isValidForComments(new Date())) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.INVALID_DATE_FOR_DATA_COLLECTION_DEFINITION, workflow.getDataCollectionDefinitionId(), "comments");
				throw exception;
			}
		}
	}

	private IAbstractDocumentService getDocumentService(WorkflowType workflowType) {
		if (WorkflowType.REGISTRATION.equals(workflowType))
			return registrationDocumentService;
		else if (WorkflowType.PARTICIPATION.equals(workflowType))
			return participationDocumentService;
		return null;
	}

	private IAbstractWorkflowService<AbstractWorkflow, IAbstractWorkflowSearch> getWorkflowService(WorkflowType workflowType) {
		if (WorkflowType.REGISTRATION.equals(workflowType))
			return registrationWorkflowService;
		else if (WorkflowType.PARTICIPATION.equals(workflowType))
			return participationWorkflowService;
		return null;
	}

	private AbstractWorkflow getWorkflow(Long workflowId, WorkflowType workflowType) {
		AbstractWorkflow workflow = getWorkflowService(workflowType).get(workflowId);
		if (workflow == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "Workflow", workflowId);
			throw exception;
		}
		return workflow;
	}

	private AbstractDocument getDocument(Long documentId, WorkflowType workflowType) {
		AbstractDocument document = getDocumentService(workflowType).get(documentId);
		if (document == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "Document", documentId);
			throw exception;
		}
		return document;
	}

	private Long getNoteId(UriInfo info) {
		Long noteId;
		try {
			noteId = RestUtils.getPathLong(info, noteIdParam);
		}
		catch (NumberFormatException e) {
			noteId = null;
		}
		if (noteId == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_PARAMETER, noteIdParam);
			throw exception;
		}

		return noteId;
	}

	private Long getWorkflowId(UriInfo info) {
		Long workflowId;
		try {
			workflowId = RestUtils.getParameterLong(info, workflowIdParam);
		}
		catch (NumberFormatException e) {
			workflowId = null;
		}
		if (workflowId == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_PARAMETER, workflowIdParam);
			throw exception;
		}

		return workflowId;
	}

	private WorkflowType getWorkflowType(UriInfo info) {
		WorkflowType workflowType = WorkflowType.getValue(RestUtils.getParameterString(info, workflowTypeParam));

		if (workflowType == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_PARAMETER, workflowTypeParam);
			throw exception;
		}
		return workflowType;
	}

}
