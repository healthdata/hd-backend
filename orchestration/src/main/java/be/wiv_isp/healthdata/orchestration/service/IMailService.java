/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.common.domain.enumeration.TemplateKey;
import be.wiv_isp.healthdata.orchestration.domain.Mail;
import be.wiv_isp.healthdata.orchestration.dto.MailDto;
import be.wiv_isp.healthdata.orchestration.domain.mail.AbstractVelocityContext;

import javax.mail.Message;
import java.util.Set;

public interface IMailService {

    void create(Mail mail);
    void update(Mail mail);
    MailDto createMail(TemplateKey templateKey, AbstractVelocityContext context, String... to);
    MailDto createMail(TemplateKey templateKey, AbstractVelocityContext context, Set<String> to);
    void createAndSendMail(TemplateKey templateKey, AbstractVelocityContext context, String... to);
    void createAndSendMail(TemplateKey templateKey, AbstractVelocityContext context, Set<String> to);
    void send(MailDto mail);
    void send(Mail mail);
    void send(Mail mail, Message.RecipientType recipientType);
    boolean status();
    Set<String> getEmailAdmins(Organization organization);
    Set<String> getEmailUsers(Organization organization);
}
