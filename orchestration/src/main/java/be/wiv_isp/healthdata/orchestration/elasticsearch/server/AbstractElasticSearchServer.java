/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.elasticsearch.server;

import be.wiv_isp.healthdata.orchestration.domain.mapper.ExistingDirectoryConfigurationMapper;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;

public abstract class AbstractElasticSearchServer implements InitializingBean, DisposableBean {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractElasticSearchServer.class);

	private static boolean INITIALIZED = false;

	private Node node;

	private Client client;

	@Autowired
	private IConfigurationService configurationService;

	@Override
	public synchronized void afterPropertiesSet() {
		LOG.info("Starting ElasticSearch server");

		if (INITIALIZED) {
			LOG.warn("ElasticSearch server is already started. Not starting again.");
			return;
		}

		final ImmutableSettings.Builder settings = ImmutableSettings.settingsBuilder();

		File elasticSearchHomeDir = null;

		// do not fail to start application if elasticsearch home directory is
		// not properly configured.
		try {
			elasticSearchHomeDir = ExistingDirectoryConfigurationMapper.map(configurationService.get(ConfigurationKey.ES_PATH_HOME));
		} catch (HealthDataException e) {
			LOG.error("Invalid " + ConfigurationKey.ES_PATH_HOME + " configuration value. ElasticSearch failed to start.", e);
			INITIALIZED = true;
			return;
		}

		settings.put("node.name", this.getNodeName());
		settings.put("cluster.name", this.getClusterName());
		settings.put("path.home", elasticSearchHomeDir);
		settings.put("http.enabled", true);
		settings.put("index.number_of_shards", "1");
		settings.put("index.number_of_replicas", "0");
		settings.put("indices.memory.index_buffer_size", "512MB");

		node = new NodeBuilder().settings(settings).data(true).local(true).build().start();

		INITIALIZED = true;
	}

	protected abstract String getClusterName();

	protected abstract String getNodeName();

	@Override
	public void destroy() throws Exception {
		LOG.info("Stopping ElasticSearch server");
		node.close();
	}

	public Client getClient() {
		if (client == null) {
			client = node.client();
		}

		return client;
	}
}
