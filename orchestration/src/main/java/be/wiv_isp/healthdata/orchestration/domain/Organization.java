/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Objects;


@Entity
@Table(name = "ORGANIZATIONS")
public class Organization implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ORGANIZATION_ID")
	private Long id;

	@Column(name = "HD_ID_TYPE")
	private String healthDataIDType;

	@Column(name = "HD_ID_VALUE")
	private String healthDataIDValue;

	@Column(name = "NAME")
	private String name;

	@Column(name = "MAIN")
	private boolean main;

	@Column(name = "HD4PRC")
	private boolean hd4prc;

	@Column(name = "DELETED")
	private boolean deleted;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHealthDataIDType() {
		return healthDataIDType;
	}

	public void setHealthDataIDType(String healthDataIDType) {
		this.healthDataIDType = healthDataIDType;
	}

	public String getHealthDataIDValue() {
		return healthDataIDValue;
	}

	public void setHealthDataIDValue(String healthDataIDValue) {
		this.healthDataIDValue = healthDataIDValue;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isMain() {
		return main;
	}

	public void setMain(boolean main) {
		this.main = main;
	}

	public boolean isHd4prc() {
		return hd4prc;
	}

	public void setHd4prc(boolean hd4prc) {
		this.hd4prc = hd4prc;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof Organization) {
			Organization other = (Organization) o;

			return Objects.equals(id, other.id);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}

	@Override
	public String toString() {
		if(main) {
			return MessageFormat.format("[Main organization: {0} - {1}]", name, healthDataIDValue);
		} else {
			return MessageFormat.format("[{0} - {1}]", name, healthDataIDValue);
		}
	}
}
