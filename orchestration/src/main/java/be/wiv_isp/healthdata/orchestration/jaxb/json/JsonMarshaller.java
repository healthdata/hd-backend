/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.jaxb.json;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class JsonMarshaller  {

	private static final ObjectMapper mapper = new ObjectMapper().disable(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS);

	static public String marshal(Object object) {
		return new String(marshalToByteArray(object), StandardCharsets.UTF_8);
	}

	static public byte[] marshalToByteArray(Object object) {
		try {
			return mapper.writeValueAsBytes(object);
		} catch (IOException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.MARSHALLING_EXCEPTION, object.toString(), e.getMessage());
			throw exception;
		}
	}

	public static byte[] marshalToBytes(Object object) {
		try {
			return mapper.writeValueAsBytes(object);
		} catch (IOException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.MARSHALLING_EXCEPTION, object.toString(), e.getMessage());
			throw exception;
		}
	}
}
