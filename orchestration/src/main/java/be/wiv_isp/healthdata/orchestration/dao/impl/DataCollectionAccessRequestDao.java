/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.orchestration.dao.IDataCollectionAccessRequestDao;
import be.wiv_isp.healthdata.orchestration.domain.DataCollectionAccessRequest;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.search.DataCollectionAccessRequestSearch;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DataCollectionAccessRequestDao extends CrudDaoV2<DataCollectionAccessRequest, Long, DataCollectionAccessRequestSearch> implements IDataCollectionAccessRequestDao {

    public DataCollectionAccessRequestDao() {
        super(DataCollectionAccessRequest.class);
    }

    @Override
    protected List<Predicate> getPredicates(DataCollectionAccessRequestSearch search, CriteriaBuilder cb, Root<DataCollectionAccessRequest> rootEntry) {

        final List<Predicate> predicates = new ArrayList<>();

        // organizationId must always be provided
        predicates.add(cb.equal(rootEntry.<User>get("user").<Organization>get("organization").get("id"), search.getOrganizationId()));

        if(search.getId() != null) {
            predicates.add(cb.equal(rootEntry.get("id"), search.getId()));
        }

        if(search.getProcessed() != null) {
            if(Boolean.TRUE.equals(search.getProcessed())) {
                predicates.add(cb.isTrue(rootEntry.<Boolean>get("processed")));
            } else {
                predicates.add(cb.isFalse(rootEntry.<Boolean>get("processed")));
            }
        }

        if(search.getUserId() != null) {
            predicates.add(cb.equal(rootEntry.<User>get("user").get("id"), search.getUserId()));
        }

        return predicates;
    }
}
