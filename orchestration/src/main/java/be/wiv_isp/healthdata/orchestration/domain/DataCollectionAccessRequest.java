/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Entity
@Table(name = "DC_ACCESS_REQUESTS")
public class DataCollectionAccessRequest {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;

	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "USER_ID")
	private User user;

	@Fetch(FetchMode.JOIN)
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "DC_ACCESS_REQUESTS_DETAILS", joinColumns = @JoinColumn(name = "DC_ACCESS_REQUEST_ID"))
	@MapKeyColumn(name = "DATA_COLLECTION_NAME")
	@Column(name = "ACCEPTED")
	private Map<String, Boolean> dataCollections;

	@Column(name = "PROCESSED")
	private boolean processed;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Map<String, Boolean> getDataCollections() {
		return dataCollections;
	}

	public void setDataCollections(Map<String, Boolean> dataCollections) {
		this.dataCollections = dataCollections;
	}

	public boolean isProcessed() {
		return processed;
	}

	public void setProcessed(boolean processed) {
		this.processed = processed;
	}

	public void setDataCollections(final Set<String> dataCollections, boolean decision) {
		if(dataCollections == null) {
			return;
		}

		this.dataCollections = new HashMap<>();
		for (final String dataCollection : dataCollections) {
			this.dataCollections.put(dataCollection, decision);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof DataCollectionAccessRequest)) return false;

		DataCollectionAccessRequest request = (DataCollectionAccessRequest) o;

		if (processed != request.processed) return false;
		if (id != null ? !id.equals(request.id) : request.id != null) return false;
		if (user != null ? !user.equals(request.user) : request.user != null) return false;
		return !(dataCollections != null ? !dataCollections.equals(request.dataCollections) : request.dataCollections != null);

	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (user != null ? user.hashCode() : 0);
		result = 31 * result + (dataCollections != null ? dataCollections.hashCode() : 0);
		result = 31 * result + (processed ? 1 : 0);
		return result;
	}
}
