/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.common.dto.DataCollectionGroupDto;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupListMap;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionGroupForwardService;
import be.wiv_isp.healthdata.orchestration.service.IUserDataCollectionService;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Component
@Path("/datacollectiongroups")
public class DataCollectionGroupForwardRestService {

    @Autowired
    private IDataCollectionGroupForwardService dataCollectionGroupService;
    @Autowired
    private IUserDataCollectionService userDataCollectionService;

    @GET
    @Path("/list")
    @PreAuthorize("hasAnyRole('ROLE_USER')")
    @Produces(MediaType.APPLICATION_JSON)
    public Response list(@Context UriInfo info, @HeaderParam("Range") String range) {
        Boolean withContent = getWithContent(info);
        Boolean latestVersion = getLatestVersion(info);
        final HDUserDetails principal = AuthenticationService.getUserDetails();
        DataCollectionGroupListMap listMap = dataCollectionGroupService.list(principal.getOrganization(), withContent, latestVersion);
        DataCollectionGroupListMap result = new DataCollectionGroupListMap();
        result.putAll(listMap);
        for (String dataCollectionGroupName : listMap.keySet()) {
            if(!userDataCollectionService.isUserAuthorized(principal, dataCollectionGroupName)) {
                result.remove(dataCollectionGroupName);
            }
        }
        return Response.ok(result).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@HeaderParam("ClientVersion") String clientVersion, @PathParam("id") Long id) {
        DataCollectionGroupDto result = dataCollectionGroupService.get(id);
        if(result == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(result).build();
    }

    private Boolean getWithContent(UriInfo info) {
        return RestUtils.getParameterBoolean(info, "withContent");
    }

    private Boolean getLatestVersion(UriInfo info) {
        return RestUtils.getParameterBoolean(info, "latestVersion");
    }
}
