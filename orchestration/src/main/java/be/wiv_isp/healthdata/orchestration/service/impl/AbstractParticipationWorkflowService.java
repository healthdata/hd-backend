/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.action.workflow.executor.IParticipationWorkflowActionExecutor;
import be.wiv_isp.healthdata.orchestration.action.workflow.executor.IWorkflowActionExecutor;
import be.wiv_isp.healthdata.orchestration.dao.IParticipationWorkflowDao;
import be.wiv_isp.healthdata.orchestration.dao.IWorkflowDao;
import be.wiv_isp.healthdata.orchestration.domain.AbstractParticipationDocument;
import be.wiv_isp.healthdata.orchestration.domain.AbstractParticipationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.domain.search.AbstractParticipationWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.service.IAbstractDocumentService;
import be.wiv_isp.healthdata.orchestration.service.IAbstractParticipationDocumentService;
import be.wiv_isp.healthdata.orchestration.service.IAbstractParticipationWorkflowService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

abstract public class AbstractParticipationWorkflowService<Workflow extends AbstractParticipationWorkflow<Document>, Search extends AbstractParticipationWorkflowSearch, Document extends AbstractParticipationDocument> extends AbstractWorkflowService<Workflow, Search, Document> implements IAbstractParticipationWorkflowService<Workflow, Search> {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractParticipationWorkflowService.class);

    @Autowired
    protected IParticipationWorkflowDao<Workflow, Search> workflowDao;

    @Autowired
    private IParticipationWorkflowActionExecutor workflowActionExecutor;

    @Autowired
    private IAbstractParticipationDocumentService<Document, ?> documentService;

    public AbstractParticipationWorkflowService(Class<Workflow> clazz) {
        super(clazz);
    }

    @Override
    @Transactional
    public void updateSendStatus(Long workflowId, String sendStatus) {
        if(workflowId == null) {
            return;
        }

        LOG.info("Updating send status of workflow {} to {}", workflowId, sendStatus);

        final Workflow workflow = get(workflowId);
        workflow.setSendStatus(sendStatus);
        update(workflow);
    }

    @Override
    protected WorkflowType getWorkflowType() {
        return WorkflowType.PARTICIPATION;
    }

    @Override
    public IWorkflowDao<Workflow, Search> getDao() {
        return workflowDao;
    }

    @Override
    protected IWorkflowActionExecutor getActionExecutor() {
        return workflowActionExecutor;
    }

    @Override
    protected IAbstractDocumentService<Document, ?> getDocumentService() {
        return documentService;
    }
}