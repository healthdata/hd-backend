/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.domain.enumeration.TemplateKey;
import be.wiv_isp.healthdata.common.domain.enumeration.TimeUnit;
import be.wiv_isp.healthdata.common.exception.HealthDataAssert;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.PasswordResetToken;
import be.wiv_isp.healthdata.orchestration.domain.PeriodExpression;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.mail.PasswordResetContext;
import be.wiv_isp.healthdata.orchestration.domain.mapper.PeriodExpressionConfigurationMapper;
import be.wiv_isp.healthdata.orchestration.domain.search.PasswordResetTokenSearch;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import be.wiv_isp.healthdata.orchestration.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.core.Response;
import java.text.MessageFormat;

@Service
public class PasswordResetService implements IPasswordResetService {

    private static final Logger LOG = LoggerFactory.getLogger(PasswordResetService.class);

    @Autowired
    private IUserService userService;

    @Autowired
    private IPasswordResetTokenService passwordResetTokenService;

    @Autowired
    private IConfigurationService configurationService;

    @Autowired
    private IPlatformService platformService;

    @Autowired
    private IMailService mailService;

    @Override
    @Transactional
    public void sendPasswordResetInstructions(Organization organization, String email) {
        HealthDataAssert.assertNotNull(organization, Response.Status.BAD_REQUEST, "organization is not specified");
        HealthDataAssert.assertNotBlank(email, Response.Status.BAD_REQUEST, "email can not be empty");

        final UserSearch search = new UserSearch();
        search.setOrganization(organization);
        search.setEmail(email);
        final User user = userService.getUnique(search);
        HealthDataAssert.assertNotNull(user, Response.Status.NOT_FOUND, "no user found with address [{0}]", email);
        final PasswordResetToken token = passwordResetTokenService.generate(user);

        final PasswordResetContext context = new PasswordResetContext();
        context.setLastName(token.getUser().getLastName());
        context.setFirstName(token.getUser().getFirstName());
        context.setResetPasswordLink(buildResetPasswordLink(organization, token.getToken()));
        final PeriodExpression tokenValidityPeriod = PeriodExpressionConfigurationMapper.map(configurationService.get(ConfigurationKey.PASSWORD_RESET_TOKEN_VALIDITY_PERIOD), TimeUnit.SECOND);
        context.setValidityPeriod(tokenValidityPeriod);
        context.setSupportEmail(configurationService.get(ConfigurationKey.SUPPORT_EMAIL).getValue());
        context.setPlatform(platformService.getCurrentPlatform());

        mailService.createAndSendMail(TemplateKey.PASSWORD_RESET, context, email); // TODO-WDC-2461: make errors in this method not "silent"
    }

    @Override
    @Transactional
    public void resetPassword(Organization organization, String token, String newPassword) {
        HealthDataAssert.assertNotNull(organization, Response.Status.BAD_REQUEST, "organization is not specified");

        final PasswordResetTokenSearch search = new PasswordResetTokenSearch();
        search.setToken(token);
        final PasswordResetToken passwordResetToken = passwordResetTokenService.getUnique(search);
        HealthDataAssert.assertNotNull(passwordResetToken, Response.Status.UNAUTHORIZED, "invalid token");

        final User user = passwordResetToken.getUser();
        HealthDataAssert.assertTrue(organization.equals(user.getOrganization()), Response.Status.UNAUTHORIZED, "invalid token");
        HealthDataAssert.assertFalse(passwordResetToken.isExpired(), Response.Status.UNAUTHORIZED, "invalid token");

        userService.updatePassword(user, newPassword);
        LOG.info("Password has been reset for user with id [{}]", user.getId());
        passwordResetTokenService.delete(passwordResetToken);
    }

    private String buildResetPasswordLink(Organization organization, String token) {
        final String guiHost = configurationService.get(ConfigurationKey.GUI_HOST).getValue();
        return MessageFormat.format("{0}/#/resetPassword?organizationId={1}&token={2}", guiHost, Long.toString(organization.getId()), token);
    }
}
