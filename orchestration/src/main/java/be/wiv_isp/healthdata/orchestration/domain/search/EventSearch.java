/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.search;

import be.wiv_isp.healthdata.common.domain.search.EntitySearch;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.EventType;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

public class EventSearch extends EntitySearch {

    private List<EventType> types;
    private User user;
    private Timestamp withNotificationTimeBefore;

    public List<EventType> getTypes() {
        return types;
    }

    public void setTypes(EventType... types) {
        this.types = Arrays.asList(types);
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public Timestamp getWithNotificationTimeBefore() {
        return withNotificationTimeBefore;
    }

    public void setWithNotificationTimeBefore(Timestamp withNotificationTimeBefore) {
        this.withNotificationTimeBefore = withNotificationTimeBefore;
    }
}
