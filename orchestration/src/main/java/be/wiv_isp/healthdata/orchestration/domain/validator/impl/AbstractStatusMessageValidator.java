/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.validator.impl;

import be.wiv_isp.healthdata.orchestration.action.StatusMessageContent;
import be.wiv_isp.healthdata.orchestration.domain.AbstractStatusMessage;
import be.wiv_isp.healthdata.orchestration.domain.validator.IStatusMessageValidator;
import org.codehaus.jettison.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.Date;

public abstract class AbstractStatusMessageValidator<T extends AbstractStatusMessage> implements IStatusMessageValidator<T> {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractStatusMessageValidator.class);

    protected static final long _ONE_HOUR_AS_MILLIS = 1000L * 60L * 60L;
    protected static final long _25_HOURS_AS_MILLIS = 25 * _ONE_HOUR_AS_MILLIS;
    protected static final long _8_HOURS_AS_MILLIS = 8 * _ONE_HOUR_AS_MILLIS;

    @Override
    public void validateTimings(T statusMessage) {
        final StatusMessageContent content = statusMessage.getContent();

        try {
            content.setValidation(StatusMessageContent.CREATED_ON, StatusMessageContent.OK);

            // check creation date versus current date
            final Timestamp createdOn = statusMessage.getCreatedOn();
            final Long intervalMillis = new Date().getTime() - createdOn.getTime();
            if (intervalMillis > _25_HOURS_AS_MILLIS) {
                content.addValidationError(StatusMessageContent.CREATED_ON, "Message was received more than 25 hours ago.");
            }

            // check creation date versus sent date
            final Timestamp sentOn = statusMessage.getSentOn();
            final Long intervalMillis2 = createdOn.getTime() - sentOn.getTime();
            if (intervalMillis2 > _8_HOURS_AS_MILLIS) {
                content.addValidationError(StatusMessageContent.CREATED_ON, "Message was received more than 8 hours after sending time.");
            }
        } catch (JSONException e) {
            LOG.error("Error while checking receiving time of status message", e);
        }
    }
}
