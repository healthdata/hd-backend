/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.*;
import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.common.json.SerializableJsonObject;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.orchestration.api.mapper.MappingResponseMapper;
import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.dto.ValidationResponse;
import be.wiv_isp.healthdata.orchestration.mapping.IMappingContext;
import be.wiv_isp.healthdata.orchestration.mapping.IMappingContextFactory;
import be.wiv_isp.healthdata.orchestration.mapping.MappingRequest;
import be.wiv_isp.healthdata.orchestration.mapping.MappingResponse;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.ws.rs.core.MediaType;
import java.text.MessageFormat;

abstract public class AbstractMappingService<RegistrationWorkflow extends AbstractRegistrationWorkflow> implements IMappingService<RegistrationWorkflow> {

	@Value("${host}")
	private String host;

	private static final Logger LOG = LoggerFactory.getLogger(AbstractMappingService.class);

	private static final String EUROPEAN_CENTRAL_TIME = "ECT";
	private static final String HAS_VALIDATION_ERRORS_KEY = "has_validation_errors";

	public class Endpoint {
		public static final String EMPTY = StringUtils.EMPTY;
		public static final String IMPORT_CSV = "/importCSV";
		public static final String IMPORT_BASIC_DATASET = "/importBasicDataset";
		public static final String MERGE = "/merge";
		public static final String ELASTICSEARCH_INDEX_SETTINGS = "/elasticsearchIndexSettings";
		public static final String REFRESH = "/refresh";
	}

	@Autowired
	protected IWebServiceClientService webServiceClientService;
	@Autowired
	protected IConfigurationService configurationService;
	@Autowired
	protected IMappingContextFactory mappingContextFactory;
	@Autowired
	private IDataCollectionDefinitionForwardService catalogueService;
	@Autowired
	private IMappingSecurityService securityService;

	@Override
	public MappingResponse mapFromCsvToJson(byte[] dcdContent, String csvContent, IMappingContext context) {
		final MappingRequest request = new MappingRequest();
		request.getInput().put(MappingRequest.Input.CSV, csvContent);
		request.getInput().put(MappingRequest.Input.DATA_COLLECTION_DEFINITION, JsonUtils.createSerializableJsonObject(dcdContent));
		request.getTargets().add(MappingRequest.Target.DOCUMENT);
		request.getTargets().add(MappingRequest.Target.TO_BE_CODED);
		request.getTargets().add(MappingRequest.Target.PROGRESS);
		request.getTargets().add(MappingRequest.Target.UNIQUE_ID);
		request.getTargets().add(MappingRequest.Target.PENDING_ATTACHMENTS);
		request.getTargets().add(MappingRequest.Target.AFFECTED_FIELDS);
		request.getInput().put(MappingRequest.Input.CONTEXT, context);

		final JSONObject response = get(request, Endpoint.IMPORT_CSV);
		return MappingResponseMapper.convert(response);
	}


	@Override
	public MappingResponse mapFromXmlToJson(byte[] dcdContent, String xmlContent, IMappingContext context) {
		final MappingRequest request = new MappingRequest();
		request.getInput().put(MappingRequest.Input.BASIC_DATASET, xmlContent);
		request.getInput().put(MappingRequest.Input.DATA_COLLECTION_DEFINITION, JsonUtils.createSerializableJsonObject(dcdContent));
		request.getTargets().add(MappingRequest.Target.DOCUMENT);
		request.getTargets().add(MappingRequest.Target.TO_BE_CODED);
		request.getTargets().add(MappingRequest.Target.PROGRESS);
		request.getTargets().add(MappingRequest.Target.UNIQUE_ID);
		request.getTargets().add(MappingRequest.Target.AFFECTED_FIELDS);
		request.getInput().put(MappingRequest.Input.CONTEXT, context);

		final JSONObject response = get(request, Endpoint.IMPORT_BASIC_DATASET);
		return MappingResponseMapper.convert(response);
	}


	@Override
	public JSONObject mapToElasticSearchDocument(RegistrationWorkflow workflow) {
		final MappingRequest request = new MappingRequest();
		final DataCollectionDefinitionDtoV7 dataCollectionDefinition = catalogueService.get(workflow.getDataCollectionDefinitionId());
		request.getInput().put(MappingRequest.Input.DATA_COLLECTION_DEFINITION, JsonUtils.createSerializableJsonObject(dataCollectionDefinition.getContent()));
		request.getTargets().add(MappingRequest.Target.ES);
		final JSONObject documentObject = new SerializableJsonObject();
		putAllDocumentContent(documentObject, workflow);
		request.getInput().put(MappingRequest.Input.DOCUMENT, documentObject);
		request.getInput().put(MappingRequest.Input.CONTEXT, mappingContextFactory.create(workflow));

		final JSONObject response = get(request);
		return MappingResponseMapper.convert(response).getEsDocument();
	}

	@Override
	public JSONObject getElasticSearchMapping(Long dataCollectionDefinitionId) {
		final MappingRequest request = new MappingRequest();
		final DataCollectionDefinitionDtoV7 dataCollectionDefinition = catalogueService.get(dataCollectionDefinitionId);
		request.getTargets().add(MappingRequest.Target.ES_MAPPING);
		request.getInput().put(MappingRequest.Input.DATA_COLLECTION_DEFINITION, JsonUtils.createSerializableJsonObject(dataCollectionDefinition.getContent()));

		final JSONObject response = get(request);
		return MappingResponseMapper.convert(response).getEsMapping();
	}

	@Override
	public String mapFromJsonToCsv(byte[] documentContent, byte[] dcdContent, boolean stable) {
		final String target = stable ? MappingRequest.Target.STABLE_CSV : MappingRequest.Target.CSV;
		final MappingRequest request = new MappingRequest();
		request.getInput().put(MappingRequest.Input.DATA_COLLECTION_DEFINITION, JsonUtils.createSerializableJsonObject(dcdContent));
		request.getTargets().add(target);
		final JSONObject documentObject = new SerializableJsonObject();
		JsonUtils.put(documentObject, MappingRequest.Fields.CONTENT, JsonUtils.createJsonObject(documentContent));
		request.getInput().put(MappingRequest.Input.DOCUMENT, documentObject);
		final JSONObject response = get(request);

		if(stable) {
			return MappingResponseMapper.convert(response).getStableCsv();
		} else {
			return MappingResponseMapper.convert(response).getCsv();
		}
	}

	@Override
	public String mapFromJsonToCsv(RegistrationWorkflow workflow, IMappingContext context, byte[] dcdContent, boolean stable) {
		final String target = stable ? MappingRequest.Target.STABLE_CSV : MappingRequest.Target.CSV;
		final MappingRequest request = new MappingRequest();
		request.getInput().put(MappingRequest.Input.DATA_COLLECTION_DEFINITION, JsonUtils.createSerializableJsonObject(dcdContent));
		request.getTargets().add(target);
		final JSONObject documentObject = new SerializableJsonObject();
		putAllDocumentContent(documentObject, workflow);
		request.getInput().put(MappingRequest.Input.DOCUMENT, documentObject);
		request.getInput().put(MappingRequest.Input.CONTEXT, context);

		final JSONObject response = get(request);

		if(stable) {
			return MappingResponseMapper.convert(response).getStableCsv();
		} else {
			return MappingResponseMapper.convert(response).getCsv();
		}
	}

	@Override
	public MappingResponse merge(RegistrationWorkflow primary, RegistrationWorkflow secondary, IMappingContext context, byte[] dcdContent) {
		if(!primary.getDataCollectionDefinitionId().equals(secondary.getDataCollectionDefinitionId())) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, "trying to merge two workflows with different data collection definition");
			throw exception;
		}

		final MappingRequest request = new MappingRequest();
		request.getInput().put(MappingRequest.Input.DATA_COLLECTION_DEFINITION, JsonUtils.createSerializableJsonObject(dcdContent));
		request.getTargets().add(MappingRequest.Target.DOCUMENT);
		request.getTargets().add(MappingRequest.Target.TO_BE_CODED);
		request.getTargets().add(MappingRequest.Target.UNIQUE_ID);
		request.getTargets().add(MappingRequest.Target.AFFECTED_FIELDS);
		request.getInput().put(MappingRequest.Input.CONTEXT, context);

		final JSONObject primaryDocument = new SerializableJsonObject();
		putAllDocumentContent(primaryDocument, primary);

		final JSONObject secondaryDocument = new SerializableJsonObject();
		putAllDocumentContent(secondaryDocument, secondary);

		request.getInput().put(MappingRequest.Input.PRIMARY, primaryDocument);
		request.getInput().put(MappingRequest.Input.SECONDARY, secondaryDocument);

		try {
			final JSONObject response = get(request, Endpoint.MERGE);
			return MappingResponseMapper.convert(response);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return null;
		}
	}

	@Override
	public String getUniqueID(RegistrationWorkflow workflow) {
		final MappingRequest request = new MappingRequest();
		final DataCollectionDefinitionDtoV7 dataCollectionDefinition = catalogueService.get(workflow.getDataCollectionDefinitionId());
		request.getInput().put(MappingRequest.Input.DATA_COLLECTION_DEFINITION, JsonUtils.createSerializableJsonObject(dataCollectionDefinition.getContent()));
		request.getTargets().add(MappingRequest.Target.UNIQUE_ID);
		final JSONObject documentObject = new SerializableJsonObject();
		putAllDocumentContent(documentObject, workflow);
		request.getInput().put(MappingRequest.Input.DOCUMENT, documentObject);
		request.getInput().put(MappingRequest.Input.CONTEXT, mappingContextFactory.create(workflow));

		final JSONObject response = get(request);
		return MappingResponseMapper.convert(response).getUniqueID();
	}

	@Override
	public String getElasticSearchIndexSettings() {
		String host = configurationService.get(ConfigurationKey.MAPPING_HOST).getValue();

		WebServiceBuilder wsb = new WebServiceBuilder();
		wsb.setUrl(host + Endpoint.ELASTICSEARCH_INDEX_SETTINGS);
		wsb.setGet(true);
		wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb.setReturnType(new GenericType<String>() {});

		return (String) webServiceClientService.callWebService(wsb);
	}

	@Override
	public boolean isValid(byte[] documentContent, byte[] dcdContent) {
		final JSONObject validationObject = validate(documentContent, null, dcdContent);
		return !JsonUtils.getBoolean(validationObject, HAS_VALIDATION_ERRORS_KEY);
	}

	@Override
	public boolean isValid(RegistrationWorkflow workflow, IMappingContext context, byte[] dcdContent) {
		final JSONObject validationObject = validate(workflow, context, dcdContent);
		return !JsonUtils.getBoolean(validationObject, HAS_VALIDATION_ERRORS_KEY);
	}

	@Override
	public ValidationResponse getValidationResponse(byte[] documentContent, byte[] dcdContent) {
		JSONObject validationObject = validate(documentContent, null, dcdContent);
		try {
			final ValidationResponse response = new ValidationResponse();
			response.setResult(validationObject);
			response.setSuccess(!validationObject.getBoolean(HAS_VALIDATION_ERRORS_KEY));
			return response;
		} catch (JSONException e) {
			LOG.error(MessageFormat.format("error while retrieving {0} attribute from json ({1}):", HAS_VALIDATION_ERRORS_KEY, validationObject.toString()));
			LOG.error(ExceptionUtils.getStackTrace(e));
			return null;
		}
	}

	@Override
	public ValidationResponse getValidationResponse(RegistrationWorkflow workflow, IMappingContext context, byte[] dcdContent) {
		JSONObject validationObject = validate(workflow, context, dcdContent);
		try {
			final ValidationResponse response = new ValidationResponse();
			response.setResult(validationObject);
			response.setSuccess(!validationObject.getBoolean(HAS_VALIDATION_ERRORS_KEY));
			return response;
		} catch (JSONException e) {
			LOG.error(MessageFormat.format("error while retrieving {0} attribute from json ({1}):", HAS_VALIDATION_ERRORS_KEY, validationObject.toString()));
			LOG.error(ExceptionUtils.getStackTrace(e));
			return null;
		}
	}

	private JSONObject validate(byte[] documentContent, IMappingContext context, byte[] dcdContent) {
		final MappingRequest request = new MappingRequest();
		request.getTargets().add(MappingRequest.Target.VALIDATION);
		request.getInput().put(MappingRequest.Input.DATA_COLLECTION_DEFINITION, JsonUtils.createSerializableJsonObject(dcdContent));
		final JSONObject documentObject = new SerializableJsonObject();
		JsonUtils.put(documentObject, MappingRequest.Fields.CONTENT, JsonUtils.createJsonObject(documentContent));
		request.getInput().put(MappingRequest.Input.DOCUMENT, documentObject);
		if(context != null) {
			request.getInput().put(MappingRequest.Input.CONTEXT, context);
		}

		final JSONObject response = get(request);
		return MappingResponseMapper.convert(response).getValidation();
	}

	private JSONObject validate(RegistrationWorkflow workflow, IMappingContext context, byte[] dcdContent) {
		final MappingRequest request = new MappingRequest();
		request.getTargets().add(MappingRequest.Target.VALIDATION);
		request.getInput().put(MappingRequest.Input.DATA_COLLECTION_DEFINITION, JsonUtils.createSerializableJsonObject(dcdContent));
		final JSONObject documentObject = new SerializableJsonObject();
		putAllDocumentContent(documentObject, workflow);
		request.getInput().put(MappingRequest.Input.DOCUMENT, documentObject);
		if(context != null) {
			request.getInput().put(MappingRequest.Input.CONTEXT, context);
		}

		final JSONObject response = get(request);
		return MappingResponseMapper.convert(response).getValidation();
	}

	@Override
	public JSONObject getComputedExpressions(RegistrationWorkflow workflow) {
		final MappingRequest request = new MappingRequest();
		final DataCollectionDefinitionDtoV7 dataCollectionDefinition = catalogueService.get(workflow.getDataCollectionDefinitionId());
		request.getInput().put(MappingRequest.Input.DATA_COLLECTION_DEFINITION, JsonUtils.createSerializableJsonObject(dataCollectionDefinition.getContent()));
		request.getTargets().add(MappingRequest.Target.COMPUTED_EXPRESSIONS);
		final JSONObject documentObject = new SerializableJsonObject();
		putAllDocumentContent(documentObject, workflow);
		request.getInput().put(MappingRequest.Input.DOCUMENT, documentObject);
		request.getInput().put(MappingRequest.Input.CONTEXT, mappingContextFactory.create(workflow));

		final JSONObject response = get(request);
		return MappingResponseMapper.convert(response).getComputedExpressions();
	}

	@Override
	public MappingResponse extractNotes(RegistrationWorkflow workflow, byte[] dcdContent) {
		final MappingRequest request = new MappingRequest();
		final JSONObject documentObject = new SerializableJsonObject();
		putAllDocumentContent(documentObject, workflow);
		request.getInput().put(MappingRequest.Input.DOCUMENT, documentObject);
		request.getInput().put(MappingRequest.Input.DATA_COLLECTION_DEFINITION, JsonUtils.createSerializableJsonObject(dcdContent));
		request.getTargets().add(MappingRequest.Target.DOCUMENT_CONTENT_WITHOUT_COMMENTS);
		request.getTargets().add(MappingRequest.Target.NOTES_FROM_COMMENTS);

		final JSONObject response = get(request);
		return MappingResponseMapper.convert(response);
	}

	@Override
	public JSONObject get(MappingRequest request) {
		return get(request, Endpoint.EMPTY);
	}

	@Override
	public JSONObject get(MappingRequest request, String endpoint) {
		ClientResponse response;
		String token = null;
		try {
			token = securityService.getToken();
			request.getInput().put(MappingRequest.Input.ACCESS_TOKEN, token);
			final String baseUrl = configurationService.get(ConfigurationKey.MAPPING_HOST).getValue();
			final String url = baseUrl + endpoint;

			final WebServiceBuilder builder = new WebServiceBuilder();
			builder.setPost(true);
			builder.setUrl(url);
			builder.setReturnType(new GenericType<ClientResponse>() {});
			builder.setJson(request);
			builder.setType(MediaType.APPLICATION_JSON_TYPE);
			builder.setAccept(MediaType.APPLICATION_JSON_TYPE);
			response = (ClientResponse) webServiceClientService.callWebService(builder);
		} catch(Exception e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.MAPPING_CONNECTION);
			throw exception;
		} finally {
			if(token != null) {
				securityService.removeToken(token);
			}
		}

		if(response.getStatus() >= 200 && response.getStatus() < 300) {
			return response.getEntity(JSONObject.class);
		}
		HealthDataException exception = new HealthDataException();
		exception.setExceptionType(ExceptionType.INVALID_MAPPING_STATUS_CODE, response.getStatus());
		throw exception;
	}

	protected abstract void putAllDocumentContent(JSONObject documentObject, RegistrationWorkflow workflow);
}
