/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.tasks;

import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.MessageFormat;
import java.util.Date;

public abstract class HealthDataTask implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(HealthDataTask.class);

    @Autowired
    protected IConfigurationService configurationService;

    private Date startTime;
    private Date endTime;
    private Exception exception;

    @Override
    public void run() {
        try {
            LOG.debug("Running [{}] task", getName().toLowerCase());

            if(minimalIntervalBetweenRuns() != null && endTime != null) {
                // This check has been introduced to avoid sending to much status messages on testing environments.
                // In fact, on testing environments, the frequency of the daily task might be reduced (few minutes for example).
                // As the status message task is part of the daily task, we do not want to send status messages repeatedly.
                final long nowAsMillis = new Date().getTime();
                final long lastRunAsMillis = endTime.getTime();
                final long millisSinceLastRun = nowAsMillis - lastRunAsMillis;
                if(millisSinceLastRun < minimalIntervalBetweenRuns()) {
                    LOG.debug("Task [{}] was executed to recently, not executing task again", getName().toLowerCase());
                    return;
                }
            }

            if(isRunning()) {
                final Long maxDuration = getMaxRunningDuration();
                final Long duration = (new Date().getTime() - startTime.getTime())/1000;
                if(maxDuration != null && duration.compareTo(maxDuration) > 0) {
                    LOG.debug("{} task running for more than {} seconds, stopping", getName().toLowerCase(), maxDuration);
                    endTime = new Date();
                    return;
                }
                LOG.warn("[{}] task is already running", getName().toLowerCase());
                return;
            }

            exception = null;
            startTime = new Date();
            endTime = null;
            execute();
            endTime = new Date();
            LOG.debug("[{}] task finished (duration: {} seconds)", getName().toLowerCase(), (endTime.getTime() - startTime.getTime()) / 1000);
        } catch (Exception e) {
            endTime = new Date();
            exception = e;
            LOG.error(MessageFormat.format("Error while running [{0}] task: {1}", getName().toLowerCase(), e.getMessage()), e);
            // exception can not be re-thrown here, otherwise subsequent executions are suppressed
            // see http://docs.oracle.com/javase/7/docs/api/java/util/concurrent/ScheduledExecutorService.html
        }
    }

    protected abstract void execute();

    protected Long getMaxRunningDuration() {
        return null;
    }

    public abstract String getName();

    public boolean isRunning() {
        return startTime != null && endTime == null;
    }

    public Date getStartTime() {
        return startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public boolean hasException() {
        return exception != null;
    }

    public Exception getException() {
        return exception;
    }

    public Long minimalIntervalBetweenRuns() {
        return null;
    }
}


