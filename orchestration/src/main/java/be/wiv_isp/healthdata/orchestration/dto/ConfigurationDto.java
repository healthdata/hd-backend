/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.dto;

import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConfigurationDto {

	public static final String UNREADABLE_VALUE = "***";

	private Long id;
	private ConfigurationKey key;
	private String value;
	private String defaultValue;
	private List<String> allowedValues;
	private Organization organization;
	private Boolean useFromMain;

	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	private Timestamp updatedOn;

	public ConfigurationDto() {
	} // needed for jersey to instanciate new object

	public ConfigurationDto(Configuration configuration) {
		this.id = configuration.getId();
		this.key = configuration.getKey();
		if (key.isReadable()) {
			this.value = configuration.getValue();
			this.defaultValue = configuration.getDefaultValue();
		} else {
			this.value = UNREADABLE_VALUE;
			this.defaultValue = UNREADABLE_VALUE;
		}
		this.allowedValues = configuration.getAllowedValues();
		this.organization = configuration.getOrganization();
		this.useFromMain = configuration.getUseFromMain();
		this.updatedOn = configuration.getUpdatedOn();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ConfigurationKey getKey() {
		return key;
	}

	public void setKey(ConfigurationKey key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getDescription() {
		return key.getDescription();
	}

	public String getType() {
		return key.getType();
	}

	public List<String> getAllowedValues() {
		return allowedValues;
	}

	public void setAllowedValues(List<String> allowedValues) {
		this.allowedValues = allowedValues;
	}

	public boolean isReadable() {
		return key.isReadable();
	}

	public boolean isAdvanced() {
		return key.isAdvanced();
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Boolean getUseFromMain() {
		return useFromMain;
	}

	public void setUseFromMain(Boolean useFromMain) {
		this.useFromMain = useFromMain;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof ConfigurationDto) {
			ConfigurationDto other = (ConfigurationDto) o;

			return Objects.equals(key, other.key) //
					&& Objects.equals(value, other.value) //
					&& Objects.equals(defaultValue, other.defaultValue) //
					&& Objects.equals(allowedValues, other.allowedValues) //
					&& Objects.equals(updatedOn, other.updatedOn) //
					&& Objects.equals(organization, other.organization) //
					&& Objects.equals(useFromMain, other.useFromMain);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(//
				this.key, //
				this.value, //
				this.defaultValue, //
				this.allowedValues, //
				this.updatedOn, //
				this.organization, //
				this.useFromMain);
	}

	@Override
	public String toString() {
		return "ConfigurationDto {" + //
				"key = " + Objects.toString(this.key) + ", " + //
				"value = " + (key != null && key.isReadable() ? Objects.toString(this.value) : "***") + ", " + //
				"defaultValue = " + (key != null && key.isReadable() ? Objects.toString(this.value) : "***") + ", " + //
				"allowedValues = " + Objects.toString(this.allowedValues) + ", " + //
				"updatedOn = " + Objects.toString(this.updatedOn) + ", " + //
				"organization = " + Objects.toString(this.organization) + ", " + //
				"useFromMain = " + Objects.toString(this.useFromMain) + "}";
	}
}
