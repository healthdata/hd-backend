/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.search;

import be.wiv_isp.healthdata.common.domain.search.EntitySearch;
import be.wiv_isp.healthdata.common.rest.RestUtils;

import javax.ws.rs.core.UriInfo;

abstract public class AbstractUserRequestSearch extends EntitySearch {

    protected String firstName;
    protected String lastName;
    protected String email;
    protected String emailRequester;
    protected Boolean approved;

    public AbstractUserRequestSearch() {

    }

    public AbstractUserRequestSearch(UriInfo info) {
        firstName = RestUtils.getParameterString(info, "firstName");
        lastName = RestUtils.getParameterString(info, "lastName");
        email = RestUtils.getParameterString(info, "email");
        emailRequester = RestUtils.getParameterString(info, "emailRequester");
        approved = RestUtils.getParameterBoolean(info, "approved");
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailRequester() {
        return emailRequester;
    }

    public void setEmailRequester(String emailRequester) {
        this.emailRequester = emailRequester;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }
}
