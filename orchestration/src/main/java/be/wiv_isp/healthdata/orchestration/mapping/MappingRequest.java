/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.mapping;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MappingRequest {

    public class Input {
        public static final String DATA_COLLECTION_DEFINITION = "dataCollectionDefinition";
        public static final String DOCUMENT = "document";
        public static final String CONTEXT = "context";
        public static final String CSV = "csv";
        public static final String LANGUAGE = "language";
        public static final String BASIC_DATASET = "basicDataset";
        public static final String PRIMARY = "primary";
        public static final String SECONDARY = "secondary";
        public static final String ACCESS_TOKEN = "accessToken";
    }

    public class Target {
        public static final String DOCUMENT = "document";
        public static final String CSV = "csv";
        public static final String STABLE_CSV = "stableCsv";
        public static final String UNIQUE_ID = "uniqueID";
        public static final String PROGRESS = "progress";
        public static final String COMPUTED_EXPRESSIONS = "computedExpressions";
        public static final String VALIDATION = "validation";
        public static final String ES = "es";
        public static final String ES_MAPPING = "esmapping";
        public static final String PENDING_ATTACHMENTS = "pendingAttachments";
        public static final String PDF_TEMPLATE = "handlebars";
        public static final String TO_BE_CODED = "toBeCoded";
        public static final String XPATH_QUERY_RESULTS = "xpathQueryResults";
        public static final String NOTES_FROM_COMMENTS = "notesFromComments";
        public static final String DOCUMENT_CONTENT_WITHOUT_COMMENTS = "documentContentWithoutComments";
        public static final String AFFECTED_FIELDS = "affectedFields";
    }

    public class Fields {
        public static final String CONTENT = "content";
        public static final String PATIENT_ID = "patientID";
        public static final String CODED = "coded";
        public static final String PRIVATE = "private";
    }

    public class XPATH_VALUE {
        public static final String IDENTIFICATION_VALUE = "/kmehrmessage/header/sender/hcparty/id[@S='ID-HCPARTY']";
        public static final String IDENTIFICATION_VALUE_STRING = "string(/kmehrmessage/header/sender/hcparty/id[@S='ID-HCPARTY'])";

    }

    private Map<String, Object> input = new HashMap<>();
    private List<String> targets = new ArrayList<>();

    public Map<String, Object> getInput() {
        return input;
    }

    public void setInput(Map<String, Object> input) {
        this.input = input;
    }

    public List<String> getTargets() {
        return targets;
    }

    public void setTargets(List<String> targets) {
        this.targets = targets;
    }
}
