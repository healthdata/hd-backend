/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.dto.AbstractRegistrationWorkflowDto;
import be.wiv_isp.healthdata.orchestration.dto.NoteDto;
import be.wiv_isp.healthdata.orchestration.service.impl.AbstractElasticSearchService.AdvancedStatus;

import java.util.List;

public interface IElasticSearchService<FullDto extends AbstractRegistrationWorkflowDto, Dto extends AbstractRegistrationWorkflowDto> {

	FullDto getDtoInstance(Dto dto, List<NoteDto> notes);
	
	void index(Long workflowId);
	
	void index(List<Long> workflowIds);

	String search(String index, String type, String searchQuery);

	String search(String[] indices, String[] types, String searchQuery);

	void rebuild();

	void rebuild(Long organizationId);

	void rebuild(Long organizationId, String dataCollectionGroup);

	void repair();

	void repair(Long organizationId);

	void repair(Long organizationId, String dataCollectionGroup);

	boolean status();

	boolean status(Long organizationId);

	boolean status(Long organizationId, String dataCollectionDefinition);

	AdvancedStatus advancedStatus();

	String buildIndexName(Organization organization, String dataCollectionName);

	void delete(Long id);
}
