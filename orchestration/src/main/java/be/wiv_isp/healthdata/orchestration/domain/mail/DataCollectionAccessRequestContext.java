/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.domain.mail;

import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class DataCollectionAccessRequestContext extends AbstractVelocityContext {

    private String lastName;
    private String firstName;
    private Set<String> requestedDataCollections;
    private SortedMap<String, Boolean> decisions;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Set<String> getRequestedDataCollections() {
        return requestedDataCollections;
    }

    public void setRequestedDataCollections(Set<String> requestedDataCollections) {
        this.requestedDataCollections = requestedDataCollections;
    }

    public Map<String, Boolean> getDecisions() {
        return decisions;
    }

    public void setDecisions(Map<String, Boolean> decisions) {
        this.decisions = new TreeMap<>();
        this.decisions.putAll(decisions);
    }

    @Override
    protected void doBuild() {
        put("lastName", lastName);
        put("firstName", firstName);
        put("requestedDataCollections", requestedDataCollections);
        put("decisions", decisions);
    }
}
