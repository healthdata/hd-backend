/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.search;

import be.wiv_isp.healthdata.common.util.StringUtils;

import java.util.Objects;

abstract public class AbstractDocumentSearch {

    private String dwhStatus;
    private Boolean withCsv;
    private Long attachmentId;

    public String getDwhStatus() {
        return dwhStatus;
    }

    public void setDwhStatus(String dwhStatus) {
        this.dwhStatus = dwhStatus;
    }

    public Boolean getWithCsv() {
        return withCsv;
    }

    public void setWithCsv(Boolean withCsv) {
        this.withCsv = withCsv;
    }

    public void setAttachmentId(Long attachmentId) {
        this.attachmentId = attachmentId;
    }

    public Long getAttachmentId() {
        return attachmentId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                this.dwhStatus,
                this.withCsv,
                this.attachmentId);
    }

    @Override
    public String toString() {
        return StringUtils.toString(this);
    }
}
