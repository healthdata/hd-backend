/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.common.domain.Baseline;
import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.persistence.*;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "FOLLOW_UPS")
public class FollowUp {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "FOLLOW_UP_ID")
	private Long id;

	@Column(name = "LABEL", nullable = false)
	private String label;

	@Column(name = "NAME", nullable = false)
	private String name;

	@Column(name = "DESCRIPTION", nullable = false)
	private String description;

	@Column(name = "TIMING", nullable = false)
	private String timing;

	@Embedded
	private Baseline baseline;

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "FOLLOW_UP_CONDITIONS", joinColumns = @JoinColumn(name="FOLLOW_UP_ID"))
	@Column(name = "CONDITION_LABEL")
	private List<String> conditions;

	@Column(name = "ACTIVE")
	private boolean active;

	@Column(name = "SUBMITTED_ON")
	private Timestamp submittedOn;

	@Column(name = "ACTIVATION_DATE")
	private Timestamp activationDate;

	@JsonIgnore
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTiming() {
		return timing;
	}

	public void setTiming(String timing) {
		this.timing = timing;
	}

	public Baseline getBaseline() {
		return baseline;
	}

	public void setBaseline(Baseline baseline) {
		this.baseline = baseline;
	}

	public List<String> getConditions() {
		return conditions;
	}

	public void setConditions(List<String> conditions) {
		this.conditions = conditions;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@JsonSerialize(using = TimestampSerializer.class)
	public Timestamp getSubmittedOn() {
		return submittedOn;
	}

	@JsonDeserialize(using = TimestampDeserializer.class)
	public void setSubmittedOn(Timestamp submittedOn) {
		this.submittedOn = submittedOn;
	}

	@JsonSerialize(using = TimestampSerializer.class)
	public Timestamp getActivationDate() {
		return activationDate;
	}

	@JsonDeserialize(using = TimestampDeserializer.class)
	public void setActivationDate(Timestamp activationDate) {
		this.activationDate = activationDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonIgnore
	public boolean isScheduled() {
		return activationDate != null;
	}

	@JsonIgnore
	public boolean isSubmitted() {
		return submittedOn != null;
	}

	@JsonIgnore
	public boolean isActivationDateOver() {
		if(activationDate == null) {
			return false;
		}

		return new Date().compareTo(activationDate) >= 0;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof FollowUp)) return false;

		FollowUp followUp = (FollowUp) o;

		if (active != followUp.active) return false;
		if (id != null ? !id.equals(followUp.id) : followUp.id != null) return false;
		if (label != null ? !label.equals(followUp.label) : followUp.label != null) return false;
		if (name != null ? !name.equals(followUp.name) : followUp.name != null) return false;
		if (description != null ? !description.equals(followUp.description) : followUp.description != null)
			return false;
		if (timing != null ? !timing.equals(followUp.timing) : followUp.timing != null) return false;
		if (baseline != null ? !baseline.equals(followUp.baseline) : followUp.baseline != null) return false;
		if (conditions != null ? !conditions.equals(followUp.conditions) : followUp.conditions != null) return false;
		if (submittedOn != null ? !submittedOn.equals(followUp.submittedOn) : followUp.submittedOn != null)
			return false;
		return !(activationDate != null ? !activationDate.equals(followUp.activationDate) : followUp.activationDate != null);

	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (label != null ? label.hashCode() : 0);
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (description != null ? description.hashCode() : 0);
		result = 31 * result + (timing != null ? timing.hashCode() : 0);
		result = 31 * result + (baseline != null ? baseline.hashCode() : 0);
		result = 31 * result + (conditions != null ? conditions.hashCode() : 0);
		result = 31 * result + (active ? 1 : 0);
		result = 31 * result + (submittedOn != null ? submittedOn.hashCode() : 0);
		result = 31 * result + (activationDate != null ? activationDate.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return MessageFormat.format("[ FollowUp : id = {0}, label = {1} ]", id, label);
	}

}