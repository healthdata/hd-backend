/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.dao.IDocumentDao;
import be.wiv_isp.healthdata.orchestration.domain.AbstractDocument;
import be.wiv_isp.healthdata.orchestration.domain.search.AbstractDocumentSearch;
import be.wiv_isp.healthdata.orchestration.service.IAbstractDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

abstract public class AbstractDocumentService<Document extends AbstractDocument, Search extends AbstractDocumentSearch> implements IAbstractDocumentService<Document, Search> {

	@Autowired
	private IDocumentDao<Document, Search> documentDao;

	@Transactional
	@Override
	public Document create(Document document) {
		return documentDao.create(document);
	}

	@Override
	public Document get(long id) {
		return documentDao.get(id);
	}

	@Transactional
	@Override
	public Document update(Document document) {
		return documentDao.update(document);
	}

	@Override
	public void delete(long id) {
		Document document = get(id);
		if (document == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.DELETE_NON_EXISTING, "DOCUMENT", id);
			throw exception;
		}
		delete(document);
	}

	@Transactional
	@Override
	public void delete(Document document) {
		documentDao.delete(document);
	}

	@Override
	public List<Document> getAll(Search search) {
		return documentDao.getAll(search);
	}

	@Override
	public long count() {
		return documentDao.count();
	}
}