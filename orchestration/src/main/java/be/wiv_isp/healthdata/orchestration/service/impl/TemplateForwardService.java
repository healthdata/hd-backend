/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.api.uri.TemplateUri;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IWebServiceClientService;
import be.wiv_isp.healthdata.common.caching.CacheManagementService;
import be.wiv_isp.healthdata.common.dto.TemplateDtoV1;
import be.wiv_isp.healthdata.common.domain.enumeration.TemplateKey;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.orchestration.service.ITemplateForwardService;
import com.sun.jersey.api.client.GenericType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;

@Service
public class TemplateForwardService implements ITemplateForwardService {

	@Autowired
	private IWebServiceClientService webServiceClientService;

	@Autowired
	private IConfigurationService configurationService;

	private static final Logger LOG = LoggerFactory.getLogger(TemplateForwardService.class);

	@Override
	@Cacheable(value = CacheManagementService.templateCache)
	public TemplateDtoV1 get(TemplateKey templateKey) {
		LOG.debug("Getting template with key [{}]", templateKey);

		Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

		TemplateUri templateUri = new TemplateUri();
		templateUri.setHost(catalogueHost.getValue());
		templateUri.setKey(templateKey);

		WebServiceBuilder wsb = new WebServiceBuilder();
		wsb.setUrl(templateUri.toString());
		wsb.addHeader(HttpHeaders.CLIENT_VERSION, TemplateUri.getClientVersion());
		wsb.setGet(true);
		wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb.setReturnType(new GenericType<TemplateDtoV1>() {
		});

		return (TemplateDtoV1) webServiceClientService.callWebService(wsb);
	}
}
