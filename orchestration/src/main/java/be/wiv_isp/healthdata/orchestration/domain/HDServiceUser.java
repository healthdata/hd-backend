/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Objects;

public class HDServiceUser implements HDUserDetails {

    public enum Username {
        MESSAGING,
        PRELOADING,
        FAST_TRACK,
        FOLLOW_UP_UPDATER,
        EFORMS,
        API
    }

    private String userName;
    private Organization organization;

    private HDServiceUser(String userName, Organization organization) {
        this.userName = userName;
        this.organization = organization;
    }

    public static HDServiceUser create(Username username) {
        return create(username, null);
    }

    public static HDServiceUser create(Username username, Organization organization) {
        return create(username.toString(), organization);
    }

    public static HDServiceUser create(String username, Organization organization) {
        return new HDServiceUser(username, organization);
    }

    @Override
    public Organization getOrganization() {
        return organization;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, organization);
    }

    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }

        if(o instanceof HDServiceUser) {
            HDServiceUser other = (HDServiceUser) o;

            return Objects.equals(userName, other.userName) && Objects.equals(organization, other.organization);
        }

        return false;
    }

    @Override
    public boolean isAdmin() {
        return false;
    }

    @Override
    public boolean isSupport() {
        return false;
    }

    @Override
    public boolean isUser() {
        return true;
    }

    @Override
    public boolean isSuperAdmin() {
        return false;
    }

    @Override
    public boolean isHd4prc() {
        return false;
    }
}
