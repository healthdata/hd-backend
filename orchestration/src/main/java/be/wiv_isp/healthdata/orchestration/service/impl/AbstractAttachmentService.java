/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.mapper.IntegerConfigurationMapper;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.dao.IAttachmentContentDao;
import be.wiv_isp.healthdata.orchestration.dao.IAttachmentDao;
import be.wiv_isp.healthdata.orchestration.dao.IRegistrationDocumentDao;
import be.wiv_isp.healthdata.orchestration.domain.AbstractDocument;
import be.wiv_isp.healthdata.orchestration.domain.Attachment;
import be.wiv_isp.healthdata.orchestration.domain.AttachmentContent;
import be.wiv_isp.healthdata.orchestration.domain.search.AbstractDocumentSearch;
import be.wiv_isp.healthdata.orchestration.domain.search.AttachmentContentSearch;
import be.wiv_isp.healthdata.orchestration.domain.search.AttachmentSearch;
import be.wiv_isp.healthdata.orchestration.service.IAttachmentService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;

public abstract class AbstractAttachmentService<DocumentSearch extends AbstractDocumentSearch> implements IAttachmentService {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractAttachmentService.class);

    @Autowired
    private IAttachmentDao attachmentDao;

    @Autowired
    private IAttachmentContentDao attachmentContentDao;

    @Autowired
    private IRegistrationDocumentDao documentDao;

    @Autowired
    private IConfigurationService configurationService;

    @Autowired
    @Qualifier("transactionManager")
    protected PlatformTransactionManager txManager;

    @Override
    @Transactional
    public Attachment create(final Attachment attachment) {
        LOG.debug("creating new attachment: {}", attachment);

        verifyAttachmentSize(attachment);

        final AttachmentContentSearch search = new AttachmentContentSearch();
        search.setHash(attachment.getContent().getHash());
        final AttachmentContent existingContent = attachmentContentDao.getUnique(search);

        if(existingContent != null) {
            LOG.debug("an existing attachment content has been found with the same hash. The new attachment will be linked to this existing content");
            attachment.setContent(existingContent);
        } else {
            LOG.debug("no existing attachment content has been found with the same hash. A new content will be created");
        }
        return attachmentDao.create(attachment);
    }

    @Override
    public List<Attachment> getAll(final AttachmentSearch search) {
        final List<Attachment> attachments = attachmentDao.getAll(search);

        if(search.getWithContent()) {
            new TransactionTemplate(txManager).execute(new TransactionCallbackWithoutResult() {
                @Override
                protected void doInTransactionWithoutResult(TransactionStatus status) {
                    for (final Attachment attachment : attachments) {
                        attachment.getContent().getContent();
                    }
                }
            });
        }

        return attachments;
    }

    @Override
    public Attachment getUnique(final AttachmentSearch search) {
        final List<Attachment> all = getAll(search);
        if(CollectionUtils.isEmpty(all)) {
            return null;
        }
        if(all.size() > 1) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.NON_UNIQUE_SEARCH_RESULT, all.size(), "ATTACHMENT", search);
            throw exception;
        }
        return all.get(0);
    }

    @Override
    @Transactional
    public void delete(final Attachment attachment) {
        LOG.debug("deleting attachment: {}", attachment);

        LOG.debug("unlinking attachments from documents");
        final DocumentSearch documentSearch = getDocumentSearch();
        documentSearch.setAttachmentId(attachment.getId());
        final List<AbstractDocument> documents = documentDao.getAll(documentSearch);
        for (final AbstractDocument document : documents) {
            document.removeAttachment(attachment);
            documentDao.update(document);
        }

        attachmentDao.delete(attachment);

        final AttachmentContent content = attachment.getContent();
        final AttachmentSearch search = new AttachmentSearch();
        search.setContentId(content.getId());
        if(CollectionUtils.isEmpty(attachmentDao.getAll(search))) {
            LOG.debug("no attachment is referring to the content anymore, also deleting content");
            attachmentContentDao.delete(content);
        }
    }

    @Override
    @Transactional
    public void linkAttachments(final AbstractDocument document, final List<Attachment> attachments) {
        LOG.debug("Linking attachments to document");
        if(CollectionUtils.isEmpty(attachments)) {
            LOG.debug("No attachments provided. Nothing to do");
            return;
        }
        for (final Attachment attachment : attachments) {
            Attachment att;
            if(attachment.getId() == null) {
                LOG.info("Attachment does not have an id. Looking for attachment by uuid");
                final AttachmentSearch search = new AttachmentSearch();
                search.setUuid(attachment.getUuid());
                att = getUnique(search);
                if(att == null) {
                    LOG.debug("no attachment found with uuid {}, creating new attachment", attachment.getUuid());
                    att = create(attachment);
                }
            } else {
                LOG.info("Attachment has an id ({}). Looking for attachment by id", attachment.getId());
                att = attachmentDao.get(attachment.getId());
            }

            document.addAttachment(att);
            documentDao.update(document);
        }
    }

    private void verifyAttachmentSize(final Attachment attachment) {
        final int maxAttachmentSize = IntegerConfigurationMapper.map(configurationService.get(ConfigurationKey.MAX_ATTACHMENT_SIZE));
        final int attachmentSize = attachment.getContent().getZippedContent().length;
        if(attachmentSize > maxAttachmentSize) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_ATTACHMENT_SIZE, attachmentSize, maxAttachmentSize);
            throw exception;
        }
    }

    protected abstract DocumentSearch getDocumentSearch();
}
