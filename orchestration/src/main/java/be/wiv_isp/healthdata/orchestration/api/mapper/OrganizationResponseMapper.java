/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api.mapper;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class OrganizationResponseMapper {

	static public String convert(List<Organization> organisations) {
		JSONArray json = new JSONArray();

		for (Organization organization : organisations) {
			Map<String, Object> organizationMap = new LinkedHashMap<>();
			organizationMap.put("id", organization.getId());
			organizationMap.put("healthDataIDType", organization.getHealthDataIDType());
			organizationMap.put("healthDataIDValue", organization.getHealthDataIDValue());
			organizationMap.put("name", organization.getName());
			organizationMap.put("main", organization.isMain());
			json.put(new JSONObject(organizationMap));
		}

		return json.toString();
	}
}
