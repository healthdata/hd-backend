/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.mapper.UserManagementTypeConfigurationMapper;
import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.UserManagementType;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.security.authentication.strategy.IUserManagementStrategy;
import be.wiv_isp.healthdata.orchestration.service.IUserService;
import be.wiv_isp.healthdata.orchestration.service.IUserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@Component
public class UserManagementService implements IUserManagementService {

    @Autowired
    private IConfigurationService configurationService;

    @Autowired
    private IOrganizationDao organizationDao;

    private List<IUserManagementStrategy> userManagementStrategies = new ArrayList();
    @Autowired
    private IUserManagementStrategy databaseUserManagementStrategy;
    @Autowired
    private IUserManagementStrategy databaseLdapUserManagementStrategy;
    @Autowired
    protected IUserService userService;

    @PostConstruct
    public void initialize() {
        userManagementStrategies.add(databaseUserManagementStrategy);
        userManagementStrategies.add(databaseLdapUserManagementStrategy);
    }

    @Override
    public IUserManagementStrategy getUserManagementStrategy() {
        final UserManagementType userManagementType = getUserManagementType();

        for(IUserManagementStrategy strategy : userManagementStrategies) {
            if(strategy.supports(userManagementType)) {
                return strategy;
            }
        }
        HealthDataException exception = new HealthDataException();
        exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION, MessageFormat.format("Unsupported user management type: {0}", userManagementType));
        throw exception;
    }

    @Override
    public UserManagementType getUserManagementType(Organization organization) {
        return UserManagementTypeConfigurationMapper.map(configurationService.get(ConfigurationKey.USER_MANAGEMENT_TYPE, organization));
    }

    @Override
    public UserManagementType getUserManagementType() {
        if(AuthenticationService.isAuthenticated()) {
            final HDUserDetails principal = AuthenticationService.getUserDetails();
            return getUserManagementType(principal.getOrganization());
        } else {
            final Organization main = organizationDao.getMain();
            return getUserManagementType(main);
        }
    }
}
