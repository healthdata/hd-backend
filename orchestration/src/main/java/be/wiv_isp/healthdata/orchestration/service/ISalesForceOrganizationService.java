/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service;

import be.wiv_isp.healthdata.common.dto.SalesForceAccountDto;
import be.wiv_isp.healthdata.common.dto.SalesForceOrganizationDto;

import java.util.List;

public interface ISalesForceOrganizationService {

	void createSalesForceOrganization(String parentIdentificationType, String parentIdentificationValue, SalesForceAccountDto organization);

	SalesForceOrganizationDto getSalesForceOrganization(String identificationType, String identificationValue);

	List<SalesForceOrganizationDto> getSalesForceSubOrganizations(String identificationType, String identificationValue);

}
