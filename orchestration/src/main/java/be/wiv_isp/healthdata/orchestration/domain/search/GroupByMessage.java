/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.search;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageStatus;

import java.util.Objects;

public class GroupByMessage {

    private MessageStatus status;
    private long count;

    public GroupByMessage(MessageStatus status, long count) {
        this.status = status;
        this.count = count;
    }

    public MessageStatus getStatus() {
        return status;
    }

    public void setStatus(MessageStatus status) {
        this.status = status;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, count);
    }

    @Override
    public boolean equals(Object other) {
        if(this == other) {
            return true;
        }

        if(other instanceof GroupByMessage) {
            GroupByMessage o = (GroupByMessage) other;

            return Objects.equals(status, o.status) && Objects.equals(count, o.count);
        }

        return false;
    }
}
