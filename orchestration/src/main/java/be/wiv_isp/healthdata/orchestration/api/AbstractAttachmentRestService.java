/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.common.api.exception.NotFoundException;
import be.wiv_isp.healthdata.common.api.exception.UnauthorizedException;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.annotation.Auditable;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ApiType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.mapper.AttachmentMapper;
import be.wiv_isp.healthdata.orchestration.domain.search.AbstractDocumentSearch;
import be.wiv_isp.healthdata.orchestration.domain.search.AbstractRegistrationWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.domain.search.AttachmentSearch;
import be.wiv_isp.healthdata.orchestration.service.IAbstractRegistrationDocumentService;
import be.wiv_isp.healthdata.orchestration.service.IAbstractRegistrationWorkflowService;
import be.wiv_isp.healthdata.orchestration.service.IAttachmentService;
import be.wiv_isp.healthdata.orchestration.service.IUserDataCollectionService;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;

public abstract class AbstractAttachmentRestService<Workflow extends AbstractRegistrationWorkflow<Document>, Search extends AbstractRegistrationWorkflowSearch, Document extends AbstractRegistrationDocument, DocumentSearch extends AbstractDocumentSearch> {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractAttachmentRestService.class);

	@Autowired
	private IAttachmentService attachmentService;

	@Autowired
	protected IUserDataCollectionService userDataCollectionService;

	@Autowired
	private IAbstractRegistrationWorkflowService<Workflow, Search> workflowService;

	@Autowired
	private IAbstractRegistrationDocumentService documentService;

	@GET
	@Path("/info/{uuid}")
	@PreAuthorize("hasRole('ROLE_USER')")
	@Produces(MediaType.APPLICATION_JSON)
	@Auditable(apiType = ApiType.ATTACHMENT)
	public Response getInfo(@PathParam("uuid") String uuid) {
		final AttachmentSearch search = new AttachmentSearch();
		search.setUuid(uuid);
		search.setWithContent(true);
		final Attachment attachment = attachmentService.getUnique(search);
		validateAction(attachment);
		return Response.ok(AttachmentMapper.mapToAttachmentDto(attachment, false)).build();
	}

	@POST
	@Path("/upload/{uuid}")
	@PreAuthorize("hasRole('ROLE_USER')")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	@Auditable(apiType = ApiType.ATTACHMENT)
	public Response upload(@PathParam("uuid") String uuid, @FormDataParam("file") InputStream uploadedInputStream, @FormDataParam("file") FormDataContentDisposition fileDetail, @FormDataParam("workflowId") String workflowId) {
		LOG.info("Uploading attachment (uuid={}, workflowId={}, filename={})", uuid, workflowId, fileDetail.getFileName());

		final AttachmentSearch search = new AttachmentSearch();
		search.setUuid(uuid);
		final Attachment existing = attachmentService.getUnique(search);
		if(existing != null) {
			LOG.error("Attachment with uuid [{}] already exists", uuid);
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.CREATE_DUPLICATE_KEY, uuid);
			throw exception;
		}


		final Attachment attachment = new Attachment();
		attachment.setUuid(uuid);
		attachment.setFilename(fileDetail.getFileName());
		try {
			final AttachmentContent content = new AttachmentContent();
			content.setContent(IOUtils.toByteArray(uploadedInputStream));
			attachment.setContent(content);
		} catch (IOException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.ERROR_WHILE_READING_DATA, e.getMessage());
			throw exception;
		}

		final AbstractWorkflow workflow = workflowService.get(Long.valueOf(workflowId));
		validateAction(attachment, workflow);

		final Attachment created = attachmentService.create(attachment);
		attachmentService.linkAttachments(workflow.getDocument(), Collections.singletonList(created));
		return Response.ok(AttachmentMapper.mapToAttachmentDto(created, false)).build();
	}

	@GET
	@Path("/download/{uuid}")
	@PreAuthorize("hasRole('ROLE_USER')")
	@Auditable(apiType = ApiType.ATTACHMENT)
	public Response download(@PathParam("uuid") String uuid) {
		LOG.info("Downloading attachment with uuid [{}]", uuid);

		final AttachmentSearch search = new AttachmentSearch();
		search.setUuid(uuid);
		search.setWithContent(true);

		final Attachment attachment = attachmentService.getUnique(search);
		validateAction(attachment);
		return Response.ok(attachment.getContent().getContent(), attachment.getMimeType()).build();
	}

	@DELETE
	@Path("/delete/{uuid}")
	@PreAuthorize("hasRole('ROLE_USER')")
	@Auditable(apiType = ApiType.ATTACHMENT)
	public Response delete(@PathParam("uuid") String uuid) {
		LOG.info("Deleting attachment with uuid [{}]", uuid);

		final AttachmentSearch search = new AttachmentSearch();
		search.setUuid(uuid);

		final Attachment attachment = attachmentService.getUnique(search);
		validateAction(attachment);
		attachmentService.delete(attachment);
		return Response.noContent().build();
	}

	private void validateAction(final Attachment attachment) {
		validateAction(attachment, null);
	}

	private void validateAction(Attachment attachment, AbstractWorkflow workflow) {
		if (attachment == null) {
			throw new NotFoundException();
		}
		if (workflow == null) {
			workflow = getWorkflow(attachment);
		}
		if (workflow == null || WorkflowStatus.DELETED.equals(workflow.getStatus())) {
			throw new NotFoundException(MessageFormat.format("Workflow with id {0} does not exist.", workflow.getId()));
		}
		final HDUserDetails principal = AuthenticationService.getUserDetails();
		if (!userDataCollectionService.isUserAuthorized(principal, workflow)) {
			throw new UnauthorizedException();
		}
	}

	private AbstractWorkflow getWorkflow(final Attachment attachment) {
		final DocumentSearch search = getDocumentSearch();
		search.setAttachmentId(attachment.getId());
		final List<AbstractDocument> documents = documentService.getAll(search);

		AbstractWorkflow workflow = null;
		for (final AbstractDocument document : documents) {
			if(workflow == null) {
				workflow = document.getWorkflow();
			} else if(!workflow.getId().equals(document.getWorkflow().getId())) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.GENERAL_CONFLICT, MessageFormat.format("Attachment {0} is linked to different workflows", attachment));
				throw exception;
			}
		}
		return workflow;
	}

	protected abstract DocumentSearch getDocumentSearch();
}
