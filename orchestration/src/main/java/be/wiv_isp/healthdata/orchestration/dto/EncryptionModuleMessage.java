/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dto;


import be.wiv_isp.healthdata.orchestration.domain.Identification;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@JsonSerialize(include = Inclusion.NON_NULL)
public class EncryptionModuleMessage {

	private static final String NEW_LINE_CHARACTER = "\r\n";

	private String content;
	private Map<String, String> metadata;
	private String subject;
	private Identification addressee;
	private Identification encryptionAddressee;

	@JsonProperty("csvContent") // can not be modified since this key is used by the encryption module
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Identification getAddressee() {
		return addressee;
	}

	public void setAddressee(Identification addressee) {
		this.addressee = addressee;
	}

	public Map<String, String> getMetadata() {
		return metadata;
	}

	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}

	public Identification getEncryptionAddressee() {
		return encryptionAddressee;
	}

	public void setEncryptionAddressee(Identification encryptionAddressee) {
		this.encryptionAddressee = encryptionAddressee;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public boolean canMerge(EncryptionModuleMessage other) {
		return Objects.equals(this.addressee, other.addressee)
				&& Objects.equals(this.encryptionAddressee, other.encryptionAddressee)
				&& Objects.equals(this.subject, other.subject)
				&& Objects.equals(this.metadata, other.metadata);
	}

	public EncryptionModuleMessage merge(EncryptionModuleMessage other) {
		if (this.addressee==null)
			this.addressee = other.addressee;
		if (this.encryptionAddressee==null)
			this.encryptionAddressee = other.encryptionAddressee;
		if (this.metadata==null)
			this.metadata = other.metadata;
		if (this.subject==null)
			this.subject = other.subject;

		if (this.canMerge(other))
		{
			if (this.content==null) {
				this.content = new String(other.content);
			}
			else {
				this.content = new StringBuilder(this.content).append(NEW_LINE_CHARACTER).append(other.content).toString();
			}
			return this;
		}

		return null;
	}

	public void addMetadata(String key, String value) {
		if(metadata == null) {
			metadata = new HashMap<>();
		}
		metadata.put(key, value);
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof EncryptionModuleMessage) {
			EncryptionModuleMessage other = (EncryptionModuleMessage) o;

			return Objects.equals(subject, other.subject)
					&& Objects.equals(encryptionAddressee, other.encryptionAddressee)
					&& Objects.equals(addressee, other.addressee)
					&& Objects.equals(metadata, other.metadata)
					&& Objects.equals(content, other.content);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.content, this.addressee, this.encryptionAddressee, this.metadata, this.subject);
	}

	@Override
	public String toString() {
		return "EncryptionModuleMessage {" + //
				"csvContent = " + Objects.toString(this.content) + ", " + //
				"addressee = " + Objects.toString(this.addressee) + ", " + //
				"encryptionAddressee = " + Objects.toString(this.encryptionAddressee) + ", " + //
				"subject = " + Objects.toString(this.subject) + ", " + //
				"metadata = " + Objects.toString(this.metadata) + "}";
	}
}

