/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDao;
import be.wiv_isp.healthdata.orchestration.dao.IDocumentDao;
import be.wiv_isp.healthdata.orchestration.domain.AbstractDocument;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.search.AbstractDocumentSearch;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

abstract public class AbstractDocumentDao<Document extends AbstractDocument, Search extends AbstractDocumentSearch> extends CrudDao<Document, Long> implements IDocumentDao<Document, Search> {

	public AbstractDocumentDao(Class<Document> entityClass) {
		super(entityClass);
	}

	@Override
	public List<Document> getAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Document> cq = cb.createQuery(entityClass);
		Root<Document> rootEntry = cq.from(entityClass);
		CriteriaQuery<Document> all = cq.select(rootEntry);
		TypedQuery<Document> allQuery = em.createQuery(all);
		return allQuery.getResultList();
	}

	@Override
	public List<Document> getAll(Search search) {
		if (search == null) {
			return getAll();
		}
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Document> cq = cb.createQuery(entityClass);
		Root<Document> rootEntry = cq.from(entityClass);
		CriteriaQuery<Document> all = cq.select(rootEntry);

		List<Predicate> predicates = getPredicates(search, cb, rootEntry);
		if (!predicates.isEmpty()) {
			cq.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		}
		TypedQuery<Document> allQuery = em.createQuery(all);
		return allQuery.getResultList();
	}

	protected List<Predicate> getPredicates(Search search, CriteriaBuilder cb, Root<Document> rootEntry) {
		return new ArrayList<>();
	}

	@Override
	public long count() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Document> rootEntry = cq.from(entityClass);

		final Path<Object> status = rootEntry.get("workflow").get("status");
		final Path<Object> orgIsDeleted = rootEntry.get("workflow").get("organization").get("deleted");
		if (ignoreDeletedWorkflows())
			cq.where(cb.or(cb.isNull(status), cb.and(cb.notEqual(status, WorkflowStatus.DELETED), cb.equal(orgIsDeleted, false))));
		else
			cq.where(cb.or(cb.isNull(status), cb.equal(orgIsDeleted, false)));

		CriteriaQuery<Long> count = cq.select(cb.countDistinct(rootEntry.get("workflow")));
		TypedQuery<Long> countQuery = em.createQuery(count);
		return countQuery.getSingleResult();
	}

	protected abstract boolean ignoreDeletedWorkflows();
}
