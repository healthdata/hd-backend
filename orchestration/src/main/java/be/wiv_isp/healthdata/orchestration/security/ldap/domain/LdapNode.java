/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.security.ldap.domain;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.apache.commons.collections.CollectionUtils;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.SearchResult;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LdapNode {

	private final SearchResult searchResult;

	private Map<String, List<String>> attributes;

	public LdapNode(SearchResult searchResult) throws NamingException {
		this.searchResult = searchResult;
		this.attributes = new HashMap<>();

		final NamingEnumeration<? extends Attribute> attributes = searchResult.getAttributes().getAll();

		while (attributes.hasMore()) {
			final Attribute attribute = attributes.next();
			final NamingEnumeration<?> specAttributes = attribute.getAll();
			while (specAttributes.hasMore()) {
				final String key = attribute.getID();
				final String value = specAttributes.next().toString();

				if (this.attributes.get(key) == null) {
					this.attributes.put(key, new ArrayList<String>());
				}

				this.attributes.get(key).add(value);
			}
		}
	}

	public List<String> getAttributes(String key) {
		return attributes.get(key);
	}

	public String getAttribute(String key) {
		return getAttribute(key, false);
	}

	public String getAttribute(String key, boolean exactlyOneValue) {
		final List<String> values = attributes.get(key);

		if(exactlyOneValue) {
			if (CollectionUtils.isEmpty(values)) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.LDAP_EXCEPTION, MessageFormat.format("no {0} attribute found for node with dn [{1}].", key, this.getDn()));
				throw exception;
			}
			if (values.size() > 1) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.LDAP_EXCEPTION, MessageFormat.format("more than 1 {0} attribute found for node with dn [{1}].", key, this.getDn()));
				throw exception;
			}
			return values.get(0);
		} else {
			if (CollectionUtils.isEmpty(values)) {
				return null;
			}
			return values.get(0);
		}
	}

	public String getDn() {
		return searchResult.getNameInNamespace();
	}

	@Override
	public String toString() {
		return getDn();
	}
}
