/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dto;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import be.wiv_isp.healthdata.orchestration.action.StatusMessageContent;
import be.wiv_isp.healthdata.orchestration.domain.AbstractStatusMessage;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class AbstractStatusMessageDto {

    protected HealthDataIdentification healthDataIdentification;
    protected StatusMessageContent content;
    protected Timestamp createdOn;
    protected Timestamp sentOn;

    public AbstractStatusMessageDto(AbstractStatusMessage statusMessage) {
        setHealthDataIdentification(statusMessage.getHealthDataIdentification());
        setContent(statusMessage.getContent());
        setCreatedOn(statusMessage.getCreatedOn());
        setSentOn(statusMessage.getSentOn());
    }

    private List<AbstractStatusMessageDto> convert(List<AbstractStatusMessage> abstractStatusMessages) {
        if(abstractStatusMessages == null) {
            return null;
        }

        final List<AbstractStatusMessageDto> converted = new ArrayList<>();

        for(final AbstractStatusMessage abstractStatusMessage : abstractStatusMessages) {
            converted.add(new AbstractStatusMessageDto(abstractStatusMessage));
        }

        return converted;
    }

    public HealthDataIdentification getHealthDataIdentification() {
        return healthDataIdentification;
    }

    public void setHealthDataIdentification(HealthDataIdentification healthDataIdentification) {
        this.healthDataIdentification = healthDataIdentification;
    }

    public StatusMessageContent getContent() {
        return content;
    }

    public void setContent(StatusMessageContent content) {
        this.content = content;
    }

    @JsonSerialize(using = TimestampSerializer.class)
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    @JsonSerialize(using = TimestampSerializer.class)
    public Timestamp getSentOn() {
        return sentOn;
    }

    public void setSentOn(Timestamp sentOn) {
        this.sentOn = sentOn;
    }
}
