/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.orchestration.dao.IAbstractEventDao;
import be.wiv_isp.healthdata.orchestration.domain.Event;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.search.EventSearch;

import javax.persistence.criteria.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractEventDao<Entity extends Event, Search extends EventSearch> extends CrudDaoV2<Entity, Long, Search> implements IAbstractEventDao<Entity, Search> {

    public AbstractEventDao(Class<Entity> entityClass) {
        super(entityClass);
    }

    @Override
    protected List<Predicate> getPredicates(Search search, CriteriaBuilder cb, Root<Entity> rootEntry) {
        final List<Predicate> predicates = new ArrayList<>();
        if (search.getTypes() != null) {
            predicates.add(rootEntry.get("type").in(search.getTypes()));
        }
        if (search.getWithNotificationTimeBefore() != null) {
            final ListJoin<Event, Timestamp> notificationTimes = rootEntry.joinList("notificationTimes");
            predicates.add(cb.lessThanOrEqualTo(notificationTimes, search.getWithNotificationTimeBefore()));
        }
        if (search.getUser() != null) {
            final Join<User, Event> join = rootEntry.join("users");
            predicates.add(cb.equal(join.<Long>get("id"), search.getUser().getId()));
        }
        return predicates;
    }
}
