/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.domain.enumeration.TemplateKey;
import be.wiv_isp.healthdata.common.dto.TemplateDtoV1;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.dao.IMailDao;
import be.wiv_isp.healthdata.orchestration.domain.Authority;
import be.wiv_isp.healthdata.orchestration.domain.Mail;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MailSecurityProtocol;
import be.wiv_isp.healthdata.orchestration.domain.mail.AbstractVelocityContext;
import be.wiv_isp.healthdata.orchestration.domain.mapper.MailSecurityProtocolConfigurationMapper;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import be.wiv_isp.healthdata.orchestration.domain.validator.impl.ValidatorUtils;
import be.wiv_isp.healthdata.orchestration.dto.MailDto;
import be.wiv_isp.healthdata.orchestration.service.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.text.MessageFormat;
import java.util.*;

@Service
public class MailService implements IMailService {

    private static final Logger LOG = LoggerFactory.getLogger(MailService.class);

    @Autowired
    protected ITemplateService templateService;
    @Autowired
    protected ITemplateForwardService templateForwardService;
    @Autowired
    protected IConfigurationService configurationService;
    @Autowired
    private IUserManagementService userManagementService;
    @Autowired
    private IMailDao mailDao;

    @Override
    public void create(Mail mail) {
        mailDao.create(mail);
    }

    @Override
    public void update(Mail mail) {
        mailDao.update(mail);
    }

    @Override
    public MailDto createMail(TemplateKey templateKey, AbstractVelocityContext context, String... to) {
        return createMail(templateKey, context, new HashSet<>(Arrays.asList(to)));
    }

    @Override
    public MailDto createMail(TemplateKey templateKey, AbstractVelocityContext context, Set<String> to) {
        TemplateDtoV1 template = templateForwardService.get(templateKey);

        if (template == null) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.ERROR_WHILE_SENDING_MAIL, "Could not retrieve mail template");
            throw exception;
        }

        MailDto mail = new MailDto();
        mail.setTo(to);
        mail.setSubject(templateService.process(template.getSubject(), context));
        mail.setText(templateService.process(template.getText(), context));
        return mail;
    }

    @Override
    public void createAndSendMail(TemplateKey templateKey, AbstractVelocityContext context, String... to) {
        createAndSendMail(templateKey, context, new HashSet<>(Arrays.asList(to)));
    }

    @Override
    public void createAndSendMail(TemplateKey templateKey, AbstractVelocityContext context, Set<String> to) {
        send(createMail(templateKey, context, to));
    }

    @Override
    public void send(MailDto mail) {
        if(CollectionUtils.isEmpty(mail.getTo())) {
            LOG.warn("Mail does not contain any recipient address. Not sending email.");
            return;
        }
        try {
            final String fromAddress = configurationService.get(ConfigurationKey.MAIL_FROM_ADDRESS).getValue();

            final MimeMessage message = new MimeMessage(createSession());
            message.setFrom(new InternetAddress(fromAddress));
            for (String to : mail.getTo()) {
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            }
            message.setSubject(mail.getSubject());
            message.setText(mail.getText(), "utf-8", "html");
            Transport.send(message);
        } catch (Exception e) {
            LOG.warn("Could not send mail. Reason: {}. Enable DEBUG logging for stacktrace.", e.getMessage());
            LOG.debug(MessageFormat.format("Could not send mail [{0}]", mail), e);
        }
    }

    @Override
    public void send(Mail mail) {
        send(mail, Message.RecipientType.TO);
    }

    @Override
    public void send(Mail mail, Message.RecipientType recipientType) {
        if(CollectionUtils.isEmpty(mail.getTo())) {
            LOG.warn("Mail does not contain any recipient address. Not sending email.");
            return;
        }
        try {
            final MimeMessage message = new MimeMessage(createSession());
            message.setFrom(new InternetAddress(mail.getFrom()));
            for (String to : mail.getTo()) {
                if(ValidatorUtils.isEmailValid(to)) {
                    message.addRecipient(recipientType, new InternetAddress(to));
                } else {
                    LOG.warn("Email address [{}] is not a valid email address", to);
                }
            }
            message.setSubject(mail.getSubject());
            message.setText(mail.getContent(), "utf-8");
            Transport.send(message);
            mail.setStatus(Mail.Status.SENT);
        } catch (Exception e) {
            LOG.warn("Could not send mail. Reason: {}. Enable DEBUG logging for stacktrace.", e.getMessage());
            LOG.debug(MessageFormat.format("Could not send mail [{0}]", mail), e);
            mail.setStatus(Mail.Status.SENT_WITH_ERROR);
            mail.setError(ExceptionUtils.getStackTrace(e));
        } finally {
            update(mail);
        }
    }

    @Override
    public boolean status() {
        LOG.debug("Checking mail server connection status");
        try {
            final Transport transport = createSession().getTransport();
            transport.connect();
            transport.close();
            return true;
        } catch (Exception e) {
            LOG.error("Could not connect to the mail server. Reason: {}. Enable TRACE logging for stacktrace.", e.getMessage());
            LOG.trace("Could not connect to the mail server", e);
            return false;
        }
    }

    private Session createSession() {
        final String host = configurationService.get(ConfigurationKey.MAIL_SMTP_HOST).getValue();
        final String port = configurationService.get(ConfigurationKey.MAIL_SMTP_PORT).getValue();
        final String username = configurationService.get(ConfigurationKey.MAIL_SMTP_USERNAME).getValue();
        final String password = configurationService.get(ConfigurationKey.MAIL_SMTP_PASSWORD).getValue();
        final MailSecurityProtocol mailSecurityProtocol = MailSecurityProtocolConfigurationMapper.map(configurationService.get(ConfigurationKey.MAIL_SECURITY_PROTOCOL));

        final Properties properties = new Properties();
        properties.setProperty("mail.smtp.host", host);
        properties.setProperty("mail.smtp.port", port);
        properties.setProperty("mail.smtp.sendpartial", "true"); // send the email even if some email addresses are invalid
        properties.setProperty("mail.transport.protocol", "smtp");
        properties.setProperty("mail.smtp.connectiontimeout", "10000"); // 10 seconds

        switch(mailSecurityProtocol) {
            case START_TLS:
                properties.setProperty("mail.smtp.starttls.enable", "true");
                break;
            case SSL:
                properties.setProperty("mail.smtp.socketFactory.port", port);
                properties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
                properties.setProperty("mail.smtp.socketFactory.fallback", "false");
                break;
        }

        LOG.debug("Properties: {}", properties);

        if(StringUtils.isNotEmpty(username)) {
            properties.setProperty("mail.smtp.auth", "true");
            final Authenticator authenticator = new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            };
            return Session.getInstance(properties, authenticator);
        }
        return Session.getInstance(properties);
    }

    @Override
    public Set<String> getEmailAdmins(Organization organization) {
        return getEmails(organization, Authority.ADMIN_AUTHORITY);
    }

    @Override
    public Set<String> getEmailUsers(Organization organization) {
        return getEmails(organization, Authority.USER_AUTHORITY);
    }

    private Set<String> getEmails(Organization organization, Authority authority) {
        final UserSearch search = new UserSearch();
        search.setOrganization(organization);
        search.setAuthority(authority);
        final List<User> users = userManagementService.getUserManagementStrategy().getAll(search).getResult();
        return getEmails(users);
    }

    static public Set<String> getEmails(List<User> users) {
        final Set<String> emails = new HashSet<>();
        for (User user : users) {
            if(user.getEmail() != null) {
                emails.add(user.getEmail());
            }
        }
        return emails;
    }

}
