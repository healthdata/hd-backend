/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.elasticsearch.client;

import be.wiv_isp.healthdata.orchestration.elasticsearch.client.impl.DeleteRequest;
import be.wiv_isp.healthdata.orchestration.elasticsearch.client.impl.ElasticSearchBulkResponse;
import be.wiv_isp.healthdata.orchestration.elasticsearch.client.impl.IndexingRequest;


public interface IElasticSearchClient {

	boolean status();
	
	boolean indexExists(String index);

	boolean typeExists(String index, String type);

	void createIndex(String index, String settings);

	void putMapping(String index, String type, String mapping);

	ElasticSearchBulkResponse index(IndexingRequest... indexingRequest);

	String search(String[] indices, String[] types, String searchQuery);

	void deleteIndex(String index);

	void deleteAllIndices();

	ElasticSearchBulkResponse delete(DeleteRequest... requests);

	Long getDocumentCount();

}
