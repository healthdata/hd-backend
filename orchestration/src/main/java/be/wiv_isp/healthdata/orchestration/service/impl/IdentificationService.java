/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.orchestration.domain.Identification;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IIdentificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;

@Service
public class IdentificationService implements IIdentificationService {

	private static final Logger LOG = LoggerFactory.getLogger(IdentificationService.class);
	
	@Autowired
	private IConfigurationService configurationService;

	@Override
	public Identification getEtkIdentification(Organization organization) {
		final String type = configurationService.get(ConfigurationKey.IDENTIFICATION_TYPE, organization).getValue();
		final String value = configurationService.get(ConfigurationKey.IDENTIFICATION_VALUE, organization).getValue();
		final String applicationId = configurationService.get(ConfigurationKey.APPLICATION_ID, organization).getValue();

		final Identification identification = new Identification();
		identification.setType(type);
		identification.setValue(value);
		identification.setApplicationId(applicationId);
		return identification;
	}

	@Override
	public HealthDataIdentification getHealthDataIdentification(Organization organization) {
		final HealthDataIdentification identification = new HealthDataIdentification();
		identification.setType(organization.getHealthDataIDType());
		identification.setValue(organization.getHealthDataIDValue());
		identification.setName(organization.getName());
		identification.setHd4prc(organization.isHd4prc());
		return identification;
	}

	@Override
	public Identification getHd4ResIdentification() {
		final String value = configurationService.get(ConfigurationKey.HD4RES_IDENTIFICATION_VALUE).getValue();
		final String type = configurationService.get(ConfigurationKey.HD4RES_IDENTIFICATION_TYPE).getValue();
		final String applicationId = configurationService.get(ConfigurationKey.HD4RES_APPLICATION_ID).getValue();

		LOG.debug(MessageFormat.format("Retrieved HD4RES identification (type: {0}, value: {1}, application id: {2})", type, value, applicationId));

		final Identification identification = new Identification();
		identification.setType(type);
		identification.setValue(value);
		identification.setApplicationId(applicationId);
		return identification;
	}

	@Override
	public Identification getEHealthCodageIdentification() {
		final String value = configurationService.get(ConfigurationKey.EHEALTH_CODAGE_IDENTIFICATION_VALUE).getValue();
		final String type = configurationService.get(ConfigurationKey.EHEALTH_CODAGE_IDENTIFICATION_TYPE).getValue();
		final String applicationId = configurationService.get(ConfigurationKey.EHEALTH_CODAGE_APPLICATION_ID).getValue();
		final String quality = "INSTITUTION";

		final Identification identification = new Identification();
		identification.setType(type);
		identification.setValue(value);
		identification.setApplicationId(applicationId);
		return identification;
	}
}
