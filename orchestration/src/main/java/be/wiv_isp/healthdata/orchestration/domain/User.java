/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.common.util.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.text.MessageFormat;
import java.util.*;

@Entity
@Table(name = "USERS")
public class User implements HDUserDetails {
	private static final long serialVersionUID = 1L;

	public static final String ADMIN = "admin";
	public static final String SUPPORT_USER = "support_user";
	public static final String SUPPORT_ADMIN = "support_admin";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "USER_ID")
	private Long id;

	@Column(name = "USERNAME", nullable = false)
	private String username;

	@Column(name = "PASSWORD")
	private String password;

	@Column(name = "FIRST_NAME")
	protected String firstName;

	@Column(name = "LAST_NAME")
	protected String lastName;

	@Column(name = "EMAIL", nullable = false)
	protected String email;

	@Column(name = "ENABLED")
	private boolean enabled;

	@Fetch(FetchMode.JOIN)
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "USER_AUTHORITIES", joinColumns = @JoinColumn(name = "USER_ID"))
	@Column(name = "AUTHORITY")
	private Set<String> authorities;

	@Fetch(FetchMode.JOIN)
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "USER_DATA_COLLECTIONS", joinColumns = @JoinColumn(name = "USER_ID"))
	@Column(name = "DATA_COLLECTION_NAME")
	private Set<String> dataCollectionNames;

	@Fetch(FetchMode.JOIN)
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "USER_METADATA", joinColumns = @JoinColumn(name = "USER_ID"))
	@MapKeyColumn(name = "LOOKUP_KEY")
	@Column(name = "VALUE")
	private Map<String, String> metaData;

	@Column(name = "LDAP_USER")
	private boolean ldapUser;

	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "ORGANIZATION_ID")
	private Organization organization;

	@Fetch(FetchMode.SUBSELECT)
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.REMOVE }, orphanRemoval = true, mappedBy = "user")
	protected List<UserConfiguration> configurations = new ArrayList<>();

	@Fetch(FetchMode.SUBSELECT)
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.REMOVE }, orphanRemoval = true, mappedBy = "user")
	protected List<DataCollectionAccessRequest> dataCollectionAccessRequests = new ArrayList<>();

	@Fetch(FetchMode.SUBSELECT)
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.REMOVE }, orphanRemoval = true, mappedBy = "user")
	protected List<PasswordResetToken> passwordResetTokens = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String getUsername() {
		return this.username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String getPassword() {
		return this.password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isLdapUser() {
		return ldapUser;
	}

	public void setLdapUser(Boolean ldapUser) {
		this.ldapUser = ldapUser;
	}

	@Override
	public Set<Authority> getAuthorities() {
		if (this.authorities == null) {
			return new HashSet<>();
		}

		final Set<Authority> authorities = new HashSet<>();
		for (final String authority : this.authorities) {
			authorities.add(new Authority(authority));
		}
		return authorities;
	}

	public void setAuthorities(Set<Authority> authorities) {
		if (this.authorities == null) {
			this.authorities = new HashSet<>();
		} else {
			this.authorities.clear();
		}

		if(authorities != null) {
			for (Authority authority : authorities) {
				this.authorities.add(authority.getAuthority());
			}
		}
	}

	public void addAuthority(Authority authority) {
		if(authorities == null) {
			authorities = new HashSet<>();
		}

		authorities.add(authority.getAuthority());
	}

	public Map<String, String> getMetaData() {
		if (metaData == null) {
			metaData = new HashMap<>();
		}

		return metaData;
	}

	public void setMetaData(Map<String, String> metaData) {
		this.metaData = metaData;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	public Set<String> getDataCollectionNames() {
		return dataCollectionNames;
	}

	public void setDataCollectionNames(Set<String> dataCollectionNames) {
		if (this.dataCollectionNames == null) {
			this.dataCollectionNames = new HashSet<>();
		} else {
			this.dataCollectionNames.clear();
		}
		this.dataCollectionNames.addAll(dataCollectionNames);
	}

	@Override
	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public boolean isAccessGranted(String dataCollectionName) {
		for (String myDataCollectionName : dataCollectionNames) {
			if(myDataCollectionName.equalsIgnoreCase(dataCollectionName)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isAdmin() {
		return hasAuthority(Authority.ADMIN);
	}

	@Override
	public boolean isUser() {
		return hasAuthority(Authority.USER);
	}

	@Override
	public boolean isSupport() {
		return hasAuthority(Authority.SUPPORT);
	}

	@Override
	public boolean isHd4prc() {
		return hasAuthority(Authority.HD4PRC);
	}

	@Override
	public boolean isSuperAdmin() {
		return hasAuthority(Authority.SUPER_ADMIN);
	}

	private boolean hasAuthority(String authorityToCheck) {
		if(CollectionUtils.isEmpty(authorities)) {
			return false;
		}
		for (String authority : authorities) {
			if(authorityToCheck.equalsIgnoreCase(authority)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof User)) return false;

		User user = (User) o;

		if (enabled != user.enabled) return false;
		if (id != null ? !id.equals(user.id) : user.id != null) return false;
		if (username != null ? !username.equals(user.username) : user.username != null) return false;
		if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null) return false;
		if (lastName != null ? !lastName.equals(user.lastName) : user.lastName != null) return false;
		if (email != null ? !email.equals(user.email) : user.email != null) return false;
		if (authorities != null ? !authorities.equals(user.authorities) : user.authorities != null) return false;
		if (dataCollectionNames != null ? !dataCollectionNames.equals(user.dataCollectionNames) : user.dataCollectionNames != null)
			return false;
		if (metaData != null ? !metaData.equals(user.metaData) : user.metaData != null) return false;
		return !(organization != null ? !organization.equals(user.organization) : user.organization != null);

	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (username != null ? username.hashCode() : 0);
		result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
		result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
		result = 31 * result + (email != null ? email.hashCode() : 0);
		result = 31 * result + (enabled ? 1 : 0);
		result = 31 * result + (authorities != null ? authorities.hashCode() : 0);
		result = 31 * result + (dataCollectionNames != null ? dataCollectionNames.hashCode() : 0);
		result = 31 * result + (metaData != null ? metaData.hashCode() : 0);
		result = 31 * result + (organization != null ? organization.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return StringUtils.toStringExclude(this, "password", "configurations", "views", "dataCollectionAccessRequests", "passwordResetTokens");
	}

	public String toStringCompact() {
		return MessageFormat.format("[id={0},username={1}]", id, username);
	}

	public class MetadataKey {
		public static final String LAST_NOTIFICATION_TIME = "LAST_NOTIFICATION_TIME";
	}
}