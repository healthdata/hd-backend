/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.util.constant;

public class EHealthCodageConstants {
	public static final String ENCODE_SUBJECT = "[Batch_Codage_Encode]";
	public static final String DECODE_SUBJECT = "[Batch_Codage_Decode]";
	
	public class MetaData {
		public static final String TTP_PROJECT = "[TTP_Project]";
		public static final String FILE_NAME = "[File_name]";
	}
}
