/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.domain.search;

import be.wiv_isp.healthdata.common.domain.search.EntitySearch;

import java.util.Objects;

public class OrganizationSearch extends EntitySearch {

	private String healthDataIDType;
	private String healthDataIDValue;
	private Boolean main;
	private Boolean deleted = false;

	public OrganizationSearch() {
		this.distinct = true;
	}

	public String getHealthDataIDType() {
		return healthDataIDType;
	}

	public void setHealthDataIDType(String healthDataIDType) {
		this.healthDataIDType = healthDataIDType;
	}

	public String getHealthDataIDValue() {
		return healthDataIDValue;
	}

	public void setHealthDataIDValue(String healthDataIDValue) {
		this.healthDataIDValue = healthDataIDValue;
	}

	public Boolean isMain() {
		return main;
	}

	public void setMain(Boolean main) {
		this.main = main;
	}

	public Boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	@Override
	public int hashCode() {
		return Objects.hash( //
				this.healthDataIDType, //
				this.healthDataIDValue, //
				this.main, //
				this.deleted);
	}

	@Override
	public String toString() {
		return "OrganizationSearch {" + //
				"healthDataIDType = " + Objects.toString(this.healthDataIDType) + ", " + //
				"healthDataIDValue = " + Objects.toString(this.healthDataIDValue) + ", " + //
				"main = " + Objects.toString(this.main) + ", " + //
				"deleted = " + Objects.toString(this.deleted) + "}";
	}
}
