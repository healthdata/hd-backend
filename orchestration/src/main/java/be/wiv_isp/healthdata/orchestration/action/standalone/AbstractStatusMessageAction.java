/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.action.standalone;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.orchestration.action.StatusMessageContent;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.StandAloneActionType;

public abstract class AbstractStatusMessageAction extends AbstractStandAloneAction {

	protected StatusMessageContent content;
	protected HealthDataIdentification healthDataIdentification;
	protected Message message;

	public StatusMessageContent getContent() {
		return this.content;
	}

	public void setContent(StatusMessageContent content) {
		this.content = content;
	}

	public HealthDataIdentification getHealthDataIdentification() {
		return healthDataIdentification;
	}

	public void setHealthDataIdentification(HealthDataIdentification healthDataIdentification) { this.healthDataIdentification = healthDataIdentification; }

	@Override
	public StandAloneActionType getAction() {
		return StandAloneActionType.STATUS_MESSAGE_ACTION;
	}

	@Override
	public void extractInfo(Message message) {
		this.message = message;
	}

}