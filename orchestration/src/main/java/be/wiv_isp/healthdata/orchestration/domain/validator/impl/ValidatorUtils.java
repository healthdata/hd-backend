/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.domain.validator.impl;

import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;

import java.util.Collection;

public class ValidatorUtils {

    public static void verifyNotNull(Object object, String propertyName) {
        if(object == null) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.MISSING_PROPERTY, propertyName);
            throw exception;
        }
    }

    public static void verifyNotBlank(String value, String propertyName) {
        if(StringUtils.isBlank(value)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.MISSING_PROPERTY, propertyName);
            throw exception;
        }
    }

    public static void verifyNotEmpty(Collection collection, String propertyName) {
        if(CollectionUtils.isEmpty(collection)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.MISSING_PROPERTY, propertyName);
            throw exception;
        }
    }

    public static void validateEmail(String email) {
        verifyNotBlank(email, "email");
        if(!isEmailValid(email)) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_PROPERTY, "email", email, "invalid email address");
            throw exception;
        }
    }

    public static boolean isEmailValid(String email) {
        return EmailValidator.getInstance().isValid(email);
    }
}
