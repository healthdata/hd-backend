/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.domain.enumeration.TemplateKey;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.orchestration.dao.IDataCollectionAccessRequestDao;
import be.wiv_isp.healthdata.orchestration.domain.DataCollectionAccessRequest;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.search.DataCollectionAccessRequestSearch;
import be.wiv_isp.healthdata.orchestration.domain.mail.DataCollectionAccessRequestContext;
import be.wiv_isp.healthdata.orchestration.service.*;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class DataCollectionAccessRequestService extends AbstractService<DataCollectionAccessRequest, Long, DataCollectionAccessRequestSearch, IDataCollectionAccessRequestDao> implements IDataCollectionAccessRequestService {

    private static final Logger LOG = LoggerFactory.getLogger(DataCollectionAccessRequestService.class);

    @Autowired
    private IDataCollectionAccessRequestDao dao;

    @Autowired
    private IUserService userService;

    @Autowired
    private IMailService mailService;

    @Autowired
    private IConfigurationService configurationService;

    @Autowired
    private IUserManagementService userManagementService;

    public DataCollectionAccessRequestService() {
        super(DataCollectionAccessRequest.class);
    }

    @Override
    protected IDataCollectionAccessRequestDao getDao() {
        return dao;
    }

    @Override
    public DataCollectionAccessRequest get(Long id) {
        HealthDataException exception = new HealthDataException();
        exception.setExceptionType(ExceptionType.METHOD_NOT_ALLOWED, "get by id");
        throw exception;
    }

    @Override
    @Transactional
    public DataCollectionAccessRequest create(DataCollectionAccessRequest request) {
        validateBeforeCreate(request);
        final DataCollectionAccessRequest created = super.create(request);
        notifyAdmins(request);
        return created;
    }

    @Override
    @Transactional
    public DataCollectionAccessRequest process(final DataCollectionAccessRequest request, final Map<String, Boolean> decisions) {
        LOG.info("Processing decisions for data collection access request with id {}", request.getId());
        LOG.debug("Decisions: {}", decisions);

        validateBeforeProcessing(request, decisions);

        final Set<String> acceptedDataCollections = getAcceptedDataCollections(decisions);
        LOG.debug("Accepted data collections: {}", acceptedDataCollections);

        if (CollectionUtils.isNotEmpty(acceptedDataCollections)) {
            final User user = request.getUser();
            final Collection<String> newDataCollections = CollectionUtils.removeAll(acceptedDataCollections, user.getDataCollectionNames());
            if (CollectionUtils.isNotEmpty(newDataCollections)) {
                /* In the UserService is verified whether the organization has access to the given data collections. Therefore, it can happen that the set of
                 * data collections that was effectively added to the user is different from the set of data collections that we want to add in this method.
                 * Therefore, to determine which data collections were effectively added, we compare the data collections the user has access to before and
                 * after update operation. */
                final Set<String> userDataCollectionsBefore = new HashSet<>(user.getDataCollectionNames());
                user.getDataCollectionNames().addAll(newDataCollections);
                final User updatedUser = userService.update(user);
                final Collection<String> addedDataCollections = CollectionUtils.removeAll(updatedUser.getDataCollectionNames(), userDataCollectionsBefore);
                LOG.debug("Added data collections: {}", addedDataCollections);
            } else {
                LOG.debug("The user already have access to all accepted data collections");
            }
        } else {
            LOG.debug("All data collection accesses were rejected");
        }

        request.setDataCollections(decisions);
        request.setProcessed(true);
        final DataCollectionAccessRequest processed = dao.update(request);

        notifyRequester(request);

        LOG.info("Data collection access request successfully processed");
        return processed;
    }

    private Set<String> getAcceptedDataCollections(final Map<String, Boolean> decisions) {
        final Set<String> acceptedDataCollections = new HashSet<>();
        for (final Map.Entry<String, Boolean> entry : decisions.entrySet()) {
            if(Boolean.TRUE.equals(entry.getValue())) {
                acceptedDataCollections.add(entry.getKey());
            }
        }
        return acceptedDataCollections;
    }

    private void validateBeforeCreate(DataCollectionAccessRequest request) {
        if(request.getDataCollections() == null || request.getDataCollections().isEmpty()) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_INPUT, "a data collection access request must contain at least one data collection");
            throw exception;
        }
    }

    private void validateBeforeProcessing(final DataCollectionAccessRequest request, final Map<String, Boolean> decisions) {
        if(request.isProcessed()) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_INPUT, "request has already been processed");
            throw exception;
        }

        if(!decisions.keySet().containsAll(request.getDataCollections().keySet())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_INPUT, "a decision must be provided for all requested data collection");
            throw exception;
        }

        for (final Boolean b : decisions.values()) {
            if(!Boolean.TRUE.equals(b) && !Boolean.FALSE.equals(b)) {
                HealthDataException exception = new HealthDataException();
                exception.setExceptionType(ExceptionType.INVALID_INPUT, "a decision must be either contain the value true or false");
                throw exception;
            }
        }
    }

    private void notifyAdmins(DataCollectionAccessRequest request) {
        final DataCollectionAccessRequestContext context = new DataCollectionAccessRequestContext();
        context.setLastName(request.getUser().getLastName());
        context.setFirstName(request.getUser().getFirstName());
        context.setRequestedDataCollections(request.getDataCollections().keySet());
        context.setInstallationUrl(configurationService.get(ConfigurationKey.GUI_HOST).getValue());
        final Set<String> to = userManagementService.getUserManagementStrategy().getEmailAdmins(request.getUser().getOrganization());
        mailService.createAndSendMail(TemplateKey.DATA_COLLECTION_ACCESS_REQUEST_NEW, context, to);
    }

    private void notifyRequester(DataCollectionAccessRequest request) {
        final DataCollectionAccessRequestContext context = new DataCollectionAccessRequestContext();
        context.setLastName(request.getUser().getLastName());
        context.setFirstName(request.getUser().getFirstName());
        context.setRequestedDataCollections(request.getDataCollections().keySet());
        context.setDecisions(request.getDataCollections());
        context.setInstallationUrl(configurationService.get(ConfigurationKey.GUI_HOST).getValue());
        final Set<String> to = new HashSet<>(Collections.singletonList(request.getUser().getEmail()));
        mailService.createAndSendMail(TemplateKey.DATA_COLLECTION_ACCESS_REQUEST_RESULT, context, to);
    }
}
