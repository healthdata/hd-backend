/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.orchestration.domain.Task;
import be.wiv_isp.healthdata.orchestration.domain.search.TaskSearch;
import be.wiv_isp.healthdata.orchestration.service.ITaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;
import java.util.List;

@Component
@Path("/tasks")
public class TaskRestService {

    @Autowired
    private ITaskService taskService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@Context UriInfo info) {
        return Response.ok(getTasks(info)).build();
    }

    private List<Task> getTasks(UriInfo info) {
        final TaskSearch search = new TaskSearch();
        final MultivaluedMap<String, String> params = info.getQueryParameters();

        for (String key : params.keySet()) {
            final String value = params.getFirst(key);
            if ("status".equalsIgnoreCase(key)) {
                search.setStatus(Task.Status.valueOf(value));
            }
            if ("name".equalsIgnoreCase(key)) {
                search.setName(value);
            }
        }
        return taskService.getAll(search);
    }
}
