/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.domain;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.text.MessageFormat;
import java.util.Objects;

@Embeddable
@JsonSerialize(include = Inclusion.NON_NULL)
public class HealthDataIdentification {

	@Column(name = "HD_IDENTIFICATION_TYPE", nullable = false)
	private String type;

	@Column(name = "HD_IDENTIFICATION_VALUE", nullable = false)
	private String value;

	@Column(name = "HD_IDENTIFICATION_NAME", nullable = false)
	private String name;

	@Column(name = "HD4PRC", nullable = false)
	private Boolean hd4prc = false;

	public HealthDataIdentification() {
	}

	public HealthDataIdentification(Organization organization) {
		setType(organization.getHealthDataIDType());
		setValue(organization.getHealthDataIDValue());
		setName(organization.getName());
		setHd4prc(organization.isHd4prc());
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getHd4prc() {
		return hd4prc;
	}

	public void setHd4prc(Boolean hd4prc) {
		this.hd4prc = hd4prc;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof HealthDataIdentification) {
			HealthDataIdentification other = (HealthDataIdentification) o;

			return Objects.equals(type, other.type)
					&& Objects.equals(value, other.value);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				this.type,
				this.value,
				this.name);
	}

	@Override
	public String toString() {
		return MessageFormat.format("[{0} - {1} - {2}]", name, value, type);
	}
}
