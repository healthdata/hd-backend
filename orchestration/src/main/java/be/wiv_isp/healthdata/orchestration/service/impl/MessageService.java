/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.domain.search.Field;
import be.wiv_isp.healthdata.common.domain.search.Ordering;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.orchestration.dao.IMessageDao;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.MessageType;
import be.wiv_isp.healthdata.orchestration.domain.search.GroupByMessage;
import be.wiv_isp.healthdata.orchestration.domain.search.MessageSearch;
import be.wiv_isp.healthdata.orchestration.service.IMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MessageService extends AbstractService<Message, Long, MessageSearch, IMessageDao> implements IMessageService {
	
	@Autowired
	private IMessageDao dao;

	public MessageService() {
		super(Message.class);
	}

	@Override
	protected IMessageDao getDao() {
		return dao;
	}

	@Override
	public List<Message> getAll() {
		return getAll(new MessageSearch());
	}

	@Override
	public List<Message> getAll(MessageSearch search) {
		if(search.getOrdering() == null) {
			// default behaviour: order by sentOn attribute
			search.setOrdering(new Ordering(Field.Message.SENT_ON, Ordering.Type.ASC));
		}
		return dao.getAll(search);
	}

	@Override
	public Map<MessageStatus, Long> getCountByStatuses(Organization organization) {
		final List<GroupByMessage> countByStatuses = dao.getCountByStatuses(organization);

		final Map<MessageStatus, Long> res = new HashMap<>();
		for(final GroupByMessage gbm : countByStatuses) {
			res.put(gbm.getStatus(), gbm.getCount());
		}

		// add statuses with count 0
		for(final MessageStatus status : MessageStatus.values()) {
			if(!res.keySet().contains(status)) {
				res.put(status, 0L);
			}
		}

		return res;
	}

	@Override
	public Message getLast(String identificationValue) {
		final MessageSearch search = new MessageSearch();
		search.setIdentificationValue(identificationValue);
		search.setMaxResults(1);
		search.setOrdering(new Ordering(Field.CREATED_ON, Ordering.Type.DESC));
		return getUnique(search);
	}

	@Override
	public Message getLast(Long workflowId, MessageType type) {
		final MessageSearch search = new MessageSearch();
		search.setWorkflowId(workflowId);
		search.setType(type);
		search.setMaxResults(1);
		search.setOrdering(new Ordering(Field.CREATED_ON, Ordering.Type.DESC));
		return getUnique(search);
	}

	@Override
	@Transactional
	public void delete(Long messageId) {
		final Message m = dao.get(messageId);
		dao.delete(m);
	}
}
