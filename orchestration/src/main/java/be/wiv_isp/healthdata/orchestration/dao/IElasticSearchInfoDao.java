/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.dao;

import be.wiv_isp.healthdata.common.dao.ICrudDaoV2;
import be.wiv_isp.healthdata.orchestration.domain.AbstractElasticSearchInfo;
import be.wiv_isp.healthdata.orchestration.domain.AbstractElasticSearchInfo.Status;
import be.wiv_isp.healthdata.orchestration.domain.ElasticSearchInfoCountDetails;
import be.wiv_isp.healthdata.orchestration.domain.search.ElasticSearchInfoSearch;

import java.util.List;

public interface IElasticSearchInfoDao<ElasticSearchInfo extends AbstractElasticSearchInfo> extends ICrudDaoV2<ElasticSearchInfo, Long, ElasticSearchInfoSearch> {

	void updateAll(Status status);

	void updateAll(ElasticSearchInfoSearch search, Status toStatus);

	List<ElasticSearchInfoCountDetails> countDetailed();

	List<Long> getWorkflowIds(Status status);

}
