/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.action.workflow;

import be.wiv_isp.healthdata.common.dto.DataCollectionDefinitionDtoV7;
import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.ByteArrayToJsonSerializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import be.wiv_isp.healthdata.orchestration.domain.*;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatusTransitionType;
import be.wiv_isp.healthdata.orchestration.domain.search.IAbstractWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.dto.AttachmentDto;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import be.wiv_isp.healthdata.orchestration.dto.NoteMessageDto;
import be.wiv_isp.healthdata.orchestration.service.IAbstractWorkflowService;
import be.wiv_isp.healthdata.orchestration.service.IUserDataCollectionService;
import be.wiv_isp.healthdata.orchestration.service.IVersionService;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public abstract class AbstractWorkflowAction<Workflow extends AbstractWorkflow, WorkflowHistory extends AbstractWorkflowHistory> implements WorkflowAction<Workflow> {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractWorkflowAction.class);

	public static final String UPDATED_ON = "updatedOn";
	public static final String METADATA_KEY_UPDATED_ON = "UPDATED_ON";

	protected Long workflowId;
	protected Long dataCollectionDefinitionId;
	protected DataCollectionDefinitionDtoV7 dataCollectionDefinition;
	protected WorkflowFlags flags;
	protected WorkflowStatus endStatus;
	protected String sendStatus = SendStatus.NA;
	protected String uniqueID;
	protected String registryDependentId;
	protected String updatedOn;
	protected Map<String, String> metaData = new HashMap<>();
	protected HealthDataIdentification identificationWorkflow;
	protected byte[] documentContent;
	protected DocumentData documentData;
	protected Long documentVersion;
	protected Long noteMaxOrder;
	protected List<FollowUp> followUps;
	protected List<Attachment> attachments;
	protected List<AttachmentDto> attachmentDtos;
	protected String file;
	protected List<NoteMessageDto> notesMessage = new ArrayList<>();
	protected Map<String,String> dataSources = new HashMap<>();
	protected Progress progress;
	protected String source = DataSource.MANUAL;
	protected JSONObject computedExpressions;
	protected Map<String, JSONObject> affectedFields;
	protected Message message;
	protected String messageType;
	protected Map<String,TypedValue> toBeCoded;
	protected Map<String,TypedValue> codedData;
	protected String ehBoxCodedNiss; // TODO: is this still necessary?
	protected String hd4dpWorkflowId; // TODO: is this still necessary?
	protected Timestamp startedOn;
	protected Timestamp completedOn;

	@Autowired
	@Qualifier("transactionManager")
	protected PlatformTransactionManager txManager;

	@Autowired
	protected IUserDataCollectionService userDataCollectionService;

	@Autowired
	private IVersionService versionService;

	@Override
	public Workflow retrieveWorkflow() {
		if (workflowId == null) {
			return null;
		}

		final Workflow workflow = getWorkflowService().get(workflowId);

		if (workflow == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.GET_NON_EXISTING, "WORKFLOW", workflowId);
			throw exception;
		}

		return workflow;
	}

	@Override
	public Workflow preExecute(Workflow workflow, HDUserDetails userDetails) {
		if(!getWorkflowService().isActionAvailable(workflow, this.getAction())) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.ACTION_NOT_ALLOWED, this.getAction());
			throw exception;
		}

		if (workflow != null) {
			if(!userDataCollectionService.isUserAuthorized(userDetails, workflow)) {
				HealthDataException exception = new HealthDataException();
				exception.setExceptionType(ExceptionType.UNAUTHORIZED_ACTION_ON_WORKFLOW, userDetails.getUsername(), workflow.getId());
				throw exception;
			}
		}

		return workflow;
	}

	@Override
	public final Workflow execute(final Workflow workflow, final HDUserDetails userDetails) {
		// need to define an explicit transaction (no annotation) since action is autowired using the ActionFactory
		return new TransactionTemplate(txManager).execute(new TransactionCallback<Workflow>() {
			@Override
			public Workflow doInTransaction(TransactionStatus status) {
				WorkflowStatus startStatus = null;
				Workflow w = workflow;

				if(w != null) {
					startStatus = w.getStatus();
					w = getWorkflowService().get(w.getId());
				}

				w = doExecute(w, userDetails);
				final WorkflowStatus endStatus = getEndStatus();

				if(getAction().modifiesWorkflowState()) {
					final WorkflowStatusTransitionType transitionType = WorkflowStatus.getTransitionType(startStatus, endStatus);
					switch (transitionType) {
						case WARNING:
							LOG.warn("Workflow status changed from {} to {}", startStatus, endStatus);
							break;
						case ERROR:
							LOG.error("Workflow status changed from {} to {}", startStatus, endStatus);
							break;
						default:
							LOG.debug("Workflow status changed from {} to {}", startStatus, endStatus);
					}

					checkForParallelModification(w); // must come before createWorkflowHistory() so the updatedOn timestamp doesn't change

					final WorkflowHistory history = createWorkflowHistory(userDetails.getUsername(), transitionType, w);
					w.addHistory(history);
					w.setStatus(endStatus);
					w.setSendStatus(getSendStatus());

					// explicitly set updatedOn on the workflow since the @PreUpdate does not run if the entity is not modified.
					w.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
				}

				LOG.debug("Updating workflow");
				return getWorkflowService().update(w);
			}
		});
	}

	private void checkForParallelModification(Workflow workflow) {
		LOG.debug("Checking for parallel modification");

		String updatedOnAsString = getUpdatedOn();
		if (updatedOnAsString == null) {
			if (metaData == null)
				return;
			updatedOnAsString = metaData.get(METADATA_KEY_UPDATED_ON);
			if (updatedOnAsString == null)
				return;
		}

		Timestamp updatedOn;

		try {
			updatedOn = new Timestamp(new SimpleDateFormat(DateFormat.DATE_AND_TIME.getPattern()).parse(updatedOnAsString).getTime());
		} catch (ParseException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.INVALID_DATE_FORMAT, updatedOnAsString, DateFormat.DATE_AND_TIME.getPattern());
			throw exception;

		}

		final Timestamp existingUpdatedOn = getWorkflowService().get(workflow.getId()).getUpdatedOn();
		existingUpdatedOn.setTime(existingUpdatedOn.getTime() / 1000 * 1000); // remove unnecessary milliseconds

		if (existingUpdatedOn.after(updatedOn)) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.PARALLEL_MODIFICATION, existingUpdatedOn, updatedOn);
			throw exception;
		}

		// not clean, but will be removed in the future anyway
		if (metaData != null)
			metaData.remove(METADATA_KEY_UPDATED_ON);

	}

	private WorkflowHistory createWorkflowHistory(String userName, WorkflowStatusTransitionType transitionType, Workflow workflow) {
		LOG.debug("Creating history entry");
		final WorkflowHistory workflowHistory = getWorkflowHistoryInstance();
		workflowHistory.setAction(getAction());
		workflowHistory.setNewStatus(getEndStatus());
		workflowHistory.setExecutedBy(userName);
		workflowHistory.setTransitionType(transitionType);
		workflowHistory.setCommit(versionService.getVersionDetails().getBackendCommit());
		if (this instanceof ReportableAction) {
			final ReportableAction reportableAction = (ReportableAction) this;
			workflowHistory.setSource(reportableAction.getSource());
			workflowHistory.setFile(reportableAction.getFile());
			workflowHistory.setMessageType(reportableAction.getMessageType());
			if (reportableAction.getProgress() != null) {
				if (workflowHistory instanceof RegistrationWorkflowHistory) {
					RegistrationProgress registrationProgress = new RegistrationProgress(reportableAction.getProgress());
					registrationProgress.setWorkflowHistory(workflowHistory);
					workflowHistory.setProgress(registrationProgress);
				}
				else if (workflowHistory instanceof ParticipationWorkflowHistory) {
					ParticipationProgress participationProgress = new ParticipationProgress(reportableAction.getProgress());
					participationProgress.setWorkflowHistory(workflowHistory);
					workflowHistory.setProgress(participationProgress);
				}
			}
		}
		setWorkflowFlags(workflowHistory, workflow);
		return workflowHistory;
	}

	protected abstract Workflow doExecute(Workflow workflow, HDUserDetails userDetails);
	protected abstract WorkflowHistory getWorkflowHistoryInstance();
	protected abstract void setWorkflowFlags(WorkflowHistory workflowHistory, Workflow workflow);
	protected abstract IAbstractWorkflowService<Workflow, ? extends IAbstractWorkflowSearch> getWorkflowService();

	@Override
	public void extractInfo(Message message) {
		// Nothing to do. Sub classes must implement this method if necessary
	}

	@Override
	public String toString() {
		return getAction().toString();
	}


	public Long getWorkflowId() {
		return workflowId;
	}

	public void setWorkflowId(Long workflowId) {
		this.workflowId = workflowId;
	}

	@Override
	public WorkflowStatus getEndStatus() {
		return endStatus;
	}

	public void setEndStatus(WorkflowStatus endStatus) {
		this.endStatus = endStatus;
	}

	@Override
	public String getSendStatus() {
		return sendStatus;
	}

	public void setSendStatus(String sendStatus) {
		this.sendStatus = sendStatus;
	}

	@Override
	public Map<String, String> getMetaData() {
		return metaData;
	}

	public void setMetaData(Map<String, String> metaData) {
		this.metaData = metaData;
	}

	@Override
	public String getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Long getDataCollectionDefinitionId() {
		return dataCollectionDefinitionId;
	}

	public void setDataCollectionDefinitionId(Long dataCollectionDefinitionId) {
		this.dataCollectionDefinitionId = dataCollectionDefinitionId;
	}

	public HealthDataIdentification getIdentificationWorkflow() {
		return identificationWorkflow;
	}

	public void setIdentificationWorkflow(HealthDataIdentification identificationWorkflow) {
		this.identificationWorkflow = identificationWorkflow;
	}

	public Long getDocumentVersion() {
		return documentVersion;
	}

	public void setDocumentVersion(Long documentVersion) {
		this.documentVersion = documentVersion;
	}

	public String getHd4dpWorkflowId() {
		return hd4dpWorkflowId;
	}

	public void setHd4dpWorkflowId(String hd4dpWorkflowId) {
		this.hd4dpWorkflowId = hd4dpWorkflowId;
	}

	public Long getNoteMaxOrder() {
		return noteMaxOrder;
	}

	public void setNoteMaxOrder(Long noteMaxOrder) {
		this.noteMaxOrder = noteMaxOrder;
	}

	public List<NoteMessageDto> getNotesMessage() {
		return notesMessage;
	}

	public void setNotesMessage(List<NoteMessageDto> notesMessage) {
		this.notesMessage = notesMessage;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	@JsonSerialize(using = ByteArrayToJsonSerializer.class)
	public byte[] getDocumentContent() {
		return documentContent;
	}

	public void setDocumentContent(byte[] documentContent) {
		this.documentContent = documentContent;
	}

	public Map<String, String> getDataSources() {
		return dataSources;
	}

	public void setDataSources(Map<String, String> dataSources) {
		this.dataSources = dataSources;
	}

	public List<FollowUp> getFollowUps() {
		return followUps;
	}

	public void setFollowUps(List<FollowUp> followUps) {
		this.followUps = followUps;
	}

	public WorkflowFlags getFlags() {
		return flags;
	}

	public void setFlags(WorkflowFlags flags) {
		this.flags = flags;
	}

	public List<Attachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<Attachment> attachments) {
		this.attachments = attachments;
	}

	public List<AttachmentDto> getAttachmentDtos() {
		return attachmentDtos;
	}

	public void setAttachmentDtos(List<AttachmentDto> attachmentDtos) {
		this.attachmentDtos = attachmentDtos;
	}

	public String getUniqueID() {
		return uniqueID;
	}

	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Progress getProgress() {
		return progress;
	}

	public void setProgress(Progress progress) {
		this.progress = progress;
	}

	public Map<String, TypedValue> getToBeCoded() {
		return toBeCoded;
	}

	public void setToBeCoded(Map<String, TypedValue> toBeCoded) {
		this.toBeCoded = toBeCoded;
	}

	public DocumentData getDocumentData() {
		return documentData;
	}

	public void setDocumentData(DocumentData documentData) {
		this.documentData = documentData;
	}

	public JSONObject getComputedExpressions() {
		return computedExpressions;
	}

	public void setComputedExpressions(JSONObject computedExpressions) {
		this.computedExpressions = computedExpressions;
	}

	public Map<String, JSONObject> getAffectedFields() {
		return affectedFields;
	}

	public void setAffectedFields(Map<String, JSONObject> affectedFields) {
		this.affectedFields = affectedFields;
	}

	public String getEhBoxCodedNiss() {
		return ehBoxCodedNiss;
	}

	public void setEhBoxCodedNiss(String ehBoxCodedNiss) {
		this.ehBoxCodedNiss = ehBoxCodedNiss;
	}

	public Map<String, TypedValue> getCodedData() {
		return codedData;
	}

	public void setCodedData(Map<String, TypedValue> codedData) {
		this.codedData = codedData;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	@JsonSerialize(using = TimestampSerializer.class)
	public Timestamp getStartedOn() {
		return startedOn;
	}

	@JsonDeserialize(using = TimestampDeserializer.class)
	public void setStartedOn(Timestamp startedOn) {
		this.startedOn = startedOn;
	}

	@JsonSerialize(using = TimestampSerializer.class)
	public Timestamp getCompletedOn() {
		return completedOn;
	}

	@JsonDeserialize(using = TimestampDeserializer.class)
	public void setCompletedOn(Timestamp completedOn) {
		this.completedOn = completedOn;
	}
}
