/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.orchestration.dao.ICommentDao;
import be.wiv_isp.healthdata.orchestration.domain.Comment;
import be.wiv_isp.healthdata.orchestration.domain.search.CommentSearch;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CommentDao extends CrudDaoV2<Comment, Long, CommentSearch> implements ICommentDao {

    public CommentDao() {  super(Comment.class); }

    @Override
    protected List<Predicate> getPredicates(CommentSearch search, CriteriaBuilder cb, Root<Comment> rootEntry) {
        final List<Predicate> predicates = new ArrayList<>();

        if(search.getNoteId() != null) {
            predicates.add(cb.equal(rootEntry.get("noteId"), search.getNoteId()));
        }
        if(search.getOrder() != null) {
            predicates.add(cb.equal(rootEntry.get("order"), search.getOrder()));
        }
        if(search.getDeleted() != null) {
            predicates.add(cb.equal(rootEntry.get("deleted"), search.getDeleted()));
        }

        return predicates;
    }

    @Override
    public List<Comment> getByDocumentId(Long documentId) {
        TypedQuery<Comment> query = em.createNamedQuery("commentsByDocumentId", Comment.class);
        query.setParameter("documentId", documentId);
        return query.getResultList();
    }

    @Override
    public List<Comment> getByNoteId(Long noteId) {
        CommentSearch search = new CommentSearch();
        search.setNoteId(noteId);
        search.setDeleted(false);

        return getAll(search);
    }

}
