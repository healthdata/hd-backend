/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.orchestration.api.uri.DataReferenceUri;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IDataReferenceForwardService;
import com.sun.jersey.api.client.GenericType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;

@Component
@Path("/datareference")
public class DataReferenceRestService {

	@Autowired
	private IDataReferenceForwardService dataReferenceService;

	@Autowired
	private IConfigurationService configurationService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(@Context UriInfo info) {
		Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

		final DataReferenceUri dataReferenceUri = new DataReferenceUri();
		dataReferenceUri.setHost(catalogueHost.getValue());

		WebServiceBuilder wsb = new WebServiceBuilder();
		MultivaluedMap<String, String> queryParameters = info.getQueryParameters();
		for (String queryParameterkey : queryParameters.keySet()) {
			wsb.addParameter(queryParameterkey, queryParameters.getFirst(queryParameterkey));
		}

		wsb.setUrl(dataReferenceUri.toString());
		wsb.setGet(true);
		wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
		wsb.setReturnType(new GenericType<String>() {
		});

		String json = dataReferenceService.getDataReference(wsb);
		return Response.ok(json).build();
	}
}
