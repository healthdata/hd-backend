/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.security.authentication.strategy.impl;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.UserManagementType;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class DatabaseLdapUserManagementStrategy extends AbstractUserManagementStrategy {

    private static final Logger LOG = LoggerFactory.getLogger(DatabaseLdapUserManagementStrategy.class);

    @Override
    public User create(User user, Organization organization) {
        user.setLdapUser(true);
        return super.create(user, organization);
    }

    @Override
    public User read(Organization organization) {
        final String username = AuthenticationService.getUserDetails().getUsername();

        final UserSearch userSearch = new UserSearch();
        userSearch.setLdapUser(false);
        userSearch.setUsername(username);
        userSearch.setOrganization(organization);
        User user =  userService.getUnique(userSearch);
        if(user != null && (user.isSuperAdmin() || user.isSupport())) {
            return user;
        }else {
            final UserSearch ldapUserSearch = new UserSearch();
            ldapUserSearch.setLdapUser(true);
            ldapUserSearch.setUsername(username);
            ldapUserSearch.setOrganization(organization);
            return userService.getUnique(ldapUserSearch);
        }
    }

    @Override
    @Transactional
    public User update(Long id, User user) {
        user.setPassword(null);
        user.setLdapUser(true);
        return super.update(id, user);
    }

    @Override
    public User updatePassword(Long id, String oldPassword, String newPassword) {
        HealthDataException exception = new HealthDataException();
        exception.setExceptionType(ExceptionType.METHOD_NOT_ALLOWED, "update password when using LDAP authentication type");
        throw exception;
    }

    @Override
    public void sendPasswordResetInstructions(Organization organization, String email) {
        HealthDataException exception = new HealthDataException();
        exception.setExceptionType(ExceptionType.METHOD_NOT_ALLOWED, "reset password when using LDAP authentication type");
        throw exception;
    }

    @Override
    public void resetPassword(Organization organization, String token, String newPassword) {
        HealthDataException exception = new HealthDataException();
        exception.setExceptionType(ExceptionType.METHOD_NOT_ALLOWED, "reset password when using LDAP authentication type");
        throw exception;
    }

    @Override
    public boolean supports(UserManagementType userManagementType) {
        return UserManagementType.DATABASE_LDAP.equals(userManagementType);
    }

    @Override
    protected UserSearch updateSearch(UserSearch search) {
        search.setLdapUser(true);
        return search;
    }
}
