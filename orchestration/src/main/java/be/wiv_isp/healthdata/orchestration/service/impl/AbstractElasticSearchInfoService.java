/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.orchestration.dao.IElasticSearchInfoDao;
import be.wiv_isp.healthdata.orchestration.domain.AbstractElasticSearchInfo;
import be.wiv_isp.healthdata.orchestration.domain.AbstractElasticSearchInfo.Status;
import be.wiv_isp.healthdata.orchestration.domain.ElasticSearchInfoCountDetails;
import be.wiv_isp.healthdata.orchestration.domain.search.ElasticSearchInfoSearch;
import be.wiv_isp.healthdata.orchestration.service.IElasticSearchInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

abstract public class AbstractElasticSearchInfoService<ElasticSearchInfo extends AbstractElasticSearchInfo> extends AbstractService<ElasticSearchInfo, Long, ElasticSearchInfoSearch, IElasticSearchInfoDao<ElasticSearchInfo>> implements IElasticSearchInfoService<ElasticSearchInfo> {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractElasticSearchInfoService.class);
	
	@Autowired
	private IElasticSearchInfoDao<ElasticSearchInfo> dao;

	public AbstractElasticSearchInfoService(Class<ElasticSearchInfo> clazz) {
		super(clazz);
	}

	@Override
	public IElasticSearchInfoDao<ElasticSearchInfo> getDao() {
		return dao;
	}

	@Override
	@Transactional
	public void updateAll(Status status) {
		dao.updateAll(status);
	}

	@Override
	@Transactional
	public void updateAll(ElasticSearchInfoSearch search, Status toStatus) {
		dao.updateAll(search, toStatus);
	}

	@Override
	public Long count(ElasticSearchInfoSearch search) {
		return dao.count(search);
	}

	@Override
	public List<ElasticSearchInfoCountDetails> countDetailed() {
		return dao.countDetailed();
	}

	@Override
	public List<Long> getWorkflowIds(Status status) {
		return dao.getWorkflowIds(status);
	}

}
