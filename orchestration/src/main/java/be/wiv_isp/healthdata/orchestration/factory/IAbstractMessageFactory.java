/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.factory;

import be.wiv_isp.healthdata.orchestration.action.workflow.WorkflowAction;
import be.wiv_isp.healthdata.orchestration.domain.AbstractParticipationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.Message;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.dto.EncryptionModuleMessage;

public interface IAbstractMessageFactory<RegistrationWorkflow extends AbstractRegistrationWorkflow, ParticipationWorkflow extends AbstractParticipationWorkflow> {

	Message createOutgoingMessage(WorkflowAction action, RegistrationWorkflow workflow);

	Message createOutgoingMessage(WorkflowAction action, ParticipationWorkflow workflow);

	Message createIncomingMessage(EncryptionModuleMessage encryptionModuleMessage, Organization organization);

	EncryptionModuleMessage createEncryptionModuleMessage(Message message);

	EncryptionModuleMessage createAcknowledgment(Message message);

	Long getAcknowledgedMessageId(Message message);

}
