/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.mapper;

import be.wiv_isp.healthdata.orchestration.domain.WorkflowFlags;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.DisplayableWorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;

import java.util.*;

public class WorkflowStatusMapper {

    public static Set<DisplayableWorkflowStatus> map(WorkflowStatus status, WorkflowFlags flags) {
        switch (status) {
            case ACTION_NEEDED:
                final Set<DisplayableWorkflowStatus> set1 = new HashSet<>();
                if(flags.isCorrections()) {
                    set1.add(DisplayableWorkflowStatus.CORRECTIONS_NEEDED);
                }
                if(flags.isFollowUp()) {
                    set1.add(DisplayableWorkflowStatus.FOLLOW_UP_NEEDED);
                }
                return set1;
            case NEW:
                final Set<DisplayableWorkflowStatus> set2 = new HashSet<>();
                if(flags.isCorrections()) {
                    set2.add(DisplayableWorkflowStatus.NEW_CORRECTION);
                }
                if(flags.isFollowUp()) {
                    set2.add(DisplayableWorkflowStatus.NEW_FOLLOW_UP);
                }
                if(set2.isEmpty()) {
                    set2.add(DisplayableWorkflowStatus.NEW);
                }
                return set2;
            case IN_PROGRESS: return new HashSet<>(Collections.singletonList(DisplayableWorkflowStatus.OPEN));
            case SUBMITTED: return new HashSet<>(Collections.singletonList(DisplayableWorkflowStatus.SUBMITTED));
            case ANNOTATIONS_NEEDED: return new HashSet<>(Collections.singletonList(DisplayableWorkflowStatus.COMMENTS_NEEDED));
            case CORRECTIONS_REQUESTED: return new HashSet<>(Collections.singletonList(DisplayableWorkflowStatus.CORRECTIONS_REQUESTED));
            case OUTBOX: return new HashSet<>(Collections.singletonList(DisplayableWorkflowStatus.SENDING));
            case APPROVED: return new HashSet<>(Collections.singletonList(DisplayableWorkflowStatus.APPROVED));
            case DELETED: return new HashSet<>(Collections.singletonList(DisplayableWorkflowStatus.DELETED));

            case UNAUTHORIZED: return new HashSet<>(Collections.singletonList(DisplayableWorkflowStatus.ERROR));
            case TECHNICAL_ERROR: return new HashSet<>(Collections.singletonList(DisplayableWorkflowStatus.ERROR));
            case AUTHORIZATION_FAILED: return new HashSet<>(Collections.singletonList(DisplayableWorkflowStatus.ERROR));
            case VALIDATION_FAILED: return new HashSet<>(Collections.singletonList(DisplayableWorkflowStatus.ERROR));
            case LOADED_IN_STG: return new HashSet<>(Collections.singletonList(DisplayableWorkflowStatus.ERROR));

            default: return null;
        }
    }

}
