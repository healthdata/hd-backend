/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.dto;

import be.wiv_isp.healthdata.orchestration.domain.DataCollectionAccessRequest;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class DataCollectionAccessRequestDto {

    private Long id;
    private UserDto user;
    private Map<String, Boolean> dataCollections;
    private boolean treated;

    public DataCollectionAccessRequestDto() {

    }

    public DataCollectionAccessRequestDto(DataCollectionAccessRequest request) {
        this.id = request.getId();
        this.user = new UserDto(request.getUser());
        this.dataCollections = new HashMap<>(request.getDataCollections());
        this.treated = request.isProcessed();
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("user")
    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    @JsonProperty("treated")
    public boolean isTreated() {
        return treated;
    }

    public void setTreated(boolean treated) {
        this.treated = treated;
    }

    @JsonProperty(value = "dataCollections")
    public Object getDataCollections() {
        if(dataCollections == null) {
            return null;
        } else if(isTreated()) {
            return dataCollections;
        } else {
            return dataCollections.keySet();
        }
    }
}
