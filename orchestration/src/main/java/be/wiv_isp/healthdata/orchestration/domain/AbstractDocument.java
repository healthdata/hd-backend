/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.common.json.serializer.ByteArrayToJsonSerializer;
import be.wiv_isp.healthdata.common.json.deserializer.JsonToByteArrayDeserializer;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import org.apache.commons.collections4.CollectionUtils;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@MappedSuperclass
abstract public class AbstractDocument<Workflow extends AbstractWorkflow> {

	public class DwhStatus {
		public static final String O = "O"; // outdated
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "DOCUMENT_ID")
	private Long id;
	@Lob
	@Column(name = "CONTENT", nullable = false)
	protected byte[] content;
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "WORKFLOW_ID")
	private Workflow workflow;
	@Column(name = "CREATED_ON", nullable = false)
	private Timestamp createdOn;
	@Column(name = "UPDATED_ON", nullable = false)
	private Timestamp updatedOn;
	@Column(name = "VERSION")
	private Long version;
	@Column(name = "NOTE_MAX_ORDER")
	private Long noteMaxOrder = 0L;

	@Fetch(FetchMode.SUBSELECT)
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "DOCUMENTS_ATTACHMENTS", joinColumns = @JoinColumn(name = "DOCUMENT_ID"), inverseJoinColumns = @JoinColumn(name = "ATTACHMENT_ID"))
	protected List<Attachment> attachments;

	@JsonIgnore
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonSerialize(using = ByteArrayToJsonSerializer.class)
	public byte[] getDocumentContent() {
		return content;
	}

	@JsonDeserialize(using = JsonToByteArrayDeserializer.class)
	public void setDocumentContent(byte[] content) {
		this.content = content;
	}

	public Workflow getWorkflow() {
		return workflow;
	}

	@JsonIgnore
	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}

	public Long getVersion() {
		return version;
	}

	@JsonIgnore
	public Long getNoteMaxOrder() {
		return noteMaxOrder;
	}

	public void setNoteMaxOrder(Long noteMaxOrder) {
		this.noteMaxOrder = noteMaxOrder;
	}

	@JsonIgnore
	public void setVersion(Long version) {
		this.version = version;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public List<Attachment> getAttachments() {
		if(attachments == null) {
			attachments = new ArrayList<>();
		}
		return attachments;
	}

	public void setAttachments(final List<Attachment> attachments) {
		this.attachments = attachments;
	}

	public void removeAttachment(final Attachment attachment) {
		if(CollectionUtils.isEmpty(attachments)) {
			return;
		}
		attachments.remove(attachment);
	}

	public void addAttachment(final Attachment attachment) {
		if(attachments == null) {
			attachments = new ArrayList<>();
		}
		attachments.add(attachment);
	}

	abstract public void setDocumentData(DocumentData documentData);

	@PrePersist
	public void onCreate() {
		Timestamp createdOnTimestamp = new Timestamp(new Date().getTime());
		setCreatedOn(createdOnTimestamp);
		setUpdatedOn(createdOnTimestamp);
	}

	@PreUpdate
	public void onUpdate() {
		setUpdatedOn(new Timestamp(new Date().getTime()));
	}
}
