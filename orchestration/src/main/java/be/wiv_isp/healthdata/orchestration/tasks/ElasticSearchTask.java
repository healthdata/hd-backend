/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.tasks;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.AbstractElasticSearchInfo.Status;
import be.wiv_isp.healthdata.orchestration.service.IElasticSearchInfoService;
import be.wiv_isp.healthdata.orchestration.service.IElasticSearchService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ElasticSearchTask extends SchedulableHealthDataTask {
	
	private static final Logger LOG = LoggerFactory.getLogger(ElasticSearchTask.class);

	private static final Long _6_HOURS_AS_SECONDS = 3600L * 6L;

	@Autowired
	private IElasticSearchService elasticSearchService;

	@Autowired
	private IElasticSearchInfoService elasticSearchInfoService;

	@Override
	public String getName() {
		return "ELASTICSEARCH";
	}

	@Override
	protected String getTimingExpression() {
		return configurationService.get(ConfigurationKey.SCHEDULED_TASK_ELASTIC_SEARCH_INDEXING_INTERVAL).getValue();
	}

	@Override
	public void execute() {
		List<Long> workflowIds;
		try {
			workflowIds = elasticSearchInfoService.getWorkflowIds(Status.NOT_INDEXED);
		}
		catch (Exception e){
			LOG.error("Elasticsearch task failed, could not retrieve registrations to index.", e);
			return;
		}

		if(CollectionUtils.isEmpty(workflowIds)) {
			LOG.debug("No documents to index");
			return;
		}

		while (CollectionUtils.isNotEmpty(workflowIds)) {
			LOG.info("Indexing {} document(s)", workflowIds.size());
			try {
				elasticSearchService.index(workflowIds);
			} catch (Exception e) {
				LOG.error("", e);
			}
			LOG.debug("Checking whether there are still documents to index");
			try {
				workflowIds = elasticSearchInfoService.getWorkflowIds(Status.NOT_INDEXED);
			}
			catch (Exception e){
				LOG.error("Elasticsearch task failed, could not retrieve registrations to index.", e);
				return;
			}
		}
		LOG.debug("No documents to index");
	}

	@Override
	protected Long getMaxRunningDuration() {
		return _6_HOURS_AS_SECONDS;
	}
}
