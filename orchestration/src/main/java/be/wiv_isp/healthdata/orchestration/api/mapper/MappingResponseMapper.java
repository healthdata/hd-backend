/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api.mapper;

import be.wiv_isp.healthdata.orchestration.domain.TypedValue;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.orchestration.domain.Attachment;
import be.wiv_isp.healthdata.orchestration.domain.DataSource;
import be.wiv_isp.healthdata.orchestration.domain.Progress;
import be.wiv_isp.healthdata.orchestration.mapping.MappingRequest;
import be.wiv_isp.healthdata.orchestration.mapping.MappingResponse;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MappingResponseMapper {

	static public MappingResponse convert(JSONObject json) {
		MappingResponse response = new MappingResponse();
		Progress progress = new Progress();
		response.setProgress(progress);

		if(json.isNull("targets")) {
			return response;
		}

		JSONObject targets = JsonUtils.getJSONObject(json, "targets");
		JSONArray names = targets.names();
		if (names == null) {
			return response;
		}

		try {
			for (int i = 0; i < targets.names().length(); ++i) {
				final String key = targets.names().getString(i);

				if(targets.isNull(key)) {
					continue;
				}

				switch (key) {
					case MappingRequest.Target.DOCUMENT:
						final JSONObject documentObject = targets.getJSONObject(key);
						response.setDocumentContent(documentObject.getJSONObject(MappingRequest.Fields.CONTENT));
						if(!documentObject.isNull(MappingRequest.Fields.PATIENT_ID))
							response.setPatientID(jsonObjectToMap(documentObject.getJSONObject(MappingRequest.Fields.PATIENT_ID)));
						if(!documentObject.isNull(MappingRequest.Fields.PRIVATE))
							response.setPrivateData(documentObject.getJSONObject(MappingRequest.Fields.PRIVATE));
						if(!documentObject.isNull(MappingRequest.Fields.CODED))
							response.setCodedData(jsonObjectToTypedValueMap(documentObject.getJSONObject(MappingRequest.Fields.CODED)));
						break;
					case MappingRequest.Target.TO_BE_CODED:
						response.setToBeCoded(jsonObjectToTypedValueMap(targets.getJSONObject(key)));
						break;
					case MappingRequest.Target.COMPUTED_EXPRESSIONS:
						response.setComputedExpressions(targets.getJSONObject(key));
						break;
					case MappingRequest.Target.CSV:
						response.setCsv(targets.getString(key));
						break;
					case MappingRequest.Target.STABLE_CSV:
						response.setStableCsv(targets.getString(key));
						break;
					case MappingRequest.Target.UNIQUE_ID:
						response.setUniqueID(targets.getString(key));
						break;
					case MappingRequest.Target.ES:
						response.setEsDocument(targets.getJSONObject(key));
						break;
					case MappingRequest.Target.ES_MAPPING:
						response.setEsMapping(targets.getJSONObject(key));
						break;
					case MappingRequest.Target.VALIDATION:
						response.setValidation(targets.getJSONObject(key));
						break;
					case MappingRequest.Target.PENDING_ATTACHMENTS:
						final List<Attachment> attachments = new ArrayList<>();
						final JSONArray jsonArray = targets.getJSONArray(key);
						for (int j = 0; j < jsonArray.length(); j++) {
							final JSONObject jsonObject = jsonArray.getJSONObject(j);
							final Attachment attachment = new Attachment();
							attachment.setUuid(jsonObject.getString("id"));
							attachment.setFilename(jsonObject.getString("name"));
							attachments.add(attachment);
						}
						response.setPendingAttachments(attachments);
						break;
					case MappingRequest.Target.PROGRESS:
						JSONObject progressJSON = targets.getJSONObject(key);
						if (progressJSON.names() != null) {
							for (int j = 0; j < progressJSON.names().length(); ++j) {
								final String progressKey = progressJSON.names().getString(j);
								if ("valid".equalsIgnoreCase(progressKey)) {
									progress.setValid(progressJSON.getLong(progressKey));
								} else if ("invalid".equalsIgnoreCase(progressKey)) {
									progress.setInvalid(progressJSON.getLong(progressKey));
								} else if ("optional".equalsIgnoreCase(progressKey)) {
									progress.setOptional(progressJSON.getLong(progressKey));
								} else if ("total".equalsIgnoreCase(progressKey)) {
									progress.setTotal(progressJSON.getLong(progressKey));
								} else if ("validRatio".equalsIgnoreCase(progressKey)) {
									progress.setValidRatio(progressJSON.getDouble(progressKey));
								} else if ("invalidRatio".equalsIgnoreCase(progressKey)) {
									progress.setInvalidRatio(progressJSON.getDouble(progressKey));
								} else if ("optionalRatio".equalsIgnoreCase(progressKey)) {
									progress.setOptionalRatio(progressJSON.getDouble(progressKey));
								} else if ("errors".equalsIgnoreCase(progressKey)) {
									progress.setErrors(progressJSON.getLong(progressKey));
								} else if ("warnings".equalsIgnoreCase(progressKey)) {
									progress.setWarnings(progressJSON.getLong(progressKey));
								}
							}
						}
						break;
					case MappingRequest.Target.PDF_TEMPLATE:
						response.setPdfTemplate(targets.getString(key).getBytes(Charset.forName("UTF-8")));
						break;
					case MappingRequest.Target.DOCUMENT_CONTENT_WITHOUT_COMMENTS:
						response.setDocumentContent(targets.getJSONObject(key));
						break;
					case MappingRequest.Target.NOTES_FROM_COMMENTS:
						response.setNotes(targets.getJSONArray(key));
						break;
					case MappingRequest.Target.AFFECTED_FIELDS:
						response.setAffectedFields(jsonObjectToJsonObjectMap(targets.getJSONObject(key)));
						break;
				}
			}
		} catch (JSONException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.JSON_EXCEPTION, e.getMessage());
			throw exception;
		}

		return response;
	}

	public static Map<String, JSONObject> mapDataSources(Map<String, JSONObject> affectedFields, String defaultValue) {
		Map<String, JSONObject> result = new HashMap<>();

		try {
			for (String field : affectedFields.keySet()) {
				if (affectedFields.get(field).has("reason")) {

					final String mappingReason = affectedFields.get(field).getString("reason");
					JSONObject jsonObject= new JSONObject();
					String reason;

					if (mappingReason.equalsIgnoreCase("importCSV") || mappingReason.equalsIgnoreCase(DataSource.CSV) || mappingReason.equalsIgnoreCase(DataSource.FAST_TRACK)) {
						if (defaultValue.equalsIgnoreCase(DataSource.FAST_TRACK))
							reason = DataSource.FAST_TRACK;
						else
							reason = DataSource.CSV;
					}
					else if (mappingReason.equalsIgnoreCase("importCBB") || mappingReason.equalsIgnoreCase(DataSource.API)) {
						reason = DataSource.API;
					}
					else if (mappingReason.equalsIgnoreCase("merge") || mappingReason.equalsIgnoreCase(DataSource.MERGE)) {
						reason = DataSource.MERGE;
					}
					else if (mappingReason.equalsIgnoreCase("computedValue") || mappingReason.equalsIgnoreCase(DataSource.COMPUTED)) {
						reason = DataSource.COMPUTED;
					}
					else if (mappingReason.equalsIgnoreCase("defaultValue") || mappingReason.equalsIgnoreCase(DataSource.DEFAULT)) {
						reason = DataSource.DEFAULT;
					}
					else if (mappingReason.equalsIgnoreCase("manual") || mappingReason.equalsIgnoreCase(DataSource.MANUAL)) {
						reason = DataSource.MANUAL;
					}
					else if (mappingReason.equalsIgnoreCase("stableData") || mappingReason.equalsIgnoreCase(DataSource.STABLE_DATA)) {
						reason = DataSource.STABLE_DATA;
					}
					else if (mappingReason.equalsIgnoreCase("patientID") || mappingReason.equalsIgnoreCase(DataSource.PATIENT_ID)) {
						reason = DataSource.PATIENT_ID;
					}
					else if (mappingReason.equalsIgnoreCase("cleared") || mappingReason.equalsIgnoreCase(DataSource.CLEARED)) {
						reason = DataSource.CLEARED;
					}
					else {
						reason = defaultValue;
					}
					jsonObject.put("reason", reason);
					result.put(field, jsonObject);
				}
				else {
					result.put(field, new JSONObject().put("reason", "unknown"));
				}
			}
		}
		catch (JSONException e) {} // doesn't occur

		return result;
	}

	private static Map<String, String> jsonObjectToMap(JSONObject jsonObject) throws JSONException {
		final Map<String, String> map = new HashMap<>();
		if (jsonObject.names() == null) {
			return map;
		}

		for (int i = 0; i < jsonObject.names().length(); ++i) {
			final String key = jsonObject.names().getString(i);
			map.put(key, jsonObject.getString(key));
		}
		return map;
	}

	private static Map<String, JSONObject> jsonObjectToJsonObjectMap(JSONObject jsonObject) throws JSONException {
		final Map<String, JSONObject> map = new HashMap<>();
		if (jsonObject.names() == null) {
			return map;
		}

		for (int i = 0; i < jsonObject.names().length(); ++i) {
			final String key = jsonObject.names().getString(i);
			map.put(key, jsonObject.getJSONObject(key));
		}
		return map;
	}

	private static Map<String, TypedValue> jsonObjectToTypedValueMap(JSONObject jsonObject) throws JSONException {
		final Map<String, TypedValue> map = new HashMap<>();
		if (jsonObject.names() == null) {
			return map;
		}

		for (int i = 0; i < jsonObject.names().length(); ++i) {
			final String key = jsonObject.names().getString(i);
			final JSONObject object = jsonObject.getJSONObject(key);
			map.put(key, new TypedValue(object.getString("value"), object.getString("type")));
		}
		return map;
	}

}
