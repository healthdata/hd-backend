/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.orchestration.action.workflow.WorkflowAction;
import be.wiv_isp.healthdata.orchestration.action.workflow.executor.IWorkflowActionExecutor;
import be.wiv_isp.healthdata.orchestration.dao.IWorkflowDao;
import be.wiv_isp.healthdata.orchestration.domain.AbstractDocument;
import be.wiv_isp.healthdata.orchestration.domain.AbstractWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.HDServiceUser;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.domain.search.AbstractWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;
import be.wiv_isp.healthdata.orchestration.factory.IActionFactory;
import be.wiv_isp.healthdata.orchestration.service.IAbstractDocumentService;
import be.wiv_isp.healthdata.orchestration.service.IAbstractWorkflowService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

abstract public class AbstractWorkflowService<Workflow extends AbstractWorkflow<Document, ?>, Search extends AbstractWorkflowSearch, Document extends AbstractDocument> extends AbstractService<Workflow, Long, Search, IWorkflowDao<Workflow,Search>> implements IAbstractWorkflowService<Workflow, Search> {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractWorkflowService.class);

	@Autowired
	protected IActionFactory actionFactory;

	public AbstractWorkflowService(Class<Workflow> clazz) {
		super(clazz);
	}

	@Override
	public Workflow create(Organization organization) {
		final Workflow workflow = getEntityInstance();
		workflow.setOrganization(organization);

		final Workflow created = getDao().create(workflow);
		LOG.debug("{} with id {} created", getWorkflowType().getLabel(), created.getId());

		return created;
	}

	@Override
	public List<Long> getWorkflowIds(Search search) {
		return getDao().getWorkflowIds(search);
	}

	@Override
	public List<Long> getDataCollectionDefinitionIds() {
		return getDao().getDataCollectionDefinitionIds();
	}

	@Override
	public List<Workflow> getAll() {
		return getAll(getSearchInstance());
	}

	@Override
	public List<Workflow> getAll(Search search) {
		List<Workflow> all = getDao().getAll(search);
		if (search.getWithDocument() != null) {
			List<Workflow> filtered = new ArrayList<>();
			for (Workflow workflow : all) {
				if (search.getWithDocument()) {
					if (workflow.getDocument() != null) {
						filtered.add(workflow);
					}
				} else {
					if (workflow.getDocument() == null) {
						filtered.add(workflow);
					}
				}
			}
			return filtered;
		}
		return all;
	}

	@Override
	public long count(Search search) {
		return getDao().count(search);
	}

	@Override
	public void setDocumentData(Workflow workflow, DocumentData documentData, boolean addHistoryEntry) {
		LOG.debug("Setting document content for {} [{}]", getWorkflowType().getLabel().toLowerCase(), workflow.getId());
		Document document =  workflow.getDocument();

		if (document == null) {
			LOG.debug("Document does not exist, creating new document");
			document = getDocumentService().getEntityInstance();
			document.setDocumentContent(documentData.getContent());
			document.setWorkflow(workflow);
			document.setVersion((long) workflow.getDocumentHistory().size());
			document = getDocumentService().create(document);
			workflow.setDocument(document);
			workflow.getDocumentHistory().add(document);
		} else if (addHistoryEntry) {
			LOG.debug("Document exists and new history entry must be added, creating new document");
			document = getDocumentService().getEntityInstance();
			document.setDocumentContent(documentData.getContent());
			document.setWorkflow(workflow);
			document.setVersion((long) workflow.getDocumentHistory().size());
			document = getDocumentService().create(document);
			workflow.setDocument(document);
			workflow.getDocumentHistory().add(document);
		} else {
			LOG.debug("Document exists and new history entry must not be added, updating existing document");
			document.setDocumentContent(documentData.getContent());
			getDocumentService().update(document);
		}
	}

	@Override
	public void processOutboxWorkflows() {
		final WorkflowStatus status = WorkflowStatus.OUTBOX;
		LOG.debug("Sending {}s with status {}", getWorkflowType().getLabel().toLowerCase(), status);
		Search search = getSearchInstance();
		Set<WorkflowStatus> statuses = new HashSet<>();
		statuses.add(status);
		search.setStatuses(statuses);
		final List<Workflow> workflows = getAll(search);

		if (workflows.isEmpty()) {
			LOG.trace("No {} found with status {}", getWorkflowType().getLabel().toLowerCase(), status);
		} else {
			LOG.info("{} {}(s) found with status {}", workflows.size(), getWorkflowType().getLabel().toLowerCase(), status);
		}

		final WorkflowAction sendAction = (WorkflowAction) actionFactory.getActionByActionType(WorkflowActionType.SEND, getWorkflowType());
		for (AbstractWorkflow workflow : workflows) {
			try {
				getActionExecutor().execute(sendAction, HDServiceUser.create(HDServiceUser.Username.MESSAGING), workflow);
			} catch (Exception e) {
				// nothing to do. Logging is performed in action executor
			}
		}
	}

	@Override
	public Set<WorkflowActionType> getAvailableActions(Workflow workflow) {
		final Set<WorkflowActionType> availableActions = new HashSet<>();
		for (WorkflowActionType action : WorkflowActionType.values()) {
			if(isActionAvailable(workflow, action)) {
				availableActions.add(action);
			}
		}
		return availableActions;
	}

	@Override
	public boolean isActionAvailable(Workflow workflow, WorkflowActionType action) {
		if(!action.appliesTo(getPlatform())) {
			return false;
		}
		if(!action.appliesTo(getWorkflowType())) {
			return false;
		}
		if (workflow == null || workflow.getStatus() == null) {
			return action.isInitialAction();
		}
		return workflow.getStatus().getAvailableActions().contains(action);
	}

	abstract protected Platform getPlatform();

	abstract protected WorkflowType getWorkflowType();

	abstract protected IWorkflowActionExecutor getActionExecutor();

	abstract protected IAbstractDocumentService<Document, ?> getDocumentService();
}