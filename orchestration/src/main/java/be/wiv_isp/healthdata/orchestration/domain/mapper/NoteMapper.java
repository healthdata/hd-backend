/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.domain.mapper;

import be.wiv_isp.healthdata.orchestration.domain.Comment;
import be.wiv_isp.healthdata.orchestration.domain.Note;
import be.wiv_isp.healthdata.orchestration.dto.CommentDto;
import be.wiv_isp.healthdata.orchestration.dto.NoteDto;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class NoteMapper {

    public static NoteDto mapToNoteDto(Note note) {
        final NoteDto dto = new NoteDto();
        dto.setId(note.getId());
        dto.setTitle(note.getTitle());
        dto.setFieldId(note.getFieldId());
        dto.setFieldPath(note.getFieldPath());
        dto.setResolved(note.isResolved());
        dto.setCreatedOn(note.getCreatedOn());
        dto.setUpdatedOn(note.getUpdatedOn());
        dto.setComments(new TreeSet<CommentDto>());
        for (Comment comment : note.getComments()) {
            CommentDto commentDto = new CommentDto();
            commentDto.setId(comment.getId());
            commentDto.setUsername(comment.getUsername());
            commentDto.setPlatform(comment.getPlatform().toString());
            commentDto.setContent(comment.getContent());
            commentDto.setCreatedOn(comment.getCreatedOn());
            commentDto.setUpdatedOn(comment.getUpdatedOn());
            dto.getComments().add(commentDto);
        }
        return dto;
    }

    public static List<NoteDto> mapToNoteDtos(List<Note> notes) {
        if(notes == null) {
            return new ArrayList<>();
        }

        final List<NoteDto> dtos = new ArrayList<>();
        for(Note note : notes) {
            NoteDto noteDto = mapToNoteDto(note);
            if(noteDto != null) {
                dtos.add(noteDto);
            }
        }
        return dtos;
    }
}
