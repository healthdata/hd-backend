/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.api.ClientVersion;
import be.wiv_isp.healthdata.common.caching.CacheManagementService;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.orchestration.api.uri.DataCollectionUri;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.DataCollection;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionService;
import be.wiv_isp.healthdata.orchestration.service.IWebServiceClientService;
import com.sun.jersey.api.client.GenericType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;

import javax.ws.rs.core.MediaType;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class AbstractDataCollectionService implements IDataCollectionService {

    @Autowired
    protected IWebServiceClientService webServiceClientService;

    @Autowired
    protected IConfigurationService configurationService;

    @Override
    @Cacheable(value = CacheManagementService.dataCollectionCache)
    public Map<DataCollection, Set<DataCollection>> getDataCollections() {
        final Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

        final DataCollectionUri dataCollectionUri = new DataCollectionUri();
        dataCollectionUri.setHost(catalogueHost.getValue());

        WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.setUrl(dataCollectionUri.toString());
        wsb.addHeader(HttpHeaders.CLIENT_VERSION, getClientVersion());
        wsb.setGet(true);
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.setReturnType(new GenericType<Map<DataCollection,Set<DataCollection>>>() {});

        return (Map<DataCollection, Set<DataCollection>>) webServiceClientService.callWebService(wsb);
    }

    @Override
    @Cacheable(value = CacheManagementService.dataCollectionCache)
    public Set<DataCollection> getDataCollectionGroups() {
        return new HashSet<>(getDataCollections().keySet());
    }

    @Override
    @Cacheable(value = CacheManagementService.dataCollectionCache)
    public Set<DataCollection> getDataCollectionsFlat() {
        Set<DataCollection> union = new HashSet<>();
        for (Set<DataCollection> set : getDataCollections().values())
            union.addAll(set);
        return union;
    }

    @Override
    @Cacheable(value = CacheManagementService.dataCollectionCache)
    public Set<String> getDataCollectionGroups(Organization organization) {
        return new HashSet<>(getDataCollections(organization).keySet());
    }

    @Override
    @Cacheable(value = CacheManagementService.dataCollectionCache)
    public Set<String> getDataCollectionsFlat(Organization organization) {
        Set<String> union = new HashSet<>();
        for (Set<String> set : getDataCollections(organization).values())
            union.addAll(set);
        return union;
    }

    @Override
    @Cacheable(value = CacheManagementService.dataCollectionCache)
    public Map<String, Set<String>> getPublicDataCollections() {
        Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

        final DataCollectionUri dataCollectionUri = new DataCollectionUri();
        dataCollectionUri.setHost(catalogueHost.getValue());

        WebServiceBuilder wsb = new WebServiceBuilder();
        wsb.setUrl(dataCollectionUri.toString());
        wsb.addHeader(HttpHeaders.CLIENT_VERSION, getClientVersion());
        wsb.setGet(true);
        wsb.setAccept(MediaType.APPLICATION_JSON_TYPE);
        wsb.addParameter("publicOnly", "true");
        wsb.setReturnType(new GenericType<Map<String,Set<String>>>() {
        });

        return (Map<String, Set<String>>) webServiceClientService.callWebService(wsb);
    }

    @Override
    @Cacheable(value = CacheManagementService.dataCollectionCache)
    public Set<String> getPublicDataCollectionGroups() {
        return new HashSet<>(getPublicDataCollections().keySet());
    }

    @Override
    @Cacheable(value = CacheManagementService.dataCollectionCache)
    public String getGroup(String dataCollectionName) {
        final Map<DataCollection, Set<DataCollection>> dataCollections = getDataCollections();
        for (Map.Entry<DataCollection, Set<DataCollection>> entry : dataCollections.entrySet()) {
            String masterName = entry.getKey().getName();
            if (masterName.equalsIgnoreCase(dataCollectionName))
                return masterName;
            for (DataCollection dataCollection : entry.getValue()) {
                if (dataCollection.getName().equalsIgnoreCase(dataCollectionName))
                    return masterName;
            }
        }
        return dataCollectionName;
    }

    protected String getClientVersion() {
        return ClientVersion.DataCollection.getDefault();
    }

    @Override
    @Cacheable(value = CacheManagementService.dataCollectionCache)
    public Set<String> getMembers(String groupName) {
        Set<String> members = null;

        Map<DataCollection, Set<DataCollection>> dataCollections = getDataCollections();
        for (DataCollection dataCollection : dataCollections.keySet()) {
            if (groupName.equalsIgnoreCase(dataCollection.getName())) {
                members = new HashSet<>();
                for (DataCollection member : dataCollections.get(dataCollection))
                    members.add(member.getName());
            }
        }

        return members;
    }
}
