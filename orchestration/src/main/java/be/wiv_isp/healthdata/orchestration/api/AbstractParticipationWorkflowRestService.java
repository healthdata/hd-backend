/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.common.caching.CacheManagementService;
import be.wiv_isp.healthdata.orchestration.annotation.Auditable;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ApiType;
import be.wiv_isp.healthdata.orchestration.action.workflow.WorkflowAction;
import be.wiv_isp.healthdata.orchestration.action.workflow.executor.IWorkflowActionExecutor;
import be.wiv_isp.healthdata.orchestration.domain.AbstractParticipationDocument;
import be.wiv_isp.healthdata.orchestration.domain.AbstractParticipationWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import be.wiv_isp.healthdata.orchestration.domain.search.AbstractParticipationWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.dto.AbstractParticipationWorkflowDto;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.nio.charset.StandardCharsets;

abstract public class AbstractParticipationWorkflowRestService<Workflow extends AbstractParticipationWorkflow, Search extends AbstractParticipationWorkflowSearch, FullDto extends AbstractParticipationWorkflowDto, Dto extends AbstractParticipationWorkflowDto, Document extends AbstractParticipationDocument>  extends AbstractWorkflowRestService<Workflow, Search, FullDto, Dto, Document> {

	@POST
	@PreAuthorize("hasRole('ROLE_USER')")
	@Auditable(apiType = ApiType.WORKFLOW)
	@Path("/actions")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@CacheEvict(value = CacheManagementService.dataCollectionGroupCache, allEntries = true)
	public Response executeActions(final String json) {
		final WorkflowAction<Workflow> action = (WorkflowAction<Workflow>) actionFactory.createAction(json.getBytes(StandardCharsets.UTF_8));

		final HDUserDetails principal = AuthenticationService.getUserDetails();
		final Workflow execute = getWorkflowActionExecutor().execute(action, principal);

		return Response.ok(workflowMapper.convert(execute)).build();
	}

	@Override
	protected WorkflowType getWorkflowType() {
		return WorkflowType.PARTICIPATION;
	}

	abstract protected IWorkflowActionExecutor<Workflow> getWorkflowActionExecutor();
}
