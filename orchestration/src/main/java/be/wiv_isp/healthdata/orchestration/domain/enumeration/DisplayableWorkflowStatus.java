/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.enumeration;

public enum DisplayableWorkflowStatus {

	// HD4DP
	OPEN("Open"),
	SUBMITTED("Submitted"),
	CORRECTIONS_NEEDED("Corrections needed"),
	FOLLOW_UP_NEEDED("Follow-up needed"),

	// HD4RES
	NEW("New"),
	NEW_CORRECTION("New correction"),
	NEW_FOLLOW_UP("New follow-up"),
	COMMENTS_NEEDED("Comments needed"),
	CORRECTIONS_REQUESTED("Corrections requested"),
	DELETED("Deleted"),

	// HD4DP & HD4RES
	SENDING("Sending"),
	APPROVED("Approved"),
	ERROR("Error");

	private final String label;

	DisplayableWorkflowStatus(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return label;
	}
}
