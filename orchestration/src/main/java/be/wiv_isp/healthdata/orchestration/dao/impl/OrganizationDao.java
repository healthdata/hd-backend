/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.search.OrganizationSearch;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class OrganizationDao extends CrudDaoV2<Organization, Long, OrganizationSearch> implements IOrganizationDao {

	public OrganizationDao() {
		super(Organization.class);
	}

	@Override
	public List<Organization> getAll() {
		return getAll(new OrganizationSearch());
	}

	@Override
	public Organization getMain() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Organization> cq = cb.createQuery(Organization.class).distinct(true);
		Root<Organization> rootEntry = cq.from(Organization.class);

		cq.where(cb.equal(rootEntry.get("main"), Boolean.TRUE));

		TypedQuery<Organization> allQuery = em.createQuery(cq);
		List<Organization> resultList = allQuery.getResultList();

		if (resultList.size() < 1) {
			return null;
		} else {
			return resultList.get(0);
		}
	}

	@Override
	public void delete(Organization organization) {
		if (!organization.isMain()) {
			organization.setDeleted(true);
			update(organization);
		}
	}

	@Override
	protected List<Predicate> getPredicates(OrganizationSearch search, CriteriaBuilder cb, Root<Organization> rootEntry) {
		final List<Predicate> predicates = new ArrayList<>();
		if (search.getHealthDataIDType() != null) {
			predicates.add(cb.equal(rootEntry.get("healthDataIDType"), search.getHealthDataIDType()));
		}
		if (search.getHealthDataIDValue() != null) {
			predicates.add(cb.equal(rootEntry.get("healthDataIDValue"), search.getHealthDataIDValue()));
		}
		if (search.isMain() != null) {
			predicates.add(cb.equal(rootEntry.get("main"), search.isMain()));
		}
		if (search.isDeleted() != null) {
			predicates.add(cb.equal(rootEntry.get("deleted"), search.isDeleted()));
		}
		return predicates;
	}
}
