/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@MappedSuperclass
abstract public class AbstractRegistrationDocument<Workflow extends AbstractRegistrationWorkflow> extends AbstractDocument<Workflow> {

    @Fetch(FetchMode.JOIN)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "CODED_DOCUMENT_CONTENT", joinColumns = @JoinColumn(name = "DOCUMENT_ID"))
    @MapKeyColumn(name = "LOOKUP_KEY")
    protected Map<String, TypedValue> codedContent;

    @Fetch(FetchMode.SUBSELECT)
    @OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL }, orphanRemoval = true)
    @JoinColumn(name = "DOCUMENT_ID", nullable = false)
    protected List<DataSource> dataSources;

    public Map<String, TypedValue> getCodedContent() {
        return codedContent;
    }

    public void setCodedContent(Map<String, TypedValue> codedContent) {
        this.codedContent = codedContent;
    }

    public List<DataSource> getDataSources() {
        return dataSources;
    }

    public void setDataSources(List<DataSource> dataSources) {
        if (this.dataSources == null) {
            this.dataSources = new ArrayList<>();
        }
        if (this.dataSources != dataSources) {
            this.dataSources.clear();
            this.dataSources.addAll(dataSources);
        }
    }
}
