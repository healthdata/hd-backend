/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service;

import be.wiv_isp.healthdata.common.service.IAbstractService;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.AbstractWorkflow;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.search.IAbstractWorkflowSearch;
import be.wiv_isp.healthdata.orchestration.dto.DocumentData;

import java.util.List;
import java.util.Set;

public interface IAbstractWorkflowService<Workflow extends AbstractWorkflow, Search extends IAbstractWorkflowSearch> extends IAbstractService<Workflow, Long, Search> {

	Search getSearchInstance();

	Workflow getEntityInstance();

	Workflow create(Organization organization);

	List<Long> getWorkflowIds(Search search);

	List<Long> getDataCollectionDefinitionIds();

	long count(Search search);

	void setDocumentData(Workflow workflow, DocumentData documentData, boolean addHistoryEntry);

	void processOutboxWorkflows();

	void updateSendStatus(Long workflowId, String sendStatus);

	Set<WorkflowActionType> getAvailableActions(Workflow workflow);

	boolean isActionAvailable(Workflow workflow, WorkflowActionType action);
}
