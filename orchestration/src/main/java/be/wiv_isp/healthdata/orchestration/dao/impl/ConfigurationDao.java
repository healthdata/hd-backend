/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.orchestration.dao.IConfigurationDao;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.search.ConfigurationSearch;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ConfigurationDao extends CrudDaoV2<Configuration, Long, ConfigurationSearch> implements IConfigurationDao {

	public ConfigurationDao() {
		super(Configuration.class);
	}

	@Override
	protected List<Predicate> getPredicates(ConfigurationSearch search, CriteriaBuilder cb, Root<Configuration> rootEntry) {
		final List<Predicate> predicates = new ArrayList<>();

		if (search.getKey() != null) {
			predicates.add(cb.equal(rootEntry.<String> get("key"), search.getKey()));
		}

		if (search.getOrganization() != null || search.getOrganizationCanBeNull() != null) {
			Predicate isNull = cb.isNull(rootEntry.get("organization"));
			Predicate equal = cb.equal(rootEntry.get("organization"), search.getOrganization());

			if(search.getOrganization() != null) {
				if(Boolean.TRUE.equals(search.getOrganizationCanBeNull())) {
					predicates.add(cb.or(isNull, equal));
				} else {
					predicates.add(equal);
				}
			} else if(Boolean.FALSE.equals(search.getOrganizationCanBeNull())) {
				predicates.add(cb.not(isNull));
			}
		}

		return predicates;
	}

	@Override
	public List<String> deleteWithInvalidKey() {
		List<String> deletedKeys = new ArrayList<>();
		for (Long id : getAllIds()) {
			try {
				getKeyById(id);
			} catch (IllegalArgumentException e) {
				deleteId(id);
				final String invalidKey = e.getMessage().substring(e.getMessage().indexOf("[")+1, e.getMessage().indexOf("]"));
				deletedKeys.add(invalidKey);
			}
		}
		return deletedKeys;
	}

	private List<Long> getAllIds() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Configuration> rootEntry = cq.from(Configuration.class);
		cq.select(rootEntry.<Long>get("id"));

		TypedQuery<Long> allQuery = em.createQuery(cq);
		return allQuery.getResultList();
	}

	private ConfigurationKey getKeyById(Long id) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ConfigurationKey> cq = cb.createQuery(ConfigurationKey.class);
		Root<Configuration> rootEntry = cq.from(Configuration.class);
		cq.select(rootEntry.<ConfigurationKey>get("key"));
		cq.where(cb.equal(rootEntry.<Long>get("id"), id));
		TypedQuery<ConfigurationKey> allQuery = em.createQuery(cq);
		return allQuery.getSingleResult();
	}

	private int deleteId(Long id) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaDelete<Configuration> delete = cb.createCriteriaDelete(Configuration.class);
		Root root = delete.from(Configuration.class);
		delete.where(cb.equal(root.get("id"), id));
		return em.createQuery(delete).executeUpdate();
	}

}
