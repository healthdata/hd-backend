/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "DATA_SOURCES")
public class DataSource {

    public static final String MANUAL = "Manual";
    public static final String CSV = "CSV";
    public static final String API = "API";
    public static final String FAST_TRACK = "Fast track";
    public static final String COMPUTED = "Computed";
    public static final String DEFAULT = "Default";
    public static final String MERGE = "Merge";
    public static final String STABLE_DATA = "Stable data";
    public static final String PATIENT_ID = "Patient ID";
    public static final String CLEARED = "Cleared";


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "FIELD_PATH", nullable = false)
    protected String fieldPath;

    @Column(name = "SOURCE")
    protected String source;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFieldPath() {
        return fieldPath;
    }

    public void setFieldPath(String fieldPath) {
        this.fieldPath = fieldPath;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataSource that = (DataSource) o;

        return Objects.equals(id, that.id)
                && Objects.equals(fieldPath, that.fieldPath)
                && Objects.equals(source, that.source);
    }

    @Override
    public int hashCode() {
        return Objects.hash(//
                this.id, //
                this.fieldPath, //
                this.source);
    }

}
