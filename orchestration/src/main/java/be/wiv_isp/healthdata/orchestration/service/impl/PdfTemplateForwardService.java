/*
 * Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.api.uri.PdfTemplateUri;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IWebServiceClientService;
import be.wiv_isp.healthdata.common.api.ClientVersion;
import be.wiv_isp.healthdata.common.caching.CacheManagementService;
import be.wiv_isp.healthdata.common.domain.enumeration.Language;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.orchestration.service.IPdfTemplateForwardService;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;
import java.io.IOException;

@Service
public class PdfTemplateForwardService implements IPdfTemplateForwardService {

	@Autowired
	private IWebServiceClientService webServiceClientService;

	@Autowired
	private IConfigurationService configurationService;

	private static final Logger LOG = LoggerFactory.getLogger(PdfTemplateForwardService.class);

	@Override
	@Cacheable(value = CacheManagementService.pdfCache)
	public byte[] get(Long dataCollectionDefinitionId, Language language) {
		LOG.info("Getting pdfTemplate for data collection definition with id " + dataCollectionDefinitionId);

		Configuration catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST);

		PdfTemplateUri pdfTemplateUri = new PdfTemplateUri();
		pdfTemplateUri.setHost(catalogueHost.getValue());
		pdfTemplateUri.setId(dataCollectionDefinitionId);

		WebServiceBuilder wsb = new WebServiceBuilder();
		wsb.setUrl(pdfTemplateUri.toString());
		wsb.addHeader(HttpHeaders.CLIENT_VERSION, getClientVersion());
		wsb.addHeader(HttpHeaders.ACCEPT_LANGUAGE, language.getHeader());
		wsb.setGet(true);
		wsb.setAccept(MediaType.APPLICATION_OCTET_STREAM_TYPE);
		wsb.setReturnType(new GenericType<ClientResponse>() {});
		wsb.setLogResponseBody(false);

		ClientResponse clientResponse = (ClientResponse) webServiceClientService.callWebService(wsb);
		try {
			return IOUtils.toByteArray(clientResponse.getEntityInputStream());
		} catch (IOException e) {
			HealthDataException exception = new HealthDataException(e);
			exception.setExceptionType(ExceptionType.GENERAL_EXCEPTION);
			throw exception;
		}
	}

	protected String getClientVersion() {
		return ClientVersion.PdfTemplate.getDefault();
	}
}
