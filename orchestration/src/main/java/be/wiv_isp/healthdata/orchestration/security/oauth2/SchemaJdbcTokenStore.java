/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.security.oauth2;

import java.text.MessageFormat;

import javax.sql.DataSource;

import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

public class SchemaJdbcTokenStore extends JdbcTokenStore {

	private static final String DEFAULT_ACCESS_TOKEN_INSERT_STATEMENT = "insert into \"{0}\".\"OAUTH_ACCESS_TOKEN\" (token_id, token, authentication_id, user_name, client_id, authentication, refresh_token) values (?, ?, ?, ?, ?, ?, ?)";

	private static final String DEFAULT_ACCESS_TOKEN_SELECT_STATEMENT = "select token_id, token from \"{0}\".\"OAUTH_ACCESS_TOKEN\" where token_id = ?";

	private static final String DEFAULT_ACCESS_TOKEN_AUTHENTICATION_SELECT_STATEMENT = "select token_id, authentication from \"{0}\".\"OAUTH_ACCESS_TOKEN\" where token_id = ?";

	private static final String DEFAULT_ACCESS_TOKEN_FROM_AUTHENTICATION_SELECT_STATEMENT = "select token_id, token from \"{0}\".\"OAUTH_ACCESS_TOKEN\" where authentication_id = ?";

	private static final String DEFAULT_ACCESS_TOKENS_FROM_USERNAME_AND_CLIENT_SELECT_STATEMENT = "select token_id, token from \"{0}\".\"OAUTH_ACCESS_TOKEN\" where user_name = ? and client_id = ?";

	private static final String DEFAULT_ACCESS_TOKENS_FROM_CLIENTID_SELECT_STATEMENT = "select token_id, token from \"{0}\".\"OAUTH_ACCESS_TOKEN\" where client_id = ?";

	private static final String DEFAULT_ACCESS_TOKEN_DELETE_STATEMENT = "delete from \"{0}\".\"OAUTH_ACCESS_TOKEN\" where token_id = ?";

	private static final String DEFAULT_ACCESS_TOKEN_DELETE_FROM_REFRESH_TOKEN_STATEMENT = "delete from \"{0}\".\"OAUTH_ACCESS_TOKEN\" where refresh_token = ?";

	private static final String DEFAULT_REFRESH_TOKEN_INSERT_STATEMENT = "insert into \"{0}\".\"OAUTH_REFRESH_TOKEN\" (token_id, token, authentication) values (?, ?, ?)";

	private static final String DEFAULT_REFRESH_TOKEN_SELECT_STATEMENT = "select token_id, token from \"{0}\".\"OAUTH_REFRESH_TOKEN\" where token_id = ?";

	private static final String DEFAULT_REFRESH_TOKEN_AUTHENTICATION_SELECT_STATEMENT = "select token_id, authentication from \"{0}\".\"OAUTH_REFRESH_TOKEN\" where token_id = ?";

	private static final String DEFAULT_REFRESH_TOKEN_DELETE_STATEMENT = "delete from \"{0}\".\"OAUTH_REFRESH_TOKEN\" where token_id = ?";

	public SchemaJdbcTokenStore(DataSource dataSource, String schema) {
		super(dataSource);
		setInsertAccessTokenSql(MessageFormat.format(DEFAULT_ACCESS_TOKEN_INSERT_STATEMENT, schema));
		setSelectAccessTokenSql(MessageFormat.format(DEFAULT_ACCESS_TOKEN_SELECT_STATEMENT, schema));
		setSelectAccessTokenAuthenticationSql(MessageFormat.format(DEFAULT_ACCESS_TOKEN_AUTHENTICATION_SELECT_STATEMENT, schema));
		setSelectAccessTokenFromAuthenticationSql(MessageFormat.format(DEFAULT_ACCESS_TOKEN_FROM_AUTHENTICATION_SELECT_STATEMENT, schema));
		setSelectAccessTokensFromUserNameAndClientIdSql(MessageFormat.format(DEFAULT_ACCESS_TOKENS_FROM_USERNAME_AND_CLIENT_SELECT_STATEMENT, schema));
		setSelectAccessTokensFromClientIdSql(MessageFormat.format(DEFAULT_ACCESS_TOKENS_FROM_CLIENTID_SELECT_STATEMENT, schema));
		setDeleteAccessTokenSql(MessageFormat.format(DEFAULT_ACCESS_TOKEN_DELETE_STATEMENT, schema));
		setInsertRefreshTokenSql(MessageFormat.format(DEFAULT_REFRESH_TOKEN_INSERT_STATEMENT, schema));
		setSelectRefreshTokenSql(MessageFormat.format(DEFAULT_REFRESH_TOKEN_SELECT_STATEMENT, schema));
		setSelectRefreshTokenAuthenticationSql(MessageFormat.format(DEFAULT_REFRESH_TOKEN_AUTHENTICATION_SELECT_STATEMENT, schema));
		setDeleteRefreshTokenSql(MessageFormat.format(DEFAULT_REFRESH_TOKEN_DELETE_STATEMENT, schema));
		setDeleteAccessTokenFromRefreshTokenSql(MessageFormat.format(DEFAULT_ACCESS_TOKEN_DELETE_FROM_REFRESH_TOKEN_STATEMENT, schema));
	}
}
