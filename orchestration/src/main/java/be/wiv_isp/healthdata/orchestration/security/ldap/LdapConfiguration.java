/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.security.ldap;

import be.wiv_isp.healthdata.orchestration.dao.IOrganizationDao;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LdapConfiguration implements ILdapConfiguration {

	@Autowired
	private IConfigurationService configurationService;

	@Autowired
	private IOrganizationDao organizationDao;

	@Override
	public String getServerUrl() {
		return configurationService.get(ConfigurationKey.LDAP_SERVER_URL, organizationDao.getMain()).getValue();
	}

	@Override
	public String getUsername() {
		return configurationService.get(ConfigurationKey.LDAP_USER, organizationDao.getMain()).getValue();
	}

	@Override
	public String getPassword() {
		return configurationService.get(ConfigurationKey.LDAP_PASSWORD, organizationDao.getMain()).getValue();
	}

	@Override
	public String getUserSearchBase(Organization organization) {
		return configurationService.get(ConfigurationKey.LDAP_USER_SEARCH_BASE, organization).getValue();
	}

	@Override
	public String getUserSearchFilter(Organization organization) {
		return "(" + getUsernameAttribute(organization) + "={0})";
	}

	@Override
	public String getUserSearchFilterWithWildCards(Organization organization) {
		return "(" + getUsernameAttribute(organization) + "=*{0}*)";
	}

	@Override
	public String getUsernameAttribute(Organization organization) {
		return configurationService.get(ConfigurationKey.LDAP_USERNAME_ATTRIBUTE, organization).getValue();
	}

	@Override
	public String getLastNameAttribute(Organization organization) {
		return configurationService.get(ConfigurationKey.LDAP_LAST_NAME_ATTRIBUTE, organization).getValue();
	}

	@Override
	public String getFirstNameAttribute(Organization organization) {
		return configurationService.get(ConfigurationKey.LDAP_FIRST_NAME_ATTRIBUTE, organization).getValue();
	}

	@Override
	public String getMailAttribute(Organization organization) {
		return configurationService.get(ConfigurationKey.LDAP_MAIL_ATTRIBUTE, organization).getValue();
	}

}
