/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.common.util.StringUtils;
import org.apache.tika.Tika;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "ATTACHMENTS")
public class Attachment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ATTACHMENT_ID")
    private Long id;

    @Column(name = "UUID", nullable = false)
    private String uuid;

    @Column(name = "FILENAME", nullable = false)
    private String filename;

    @Access(AccessType.PROPERTY)
    @Column(name = "MIMETYPE", nullable = false)
    private String mimeType;

    @ManyToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST })
    @JoinColumn(name = "CONTENT_ID", nullable = false)
    private AttachmentContent content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getMimeType() {
        if(mimeType == null) {
            computeMimeType();
        }
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public AttachmentContent getContent() {
        return content;
    }

    public void setContent(AttachmentContent content) {
        this.content = content;
    }

    private void computeMimeType() {
        if(filename != null) {
            mimeType = new Tika().detect(filename);
        }
    }

    @Override
    public boolean equals(Object other) {
        if(this == other) {
            return true;
        }

        if(other instanceof Attachment) {
            Attachment o = (Attachment) other;

            return Objects.equals(uuid, o.uuid);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }

    @Override
    public String toString() {
        return StringUtils.toString(this);
    }
}
