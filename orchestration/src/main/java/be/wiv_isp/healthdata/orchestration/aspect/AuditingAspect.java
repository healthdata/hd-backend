/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.aspect;

import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.annotation.Auditable;
import be.wiv_isp.healthdata.orchestration.domain.Audit;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.StatusResult;
import be.wiv_isp.healthdata.orchestration.service.IAuditService;
import com.sun.jersey.core.spi.factory.ResponseImpl;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.core.UriInfo;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Aspect
@Component
public class AuditingAspect {

	private static final Logger LOG = LoggerFactory.getLogger(AuditingAspect.class);

	@Autowired
	private IAuditService auditService;

	@Around("@annotation(auditable)")
	@Transactional
	public Object createAudit(ProceedingJoinPoint pjp, Auditable auditable) throws Throwable {
		final String apiType = auditable.apiType().getDescription();
		final String methodName = pjp.getSignature().getName();

		MethodSignature signature = (MethodSignature) pjp.getSignature();
		List<String> parameterNames = Arrays.asList(signature.getParameterNames());
		List<Object> parameterValues = Arrays.asList(pjp.getArgs());

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < parameterNames.size(); i++) {
			sb.append(parameterNames.get(i));
			sb.append(" = ");
			sb.append(parameterToString(parameterValues, i));
			if (i != parameterNames.size() - 1) {
				sb.append(" & ");
			}
		}
		final String subject = sb.toString();
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = null;
		if (authentication != null) {
			if ("anonymousUser".equals(authentication.getPrincipal().toString())) {
				username = "anonymousUser";
			} else {
				final UserDetails principal = (UserDetails) authentication.getPrincipal();
				username = principal.getUsername();
			}
		}

		Audit audit = new Audit();
		audit.setApi(apiType);
		audit.setMethod(methodName);
		audit.setSubject(subject);
		audit.setUserName(username);
		audit = auditService.create(audit);

		try {
			Object output = pjp.proceed();
			final String status = getStatus(output);
			updateAudit(audit, status);
			return output;
		} catch (HealthDataException e) {
			String status;
			if(e.getResponseStatus() != null) {
				status = String.valueOf(e.getResponseStatus().getStatusCode());
			} else {
				status = "ERROR";
			}
			updateAudit(audit, status, e.getMessage());
			throw e;
		}
	}

	private Audit createAudit(Audit audit) {
		/*
		Audit creation must not be blocking
		 */
		LOG.debug(MessageFormat.format("Creating Auditing entry: {0}", audit));
		try {
			return auditService.create(audit);
		} catch (Exception e) {
			LOG.error("Error while creating audit entry. Subject might be too long.", e);
		}

		// subject is probably too long
		final String subject = "Could not save subject";
		LOG.debug(MessageFormat.format("Setting subject to [{0}] and trying again", subject));
		audit.setSubject(subject);

		try {
			return auditService.create(audit);
		} catch (Exception e) {
			LOG.error("Audit entry could still not be saved", e);
		}

		return null;
	}

	private Audit updateAudit(Audit audit, String status) {
		return updateAudit(audit, status, null);
	}

	private Audit updateAudit(Audit audit, String status, String exceptionMessage) {
		/*
		Audit update must not be blocking
		 */
		LOG.debug("Updating Auditing entry: status=" + status + ", exceptionMessage=" + exceptionMessage);
		if(audit == null) {
			LOG.error("Audit is null, probably due to an unexpected error during audit creation. Not updating entry.");
			return null;
		}

		audit.setStatus(status);
		audit.setExceptionMessage(exceptionMessage);

		try {
			return auditService.update(audit);
		} catch (Exception e) {
			LOG.error("Error while update audit entry. Exception message might be too long.", e);
		}

		// exception message is probably too long
		final String newExceptionMessage = "Could not save exception message";
		LOG.debug(MessageFormat.format("Setting exception message to [{0}] and trying again", newExceptionMessage));
		audit.setExceptionMessage(newExceptionMessage);

		try {
			return auditService.update(audit);
		} catch (Exception e) {
			LOG.error("Audit entry could still not be updated", e);
		}

		return null;
	}

	private String parameterToString(List<Object> parameterValues, int i) {
		Object parameter = parameterValues.get(i);
		if (parameter instanceof UriInfo) {
			StringBuilder sb = new StringBuilder();
			UriInfo uriInfo = (UriInfo) parameter;
			if (!uriInfo.getPathParameters().isEmpty()) {
				sb.append("path: ");
				sb.append(uriInfo.getPathParameters());
				sb.append(" ");
			}
			if (!uriInfo.getQueryParameters().isEmpty()) {
				sb.append("query: ");
				sb.append(uriInfo.getQueryParameters());
				sb.append(" ");
			}
			return sb.toString();
		}
		return String.valueOf(parameter);
	}

	private String getStatus(Object output) {
		StringBuilder sb;
		String status;
		if (output instanceof HashMap) {
			Map<String, StatusResult> map = (HashMap<String, StatusResult>) output;
			sb = new StringBuilder();
			int i = 1;
			for (Map.Entry<String, StatusResult> entry : map.entrySet()) {
				sb.append(entry.getKey());
				sb.append("=");
				sb.append(entry.getValue().name());
				if (i != map.size()) {
					sb.append(" & ");
				}
				i++;
			}
			status = sb.toString();
		} else {
			status = String.valueOf(((ResponseImpl) output).getStatusType().getStatusCode());
		}
		return status;
	}

	public IAuditService getAuditService() {
		return auditService;
	}

	public void setAuditService(IAuditService auditService) {
		this.auditService = auditService;
	}
}
