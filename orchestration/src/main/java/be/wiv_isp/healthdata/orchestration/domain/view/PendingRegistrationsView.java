/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.view;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.persistence.*;
import java.sql.Timestamp;
import java.text.MessageFormat;

@Entity
@Table(name = "V_PENDING_REGISTRATIONS")
public class PendingRegistrationsView {

    @Id
    @Column(name = "WORKFLOW_ID")
    protected Long workflowId;

    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    protected WorkflowStatus status;

    @Column(name = "USER_ID")
    protected Long userId;

    @Column(name = "USERNAME")
    protected String username;

    @Column(name = "OPEN_SINCE")
    @JsonSerialize(using = TimestampSerializer.class) // todo remove
    protected Timestamp openSince;

    public Long getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(Long workflowId) {
        this.workflowId = workflowId;
    }

    public WorkflowStatus getStatus() {
        return status;
    }

    public void setStatus(WorkflowStatus status) {
        this.status = status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Timestamp getOpenSince() {
        return openSince;
    }

    public void setOpenSince(Timestamp openSince) {
        this.openSince = openSince;
    }

    @Override
    public String toString() {
        return MessageFormat.format("PendingRegistration[userId={0},username={1},workflowId={2},status={3},openSince={4}]", userId, username, workflowId, status, DateFormat.DATE_AND_TIME.format(openSince));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PendingRegistrationsView)) return false;

        PendingRegistrationsView that = (PendingRegistrationsView) o;

        if (workflowId != null ? !workflowId.equals(that.workflowId) : that.workflowId != null) return false;
        if (status != that.status) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        return !(openSince != null ? !openSince.equals(that.openSince) : that.openSince != null);

    }

    @Override
    public int hashCode() {
        int result = workflowId != null ? workflowId.hashCode() : 0;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (openSince != null ? openSince.hashCode() : 0);
        return result;
    }
}
