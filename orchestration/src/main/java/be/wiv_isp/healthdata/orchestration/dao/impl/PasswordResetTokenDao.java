/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.dao.impl;

import be.wiv_isp.healthdata.common.dao.impl.CrudDaoV2;
import be.wiv_isp.healthdata.orchestration.dao.IPasswordResetTokenDao;
import be.wiv_isp.healthdata.orchestration.domain.PasswordResetToken;
import be.wiv_isp.healthdata.orchestration.domain.search.PasswordResetTokenSearch;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class PasswordResetTokenDao extends CrudDaoV2<PasswordResetToken, Long, PasswordResetTokenSearch> implements IPasswordResetTokenDao {

    public PasswordResetTokenDao() {
        super(PasswordResetToken.class);
    }

    @Override
    protected List<Predicate> getPredicates(PasswordResetTokenSearch search, CriteriaBuilder cb, Root<PasswordResetToken> rootEntry) {
        final List<Predicate> predicates = new ArrayList<>();
        if(search.getToken() != null) {
            predicates.add(cb.equal(rootEntry.get("token"), search.getToken()));
        }
        if (search.getExpired() != null) {
            if(search.getExpired()) {
                predicates.add(cb.lessThanOrEqualTo(rootEntry.<Timestamp>get("expiryDate"), new Date()));
            } else {
                predicates.add(cb.greaterThanOrEqualTo(rootEntry.<Timestamp>get("expiryDate"), new Date()));
            }
        }
        return predicates;
    }

}
