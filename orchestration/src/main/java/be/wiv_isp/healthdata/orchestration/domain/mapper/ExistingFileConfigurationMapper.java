/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.domain.mapper;

import be.wiv_isp.healthdata.orchestration.domain.IConfiguration;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;

import java.io.File;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.text.MessageFormat;

public class ExistingFileConfigurationMapper {

	static public File map(IConfiguration configuration) {

		final String value = configuration.getValue();

		if (value == null) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_PROPERTY,configuration.getKey(), value, "configuration value is null");
			throw exception;
		}

		try {
			Paths.get(value);
		} catch (InvalidPathException e) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_PROPERTY,configuration.getKey(), value, MessageFormat.format("path [{0}] is invalid", value));
			throw exception;
		}

		final File file = new File(value);

		if (!file.isFile()) {
			HealthDataException exception = new HealthDataException();
			exception.setExceptionType(ExceptionType.INVALID_PROPERTY,configuration.getKey(), value, MessageFormat.format("path [{0}] is not a file", value));
			throw exception;
		}

		return file;
	}
}
