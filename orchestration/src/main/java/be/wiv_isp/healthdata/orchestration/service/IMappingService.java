/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service;

import be.wiv_isp.healthdata.orchestration.domain.AbstractRegistrationWorkflow;
import be.wiv_isp.healthdata.orchestration.dto.ValidationResponse;
import be.wiv_isp.healthdata.orchestration.mapping.IMappingContext;
import be.wiv_isp.healthdata.orchestration.mapping.MappingRequest;
import be.wiv_isp.healthdata.orchestration.mapping.MappingResponse;
import org.codehaus.jettison.json.JSONObject;

public interface IMappingService<RegistrationWorkflow extends AbstractRegistrationWorkflow> {

	MappingResponse mapFromCsvToJson(byte[] dcdContent, String csvContent, IMappingContext context);

	MappingResponse mapFromXmlToJson(byte[] dcdContent, String xmlContent, IMappingContext context);

	String mapFromJsonToCsv(byte[] documentContent, byte[] dcdContent, boolean stable);

	String mapFromJsonToCsv(RegistrationWorkflow workflow, IMappingContext context, byte[] dcdContent, boolean stable);

	JSONObject mapToElasticSearchDocument(RegistrationWorkflow workflow);

	JSONObject getElasticSearchMapping(Long dataCollectionDefinitionId);

	MappingResponse merge(RegistrationWorkflow primary, RegistrationWorkflow secondary, IMappingContext context, byte[] dcdContent);

	String getUniqueID(RegistrationWorkflow workflow);

	String getElasticSearchIndexSettings();

	boolean isValid(byte[] documentContent, byte[] dcdContent);

	boolean isValid(RegistrationWorkflow workflow, IMappingContext context, byte[] dcdContent);

	ValidationResponse getValidationResponse(byte[] documentContent, byte[] dcdContent);

	ValidationResponse getValidationResponse(RegistrationWorkflow workflow, IMappingContext context, byte[] dcdContent);

	JSONObject getComputedExpressions(RegistrationWorkflow workflow);

	JSONObject get(MappingRequest request);

	JSONObject get(MappingRequest request, String endpoint);

	MappingResponse extractNotes(RegistrationWorkflow workflow, byte[] dcdContent);

}
