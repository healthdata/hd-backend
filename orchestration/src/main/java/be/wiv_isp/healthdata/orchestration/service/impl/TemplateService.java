/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.mail.AbstractVelocityContext;
import be.wiv_isp.healthdata.orchestration.service.ITemplateService;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.apache.velocity.runtime.resource.loader.StringResourceLoader;
import org.apache.velocity.runtime.resource.util.StringResourceRepository;
import org.apache.velocity.runtime.resource.util.StringResourceRepositoryImpl;
import org.springframework.stereotype.Service;

import java.io.StringWriter;

@Service
public class TemplateService implements ITemplateService {

    public static final String TEMPLATE_NAME = "template";

    private VelocityEngine ve;

    @Override
    public String process(String templateContent, AbstractVelocityContext context) {
        init();

        context.build();

        final StringResourceRepository repo = StringResourceLoader.getRepository();
        repo.putStringResource(TEMPLATE_NAME, templateContent);

        final StringWriter writer = new StringWriter();
        final Template template = ve.getTemplate(TEMPLATE_NAME);
        template.merge(context, writer);

        repo.removeStringResource(TEMPLATE_NAME);
        return writer.toString();
    }

    private void init() {
        if(ve == null) {
            ve = new VelocityEngine();
            ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "string,classpath"); // string for loading template from the catalogue, classpath to load included content from the classpath
            ve.setProperty("string.resource.loader.class", StringResourceLoader.class.getName());
            ve.setProperty("string.resource.loader.repository.class", StringResourceRepositoryImpl.class.getName());
            ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
            ve.setProperty("runtime.references.strict", true); // throws an exception if a placeholder is not mapped to a value in the context (default = false)
            ve.init();
        }
    }
}
