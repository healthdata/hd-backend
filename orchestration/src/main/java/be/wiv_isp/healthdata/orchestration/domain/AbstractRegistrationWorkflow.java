/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@MappedSuperclass
abstract public class AbstractRegistrationWorkflow<Document extends AbstractRegistrationDocument> extends AbstractWorkflow<Document, RegistrationWorkflowHistory> {

	@Fetch(FetchMode.JOIN)
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "METADATA", joinColumns = @JoinColumn(name = "WORKFLOW_ID"))
	@MapKeyColumn(name = "LOOKUP_KEY")
	@Column(name = "VALUE")
	protected Map<String, String> metaData;

	@Fetch(FetchMode.SUBSELECT)
	@OneToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL }, orphanRemoval = true)
	@JoinColumn(name = "WORKFLOW_ID", nullable = false)
	protected List<FollowUp> followUps;

	@Column(name = "SUBMITTED_ON")
	protected Timestamp submittedOn;

	@Embedded
	protected WorkflowFlags flags = new WorkflowFlags();

	public Map<String, String> getMetaData() {
		if (metaData == null) {
			metaData = new HashMap<>();
		}

		return metaData;
	}

	public void setMetaData(Map<String, String> metaData) {
		this.metaData = metaData;
	}

	public List<FollowUp> getFollowUps() {
		if(followUps == null) {
			followUps = new ArrayList<>();
		}
		return followUps;
	}

	public void setFollowUps(List<FollowUp> followUps) {
		if(this.followUps == null) {
			this.followUps = followUps;
		} else {
			this.followUps.clear();
			this.followUps.addAll(followUps);
		}
	}

	public void addFollowUp(FollowUp followUp) {
		if(followUps == null) {
			followUps = new ArrayList<>();
		}
		followUps.add(followUp);
	}

	public Timestamp getSubmittedOn() {
		return submittedOn;
	}

	public void setSubmittedOn(Timestamp submittedOn) {
		this.submittedOn = submittedOn;
	}

	public WorkflowFlags getFlags() {
		return flags;
	}

	public void setFlags(WorkflowFlags flags) {
		this.flags = flags;
	}

	public String getPatientId() {
		if (document != null && document.getCodedContent() != null) {
			final TypedValue patientId = (TypedValue) document.getCodedContent().get(CodedDocumentData.PATIENT_ID);
			if (patientId != null && "patientID".equals(patientId.getType()))
				return patientId.getValue();
		}
		if (metaData != null) {
			return metaData.get(MetaData.PATIENT_IDENTIFIER);
		}
		return null;
	}

	@Override
	public WorkflowType getWorkflowType() {
		return WorkflowType.REGISTRATION;
	}

}