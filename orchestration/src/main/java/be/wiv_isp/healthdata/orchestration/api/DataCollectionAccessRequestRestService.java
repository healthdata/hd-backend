/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.orchestration.annotation.Auditable;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ApiType;
import be.wiv_isp.healthdata.common.json.JsonUtils;
import be.wiv_isp.healthdata.common.rest.RestUtils;
import be.wiv_isp.healthdata.orchestration.annotation.OrganizationPreAuthorize;
import be.wiv_isp.healthdata.orchestration.domain.DataCollectionAccessRequest;
import be.wiv_isp.healthdata.orchestration.domain.search.DataCollectionAccessRequestSearch;
import be.wiv_isp.healthdata.orchestration.dto.DataCollectionAccessRequestDto;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionAccessRequestService;
import be.wiv_isp.healthdata.orchestration.service.IUserManagementService;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@Path("/organizations/{organizationId}/dataCollectionAccessRequests")
public class DataCollectionAccessRequestRestService {

	@Autowired
	private IDataCollectionAccessRequestService dataCollectionAccessRequestService;

	@Autowired
	private IUserManagementService userManagementService;

	@POST
	@OrganizationPreAuthorize
	@Auditable(apiType = ApiType.DATA_COLLECTION_ACCESS_REQUEST)
	@PreAuthorize("hasRole('ROLE_USER')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(@Context UriInfo info, final String json) {
		final JSONObject jsonObject = JsonUtils.createJsonObject(json);
		final DataCollectionAccessRequest request = new DataCollectionAccessRequest();
		request.setDataCollections(JsonUtils.getStringSet(jsonObject, "dataCollections"), false);
		request.setUser(userManagementService.getUserManagementStrategy().read(AuthenticationService.getAuthenticatedOrganization()));

		final DataCollectionAccessRequest created = dataCollectionAccessRequestService.create(request);
		return Response.ok(new DataCollectionAccessRequestDto(created)).build();
	}

	@GET
	@OrganizationPreAuthorize
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(@Context UriInfo info) {
		final DataCollectionAccessRequestSearch search = getSearch(info);
		final List<DataCollectionAccessRequest> all = dataCollectionAccessRequestService.getAll(search);
		return Response.ok(mapToDtos(all)).build();
	}

	@GET
	@Path("/{id}")
	@OrganizationPreAuthorize
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context UriInfo info) {
		final DataCollectionAccessRequestSearch search = getSearch(info);
		final DataCollectionAccessRequest request = dataCollectionAccessRequestService.getUnique(search);

		if(request == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}

		return Response.ok(new DataCollectionAccessRequestDto(request)).build();
	}

	@POST
	@Path("/{id}/decision")
	@OrganizationPreAuthorize
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Produces(MediaType.APPLICATION_JSON)
	public Response decisions(@Context UriInfo info, final Map<String, Boolean> decisions) {
		final DataCollectionAccessRequestSearch search = getSearch(info);
		final DataCollectionAccessRequest request = dataCollectionAccessRequestService.getUnique(search);

		if(request == null) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}

		final DataCollectionAccessRequest treated = dataCollectionAccessRequestService.process(request, decisions);
		return Response.ok(new DataCollectionAccessRequestDto(treated)).build();
	}

	private DataCollectionAccessRequestSearch getSearch(final UriInfo info) {
		final Long organizationId = RestUtils.getPathLong(info, "organizationId");
		final DataCollectionAccessRequestSearch search = new DataCollectionAccessRequestSearch(organizationId);
		search.setId(RestUtils.getPathLong(info, "id"));
		search.setUserId(RestUtils.getParameterLong(info, "userId"));
		search.setProcessed(RestUtils.getParameterBoolean(info, "processed"));
		return search;
	}

	private List<DataCollectionAccessRequestDto> mapToDtos(final List<DataCollectionAccessRequest> requests) {
		final List<DataCollectionAccessRequestDto> dtos = new ArrayList<>();
		for (final DataCollectionAccessRequest request : requests) {
			dtos.add(new DataCollectionAccessRequestDto(request));
		}
		return dtos;
	}
}
