/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.api;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import be.wiv_isp.healthdata.orchestration.annotation.Auditable;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ApiType;
import be.wiv_isp.healthdata.orchestration.domain.mapper.Mapper;
import be.wiv_isp.healthdata.orchestration.domain.search.StatusMessageSearch;
import be.wiv_isp.healthdata.orchestration.domain.validator.IStatusMessageValidator;
import be.wiv_isp.healthdata.orchestration.service.IPlatformService;
import be.wiv_isp.healthdata.orchestration.service.IStatusMessageService;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Path("/statusmessages")
public class StatusMessageRestService<STATUS_MESSAGE, STATUS_MESSAGE_DTO> {

    private static final Logger LOG = LoggerFactory.getLogger(StatusMessageRestService.class);

    @Autowired
    private IStatusMessageService<STATUS_MESSAGE> statusMessageService;

    @Autowired
    private IStatusMessageValidator<STATUS_MESSAGE> statusMessageValidator;

    @Autowired
    private Mapper<STATUS_MESSAGE, STATUS_MESSAGE_DTO> statusMessageMapper;

    @Autowired
    private IPlatformService platformService;

    @POST
    @Path("/send")
    @Produces(MediaType.APPLICATION_JSON)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Response send() {
        statusMessageService.send();
        return Response.ok().build();
    }

    @GET
    @Auditable(apiType = ApiType.STATUS_MESSAGE)
    @Produces(MediaType.APPLICATION_JSON)
    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    public Response getAll(@QueryParam("createdOn") String createdOn) {
        LOG.info("Get status message. (createdOn [{}]).", createdOn);
        StatusMessageSearch search = new StatusMessageSearch();
        if (StringUtils.isNotBlank(createdOn)) {
            Date createdOnDate = DateFormat.stringToDate(createdOn, DateFormat.DATE_AND_TIME);
            search.setCreatedOn(createdOnDate);
        } else {
            search.setLastOnly(true);
        }

        // on the HD4DP platform, only return the status message for the current organization
        if(Platform.HD4DP.equals(platformService.getCurrentPlatform())) {
            final HDUserDetails principal = AuthenticationService.getUserDetails();

            final Organization organization = principal.getOrganization();
            final HealthDataIdentification healthDataIdentification = new HealthDataIdentification();
            healthDataIdentification.setType(organization.getHealthDataIDType());
            healthDataIdentification.setValue(organization.getHealthDataIDValue());
            search.setHealthDataIdentification(healthDataIdentification);
        }

        final List<STATUS_MESSAGE> statusMessages = statusMessageService.getAll(search);
        final List<STATUS_MESSAGE_DTO> statusMessagesDto = new ArrayList<>();

        for (STATUS_MESSAGE statusMessage : statusMessages) {
            statusMessageValidator.validateTimings(statusMessage);
            statusMessagesDto.add(statusMessageMapper.map(statusMessage));
        }

        return Response.ok(statusMessagesDto).build();
    }
}
