/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.tasks;

import be.wiv_isp.healthdata.common.domain.enumeration.DateFormat;
import be.wiv_isp.healthdata.common.domain.enumeration.TemplateKey;
import be.wiv_isp.healthdata.common.dto.DataCollectionGroupDto;
import be.wiv_isp.healthdata.orchestration.domain.DataCollection;
import be.wiv_isp.healthdata.orchestration.domain.EventDataCollectionGroup;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.EventType;
import be.wiv_isp.healthdata.orchestration.domain.mail.DataCollectionDefinitionContext;
import be.wiv_isp.healthdata.orchestration.domain.search.EventDataCollectionGroupSearch;
import be.wiv_isp.healthdata.orchestration.service.IDataCollectionGroupForwardService;
import be.wiv_isp.healthdata.orchestration.service.IEventDataCollectionGroupService;
import be.wiv_isp.healthdata.orchestration.service.IMailService;
import be.wiv_isp.healthdata.orchestration.service.IUserDataCollectionService;
import be.wiv_isp.healthdata.orchestration.util.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@Component
public class DataCollectionGroupNotificationTask extends HealthDataTask {

    private static final Logger LOG = LoggerFactory.getLogger(DataCollectionGroupNotificationTask.class);

    @Autowired
    private IEventDataCollectionGroupService eventDataCollectionGroupService;

    @Autowired
    private IUserDataCollectionService userDataCollectionService;

    @Autowired
    private IDataCollectionGroupForwardService dataCollectionGroupForwardService;

    @Autowired
    private IMailService mailService;

    @Override
    public String getName() {
        return "DATA_COLLECTION_GROUP_NOTIFICATION_TASK";
    }

    @Override
    protected void execute() {
        final EventDataCollectionGroupSearch search = new EventDataCollectionGroupSearch();
        search.setWithNotificationTimeBefore(new Timestamp(new Date().getTime()));
        search.setTypes(EventType.START_DATA_COLLECTION_PERIOD, EventType.END_DATA_COLLECTION_PERIOD);
        final List<EventDataCollectionGroup> events = eventDataCollectionGroupService.getAll(search);

        for (final EventDataCollectionGroup event : events) {
            DataCollectionGroupDto dto;
            try {
                dto = dataCollectionGroupForwardService.get(event.getDataCollectionGroupName(), event.getMajorVersion());
            } catch (Exception e) {
                LOG.error(MessageFormat.format("Data collection group {0} and major version {1} could not be retrieved from the catalogue.", event.getDataCollectionGroupName(), event.getMajorVersion()), e);
                continue;
            }

            if(dto == null) {
                LOG.warn("No data collection group found for data collection group name {} and major version {}.", event.getDataCollectionGroupName(), event.getMajorVersion());
                removeExpiredNotificationTimes(event);
                continue;
            }

            if(EventType.START_DATA_COLLECTION_PERIOD.equals(event.getType())) {
                handleStartDataCollectionEvent(event, dto);
            } else if (EventType.END_DATA_COLLECTION_PERIOD.equals(event.getType())) {
                handleEndDataCollectionEvent(event, dto);
            }

        }
    }

    private void handleStartDataCollectionEvent(EventDataCollectionGroup event, DataCollectionGroupDto dto) {
        if(dto.getStartDate() == null) {
            LOG.warn("Start data collection event was triggered but start date is now undefined (null). Notification will not be sent.");
            removeExpiredNotificationTimes(event);
            return;
        }

        if(dto.getStartDate().after(new Date())) {
            LOG.warn("Start data collection event was triggered but start date ({}) is not reached yet (data collection is not started). Notification will not be sent.", DateFormat.DATE_AND_TIME.format(dto.getStartDate()));
            removeExpiredNotificationTimes(event);
            return;
        }

        preNotify(event);
        final DataCollectionDefinitionContext context = new DataCollectionDefinitionContext();
        context.setDataCollectionName(event.getDataCollectionGroupName());
        context.setInstallationUrl(configurationService.get(ConfigurationKey.GUI_HOST).getValue());
        final Set<String> emails = userDataCollectionService.getEmailAddresses(new DataCollection(event.getDataCollectionGroupName()));
        mailService.createAndSendMail(TemplateKey.START_OF_DATA_COLLECTION_PERIOD, context, emails);
        postNotify(event);
    }

    private void handleEndDataCollectionEvent(EventDataCollectionGroup event, DataCollectionGroupDto dto) {
        if(dto.getEndDateCreation() == null) {
            LOG.warn("End data collection event was triggered but end date is now undefined (null). Notification will not be sent.");
            removeExpiredNotificationTimes(event);
            return;
        }

        if(dto.getEndDateCreation().before(new Date())) {
            LOG.warn("End data collection event was triggered but end date ({}) is in the past (data collection is already closed). Notification will not be sent.", DateFormat.DATE_AND_TIME.format(dto.getEndDateCreation()));
            removeExpiredNotificationTimes(event);
            return;
        }

        preNotify(event);
        final DataCollectionDefinitionContext context = new DataCollectionDefinitionContext();
        context.setDataCollectionName(event.getDataCollectionGroupName());
        context.setEndDateCreation(DateFormat.DDMMYYYY.format(dto.getEndDateCreation()));
        context.setEndDateSubmission(DateFormat.DDMMYYYY.format(dto.getEndDateSubmission()));
        context.setEndDateComments(DateFormat.DDMMYYYY.format(dto.getEndDateComments()));
        context.setPeriod(DateUtils.getPrettyPrintedPeriod(new Date(), dto.getEndDateCreation(), true));
        context.setInstallationUrl(configurationService.get(ConfigurationKey.GUI_HOST).getValue());
        final Set<String> emails = userDataCollectionService.getEmailAddresses(new DataCollection(event.getDataCollectionGroupName()));
        mailService.createAndSendMail(TemplateKey.END_OF_DATA_COLLECTION_PERIOD, context, emails);
        postNotify(event);
    }

    private void preNotify(final EventDataCollectionGroup event) {
        LOG.info("Event[{}]: notifying event - {}", event.getId(), event);
    }

    private void postNotify(final EventDataCollectionGroup event) {
        LOG.info("Event[{}]: notified", event.getId());
        removeExpiredNotificationTimes(event);
    }

    private void removeExpiredNotificationTimes(EventDataCollectionGroup event) {
        LOG.debug("Event[{}]: removing expired notification times", event.getId());

        final Date now = new Date();
        for (final Iterator<Timestamp> iterator = event.getNotificationTimes().iterator(); iterator.hasNext(); ) {
            final Timestamp notificationTime = iterator.next();
            if(notificationTime.before(now)) {
                iterator.remove();
            }
        }

        if(event.getNotificationTimes().isEmpty()) {
            LOG.debug("Event[{}] has no remaining notification times. Deleting event.", event.getId());
            eventDataCollectionGroupService.delete(event);
        } else {
            LOG.debug("Event[{}] has {} remaining notification time(s): {}.", event.getId(), event.getNotificationTimes().size(), event.notificationTimesAsString());
            eventDataCollectionGroupService.update(event);
        }
    }
}
