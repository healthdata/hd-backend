/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.StatusResult;
import be.wiv_isp.healthdata.orchestration.api.uri.DataCollectionUri;
import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.AuthenticationType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.api.uri.WorkflowUri;
import be.wiv_isp.healthdata.orchestration.domain.AbstractStatusMessage;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.search.StatusMessageSearch;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import be.wiv_isp.healthdata.orchestration.service.*;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

@Service
public class StatusService implements IStatusService {

    private static final Logger LOG = LoggerFactory.getLogger(StatusService.class);

    @Autowired
    private IUserService userService;

    @Autowired
    private IOrganizationService organizationService;

    @Autowired
    private IConfigurationService configurationService;

    @Autowired
    private IUserManagementService userManagementService;

    @Autowired
    private IMailService mailService;

    @Autowired
    private IStatusMessageService statusMessageService;


    @Value("${host}")
    private String host;

    public static final String STATUS_KEY_DATABASE = "database";
    public static final String STATUS_KEY_CATALOGUE = "catalogue";
    public static final String STATUS_KEY_WORKFLOW = "workflow";
    public static final String STATUS_KEY_LDAP = "ldap";
    public static final String STATUS_KEY_MAPPING = "mapping";
    public static final String STATUS_KEY_ELASTICSEARCH = "elasticsearch";
    public static final String STATUS_KEY_MAIL_SERVER = "mail_server";
    public static final String STATUS_KEY_CODAGE_INCOMING = "codage_incoming";


    @Override
    public JSONObject getStatus() throws Exception {
        return getStatus(false);
    }

    @Override
    public JSONObject getStatus(boolean withCodage) throws Exception {
        LOG.debug("Building status map");
        final JSONObject result = new JSONObject();

        Organization main = organizationService.getMain();

        // check if DB connection to Gateway is up
        LOG.debug("Checking database connection");
        long start = System.nanoTime();
        boolean database = checkDB(main);
        long end = System.nanoTime();
        LOG.debug("Database connection status: {}. Time elapsed: {} milliseconds", database, getIntervalMillis(start, end));
        result.put(STATUS_KEY_DATABASE, database ? StatusResult.UP : StatusResult.DOWN);

        LOG.debug("Checking catalogue connection");
        start = System.nanoTime();
        boolean catalogue = checkRestServiceStatus(MessageFormat.format(DataCollectionUri.URI_PATTERN, host));
        end = System.nanoTime();
        LOG.debug("Catalogue connection status: {}. Time elapsed: {} milliseconds", catalogue, getIntervalMillis(start, end));
        result.put(STATUS_KEY_CATALOGUE, catalogue ? StatusResult.UP : StatusResult.DOWN);

        LOG.debug("Checking workflow rest service");
        start = System.nanoTime();
        boolean workflowRestService = checkRestServiceStatus(MessageFormat.format(WorkflowUri.URI_PATTERN, host));
        end = System.nanoTime();
        LOG.debug("Workflow rest service status: {}. Time elapsed: {} milliseconds", workflowRestService, getIntervalMillis(start, end));
        result.put(STATUS_KEY_WORKFLOW, workflowRestService ? StatusResult.UP : StatusResult.DOWN);

        LOG.debug("Checking elasticsearch status");
        start = System.nanoTime();
        boolean elasticSearchRestService = checkRestServiceStatus(MessageFormat.format("{0}/elasticsearch", host));
        end = System.nanoTime();
        LOG.debug("Elasticsearch status: {}. Time elapsed: {} milliseconds", elasticSearchRestService, getIntervalMillis(start, end));
        result.put(STATUS_KEY_ELASTICSEARCH, elasticSearchRestService ? StatusResult.UP : StatusResult.DOWN);

        // only check LDAP status if authenticationType is ldap and ldap server url is configured
        if (userManagementService.getUserManagementType().getAuthenticationType().equals(AuthenticationType.LDAP)) {
            if(StringUtils.isNotEmpty(configurationService.get(ConfigurationKey.LDAP_SERVER_URL, main).getValue())) {
                LOG.debug("Checking ldap server connection");
                start = System.nanoTime();
                boolean ldapRestService = checkRestServiceStatus(MessageFormat.format("{0}/ldap", host));
                end = System.nanoTime();
                LOG.debug("Ldap server status: {}. Time elapsed: {} milliseconds", ldapRestService, getIntervalMillis(start, end));
                result.put(STATUS_KEY_LDAP, ldapRestService ? StatusResult.UP : StatusResult.DOWN);
            }
        }

        String mappingHost = configurationService.get(ConfigurationKey.MAPPING_HOST).getValue();

        LOG.debug("Checking mapping-api connection");
        start = System.nanoTime();
        boolean mappingRestService = checkRestServiceStatus(mappingHost);
        end = System.nanoTime();
        LOG.debug("Mapping-api status: {}. Time elapsed: {} milliseconds", mappingRestService, getIntervalMillis(start, end));
        result.put(STATUS_KEY_MAPPING, mappingRestService ? StatusResult.UP : StatusResult.DOWN);

        LOG.debug("Checking mail server connection");
        start = System.nanoTime();
        boolean mailServer = mailService.status();
        end = System.nanoTime();
        LOG.debug("Mail server status: {}. Time elapsed: {} milliseconds", mailServer, getIntervalMillis(start, end));
        result.put(STATUS_KEY_MAIL_SERVER, mailServer ? StatusResult.UP : StatusResult.DOWN);

        if (withCodage) {
            LOG.debug("Checking codage status");
            start = System.nanoTime();
            boolean codage = checkCodage(main);
            end = System.nanoTime();
            LOG.debug("Codage status: {}. Time elapsed: {} milliseconds", codage, getIntervalMillis(start, end));
            result.put(STATUS_KEY_CODAGE_INCOMING, codage ? StatusResult.UP : StatusResult.DOWN);
        }

        return result;
    }

    private long getIntervalMillis(long start, long end) {
        return (end-start)/1_000_000L;
    }

    private boolean checkRestServiceStatus(String url) {
        try {
            ClientConfig config = new DefaultClientConfig();
            config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
            Client client = Client.create(config);
            WebResource wr = client.resource(url + "/status");

            ClientResponse clientResponse = wr.get(ClientResponse.class);
            return ClientResponse.Status.NO_CONTENT.getStatusCode() == clientResponse.getStatus() || ClientResponse.Status.OK.getStatusCode() == clientResponse.getStatus();
        } catch (Exception e) {
            return false;
        }
    }

    private boolean checkDB(Organization organization) {
        try {
            final UserSearch userSearch = new UserSearch();
            userSearch.setOrganization(organization);
            List<User> users = userService.getAll(userSearch);
            return !users.isEmpty();
        } catch (Exception e) {
            LOG.error("Error while checking DB Connection.", e);
            return false;
        }
    }

    private boolean checkCodage(Organization organization) {
        final long _24H_IN_MILLISECONDS = 24 * 60 * 60 * 1000;
        try {

            final HealthDataIdentification identification = new HealthDataIdentification();
            identification.setType(organization.getHealthDataIDType());
            identification.setValue(organization.getHealthDataIDValue());

            final StatusMessageSearch search = new StatusMessageSearch();
            search.setLastOnly(true);
            search.setHealthDataIdentification(identification);

            final List<AbstractStatusMessage> statusMessages = statusMessageService.getAll(search);
            return (new Date().getTime() - statusMessages.get(0).getSentOn().getTime()) < _24H_IN_MILLISECONDS;
        }
        catch (Exception e) {
            return false;
        }

    }
}
