/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.service.impl.AbstractService;
import be.wiv_isp.healthdata.orchestration.dao.IFollowUpDao;
import be.wiv_isp.healthdata.orchestration.domain.FollowUp;
import be.wiv_isp.healthdata.orchestration.domain.search.FollowUpSearch;
import be.wiv_isp.healthdata.orchestration.service.IAbstractFollowUpService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract public class AbstractFollowUpService extends AbstractService<FollowUp, Long, FollowUpSearch, IFollowUpDao<FollowUp>> implements IAbstractFollowUpService {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractFollowUpService.class);

    public AbstractFollowUpService(Class<FollowUp> followUpClass) {
        super(followUpClass);
    }

    @Override
    public void unschedule(FollowUp followUp) {
        LOG.debug("Unscheduling follow-up {}", followUp);
        if(followUp.isActive()) {
            LOG.debug("Unable to unschedule follow-up because it is already active. Doing nothing.");
            return;
        }

        followUp.setActivationDate(null);
    }
}
