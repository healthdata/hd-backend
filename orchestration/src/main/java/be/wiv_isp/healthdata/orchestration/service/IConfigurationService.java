/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.service;

import be.wiv_isp.healthdata.common.service.IAbstractService;
import be.wiv_isp.healthdata.orchestration.domain.Configuration;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.domain.search.ConfigurationSearch;

public interface IConfigurationService extends IAbstractService<Configuration, Long, ConfigurationSearch> {

	Configuration get(ConfigurationKey key);

	Configuration get(ConfigurationKey key, Organization organization);

	boolean isTrue(ConfigurationKey key);

	boolean isTrue(ConfigurationKey key, Organization organization);

	void deleteInvalid();

	void deleteInapplicable();

	void createMissing();

	void migrateEmInterfaceType();

	void deleteUnattachedOrganizationConfigurations();

}
