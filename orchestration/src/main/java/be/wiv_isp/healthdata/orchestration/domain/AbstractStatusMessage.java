/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.orchestration.action.StatusMessageContent;
import be.wiv_isp.healthdata.orchestration.converter.StatusMessageContentToByteArrayConverter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@MappedSuperclass
public abstract class AbstractStatusMessage {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "STATUS_MESSAGE_ID", nullable = false)
	protected Long id;

	@Embedded
	protected HealthDataIdentification healthDataIdentification;

	@Column(name = "CONTENT", nullable = false)
	@Convert(converter = StatusMessageContentToByteArrayConverter.class)
	protected StatusMessageContent content;

	@Column(name = "CREATED_ON", nullable = false)
	protected Timestamp createdOn;

	@Column(name = "SENT_ON", nullable = false)
	protected Timestamp sentOn;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setHealthDataIdentification(HealthDataIdentification healthDataIdentification) {
		this.healthDataIdentification = healthDataIdentification;
	}

	public HealthDataIdentification getHealthDataIdentification() {
		return healthDataIdentification;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Timestamp getSentOn() {
		return sentOn;
	}

	public void setSentOn(Timestamp sentOn) {
		this.sentOn = sentOn;
	}

	public StatusMessageContent getContent() {
		return this.content;
	}

	public void setContent(StatusMessageContent content) {
		this.content = content;
	}

	@PrePersist
	public void onCreate() {
		Timestamp createdOnTimestamp = new Timestamp(new Date().getTime());
		setCreatedOn(createdOnTimestamp);
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof AbstractStatusMessage) {
			AbstractStatusMessage other = (AbstractStatusMessage) o;

			return Objects.equals(id, other.id);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}
}
