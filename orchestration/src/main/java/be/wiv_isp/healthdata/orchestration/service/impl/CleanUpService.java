/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.service.impl;

import ch.qos.logback.classic.LoggerContext;
import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;

@Component
public class CleanUpService {

    private static final Logger LOG = LoggerFactory.getLogger(CleanUpService.class);
    private static final ILoggerFactory I_LOGGER_FACTORY = LoggerFactory.getILoggerFactory();

    @PreDestroy
    public void cleanup() {
        if (I_LOGGER_FACTORY instanceof LoggerContext) {
            LOG.debug("Stopping logger.");
            ((LoggerContext) I_LOGGER_FACTORY).stop();
        }
    }

}
