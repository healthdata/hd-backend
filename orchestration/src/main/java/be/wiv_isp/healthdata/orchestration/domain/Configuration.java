/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.impl.EncryptionService;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "CONFIGURATIONS")
public class Configuration implements Serializable, IConfiguration {

	private static final long serialVersionUID = 7767507069644394827L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CONFIGURATION_ID")
	private Long id;

	@Column(name = "LOOKUP_KEY", unique = false, nullable = false, updatable = false)
	@Enumerated(EnumType.STRING)
	private ConfigurationKey key;

	@Column(name = "VALUE", nullable = true)
	private String value;

	@Column(name = "DEFAULT_VALUE", nullable = true)
	private String defaultValue;

	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "ORGANIZATION_ID")
	private Organization organization;

	@Column(name = "UPDATED_ON")
	private Timestamp updatedOn;

	@Column(name = "USE_FROM_MAIN")
	private Boolean useFromMain;

	@Transient
	private transient String decryptedValue;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ConfigurationKey getKey() {
		return key;
	}

	public void setKey(ConfigurationKey key) {
		this.key = key;
	}

	public String getValue() {
		if (decryptedValue != null) {
			return decryptedValue;
		}
		if (value == null)
			return "";
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDefaultValue() {
		if (defaultValue == null)
			return "";
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getDescription() {
		return key.getDescription();
	}

	public String getType() {
		return key.getType();
	}

	public List<String> getAllowedValues() {
		List<String> allowedValues = new ArrayList<>();
		if (key.getFullType().isEnum()) {
			for (Object name : key.getFullType().getEnumConstants())
				allowedValues.add(name.toString());
		}
		return allowedValues;
	}

	public boolean isReadable() {
		return key.isReadable();
	}

	public boolean isAdvanced() {
		return key.isAdvanced();
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public Boolean getUseFromMain() {
		return useFromMain;
	}

	public void setUseFromMain(Boolean useFromMain) {
		this.useFromMain = useFromMain;
	}

	@PreUpdate
	public void onUpdate() {
		setUpdatedOn(new Timestamp(new Date().getTime()));

		if (!key.isReadable()) {
			decryptedValue = value;
			value = new EncryptionService().encrypt(value);
		}
	}

	@PostLoad
	public void onLoad() {
		if (!key.isReadable()) {
			decryptedValue = new EncryptionService().decrypt(value);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof Configuration) {
			Configuration other = (Configuration) o;

			return Objects.equals(key, other.key)
					&& Objects.equals(value, other.value)
					&& Objects.equals(defaultValue, other.defaultValue)
					&& Objects.equals(updatedOn, other.updatedOn)
					&& Objects.equals(organization, other.organization)
					&& Objects.equals(useFromMain, other.useFromMain);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				this.key,
				this.value,
				this.defaultValue,
				this.updatedOn,
				this.organization,
				this.useFromMain);
	}

	@Override
	public String toString() {
		return "Configuration {" +
				"key = " + Objects.toString(this.key) + ", " +
				"value = " + (key != null && key.isReadable() ? Objects.toString(this.value) : "***") + ", " +
				"defaultValue = " + (key != null && key.isReadable() ? Objects.toString(this.value) : "***") + ", " +
				"updatedOn = " + Objects.toString(this.updatedOn) + ", " +
				"organization = " + Objects.toString(this.organization) + ", " +
				"useFromMain = " + Objects.toString(this.useFromMain) + "}";
	}
}
