/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package be.wiv_isp.healthdata.orchestration.dto;

import be.wiv_isp.healthdata.orchestration.domain.TypedValue;
import be.wiv_isp.healthdata.common.json.deserializer.JsonToByteArrayDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.ByteArrayToJsonSerializer;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jettison.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.Map;

@JsonSerialize(include= JsonSerialize.Inclusion.NON_NULL)
public class DocumentData {

    @JsonSerialize(using = ByteArrayToJsonSerializer.class)
    @JsonDeserialize(using = JsonToByteArrayDeserializer.class)
    private byte [] content;
    @JsonSerialize(using = ByteArrayToJsonSerializer.class)
    @JsonDeserialize(using = JsonToByteArrayDeserializer.class)
    @JsonProperty("private")
    private byte [] privateData;
    private Map<String, TypedValue> coded;
    private Map<String, String> patientID;

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public byte[] getPrivate() {
        return privateData;
    }

    public void setPrivate(byte[] privateData) {
        this.privateData = privateData;
    }

    public Map<String, TypedValue> getCoded() {
        return coded;
    }

    public void setCoded(Map<String, TypedValue> coded) {
        this.coded = coded;
    }

    public Map<String, String> getPatientID() {
        return patientID;
    }

    public void setPatientID(Map<String, String> patientID) {
        this.patientID = patientID;
    }

    public void setContent(JSONObject content) {
        if(content == null) {
            this.content = null;
        } else {
            this.content = content.toString().getBytes(StandardCharsets.UTF_8);
        }
    }

    public void setPrivate(JSONObject privateData) {
        if(privateData == null) {
            this.privateData = null;
        } else {
            this.privateData = privateData.toString().getBytes(StandardCharsets.UTF_8);
        }
    }
}