/*
 *  Copyright 2014-2016 Healthdata.be
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.jaxb.xml;

import javax.xml.bind.annotation.adapters.*;

public class SqlTimestampAdapter extends XmlAdapter<java.util.Date, java.sql.Timestamp> {

    @Override
    public java.util.Date marshal(java.sql.Timestamp sqlTimestamp) throws Exception {
        if(null == sqlTimestamp) {
            return null;
        }
        return new java.util.Date(sqlTimestamp.getTime());
    }

    @Override
    public java.sql.Timestamp unmarshal(java.util.Date utilDate) throws Exception {
        if(null == utilDate) {
            return null;
        }
        return new java.sql.Timestamp(utilDate.getTime());
    }

}