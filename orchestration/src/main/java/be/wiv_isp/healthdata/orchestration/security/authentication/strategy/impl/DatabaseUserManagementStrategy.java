/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.security.authentication.strategy.impl;

import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.UserManagementType;
import be.wiv_isp.healthdata.common.exception.ExceptionType;
import be.wiv_isp.healthdata.common.exception.HealthDataException;
import be.wiv_isp.healthdata.orchestration.domain.HDUserDetails;
import be.wiv_isp.healthdata.orchestration.domain.User;
import be.wiv_isp.healthdata.orchestration.domain.search.UserSearch;
import be.wiv_isp.healthdata.orchestration.service.IPasswordResetService;
import be.wiv_isp.healthdata.orchestration.service.impl.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class DatabaseUserManagementStrategy extends AbstractUserManagementStrategy {

    private static final Logger LOG = LoggerFactory.getLogger(DatabaseUserManagementStrategy.class);

    @Resource(name = "passwordEncoder")
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IPasswordResetService passwordResetService;

    @Override
    public User updatePassword(Long id, String oldPassword, String newPassword) {
        final User user = read(id);
        final HDUserDetails principal = AuthenticationService.getUserDetails();
        final String username = user.getUsername();
        if (!username.equals(principal.getUsername())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.UNAUTHORIZED_USER_UPDATE, principal.getUsername(), username);
            throw exception;

        }
        if (!passwordEncoder.matches(oldPassword, user.getPassword())) {
            HealthDataException exception = new HealthDataException();
            exception.setExceptionType(ExceptionType.INVALID_USER_PASSWORD, "PASSWORD");
            throw exception;
        }

        final User updatedUser = userService.updatePassword(user, newPassword);
        LOG.info("New password has been set for user with id [{}]", id);
        return updatedUser;
    }

    @Override
    public void sendPasswordResetInstructions(Organization organization, String email) {
        passwordResetService.sendPasswordResetInstructions(organization, email);
    }

    @Override
    public void resetPassword(Organization organization, String token, String newPassword) {
        passwordResetService.resetPassword(organization, token, newPassword);
    }

    @Override
    public boolean supports(UserManagementType userManagementType) {
        return UserManagementType.DATABASE.equals(userManagementType);
    }

    @Override
    protected UserSearch updateSearch(UserSearch search) {
        search.setLdapUser(false);
        return search;
    }
}
