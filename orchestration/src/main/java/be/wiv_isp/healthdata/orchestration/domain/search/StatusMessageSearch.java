/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.search;

import be.wiv_isp.healthdata.orchestration.domain.HealthDataIdentification;
import be.wiv_isp.healthdata.common.domain.search.EntitySearch;

import java.util.Date;
import java.util.Objects;

public class StatusMessageSearch extends EntitySearch {

	private Date createdOn;
	private Boolean lastOnly;
	private HealthDataIdentification healthDataIdentification;
	private Date createdOnAfter;

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Boolean getLastOnly() {
		return lastOnly;
	}

	public void setLastOnly(Boolean lastOnly) {
		this.lastOnly = lastOnly;
	}

	public HealthDataIdentification getHealthDataIdentification() {
		return healthDataIdentification;
	}

	public void setHealthDataIdentification(HealthDataIdentification healthDataIdentification) {
		this.healthDataIdentification = healthDataIdentification;
	}

	public Date getCreatedOnAfter() {
		return createdOnAfter;
	}

	public void setCreatedOnAfter(Date createdOnAfter) {
		this.createdOnAfter = createdOnAfter;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof StatusMessageSearch) {
			StatusMessageSearch other = (StatusMessageSearch) o;

			return Objects.equals(createdOn, other.createdOn) //
					&& Objects.equals(lastOnly, other.lastOnly) //
					&& Objects.equals(healthDataIdentification, other.healthDataIdentification);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash( //
				this.createdOn, //
				this.lastOnly, //
				this.healthDataIdentification);
	}

}
