/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.tasks;

import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.IAbstractParticipationWorkflowService;
import be.wiv_isp.healthdata.orchestration.service.IAbstractRegistrationWorkflowService;
import be.wiv_isp.healthdata.orchestration.service.IMessageProcessingService;
import be.wiv_isp.healthdata.orchestration.service.IMessagingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;

import java.util.Date;

public abstract class AbstractMessagingTask extends SchedulableHealthDataTask {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractMessagingTask.class);

	@Autowired
	private IAbstractRegistrationWorkflowService registrationWorkflowService;

	@Autowired
	private IAbstractParticipationWorkflowService participationWorkflowService;

	@Autowired
	private IMessagingService messagingService;

	@Autowired
	private IMessageProcessingService messageProcessingService;

	@Autowired
	private ElasticSearchTask elasticSearchIndexingTask;

	@Autowired
	private TaskScheduler taskScheduler;

	@Override
	public String getName() {
		return "MESSAGING";
	}

	@Override
	protected String getTimingExpression() {
		return configurationService.get(ConfigurationKey.SCHEDULED_TASK_MESSAGING_SERVICE_INTERVAL).getValue();
	}

	@Override
	public void execute() {
		executeCommonActions();
		executePlatformSpecificActions();
		taskScheduler.schedule(elasticSearchIndexingTask, new Date());
	}

	private void executeCommonActions() {
		try {
			registrationWorkflowService.processOutboxWorkflows();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		try {
			participationWorkflowService.processOutboxWorkflows();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		try {
			messagingService.send();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		try {
			messagingService.receive();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		try {
			messageProcessingService.process();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		try {
			messagingService.checkUnacknowledgedMessages();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	protected abstract void executePlatformSpecificActions();
}
