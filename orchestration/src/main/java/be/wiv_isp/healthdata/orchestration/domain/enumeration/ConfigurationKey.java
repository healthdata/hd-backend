/*
 * Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package be.wiv_isp.healthdata.orchestration.domain.enumeration;

import be.wiv_isp.healthdata.common.domain.enumeration.Platform;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import static be.wiv_isp.healthdata.common.domain.enumeration.Platform.HD4DP;
import static be.wiv_isp.healthdata.common.domain.enumeration.Platform.HD4RES;


public enum ConfigurationKey implements IConfigurationKey {

	ACKNOWLEDGMENT_TIME_LIMIT("Amount of time after which a message is assumed lost", Integer.class, true, true, false, false, HD4DP, HD4RES),
	ADT_SERVICE_HOST("The host of the ADT service", String.class, true, true, true, true, HD4DP),
	ADT_SERVICE_PORT("The port of the ADT service", String.class, true, true, true, true, HD4DP),
	APPLICATION_ID("Application ID of the certificate used by the Encryption Module", String.class, true, false, true, true, HD4DP, HD4RES),
	ARCHIVED_DIRECTORY("Folder in which archived messages are stored", String.class, true, false, true, true, HD4DP),
	CATALOGUE_HOST("The host of the Healthdata catalogue", String.class, true, true, false, false, HD4DP, HD4RES),
	CATALOGUE_PASSWORD("The password of the Healthdata catalogue", String.class, false, true, false, true, HD4RES),
	CATALOGUE_USERNAME("The username of the Healthdata catalogue", String.class, true, true, false, true, HD4RES),
	CATALOGUE_PROD_HOST("The host of the Healthdata PROD catalogue", String.class, true, true, false, true, HD4RES),
	CATALOGUE_PROD_USERNAME("The username of the Healthdata PROD catalogue", String.class, true, true, false, true, HD4RES),
	CATALOGUE_PROD_PASSWORD("The password of the Healthdata PROD catalogue", String.class, false, true, false, true, HD4RES),
	CATALOGUE_REGISTRATION_TOKEN("The registration token of the HealthData catalogue", String.class, false, true, false, false, HD4DP),
	CUSTOM_REST_NRC_PASSWORD("The password for connection to a REST service that connects to the national register", String.class, false, true, true, true, HD4DP),
	CUSTOM_REST_NRC_URL("The username for connection to a REST service that connects to the national register", String.class, true, true, true, true, HD4DP),
	CUSTOM_REST_NRC_USER("The username for connection to a REST service that connects to the national register", String.class, true, true, true, true, HD4DP),
	DWH_TO_PROCESS_DIRECTORY("Location to write JSON file for the data warehouse software", String.class, true, true, false, true, HD4RES),
	EHEALTH_CODAGE_APPLICATION_ID("Identification type of the eHealth certificate for eHealth batch codage", String.class, true, true, false, false, HD4DP, HD4RES),
	EHEALTH_CODAGE_IDENTIFICATION_TYPE("Identification value of the eHealth certificate for eHealth batch codage", String.class, true, true, false, false, HD4DP, HD4RES),
	EHEALTH_CODAGE_IDENTIFICATION_VALUE("Application ID of the eHealth certificate for eHealth batch codage", String.class, true, true, false, false, HD4DP, HD4RES),
	EHEALTH_CODAGE_TTP_PROJECT("The TTP project of eHealth batch codage", String.class, true, true, false, false, HD4DP, HD4RES),

	EM_INTERFACE_TYPE("Indicates whether the communication with the Encryption Module is via rest service (" + EMInterfaceType.REST + ") or via file interface (" + EMInterfaceType.FILE_SYSTEM + ")", EMInterfaceType.class, true, false, true, true, HD4DP),
	@Deprecated // can be removed when Q1-2017 is installed everywhere
	EM_INTERFACE_IN("This configuration is not relevant anymore and will be removed in a next version. Please use EM_INTERFACE_TYPE instead.", EMInterfaceType.class, true, false, true, true, HD4DP),
	@Deprecated // can be removed when Q1-2017 is installed everywhere
	EM_INTERFACE_OUT("This configuration is not relevant anymore and will be removed in a next version. Please use EM_INTERFACE_TYPE instead.", EMInterfaceType.class, true, false, true, true, HD4DP),

	EM_REST_URL_IN("Encryption Module URL for incoming messages", String.class, true, true, true, true, HD4DP, HD4RES),
	EM_REST_URL_OUT("Encryption Module URL for outgoing messages", String.class, true, true, true, true, HD4DP, HD4RES),
	ERROR_DIRECTORY("Folder in which error messages are stored. Only applicable in case " + EM_INTERFACE_TYPE + " is configured as " + EMInterfaceType.FILE_SYSTEM, String.class, true, false, true, true, HD4DP),
	ES_PATH_HOME("The home directory of Elasticsearch", String.class, true, true, false, true, HD4DP, HD4RES),
	EXTENDED_LOGGING("Extended logging activates the logging of ALL webservice requests/responses", Boolean.class, true, true, false, true, HD4DP, HD4RES),
	HD4RES_APPLICATION_ID("Application ID of the eHealth certificate of Healthdata", String.class, true, true, false, false, HD4DP),
	HD4RES_IDENTIFICATION_TYPE("Identification type of the eHealth certificate of Healthdata", String.class, true, true, false, false, HD4DP),
	HD4RES_IDENTIFICATION_VALUE("Identification value of the eHealth certificate of Healthdata", String.class, true, true, false, false, HD4DP),
	HEALTHDATA_ID_TYPE("Identification type of the Healthdata component", String.class, true, false, false, true, HD4DP, HD4RES),
	HEALTHDATA_ID_VALUE("Identification value of the Healthdata component", String.class, true, false, false, true, HD4DP, HD4RES),
	IDENTIFICATION_TYPE("Identification type of the eHealth certificate", String.class, true, false, true, true, HD4DP, HD4RES),
	IDENTIFICATION_VALUE("Identification value of the eHealth certificate", String.class, true, false, true, true, HD4DP, HD4RES),
	INCOMING_DIRECTORY("Folder in which incoming messages from the Encryption Module are stored. Only applicable in case " + EM_INTERFACE_TYPE + " is configured as " + EMInterfaceType.FILE_SYSTEM, String.class, true, false, true, true, HD4DP),
	LDAP_SERVER_URL("The URL of the LDAP server", String.class, true, true, false, true, HD4DP, HD4RES),
	LDAP_USER("The username for the LDAP server", String.class, true, true, false, true, HD4DP, HD4RES),
	LDAP_PASSWORD("The password for the LDAP server", String.class, false, true, false, true, HD4DP, HD4RES),
	LDAP_USERNAME_ATTRIBUTE("Attribute containing the username. Example: sAMAccountName", String.class, true, true, true, true, HD4DP, HD4RES),
	LDAP_LAST_NAME_ATTRIBUTE("Attribute containing the lastname. Example: sn", String.class, true, true, true, true, HD4DP, HD4RES),
	LDAP_FIRST_NAME_ATTRIBUTE("Attribute containing the firstname. Example: givenName", String.class, true, true, true, true, HD4DP, HD4RES),
	LDAP_MAIL_ATTRIBUTE("Attribute containing the email address. Example: mail", String.class, true, true, true, true, HD4DP, HD4RES),
	LDAP_USER_SEARCH_BASE("example: ou=users,dc=hd4dp,dc=be", String.class, true, true, true, true, HD4DP, HD4RES),
	MAIL_SMTP_HOST("The SMTP host address for sending mails", String.class, true, true, false, true, HD4DP, HD4RES),
	MAIL_SMTP_PORT("The SMTP port for sending mails", Integer.class, true, true, false, true, HD4DP, HD4RES),
	MAIL_FROM_ADDRESS("The mail address that is used to send mails from", String.class, true, true, false, true, HD4DP, HD4RES),
	MAIL_SMTP_USERNAME("The username for the SMTP server (empty if no authentication must be performed)", String.class, true, true, false, true, HD4DP, HD4RES),
	MAIL_SMTP_PASSWORD("The password for the SMTP server", String.class, false, true, false, true, HD4DP, HD4RES),
	MAIL_SECURITY_PROTOCOL("Protocol used for data encryption and authentication", MailSecurityProtocol.class, true, true, false, true, HD4DP, HD4RES),
	MAPPING_HOST("The host of the mapping API", String.class, true, true, false, true, HD4DP, HD4RES),
	MAX_ATTACHMENT_SIZE("The maximum (compressed) size for attachments", Integer.class, true, true, false, false, HD4DP, HD4RES),
	MOCK_WORKING_DIR("Working directory of the mock services", String.class, true, false, false, true, HD4RES),
	NATIONAL_REGISTER_CONNECTOR("Type of integration with a patient identifier service", NationalRegisterConnector.class, true, false, true, true, HD4DP),
	OUTGOING_DIRECTORY("Folder in which outgoing messages to the Encryption Module are stored. Only applicable in case " + EM_INTERFACE_TYPE + " is configured as " + EMInterfaceType.FILE_SYSTEM, String.class, true, false, true, true, HD4DP),
	FAST_TRACK_DIRECTORY("Root directory to create registry-specific folders in which CSV files can be placed to create and process pre-filled registrations", String.class, true, false, false, true, HD4DP),
	PROVISIONING_DIRECTORY("Root directory to create registry-specific folders in which CSV files can be placed to create pre-filled registrations", String.class, true, false, false, true, HD4DP),
	RRNCONNECTOR_APPLICATION_TOKEN("The application token used to authenticate in the SOAP connector", String.class, true, true, true, true, HD4DP),
	RRNCONNECTOR_URL("The url for connection to a SOAP service that connects to the national register", String.class, true, true, true, true, HD4DP),
	RRNCONNECTOR_USER_ID("The user id used to authenticate in the SOAP connector", String.class, true, true, true, true, HD4DP),
	SALESFORCE_CLIENT_ID("The client id parameter of Salesforce", String.class, false, true, false, true, HD4RES),
	SALESFORCE_CLIENT_SECRET("The client secret parameter of Salesforce", String.class, false, true, false, true, HD4RES),
	SALESFORCE_PASSWORD("The password of Salesforce", String.class, false, true, false, true, HD4RES),
	SALESFORCE_URL("The url of Salesforce", String.class, true, true, false, true, HD4RES),
	SALESFORCE_USERNAME("The username of Salesforce", String.class, true, true, false, true, HD4RES),
	SCHEDULED_TASK_DATAWAREHOUSE_SERVICE_INTERVAL("Interval to check for approved documents and notify sending HD4DP (in seconds)", Integer.class, true, true, false, false, HD4RES),
	SCHEDULED_TASK_ELASTIC_SEARCH_INDEXING_INTERVAL("Interval to look for documents to be indexed (in seconds)", Integer.class, true, true, false, false, HD4DP, HD4RES),
	SCHEDULED_TASK_MESSAGING_SERVICE_INTERVAL("Interval to check for new incoming messages and send outgoing messages (in seconds)", Integer.class, true, true, false, false, HD4DP, HD4RES),
	SCHEDULED_TASK_PROVISIONING_INTERVAL("Interval to check for new provisioning files (in seconds)", Integer.class, true, true, false, false, HD4DP),
	SCHEDULED_TASK_STATUS_MESSAGE_ENABLED("Enable sending a status message to Healthdata to allow monitoring. Status messages do not contain any personal nor sensitive data", Boolean.class, true, false, false, true, HD4DP),
	SHIBBOLETH_LOGOUT_ENDPOINT("Logout endpoint of the Shibboleth Service Provider", String.class, true, true, false, false, HD4DP),
	DAILY_TASK_EXECUTION_TIME("Defines when the daily background processes should be executed", String.class, true, true, false, false, HD4DP, HD4RES),
	SUPER_ADMIN_ENABLED("Determine whether the admin can log in when LDAP authentication is used", Boolean.class, true, true, false, true, HD4DP, HD4RES),
	SUPPORT_ENABLED("Determine whether the support accounts are active", Boolean.class, true, true, false, true, HD4DP, HD4RES),
	USE_DATA_DEFINITION_EDITOR("Allow data collection definitions to be changed by the DDE", Boolean.class, true, true, false, true, HD4RES),
	USE_DATA_DEFINITION_PUBLISH("Allow data collection definitions to be published to catalogue configured in " + CATALOGUE_PROD_HOST + " configuration", Boolean.class, true, true, false, true, HD4RES),
	USE_SALESFORCE("Allows bypassing Salesforce", Boolean.class, true, true, false, true, HD4RES),
	STABLE_DATA_TASK_TRIGGER("Defines when the stable data task should fire up", String.class, true, true, false, false, HD4DP, HD4RES),
	STABLE_DATA_RECORDS_PER_RUN("Number of stable data records that are sent during one run of the stable data task", String.class, true, true, false, true, HD4RES),
	STATUS_MESSAGE_VALIDITY_PERIOD("HD4RES only responds to status messages that were received during the past validity period", String.class, true, true, false, false, HD4RES),
	UPLOADED_STABLE_DATA_NOTIFICATION_ENABLED("Determines whether users are notified when stable data upload is completed", Boolean.class, true, true, false, true, HD4RES),
	USE_VALIDATION("Allows bypassing validation when receiving a document", Boolean.class, true, true, false, true, HD4DP, HD4RES),
	USER_MANAGEMENT_TYPE("User management can be performed via the application database (" + UserManagementType.DATABASE + "), or using the organization's LDAP server and the application database (" + UserManagementType.DATABASE_LDAP + ": authentication via LDAP, authorization via the application)", UserManagementType.class, true, false, true, true, HD4DP, HD4RES),
	XCONNECT_CONNECTOR_URL("The URL of the XConnect national register connector", String.class, true, true, true, true, HD4DP),
	XCONNECT_CONNECTOR_CAMPUS("The campus parameter for the XConnect national register connector", String.class, true, true, true, true, HD4DP),
	XCONNECT_CONNECTOR_APPLICATION_ID("The application ID parameter for the XConnect national register connector", String.class, true, true, true, true, HD4DP),
	XCONNECT_CONNECTOR_IDENTIFICATION_VALUE("The identification value parameter for the XConnect national register connector", String.class, true, true, true, true, HD4DP),
	HD4PRC_WORKFLOW_EXPIRATION_TIME("the time in minutes it takes for an HD4PrC eForms workflow to expire", Integer.class, true, true, false, false, HD4DP),
	HEALTHDATA_HD4PRC_ID_TYPE("Identification type of the HD4PrC component", String.class, true, false, false, true, HD4RES),
	HEALTHDATA_HD4PRC_ID_VALUE("Identification value of the HD4PrC component", String.class, true, false, false, true, HD4RES),
	HEALTHDATA_EMAIL("The email of Healthdata used for some notifications", String.class, true, false, false, true, HD4DP, HD4RES),
	HANDLEBARS_HEADER("The path to the handlebars PDF header", String.class, true, true, false, true, HD4RES),
	HANDLEBARS_FOOTER("The path to the handlebars PDF footer", String.class, true, true, false, true, HD4RES),
	PDF_HOST("The host to the PDF creator", String.class, true, true, false, true, HD4DP),
	END_DATA_COLLECTION_PERIOD_REMINDERS("The intervals at which a notifications are sent before the end of a data collection period", String.class, true, true, false, false, HD4DP, HD4RES),
	REGISTRATIONS_EMAIL_NOTIFICATION_ENABLED("Determine whether users are notified when registrations are waiting for their input", Boolean.class, true, false, false, true, HD4DP, HD4RES),
	USE_GUEST_ACCOUNT("Activate guests accounts, these accounts can't change passwords or edit data collections", Boolean.class, true, true, false, false, HD4DP, HD4RES),
	GUI_HOST("The host of the application", String.class, true, true, false, false, HD4DP, HD4RES),
	SUPPORT_EMAIL("The email address of HealthData support", String.class, true, true, false, false, HD4DP, HD4RES),
	PASSWORD_RESET_TOKEN_VALIDITY_PERIOD("Determines how long a password reset token is valid", String.class, true, true, false, false, HD4DP, HD4RES),
	MESSAGING_QUEUE_SIZE("Number of outgoing messages to group together", Integer.class, true, true, false, false, HD4DP, HD4RES);

	private String description;
	private Class type;
	private boolean readable;
	private boolean advanced;
	private boolean organizationConfig;
	private boolean visible;
	private Platform [] platforms;


	ConfigurationKey(String description, Class type, boolean readable, boolean advanced, boolean organizationConfig, boolean visible, Platform... platforms) {
		this.description = description;
		this.type = type;
		this.readable = readable;
		this.advanced = advanced;
		this.organizationConfig = organizationConfig;
		this.visible = visible;
		this.platforms = platforms;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String getType() {
		if (type.isEnum())
			return "ENUM";
		return type.getSimpleName().toUpperCase();
	}

	public Class getFullType() {
		return type;
	}

	public boolean isReadable() {
		return readable;
	}

	public boolean isAdvanced() {
		return advanced;
	}

	public boolean isOrganizationConfig() {
		return organizationConfig;
	}

	public boolean isVisible() {
		return visible;
	}

	public boolean isHd4dp() {
		return Arrays.asList(platforms).contains(HD4DP);
	}

	public boolean isHd4res() {
		return Arrays.asList(platforms).contains(HD4RES);
	}

	public boolean isApplicable(Platform platform) {
		return Arrays.asList(platforms).contains(platform);
	}

	public static List<ConfigurationKey> getOrganizationConfigurations() {
		final List<ConfigurationKey> result = new ArrayList<>();

		for (final ConfigurationKey ck : ConfigurationKey.values()) {
			if (ck.isOrganizationConfig()) {
				result.add(ck);
			}
		}

		return result;
	}
}