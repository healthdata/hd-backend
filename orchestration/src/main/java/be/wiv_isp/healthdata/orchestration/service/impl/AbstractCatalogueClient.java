/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.service.impl;

import be.wiv_isp.healthdata.common.rest.HttpHeaders;
import be.wiv_isp.healthdata.common.rest.WebServiceBuilder;
import be.wiv_isp.healthdata.common.util.TokenUtils;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.ConfigurationKey;
import be.wiv_isp.healthdata.orchestration.service.ICatalogueClient;
import be.wiv_isp.healthdata.orchestration.service.IConfigurationService;
import be.wiv_isp.healthdata.orchestration.service.IWebServiceClientService;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.core.Response;

public abstract class AbstractCatalogueClient implements ICatalogueClient {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractCatalogueClient.class);

    private String accessToken;

    @Autowired
    private IWebServiceClientService webServiceClientService;

    @Autowired
    private IConfigurationService configurationService;

    @Override
    public ClientResponse addAuthorizationHeaderAndCallWebService(WebServiceBuilder wsb) {
        if(accessToken == null) {
            refreshAccessToken();
        }

        wsb.setReturnType(new GenericType<ClientResponse>() {});
        wsb.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
        ClientResponse response = (ClientResponse) webServiceClientService.callWebService(wsb);

        if(Response.Status.UNAUTHORIZED.getStatusCode() == response.getStatus()) {
            LOG.debug("Received status code {}, access token might be expired. Refreshing access token and retrying again.", response.getStatus());
            refreshAccessToken();
            wsb.addHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
            response = (ClientResponse) webServiceClientService.callWebService(wsb);
        }

        return response;
    }

    private void refreshAccessToken() {
        LOG.debug("Retrieving access token");
        final String catalogueHost = configurationService.get(ConfigurationKey.CATALOGUE_HOST).getValue();
        accessToken = TokenUtils.getAccessTokenCatalogue(catalogueHost, getUsername(), getPassword());
        LOG.debug("Access token received");
    }

    protected abstract String getUsername();

    protected abstract String getPassword();

}
