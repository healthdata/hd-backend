/*
 * Copyright 2014-2016 Healthdata.be
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.wiv_isp.healthdata.orchestration.domain.mail;

import be.wiv_isp.healthdata.orchestration.domain.PeriodExpression;

public class PasswordResetContext extends AbstractVelocityContext {

    private String lastName;
    private String firstName;
    private String resetPasswordLink;
    private PeriodExpression validityPeriod;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getResetPasswordLink() {
        return resetPasswordLink;
    }

    public void setResetPasswordLink(String resetPasswordLink) {
        this.resetPasswordLink = resetPasswordLink;
    }

    public PeriodExpression getValidityPeriod() {
        return validityPeriod;
    }

    public void setValidityPeriod(PeriodExpression validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    @Override
    protected void doBuild() {
        put("lastName", lastName);
        put("firstName", firstName);
        put("resetPasswordLink", resetPasswordLink);
        put("validityPeriod", validityPeriod);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PasswordResetContext)) return false;
        if (!super.equals(o)) return false;

        PasswordResetContext context = (PasswordResetContext) o;

        if (lastName != null ? !lastName.equals(context.lastName) : context.lastName != null) return false;
        if (firstName != null ? !firstName.equals(context.firstName) : context.firstName != null) return false;
        if (resetPasswordLink != null ? !resetPasswordLink.equals(context.resetPasswordLink) : context.resetPasswordLink != null)
            return false;
        return !(validityPeriod != null ? !validityPeriod.equals(context.validityPeriod) : context.validityPeriod != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (resetPasswordLink != null ? resetPasswordLink.hashCode() : 0);
        result = 31 * result + (validityPeriod != null ? validityPeriod.hashCode() : 0);
        return result;
    }
}
