/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import be.wiv_isp.healthdata.common.json.deserializer.TimestampDeserializer;
import be.wiv_isp.healthdata.common.json.serializer.TimestampSerializer;
import be.wiv_isp.healthdata.common.util.StringUtils;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowActionType;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatusTransitionType;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@MappedSuperclass
abstract public class AbstractWorkflowHistory implements Comparable<AbstractWorkflowHistory> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "WORKFLOW_HISTORY_ID")
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "ACTION", nullable = false)
	private WorkflowActionType action;

	@Enumerated(EnumType.STRING)
	@Column(name = "NEW_STATUS", nullable = false)
	private WorkflowStatus newStatus;

	@Enumerated(EnumType.STRING)
	@Column(name = "TRANSITION_TYPE", nullable = false)
	private WorkflowStatusTransitionType transitionType;

	@Column(name = "EXECUTED_ON", nullable = false)
	@JsonSerialize(using = TimestampSerializer.class)
	@JsonDeserialize(using = TimestampDeserializer.class)
	private Timestamp executedOn;

	@Column(name = "EXECUTED_BY")
	private String executedBy;

	@Column(name = "SOURCE")
	private String source;

	@Column(name = "FILE_NAME")
	private String file;

	@Column(name = "MESSAGE_TYPE")
	private String messageType;

	@Column(name = "COMMIT_CODE")
	private String commit;


	@JsonIgnore
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public WorkflowActionType getAction() {
		return action;
	}

	public void setAction(WorkflowActionType action) {
		this.action = action;
	}

	public WorkflowStatus getNewStatus() {
		return newStatus;
	}

	public void setNewStatus(WorkflowStatus newStatus) {
		this.newStatus = newStatus;
	}

	public Timestamp getExecutedOn() {
		return executedOn;
	}

	public void setExecutedOn(Timestamp executedOn) {
		this.executedOn = executedOn;
	}

	public String getExecutedBy() {
		return executedBy;
	}

	public void setExecutedBy(String executedBy) {
		this.executedBy = executedBy;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getCommit() {
		return commit;
	}

	public void setCommit(String commit) {
		this.commit = commit;
	}

	abstract public Progress getProgress();

	abstract public void setProgress(Progress progress);

	@PrePersist
	public void onCreate() {
		setExecutedOn(new Timestamp(new Date().getTime()));
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof AbstractWorkflowHistory) {
			AbstractWorkflowHistory other = (AbstractWorkflowHistory) o;

			return Objects.equals(id, other.id)
					&& Objects.equals(action, other.action)
					&& Objects.equals(newStatus, other.newStatus)
					&& Objects.equals(executedOn, other.executedOn)
					&& Objects.equals(executedBy, other.executedBy)
					&& Objects.equals(source, other.source)
					&& Objects.equals(file, other.file)
					&& Objects.equals(messageType, other.messageType);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
				this.id,
				this.action,
				this.newStatus,
				this.executedOn,
				this.executedBy,
				this.source,
				this.file,
				this.messageType);
	}

	@Override
	public String toString() {
		return StringUtils.toString(this);
	}

	@Override
	public int compareTo(AbstractWorkflowHistory other) {
		if (this.executedOn == null && other.executedOn == null) {
			return 0;
		}
		if (this.executedOn == null) {
			return -1;
		}
		if (other.executedOn == null) {
			return 1;
		}
		if (this.executedOn.equals(other.executedOn)) {
			return this.action.compareTo(other.action);
		}
		return this.executedOn.compareTo(other.executedOn);
	}

	public WorkflowStatusTransitionType getTransitionType() {
		return transitionType;
	}

	public void setTransitionType(WorkflowStatusTransitionType transitionType) {
		this.transitionType = transitionType;
	}
}