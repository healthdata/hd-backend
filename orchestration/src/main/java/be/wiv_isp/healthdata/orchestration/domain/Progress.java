/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.util.Objects;

@MappedSuperclass
public class Progress {

	@Id
	@GeneratedValue(generator="progressIdGenerator")
	@GenericGenerator(name="progressIdGenerator", strategy="foreign", parameters=@Parameter(value="workflowHistory", name="property"))
	@Column(name = "WORKFLOW_HISTORY_ID")
	private Long id;

	@Column(name = "VALID")
	private long valid;

	@Column(name = "INVALID")
	private long invalid;

	@Column(name = "OPTIONAL")
	private long optional;

	@Column(name = "TOTAL")
	private long total;

	@Column(name = "VALID_RATIO")
	private double validRatio;

	@Column(name = "INVALID_RATIO")
	private double invalidRatio;

	@Column(name = "OPTIONAL_RATIO")
	private double optionalRatio;

	@Column(name = "ERRORS")
	private long errors;

	@Column(name = "WARNINGS")
	private long warnings;

	public long getValid() {
		return valid;
	}

	public void setValid(long valid) {
		this.valid = valid;
	}

	public long getInvalid() {
		return invalid;
	}

	public void setInvalid(long invalid) {
		this.invalid = invalid;
	}

	public long getOptional() {
		return optional;
	}

	public void setOptional(long optional) {
		this.optional = optional;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public double getValidRatio() {
		return validRatio;
	}

	public void setValidRatio(double validRatio) {
		this.validRatio = validRatio;
	}

	public double getInvalidRatio() {
		return invalidRatio;
	}

	public void setInvalidRatio(double invalidRatio) {
		this.invalidRatio = invalidRatio;
	}

	public double getOptionalRatio() {
		return optionalRatio;
	}

	public void setOptionalRatio(double optionalRatio) {
		this.optionalRatio = optionalRatio;
	}

	public long getErrors() {
		return errors;
	}

	public void setErrors(long errors) {
		this.errors = errors;
	}

	public long getWarnings() {
		return warnings;
	}

	public void setWarnings(long warnings) {
		this.warnings = warnings;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}

		if (o instanceof Progress) {
			Progress other = (Progress) o;

			return Objects.equals(id, other.id) //
					&& Objects.equals(valid, other.valid) //
					&& Objects.equals(invalid, other.invalid) //
					&& Objects.equals(optional, other.optional) //
					&& Objects.equals(total, other.total) //
					&& Objects.equals(validRatio, other.validRatio) //
					&& Objects.equals(invalidRatio, other.invalidRatio) //
					&& Objects.equals(optionalRatio, other.optionalRatio) //
					&& Objects.equals(errors, other.errors) //
					&& Objects.equals(warnings, other.warnings);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(//
				this.id, //
				this.valid, //
				this.invalid, //
				this.optional, //
				this.total, //
				this.validRatio, //
				this.invalidRatio, //
				this.optionalRatio, //
				this.errors, //
				this.warnings);
	}

	@Override
	public String toString() {
		return "Progress {" + //
				"id = " + Objects.toString(this.id) + ", " + //
				"valid = " + Objects.toString(this.valid) + ", " + //
				"invalid = " + Objects.toString(this.invalid) + ", " + //
				"optional = " + Objects.toString(this.optional) + ", " + //
				"total = " + Objects.toString(this.total) + ", " + //
				"validRatio = " + Objects.toString(this.validRatio) + ", " + //
				"invalidRatio = " + Objects.toString(this.invalidRatio) + ", " + //
				"optionalRatio = " + Objects.toString(this.optionalRatio) + ", " + //
				"errors = " + Objects.toString(this.errors) + ", " + //
				"warnings = " + Objects.toString(this.warnings) + "}";
	}
}
