/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package be.wiv_isp.healthdata.orchestration.domain.search;

import be.wiv_isp.healthdata.common.domain.search.EntitySearch;
import be.wiv_isp.healthdata.common.domain.search.Field;
import be.wiv_isp.healthdata.common.domain.search.Ordering;
import be.wiv_isp.healthdata.orchestration.domain.Organization;
import be.wiv_isp.healthdata.orchestration.domain.enumeration.WorkflowStatus;

import java.util.Date;
import java.util.List;
import java.util.Set;

abstract public class AbstractWorkflowSearch extends EntitySearch implements IAbstractWorkflowSearch {

	protected Set<WorkflowStatus> statuses;
	protected String dataCollectionName;
	protected Long dataCollectionDefinitionId;
	protected Long notWorkflowId;
	protected boolean returnDeleted;
	protected Date notUpdatedAfterDate;
	protected Date createdAfterDate;
	protected Date updatedAfterDate;
	protected List<Long> workflowIds; // not used anymore
	protected Organization organization;
	protected Boolean withDocument = Boolean.TRUE; // checked on service level
	protected String sendStatus;

	public AbstractWorkflowSearch() {
		this.ordering = new Ordering(Field.UPDATED_ON, Ordering.Type.DESC);
	}

	@Override
	public Set<WorkflowStatus> getStatuses() {
		return statuses;
	}
	@Override
	public void setStatuses(Set<WorkflowStatus> statuses) {
		this.statuses = statuses;
	}

	@Override
	public String getDataCollectionName() {
		return dataCollectionName;
	}

	@Override
	public void setDataCollectionName(String dataCollectionName) {
		this.dataCollectionName = dataCollectionName;
	}

	@Override
	public Long getDataCollectionDefinitionId() {
		return dataCollectionDefinitionId;
	}

	@Override
	public void setDataCollectionDefinitionId(Long dataCollectionDefinitionId) {
		this.dataCollectionDefinitionId = dataCollectionDefinitionId;
	}

	@Override
	public Long getNotWorkflowId() {
		return notWorkflowId;
	}

	@Override
	public void setNotWorkflowId(Long notWorkflowId) {
		this.notWorkflowId = notWorkflowId;
	}

	@Override
	public boolean getReturnDeleted() {
		return returnDeleted;
	}

	@Override
	public void setReturnDeleted(boolean returnDeleted) {
		this.returnDeleted = returnDeleted;
	}

	@Override
	public Date getNotUpdatedAfterDate() {
		return notUpdatedAfterDate;
	}

	@Override
	public void setNotUpdatedAfterDate(Date notUpdatedAfterDate) {
		this.notUpdatedAfterDate = notUpdatedAfterDate;
	}

	@Override
	public Date getCreatedAfterDate() {
		return createdAfterDate;
	}

	@Override
	public void setCreatedAfterDate(Date createdAfterDate) {
		this.createdAfterDate = createdAfterDate;
	}

	@Override
	public Date getUpdatedAfterDate() {
		return updatedAfterDate;
	}

	@Override
	public void setUpdatedAfterDate(Date updatedAfterDate) {
		this.updatedAfterDate = updatedAfterDate;
	}

	@Override
	public List<Long> getWorkflowIds() {
		return workflowIds;
	}

	@Override
	public void setWorkflowIds(List<Long> workflowIds) {
		this.workflowIds = workflowIds;
	}

	@Override
	public Boolean getWithDocument() {
		return withDocument;
	}

	@Override
	public void setWithDocument(Boolean withDocument) {
		this.withDocument = withDocument;
	}

	@Override
	public Organization getOrganization() {
		return organization;
	}

	@Override
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	@Override
	public String getSendStatus() {
		return sendStatus;
	}

	@Override
	public void setSendStatus(String sendStatus) {
		this.sendStatus = sendStatus;
	}

}
